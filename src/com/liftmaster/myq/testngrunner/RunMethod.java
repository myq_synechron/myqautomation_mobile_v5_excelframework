package com.liftmaster.myq.testngrunner;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import org.testng.ITestContext;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Recordset;
import com.liftmaster.myq.testmethods.Android;
import com.liftmaster.myq.testmethods.IOS;
import com.liftmaster.myq.utils.AppiumServer;
import com.liftmaster.myq.utils.UtilityExcel;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class RunMethod {

	AndroidDriver<AndroidElement> androidDriver;
	IOSDriver<IOSElement> iOSDriver;
	public static String env;
	public String activeUserRole;
	public String userType;

	/**
	 * Description : This function will kill all the nodes
	 */
	@BeforeSuite
	public void killNodes() {
		AppiumServer.stopServer();
	}

	/**
	 * Description : This function would fetch all the desired input data from the excel
	 * @throws IOException 
	 */
	@BeforeTest
	public void runtest(ITestContext tContext) throws FilloException, InterruptedException, IOException {

		String testName = tContext.getCurrentXmlTest().getName(); 
		String filepath = ".//TestConfig//MyQTestExecution_Controller.xlsm";

		Recordset recordset = UtilityExcel.ReadExcel(filepath, "EnvironmentName","TestConfig", "ExecutionFlag", "Yes");

		HashMap<String,String> input = new HashMap<String,String>();
		while (recordset.next()) {
			ArrayList<String> dataColl = recordset.getFieldNames();
			Iterator<String> dataIterator = dataColl.iterator();
			while (dataIterator.hasNext()) {
				for (int i = 0; i <= dataColl.size() - 1; i++) { 
					String data = dataIterator.next();
					String dataVal = recordset.getField(data);
					input.put(data,dataVal);
				}
				break;
			}

			env = input.get("EnvironmentName");
			userType = input.get("UserType");
			String testGroup = input.get("TestGroup");
			activeUserRole = input.get("UserRole");
			String platform = input.get("Platform");
			String deviceTest = input.get("Device");
			String brandName = UtilityExcel.getParameters(testName,1);

			ArrayList<String> testCases = UtilityExcel.ReadExcelDataValue(filepath, "TestCase",testGroup,null,null);
			ArrayList<String> testData = UtilityExcel.ReadExcelDataValue(filepath, activeUserRole, testGroup, null, null);
			ArrayList<String> testScenarioName = UtilityExcel.ReadExcelDataValue(filepath, "TestScenarioName", testGroup, null, null);
 
			if (platform.equals("Android")) {
				if (testName.contains(deviceTest)) {

					Android android = new Android(androidDriver, env, activeUserRole, userType);
					androidDriver = android.setUp(testName);
					android.configReport(testName,env,deviceTest,brandName);

					for (int i = 0; i < testCases.size(); i++) {
						
						 if(testCases.get(i).equals("")|| testCases.get(i).equals(null)) {
							 break;
						 }
						ArrayList<String> testDataValueAndroid = new ArrayList<String>();
						testDataValueAndroid.add(testData.get(i));
						
						String testScenarioValueAndroid = testScenarioName.get(i);
						if(testScenarioValueAndroid.equals("")){
							testScenarioValueAndroid = testCases.get(i);
						}
						try {   
							Method method = android.getClass().getMethod(testCases.get(i), ArrayList.class, String.class , String.class );
							method.invoke(android, testDataValueAndroid, deviceTest, testScenarioValueAndroid);
						} catch (Exception e) {
							e.printStackTrace();
							if(e.getClass().getName().equals("java.lang.NoSuchMethodException")) {
								android.skipAndroidTest(testCases.get(i), deviceTest);
							}
						} 
						android.reportFlushAndroid(deviceTest);
					}
					android.applyCustomCSS(deviceTest,brandName);
					android.quitAndroidDriver();
				}
			} else {
				if (testName.contains(deviceTest)) {
					IOS iOS = new IOS(iOSDriver, env, activeUserRole, userType);
					iOSDriver = iOS.setUp(testName);
					iOS.configReport(testName,env,deviceTest,brandName);
					
					for (int i = 0; i < testCases.size(); i++) {
						 if(testCases.get(i).equals("")|| testCases.get(i).equals(null)) {
							 break;
						 }
						ArrayList<String> testDataValueIOS = new ArrayList<String>();
						testDataValueIOS.add(testData.get(i));
						
						String testScenarioValueIOS = testScenarioName.get(i);
						if(testScenarioValueIOS.equals("")){
							testScenarioValueIOS = testCases.get(i);
						}
						try {   
							Method method = iOS.getClass().getMethod(testCases.get(i), ArrayList.class, String.class, String.class);
							method.invoke(iOS, testDataValueIOS, deviceTest, testScenarioValueIOS);
						} catch (Exception e) {
							e.printStackTrace();
							if(e.getClass().getName().equals("java.lang.NoSuchMethodException")) {
								iOS.skipIOSTest(testCases.get(i), deviceTest);
							}
						} 
						iOS.reportFlushIOS(deviceTest);
					}
					iOS.applyCustomCSS(deviceTest,brandName);
					iOS.quitIOSDriver();
				}
			}
		}
	}
	public static String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }
}
