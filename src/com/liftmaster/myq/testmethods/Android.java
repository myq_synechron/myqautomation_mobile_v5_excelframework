package com.liftmaster.myq.testmethods;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.liftmaster.myq.android.pages.AlertPageAndroid;
import com.liftmaster.myq.android.pages.DeviceManagementPageAndroid;
import com.liftmaster.myq.android.pages.DevicesPageAndroid;
import com.liftmaster.myq.android.pages.HelpPageAndroid;
import com.liftmaster.myq.android.pages.HistoryPageAndroid;
import com.liftmaster.myq.android.pages.LoginPageAndroid;
import com.liftmaster.myq.android.pages.ManageUsersPageAndroid;
import com.liftmaster.myq.android.pages.MenuPageAndroid;
import com.liftmaster.myq.android.pages.SchedulePageAndroid;
import com.liftmaster.myq.android.pages.SignUpPageAndroid;
import com.liftmaster.myq.android.pages.UsersAccountPageAndroid;
import com.liftmaster.myq.utils.AppiumServer;
import com.liftmaster.myq.utils.DriverInitializer;
import com.liftmaster.myq.utils.GlobalVariables;
import com.liftmaster.myq.utils.UtilityAndroid;
import com.liftmaster.myq.utils.UtilityExcel;
import com.liftmaster.myq.utils.WebServices;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;


public class Android {

	public ExtentHtmlReporter extentHtmlReporterAndroid1,extentHtmlReporterAndroid2;
	public ExtentReports extentReportsAndroid1,extentReportsAndroid2;
	public ExtentTest extentTestAndroid;
	public AndroidDriver<AndroidElement> androidDriver = null;

	String brandName;
	String platform;
	String deviceName;
	String udid;
	String appPackage;
	String appActivity;
	String appiumServer;
	int appiumPort;
	int bootstrapPort;
	String email;
	String password;

	LoginPageAndroid loginPageAndroid = null;
	MenuPageAndroid menuPageAndroid = null;
	DeviceManagementPageAndroid deviceManagementPageAndroid = null;
	DevicesPageAndroid devicesPageAndroid = null;
	AlertPageAndroid alertPageAndroid = null;
	SchedulePageAndroid schedulePageAndroid = null;
	ManageUsersPageAndroid manageUsersPageAndroid = null;
	UsersAccountPageAndroid usersAccountPageAndroid = null;
	SignUpPageAndroid signUpPageAndroid = null;
	HelpPageAndroid helpPageAndroid = null;
	UtilityAndroid utilityAndroid = null;
	HistoryPageAndroid historyPageAndroid = null;

	String device = null;
	String activeAccountName;
	boolean roleSelected;
	boolean multiUser;
	boolean adminInvitationSent = false;
	boolean familyInvitationSent = false;
	boolean guestInvitationSent = false;
	boolean userAsAdminAccepted = false;
	boolean userAsFamilyAccepted = false;
	boolean userAsGuestAccepted = false;

	public String env;
	public String activeUserRole;
	public String userType;
	public int deviceCount;

	public Android(AndroidDriver<AndroidElement> androidDriver,String env,String activeUserRole,String userType){
		this.androidDriver = androidDriver;
		this.env = env;
		this.activeUserRole = activeUserRole;
		this.userType = userType;
	}

	/**
	 * Description	 :	This function is use to create test reports
	 */
	public ExtentTest createTest(String deviceTest, String testName){
		if (deviceTest.equals("Android1")) {
			extentTestAndroid = extentReportsAndroid1.createTest(testName);
		} else {
			extentTestAndroid = extentReportsAndroid2.createTest(testName);
		}
		return extentTestAndroid;
	}

	/**
	 * Description	 :	This function is for configuring reports
	 * @throws IOException 
	 */
	public void configReport(String testName,String environmentName,String deviceTest,String brandName) throws InterruptedException, IOException {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String strDate = formatter.format(date);
		File file = new File(".//Reports//"+strDate);
		if (!file.exists()) {
			file.mkdirs();
		}
		String dateTime = new SimpleDateFormat("dd-MM-yyyy-hhmmss").format(new Date());
		if (deviceTest.equals("Android1")) {
			extentHtmlReporterAndroid1 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			Thread.sleep(15000);
			extentReportsAndroid1 = new ExtentReports();		
			extentReportsAndroid1.attachReporter(extentHtmlReporterAndroid1);
			extentReportsAndroid1.setSystemInfo("Environment", environmentName);
			extentHtmlReporterAndroid1.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterAndroid1.config().setReportName("MyQ Android Report");
			extentHtmlReporterAndroid1.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterAndroid1.config().setTheme(Theme.STANDARD);
		} else {
			extentHtmlReporterAndroid2 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			Thread.sleep(15000);
			extentReportsAndroid2 = new ExtentReports();		
			extentReportsAndroid2.attachReporter(extentHtmlReporterAndroid2);
			extentReportsAndroid2.setSystemInfo("Environment", environmentName);
			extentHtmlReporterAndroid2.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterAndroid2.config().setReportName("MyQ Android Report");
			extentHtmlReporterAndroid2.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterAndroid2.config().setTheme(Theme.STANDARD);
		}
	}

	/**
	 * Description : This function would fetch all the desired capabilities from excel (DeviceInfo sheet)
	 */
	public AndroidDriver<AndroidElement> setUp(String testName) throws InterruptedException {
		brandName = UtilityExcel.getParameters(testName,1);
		platform = UtilityExcel.getParameters(testName,2);
		deviceName  = UtilityExcel.getParameters(testName,3);
		udid = UtilityExcel.getParameters(testName,4);
		appPackage = UtilityExcel.getParameters(testName,5);
		appActivity = UtilityExcel.getParameters(testName,6);
		appiumServer = UtilityExcel.getParameters(testName,7);
		appiumPort = Integer.parseInt(UtilityExcel.getParameters(testName,8));
		bootstrapPort = Integer.parseInt(UtilityExcel.getParameters(testName,9));

		AppiumServer.startServer(appiumServer,appiumPort);
		Thread.sleep(5000);

		androidDriver = DriverInitializer.getAndroidDriver(platform, deviceName, udid, appPackage, appActivity, appiumServer, appiumPort,bootstrapPort);
		return androidDriver;
	}

	/**
	 * Description	 :	This is the login test case
	 * @throws InterruptedException 
	 */
	public void loginAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {
		email = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);

		//select environment and version for app and login to application
		loginPageAndroid.selectEnvironment(env)
		.selectVersion(userType)
		.login(email, password);
		if(userType.equalsIgnoreCase("Single")) {
			roleSelected = true;
			multiUser = false;
		} else if (userType.equalsIgnoreCase("Multi User")) {
			multiUser = true;
			if(activeUserRole.equalsIgnoreCase("Creator")) {
				roleSelected = true;
			}
		}
	}

	/**
	 * Description : This method is for Android and would verify if blue bar is present and select the role passed from sheet and verify the number of devices
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 */
	public void MTA63_SelectAndValidateUserRole(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException {
		int actualCount;
		activeAccountName = testData.get(0).split(",")[0];
		deviceCount = Integer.parseInt(testData.get(0).split(",")[1]);

		extentTestAndroid = createTest(deviceTest, scenarioName + " : " +activeUserRole); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);

		roleSelected = devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);
		if (roleSelected) {
			extentTestAndroid.log(Status.PASS, activeUserRole + " is selected");
			if(devicesPageAndroid.verifyNoDeviceText() && deviceCount == 0) {
				extentTestAndroid.log(Status.PASS, "You have no devices on your account");				
			} else {
				actualCount = devicesPageAndroid.verifyAccountDeviceCount(udid); 
				if (actualCount == deviceCount) {
					extentTestAndroid.log(Status.PASS, " There are " + deviceCount + " devices present ");
				} else {
					extentTestAndroid.log(Status.FAIL, "Actual no of devices -> " + actualCount + " expected count is -> " + deviceCount );
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available , hence cannot select");
		}
	}

	/**
	 * Description : This is an Android test method to add gateway / hubs using API(V2) calls.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void AddGatewayAndroid(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		extentTestAndroid = createTest(deviceTest,scenarioName); 

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyPlusButton()) {
				webService.SendGatewayOnlineService(gatewaySerialNumber);
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.addGateway(gatewaySerialNumber, udid)
				.enterGatewayName(gatewayName)
				.clickSaveButton()
				.clickBackButton();	
				if(deviceManagementPageAndroid.verifyHubName(gatewayName, udid)) {
					extentTestAndroid.log(Status.PASS, gatewayName + " is added");
				} else {
					extentTestAndroid.log(Status.FAIL, gatewayName + " is not added");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,   activeUserRole + " user doesn't have premission to add gateway ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user doesn't have premission to add gateway ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 *  Description : This is an Android test method to add light to the gateway / hubs using API(V2) calls. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void AddLightAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String lightSerialNumber;
		String lightName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		lightSerialNumber = testData.get(0).split(",")[2];
		lightName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.selectGateway(gatewayName, udid)
				.addNewDevice()
				.clickRemoteLightButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnLightToGatewayService(gatewaySerialNumber,lightSerialNumber);
				devicesPageAndroid.enterDeviceName(lightName)
				.clickOnNext();

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user has premission to add light ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add light ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,   activeUserRole + " user doesn't have premission to add light ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user doesn't have premission to add light ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 *  Description : This is an Android test method to add GDO to the gateway / hubs using API(V2) calls. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void AddGDOAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String gdoSerialNumber;
		String gdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		gdoSerialNumber = testData.get(0).split(",")[2];
		gdoName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.selectGateway(gatewayName, udid) 
				.addNewDevice()
				.clickGDOButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnGDOToGatewayService(gatewaySerialNumber,gdoSerialNumber);
				devicesPageAndroid.enterDeviceName(gdoName)
				.clickOnNext();

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user has premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, "Test MTA- :-  " + activeUserRole + " user has premission to add GDO ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,   activeUserRole + " user doesn't have premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user doesn't have premission to add GDO ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 *  Description : This is an Android test method to add CDO to the gateway / hubs using API(V2) calls. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void AddCDOAndroid(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String cdoSerialNumber;
		String cdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		cdoSerialNumber = testData.get(0).split(",")[2];
		cdoName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.selectGateway(gatewayName, udid) 
				.addNewDevice()
				.clickCDOButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnCDOToGatewayService(gatewaySerialNumber,cdoSerialNumber);
				devicesPageAndroid.enterDeviceName(cdoName)
				.clickOnNext();

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user has premission to add CDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add CDO ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,   activeUserRole + " user doesn't have premission to add CDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user doesn't have premission to add CDO ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 *  Description : This is an Android test method to add Gate to the gateway / hubs using API(V2) calls. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void AddGateAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String gateSerialNumber;
		String gateName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		gateSerialNumber = testData.get(0).split(",")[2];
		gateName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.selectGateway(gatewayName, udid) 
				.addNewDevice()
				.clickGateButton()
				.clickOnNext()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnGateToGatewayService(gatewaySerialNumber,gateSerialNumber);
				devicesPageAndroid.enterDeviceName(gateName)
				.clickOnNext();

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,   activeUserRole + " user has premission to add Gate ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add Gate ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,   activeUserRole + " user doesn't have premission to add Gate ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Gate ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to add Stronghold GDO to Ethernet Gateway. This method verifies if the gateway is added already,
	 * if not added, would then added the gateway, then would add the GDO and attach the lock to it,
	 * verify the lock attached , verify the push notification received , clear the notification
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA237_ValidateAddingStrGDOToEthernetGW(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String gdoSerialNumber;
		String gdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		gdoSerialNumber = testData.get(0).split(",")[2];
		gdoName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				//to check if the Gateway is already added or not
				if(deviceManagementPageAndroid.verifyHubName(gatewayName, udid) || deviceManagementPageAndroid.verifyHubName(gatewaySerialNumber, udid)) {
					extentTestAndroid.log(Status.INFO, gatewayName+" with serial no :" +gatewaySerialNumber+" is already added");
				} else {
					webService.SendGatewayOnlineService(gatewaySerialNumber);
					devicesPageAndroid.addGateway(gatewaySerialNumber, udid)
					.enterGatewayName(gatewayName)
					.clickSaveButton()
					.clickBackButton();
				}
				//adding the GDO to the device
				devicesPageAndroid.selectGateway(gatewayName, udid) 
				.addNewDevice()
				.clickGDOButton()
				.clickNextButton();
				//API call to add GDO
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnGDOToGatewayService(gatewaySerialNumber,gdoSerialNumber);
				//enter strong hold gdo device name
				devicesPageAndroid.enterDeviceName(gdoName)
				.clickNextButton();
				//verify is GDO is added or not 
				menuPageAndroid.clickMenuLink("Devices");
				if (devicesPageAndroid.verifyDevicePresent(gdoName, udid)) {
					extentTestAndroid.log(Status.PASS, gdoName+" is added  ");
					//if shield icon is already present , de-attach it 
					if(devicesPageAndroid.verifyShieldIcon(gdoName)) {
						webService.detachLockGDO(gdoSerialNumber);
					}
					//attach the lock to GDO and verify it
					webService.attachLockGDO(gdoSerialNumber);

					//Below code is commented as per MSTR-56 
					/*devicesPageAndroid.tapOnDevice(gdoName);

					if(devicesPageAndroid.verifyShieldIcon(gdoName)) {
						extentTestAndroid.log(Status.PASS, " Lock added on " + gdoName );
						if(devicesPageAndroid.verifyNotifications(gdoName)) {
							extentTestAndroid.log(Status.PASS, "Notifcation recevied for lock is ready");
						} else {
							extentTestAndroid.log(Status.FAIL, "Notifcation not recevied for lock is ready");
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Lock not added on " + gdoName );
						utilityAndroid.captureScreenshot(androidDriver); 
					}*/
				} else {
					extentTestAndroid.log(Status.FAIL,gdoName+" is not added ");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has premission to add GDO ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add GDO ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to add Wifi-GDO, verifies WGDO being added, then would attach the lock to the GDO that comes with WGDO
	 * verify the lock attached , verify the push notification received , clear the notification 
	 * @param testData
	 * @param deviceTest
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA230_ValidateAddingStrGDOToWGDO(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String wgdoSerialNumber;
		String wgdoName;
		String deviceSerialNumber;
		String deviceName;

		wgdoSerialNumber = testData.get(0).split(",")[0];
		wgdoName = testData.get(0).split(",")[1];
		deviceSerialNumber = testData.get(0).split(",")[2];
		deviceName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyPlusButton()) {
				//API call to Add Wifi GDO 
				webService.SendGatewayOnlineService(wgdoSerialNumber);
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.addWGDOAndGDO(wgdoSerialNumber, wgdoName,deviceName, udid); 
				webService.detachLockGDO(deviceSerialNumber);

				//navigate to device page and verify if GDO is there
				menuPageAndroid.clickMenuLink("Devices");
				if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
					extentTestAndroid.log(Status.PASS, "Strong Hold GDO is added");
					//attaching the lock to GDO
					webService.attachLockGDO(deviceSerialNumber);
					//Below code is commented as per MSTR-56 
					/*	devicesPageAndroid.tapOnDevice(deviceName);
					//verify shield icon 
					if(devicesPageAndroid.verifyShieldIcon(deviceName)) {
						extentTestAndroid.log(Status.PASS, "Lock added on " + deviceName );
						if(devicesPageAndroid.verifyNotifications(deviceName)) {
							extentTestAndroid.log(Status.PASS, "Notifcation recevied for lock is ready");
						} else {
							extentTestAndroid.log(Status.FAIL, "Notifcation not recevied for lock is ready");
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Lock not added on " + deviceName );
						utilityAndroid.captureScreenshot(androidDriver);
					}*/
					if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has premission to add strong hold GDO ");
					} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add strong hold GDO ");
					}
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName+" not found ");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add strong hold GDO");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add strong hold GDO");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available ");
		}
	}	

	/**
	 * Description : This is an Android test method to verify the lock icon when it is detached and the attach the lock 
	 * @param testData
	 * @param deviceTest
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA247_ValidateStrGDOLockIconWhenDetached (ArrayList<String> testData, String deviceTest, String scenarioName)throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException  {
		String gdoSerialNumber;
		String gdoName;

		gdoSerialNumber = testData.get(0).split(",")[0];
		gdoName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Devices")) {
				//remove the lock from StrongHold GDO
				webService.detachLockGDO(gdoSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify shield icon present or not
				if(!devicesPageAndroid.verifyShieldIcon(gdoName)) {
					extentTestAndroid.log(Status.PASS, "Lock removed from " + gdoName);
				} else {
					extentTestAndroid.log(Status.FAIL, "Lock is not removed from " + gdoName);
					utilityAndroid.captureScreenshot(androidDriver);
				}
				webService.attachLockGDO(gdoSerialNumber);

				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to control devices");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to control devices");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to add VGDO to it using API(V2) calls. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA337_ValidateAddingVGDOToAHub(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String ahubName;
		String vgdoName;
		String vgdoSerialNumber;

		ahubName  = testData.get(0).split(",")[0];
		vgdoName = testData.get(0).split(",")[1];
		vgdoSerialNumber = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				//adding the VGDO to the device
				devicesPageAndroid.selectGateway(ahubName, udid) 
				.addNewDevice()
				.clickGDOButton()
				.clickOnNext() // change as per 3.103 build
				.clickNextButton();
				webService.enterLearnModeForVGDO(vgdoSerialNumber);
				webService.testSensorVGDO(vgdoSerialNumber, "Test OOK");
				devicesPageAndroid.tapToSelect()
				.enterTextToSearch("LiftMaster")
				.clickOnBrandSearched()
				.tapToSelect()
				.selectColorOfProgramButton("Yellow")
				.clickOnProgramDoor()
				.clickOnContinueButton()
				.clickOnPressedProgramButton()
				.enterDeviceName(vgdoName)
				.clickOnNext();

				menuPageAndroid.clickMenuLink("Devices");
				if(devicesPageAndroid.verifyDevicePresent(vgdoName, udid)){
					extentTestAndroid.log(Status.PASS, vgdoName + " is added to " + ahubName);
				} else {
					extentTestAndroid.log(Status.FAIL, vgdoName + " is not added to " + ahubName);
					utilityAndroid.captureScreenshot(androidDriver);
				}
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add GDO ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add GDO ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to add VGDO to WifiHub using API(V2) calls, and verify if added successfully
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA359_ValidateAddingVGDOInWifiHub(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String wifihubName;
		String vgdoName;
		String vgdoSerialNumber;

		wifihubName =  testData.get(0).split(",")[0];
		vgdoSerialNumber = testData.get(0).split(",")[1];
		vgdoName = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				//adding the VGDO to the device
				devicesPageAndroid.selectGateway(wifihubName, udid) 
				.addNewDevice()
				.clickGDOButton()
				.clickOnNext() // change as per 3.103 build
				.clickNextButton();
				//learning VGDO to wifi-hub
				webService.enterLearnModeForVGDO(vgdoSerialNumber)
				.testSensorVGDO(vgdoSerialNumber, "BlueTooth");
				devicesPageAndroid.tapToSelect()
				.enterTextToSearch("LiftMaster")
				.clickOnBrandSearched()
				.tapToSelect()
				.selectColorOfProgramButton("Yellow")
				.clickOnProgramDoor()
				.clickOnContinueButton()
				.clickOnPressedProgramButton()
				.enterDeviceName(vgdoName)
				.clickOnNext();
				//verify vgdo added or not
				menuPageAndroid.clickMenuLink("Devices");
				if(devicesPageAndroid.verifyDevicePresent(vgdoName,udid)){
					extentTestAndroid.log(Status.PASS,  vgdoName + " is added to " + wifihubName);
				} else {
					extentTestAndroid.log(Status.FAIL,  vgdoName + " is not added to " + wifihubName);
					utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
				}

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has premission to add GDO ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add GDO ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to add light to the WGDO using API(V2) calls.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA356_ValidateAddingWGDO(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String wifiGDOSerialNumber;
		String wifiGDOName;
		String deviceName;

		wifiGDOSerialNumber = testData.get(0).split(",")[0];
		wifiGDOName = testData.get(0).split(",")[1];
		deviceName = testData.get(0).split(",")[2];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				webService.SendGatewayOnlineService(wifiGDOSerialNumber);
				devicesPageAndroid.addWGDOAndGDO(wifiGDOSerialNumber, wifiGDOName,deviceName, udid);
				if(deviceManagementPageAndroid.verifyHubName(wifiGDOName, udid)) {
					extentTestAndroid.log(Status.PASS, wifiGDOName+" is added successfully");
				} else {
					extentTestAndroid.log(Status.FAIL,wifiGDOName+" is not added");
					utilityAndroid.captureScreenshot(androidDriver);
				}

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has premission to add Wifi-GDO");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add Wifi-GDO ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add Wifi-GDO ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add Wifi-GDO ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}


	/**
	 * Description : This is an Android test method to verify WiFi-GDO is added as parent and child by trying to delete the GDO in Wifi-GDO and verify the error message
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA390_ValidateWGDOAsParentChild(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String wifiGDOName;
		String wifiGDODeviceName;
		String error_message = "This garage door is permanently linked to its hub and cannot be removed. To remove garage door, go to Manage Places and remove the hub. (601)";
		String tempText =  null;

		wifiGDOName = testData.get(0).split(",")[0];
		wifiGDODeviceName = testData.get(0).split(",")[1];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				devicesPageAndroid.selectGateway(wifiGDOName, udid)
				.clickOnDeleteForDevice(wifiGDODeviceName)
				.clickOnDeleteConfirmation();

				if(deviceManagementPageAndroid.verifyErrorPopup()) {
					tempText = deviceManagementPageAndroid.getPopUpText();
					if(tempText.equals(error_message)) {
						extentTestAndroid.log(Status.PASS, "Popup 'This GDO is linked to its hub and cannot be removed.' is displayed successfully");
						deviceManagementPageAndroid.clickOkButton();
					} else {
						utilityAndroid.captureScreenshot(androidDriver);
						extentTestAndroid.log(Status.FAIL, "Different error message displayed -> " + tempText);
						deviceManagementPageAndroid.clickOkButton();
					}
				} else {
					utilityAndroid.captureScreenshot(androidDriver);
					extentTestAndroid.log(Status.FAIL, "No Error popup was displayed while deleting the GDO");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to Device Management ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("CreatorAdmin")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to Device Management ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		} 
	} 

	/**
	 * Description : This is an Android test method to delete device from given gateway / hub.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void DeleteDeviceAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String gatewayName;
		String deviceName;

		gatewayName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				//deleting device
				devicesPageAndroid.selectGateway(gatewayName, udid)
				.deleteDevice(deviceName)
				.clickBackButton();
				//verify if device is deleted or not
				deviceManagementPageAndroid.tapOnHubName(gatewayName, udid);
				if (deviceManagementPageAndroid.verifyDeviceDeleted(deviceName, udid)) {
					deviceManagementPageAndroid.clickOnBackButton();
					extentTestAndroid.log(Status.PASS, deviceName+" is deleted ");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName +" is not deleted ");
					utilityAndroid.captureScreenshot(androidDriver);
					deviceManagementPageAndroid.clickOnBackButton();
				}
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has premission to delete device ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to delete device ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to delete device ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to delete device ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to delete Gateway / hubs
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void DeleteGatewayAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String gatewayName;
		gatewayName = testData.get(0).split(",")[0];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				deviceManagementPageAndroid.deleteHub(gatewayName, udid);

				if(deviceManagementPageAndroid.verifyHubName(gatewayName, udid)) {
					extentTestAndroid.log(Status.FAIL, gatewayName + " is not deleted");
				} else {
					extentTestAndroid.log(Status.PASS, gatewayName + " is deleted successfully");
				}

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has premission to delete hub/gateway ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to delete hub/gateway ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to delete hub/gateway ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to delete hub/gateway ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description	 :	This method is to rename the Hub and EPD and revert back to original name
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA78_EditHubAndDeviceName(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String newDeviceName;
		String hubName;
		String newHubName;
		boolean placeRenameSuccess =false;
		boolean placeRevertSuccess =false;
		boolean deviceRenameSuccess=false;
		boolean deviceRevertSuccess=false;

		deviceName = testData.get(0).split(",")[0];
		newDeviceName = testData.get(0).split(",")[1];
		hubName = testData.get(0).split(",")[2];
		newHubName = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				
				deviceManagementPageAndroid.selectPlace(hubName, udid)
				.enterPlaceName(newHubName)
				.clickSave();
				// rename hub name
				placeRenameSuccess = deviceManagementPageAndroid.verifyHubName(newHubName, udid);
				//revert hub name 
				deviceManagementPageAndroid.selectPlace(newHubName, udid)
				.enterPlaceName(hubName)
				.clickSave();
				placeRevertSuccess = deviceManagementPageAndroid.verifyHubName(hubName, udid);
				if(placeRenameSuccess && placeRevertSuccess) {
					extentTestAndroid.log(Status.PASS, "Place name changed sucessfully ");
				} else {
					extentTestAndroid.log(Status.FAIL, "Place name not changed sucessfully ");
				}
				// rename device name
				deviceManagementPageAndroid.selectPlace(hubName, udid)
				.selectEndPontDevice(deviceName, udid)
				.enterEndPointDeviceName(newDeviceName)
				.clickSave();
				deviceRenameSuccess = deviceManagementPageAndroid.verifyDevice(newDeviceName, udid);
				//revert device name
				deviceManagementPageAndroid.selectPlace(hubName, udid)
				.selectEndPontDevice(newDeviceName, udid)
				.enterEndPointDeviceName(deviceName)
				.clickSave();
				deviceRevertSuccess = deviceManagementPageAndroid.verifyDevice(deviceName, udid);
				
				if (deviceRenameSuccess && deviceRevertSuccess) {
					extentTestAndroid.log(Status.PASS, "Device name changed sucessfully ");
				} else {
					extentTestAndroid.log(Status.FAIL, "Device name not changed sucessfully ");
				}
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has premission to Edit place / device name ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to Edit place / device name ");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to modify the hub and device name ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to modify the hub and device name ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-78 :- "  + activeAccountName + " as " + activeUserRole + " is not available ");
		}
	}

	/**
	 * Description : This is an Android test method to rename the StrongHold GDO in Wifi-GDO
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA291_ValidateRenamingStrGDOInWGDO(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String hubName;
		String deviceName;
		String newDeviceName;
		boolean deviceRenameSuccess = false;
		boolean deviceRevertSuccess = false;

		hubName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		newDeviceName = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				// rename device name
				deviceManagementPageAndroid.selectPlace(hubName, udid)
				.selectEndPontDevice(deviceName, udid)
				.enterEndPointDeviceName(newDeviceName)
				.clickSave();
				deviceRenameSuccess = deviceManagementPageAndroid.verifyDevice(newDeviceName, udid);
				//revert device name
				deviceManagementPageAndroid.selectPlace(hubName, udid)
				.selectEndPontDevice(newDeviceName, udid)
				.enterEndPointDeviceName(deviceName)
				.clickSave();
				deviceRevertSuccess = deviceManagementPageAndroid.verifyDevice(deviceName, udid);

				if (deviceRenameSuccess && deviceRevertSuccess) {
					extentTestAndroid.log(Status.PASS, deviceName + " renamed to " +newDeviceName+" and reverted back successfully");
				} else {
					extentTestAndroid.log(Status.FAIL,  deviceName + " not renamed to " +newDeviceName);
					utilityAndroid.captureScreenshot(androidDriver);
				}

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has premission to edit end point device name");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has premission to edit end point device name");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to modify the hub and device name ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to modify the hub and device name ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available ");
		}
	}

	/**
	 * Description : This Android test method is to actuate light, validate state change and verify that event is recorded in history
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA76_ValidateLightStateChangeAndHistory(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String currentStatus;
		String expectedDeviceState;
		String eventText;
		String time;

		deviceName = testData.get(0).split(",")[0];
		expectedDeviceState = testData.get(0).split(",")[1];
		eventText = "just turned "+ expectedDeviceState;
		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Devices")) {
				if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
					currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
					if(!expectedDeviceState.equalsIgnoreCase(currentStatus)) {
						devicesPageAndroid.deviceActuation(deviceName);
					} else {
						devicesPageAndroid.deviceActuation(deviceName);
						devicesPageAndroid.deviceActuation(deviceName);
					}
					time = utilityAndroid.getCurrentTime();
					currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
					if(expectedDeviceState.equalsIgnoreCase(currentStatus)) {
						extentTestAndroid.log(Status.PASS, deviceName + " is now " + currentStatus );

						if(activeUserRole.equalsIgnoreCase("Guest")) {
							if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
								extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
							} else {
								extentTestAndroid.log(Status.FAIL, activeUserRole + " user has access to History");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							menuPageAndroid.clickMenuLink("History");
							if(historyPageAndroid.verifyHistory(time, deviceName, eventText)) {
								extentTestAndroid.log(Status.PASS, "History found for "+ deviceName );
							} else {
								extentTestAndroid.log(Status.FAIL, "History not  found for "+ deviceName + " for time " + time );
								utilityAndroid.captureScreenshot(androidDriver);
							}
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Status of " + deviceName + " is not changed. Status now is -> " + currentStatus );
						utilityAndroid.captureScreenshot(androidDriver);
					}
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.PASS,  activeUserRole + " user changed the state of Light");
					}	
				} else {
					if(activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, "Light is not visible to " + activeUserRole + " user");
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				}
			} else { 
				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to control devices");
				}
			} 
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This Android test method is to actuate GDO / CDO / Gate / VGDO, validate state change and verify that event is recorded in history 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA76_ValidateDeviceStateChangeAndHistory(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String currentStatus;
		String expectedDeviceState;
		String eventText;
		String time;

		deviceName = testData.get(0).split(",")[0];
		expectedDeviceState = testData.get(0).split(",")[1];
		eventText = " was "+ expectedDeviceState;
		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Devices")) {
				if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
					currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
					if(!currentStatus.toUpperCase().contains(expectedDeviceState.toUpperCase())) {
						devicesPageAndroid.deviceActuation(deviceName);
					} else {
						devicesPageAndroid.deviceActuation(deviceName);
						devicesPageAndroid.deviceActuation(deviceName);
					}
					time = utilityAndroid.getCurrentTime();
					currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);

					if(currentStatus.toUpperCase().contains(expectedDeviceState.toUpperCase())) {
						extentTestAndroid.log(Status.PASS, deviceName + " is now " + expectedDeviceState );

						if(activeUserRole.equalsIgnoreCase("Guest")) {
							if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
								extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
							}else {
								extentTestAndroid.log(Status.FAIL, activeUserRole + " user has access to History");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							menuPageAndroid.clickMenuLink("History");
							if(historyPageAndroid.verifyHistory(time, deviceName, eventText)) {
								extentTestAndroid.log(Status.PASS, "History found for "+ deviceName );
							} else {
								extentTestAndroid.log(Status.FAIL, "History not  found for "+ deviceName + " for time " + time );
								utilityAndroid.captureScreenshot(androidDriver);
							}
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Status of " + deviceName + " is not changed. Status now is -> " + currentStatus );
						utilityAndroid.captureScreenshot(androidDriver);
					}

					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user changed the state of device");
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else { 
				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to control devices");
				}
			}  
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the lock icon present on state change of the device
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA235_ValidateStrGDOLockIconWhenChangingState(ArrayList<String> testData, String deviceTest, String scenarioName) {
		String deviceName;
		String currentStatus;
		deviceName = testData.get(0).split(",")[0];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if(devicesPageAndroid.verifyShieldIcon(deviceName)){
					extentTestAndroid.log(Status.PASS,  deviceName+" is now " + currentStatus + " and shield icon is present");
				}
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if(devicesPageAndroid.verifyShieldIcon(deviceName)){
					extentTestAndroid.log(Status.PASS,  deviceName+" is now " + currentStatus + " and shield icon is present");
				}
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if(devicesPageAndroid.verifyShieldIcon(deviceName)){
					extentTestAndroid.log(Status.PASS,  deviceName + " is now " + currentStatus + " and shield icon is present");
				}
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if(devicesPageAndroid.verifyShieldIcon(deviceName)){
					extentTestAndroid.log(Status.PASS, deviceName+ " is now " + currentStatus + " and shield icon is present");
				}
				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")
						|| activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user changed the state of GDO");
				}
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the shield icon in Stronghold GDO in grid view and then change back the layout to list view
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA242_ValidateStrGDOLockIconInGridView(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String gatewayName;
		String deviceName;

		gatewayName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			//changing the layout to grid view [from list view]
			devicesPageAndroid.selectDeviceView()
			.selectGridView();
			//verify the shield icon [strong hold lock]
			if(devicesPageAndroid.verifyShieldInGridView(gatewayName , deviceName)) {
				extentTestAndroid.log(Status.PASS, "Shield icon is present for " + deviceName + " for " + gatewayName + " in grid view");
			} else {
				extentTestAndroid.log(Status.FAIL, "Shield icon is not present for " + deviceName + " for " + gatewayName + " in grid view");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			devicesPageAndroid.selectDeviceView();
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available");
		} 
	}	

	/**
	 * Description : This is an Android test method to verify the shield icon in Stronghold GDO in gallery view and then change back the layout to list view
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA352_ValidateStrGDOLockIconInGalleryView(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;

		deviceName = testData.get(0).split(",")[0];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			//changing the layout to gallery view [from list view]
			devicesPageAndroid.selectDeviceView();
			//verify shield icon in gallery view
			if(devicesPageAndroid.verifyShieldIconInGalleryView(deviceName, udid)) {
				extentTestAndroid.log(Status.PASS, "Shield icon is present for" + deviceName + " in gallery view" );
			} else {
				extentTestAndroid.log(Status.FAIL, "Shield icon is not present for" + deviceName + " in gallery view" );
				utilityAndroid.captureScreenshot(androidDriver);
			}
			//change the layout back to list view
			devicesPageAndroid.selectGridView()
			.selectDeviceView();
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available");
		} 
	}

	/**
	 * Description : This is an Android test method to check for the errors messages on Alert page. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA125_ValidateErrorsOnAlertCreation(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) { 
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				//create alert
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName)
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();
						if (alertPageAndroid.verifyAlertErrors()) {
							extentTestAndroid.log(Status.INFO, "Alert not saved. Error pop-up present");
						} else {
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, "Alert is created with name " + alertName );
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for Light is On  is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						}
					} else {
						extentTestAndroid.log(Status.FAIL, " Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||	activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add alerts");
				}
			} else { 
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||	activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		}else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to create an instant alert for a Light when it turns ON / OFF, verify push notification, clear push notification and delete alert.  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA93_CreateAndValidateInstantAlertForLight(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus;
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				//create alert
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("On")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							verificationText = "turned on";
						} else if(triggerState.equalsIgnoreCase("Off")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);	
							verificationText = "turned off";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, activeUserRole + " created Alert for Light is On created " + alertName );
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.equalsIgnoreCase(triggerState)) {
										devicesPageAndroid.deviceActuation(deviceName);
										devicesPageAndroid.deviceActuation(deviceName);
									} else {
										devicesPageAndroid.deviceActuation(deviceName);
									}
									//getting the status of device after actuation
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									extentTestAndroid.log(Status.INFO, " Current Status of device is - " + deviceStatus);
									//checking for notificaiton
									if (alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification received ");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification not received");
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								//delete the alert
								menuPageAndroid.clickMenuLink("Alerts");
								alertPageAndroid.deleteAlert(alertName, deviceName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + " with name " + alertName + "  is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert : " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||	activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||	activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add alerts");
				}	
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available ");
		}
	}

	/**
	 * Description : This is an Android test method to create an instant alert for a GDO / CDO / Gate / VGDO  when it is OPEN / CLOSE, verify push notification, clear push notification and delete alert.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA93_CreateAndValidateInstantAlertForDevice(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus;
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				//create alert
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("Open") || triggerState.equalsIgnoreCase("Opened")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							triggerState = "Open";
							verificationText = "just opened"; 
						} else if(triggerState.equalsIgnoreCase("Close") || triggerState.equalsIgnoreCase("Closed")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);
							triggerState = "Closed";
							verificationText = "just closed";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, activeUserRole + " created Alert for Garage Door is Open with name " + alertName );
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									//get the status of device
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.toUpperCase().contains(triggerState.toUpperCase())) {
										devicesPageAndroid.deviceActuation(deviceName);
										devicesPageAndroid.deviceActuation(deviceName);
									} else {
										devicesPageAndroid.deviceActuation(deviceName);
									}
									//getting the status of device after actuation
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									extentTestAndroid.log(Status.INFO, " Current Status of device is - " + deviceStatus);
									//checking for notificaiton 
									if (alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification received ");	
									} else {
										extentTestAndroid.log(Status.FAIL, " Alert Notification not received");	
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								//delete the alert
								menuPageAndroid.clickMenuLink("Alerts");
								alertPageAndroid.deleteAlert(alertName, deviceName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + " with name " + alertName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert : " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						utilityAndroid.captureScreenshot(androidDriver);
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||	activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||	activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to rename any alert. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA132_ValidateRenamingAlert(ArrayList<String> testData, String deviceTest , String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String alertRename;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		alertRename = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName)
						.setStateToggle(alertPageAndroid.alert_on_open_toggle, true)
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {
							//verify alert created
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, "Alert created witb name : "+alertName);
								alertPageAndroid.tapAlert(alertName, deviceName)
								.setAlertName(alertRename)
								.clickSave();
								if (alertPageAndroid.verifyAlertForDevice(alertRename, deviceName)) {
									extentTestAndroid.log(Status.PASS, alertName + " renamed to " + alertRename );
									alertPageAndroid.deleteAlert(alertRename, deviceName);
								} else {
									extentTestAndroid.log(Status.FAIL, alertName + " not renamed to " + alertRename );
									utilityAndroid.captureScreenshot(androidDriver);
									alertPageAndroid.deleteAlert(alertName, deviceName);
								} 
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert not created");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to  modify/edit alerts ");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to  modify/edit alerts ");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to modify/edit alerts ");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to modify/edit alerts ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	} 

	/**
	 * Description: This is an Android test method to create an alert on custom time and all days for Light when it turns ON / OFF,
	 *  verifies push notification not received as pass criteria, clears push notification if received and deletes alert at the end. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA91_CreateAndValidateAlertWithCustomTimeAndAllDaysLight(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String deviceStatus = null;
		String triggerState;
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()){
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("On")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							verificationText = "turned on";
						} else if(triggerState.equalsIgnoreCase("Off")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);	
							verificationText = "turned off";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.all_times_and_days_toggle, false)
						.selectFromTime()
						.selectToTime()
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {
							//verify alert created
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, "Alert for " + deviceName + "  with name : " + alertName + " is created ");
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									//get the status of device
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);

									if (deviceStatus.equalsIgnoreCase(triggerState)) {
										devicesPageAndroid.deviceActuation(deviceName);
										devicesPageAndroid.deviceActuation(deviceName);
									} else {
										devicesPageAndroid.deviceActuation(deviceName);
									}
									//getting the status of device after actuation
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									extentTestAndroid.log(Status.INFO, " Current Status of device is - " + deviceStatus);
									//verify notification received
									if (!alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification not received before the set time, as expected");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification received before the set time");	
										utilityAndroid.captureScreenshot(androidDriver);
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								//delete the alert
								menuPageAndroid.clickMenuLink("Alerts");
								alertPageAndroid.deleteAlert(alertName, deviceName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + "  with name : " + alertName + " is not created");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to create an alert on custom time and all days for GDO / CDO / Gate / VGDO when it is OPEN / CLOSE,
	 *  verifies push notification not received as pass criteria, clears push notification if received and deletes alert at the end.  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA91_CreateAndValidateAlertWithCustomTimeAndAllDaysForDevice(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus = null;
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("Open") || triggerState.equalsIgnoreCase("Opened")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							triggerState = "Open";
							verificationText = "just opened"; 
						} else if(triggerState.equalsIgnoreCase("Close") || triggerState.equalsIgnoreCase("Closed")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);
							triggerState = "Closed";
							verificationText = "just closed";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.all_times_and_days_toggle, false)
						.selectFromTime()
						.selectToTime()
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();
						if(!alertPageAndroid.verifyErrorDispalyed()) {
							//verify alert created
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, "Alert for " + deviceName + "  with name : " + alertName + " is created ");
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									//get the status of device
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.toUpperCase().contains(triggerState.toUpperCase())) {
										devicesPageAndroid.deviceActuation(deviceName);
										devicesPageAndroid.deviceActuation(deviceName);
									} else {
										devicesPageAndroid.deviceActuation(deviceName);
									}
									if (!alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification not received before the set time, as expected ");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification received before the set time");	
										utilityAndroid.captureScreenshot(androidDriver);
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								//delete the alert
								menuPageAndroid.clickMenuLink("Alerts");
								alertPageAndroid.deleteAlert(alertName, deviceName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + "  with name : " + alertName + " is not created");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to create an instant alert for all days but today for Light when turns ON,
	 * verifies that no push notification is received as PASS criteria, clears push notification if received and deletes alert at the end.  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA89_CreateAndValidateAlertWithSpecificDaysLight(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus = "null";
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("On")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							verificationText = "turned on";
						} else if(triggerState.equalsIgnoreCase("Off")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);
							verificationText = "turned off";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.all_times_and_days_toggle, false)
						.clickOnTheseDays()
						.deselectCurrentDay()
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {		
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, "Alert for " + deviceName + "  with name : " + alertName + " is created ");
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.equalsIgnoreCase(triggerState)) {
										devicesPageAndroid.deviceActuation(deviceName);
										devicesPageAndroid.deviceActuation(deviceName);
									} else {
										devicesPageAndroid.deviceActuation(deviceName);
									}
									if (!alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification not received for currrent day that was de-selected");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification received for currrent day that was de-selected");	
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								//delete the alert
								menuPageAndroid.clickMenuLink("Alerts");
								alertPageAndroid.deleteAlert(alertName, deviceName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + "  with name : " + alertName + " is not created");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to create an instant alert for all days but today for GDO when it is OPEN,
	 * verifies that no push notification is received as PASS criteria, clears push notification if received and deletes alert at the end.   
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA89_CreateAndValidateAlertWithSpecificDaysDevice(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus = "null";
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()){
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("Open") || triggerState.equalsIgnoreCase("Opened")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							triggerState = "Open";
							verificationText = "just opened"; 
						} else if(triggerState.equalsIgnoreCase("Close") || triggerState.equalsIgnoreCase("Closed")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);
							triggerState = "Closed";
							verificationText = "just closed";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.all_times_and_days_toggle, false)
						.clickOnTheseDays()
						.deselectCurrentDay()
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {
							if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, "Alert for " + deviceName + "  with name : " + alertName + " is created ");

								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.toUpperCase().contains(triggerState.toUpperCase())) {
										devicesPageAndroid.deviceActuation(deviceName);
										devicesPageAndroid.deviceActuation(deviceName);
									} else {
										devicesPageAndroid.deviceActuation(deviceName);
									}
									if (!alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification not received for currrent day that was de-selected");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification received for currrent day that was de-selected");	
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								//delete the alert
								menuPageAndroid.clickMenuLink("Alerts");
								alertPageAndroid.deleteAlert(alertName, deviceName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + "  with name : " + alertName + " is not created");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") ||activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to created an alert delayed by 2 minutes for Light when it turns ON / OFF, 
	 *  verifies push notification, clears push notification if received and deletes alert at the end.  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA97_CreateAndValidateDelayedAlertLight(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus = "null";
		String verificationText = null;
		int hours = 0;
		int minutes = 2;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				//create alert
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("On")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							verificationText = "remained on";
						} else if(triggerState.equalsIgnoreCase("Off")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);	
							verificationText = "remained off";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.as_soon_as_it_happens_toggle, false)
						.setDurationForLongerThan(hours , minutes)
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {	
							menuPageAndroid.clickMenuLink("Devices");
							if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
								//get the status of device
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								if (deviceStatus.equalsIgnoreCase(triggerState)) {
									devicesPageAndroid.deviceActuation(deviceName);
									devicesPageAndroid.deviceActuation(deviceName);
								} else {
									devicesPageAndroid.deviceActuation(deviceName);
								}
								//getting the status of device after actuation
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								extentTestAndroid.log(Status.INFO, " Current Status of device is - " + deviceStatus);
								//checking for notificaiton
								if (alertPageAndroid.checkPushNotificationReceived(deviceName , verificationText, udid)) {
									extentTestAndroid.log(Status.FAIL, "Alert Notification received before time");	
								} else {
									devicesPageAndroid.validateDeviceTimer(deviceName, minutes);
									alertPageAndroid.waitTime();
									if (alertPageAndroid.checkPushNotificationReceived(deviceName , verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification received as expected");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification not received");	
									}
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Expected device is not present");
								utilityAndroid.captureScreenshot(androidDriver);
							}
							//delete the alert
							menuPageAndroid.clickMenuLink("Alerts");
							alertPageAndroid.deleteAlert(alertName, deviceName);
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to created an alert delayed by 2 minutes for GDO when it is OPEN, 
	 *  verifies push notification, clears push notification if received and deletes alert at the end. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA97_CreateAndValidateDelayedAlertDevice(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus = "null";
		String verificationText = null;
		int hours = 0;
		int minutes = 2;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				//create alert
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName);
						if(triggerState.equalsIgnoreCase("Open") || triggerState.equalsIgnoreCase("Opened")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
							triggerState = "Open";
							verificationText = "remained open";
						} else if(triggerState.equalsIgnoreCase("Close") || triggerState.equalsIgnoreCase("Closed")) {
							alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);
							triggerState = "Closed";
							verificationText = "remained closed";
						}
						alertPageAndroid.setStateToggle(alertPageAndroid.as_soon_as_it_happens_toggle, false)
						.setDurationForLongerThan(hours , minutes)
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						if(!alertPageAndroid.verifyErrorDispalyed()) {
							menuPageAndroid.clickMenuLink("Devices");
							if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
								//get the status of device
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								if (deviceStatus.toUpperCase().contains(triggerState.toUpperCase())) {
									devicesPageAndroid.deviceActuation(deviceName);
									devicesPageAndroid.deviceActuation(deviceName);
								} else {
									devicesPageAndroid.deviceActuation(deviceName);
								}
								//getting the status of device after actuation
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								extentTestAndroid.log(Status.INFO, " Current Status of device is - " + deviceStatus);
								//checking for notificaiton
								if (alertPageAndroid.checkPushNotificationReceived(deviceName , verificationText, udid)) {
									extentTestAndroid.log(Status.FAIL, "Alert Notification received before time");	
								} else {
									devicesPageAndroid.validateDeviceTimer(deviceName, minutes);
									alertPageAndroid.waitTime();
									if (alertPageAndroid.checkPushNotificationReceived(deviceName , verificationText, udid)) {
										extentTestAndroid.log(Status.PASS, "Alert Notification received as expected");	
									} else {
										extentTestAndroid.log(Status.FAIL, "Alert Notification not received");	
									}
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Expected device is not present");
								utilityAndroid.captureScreenshot(androidDriver);
							}
							//delete the alert
							menuPageAndroid.clickMenuLink("Alerts");
							alertPageAndroid.deleteAlert(alertName, deviceName);
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							alertPageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation()
							.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method creates the alert, disables it, actuates device 
	 * 	and verifies that no push notification is received as a PASS criteria. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */	
	public void MTA130_ValidateDisablingAlert(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String deviceStatus;
		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName)
						.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true)
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						//verify alert created
						if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
							extentTestAndroid.log(Status.PASS, "Alert created with name " + alertName);
							//verify if alert is disable or not
							if (alertPageAndroid.disableAlert(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, alertName + " is disabled ");
								menuPageAndroid.clickMenuLink("Devices");
								//get the status of device
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								if (deviceStatus.equalsIgnoreCase("ON")) {
									devicesPageAndroid.deviceActuation(deviceName);
									devicesPageAndroid.deviceActuation(deviceName);
								} else {
									devicesPageAndroid.deviceActuation(deviceName);
								}
								if (alertPageAndroid.checkPushNotificationReceived(deviceName, "turned off", udid)) {
									extentTestAndroid.log(Status.FAIL, "Alert Notification received for disabled alert ");	
								} else {
									extentTestAndroid.log(Status.PASS, "Alert Notification not received, as expected ");	
								}
							} else {
								extentTestAndroid.log(Status.FAIL,  alertName + " is not disabled ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
							//delete the alert
							menuPageAndroid.clickMenuLink("Alerts");
							alertPageAndroid.deleteAlert(alertName, deviceName);
						} else {
							extentTestAndroid.log(Status.FAIL, " Alert not created");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to modify / edit alert ");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to modify / edit alert ");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to modify / edit alert ");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to modify / edit alert ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method creates the alert, deletes it. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA128_ValidateDeletingAlert(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if(!alertPageAndroid.verifyErrorDispalyed()) {
					if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
						alertPageAndroid.setAlertName(alertName)
						.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true)
						.setStateToggle(alertPageAndroid.push_notification_toggle, true)
						.clickSave();

						//verify alert created
						if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
							extentTestAndroid.log(Status.PASS, "lert created with name " + alertName);
							if (alertPageAndroid.deleteAlert(alertName, deviceName)) {
								extentTestAndroid.log(Status.PASS, alertName + " Deleted ");
							} else {
								extentTestAndroid.log(Status.FAIL, alertName + " is not deleted ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Alert not created");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, alertPageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to delete alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to delete alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to delete alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to delete alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	} 

	/**
	 * Description : This is an Android test method to create instant alert for today for Light in Wifi-hub when it turns ON,
	 * verify push notification, clear push notification and delete alert.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA367_CreateAndValidateInstantCurrentDayAlertForLight(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String alertName;
		String triggerState;
		String deviceStatus = null;
		String verificationText = null;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Alerts")) {
				alertPageAndroid.addAlert();
				if (alertPageAndroid.verifyAndSelectDevice(deviceName, udid)) {
					alertPageAndroid.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("On")) {
						alertPageAndroid.setStateToggle(alertPageAndroid.alert_on_open_toggle, true);
						verificationText = "turned on";
					} else if(triggerState.equalsIgnoreCase("Off")) {
						alertPageAndroid.setStateToggle(alertPageAndroid.alert_off_closed_toggle, true);	
						verificationText = "turned off";
					}
					alertPageAndroid.setStateToggle(alertPageAndroid.all_times_and_days_toggle, false)
					.clickOnTheseDays()
					.selectCurrentDay()
					.setStateToggle(alertPageAndroid.push_notification_toggle, true)
					.clickSave();

					if(!alertPageAndroid.verifyErrorDispalyed()) {
						//verify alert created
						if (alertPageAndroid.verifyAlertForDevice(alertName, deviceName)) {
							extentTestAndroid.log(Status.PASS, "Alert for " + deviceName + "  with name : " + alertName + " is created ");
							menuPageAndroid.clickMenuLink("Devices");
							if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
								//get the status of device
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								if (deviceStatus.equalsIgnoreCase(triggerState)) {
									devicesPageAndroid.deviceActuation(deviceName);
									devicesPageAndroid.deviceActuation(deviceName);
								} else {
									devicesPageAndroid.deviceActuation(deviceName);
								}
								//get the status of the device after actuation 
								deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
								extentTestAndroid.log(Status.INFO, "Current status of "+ deviceName + " is " + deviceStatus);
								//verify notification
								if (alertPageAndroid.checkPushNotificationReceived(deviceName, verificationText, udid)) {
									extentTestAndroid.log(Status.PASS, "Alert Notification  received for today ");	
								} else {
									extentTestAndroid.log(Status.FAIL, "Alert Notification not received ");	
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Expected device is not present");
								utilityAndroid.captureScreenshot(androidDriver);
							}
							//delete the alert
							menuPageAndroid.clickMenuLink("Alerts");
							alertPageAndroid.deleteAlert(alertName, deviceName);
						} else {
							extentTestAndroid.log(Status.FAIL, "Alert for " + deviceName + "  with name : " + alertName + " is not created");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Error while saving the alert: " + alertPageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						alertPageAndroid.clickOkButton()
						.clickBackButton()
						.clickBackConfirmation()
						.clickBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
					utilityAndroid.captureScreenshot(androidDriver);
					alertPageAndroid.clickBackButton();
				}

				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add alerts");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add alerts");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add alerts");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method that creates a schedule for EPD to CLOSE/ Turn On / Turn Off after 2 minutes of current system time,
	 *  verifies device state change, verifies push notification, clears push notification if received, verifies record in history and deletes the schedule. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA34_CreateAndValidateScheduleAndHistoryForDevice(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;
		String deviceStatus = "null";
		String timeSelected = null;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		state = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//verify if yellow bar is present for device
				if (!devicesPageAndroid.verifyYellowBarPresent(deviceName, udid)) {
					if(state.equalsIgnoreCase("Close")) {
						state = "Closed";
					}
					//on device page ,check the status of the device. If device is in expected state, tap on device to change the state
					if((devicesPageAndroid.getDeviceStatus(deviceName).toUpperCase().contains(state.toUpperCase()))) {
						extentTestAndroid.log(Status.INFO, deviceName + " is currently in " + devicesPageAndroid.getDeviceStatus(deviceName) + " state, similar for schedule to trigger. Hence actuating to change state.");
						devicesPageAndroid.tapOnDevice(deviceName);
					} else {
						extentTestAndroid.log(Status.INFO, deviceName + " is currently in " + devicesPageAndroid.getDeviceStatus(deviceName) + " state, and  schedule to trigger is for " +state +".");
					}
					if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
						schedulePageAndroid.addSchedule();
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
							if(!schedulePageAndroid.verifyErrorDispalyed()) {
								if (schedulePageAndroid.verifySchedule(scheduleName)) {
									extentTestAndroid.log(Status.PASS,  scheduleName + " is created ");
									//navigate to devices page
									menuPageAndroid.clickMenuLink("Devices");
									if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
										//wait for duration which is set in schedule
										schedulePageAndroid.waitForNotification(timeSelected);
										//check for notification 
										if (schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
											extentTestAndroid.log(Status.PASS, "Schedule Notification received ");	
										} else {
											extentTestAndroid.log(Status.FAIL,  scheduleName + " Schedule Notification not received ");	
										}
										deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
										if (deviceStatus.toUpperCase().contains(state.toUpperCase())) {
											extentTestAndroid.log(Status.PASS,  deviceName + " status is - "+ deviceStatus);	
										} else {
											extentTestAndroid.log(Status.FAIL, "Status not changed. For " + deviceName+ " status now is - "+ deviceStatus );
											utilityAndroid.captureScreenshot(androidDriver);
										}
										//verify history
										if(activeUserRole.equalsIgnoreCase("Guest")) {
											if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
												extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have access to History");
											}else {
												extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has access to History");
												utilityAndroid.captureScreenshot(androidDriver);
											}
										} else {
											menuPageAndroid.clickMenuLink("History");
											if(historyPageAndroid.verifyHistory(timeSelected, scheduleName, " was triggered")) {
												extentTestAndroid.log(Status.PASS, "History found for "+ scheduleName );
											} else {
												extentTestAndroid.log(Status.FAIL, " History not  found for "+ scheduleName + " for time " + timeSelected );
												utilityAndroid.captureScreenshot(androidDriver);
											}
										}
									} else {
										extentTestAndroid.log(Status.FAIL, "Expected device is not present");
										utilityAndroid.captureScreenshot(androidDriver);
									}
									menuPageAndroid.clickMenuLink("Schedules");
									schedulePageAndroid.deleteSchedule(scheduleName);
								} else {
									extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
								utilityAndroid.captureScreenshot(androidDriver);
								schedulePageAndroid.clickOkButton()
								.clickBackButton()
								.clickBackConfirmation();
							}
						} else {
							extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickLaterButton();
						}
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add Schedule");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
						}
					}
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected device is not present");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to creates a schedule for EPD to CLOSE/ Turn On / Turn Off after 2 minutes of current system time. This method would
	 * set the device to the expected state , creates create schedule and verify no notification received as PASS criteria. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA487_CreateAndValidateScheduleNotTriggered(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;
		String timeSelected = null;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		state = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		if (roleSelected) {
			//on device page ,check the status of the device. If device is in expected state, tap on device to change the state
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//verify if yellow bar is present
				if (!devicesPageAndroid.verifyYellowBarPresent(deviceName, udid)) {
					if(state.equalsIgnoreCase("Close")) {
						state = "Closed";
					}
					// verify the status of the device
					if(!(devicesPageAndroid.getDeviceStatus(deviceName).toUpperCase().contains(state.toUpperCase()))) {
						extentTestAndroid.log(Status.INFO, deviceName + " is currently in " + devicesPageAndroid.getDeviceStatus(deviceName) + " state. Schdeule to trigger is for " +state+ " state. Hence actuating to change state to verify notification not being received.");
						devicesPageAndroid.tapOnDevice(deviceName);
					} else {
						extentTestAndroid.log(Status.INFO, deviceName + " is currently in " + state + " state, similar for schedule to trigger.");
					}
					if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
						schedulePageAndroid.addSchedule();
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 1, "All Days", true, udid);
							if(!schedulePageAndroid.verifyErrorDispalyed()) {
								if (schedulePageAndroid.verifySchedule(scheduleName)) {
									extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
									//wait for duration which is set in schedule
									schedulePageAndroid.waitForNotification(timeSelected);
									//check for notification 
									if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
										extentTestAndroid.log(Status.PASS, "As the device was in closed state, " + scheduleName+ " did not trigger");	
									} else {
										extentTestAndroid.log(Status.FAIL, scheduleName+ " was not expected to get triggered , as device was in closed state");	
									}
									schedulePageAndroid.deleteSchedule(scheduleName);
								} else {
									extentTestAndroid.log(Status.FAIL,  scheduleName + " is not created ");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
								utilityAndroid.captureScreenshot(androidDriver);
								schedulePageAndroid.clickOkButton()
								.clickBackButton()
								.clickBackConfirmation();
							}
						} else {
							extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickLaterButton();
						}
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add Schedule");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
						}
					}
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected device is not present");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to creates a schedule for EPD to CLOSE/ Turn On / Turn Off on all days but today, after 2 minutes of current system time,
	 *  verifies that no push notification is received as a PASS criteria, clears push notification if received and deletes the schedule. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA37_CreateAndValidateScheduleSpecificDayForDevice(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;
		String timeSelected = null;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		state = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName , state);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//verify if yellow bar is present
				if (!devicesPageAndroid.verifyYellowBarPresent(deviceName, udid)) {
					if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
						schedulePageAndroid.addSchedule();
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							timeSelected =  schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "Today", true, udid);
							if(!schedulePageAndroid.verifyErrorDispalyed()) {
								if (schedulePageAndroid.verifySchedule(scheduleName)) {
									extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
									//wait for duration which is set in schedule
									schedulePageAndroid.waitForNotification(timeSelected);
									//check for notification 
									if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
										extentTestAndroid.log(Status.PASS, "Schedule Notification not received, as expected ");	
									} else {
										extentTestAndroid.log(Status.FAIL, scheduleName + " Schedule Notification received but shouldn't received ");	
									}
									schedulePageAndroid.deleteSchedule(scheduleName);
								} else {
									extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
								utilityAndroid.captureScreenshot(androidDriver);
								schedulePageAndroid.clickOkButton()
								.clickBackButton()
								.clickBackConfirmation();
							}
						} else {
							extentTestAndroid.log(Status.FAIL,  schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickLaterButton();
						}
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add Schedule");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add Schedule");
						}
					}
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected device is not present");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the error message shown to user while creating schedule for VGDO in Wifi-hub
	 * @param testData
	 * @param deviceTest 
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA398_CheckNoScheduleVGDOInWifiHub(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;
		String errText;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		state = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
				//create schedule
				schedulePageAndroid.addSchedule();
				schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
				//verify if error message is displayed or  not
				if(schedulePageAndroid.verifyErrorDispalyed()) {
					errText =schedulePageAndroid.getErrorMsg();
					//compare the error message and error code
					//	if(errText.equalsIgnoreCase("For your security, schedules to close a garage has been disabled until further notice. We apologize for the inconvenience. (707)")) {
					if(errText.equalsIgnoreCase("For your security, schedules to close a garage have been disabled until further notice. We apologize for the inconvenience. (707)")) {
						extentTestAndroid.log(Status.PASS, errText);
					} else {
						extentTestAndroid.log(Status.FAIL, "Error message recevied is not expected : " + errText);	
						utilityAndroid.captureScreenshot(androidDriver);
					}
					schedulePageAndroid.clickOkButton()
					.clickBackButton()
					.clickBackConfirmation();
				} else {
					extentTestAndroid.log(Status.FAIL, " No error received for creating schedule in detonator ");	
					utilityAndroid.captureScreenshot(androidDriver);
					schedulePageAndroid.clickBackButton()
					.clickBackConfirmation();
				}

				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to rename a schedule.  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA46_ValidateRenamingSchedule(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;
		String scheduleRename;

		deviceName = testData.get(0).split(",")[0];
		state = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];
		scheduleRename = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
				schedulePageAndroid.addSchedule();
				if(!schedulePageAndroid.verifyErrorDispalyed()) {
					schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", false, udid);
					if (schedulePageAndroid.verifySchedule(scheduleName)) {
						extentTestAndroid.log(Status.PASS, "Schedule created "+scheduleName);
						schedulePageAndroid.tapOnSchedule(scheduleName)
						.clearScheduleName()
						.setScheduleName(scheduleRename)
						.clickSaveButton();
						if (schedulePageAndroid.verifySchedule(scheduleRename)) {
							extentTestAndroid.log(Status.PASS, scheduleName + " is renamed to " + scheduleRename);
							schedulePageAndroid.deleteSchedule(scheduleRename);
						} else {
							extentTestAndroid.log(Status.FAIL, scheduleName + " is not renamed to " + scheduleRename);
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.deleteSchedule(scheduleName);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Schedule not created");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  schedulePageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					schedulePageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to modify/edit Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to modify/edit Schedule");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to modify/edit Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to modify/edit Schedule");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 *Description: This is an Android test method to creates a schedule and deletes it. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA54_ValidateDeletingSchedule(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;

		deviceName = testData.get(0).split(",")[0];
		state = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
				schedulePageAndroid.addSchedule();
				if(!schedulePageAndroid.verifyErrorDispalyed()) {
					schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", false, udid);

					schedulePageAndroid.deleteSchedule(scheduleName);
					if (!schedulePageAndroid.verifySchedule(scheduleName)) {
						extentTestAndroid.log(Status.PASS, scheduleName + " is deleted ");
					} else {
						extentTestAndroid.log(Status.FAIL,  scheduleName + " is not deleted ");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL,  schedulePageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					schedulePageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to delete Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to delete Schedule");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to delete Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to delete Schedule");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to create schedule and sets it to the Disabled state.
	 * Once the  schedule is set to disabled, it verifies that no push notification is received as PASS criteria and deletes the schedule. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA50_ValidateDisbalingSchedule(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;
		String sechduleExpectedState = "Disabled";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		state = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
				schedulePageAndroid.addSchedule();
				if(!schedulePageAndroid.verifyErrorDispalyed()) {
					timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);

					if (schedulePageAndroid.verifySchedule(scheduleName)) {
						extentTestAndroid.log(Status.PASS, "Schedule created "+scheduleName);
						if (schedulePageAndroid.disableSchedule(scheduleName, sechduleExpectedState)) {
							extentTestAndroid.log(Status.PASS,  scheduleName + " is " + sechduleExpectedState );
							schedulePageAndroid.waitForNotification(timeSelected);
							if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
								extentTestAndroid.log(Status.PASS, "No notification received for disabled schedule ");
							} else {
								extentTestAndroid.log(Status.FAIL, "Notification received for disabled schedule ");
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Schedule not created");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL,  scheduleName + " is not " + sechduleExpectedState );
						utilityAndroid.captureScreenshot(androidDriver);
					}
					schedulePageAndroid.deleteSchedule(scheduleName);
				} else {
					extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					schedulePageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to modify/edit Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to modify/edit Schedule");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to modify/edit Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to modify/edit Schedule");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to verify different error messages shown to user in case an user makes a mistake while creating a schedule 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA41_ValidateErrorOnScheduleCreation(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String state;
		String scheduleName;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		state = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
				schedulePageAndroid.addSchedule();
				if(!schedulePageAndroid.verifyErrorDispalyed()) {
					schedulePageAndroid.setScheduleName("")
					.clickSaveButton();
					if (schedulePageAndroid.verifyError()) {
						extentTestAndroid.log(Status.INFO, "There is an error pop-up present ");
					} else {
						extentTestAndroid.log(Status.INFO, "Error heading / text is different than expected ");
					}
					schedulePageAndroid.setScheduleName(scheduleName)
					.clickSaveButton();
					if (schedulePageAndroid.verifyError()) {
						extentTestAndroid.log(Status.INFO, "There is an error pop -up present ");
					} else {
						extentTestAndroid.log(Status.INFO, "Error heading / text is different than expected ");
					}
					schedulePageAndroid.clickOnSetDevicesIcon()
					.selectDeviceAndState(deviceName, state, udid)
					.clickBackButton()
					.clickOnDays()
					.selectDeselectWeek(false)
					.clickDaysDoneButton()
					.clickSaveButton();
					if (schedulePageAndroid.verifyError()) {
						extentTestAndroid.log(Status.INFO, " There is an error pop -up present ");
					} else {
						extentTestAndroid.log(Status.INFO, "Error heading / text is different than expected ");
					}
					schedulePageAndroid.clickOnDays()
					.selectDeselectWeek(true)
					.clickDaysDoneButton()
					.clickSaveButton();

					schedulePageAndroid.deleteSchedule(scheduleName);
				} else {
					extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
					utilityAndroid.captureScreenshot(androidDriver);
					schedulePageAndroid.clickLaterButton();
				}
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add Schedule");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}	
	}

	/**
	 * Description : This is an Android test method to creates a schedule for device after 2 minutes of current system time. 
	 * Modify the schedule to remove's the added device from the schedule and adds a new device ,set new time after 2 minutes the current system time,
	 * verifies that Schedule is triggered and the state of the Device is as expected
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA164_ValidateRemoveDeviceFromSchedule(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName1;
		String state1;
		String deviceName2;
		String state2;
		String scheduleName;
		String deviceStatus = "null";
		int minutes = 2;
		String timeSelected = null;

		scheduleName = testData.get(0).split(",")[0];
		deviceName1 = testData.get(0).split(",")[1];
		state1 = testData.get(0).split(",")[2];
		deviceName2 = testData.get(0).split(",")[3];
		state2 = testData.get(0).split(",")[4];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName1, state1);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyDevicePresent(deviceName2, udid)) {
				//verify if yellow bar is present for device
				if (!devicesPageAndroid.verifyYellowBarPresent(deviceName2, udid)) {
					if(state2.equalsIgnoreCase("Close")) {
						state2 = "Closed";
					}
					//on device page ,check the status of the device. If device is in expected state, tap on device to change the state
					if((devicesPageAndroid.getDeviceStatus(deviceName2).toUpperCase().contains(state2.toUpperCase()))) {
						devicesPageAndroid.tapOnDevice(deviceName2);
					} else {
						extentTestAndroid.log(Status.INFO, "Device is not in same state as schedule to trigger");
					}
					if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) { 
						schedulePageAndroid.addSchedule();
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, minutes, "All Days", true, udid);
							if(!schedulePageAndroid.verifyErrorDispalyed()) {
								if (schedulePageAndroid.verifySchedule(scheduleName)) {
									extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
									schedulePageAndroid.tapOnSchedule(scheduleName)
									.clickOnSetDevicesIcon()
									.removeAllSelectedDevices()
									.clickOnSetDevicesIcon()
									.selectDeviceAndState(deviceName2, state2, udid)
									.clickBackButton()
									.clickOnTime();
									timeSelected = schedulePageAndroid.setTime(minutes);
									schedulePageAndroid.clickSaveButton();

									menuPageAndroid.clickMenuLink("Devices");				
									if(devicesPageAndroid.verifyDevicePresent(deviceName2, udid)) {
										//wait for duration which is set in schedule
										schedulePageAndroid.waitForNotification(timeSelected);
										//check for notification 
										if (schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
											deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName2);
											if (deviceStatus.toUpperCase().contains(state2.toUpperCase())) {
												extentTestAndroid.log(Status.PASS, deviceName2 + " status is - "+ deviceStatus);	
											} else {
												extentTestAndroid.log(Status.FAIL, deviceName2+ " status now is - "+ deviceStatus );
												utilityAndroid.captureScreenshot(androidDriver);
											}
										} else {
											extentTestAndroid.log(Status.FAIL, scheduleName + " Schedule Notification not received ");	
										}
									} else {
										extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
										utilityAndroid.captureScreenshot(androidDriver);
									}
									menuPageAndroid.clickMenuLink("Schedules");
									schedulePageAndroid.deleteSchedule(scheduleName);
								} else {
									extentTestAndroid.log(Status.FAIL, "For " + scheduleName + " is not created ");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
								utilityAndroid.captureScreenshot(androidDriver);
								schedulePageAndroid.clickOkButton()
								.clickBackButton()
								.clickBackConfirmation();
							}
						} else {
							extentTestAndroid.log(Status.FAIL,  schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickLaterButton();
						}
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to create / modify Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to create / modify Schedule");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to create / modify Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to create / modify Schedule");
						}
					}
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName2 + " is in error state.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected device is not present");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description: This is an Android test method to send an invite with Admin / Family / Guest privileges to the specified email id. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA58_ValidateAddingUser(ArrayList<String> testData, String deviceTest , String scenarioName) throws Exception {
		String invitation_Email;
		String userRole;

		invitation_Email = testData.get(0).split(",")[0];
		userRole = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (roleSelected) {
				if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
					manageUsersPageAndroid.clickOnAddUserbtn();
					if (!manageUsersPageAndroid.verifyErrorPopUp()) {
						manageUsersPageAndroid.enterUserEmail(invitation_Email) // Enter invitation email
						.enterConfirmUserEmail(invitation_Email)
						.clickOnUserDropdown()
						.selectUserRole(userRole)
						.clickOnSendButton();
						if (!manageUsersPageAndroid.verifyErrorPopUp()) {
							manageUsersPageAndroid.clickOnDoneButton();
							if (!manageUsersPageAndroid.verifyNoUserInvitedText() && manageUsersPageAndroid.verifyInvitedUsersEmail(invitation_Email)) { 
								extentTestAndroid.log(Status.PASS, "Invitation sent to :- " + invitation_Email + " successfully as :- " + userRole);
								if(userRole.equalsIgnoreCase("Admin")) {
									adminInvitationSent = true;
								} else if (userRole.equalsIgnoreCase("Family")) {
									familyInvitationSent = true;
								} else if(userRole.equalsIgnoreCase("Guest")) {
									guestInvitationSent = true;
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Invited users email ID :- " + invitation_Email + " for Role :- " + userRole + ", not found in the Invitation Users list");
								utilityAndroid.captureScreenshot(androidDriver);
							} 
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + invitation_Email);
							utilityAndroid.captureScreenshot(androidDriver);
							manageUsersPageAndroid.clickOnOkButton()
							.clickOnBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + invitation_Email);
						utilityAndroid.captureScreenshot(androidDriver);
						manageUsersPageAndroid.clickOnOkButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add a user");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to add a user");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add a user");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add a user");
					}
				}
			} else {
				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**
	 * Description: This is an Android test method to accept or decline an invitation with Admin / Family / Guest privileges to creator�s account. 
	 * The test method logs out of creator�s account, logs in to the invitation recipient�s account and accepts/decline the invitation. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA69_ValidateUserInvitationStatus(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String acceptOrDecline;
		String secondaryEmailId;
		String secondaryPassword;
		String userRole;
		String inviteeAccountName;

		secondaryEmailId = testData.get(0).split(",")[0];
		secondaryPassword = testData.get(0).split(",")[1];
		acceptOrDecline = testData.get(0).split(",")[2];
		inviteeAccountName = testData.get(0).split(",")[3];
		userRole = testData.get(0).split(",")[4];

		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if(adminInvitationSent || familyInvitationSent || guestInvitationSent) {
				menuPageAndroid.tappingAccountView();
				loginPageAndroid.clickLogout(); // logout from primary user and login as secondary user which has to take action
				if(loginPageAndroid.verifyEnvTextPresent()) {
					loginPageAndroid.selectFeature(userType);
				}
				loginPageAndroid.login(secondaryEmailId, secondaryPassword); 

				menuPageAndroid.tappingAccountView();// Tap on Account view
				if (usersAccountPageAndroid.verifyAccountPage(secondaryEmailId)) {
					usersAccountPageAndroid.clickOnAccountInvitationLink();
					if (!usersAccountPageAndroid.verifyNoInvitationText()) {
						if (usersAccountPageAndroid.verifyPendingInvitations(inviteeAccountName, userRole)) {
							usersAccountPageAndroid.clickOnPendingInvitations(inviteeAccountName, userRole)
							.clickOnAcceptOrDecline(acceptOrDecline);

							if (acceptOrDecline.equalsIgnoreCase("Accept") && devicesPageAndroid.verifyBlueBarPresent()) {
								extentTestAndroid.log(Status.PASS, "Invitation has been accepted successfully by " + inviteeAccountName);
								if (devicesPageAndroid.verifyAndSelectUserRole(userRole, inviteeAccountName)) {
									if(userRole.equalsIgnoreCase("Admin")) {
										userAsAdminAccepted = true;
									} else if (userRole.equalsIgnoreCase("Family")) {
										userAsFamilyAccepted = true;
									} else if(userRole.equalsIgnoreCase("Guest")) {
										userAsGuestAccepted = true;
									}
									extentTestAndroid.log(Status.PASS, "Successfully switched to " + inviteeAccountName + " as " + userRole);
								} else {
									extentTestAndroid.log(Status.FAIL, "The Accepted invite is not shown in this account");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else if (acceptOrDecline.equalsIgnoreCase("Decline")) {
								extentTestAndroid.log(Status.INFO, "The Invitation was declined , hence cannot switch the user");
							}
						}
						else {
							extentTestAndroid.log(Status.FAIL, "Expected invitation not found");
							utilityAndroid.captureScreenshot(androidDriver);
							usersAccountPageAndroid.clickOnBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "You have no account invitations");
						utilityAndroid.captureScreenshot(androidDriver);
						usersAccountPageAndroid.clickOnBackButton();
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "user profile is of not expected user");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				extentTestAndroid.log(Status.INFO, " Invitation as " +  userRole + " user was not sent ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}

	}

	/**
	 * Description : This is an Android test method to verify if Shield icon on GDO for Admin / Family / Guest User. This method would trigger the API(V2) call
	 * to attach the lock and verify, if the lock icon is visible.Then Detach the lock using API call and verify lock icon shouldn't be visible 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA248_ValidateShieldIconLockForInvitedUser(ArrayList<String> testData, String deviceTest, String scenarioName)throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException  {
		String userAccountName;
		String gdoSerialNumber;
		String gdoName;
		String userRole;

		gdoSerialNumber = testData.get(0).split(",")[0];
		gdoName = testData.get(0).split(",")[1];
		userAccountName = testData.get(0).split(",")[2];
		userRole = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if(multiUser){
			if (roleSelected) {
				if((adminInvitationSent && userAsAdminAccepted) || (familyInvitationSent && userAsFamilyAccepted) || (guestInvitationSent && userAsGuestAccepted) ) {
					if(devicesPageAndroid.verifyAndSelectUserRole(userRole,userAccountName)) {
						if (menuPageAndroid.verifyAndClickMenuLink("Devices")) {
							//attach lock to StrongHold GDO
							webService.attachLockGDO(gdoSerialNumber);
							menuPageAndroid.clickMenuLink("Devices");
							//verify shield icon present or not
							if(devicesPageAndroid.verifyShieldIcon(gdoName)) {
								extentTestAndroid.log(Status.PASS, "Lock is present for " + gdoName);
							} else {
								extentTestAndroid.log(Status.FAIL, "Lock is not present for " + gdoName ); 
								utilityAndroid.captureScreenshot(androidDriver);
							}
							//detach lock from StrongHold GDO
							webService.detachLockGDO(gdoSerialNumber);
							menuPageAndroid.clickMenuLink("Devices");
							//verify shield icon present or not
							if(!devicesPageAndroid.verifyShieldIcon(gdoName)) {
								extentTestAndroid.log(Status.PASS, "Lock removed from " + gdoName );
							} else {
								extentTestAndroid.log(Status.FAIL, "Lock is not removed from " + gdoName );
								utilityAndroid.captureScreenshot(androidDriver);
							}
							if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
								extentTestAndroid.log(Status.PASS, activeUserRole + " user is able to view shield icon");
							}
						} else {
							if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
								extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to control devices");
							}
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "User not selected as " +userRole);
					}
				} else {
					extentTestAndroid.log(Status.INFO, "User was not invited " +userRole);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**
	 * Description	: This is an Android test method that logs in to the creator account change the existing user role as specified in test data.
	 * logs in to the invitation recipient�s account, verifies the changed role and logs out.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA66_ValidateChangeRole(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception{
		String invitedUsersName;
		String existingUserRole;
		String newUerRole;
		String secondaryEmail;
		String secondaryPassword;
		String primaryUserName;

		invitedUsersName = testData.get(0).split(",")[0];
		existingUserRole = testData.get(0).split(",")[1];
		newUerRole = testData.get(0).split(",")[2];
		secondaryEmail = testData.get(0).split(",")[3];
		secondaryPassword = testData.get(0).split(",")[4];
		primaryUserName = testData.get(0).split(",")[5];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (roleSelected) {
				if ((adminInvitationSent && userAsAdminAccepted) || (familyInvitationSent && userAsFamilyAccepted) || (guestInvitationSent && userAsGuestAccepted)) {
					menuPageAndroid.tappingAccountView();
					loginPageAndroid.clickLogout() // logout from primary user and login as secondary user which has to take action
					.selectFeature(userType)
					.login(email, password);

					devicesPageAndroid.verifyAndSelectUserRole(activeUserRole, activeAccountName);

					if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
						if (!manageUsersPageAndroid.verifyNoUserInvitedText()) {
							if (manageUsersPageAndroid.verifyUserPresent(invitedUsersName, newUerRole)) {
								extentTestAndroid.log(Status.FAIL, invitedUsersName + " is already added as " + newUerRole);
							} else {
								manageUsersPageAndroid.clickOnUser(invitedUsersName,existingUserRole)
								.clickOnUserDropdown()
								.selectUserRole(newUerRole)
								.clickOnBackButton()
								.changeRoleConfirmation();

								menuPageAndroid.tappingAccountView();
								loginPageAndroid.clickLogout()
								.selectFeature(userType)
								.login(secondaryEmail, secondaryPassword);

								menuPageAndroid.tappingAccountView();// Tap on Account view

								usersAccountPageAndroid.clickOnAccountInvitationLink();
								if (usersAccountPageAndroid.verifyAcceptedInvitation(primaryUserName,newUerRole)) {
									extentTestAndroid.log(Status.PASS, invitedUsersName + " role is changed successfully to " +newUerRole);
								}else {
									extentTestAndroid.log(Status.FAIL,invitedUsersName + " role is not changed " +newUerRole);
									utilityAndroid.captureScreenshot(androidDriver);
								}
								manageUsersPageAndroid.clickOnBackButton();
								manageUsersPageAndroid.clickOnBackButton();
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "There are no users");
							utilityAndroid.captureScreenshot(androidDriver);
						}
						if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission for users link");	
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission for users link");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
							extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission for users link");	
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
							extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission for users link");
						}
					}

				} else {
					extentTestAndroid.log(Status.PASS, " Invitation was not sent to user , hence could't change role");
				}
			} else {
				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**
	 * Description: This is an Android test method to delete an Admin / Family / Guest user from list of permitted users for an account. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA74_ValidateInvitedUserIsDeleted(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String invitedUsersName;
		String userRole;

		invitedUsersName = testData.get(0).split(",")[0];
		userRole = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (adminInvitationSent || familyInvitationSent || guestInvitationSent) {
				menuPageAndroid.tappingAccountView();
				loginPageAndroid.clickLogout() // logout from primary user and login as secondary user which has to take action
				.selectFeature(userType)
				.login(email, password); 

				devicesPageAndroid.verifyAndSelectUserRole(activeUserRole, activeAccountName);

				if (roleSelected) {
					if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
						if (!manageUsersPageAndroid.verifyNoUserInvitedText()) {
							if (manageUsersPageAndroid.verifyUserPresent(invitedUsersName, userRole)) {
								manageUsersPageAndroid.clickOnUser(invitedUsersName,userRole)
								.clickOnDeleteButton()
								.clickDeleteConfirmation();
								if (manageUsersPageAndroid.verifyUserDeleted(invitedUsersName)) {
									extentTestAndroid.log(Status.PASS, invitedUsersName + " is deleted successfully from user list" );
								} else {
									extentTestAndroid.log(Status.FAIL, invitedUsersName + " is not deleted from user list");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Test MTA-74 :- " +invitedUsersName + "with role as" +userRole + " is not present");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "No users are invited to this account");
							utilityAndroid.captureScreenshot(androidDriver);
						}
						if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
							extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to delete user");	
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to delete user");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to delete user");	
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
							extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to delete user");
						}
					}
				} else {
					extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
				}
			} else {
				extentTestAndroid.log(Status.SKIP, " Invitation as Admin user was not sent ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**
	 * Description	: This is an Android test method that sends an invite to the email id (passed as input). 
	 * Verifies that invitation is sent and then delete the sent invitation
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA185_ValidateDeleteInvitations(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception{
		String email;
		String userRole;

		email = testData.get(0).split(",")[0];
		userRole = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (roleSelected) {
				if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
					manageUsersPageAndroid.clickOnAddUserbtn();
					if (!manageUsersPageAndroid.verifyErrorPopUp()) {
						manageUsersPageAndroid.enterUserEmail(email) // Enter invitation email
						.enterConfirmUserEmail(email)
						.clickOnUserDropdown()
						.selectUserRole(userRole)
						.clickOnSendButton();
						if (!manageUsersPageAndroid.verifyErrorPopUp()) {
							manageUsersPageAndroid.clickOnDoneButton();
							if (manageUsersPageAndroid.verifyInvitedUsersEmail(email)) { // Verify email is present in invited list
								extentTestAndroid.log(Status.PASS, "Invitation sent to :- " + email + " email ID successfully for Role :- " + userRole);
								manageUsersPageAndroid.clickOnInvitationSent(email)
								.clickOnDeleteButton()
								.clickDeleteConfirmation();
								if (manageUsersPageAndroid.verifyUserDeleted(email)) {
									extentTestAndroid.log(Status.PASS, "user is deleted successfully " +email);
								} else {
									extentTestAndroid.log(Status.FAIL, "user is not deleted" +email);
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, "Invited users email ID :- " + email + " for Role :- " + userRole + ", not found in the Invitation Users list");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while sending invitation to " + email);
							utilityAndroid.captureScreenshot(androidDriver);
							manageUsersPageAndroid.clickOnOkButton()
							.clickOnBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + email);
						utilityAndroid.captureScreenshot(androidDriver);
						manageUsersPageAndroid.clickOnOkButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission for users link");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission for users link");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission for users link");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission for users link");
					}
				}
			} else {
				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**                           
	 * Description	: This is an Android test method that first sends the invite to email as per the test data, verifies the invitation send and resends
	 *  the invite to the same user.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA100_ValidateResendInvitations(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception{
		String email;
		String userRole;

		email = testData.get(0).split(",")[0];
		userRole = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (roleSelected) {
				if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
					if ((!manageUsersPageAndroid.verifyNoUserInvitedText() && !manageUsersPageAndroid.verifyInvitedUsersEmail(email)) || manageUsersPageAndroid.verifyNoUserInvitedText()) {
						manageUsersPageAndroid.clickOnAddUserbtn();
						if (!manageUsersPageAndroid.verifyErrorPopUp()) {
							manageUsersPageAndroid.enterUserEmail(email) // Enter invitation email
							.enterConfirmUserEmail(email)
							.clickOnUserDropdown()
							.selectUserRole(userRole)
							.clickOnSendButton();
							if (!manageUsersPageAndroid.verifyErrorPopUp()) {
								manageUsersPageAndroid.clickOnDoneButton();
							} else {
								extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + email);
								utilityAndroid.captureScreenshot(androidDriver);
								manageUsersPageAndroid.clickOnOkButton()
								.clickOnBackButton();
							}
							if (!manageUsersPageAndroid.verifyNoUserInvitedText() && manageUsersPageAndroid.verifyInvitedUsersEmail(email)) { 
								extentTestAndroid.log(Status.PASS, "Invitation sent to :- " + email + " successfully as :- " + userRole);
							} else {
								extentTestAndroid.log(Status.FAIL, "Invited users email ID :- " + email + " for Role :- " + userRole + ", not found in the Invitation Users list");
								utilityAndroid.captureScreenshot(androidDriver);
							} 
							manageUsersPageAndroid.clickOnInvitationSent(email)
							.clickOnDeleteButton()
							.clickDeleteConfirmation();
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + email);
							utilityAndroid.captureScreenshot(androidDriver);
							manageUsersPageAndroid.clickOnOkButton();
						}
					}
					if (!manageUsersPageAndroid.verifyNoUserInvitedText() && manageUsersPageAndroid.verifyInvitedUsersEmail(email)) { 
						manageUsersPageAndroid.clickOnResendInvitation(email)
						.clickOnDoneButton();
						if (manageUsersPageAndroid.verifyExpiryDate(email)) {
							extentTestAndroid.log(Status.PASS, "Invitation resent successfully to "+email);
						} else {
							extentTestAndroid.log(Status.FAIL, "Invitaion not resent to "+email);
							utilityAndroid.captureScreenshot(androidDriver);
						}
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add a user");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add a user");
					}
				}
			} else {
				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**
	 * Description : This is an Android test method to verify different error messages shown to user while sending invites 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA182_ValidateErrorMessagesForAddUser(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String current_account_email;
		String correct_Email;
		String invalid_Email;
		String userRole;

		current_account_email = testData.get(0).split(",")[0];
		correct_Email = testData.get(0).split(",")[1];
		invalid_Email = testData.get(0).split(",")[2];
		userRole = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (roleSelected) {
				if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
					// Validating error message when user enters blank 
					manageUsersPageAndroid.clickOnAddUserbtn();
					if(!manageUsersPageAndroid.verifyErrorPopUp()){
						manageUsersPageAndroid.enterUserEmail(" ")
						.enterConfirmUserEmail(" ")
						.clickOnSendButton();
						if (manageUsersPageAndroid.verifyErrorMessage()) {
							extentTestAndroid.log(Status.PASS, "Error message validated for sending invitation without entering users email id  ");
						} else {
							extentTestAndroid.log(Status.FAIL, "Error message validation failed for sending invitation without entering users email id ");
						}

						// Validating error message when user enters invalid email id's 
						manageUsersPageAndroid.enterUserEmail(invalid_Email)
						.enterConfirmUserEmail(invalid_Email)
						.clickOnUserDropdown()
						.selectUserRole(userRole)
						.clickOnSendButton();
						if (manageUsersPageAndroid.verifyErrorMessage()) {
							extentTestAndroid.log(Status.PASS, "Error message validation done successfully for user entering invailid email id's ");
						} else {
							extentTestAndroid.log(Status.FAIL, "Error message  validation failed for user entering invalid email id's ");
						}

						// Validating error message when user enters own email id
						manageUsersPageAndroid.enterUserEmail(current_account_email)// Sending invitation for first time
						.enterConfirmUserEmail(current_account_email)
						.clickOnUserDropdown()
						.selectUserRole(userRole)
						.clickOnSendButton();
						if (manageUsersPageAndroid.verifyErrorMessage()) {
							extentTestAndroid.log(Status.PASS, "Error message validation done successfully for user enters own email id ");
						} else {
							extentTestAndroid.log(Status.FAIL, "Error message  validation failed for user enters own email id ");
						}

						// Validating error message when user enters mismatched email id;s
						manageUsersPageAndroid.enterUserEmail(correct_Email)// Sending invitation for first time
						.enterConfirmUserEmail(invalid_Email)
						.clickOnUserDropdown()
						.selectUserRole(userRole)
						.clickOnSendButton();
						if (manageUsersPageAndroid.verifyErrorMessage()) {
							extentTestAndroid.log(Status.PASS, "Error message validation done successfully for user enters mismatched email id's ");
						} else {
							extentTestAndroid.log(Status.FAIL, "Error message  validation failed for user enters mismatched email id's ");
						}

						// Send two invitations to same user
						manageUsersPageAndroid.enterUserEmail(correct_Email)// Sending invitation for first time
						.enterConfirmUserEmail(correct_Email)
						.clickOnSendButton()
						.clickOnDoneButton()
						.clickOnAddUserbtn(); // Sending invitation to same user second time
						if (!manageUsersPageAndroid.verifyErrorMessage()) {
							manageUsersPageAndroid.enterUserEmail(correct_Email)
							.enterConfirmUserEmail(correct_Email)
							.clickOnSendButton();
							if (manageUsersPageAndroid.verifyErrorMessage()) {
								extentTestAndroid.log(Status.PASS, "Error message validation done successfully for user sending multiple invitation to same user ");
							} else {
								extentTestAndroid.log(Status.FAIL, "Error message  validation failed for user sending multiple invitation to same user ");
							}
							manageUsersPageAndroid.clickOnBackButton()
							.clickOnInvitationSent(correct_Email)// Deleting the sent invitation to cleanup
							.clickOnDeleteButton()
							.clickDeleteConfirmation();
						} else {
							extentTestAndroid.log(Status.PASS, "Error message validation done successfully for user sending multiple invitation to same user ");
							manageUsersPageAndroid.clickOnInvitationSent(correct_Email)// Deleting the sent invitation to cleanup
							.clickOnDeleteButton()
							.clickDeleteConfirmation();
						}	
					} else {
						extentTestAndroid.log(Status.PASS,manageUsersPageAndroid.getErrorMessage());
						manageUsersPageAndroid.clickOnOkButton();
					}

					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission for users link");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission for users link");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission for users link");	
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission for users link");
					}
				}
			} else {
				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**                           
	 * Description : This is an Android method that would send the invitation to secondary user with Admin / Family / Guest privileges , logs out and log in to the secondary user account,
	 * decline the invitation , re-login to Host account and verify the invitation list doesn't show the declined invite.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA302_ValidateDecliningPendingInvitationForUser(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception{
		String primaryEmail;
		String primaryPassword;
		String primaryUserRole;
		String primaryAccountName;
		String secondaryEmail;
		String secondaryPassword; 

		primaryEmail = testData.get(0).split(",")[0];
		primaryPassword = testData.get(0).split(",")[1];
		primaryUserRole = testData.get(0).split(",")[2];
		primaryAccountName = testData.get(0).split(",")[3];
		secondaryEmail = testData.get(0).split(",")[4];
		secondaryPassword = testData.get(0).split(",")[5];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
				if ((!manageUsersPageAndroid.verifyNoUserInvitedText() && !manageUsersPageAndroid.verifyInvitedUsersEmail(secondaryEmail)) 
						|| manageUsersPageAndroid.verifyNoUserInvitedText()) {
					manageUsersPageAndroid.clickOnAddUserbtn();
					if (!manageUsersPageAndroid.verifyErrorMessage()) {
						manageUsersPageAndroid.enterUserEmail(secondaryEmail) // Enter invitation email
						.enterConfirmUserEmail(secondaryEmail)
						.clickOnUserDropdown()
						.selectUserRole(primaryUserRole)
						.clickOnSendButton();

						if (!manageUsersPageAndroid.verifyErrorMessage()) {
							manageUsersPageAndroid.clickOnDoneButton();
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + secondaryEmail);
							manageUsersPageAndroid.clickOnBackButton();
						}
						if (!manageUsersPageAndroid.verifyNoUserInvitedText() && manageUsersPageAndroid.verifyInvitedUsersEmail(secondaryEmail)) { 
							extentTestAndroid.log(Status.PASS, "Invitation sent to :- " + secondaryEmail + " successfully as :- " + primaryUserRole);

							menuPageAndroid.tappingAccountView();
							loginPageAndroid.clickLogout();
							if(loginPageAndroid.verifyEnvTextPresent()) {
								loginPageAndroid.selectFeature(userType);
							}
							loginPageAndroid.login(secondaryEmail, secondaryPassword);
							extentTestAndroid.log(Status.INFO, "Login to the secondary user account "+secondaryEmail);

							menuPageAndroid.tappingAccountView();// Tap on Account view

							if (usersAccountPageAndroid.verifyAccountPage(secondaryEmail)) {
								extentTestAndroid.log(Status.INFO, "Secondary email ID verified on user account page- "+secondaryEmail);
								usersAccountPageAndroid.clickOnAccountInvitationLink();
								if (!usersAccountPageAndroid.verifyNoInvitationText() && usersAccountPageAndroid.verifyPendingInvitations(primaryAccountName, primaryUserRole)) {
									extentTestAndroid.log(Status.INFO, "Invitation received successfully from - "+primaryEmail);
									usersAccountPageAndroid.clickOnPendingInvitations(primaryAccountName, primaryUserRole)
									.clickOnAcceptOrDecline("Decline");
									if (usersAccountPageAndroid.verifyNoInvitationText() || !usersAccountPageAndroid.verifyPendingInvitations(primaryAccountName, primaryUserRole)) {
										extentTestAndroid.log(Status.PASS, "Secondary user successfully decline the pending guest invitation.");
									} else {
										extentTestAndroid.log(Status.FAIL, "Secondary user did not decline the pending guest invitation.");
									}
									usersAccountPageAndroid.clickOnBackButton();
								} else {
									extentTestAndroid.log(Status.FAIL, "Invitation did not receive from - "+primaryEmail);
									utilityAndroid.captureScreenshot(androidDriver);
								} 
							}
							else {							
								extentTestAndroid.log(Status.FAIL, "Secondary email ID not verified - "+secondaryEmail);
								utilityAndroid.captureScreenshot(androidDriver);
							} 

							menuPageAndroid.tappingAccountView();
							loginPageAndroid.clickLogout();
							if(loginPageAndroid.verifyEnvTextPresent()) {
								loginPageAndroid.selectFeature(userType);
							}
							loginPageAndroid.login(primaryEmail, primaryPassword);

							extentTestAndroid.log(Status.INFO, "Login to the primary user account "+primaryEmail);
							devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);

							menuPageAndroid.clickMenuLink("Users");// Tap on Account view
							if (manageUsersPageAndroid.verifyNoUserInvitedText() || !manageUsersPageAndroid.verifyInvitedUsersEmail(secondaryEmail)) {
								extentTestAndroid.log(Status.PASS, "Invitation not available after declining, as expected");
							} else {
								extentTestAndroid.log(Status.FAIL, " Invitation still available after declining");
							}
						} 	else {
							extentTestAndroid.log(Status.FAIL, "Invited users email ID :- " + secondaryEmail + " for Role :- " + primaryUserRole + ", not found in the Invitation Users list");
						} 
					} else {
						extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + secondaryEmail);
						manageUsersPageAndroid.clickOnBackButton();
					}
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission for users link");	
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission for users link");
				}
			}
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	}

	/**                           
	 * Description : This is an Android method that would send the invitation to secondary user with Admin / Family / Guest  privileges , logs out and log in to the secondary user account,
	 * accepts the invitation and the deletes the accepted invite , re-login to Host account and verify the accepted user is not present 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA309_ValidateDeletionOfAcceptedInvitationByUser(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception{
		String primaryEmail;
		String primaryPassword;
		String primaryUserRole;
		String primaryAccountName;
		String secondaryEmail;
		String secondaryPassword; 

		primaryEmail = testData.get(0).split(",")[0];
		primaryPassword = testData.get(0).split(",")[1];
		primaryUserRole = testData.get(0).split(",")[2];
		primaryAccountName = testData.get(0).split(",")[3];
		secondaryEmail = testData.get(0).split(",")[4];
		secondaryPassword = testData.get(0).split(",")[5];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		manageUsersPageAndroid = new ManageUsersPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(multiUser) {
			if (menuPageAndroid.verifyAndClickMenuLink("Users")) {
				if ((!manageUsersPageAndroid.verifyNoUserInvitedText() && !manageUsersPageAndroid.verifyInvitedUsersEmail(secondaryEmail)) || manageUsersPageAndroid.verifyNoUserInvitedText()) {
					manageUsersPageAndroid.clickOnAddUserbtn();
					if (!manageUsersPageAndroid.verifyErrorMessage()) {
						manageUsersPageAndroid.enterUserEmail(secondaryEmail) // Enter invitation email
						.enterConfirmUserEmail(secondaryEmail)
						.clickOnUserDropdown()
						.selectUserRole(primaryUserRole)
						.clickOnSendButton();

						if (!manageUsersPageAndroid.verifyErrorMessage()) {
							manageUsersPageAndroid.clickOnDoneButton();
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + secondaryEmail);
							manageUsersPageAndroid.clickOnBackButton();
						}
						if (!manageUsersPageAndroid.verifyNoUserInvitedText() && manageUsersPageAndroid.verifyInvitedUsersEmail(secondaryEmail)) { 
							extentTestAndroid.log(Status.PASS, "Invitation sent to :- " + secondaryEmail + " successfully as :- " + primaryUserRole);

							menuPageAndroid.tappingAccountView();
							loginPageAndroid.clickLogout();
							if(loginPageAndroid.verifyEnvTextPresent()) {
								loginPageAndroid.selectFeature(userType);
							}
							loginPageAndroid.login(secondaryEmail, secondaryPassword);
							extentTestAndroid.log(Status.INFO, "Login to the secondary user account "+secondaryEmail);

							menuPageAndroid.tappingAccountView();// Tap on Account view
							if (usersAccountPageAndroid.verifyAccountPage(secondaryEmail)) {
								extentTestAndroid.log(Status.INFO, "Secondary email ID verified on user account page- "+secondaryEmail);
								usersAccountPageAndroid.clickOnAccountInvitationLink();
								if (!usersAccountPageAndroid.verifyNoInvitationText() && usersAccountPageAndroid.verifyPendingInvitations(primaryAccountName, primaryUserRole)) {
									extentTestAndroid.log(Status.INFO, "Invitation received successfully from - "+primaryEmail);
									usersAccountPageAndroid.clickOnPendingInvitations(primaryAccountName, primaryUserRole)
									.clickOnAcceptOrDecline("Accept");

									menuPageAndroid.tappingAccountView();// Tap on Account view
									usersAccountPageAndroid.clickOnAccountInvitationLink();

									if (usersAccountPageAndroid.verifyAcceptedInvitation(primaryAccountName, primaryUserRole)) {
										extentTestAndroid.log(Status.PASS, "Invitation has been accepted successfully as admin");
										usersAccountPageAndroid.deleteAcceptedInvitation();
										if (usersAccountPageAndroid.verifyNoInvitationText() || !usersAccountPageAndroid.verifyAcceptedInvitation(primaryAccountName, primaryUserRole)) {
											extentTestAndroid.log(Status.PASS, "Secondary user successfully delete the accepted admin invitation.");
										} else {
											extentTestAndroid.log(Status.FAIL, "Secondary user did not delete the accepted admin invitation.");
											utilityAndroid.captureScreenshot(androidDriver);
										}
										usersAccountPageAndroid.clickOnBackButton();
									} else{
										extentTestAndroid.log(Status.FAIL, "Invitation is not matching the username or role");
										utilityAndroid.captureScreenshot(androidDriver);
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Invitation did not receive from - "+primaryEmail);
									utilityAndroid.captureScreenshot(androidDriver);
								} 
							} else {							
								extentTestAndroid.log(Status.FAIL, "Secondary email ID not verified - "+secondaryEmail);
								utilityAndroid.captureScreenshot(androidDriver);
							} 

							menuPageAndroid.tappingAccountView();
							loginPageAndroid.clickLogout();
							if(loginPageAndroid.verifyEnvTextPresent()) {
								loginPageAndroid.selectFeature(userType);
							}
							loginPageAndroid.login(primaryEmail, primaryPassword);

							extentTestAndroid.log(Status.INFO, "Login to the primary user account "+primaryEmail);
							devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);

							menuPageAndroid.clickMenuLink("Users");// Tap on Account view
							if (manageUsersPageAndroid.verifyNoUserInvitedText() || !manageUsersPageAndroid.verifyInvitedUsersEmail(secondaryEmail)) {
								extentTestAndroid.log(Status.PASS, "Invitation not available after deleting, as expected");
							} else {
								extentTestAndroid.log(Status.FAIL, "Invitation is still available after deleting");
							}

						} else {
							extentTestAndroid.log(Status.FAIL, "Invited users email ID :- " + secondaryEmail + " for Role :- " + primaryUserRole + ", not found in the Invitation Users list");
						} 
					} else {
						extentTestAndroid.log(Status.FAIL, "Error while sending the invitation to " + secondaryEmail);
					}
				}
			} 
		} else {
			extentTestAndroid.log(Status.SKIP, " Since V5 only is selected , multi - user scenario is not applicable ");
		}
	} 

	/**
	 * Description : This is an Android test method to change all the fields on edit user info page , verify all the changes done 
	 * and revert all the field data to original
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA85_ValidateEditInfo(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String firstName;
		String lastName;
		String accountName;
		String street1;
		String street2;
		String accountCity;
		String accountState;
		String accountZipCode;
		String mobileNumber;
		String country;
		String language;
		String firstNameOld;
		String lastNameOld;
		String accountNameOld;
		String street1Old;
		String street2Old;
		String accountCityOld;
		String accountStateOld;
		String accountZipCodeOld;
		String mobileNumberOld;
		String countryOld;
		String languageOld;
		String usersName;
		String tempText;

		firstName = testData.get(0).split(",")[0];
		lastName = testData.get(0).split(",")[1];
		accountName = testData.get(0).split(",")[2];
		street1 = testData.get(0).split(",")[3];
		street2 = testData.get(0).split(",")[4];
		accountCity = testData.get(0).split(",")[5];
		accountState = testData.get(0).split(",")[6];
		accountZipCode = testData.get(0).split(",")[7];
		mobileNumber = testData.get(0).split(",")[8];
		country = testData.get(0).split(",")[9];
		language = testData.get(0).split(",")[10];

		extentTestAndroid =  createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		//select account user
		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tapEditUserInfo();
		//get the existing data, this would be used for verification
		firstNameOld = usersAccountPageAndroid.getFirstName();
		lastNameOld = usersAccountPageAndroid.getLastName();
		accountNameOld = usersAccountPageAndroid.getAccountName();
		street1Old = usersAccountPageAndroid.getStreet1();
		street2Old = usersAccountPageAndroid.getStreet2();
		accountCityOld = usersAccountPageAndroid.getCity();
		accountStateOld = usersAccountPageAndroid.getState();
		accountZipCodeOld = usersAccountPageAndroid.getZipCode();
		mobileNumberOld = usersAccountPageAndroid.getMobileNumber();
		countryOld = usersAccountPageAndroid.getCountry();
		languageOld = usersAccountPageAndroid.getLanguage();

		//verify edit user profile
		usersAccountPageAndroid.editFirstName(firstName)
		.editLastName(lastName)
		.editAccountName(accountName)
		.editStreet1(street1)
		.editStreet2(street2)
		.editAccountcity(accountCity)
		.editAccountState(accountState)
		.editAccountZipCode(accountZipCode)
		.editMobileNumber(mobileNumber)
		.selectCountry(country)
		.selectLanguage(language)
		.clickOnSaveButton();

		if(!usersAccountPageAndroid.verifyErrorPresent()) {
			if (usersAccountPageAndroid.getUserNameAccountInfo().equalsIgnoreCase((firstName + " " + lastName))) {
				extentTestAndroid.log(Status.PASS, "Account user name changed successfully on account info page");
			} else {
				extentTestAndroid.log(Status.FAIL, "Account user name not changed on account info page");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			menuPageAndroid.clickHamburger(); //Verify user name text present at bottom of hamburger menu
			usersName = firstName.concat(" ").concat(lastName);
			tempText = menuPageAndroid.getUserName();
			if(tempText.equals(usersName)){
				extentTestAndroid.log(Status.PASS, "Test MTA-308 :- Account user name changed successfully on menulinks");
			} else {
				extentTestAndroid.log(Status.FAIL, "Test MTA-308 :- Account user name not changed. It is : " + tempText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			menuPageAndroid.clickHamburger(); 
			usersAccountPageAndroid.tapEditUserInfo();
			if (usersAccountPageAndroid.getFirstName().equals(firstName)) {
				extentTestAndroid.log(Status.PASS, "First name changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "First name not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getLastName().equals(lastName)) {
				extentTestAndroid.log(Status.PASS, "Last name changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "Last name not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getAccountName().equals(accountName)) {
				extentTestAndroid.log(Status.PASS, "Account name changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "Account name not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (!usersAccountPageAndroid.editAccountEmail()) {
				extentTestAndroid.log(Status.PASS, "Email is not editable");
			} else {
				extentTestAndroid.log(Status.FAIL, "Email is editable");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getStreet1().equals(street1)) {
				extentTestAndroid.log(Status.PASS, "street 1 changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "street 1 not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getStreet2().equals(street2)) {
				extentTestAndroid.log(Status.PASS, "street 2 changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "street 2 not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getCity().equals(accountCity)) {
				extentTestAndroid.log(Status.PASS, "City changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "City not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getState().equals(accountState)) {
				extentTestAndroid.log(Status.PASS, "State changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "State not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getZipCode().equals(accountZipCode)) {
				extentTestAndroid.log(Status.PASS, "Zipcode changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "Zipcode changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getMobileNumber().equals(mobileNumber)) {
				extentTestAndroid.log(Status.PASS, "Mobile Number changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, "Mobile Number not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getCountry().equals(country)) {
				extentTestAndroid.log(Status.PASS, " Country changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, " Country not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			if (usersAccountPageAndroid.getLanguage().equals(language)) {
				extentTestAndroid.log(Status.PASS, " language changed successfully");
			} else {
				extentTestAndroid.log(Status.FAIL, " language not changed successfully");
				utilityAndroid.captureScreenshot(androidDriver);
			}

			usersAccountPageAndroid.editFirstName(firstNameOld)
			.editLastName(lastNameOld)
			.editAccountName(accountNameOld)
			.editStreet1(street1Old)
			.editStreet2(street2Old)
			.editAccountcity(accountCityOld)
			.editAccountState(accountStateOld)
			.editAccountZipCode(accountZipCodeOld)
			.editMobileNumber(mobileNumberOld)
			.selectCountry(countryOld)
			.selectLanguage(languageOld)
			.clickOnSaveButton();

			if(!usersAccountPageAndroid.verifyErrorPresent()) {
				extentTestAndroid.log(Status.PASS, " Changes reverted back to original");
			} else {
				extentTestAndroid.log(Status.FAIL,  usersAccountPageAndroid.getErrorText());
				utilityAndroid.captureScreenshot(androidDriver);
				usersAccountPageAndroid.clickOnOkButton()
				.clickOnBackButton();
			}
		} else {
			extentTestAndroid.log(Status.FAIL, usersAccountPageAndroid.getErrorText());
			utilityAndroid.captureScreenshot(androidDriver);
			usersAccountPageAndroid.clickOnOkButton()
			.clickOnBackButton();
		}
	}

	/**
	 * Description : This is an Android test method to verify all the error messages shown while editing the user information
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA187_ValidateErrorMessagesOnEditInfo(ArrayList<String> testData, String deviceTest, String scenarioName) {
		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		//select account user
		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tapEditUserInfo();
		usersAccountPageAndroid.editFirstName("")
		.clickOnSaveButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, " Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, " No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		usersAccountPageAndroid.editFirstName("FirstName")
		.editLastName("")
		.clickOnSaveButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, " Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, " No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		usersAccountPageAndroid.editLastName("LastName")
		.editAccountZipCode("")
		.clickOnSaveButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, " Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, " No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		usersAccountPageAndroid.clickOnBackButton();
	}

	/**
	 * Description : This is an Android test method to verify maximum field limit for first name, last name and zip code on edit info page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA307_ValidateTextInputLimitOnEditInfo(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String accountName = "MyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProdMyQPreProd";
		String firstName = "myQautomationmyQautomationmyQautomation";
		String lastName = "synechronmyqsynechronmyqsynechronmyq";
		String zipCode = "1234567890112";
		String textEntered = "";
		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tapEditUserInfo();
		// Verify account name
		usersAccountPageAndroid.editAccountName(accountName);
		textEntered = usersAccountPageAndroid.edit_account_name.getText();
		int lenAccountName = textEntered.length();
		if (lenAccountName == 225) {
			extentTestAndroid.log(Status.PASS, "The Account name field accepts only 225 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "Account name Text Entered: " + accountName + " ,text retreived after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Verify first name
		usersAccountPageAndroid.editFirstName(firstName);
		textEntered = usersAccountPageAndroid.edit_first_name.getText();
		int lenFirstName = textEntered.length();
		if (lenFirstName == 30) {
			extentTestAndroid.log(Status.PASS, "The First name field accepts only 30 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "First name Text Entered: " + firstName + " ,text retrieved after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verify Last Name		
		usersAccountPageAndroid.editLastName(lastName);
		textEntered = usersAccountPageAndroid.edit_last_name.getText();
		int lenLastName = textEntered.length();
		if (lenLastName == 30) {
			extentTestAndroid.log(Status.PASS, "The Last name field accepts only 30 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "Last name Text Entered: " + lastName + " ,text retrieved after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Verify Zipcode		
		usersAccountPageAndroid.editAccountZipCode(zipCode);
		textEntered = usersAccountPageAndroid.edit_zip_code.getText();
		int lenZipCode = textEntered.length();
		if (lenZipCode == 10) {
			extentTestAndroid.log(Status.PASS, "The Zipcode field accepts only 10 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "Zipcode Text Entered: " + zipCode + " ,text retrieved after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}
		usersAccountPageAndroid.clickOnBackButton();
	}

	/**
	 * Description : This is an Android test method that enter's a new email id to changes the current e-mail,
	 * and verify the change email confirmation pop-up 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA88_ValidateChangeEmail(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String newEmail;
		String currentPassword;
		String currentEmail;

		newEmail = testData.get(0).split(",")[0];
		currentPassword = testData.get(0).split(",")[1];
		currentEmail = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		//select account user
		menuPageAndroid.tappingAccountView();
		//verify change Email
		usersAccountPageAndroid.tapChangeEmail()
		.setNewEmail(newEmail)
		.setConfirmNewEmail(newEmail)
		.enterCurrentPassword(currentPassword)
		.clickNextButton();
		if (usersAccountPageAndroid.verifyEmailChangeConfimation(currentEmail)) {
			extentTestAndroid.log(Status.PASS, "Email changed");
		} else {
			extentTestAndroid.log(Status.FAIL, "Email not changed ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
	}

	/**
	 * Description : This is an Android test method to verify error messages shown to user while changing the email id
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA88_ValidateErrorMessagesOnChangeEmail(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String email;
		String currentPassword;
		String errText = null;

		email = testData.get(0).split(",")[0];
		currentPassword = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		//error for empty fields
		usersAccountPageAndroid.tapChangeEmail()
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Test MTA-88 :- Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-88 :- No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//incorrect password
		usersAccountPageAndroid.setNewEmail("test@test.com")
		.setConfirmNewEmail("test@test.com")
		.enterCurrentPassword("123Sample122")
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Test MTA-88 :- Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-88 :- No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//incorrect email
		usersAccountPageAndroid.setNewEmail(".com")
		.setConfirmNewEmail(".com")
		.enterCurrentPassword(currentPassword)
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Test MTA-88 :- Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-88 :- No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//email do not match
		usersAccountPageAndroid.setNewEmail("abc@abc.com")
		.setConfirmNewEmail("Test@test.com")
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Test MTA-88 :- Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-88 :- No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//email already in use and device error code (703)
		usersAccountPageAndroid.setNewEmail(email)
		.setConfirmNewEmail(email)
		.clickNextButton();
		if(usersAccountPageAndroid.verifyErrorPresent()) {
			errText = usersAccountPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("This email is already in use. (703)")) {
				extentTestAndroid.log(Status.PASS, "Test MTA-88 :- Error message as expected " + errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Test MTA-88 :- Different error pop-up was present :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			usersAccountPageAndroid.clickOnOkButton();
			usersAccountPageAndroid.clickCancelButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-88 :- No error pop-up present, while tapping the login button with valid username and invalid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
	}

	/**
	 * Description : This is an Android test method to change the password, logs out and login with the new password and revert the password to original
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */	
	public void MTA96_ValidateChangePassword(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String currentEmail;
		String currentPassword;
		String newPassword;

		currentEmail = testData.get(0).split(",")[0];
		currentPassword = testData.get(0).split(",")[1];
		newPassword = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		//select account user
		menuPageAndroid.tappingAccountView();
		//change password
		usersAccountPageAndroid.tappingChangePassword()
		.enterPassword(currentPassword)
		.enterNewPassword(newPassword);
		// Verify the password passed in the test data is valid
		if (usersAccountPageAndroid.validatePasswordCriteria(newPassword)) {
			extentTestAndroid.log(Status.INFO, "Password contains valid combination of characters");
		} else {
			extentTestAndroid.log(Status.FAIL, "Validate Change Password: '" + newPassword + "' New password doesn't contain the required combination of characters");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		usersAccountPageAndroid.enterConfirmPassword(newPassword);
		usersAccountPageAndroid.clickNextButton();
		loginPageAndroid.clickLogout();
		// Re login to the application using new password to verify updated password
		loginPageAndroid.login(currentEmail, newPassword); 
		if (devicesPageAndroid.verifyHomeScreen()) { // Verify Login successful using new password
			extentTestAndroid.log(Status.PASS, "Validate Change Password: Login successful using updated password");
		} else {
			extentTestAndroid.log(Status.FAIL, "Validate Change Password: Login unsuccessful using updated password");	
			utilityAndroid.captureScreenshot(androidDriver);
		}

		menuPageAndroid.tappingAccountView();
		// Reset the password back to the original one
		usersAccountPageAndroid.tappingChangePassword()
		.enterPassword(newPassword)
		.enterNewPassword(currentPassword);
		usersAccountPageAndroid.enterConfirmPassword(currentPassword);
		usersAccountPageAndroid.clickNextButton();
	}

	/**
	 * Description : This is an Android test method to verify the error message shown while changing the password
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */	
	public void MTA96_ValidateErrorMessagesOnChangePassword(ArrayList<String> testData, String deviceTest, String scenarioName) {
		String currentPassword;

		currentPassword = testData.get(0).split(",")[0];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingChangePassword()
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		usersAccountPageAndroid.enterPassword("Abcd5678")
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		usersAccountPageAndroid.enterPassword(currentPassword)
		.enterNewPassword("")
		.enterConfirmPassword("")
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		usersAccountPageAndroid.enterNewPassword("ABC")
		.enterConfirmPassword("ABC")
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		usersAccountPageAndroid.enterNewPassword("Abcd1234")
		.enterConfirmPassword("Abcd5678")
		.clickNextButton();
		if (usersAccountPageAndroid.verifyErrorOnScreen()) {
			extentTestAndroid.log(Status.PASS, "Error pop-up present ");
		} else {
			extentTestAndroid.log(Status.FAIL, "No error");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		usersAccountPageAndroid.clickOnBackButton();
	}

	/**
	 * Description : This is Android test method verifies the automatic login
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA169_ValdiateAutomaticLogin(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enableLaunchingApplication(false)
		.clickDoneButton();

		//to close the app
		androidDriver.navigate().back();
		androidDriver.navigate().back();
		// to launch the app
		androidDriver.startActivity(appPackage, appActivity);

		//		androidDriver.closeApp();
		//		androidDriver.startActivity(appPackage, appActivity);

		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);

		if (devicesPageAndroid.verifyLoginPage()) {
			extentTestAndroid.log(Status.PASS, "Auto login successfull");
		} else {
			extentTestAndroid.log(Status.FAIL, "Auto Login failed");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enableLaunchingApplication(true)
		.clickDoneButton();
	} 

	/**
	 * Description : This is an Android test method to verify application launch via passcode
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA183_ValidateLaunchingPasscodeLogin(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String passcode;
		passcode = testData.get(0).split(",")[0];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enablePasscode(true);
		if(usersAccountPageAndroid.verifyNewPasscodeScreen()) {
			usersAccountPageAndroid.setPasscode(passcode)
			.enableLaunchingApplication(true)
			.clickDoneButton();
		} else {
			usersAccountPageAndroid.tapOnPasscode()
			.enterPasscode(passcode)
			.clickOnChangePasscode()
			.setPasscode(passcode)
			.enableLaunchingApplication(true)
			.clickDoneButton();
		}

		//to close the app
		androidDriver.navigate().back();
		androidDriver.navigate().back();
		androidDriver.startActivity(appPackage, appActivity);

		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);

		loginPageAndroid.enterPasscode(passcode);
		if (loginPageAndroid.verifyLoginPage()) {
			extentTestAndroid.log(Status.PASS, "Successfully launched application using passcode");
			menuPageAndroid.tappingAccountView();
			usersAccountPageAndroid.tappingSecurity()
			.enablePasscode(false)
			.clickDoneButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "Login with passcode not successfull");
			utilityAndroid.captureScreenshot(androidDriver);
		}
	} 

	/**
	 * Description : This is an Android test method to verify accessing account view via passcode
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA193_ValidatePasscodeForAccountView(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String passcode;
		passcode = testData.get(0).split(",")[0];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enablePasscode(true);
		if (usersAccountPageAndroid.verifyNewPasscodeScreen()) {
			usersAccountPageAndroid.setPasscode(passcode)
			.enableLaunchingApplication(false)
			.enableAccessingAccountView(true)
			.clickDoneButton();
		} else {
			usersAccountPageAndroid.tapOnPasscode()
			.enterPasscode(passcode)
			.clickOnChangePasscode()
			.setPasscode(passcode)
			.enableLaunchingApplication(false)
			.enableAccessingAccountView(true)
			.clickDoneButton();
		}

		menuPageAndroid.clickMenuLink("Devices");
		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.enterPasscode(passcode);

		if (usersAccountPageAndroid.verifyAccountPage()) {
			extentTestAndroid.log(Status.PASS, "Successfully accessed account view using passcode");
			usersAccountPageAndroid.tappingSecurity()
			.enablePasscode(false)
			.enableLaunchingApplication(true)
			.enableAccessingAccountView(false)
			.clickDoneButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "Account view not accessed with passcode ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
	} 

	/**
	 * Description : This is an Android test method to verify accessing account view using password
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA208_ValidatePasswordForAccountView(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String password;
		password = testData.get(0).split(",")[0];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enableLaunchingApplication(false)
		.enableAccessingAccountView(true)
		.clickDoneButton();

		menuPageAndroid.clickMenuLink("Devices");
		menuPageAndroid.tappingAccountView();
		loginPageAndroid.enterAccountPassword(password)
		.clickLoginButton();

		if (usersAccountPageAndroid.verifyAccountPage()) {
			extentTestAndroid.log(Status.PASS, "Successfully accessed account view using password");
			usersAccountPageAndroid.tappingSecurity()
			.enableLaunchingApplication(true)
			.enableAccessingAccountView(false)
			.clickDoneButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "Account view not accessed with pasword ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
	}

	/**
	 * Description : This is an Android test method to verify devices(Gates and Door) could be OPENED using passcode
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA209_ValidatePasscodeForDevices(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String passcode;
		String deviceName;

		passcode = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enablePasscode(true);

		if(usersAccountPageAndroid.verifyNewPasscodeScreen()) {
			usersAccountPageAndroid.setPasscode(passcode)
			.enableLaunchingApplication(false)
			.enableOpeningDoorsOrGate(true)
			.clickDoneButton();
		} else {
			usersAccountPageAndroid.tapOnPasscode()
			.enterPasscode(passcode)
			.clickOnChangePasscode()
			.setPasscode(passcode)
			.enableLaunchingApplication(false)
			.enableOpeningDoorsOrGate(true)
			.clickDoneButton();
		}
		menuPageAndroid.clickMenuLink("Devices");

		if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
			String status = devicesPageAndroid.getDeviceStatus(deviceName);
			if(status.equalsIgnoreCase("CLOSED")) {
				devicesPageAndroid.tapOnDeviceWithSecurity(deviceName);
			} else if (status.equalsIgnoreCase("OPEN")){
				devicesPageAndroid.deviceActuation(deviceName);
				devicesPageAndroid.tapOnDeviceWithSecurity(deviceName);
			}
			devicesPageAndroid.enterPasscode(passcode);
			Thread.sleep(10000);
			status = devicesPageAndroid.getDeviceStatus(deviceName);
			if(status.equalsIgnoreCase("OPEN")) {
				devicesPageAndroid.tapOnDeviceWithSecurity(deviceName);
				extentTestAndroid.log(Status.PASS, deviceName + " opened using passcode");
			} else {
				extentTestAndroid.log(Status.FAIL, deviceName + " not opened using passcode");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, deviceName + " is not present ");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enablePasscode(false)
		.enableLaunchingApplication(true)
		.enableOpeningDoorsOrGate(false)
		.clickDoneButton();
	} 

	/**
	 * Description : This is an Android test method to verify devices(Gates and Door) could be OPENED using password
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA211_ValidatePasswordForDevices(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String password;
		String deviceName;
		password = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		usersAccountPageAndroid = new UsersAccountPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enableLaunchingApplication(false)
		.enableOpeningDoorsOrGate(true)
		.clickDoneButton();

		menuPageAndroid.clickMenuLink("Devices");
		if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
			String status = devicesPageAndroid.getDeviceStatus(deviceName);
			if(status.equalsIgnoreCase("CLOSED")) {
				devicesPageAndroid.tapOnDeviceWithSecurity(deviceName);
				loginPageAndroid.enterAccountPassword(password)
				.clickLoginButton();
			} else if (status.equalsIgnoreCase("OPEN")){
				devicesPageAndroid.deviceActuation(deviceName);
				devicesPageAndroid.tapOnDeviceWithSecurity(deviceName);
				loginPageAndroid.enterAccountPassword(password)
				.clickLoginButton();
			}
			Thread.sleep(10000);
			status = devicesPageAndroid.getDeviceStatus(deviceName);
			if(status.equalsIgnoreCase("OPEN")) {
				devicesPageAndroid.tapOnDeviceWithSecurity(deviceName);
				extentTestAndroid.log(Status.PASS, deviceName + " opened using password");
			} else {
				extentTestAndroid.log(Status.FAIL, deviceName + " not opened using password");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, deviceName + " is not present ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		menuPageAndroid.tappingAccountView();
		usersAccountPageAndroid.tappingSecurity()
		.enableLaunchingApplication(true)
		.enableOpeningDoorsOrGate(false)
		.clickDoneButton();
	}

	//User Guide in Help is depricated from build 3.101
	/**
	 * Description	: This is an Android test method to verify text on User Guide page
	 * @param testData
	 * @param deviceTest
	 * @throws Exception
	 */	
	public void MTA170_ValidateTextOnHelpUserGuide(ArrayList<String> testData, String deviceTest) throws Exception {
		String text;
		if(brandName.equalsIgnoreCase("Liftmaster")) {
			text = "User's Guides";
		} else {
			text = "Manuals and Guides";
		}

		extentTestAndroid = createTest(deviceTest, " MTA-170 :- Validate Help for user's Guide"); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		helpPageAndroid = new HelpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.clickMenuLink("Help");
		//validate help for user's guide
		helpPageAndroid.clickOnHelpLink("user's Guide");
		if (helpPageAndroid.verifyTextOnPage(text)) {
			extentTestAndroid.log(Status.PASS, "user's Guide page is displayed ");
		} else {
			extentTestAndroid.log(Status.FAIL, " user Guide page not displayed");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		androidDriver.navigate().back();
	}

	/**
	 * Description :  This is an Android test method to verify text on MyQ Support page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */	
	public void MTA172_ValidateTextOnHelpMyQSupport(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String text;
        if(brandName.equalsIgnoreCase("Liftmaster")) {
            text = "FAQs";
        } else {
            text = "MyQ Support";
        }
        // FAQ link changed to MyQ Support in help as part of 3.100 build changes
        extentTestAndroid = createTest(deviceTest, scenarioName); 
        GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
        // object for page factory class
        menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
        helpPageAndroid = new HelpPageAndroid(androidDriver, extentTestAndroid);
        utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

        menuPageAndroid.clickMenuLink("Help");
        //validate help for FAQ
        helpPageAndroid.clickOnHelpLink("MyQ Support");
        if(brandName.equalsIgnoreCase("Liftmaster")) {
            if (helpPageAndroid.verifyFAQTextOnPage(text)) {
                extentTestAndroid.log(Status.PASS, "FAQ / MyQ Support page is displayed ");
            } else {
                extentTestAndroid.log(Status.FAIL, " FAQ / MyQ Support page not displayed");
                utilityAndroid.captureScreenshot(androidDriver);
            }    
        } else if(brandName.equalsIgnoreCase("Chamberlain"))  {
            if (helpPageAndroid.verifyTextOnPage(text)) {
                extentTestAndroid.log(Status.PASS, "FAQ / MyQ Support page is displayed ");
            } else {
                extentTestAndroid.log(Status.FAIL, " FAQ / MyQ Support page not displayed");
                utilityAndroid.captureScreenshot(androidDriver);
            }
        }
        helpPageAndroid.clickBackButton();
    }

	/**
	 * Description	: This is an Android test method to verify text on License And Terms Of Use page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA174_ValidateTextOnHelpLicenseAndTermsOfUse(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String text =  "AGREEMENT AND TERMS OF USE";
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		helpPageAndroid = new HelpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.clickMenuLink("Help");
		//validate help for License and Terms of Use
		helpPageAndroid.clickOnHelpLink("License and Terms of Use");
		if (helpPageAndroid.verifyTextOnPage(text)) {
			extentTestAndroid.log(Status.PASS, "License And Terms of Use page is displayed ");
		} else {
			extentTestAndroid.log(Status.FAIL, "License And Terms of Use page not displayed");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		helpPageAndroid.clickBackButton();
	}

	/**
	 * Description : This is an Android test method to verify text on Privacy Statement page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA176_ValidateTextOnHelpPrivacyStatement(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String text;
		if(brandName.equalsIgnoreCase("Liftmaster")) {
			text = "LiftMaster Privacy Statement";
		} else {
			text = "Chamberlain Privacy Statement";
		}
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		helpPageAndroid = new HelpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.clickMenuLink("Help");
		//validate help for privacy statement
		helpPageAndroid.clickOnHelpLink("Privacy Statement");
		if (helpPageAndroid.verifyTextOnPage(text)) {
			extentTestAndroid.log(Status.PASS, "Privacy Statement page is displayed ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Privacy Statement page not displayed");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		helpPageAndroid.clickBackButton();
	}

	/**
	 * Description :This is an Android test method to verify text on Legal Disclaimer page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA178_ValidateTextOnHelpLegalDisclaimer(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String text = "Legal Disclaimer";
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		helpPageAndroid = new HelpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		menuPageAndroid.clickMenuLink("Help");
		//validate help for Legal Disclaimer
		helpPageAndroid.clickOnHelpLink("Legal Disclaimer");
		if (helpPageAndroid.verifyTextOnPage(text)) {
			extentTestAndroid.log(Status.PASS, "Legal Disclaimer page is displayed ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Legal Disclaimer page not displayed");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		helpPageAndroid.clickBackButton();
	}

	/**
	 * Description : This is an Android test method that click's on the forgot password link on login page, enters the email id as passed in excel sheet 
	 * verifies the confirmation pop-up present  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA31_ValidateForgotPassword(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String emailAddress;
		emailAddress = testData.get(0).split(",")[0];
		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		loginPageAndroid.clickForgotPassword()
		.verifyForgotPasswordPopup();
		if(loginPageAndroid.verifyEditEmailInForgetPassword()){
			extentTestAndroid.log(Status.PASS, "Email field in the forget password popup is editable");
		} else {
			extentTestAndroid.log(Status.FAIL, "Email field in the forget password popup is not editable");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		loginPageAndroid.clickCancelButton();

		//verify forgot password when email id is entered before clicking on forget passwrod button
		loginPageAndroid.enterAccountEmailId(emailAddress)
		.clickForgotPassword();
		if(loginPageAndroid.verifyEnteredEmail(emailAddress)){
			extentTestAndroid.log(Status.PASS, "Email id in the forget password popup and entered user's email id are same");
			loginPageAndroid.clickOkButton();
			if(loginPageAndroid.verifyEmailConfimation(emailAddress)){
				extentTestAndroid.log(Status.PASS, "Validate Forgot Password");
			} else {
				extentTestAndroid.log(Status.FAIL, " Validate Forgot Password");
				utilityAndroid.captureScreenshot(androidDriver);
			}
			loginPageAndroid.clickOkButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "Email id in the forget password popup and entered user's email id are not same");
			utilityAndroid.captureScreenshot(androidDriver);
			loginPageAndroid.clickCancelButton();
		}
	}

	/**
	 * Description : This is an Android test method to verify the error messages shown for incorrect login attempts
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA186_ValidateErrorMessagesOnLogin(ArrayList<String> testData, String deviceTest, String scenarioName) { 
		String invalidEmailID;
		String invalidPassword;
		String validEmailID;
		String validPassword;
		String errText = null;

		invalidEmailID = testData.get(0).split(",")[0];
		invalidPassword = testData.get(0).split(",")[1];
		validEmailID = testData.get(0).split(",")[2];
		validPassword = testData.get(0).split(",")[3];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver,extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		loginPageAndroid.selectEnvironment(env)
		.selectFeature(userType);
		// Validating error message when user enters blank email-id and blank password to login
		loginPageAndroid.clearAccountEmailId()
		.clearAccountPassword()
		.clickLoginButton();
		if(loginPageAndroid.verifyErrorPresent()) {
			errText = loginPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("Please enter a valid email address.")) {
				extentTestAndroid.log(Status.PASS, "Error message as expected : "+ errText);
				loginPageAndroid.clickOkButton();
			} else {
				extentTestAndroid.log(Status.FAIL, "Error message validation failed for login with blank credentials :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, "No error pop-up present,while tapping the login button with valid email-id and invalid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Validating error message when user enters invalid email-id and valid password to login
		loginPageAndroid.clearAccountEmailId()
		.enterAccountEmailId(invalidEmailID)
		.clearAccountPassword()
		.enterAccountPassword(validPassword)
		.clickLoginButton();
		if(loginPageAndroid.verifyErrorPresent()) {
			errText = loginPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("Please enter a valid email address.")) {
				extentTestAndroid.log(Status.PASS, "Error messgae as expected " + errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			loginPageAndroid.clickOkButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "No error pop-up present,while tapping the login button with invalid email-id and valid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Validating error message when user enters valid email-id and invalid password to login and verify error code 203
		loginPageAndroid.clearAccountEmailId()
		.enterAccountEmailId(validEmailID)
		.clearAccountPassword()
		.enterAccountPassword(invalidPassword)
		.clickLoginButton();
		if(loginPageAndroid.verifyErrorPresent()) {
			errText = loginPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("The username or password you entered is incorrect. Try again. (203)")) {
				extentTestAndroid.log(Status.PASS, "Error message as expected " + errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			loginPageAndroid.clickOkButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "No error pop-up present,while tapping the login button with valid email-id and invalid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Validating device error code (205) for valid email-id and invalid password
		loginPageAndroid.clearAccountEmailId()
		.enterAccountEmailId(validEmailID)
		.clearAccountPassword()
		.enterAccountPassword(invalidPassword)
		.clickLoginButton()  // 2nd unsuccessful attempt
		.clickOkButton()
		.clickLoginButton()  // 3rd unsuccessful attempt
		.clickOkButton()
		.clickLoginButton()  // 4th unsuccessful attempt
		.clickOkButton()
		.clickLoginButton() // 5th unsuccessful attempt
		.clickOkButton()
		.clickLoginButton() // 6th unsuccessful attempt
		.clickOkButton()
		.clickLoginButton();

		if(loginPageAndroid.verifyErrorPresent()) {
			errText = loginPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("The user will be locked out. (205)")) {
				extentTestAndroid.log(Status.INFO, errText);
				extentTestAndroid.log(Status.PASS, "Verify device error code (205) after 3 unsuccessful attempt, as expected ");
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			loginPageAndroid.clickOkButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "No error pop-up present,while tapping the login button with valid email-id and invalid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Validating device error code (207) for valid email-id and invalid password
		loginPageAndroid.clearAccountEmailId()
		.enterAccountEmailId("myqautomationpreprod9.pune@gmail.com")
		.clearAccountPassword()
		.enterAccountPassword(invalidPassword)
		.clickLoginButton(); // user account was already locked
		if(loginPageAndroid.verifyErrorPresent()) {
			errText = loginPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("The user is locked out. (207)")) {
				extentTestAndroid.log(Status.PASS, "Error messgae as expected " + errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			loginPageAndroid.clickOkButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "No error pop-up present,while tapping the login button with valid email-id and invalid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
	}

	/**
	 * Description : This is an Android test method to verify remember functionality. This method select the remember me radio button on login page, then login to application
	 * close the app , and launch again and verify successful login  
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA22_ValidateRememberMe(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String email;
		String password;

		email = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		loginPageAndroid.selectFeature(userType)
		.clickOnRememberMe()
		.enterAccountEmailId(email)
		.enterAccountPassword(password)
		.clickLoginButton();
		//to close the app
		androidDriver.navigate().back();
		androidDriver.navigate().back();
		// to launch the app
		androidDriver.startActivity(appPackage, appActivity);
		//verify if logged-in 
		if(devicesPageAndroid.verifyLoginPage()){
			extentTestAndroid.log(Status.PASS, "Login with remember me success ");
		} else {
			extentTestAndroid.log(Status.FAIL, "Not logged in with remember me");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		menuPageAndroid.tappingAccountView();
		loginPageAndroid.clickLogout();
	}

	/**
	 * Description : This is an Android test method to Sign up using a random email id. Logs in with the signed up email id and verify the error message
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA19_ValidateSignUp(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String firstName;
		String lastName;
		String password;
		String zipCode;
		String country;
		String language;
		String email;

		firstName = testData.get(0).split(",")[0];
		lastName = testData.get(0).split(",")[1];
		password = testData.get(0).split(",")[2];
		zipCode = testData.get(0).split(",")[3];
		country = testData.get(0).split(",")[4];
		language = testData.get(0).split(",")[5];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		signUpPageAndroid = new SignUpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(loginPageAndroid.verifyEnvTextPresent()) {
			loginPageAndroid.selectFeature(userType);
		}

		signUpPageAndroid.clickSignUpButton()
		.tapOnChooseCountry()
		.selectCountry(country, udid)		
		.enterFirstName(firstName)
		.enterLastName(lastName);
		email = signUpPageAndroid.enterAccountEmail(firstName,lastName);
		signUpPageAndroid.enterCreatePassword(password)
		.enterVerifyPassword(password)
		.enterZipCode(zipCode)
		.selectLanguage(language)
		.clickSubmitButton();

		if(!signUpPageAndroid.verifyErrorPopupPresent()) {
			signUpPageAndroid.clickDoneButton();
			extentTestAndroid.log(Status.PASS, "user profile created. An email has been sent to you. Please check your email inbox to complete the registration process.");
			loginPageAndroid.enterAccountEmailId(email)
			.enterAccountPassword(password)
			.clickLoginButton();
			if(loginPageAndroid.verifyUserNotActivated()) {
				extentTestAndroid.log(Status.PASS, " Pop up displayed for user not activated ");
				if(loginPageAndroid.verifyEmailSent()) {
					extentTestAndroid.log(Status.PASS, " Email Sent ");
				} else {
					extentTestAndroid.log(Status.FAIL, " Email not sent ");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, " Pop up not displayed for sending a new activation email ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, "There was an error on Sign up :- " + signUpPageAndroid.getErrorText());
			utilityAndroid.captureScreenshot(androidDriver);
			signUpPageAndroid.clickOnOkButton()
			.clickBackButton();
			
		}
	}

	/**
	 * Description : This is an Android test method to verify the error messages shown to user on sign-up
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA19_ValidateErrorMessagesOnSignUp(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String currentEmail;
		String errText = null;
		currentEmail = testData.get(0).split(",")[0];
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		signUpPageAndroid = new SignUpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(loginPageAndroid.verifyEnvTextPresent()) {
			loginPageAndroid.selectFeature(userType);
		}
		//error for empty fields
		signUpPageAndroid.clickSignUpButton()
		.tapOnChooseCountry()
		.selectCountry("Australia", udid)		
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equals("Please enter a first and last name.")) {
				extentTestAndroid.log(Status.PASS, errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error displayed : " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		}

		//error for zip code
		signUpPageAndroid.enterFirstName("Test")
		.enterLastName("Error")
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equals("Please enter a zip code.")) {
				extentTestAndroid.log(Status.PASS, errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error displayed : " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		}

		//empty email
		signUpPageAndroid.enterZipCode("12345")
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equals("Please enter your email to login.")) {
				extentTestAndroid.log(Status.PASS, errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error displayed : " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		}

		//incorrect email
		signUpPageAndroid.enterEmail("test@test.co.")
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equals("Please enter a valid email address.")) {
				extentTestAndroid.log(Status.PASS, errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error displayed : " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		}

		//weak password
		signUpPageAndroid.enterEmail("test@test.co")
		.enterCreatePassword("")
		.enterVerifyPassword("")
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equals("Passwords need to be at least 8 characters in length and must contain at least 3 of the following 4 types of characters: uppercase letter, lowercase letter, number or symbol.")) {
				extentTestAndroid.log(Status.PASS, errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error displayed : " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		}

		// password not matching
		signUpPageAndroid.enterCreatePassword("Qwerty123")
		.enterVerifyPassword("Qwerty456")
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equals("Both passwords must match.")) {
				extentTestAndroid.log(Status.PASS, errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error displayed : " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		}

		//email already in use and verify device error code (201)
		signUpPageAndroid.enterEmail(currentEmail)
		.enterCreatePassword("Qwerty123$")
		.enterVerifyPassword("Qwerty123$")
		.clickSubmitButton();
		if(signUpPageAndroid.verifyErrorPopupPresent()) {
			errText = signUpPageAndroid.getErrorText();
			if(errText.equalsIgnoreCase("That username already exists. Check username or use another username. (201)")) {
				extentTestAndroid.log(Status.PASS, "Error message as expected " + errText);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
				utilityAndroid.captureScreenshot(androidDriver);
			}
			signUpPageAndroid.clickOnOkButton();
		} else {
			extentTestAndroid.log(Status.FAIL, "No error pop-up present,while tapping the login button with valid username and invalid password ");
			utilityAndroid.captureScreenshot(androidDriver);
		}
		signUpPageAndroid.clickBackButton();
	}

	/**
	 * Description : This is an Android test method to verify the maximum limit for first name , last name and zip code field on sign up page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA281_ValidateTextInputLimitOnSignUp(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String firstName = "myQautomationmyQautomationmyQautomation";
		String lastName = "synechronmyqsynechronmyqsynechronmyq";
		String zipCode = "1234567890112";
		String textEntered = "";
		extentTestAndroid = createTest(deviceTest , scenarioName ); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		signUpPageAndroid = new SignUpPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(loginPageAndroid.verifyEnvTextPresent()) {
			loginPageAndroid.selectFeature(userType);
		}
		// Verify first name 
		signUpPageAndroid.clickSignUpButton()
		.tapOnChooseCountry()
		.selectCountry("Australia", udid)	
		.enterFirstName(firstName);
		textEntered = signUpPageAndroid.first_name_textbox.getText();
		int lenFirstName = textEntered.length();
		if (lenFirstName == 30) {
			extentTestAndroid.log(Status.PASS, "The First name field accepts only 30 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "First name Text Entered: " + firstName + " ,text retreived after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}

		//Verify Last Name		
		signUpPageAndroid.enterLastName(lastName);
		textEntered = signUpPageAndroid.last_name_textbox.getText();
		int lenLastName = textEntered.length();
		if (lenLastName == 30) {
			extentTestAndroid.log(Status.PASS, "The Last name field accepts only 30 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "Last name Text Entered: " + lastName + " ,text retreived after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}

		// Verify Zipcode		
		signUpPageAndroid.enterZipCode(zipCode);
		textEntered = signUpPageAndroid.zip_code_textbox.getText();
		int lenZipCode = textEntered.length();
		if (lenZipCode == 10) {
			extentTestAndroid.log(Status.PASS, "The Zipcode field accepts only 10 characters.");
		} else {
			extentTestAndroid.log(Status.FAIL, "Zipcode Text Entered: " + zipCode + " ,text retreived after entry: " + textEntered);
			utilityAndroid.captureScreenshot(androidDriver);
		}
		signUpPageAndroid.clickBackButton();
	}

	/**
	 *  Description : This is an Android test method to verify the sensor error for VGDO by triggering API(V2) calls,
	 *  verify the yellow bar below device and error pop-up on tapping the device.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA345_ValidateSensorCommnErrorForVGDO(ArrayList<String> testData, String deviceTest, String scenarioName ) throws Exception {
		String vgdoSerialNumber;
		String vgdoName;
		String errText = null;
		String scheduleName = "SensorErrorSchedule";
		String timeSelected = null;

		vgdoSerialNumber = testData.get(0).split(",")[0];
		vgdoName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(vgdoName, "Close");

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if(devicesPageAndroid.verifyDevicePresent(vgdoName, udid)) {
				//API call  
				webService.enterDPSNoCommunication(vgdoSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				//tap on device and verify the error pop-up
				devicesPageAndroid.tapDeviceForError(vgdoName);
				if(devicesPageAndroid.verifyErrorPresent()) {
					errText = devicesPageAndroid.getErrorText();
					if(errText.equals("The sensor for this garage door is not communicating, so it cannot be controlled.")) {
						extentTestAndroid.log(Status.PASS, "Error pop-up present while tapping the device, as expected -> " + errText);
						devicesPageAndroid.clickOnOkButton();
					} else {
						extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
				}
				//verify sensor error and error mark is displayed
				if(devicesPageAndroid.verifyYellowBarText(vgdoName,"Sensor Error", udid)) {
					extentTestAndroid.log(Status.PASS, "Sensor error present ");
				} else {
					extentTestAndroid.log(Status.FAIL, "Sensor error not present after triggering API call ");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation : Schedule should not get triggered there is sensor error on device
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS,  scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS, "As "+ vgdoName +" is in sensor error mode," + scheduleName+ " did not triggered as expected");	
								} else {
									extentTestAndroid.log(Status.FAIL, scheduleName+ " was not expected to get triggered , as the VGDO "+ vgdoName + " is in sensor error mode");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//API call to exit no communication
				webService.exitDPSNoCommunication(vgdoSerialNumber);

				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user changed the state of VGDO");
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, activeUserRole + vgdoName + " is not present");
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 *  Description	: This is an Android test method to verify the monitor door only error for VGDO by triggering API(V2) calls,
	 *  verify the yellow bar , error text and error pop-up on tapping the device
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA346_ValidateMonitorOnlyErrorForVGDO(ArrayList<String> testData, String deviceTest, String scenarioName ) throws Exception {
		String vgdoSerialNumber;
		String vgdoName;
		String scheduleName = "MonitorErrorSchedule";
		String errText = null;

		vgdoSerialNumber = testData.get(0).split(",")[0];
		vgdoName = testData.get(0).split(",")[1];

		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyDevicePresent(vgdoName, udid)) {
				//API call 
				webService.setDpsMonitorOnly(vgdoSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				//verify Monitor Door Only error 
				devicesPageAndroid.tapDeviceForError(vgdoName);
				if(devicesPageAndroid.verifyErrorPresent()) {
					errText = devicesPageAndroid.getErrorText();
					if(errText.equals("This door is in monitor only mode.")) {
						extentTestAndroid.log(Status.PASS, "Error pop-up present while tapping the device, as expected -> " + errText);
						devicesPageAndroid.clickOnOkButton();
					} else {
						extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
				}
				// check for the yellow bar below the device
				if(devicesPageAndroid.verifyYellowBarText(vgdoName,"Monitor Door Only", udid)) {
					extentTestAndroid.log(Status.PASS, " Monitor Door only error present ");
				} else {
					extentTestAndroid.log(Status.FAIL, "Monitor only error not present after triggering API call ");
					utilityAndroid.captureScreenshot(androidDriver);
				}

				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule()
					.setScheduleName(scheduleName);
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						schedulePageAndroid.clickOnSetDevicesIcon();
						if(!schedulePageAndroid.verifyDevicePresentForSchedule(vgdoName)) {
							extentTestAndroid.log(Status.PASS, vgdoName + " is not present in the device list");
							schedulePageAndroid.clickBackButton()
							.clickSaveButton();
							if(schedulePageAndroid.verifyErrorDispalyed()) {
								errText = schedulePageAndroid.getErrorMsg();
								if(errText.equals("At least one device must be selected.")) {
									extentTestAndroid.log(Status.PASS, "Error pop-up present for device not selected");
								} else {
									extentTestAndroid.log(Status.FAIL, "Different error pop-up present - " + schedulePageAndroid.getErrorMsg());
								}
								schedulePageAndroid.clickOkButton()
								.clickBackButton()
								.clickBackConfirmation();
							} else {
								extentTestAndroid.log(Status.FAIL, "No error present");
							}
						} else {
							extentTestAndroid.log(Status.FAIL, vgdoName + " is present in the device list , when in Monitor oly state");
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//API call to set door to actionable
				webService.setDpsActionableOnly(vgdoSerialNumber);

			} else {
				extentTestAndroid.log(Status.FAIL, vgdoName + " is not present");
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description :  This is an Android test method to bring the GDO to the Closed state, trigger the API(V2)for open obstruction,verify Stopped state, tap on device 
	 * verify the error message appearing after 2 minutes and exit from open obstruction [Edge Case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA327_ValidateGDOOpenObstructionError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String currentStatus;
		String gdoSerialNumber;
		String scheduleName = "ScheduleGDOOpenObstruction";
		String timeSelected = null;
		String deviceStatus = null;

		deviceName = testData.get(0).split(",")[0];
		gdoSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid); 
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("OPEN")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter obstruction  for GDO
				webService.enterOpenObstructionGDO(gdoSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//verify if the device status is Stopped or not
				if (currentStatus.equalsIgnoreCase("STOPPED")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is now in " + currentStatus + " state");
				} else {
					extentTestAndroid.log(Status.FAIL, "Device is not in Stopped state, API trigger was not successfull");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS,  scheduleName + " is created ");
								//navigate to devices page
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									//wait for duration which is set in schedule
									schedulePageAndroid.waitForNotification(timeSelected);
									//check for notification 
									if (schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
										extentTestAndroid.log(Status.PASS, "Schedule Notification received ");	
									} else {
										extentTestAndroid.log(Status.FAIL, scheduleName + " Schedule Notification not received ");	
									}
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.toUpperCase().contains("CLOSED")) {
										extentTestAndroid.log(Status.PASS, deviceName + " status is - "+ deviceStatus);	
									} else {
										extentTestAndroid.log(Status.FAIL, "Status not changed. " + deviceName+ " status now is - "+ deviceStatus );
										utilityAndroid.captureScreenshot(androidDriver);
									}
									//verify history
									if(activeUserRole.equalsIgnoreCase("Guest")) {
										if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
											extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have access to History");
										}else {
											extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has access to History");
											utilityAndroid.captureScreenshot(androidDriver);
										}
									} else {
										menuPageAndroid.clickMenuLink("History");
										if(historyPageAndroid.verifyHistory(timeSelected, scheduleName, " was triggered")) {
											extentTestAndroid.log(Status.PASS, "History found for "+ scheduleName );
										} else {
											extentTestAndroid.log(Status.FAIL, "History not  found for "+ scheduleName + " for time " + timeSelected );
											utilityAndroid.captureScreenshot(androidDriver);
										}
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								menuPageAndroid.clickMenuLink("Schedules");
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL,  scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from open obstruction
				webService.exitOpenObstructionGDO(gdoSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("OPEN")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitOpenObstructionGDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitOpenObstructionGDO API Call is triggered");
					webService.exitOpenObstructionGDO(gdoSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to bring the CDO to the Closed state, trigger the API(V2)for open obstruction,verify Stopped state, tap on device 
	 * verify the error message appearing after 2 minutes and exit from open obstruction [Edge Case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA328_ValidateCDOOpenObstructionError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String currentStatus;
		String cdoSerialNumber;
		String scheduleName = "ScheduleCDOOpenObstruction";
		String timeSelected = null;
		String deviceStatus = null;

		deviceName = testData.get(0).split(",")[0];
		cdoSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("OPEN")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter obstruction  for CDO
				webService.enterOpenObstructionCDO(cdoSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//verify if the device status is Stopped or not
				if (currentStatus.equalsIgnoreCase("STOPPED")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is now in " + currentStatus + " state");
				} else {
					extentTestAndroid.log(Status.FAIL, "Device is not in Stopped state, API trigger was not successfull");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//navigate to devices page
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									//wait for duration which is set in schedule
									schedulePageAndroid.waitForNotification(timeSelected);
									//check for notification 
									if (schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
										extentTestAndroid.log(Status.PASS, "Schedule Notification received ");	
									} else {
										extentTestAndroid.log(Status.FAIL, scheduleName + " Schedule Notification not received ");	
									}
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.toUpperCase().contains("CLOSED")) {
										extentTestAndroid.log(Status.PASS,  deviceName + " status is - "+ deviceStatus);	
									} else {
										extentTestAndroid.log(Status.FAIL, "Status not changed. For " + deviceName+ " status now is - "+ deviceStatus );
										utilityAndroid.captureScreenshot(androidDriver);
									}
									//verify history
									if(activeUserRole.equalsIgnoreCase("Guest")) {
										if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
											extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
										}else {
											extentTestAndroid.log(Status.FAIL, activeUserRole + " user has access to History");
											utilityAndroid.captureScreenshot(androidDriver);
										}
									} else {
										menuPageAndroid.clickMenuLink("History");
										if(historyPageAndroid.verifyHistory(timeSelected, scheduleName, " was triggered")) {
											extentTestAndroid.log(Status.PASS, "History found for "+ scheduleName );
										} else {
											extentTestAndroid.log(Status.FAIL, "History not  found for "+ scheduleName + " for time " + timeSelected );
											utilityAndroid.captureScreenshot(androidDriver);
										}
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								menuPageAndroid.clickMenuLink("Schedules");
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from open obstruction
				webService.exitOpenObstructionCDO(cdoSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("OPEN")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitOpenObstructionCDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitOpenObstructionCDO API Call is triggered");
					webService.exitOpenObstructionCDO(cdoSerialNumber);
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to bring the Gate to the Closed state, trigger the API(V2)for open obstruction,verify Stopped state, tap on device 
	 * verify the error message appearing after 2 minutes and exit from open obstruction [Edge Case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA305_ValidateGateOpenObstructionError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String currentStatus;
		String gateSerialNumber;
		String scheduleName = "ScheduleGateOpenObstruction";
		String timeSelected = null;
		String deviceStatus = null;

		deviceName = testData.get(0).split(",")[0];
		gateSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("OPEN")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter obstruction  for Gate
				webService.enterOpenObstructionGate(gateSerialNumber);
				//just to refresh the page
				menuPageAndroid.clickHamburger().clickHamburger();
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//verify if the device status is Stopped or not
				if (currentStatus.equalsIgnoreCase("STOPPED")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is now in " + currentStatus + " state");
				} else {
					extentTestAndroid.log(Status.FAIL, "Device is not in Stopped state, API trigger was not successfull");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//navigate to devices page
								menuPageAndroid.clickMenuLink("Devices");
								if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
									//wait for duration which is set in schedule
									schedulePageAndroid.waitForNotification(timeSelected);
									//check for notification 
									if (schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
										extentTestAndroid.log(Status.PASS, "Schedule Notification received ");	
									} else {
										extentTestAndroid.log(Status.FAIL, scheduleName + " Schedule Notification not received ");	
									}
									deviceStatus = devicesPageAndroid.getDeviceStatus(deviceName);
									if (deviceStatus.toUpperCase().contains("CLOSED")) {
										extentTestAndroid.log(Status.PASS, deviceName + " status is - "+ deviceStatus);	
									} else {
										extentTestAndroid.log(Status.FAIL, "Status not changed. For " + deviceName+ " status now is - "+ deviceStatus );
										utilityAndroid.captureScreenshot(androidDriver);
									}
									//verify history
									if(activeUserRole.equalsIgnoreCase("Guest")) {
										if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
											extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
										}else {
											extentTestAndroid.log(Status.FAIL, activeUserRole + " user has access to History");
											utilityAndroid.captureScreenshot(androidDriver);
										}
									} else {
										menuPageAndroid.clickMenuLink("History");
										if(historyPageAndroid.verifyHistory(timeSelected, scheduleName, " was triggered")) {
											extentTestAndroid.log(Status.PASS, "History found for "+ scheduleName );
										} else {
											extentTestAndroid.log(Status.FAIL, "History not  found for "+ scheduleName + " for time " + timeSelected );
											utilityAndroid.captureScreenshot(androidDriver);
										}
									}
								} else {
									extentTestAndroid.log(Status.FAIL, "Expected device is not present");
									utilityAndroid.captureScreenshot(androidDriver);
								}
								menuPageAndroid.clickMenuLink("Schedules");
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from open obstruction
				webService.exitOpenObstructionGate(gateSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("OPEN")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitOpenObstructionGate API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitOpenObstructionGate API Call is triggered");
					webService.exitOpenObstructionGate(gateSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to bring GDO to Open state, trigger API(V2) for close obstruction, verify yellow bar and text present on it,
	 * exclamation  mark on device, tap on device and verify the error message, exit from Close obstruction [Edge case]
	 * @param testData
	 * @param deviceTest 
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA443_ValidateGDOCloseObstructionError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gdoSerialNumber;
		String currentStatus;
		String errText = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		String tempText;
		String scheduleName = "ScheduleGDOCloseObstruction";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		gdoSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//if device is in Closed state, tap on it to change the state to Open
				if (currentStatus.equalsIgnoreCase("Closed")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter close obstruction for GDO
				webService.enterCloseObstructionGDO(gdoSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify the warning symbol on device
				if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, " Warning sybmol on device is present");
					devicesPageAndroid.tapDeviceForError(deviceName);
					//verify the error pop-up displayed after tapping on device
					if(devicesPageAndroid.verifyErrorPresent()) {
						tempText = devicesPageAndroid.getErrorText();
						//compare the error text
						if (tempText.equalsIgnoreCase(errText)) {
							extentTestAndroid.log(Status.PASS," Error text as expected :- " +tempText);
						} else {
							extentTestAndroid.log(Status.FAIL," Expected error text is not displayed :- " +tempText);
							utilityAndroid.captureScreenshot(androidDriver);
						}
						devicesPageAndroid.clickOnOkButton();
					} else {
						extentTestAndroid.log(Status.FAIL, " Error pop-up not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, " Error warning symbol on device is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//verify error label below device icon
				if(devicesPageAndroid.verifyYellowBarText(deviceName, "Close Error", udid)) {
					extentTestAndroid.log(Status.PASS, " Error label below device icon is present");
				} else {
					extentTestAndroid.log(Status.FAIL, " Error label below device icon is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation : Schedule should not get triggered when device is in error state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS,  deviceName +" is in error mode, " + scheduleName+ " did not triggered as expected");	
								} else {
									extentTestAndroid.log(Status.FAIL,  scheduleName+ " was not expected to get triggered , as the  "+ deviceName + " is in close obstruction");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from close obstruction
				webService.exitCloseObstructionGDO(gdoSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify the warning symbol on device
				if (!devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitCloseObstructionGDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitAcLexitCloseObstructionGDOossForGate API Call is triggered");
					webService.exitCloseObstructionGDO(gdoSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to bring CDO to Open state, trigger API(V2) for close obstruction, verify yellow bar and text present on it,
	 * exclamation  mark on device, tap on device and verify the error message, exit from Close obstruction [Edge case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA449_ValidateCDOCloseObstructionError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String cdoSerialNumber;
		String currentStatus;
		String errText = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		String tempText;
		String scheduleName = "ScheduleCDOCloseObstruction";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		cdoSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//if device is in Closed state, tap on it to change the state to Open
				if (currentStatus.equalsIgnoreCase("Closed")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter close obstruction for CDO
				webService.enterCloseObstructionCDO(cdoSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				//verify the warning symbol on device
				if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, "Warning sybmol on device is present");
					devicesPageAndroid.tapDeviceForError(deviceName);
					//verify the error pop-up displayed after tapping on device
					if(devicesPageAndroid.verifyErrorPresent()) {
						tempText = devicesPageAndroid.getErrorText();
						//compare the error text
						if (tempText.equalsIgnoreCase(errText)) {
							extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
						} else {
							extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
							utilityAndroid.captureScreenshot(androidDriver);
						}
						devicesPageAndroid.clickOnOkButton();
					} else {
						extentTestAndroid.log(Status.FAIL, "Error pop-up not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Error warning symbol on device is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//verify error label below device icon
				if(devicesPageAndroid.verifyYellowBarText(deviceName, "Close Error", udid)) {
					extentTestAndroid.log(Status.PASS, "Error label below device icon is present");
				} else {
					extentTestAndroid.log(Status.FAIL, "Error label below device icon is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation : Schedule should not get triggered when device is in error state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS, deviceName +" is in error mode, " + scheduleName+ " did not triggered as expected");	
								} else {
									extentTestAndroid.log(Status.FAIL,  scheduleName+ " was not expected to get triggered , as the  "+ deviceName + " is in close obstruction");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, " Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from close obstruction
				webService.exitCloseObstructionCDO(cdoSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify the warning symbol on device
				if (!devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitCloseObstructionCDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitCloseObstructionCDO API Call is triggered");
					webService.exitCloseObstructionCDO(cdoSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Test MTA-449 :- Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, "Test MTA-449 :- " + activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to bring the Gate to the Open state, trigger the API(V2)for close obstruction, tap on device 
	 * verify the error message appearing after 2 minutes and exit from close obstruction [Edge Case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA459_ValidateGateCloseObstructionError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gateSerialNumber;
		String currentStatus;
		String tempText;
		String scheduleName = "ScheduleGateCloseObstruction";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		gateSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");
		String errText = deviceName.concat(" is not responding. Please try again.");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//if device is in Closed state, tap on it to change the state to Open
				if (currentStatus.equalsIgnoreCase("Closed")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter close obstruction for Gate
				webService.enterCloseObstructionGate(gateSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				devicesPageAndroid.tapDevice(deviceName);
				//verify the error pop-up displayed after tapping on device
				if(devicesPageAndroid.verifyErrorForDevice()) {
					tempText = devicesPageAndroid.getErrorText();
					//compare the error text
					if (tempText.equalsIgnoreCase(errText)) {
						extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
					} else {
						extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
					devicesPageAndroid.clickOnOkButton();
				} else {
					extentTestAndroid.log(Status.FAIL, "Error pop-up not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation : Schedule should not get triggered when device is in error state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS, "Schedule notification received for " + deviceName);
									menuPageAndroid.verifyAndClickMenuLink("Devices");
									if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
										currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
										//if device is in Closed state, tap on it to change the state to Open
										if (currentStatus.equalsIgnoreCase("Open")) {
											extentTestAndroid.log(Status.PASS, "Device state not changed. Cuurent state :-" + currentStatus);	
										} else {
											extentTestAndroid.log(Status.FAIL,  "Device was not expected to change the state. Current state -" + currentStatus);	
										}
									}
									menuPageAndroid.clickMenuLink("Schedules");
								} else {
									extentTestAndroid.log(Status.FAIL,  scheduleName +" was expected to trigger but did not get triggered.");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, " Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from close obstruction
				webService.exitCloseObstructionGate(gateSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify the warning symbol on device
				if (!devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitCloseObstructionGate API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitCloseObstructionGate API Call is triggered");
					webService.exitCloseObstructionGate(gateSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
			}
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify yellow label, error text present on it , exclamation on device icon
	 * error pop-up when user tap on GDO after triggering the API(V2) for Photoeye, exit from Photoeye api[Edge case] 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA445_ValidateGDOPhotoEyeBlockError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gdoSerialNumber;
		String currentStatus;
		String tempText;
		String errText = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		String state1;
		String scheduleName = "ScheduleGDOPhotoEye";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		gdoSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//if device is in Closed state, tap on it to change the state to Open
				if (currentStatus.equalsIgnoreCase("Closed")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice for block photo eye
				webService.blockPhotoEyeGDO(gdoSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				//verify the warning symbol on device
				if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, "Warning sybmol on device is present");
					devicesPageAndroid.tapDeviceForError(deviceName);
					//verify the error pop-up displayed after tapping on device
					if(devicesPageAndroid.verifyErrorPresent()) {
						tempText = devicesPageAndroid.getErrorText();
						//compare the error text
						if (tempText.equalsIgnoreCase(errText)) {
							extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
						} else {
							extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
							utilityAndroid.captureScreenshot(androidDriver);
						}
						devicesPageAndroid.clickOnOkButton();
					} else {
						extentTestAndroid.log(Status.FAIL, "Error pop-up not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Error warning symbol on device is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//verify error label below device icon
				if(devicesPageAndroid.verifyYellowBarText(deviceName, "Close Error", udid)) {
					extentTestAndroid.log(Status.PASS, "Error label below device icon is present");
				} else {
					extentTestAndroid.log(Status.FAIL, "Error label below device icon is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation : Schedule should not get triggered when device is in error state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS, deviceName +" is in error mode, " + scheduleName+ " did not triggered as expected");	
								} else {
									extentTestAndroid.log(Status.FAIL,  scheduleName+ " was not expected to get triggered , as the  "+ deviceName + " is in Photo eye error mode");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, " Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit photo eye block
				webService.unblockPhotoEyeGDO(gdoSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify the warning symbol on device
				if (!devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after unblockPhotoEyeGDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after unblockPhotoEyeGDO API Call is triggered");
					webService.unblockPhotoEyeGDO(gdoSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Test MTA-459 :- Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify yellow label, error text present on it , exclamation on device icon
	 * error pop-up when user tap on CDO after triggering the API(V2) for Photoeye, exit from Photoeye api [Edge case] 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA451_ValidateCDOPhotoEyeBlockError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String cdoSerialNumber;
		String currentStatus;
		String tempText;
		String errText = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		String scheduleName = "ScheduleCDOPhotoEye";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		cdoSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//if device is in Closed state, tap on it to change the state to Open
				if (currentStatus.equalsIgnoreCase("Closed")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice for block photo eye
				webService.blockPhotoEyeCDO(cdoSerialNumber);
				menuPageAndroid.clickHamburger();
				menuPageAndroid.clickHamburger();
				//verify error label below device icon
				if(devicesPageAndroid.verifyYellowBarText(deviceName, "Close Error", udid)) {
					extentTestAndroid.log(Status.PASS, "Error label below device icon is present");
					//verify the warning symbol on device
					if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
						extentTestAndroid.log(Status.PASS, "Warning sybmol on device is present");
						devicesPageAndroid.tapDeviceForError(deviceName);
						//verify the error pop-up displayed after tapping on device
						if(devicesPageAndroid.verifyErrorPresent()) {
							tempText = devicesPageAndroid.getErrorText();
							//compare the error text
							if (tempText.equalsIgnoreCase(errText)) {
								extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
								devicesPageAndroid.clickOnOkButton();
							} else {
								extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error pop-up not present");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Error warning symbol on device is not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Error label below device icon is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}

				//MTA-487 Implementation : Schedule should not get triggered when device is in error state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS, deviceName +" is in error mode, " + scheduleName+ " did not triggered as expected");	
								} else {
									extentTestAndroid.log(Status.FAIL, scheduleName+ " was not expected to get triggered , as the  "+ deviceName + " is in Photo eye error mode");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL,scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit block photo eye
				webService.unblockPhotoEyeCDO(cdoSerialNumber);
				menuPageAndroid.clickMenuLink("Devices");
				//verify the warning symbol on device
				if (!devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after unblockPhotoEyeCDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after unblockPhotoEyeCDO API Call is triggered");
					webService.unblockPhotoEyeCDO(cdoSerialNumber);
				}
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the error pop-up when user tap on GDO after triggering the API(V2) for ACLoss [Edge case] 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA447_ValidateGDOACLossError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gdoSerialNumber;
		String tempText;
		String currentStatus1;
		String currentStatus2;

		deviceName = testData.get(0).split(",")[0];
		gdoSerialNumber = testData.get(0).split(",")[1];
		String errText = deviceName.concat(" is not responding. Please try again.");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//webservice for AC Loss
				webService.enterAcLossForGDO(gdoSerialNumber);
				menuPageAndroid.clickHamburger();
				menuPageAndroid.clickHamburger();
				devicesPageAndroid.tapDevice(deviceName);
				//verify the error pop-up displayed after tapping on device
				if(devicesPageAndroid.verifyErrorForDevice()) {
					tempText = devicesPageAndroid.getErrorText();
					//compare the error text
					if (tempText.equalsIgnoreCase(errText)) {
						extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
					} else {
						extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
					devicesPageAndroid.clickOnOkButton();
				} else {
					extentTestAndroid.log(Status.FAIL, "Expected error pop-up not present");
					utilityAndroid.captureScreenshot(androidDriver);
					if(devicesPageAndroid.verifyErrorPresent()) {
						extentTestAndroid.log(Status.INFO,devicesPageAndroid.getErrorText());
						devicesPageAndroid.clickOnOkButton();
					}
				}
				//exit from AC Loss
				webService.exitAcLossForGDO(gdoSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				devicesPageAndroid.verifyDevicePresent(deviceName, udid);
				currentStatus1 = devicesPageAndroid.getDeviceStatus(deviceName);
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus2 = devicesPageAndroid.getDeviceStatus(deviceName);
				if (!currentStatus1.equalsIgnoreCase(currentStatus2)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitAcLossForGDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitAcLossForGDO API Call is triggered");
					webService.exitAcLossForGDO(gdoSerialNumber);
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the error pop-up when user tap on CDO after triggering the API(V2) for ACLoss [Edge case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA453_ValidateCDOACLossError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String cdoSerialNumber;
		String tempText;
		String currentStatus1;
		String currentStatus2;

		deviceName = testData.get(0).split(",")[0];
		cdoSerialNumber = testData.get(0).split(",")[1];
		String errText = deviceName.concat(" is not responding. Please try again.");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//webservice for AC Loss
				webService.enterAcLossForCDO(cdoSerialNumber);
				menuPageAndroid.clickHamburger();
				menuPageAndroid.clickHamburger();
				devicesPageAndroid.tapDevice(deviceName);
				//verify the error pop-up displayed after tapping on device
				if(devicesPageAndroid.verifyErrorForDevice()) {
					tempText = devicesPageAndroid.getErrorText();
					//compare the error text
					if (tempText.equalsIgnoreCase(errText)) {
						extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
					} else {
						extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
					devicesPageAndroid.clickOnOkButton();
				} else {
					extentTestAndroid.log(Status.FAIL, "Expected error pop-up not present");
					utilityAndroid.captureScreenshot(androidDriver);
					if(devicesPageAndroid.verifyErrorPresent()) {
						extentTestAndroid.log(Status.INFO,devicesPageAndroid.getErrorText());
						devicesPageAndroid.clickOnOkButton();
					}
				}
				//exit from AC loss
				webService.exitAcLossForCDO(cdoSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				devicesPageAndroid.verifyDevicePresent(deviceName, udid);
				currentStatus1 = devicesPageAndroid.getDeviceStatus(deviceName);
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus2 = devicesPageAndroid.getDeviceStatus(deviceName);
				if (!currentStatus1.equalsIgnoreCase(currentStatus2)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitAcLossForCDO API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitAcLossForCDO API Call is triggered");
					webService.exitAcLossForCDO(cdoSerialNumber);
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the error pop-up when user tap on Gate after triggering the API(V2) for ACLoss [Edge case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA461_ValidateGateACLossError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gateSerialNumber;
		String tempText;
		String currentStatus1;
		String currentStatus2;

		deviceName = testData.get(0).split(",")[0];
		gateSerialNumber = testData.get(0).split(",")[1];
		String errText = deviceName.concat(" is not responding. Please try again.");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//webservice for AC Loss
				webService.enterAcLossForGate(gateSerialNumber);
				menuPageAndroid.clickHamburger();
				menuPageAndroid.clickHamburger();
				devicesPageAndroid.tapDevice(deviceName);
				//verify the error pop-up displayed after tapping on device
				if(devicesPageAndroid.verifyErrorForDevice()) {
					tempText = devicesPageAndroid.getErrorText();
					//compare the error text
					if (tempText.equalsIgnoreCase(errText)) {
						extentTestAndroid.log(Status.PASS," Error text as expected :- " +tempText);
					} else {
						extentTestAndroid.log(Status.FAIL," Expected error text is not displayed :- " +tempText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
					devicesPageAndroid.clickOnOkButton();
				} else {
					extentTestAndroid.log(Status.FAIL, " Expected error pop-up not present");
					utilityAndroid.captureScreenshot(androidDriver);
					if(devicesPageAndroid.verifyErrorPresent()) {
						extentTestAndroid.log(Status.INFO,devicesPageAndroid.getErrorText());
						devicesPageAndroid.clickOnOkButton();
					}
				}
				//exit from Ac loss
				webService.exitAcLossForGate(gateSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				devicesPageAndroid.verifyDevicePresent(deviceName, udid);
				currentStatus1 = devicesPageAndroid.getDeviceStatus(deviceName);
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus2 = devicesPageAndroid.getDeviceStatus(deviceName);
				if (!currentStatus1.equalsIgnoreCase(currentStatus2)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitAcLossForGate API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitAcLossForGate API Call is triggered");
					webService.exitAcLossForGate(gateSerialNumber);
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
			}
		} else {
			extentTestAndroid.log(Status.FAIL,   activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the error pop-up when user tap on light after triggering the API(V2) for ACLoss[Edge case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA455_ValidateLightACLossError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String lightSerialNumber;
		String tempText;
		String currentStatus1;
		String currentStatus2;

		deviceName = testData.get(0).split(",")[0];
		lightSerialNumber = testData.get(0).split(",")[1];
		String errText = deviceName.concat(" is not responding. Please try again.");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//web service for AC Loss
				webService.enterAcLossForLight(lightSerialNumber);
				menuPageAndroid.clickHamburger();
				menuPageAndroid.clickHamburger();
				devicesPageAndroid.tapDevice(deviceName);
				//verify the error pop-up displayed after tapping on device
				if(devicesPageAndroid.verifyErrorForDevice()) {
					tempText = devicesPageAndroid.getErrorText();
					//compare the error text
					if (tempText.equalsIgnoreCase(errText)) {
						extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
					} else {
						extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
						utilityAndroid.captureScreenshot(androidDriver);
					}
					devicesPageAndroid.clickOnOkButton();
				} else {
					extentTestAndroid.log(Status.FAIL, "Expected error pop-up not present");
					utilityAndroid.captureScreenshot(androidDriver);
					if(devicesPageAndroid.verifyErrorPresent()) {
						extentTestAndroid.log(Status.INFO,devicesPageAndroid.getErrorText());
						devicesPageAndroid.clickOnOkButton();
					}
				}
				//exit from Ac loss
				webService.exitAcLossForLight(lightSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				devicesPageAndroid.verifyDevicePresent(deviceName, udid);
				currentStatus1 = devicesPageAndroid.getDeviceStatus(deviceName);
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus2 = devicesPageAndroid.getDeviceStatus(deviceName);
				if (!currentStatus1.equalsIgnoreCase(currentStatus2)) {
					extentTestAndroid.log(Status.PASS, deviceName + " is in healthy state, after exitAcLossForLight API Call is triggered");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is in error state, after exitAcLossForLight API Call is triggered");
					webService.exitAcLossForLight(lightSerialNumber);
				}
			} else {
				if(activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestAndroid.log(Status.PASS, "Light is not visible to " + activeUserRole + " user");
				} else {
					extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL,   activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify the yellow bar , offline text , error pop and trouble-shooting text upon clicking on bar
	 * when the Gateway is offline after triggering Gateway AC Loss API(V2) [Edge case]
	 * @param testData
	 * @param deviceTest
	 * @throws Exception
	 */
	public void MTA457_ValidateGatewayACLossError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gatewaySerialNumber;
		String tempText;
		String errText = "The device is offline. Please check the power and network connections. (309)";
		String scheduleName = "ScheduleAcLoss";
		String state;
		String timeSelected = null;
		String currentStatus1;
		String currentStatus2;

		deviceName = testData.get(0).split(",")[0];
		gatewaySerialNumber = testData.get(0).split(",")[1];
		state = testData.get(0).split(",")[2];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Devices")) {
				if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
					//webservice to send gateway offline
					webService.SendGatewayOfflineService(gatewaySerialNumber);
					menuPageAndroid.clickHamburger().clickHamburger();
					//verify the warning symbol on device
					if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
						extentTestAndroid.log(Status.PASS, "Warning sybmol on device is present");
						devicesPageAndroid.tapDeviceForError(deviceName);
						//verify the error pop-up displayed after tapping on device
						if(devicesPageAndroid.verifyErrorPresent()) {
							tempText = devicesPageAndroid.getErrorText();
							//compare the error text
							if (tempText.equalsIgnoreCase(errText)) {
								extentTestAndroid.log(Status.PASS,"Error text as expected :- " +tempText);
							} else {
								extentTestAndroid.log(Status.FAIL,"Expected error text is not displayed :- " +tempText);
								utilityAndroid.captureScreenshot(androidDriver);
							}
							devicesPageAndroid.clickOnOkButton();
						} else {
							extentTestAndroid.log(Status.FAIL, "Error pop-up not present");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Error warning symbol on device is not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
					//verify error label below device icon
					if(devicesPageAndroid.verifyYellowBarText(deviceName, "Offline", udid)) {
						extentTestAndroid.log(Status.PASS, "Error label below device icon is present");
						devicesPageAndroid.tapOnErrorLabel(deviceName);
						if(devicesPageAndroid.verifyTroubleShootingText()) {
							extentTestAndroid.log(Status.PASS, "MyQ Troubleshooting text is displayed");
							devicesPageAndroid.clickBackButton();
						} else {
							extentTestAndroid.log(Status.FAIL, "MyQ Troubleshooting text is not displayed");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, "Error label below device icon is not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
					if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
						schedulePageAndroid.addSchedule();
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
							if(!schedulePageAndroid.verifyErrorDispalyed()) {
								if (schedulePageAndroid.verifySchedule(scheduleName)) {
									extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
									//wait for duration which is set in schedule
									schedulePageAndroid.waitForNotification(timeSelected);
									//check for notification 
									if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
										extentTestAndroid.log(Status.PASS, " As the parent gateway"+ gatewaySerialNumber +" was offline," + scheduleName+ " did not triggered as expected");	
									} else {
										extentTestAndroid.log(Status.FAIL,  scheduleName+ " was not expected to get triggered , as the parent gateway" + gatewaySerialNumber +" was offline");	
									}
									schedulePageAndroid.deleteSchedule(scheduleName);
								} else {
									extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
									utilityAndroid.captureScreenshot(androidDriver);
								}
							} else {
								extentTestAndroid.log(Status.FAIL, " Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
								utilityAndroid.captureScreenshot(androidDriver);
								schedulePageAndroid.clickOkButton()
								.clickBackButton()
								.clickBackConfirmation();
							}
						} else {
							extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickLaterButton();
						}
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
							extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
						}
					}
					//exit from gateway offline
					webService.SendGatewayOnlineService(gatewaySerialNumber);
					menuPageAndroid.verifyAndClickMenuLink("Devices");
					devicesPageAndroid.verifyDevicePresent(deviceName, udid);
					currentStatus1 = devicesPageAndroid.getDeviceStatus(deviceName);
					devicesPageAndroid.deviceActuation(deviceName);
					currentStatus2 = devicesPageAndroid.getDeviceStatus(deviceName);
					if (!currentStatus1.equalsIgnoreCase(currentStatus2)) {
						extentTestAndroid.log(Status.PASS, deviceName + " is now in healthy state ,after SendGatewayOnlineService API Call");
					} else {
						extentTestAndroid.log(Status.FAIL, deviceName + " status not changed, not in healthy state.");
						webService.SendGatewayOnlineService(gatewaySerialNumber);
					}
				} else {
					if(activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, "Light is not visible to " + activeUserRole + " user");
					} else {
						extentTestAndroid.log(Status.FAIL, "Expected Device not present ");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				}
			} else { 
				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to control devices");
				}
			} 
		} else {
			extentTestAndroid.log(Status.FAIL,  activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify copy right text on login page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 */
	public void MTA528_VerifyCopyRightText(ArrayList<String> testData, String deviceTest, String scenarioName){
		String copyRightText ;
		String text;
		copyRightText = "� 2018 The Chamberlain Group, Inc. All Rights Reserved.";
		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if(loginPageAndroid.isCopyRightTextPresent()) {
			text = loginPageAndroid.getCopyRightText();
			if(text.equals(copyRightText)) {
				extentTestAndroid.log(Status.PASS, "Copy right text is present on Login page as " + text);
			} else {
				extentTestAndroid.log(Status.FAIL, "Different text present - " +text );
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, "Copyright text is not displayed on login page" );
			utilityAndroid.captureScreenshot(androidDriver);
		}
	}

	/**
	 * Description : This is an Android test method to bring the Gate to the Closed state, trigger the API(V2) for Gate double entrapment, Verify Gate should be in STOPPED State,
	 * Verify warning symbol and error label below device, tap on device and verify error , create schedule and verify Schedule not triggered as PASS criteria
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA526_ValidateGateDoubleEntrapmentError(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String gateSerialNumber;
		String currentStatus;
		String errText = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		String tempText;
		String scheduleName = "ScheduleGateDoubleEntrapment";
		String timeSelected = null;

		deviceName = testData.get(0).split(",")[0];
		gateSerialNumber = testData.get(0).split(",")[1];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, "Close");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//if device is in Closed state, tap on it to change the state to Open
				if (currentStatus.equalsIgnoreCase("Open")) {
					devicesPageAndroid.deviceActuation(deviceName);
				} 
				//webservice to enter close obstruction for GDO
				webService.enterDoubleEntrapmentGate(gateSerialNumber);
				menuPageAndroid.clickHamburger().clickHamburger();
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				//verify if the device status is Stopped or not
				if (currentStatus.equalsIgnoreCase("STOPPED")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is now in " + currentStatus + " state");
				} else {
					extentTestAndroid.log(Status.FAIL, "Device is not in Stopped state, API trigger was not successfull");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//verify the warning symbol on device
				if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, " Warning sybmol on device is present");
					devicesPageAndroid.tapDeviceForError(deviceName);
					//verify the error pop-up displayed after tapping on device
					if(devicesPageAndroid.verifyErrorPresent()) {
						tempText = devicesPageAndroid.getErrorText();
						//compare the error text
						if (tempText.equalsIgnoreCase(errText)) {
							extentTestAndroid.log(Status.PASS," Error text as expected :- " +tempText);
						} else {
							extentTestAndroid.log(Status.FAIL," Expected error text is not displayed :- " +tempText);
							utilityAndroid.captureScreenshot(androidDriver);
						}
						devicesPageAndroid.clickOnOkButton();
					} else {
						extentTestAndroid.log(Status.FAIL, " Error pop-up not present");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, " Error warning symbol on device is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//verify error label below device icon
				if(devicesPageAndroid.verifyYellowBarText(deviceName, "Close Error", udid)) {
					extentTestAndroid.log(Status.PASS, " Error label below device icon is present");
				} else {
					extentTestAndroid.log(Status.FAIL, " Error label below device icon is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-487 Implementation : Schedule should not get triggered when device is in error state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(!schedulePageAndroid.verifyErrorDispalyed()) {
							if (schedulePageAndroid.verifySchedule(scheduleName)) {
								extentTestAndroid.log(Status.PASS, scheduleName + " is created ");
								//wait for duration which is set in schedule
								schedulePageAndroid.waitForNotification(timeSelected);
								//check for notification 
								if (!schedulePageAndroid.checkPushNotificationReceived(scheduleName, udid)) {
									extentTestAndroid.log(Status.PASS,  deviceName +" is in error mode, " + scheduleName+ " did not triggered as expected");	
								} else {
									extentTestAndroid.log(Status.FAIL,  scheduleName+ " was not expected to get triggered , as the  "+ deviceName + " is in close obstruction");	
								}
								schedulePageAndroid.deleteSchedule(scheduleName);
							} else {
								extentTestAndroid.log(Status.FAIL, scheduleName + " is not created ");
								utilityAndroid.captureScreenshot(androidDriver);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, "Error while saving the schedule : " + schedulePageAndroid.getErrorMsg());
							utilityAndroid.captureScreenshot(androidDriver);
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has the premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user has the premission to add Schedule");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from close obstruction
				webService.exitDoubleEntrapmentGate(gateSerialNumber);
				menuPageAndroid.verifyAndClickMenuLink("Devices");
				devicesPageAndroid.verifyDevicePresent(deviceName, udid);
				if(!devicesPageAndroid.verifyExclaimationPoint(deviceName)){
					extentTestAndroid.log(Status.PASS, " Warning Symbol and error label not present");
				} else {
					extentTestAndroid.log(Status.PASS, " Warning Symbol and error label not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				devicesPageAndroid.deviceActuation(deviceName);
				currentStatus = devicesPageAndroid.getDeviceStatus(deviceName);
				if (currentStatus.equalsIgnoreCase("CLOSED")) {
					extentTestAndroid.log(Status.PASS, deviceName + " is now in " +currentStatus+ " after exitDoubleEntrapmentGate API Call");
				} else {
					extentTestAndroid.log(Status.FAIL, deviceName + " is now in " + currentStatus+ " and not in CLOSED state, after exitDoubleEntrapmentGate API Call");
					webService.exitDoubleEntrapmentGate(gateSerialNumber);
				}

				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to control devices");
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA341_ValidateErrorForVGDOInLowBattery(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String deviceName;
		String vgdoSerialNumber;
		String voltage;
		String cycleCount;
		String heading = "Important MyQ Alert";
		String scheduleErrorText = "Your door sensor battery is low. For security, app door control and schedules will not work until the battery is replaced. (708)";
		String tempText;
		String state;
		String scheduleName;

		deviceName = testData.get(0).split(",")[0];
		vgdoSerialNumber = testData.get(0).split(",")[1];
		voltage = testData.get(0).split(",")[2];
		cycleCount = testData.get(0).split(",")[3];
		scheduleName = testData.get(0).split(",")[4];
		state = testData.get(0).split(",")[5];
		HashMap<String, String> deviceAndState = new HashMap<>();
		deviceAndState.put(deviceName, state);

		String text1 = "Urgent: The battery in ".concat(deviceName).concat(" door sensor needs to be replaced in order to continue using ").concat(deviceName).concat(" with your MyQ app.");
		String text2 = "The battery in ".concat(deviceName).concat(" door sensor is running low. Replace it as soon as possible.");

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.verifyAndClickMenuLink("Devices");
			if (devicesPageAndroid.verifyDevicePresent(deviceName, udid)) {
				//webservice to enter close obstruction for GDO
				webService.setCycleCountVGDO(vgdoSerialNumber,cycleCount);
				webService.setVoltageVGDO(vgdoSerialNumber,voltage);

				// wait for one minute
				devicesPageAndroid.waitFor2Minute();
				//verify notification
				utilityAndroid.openNotificationPanel();
				if(devicesPageAndroid.verifyNotifications(deviceName,heading , text1)) {
					extentTestAndroid.log(Status.PASS, "Notification received for " +deviceName + " in low battery state");
				} else if (devicesPageAndroid.verifyNotifications(deviceName, heading, text2)) {
					extentTestAndroid.log(Status.PASS, "1st Notification received for " +deviceName + " in low battery state");
					if (devicesPageAndroid.verifySecondNotifications(deviceName, heading, text2)) {
						extentTestAndroid.log(Status.PASS, "2nd Notification received for " +deviceName + " in low battery state");
					} else {
						extentTestAndroid.log(Status.FAIL, "2nd Notification not received for " +deviceName + " in low battery state");
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Notification received not for " +deviceName + " in low battery state");
				}
				utilityAndroid.closeNotificationPanel(udid);

				//verify the warning symbol on device
				if (devicesPageAndroid.verifyExclaimationPoint(deviceName)) {
					extentTestAndroid.log(Status.PASS, " Warning sybmol on device is present");
				} else {
					extentTestAndroid.log(Status.FAIL, " Error warning symbol on device is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//verify error label below device icon
				if(devicesPageAndroid.verifyYellowBarText(deviceName, "Low Battery", udid)) {
					extentTestAndroid.log(Status.PASS, " Error label below device icon is present");
				} else {
					extentTestAndroid.log(Status.FAIL, " Error label below device icon is not present");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//MTA-497 Error verification for Schedule in Low Battery state
				if (menuPageAndroid.verifyAndClickMenuLink("Schedules")) {
					schedulePageAndroid.addSchedule();
					if(!schedulePageAndroid.verifyErrorDispalyed()) {
						schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
						if(schedulePageAndroid.verifyErrorDispalyed()) {
							tempText = schedulePageAndroid.getErrorMsg();
							if(tempText.equals(scheduleErrorText)) {
								extentTestAndroid.log(Status.PASS, "Expected error pop-up shown :- " +tempText);
							} else {
								extentTestAndroid.log(Status.PASS, "Different error pop-up shown :- " +tempText);
							}
							schedulePageAndroid.clickOkButton()
							.clickBackButton()
							.clickBackConfirmation();
						} else {
							extentTestAndroid.log(Status.FAIL, "Error not displayed while creating schedule for " +deviceName+ " in low battery state" );
						}
					} else {
						extentTestAndroid.log(Status.FAIL, schedulePageAndroid.getErrorMsg());
						utilityAndroid.captureScreenshot(androidDriver);
						schedulePageAndroid.clickLaterButton();
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add Schedule");	
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator"))  {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add Schedule");
					}
				}
				//exit from close obstruction
				webService.setVoltageVGDO(vgdoSerialNumber,"270");
				webService.setCycleCountVGDO(vgdoSerialNumber,"0");

				menuPageAndroid.verifyAndClickMenuLink("Devices");
				devicesPageAndroid.verifyDevicePresent(deviceName, udid);
				if(!devicesPageAndroid.verifyExclaimationPoint(deviceName)){
					extentTestAndroid.log(Status.PASS, " Warning Symbol and error label not present. Device is back in healty state");
				} else {
					extentTestAndroid.log(Status.FAIL, " Warning Symbol and error label is still  present");
					utilityAndroid.captureScreenshot(androidDriver);
				}

				if (activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestAndroid.log(Status.PASS,  activeUserRole + " user has the premission to control devices");
				}	
			} else {
				extentTestAndroid.log(Status.FAIL, " Expected Device not present ");
				utilityAndroid.captureScreenshot(androidDriver);
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method that adds a light to provided gateway, create instant alert and regular schedule to trigger after 2 minutes of current system time,
	 * deletes the light, and verify is schedule and alerts got deleted as well.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA293_ValidateScheduleAlertOnDeletingLight(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String gatewayName;
		String gatewaySerialNumber;
		String lightSerialNumber;
		String lightName;
		String scheduleName = "ScheduleDelete";
		String alertName = "Alert-Delete";
		HashMap<String, String> deviceAndState = new HashMap<>();

		gatewayName = testData.get(0).split(",")[0];
		gatewaySerialNumber = testData.get(0).split(",")[1];
		lightName = testData.get(0).split(",")[2];
		lightSerialNumber = testData.get(0).split(",")[3];
		deviceAndState.put(lightName, "On");
		extentTestAndroid = createTest(deviceTest , scenarioName);
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				devicesPageAndroid.selectGateway(gatewayName, udid)
				.addNewDevice() 
				.clickRemoteLightButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnLightToGatewayService(gatewaySerialNumber,lightSerialNumber);
				// enter the name of Light
				devicesPageAndroid.enterDeviceName(lightName);    
				devicesPageAndroid.clickNextButton();

				//Create Schedule
				menuPageAndroid.clickMenuLink("Schedules");  
				schedulePageAndroid.addSchedule();
				schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
				if (schedulePageAndroid.verifySchedule(scheduleName)) {
					extentTestAndroid.log(Status.PASS, "The Schedule with name "+scheduleName+" is created.");
				} else {
					extentTestAndroid.log(Status.FAIL, "The Schedule with name "+scheduleName+" was not created.");
				}
				//Create Alert
				menuPageAndroid.clickMenuLink("Alerts");
				alertPageAndroid.addAlert();
				if (alertPageAndroid.verifyAndSelectDevice(lightName, udid)) {
					alertPageAndroid.setAlertName(alertName)
					.setStateToggle(alertPageAndroid.alert_on_open_toggle, true)
					.setStateToggle(alertPageAndroid.push_notification_toggle, true)
					.clickSave();
					if (alertPageAndroid.verifyAlertForDevice(alertName, lightName)) {
						extentTestAndroid.log(Status.PASS, "Alert is created with name "+alertName+".");
					} else {
						extentTestAndroid.log(Status.FAIL, alertName+" for "+lightName+" is not created.");
					}
				} else {
					extentTestAndroid.log(Status.FAIL, lightName + " Device is not available.");
				}
				//Delete the Device
				if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) { 
					devicesPageAndroid.selectGateway(gatewayName, udid)
					.deleteDevice(lightName)
					.clickBackButton();
					devicesPageAndroid.selectGateway(gatewayName, udid);
					if(deviceManagementPageAndroid.verifyDeviceDeleted(lightName, udid)) {
						extentTestAndroid.log(Status.PASS, lightName + " was removed.");
						//Verify Schedule is also deleted 
						menuPageAndroid.clickMenuLink("Schedules");  
						if (!schedulePageAndroid.verifySchedule(scheduleName)) {
							extentTestAndroid.log(Status.PASS, "On deleting the "+lightName+" schedule with name "+scheduleName+" got deleted successfully.");
						} else {
							extentTestAndroid.log(Status.FAIL, "On deleting the "+lightName+" ,"+scheduleName+" still exist.");
							utilityAndroid.captureScreenshot(androidDriver);
						}
						//Verify Alert should also be deleted
						menuPageAndroid.clickMenuLink("Alerts"); 
						if (!alertPageAndroid.verifyAlertForDevice(alertName, lightName)) {
							extentTestAndroid.log(Status.PASS, "Alert for " + lightName + " with name "+alertName+" has been deleted.");
						} else {
							extentTestAndroid.log(Status.FAIL, "On deleting the " + lightName + " still "+alertName+" exist.");
							utilityAndroid.captureScreenshot(androidDriver);
						}
					} else {
						extentTestAndroid.log(Status.FAIL, lightName + " was not removed.");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "The Device Management link was not available.");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " User doesn't have  the premission to add light.");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestAndroid.log(Status.FAIL, activeUserRole + " User doesn't have  premission to add light.");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available ");
		} 
	}

	/**
	 * Description : This is an Android test method to add gateway and light to it, create instant alert, regular schedule to trigger after 2 minutes of current system time,
	 * verify history before deleting gateway, deletes gateway and verify light gets deleted, alert and schedule get deleted as well, verify history should remain. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA299_ValidateEPDOnDeletingGateway(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String gatewaySerialNumber;
		String gatewayName;
		String lightSerialNumber;
		String lightName;
		String timeSelected = null;
		String deviceStatus = "null";
		String scheduleName = "ScheduleDelete";
		String alertName = "Alert-Delete";
		HashMap<String, String> deviceAndState = new HashMap<>();

		gatewayName = testData.get(0).split(",")[0];
		gatewaySerialNumber = testData.get(0).split(",")[1];
		lightName = testData.get(0).split(",")[2]; 
		lightSerialNumber = testData.get(0).split(",")[3];
		deviceAndState.put( lightName, "On");

		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		extentTestAndroid = createTest(deviceTest , scenarioName ); 

		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		schedulePageAndroid = new SchedulePageAndroid(androidDriver, extentTestAndroid);
		alertPageAndroid = new AlertPageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				webService.SendGatewayOnlineService(gatewaySerialNumber);
				devicesPageAndroid.addGateway(gatewaySerialNumber, udid)
				.enterGatewayName(gatewayName)
				.clickSaveButton()
				.clickBackButton()
				.selectGateway(gatewayName, udid)
				.addNewDevice()
				.clickRemoteLightButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnLightToGatewayService(gatewaySerialNumber,lightSerialNumber);
				devicesPageAndroid.enterDeviceName(lightName);   
				devicesPageAndroid.clickNextButton();

				//Create Alert
				menuPageAndroid.clickMenuLink("Alerts");
				alertPageAndroid.addAlert();
				if (alertPageAndroid.verifyAndSelectDevice(lightName, udid)) {
					alertPageAndroid.setAlertName(alertName)
					.setStateToggle(alertPageAndroid.alert_on_open_toggle, true)
					.setStateToggle(alertPageAndroid.push_notification_toggle, true)
					.clickSave();
					if (alertPageAndroid.verifyAlertForDevice(alertName, lightName)) {
						extentTestAndroid.log(Status.PASS, "Alert is created with name "+alertName+".");
					} else {
						extentTestAndroid.log(Status.FAIL, alertName+" for "+lightName+" is not created.");
					}
				} else {
					extentTestAndroid.log(Status.FAIL, lightName + " Device is not available.");
				}
				//check for the status of device, if same as for schedule, then actuate the device
				menuPageAndroid.clickMenuLink("Devices"); 
				deviceStatus = devicesPageAndroid.getDeviceStatus(lightName);
				if((deviceStatus.equals("ON"))) {
					extentTestAndroid.log(Status.INFO, lightName + " is currently in " + deviceStatus + " state, similar for schedule to trigger. Hence actuating to change state.");
					devicesPageAndroid.deviceActuation(lightName);
				} else {
					extentTestAndroid.log(Status.INFO, lightName + " is currently in " + deviceStatus + " state, and  schedule to trigger is for ON state.");
				}
				//Create Schedule 
				menuPageAndroid.clickMenuLink("Schedules");
				schedulePageAndroid.addSchedule();
				timeSelected = schedulePageAndroid.createSchedule(scheduleName, deviceAndState, 2, "All Days", true, udid);
				if (schedulePageAndroid.verifySchedule(scheduleName)) {
					extentTestAndroid.log(Status.PASS, "The Schedule with name 'ScheduleDelete' is created.");
					//navigate to devices page
					menuPageAndroid.clickMenuLink("Devices"); 
					//wait for duration which is set in schedule
					schedulePageAndroid.waitForNotification(timeSelected);

					deviceStatus = devicesPageAndroid.getDeviceStatus(lightName);
					if (deviceStatus.equalsIgnoreCase("On")) {
						extentTestAndroid.log(Status.INFO, lightName + " status change to "+ deviceStatus); 
					} else {
						extentTestAndroid.log(Status.INFO, lightName + " status now is -"+ deviceStatus );
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "The Schedule with name 'ScheduleDelete' was not created.");
				}
				//Verify History before deletion of Gateway
				if(activeUserRole.equalsIgnoreCase("Guest")) {
					if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
					}else {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has access to History");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					menuPageAndroid.clickMenuLink("History");
					if(historyPageAndroid.verifyHistory(timeSelected, scheduleName, " was triggered")) {
						extentTestAndroid.log(Status.PASS, "Before deletion of Gateway History found for : "+ scheduleName );
					} else {
						extentTestAndroid.log(Status.FAIL, "Before deletion of Gateway History not found for : "+ scheduleName + " for time " + timeSelected );
					}
					//Verify Light state change
					if(historyPageAndroid.verifyHistory(timeSelected, lightName, " just turned On")) {
						extentTestAndroid.log(Status.PASS, "Before deletion of Gateway History found for : " + lightName );
					} else {
						extentTestAndroid.log(Status.FAIL, "Before deletion of Gateway History not found for : "+ lightName + " for time " + timeSelected );
					}
				}
				// Deleting the gateway
				menuPageAndroid.clickMenuLink("Device Management");
				deviceManagementPageAndroid.deleteHub(gatewayName, udid);
				// Verify device got deleted automatically.
				menuPageAndroid.clickMenuLink("Devices");
				if (!devicesPageAndroid.verifyDevicePresent(lightName, udid)){
					extentTestAndroid.log(Status.PASS, "On deleting the "+gatewayName +", "+ lightName +"' device also got deleted successfully.");
				} else {
					extentTestAndroid.log(Status.FAIL, "On deleting the "+gatewayName +", "+ lightName +"' device still exists.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//Verify Schedule is also deleted 
				menuPageAndroid.clickMenuLink("Schedules");  
				if (!schedulePageAndroid.verifySchedule(scheduleName)) {
					extentTestAndroid.log(Status.PASS, "On deleting the "+gatewayName+" schedule with name "+scheduleName+" for "+lightName+" got deleted successfully.");
				} else {
					extentTestAndroid.log(Status.FAIL, "On deleting the "+gatewayName+" ,"+scheduleName+" for "+lightName+" still exist.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//Verify Alert should also be deleted
				menuPageAndroid.clickMenuLink("Alerts"); 
				if (!alertPageAndroid.verifyAlertForDevice("Alert-Delete", lightName)) {
					extentTestAndroid.log(Status.PASS, "Alert for " + lightName + " with name "+alertName+" has been deleted by deleteing "+gatewayName+" .");
				} else {
					extentTestAndroid.log(Status.FAIL, "On deleting the " + gatewayName + " still "+alertName+" exist for "+lightName+".");
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//Verify History After deletion of Gateway
				if(activeUserRole.equalsIgnoreCase("Guest")) {
					if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
					}else {
						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has access to History");
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					menuPageAndroid.clickMenuLink("History");
					if(historyPageAndroid.verifyHistory(timeSelected, scheduleName, " was triggered")) {
						extentTestAndroid.log(Status.PASS, "After deletion of Gateway History found for : "+ scheduleName );
					} else {
						extentTestAndroid.log(Status.FAIL, "After deletion of Gateway History not found for : "+ scheduleName + " for time " + timeSelected );
						utilityAndroid.captureScreenshot(androidDriver);
					}
					//Verify Light state change
					if(historyPageAndroid.verifyHistory(timeSelected, lightName, " just turned On")) {
						extentTestAndroid.log(Status.PASS, "After deletion of Gateway History found for: " + lightName );
					} else {
						extentTestAndroid.log(Status.FAIL, "After deletion of Gateway History not found for: "+ lightName + " for time " + timeSelected );
						utilityAndroid.captureScreenshot(androidDriver);
					}
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " User doesn't have  premission to add gateway");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " User doesn't have  premission to add gateway");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to verify text appearing when clicked on help link on device management page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */	
	public void MTA391_ValidateHelpTextOnDeviceManagement(ArrayList<String> testData, String deviceTest, String scenarioName) throws Exception {
		String helpText;
		helpText = testData.get(0);

		extentTestAndroid = createTest(deviceTest , scenarioName ); 
		GlobalVariables.currentTestAndroid =  Thread.currentThread().getStackTrace()[1].getMethodName();
		// object for page factory class
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
			//validate help link on Device management screen 
			deviceManagementPageAndroid.clickOnHelpLink();
			if (deviceManagementPageAndroid.verifyTextOnPage(helpText)) {
				extentTestAndroid.log(Status.PASS, "LiftMaster help page was displayed successfully.");
			} else {
				utilityAndroid.captureScreenshot(androidDriver);
				extentTestAndroid.log(Status.FAIL, "LiftMaster help page is not displayed.");
			}
			deviceManagementPageAndroid.clickOnBackButton();
		} else {
			if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
				extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission for device management ");
			} else if (activeUserRole.equals("Admin") || activeUserRole.equals("CreatorAdmin")) { 
				extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission for device management ");
			}
		}
	}

	/**
	 * Description : This is an Android test method to check if user is able to delete the history
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA433_ValidateDeletingHistory(ArrayList<String> testData, String deviceTest, String scenarioName) {
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		historyPageAndroid = new HistoryPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if(activeUserRole.equalsIgnoreCase("Guest")) {
				if(!menuPageAndroid.verifyAndClickMenuLink("History")) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have access to History");
				}else {
					extentTestAndroid.log(Status.FAIL,activeUserRole + " user has access to History");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				menuPageAndroid.clickMenuLink("History");
				if (historyPageAndroid.deleteHistory()) {
					extentTestAndroid.log(Status.PASS, "Deleted history successfull.");
				} else {
					extentTestAndroid.log(Status.FAIL, "History was not deleted. There are still events logged.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			}
			if ( activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
				extentTestAndroid.log(Status.PASS, activeUserRole + " has access to history");
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available");
		}
	}

	/**
	 * Description : This is an Android test method to verify the error message and error code when gateway serial number is invalid
	 * @param testData
	 * @param deviceTest
	 * @throws InterruptedException 
	 */
	public void MTA415_ValidateGWSerialNumberError301(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {
		String errorMessage="The serial number was invalid. Please try again. (301)";
		String GWSerialNumber = testData.get(0);

		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				devicesPageAndroid.enterGWDetails(GWSerialNumber); // enter GW serial number
				if (devicesPageAndroid.verifyErrorMessage(errorMessage)) {
					extentTestAndroid.log(Status.PASS, "'" + errorMessage + "' error code displayed successfully");
				} else {
					extentTestAndroid.log(Status.FAIL, "'" + errorMessage + "' error code NOT displayed.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to access Device Management");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to access Device Management");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available");
		}
	}

	/**
	 * Description : This is an Android test method to verify error message and code when brand of gateway is not recognized.
	 * @param testData
	 * @param deviceTest
	 * @throws InterruptedException 
	 */
	public void MTA315_ValidateGWSerialNumberError302(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {
		String errorMessage="That brand of gateway or hub can't be registered to your account. Check the serial number and try again (302)";
		String GWSerialNumber = testData.get(0);
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				devicesPageAndroid.enterGWDetails(GWSerialNumber); // enter GW serial number
				if (devicesPageAndroid.verifyErrorMessage(errorMessage)) {
					extentTestAndroid.log(Status.PASS, "'" + errorMessage + "' error code displayed successfully");
				} else {
					extentTestAndroid.log(Status.FAIL, "'" + errorMessage + "' error code NOT displayed.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to access Device Management");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to access Device Management");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available");
		}
	}

	/**
	 * Description : This is an Android test method to verify error code when an already mapped GW serial number is being added on to other user's account
	 * @param testData
	 * @param deviceTest
	 * @throws InterruptedException 
	 */
	public void MTA419_ValidateGWSerialNumberError310(ArrayList<String> testData, String deviceTest, String scenarioName) throws InterruptedException {
		String errorMessage="That device is on another user's account. Unable to add (310)";
		String GWSerialNumber = testData.get(0);
		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				devicesPageAndroid.enterGWDetails(GWSerialNumber); // enter GW serial number
				if (devicesPageAndroid.verifyErrorMessage(errorMessage)) {
					extentTestAndroid.log(Status.PASS, "'" + errorMessage + "' error code displayed successfully");
				} else {
					extentTestAndroid.log(Status.FAIL, "'" + errorMessage + "' error code NOT displayed.");
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to access Device Management");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to access Device Management");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available");
		}
	}
	
	/**
	 * Description : This is an Android test method to add A-Hub in OEM Tx Pairing Option 1 by triggering necessary API Calls first , then add hub on device management page, 
	 * verify device added with default name and in monitor door only mode, verify error , complete the VGDO set-up process
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA532_ValidateOEMTxPairingOption1(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String vgdoName;
		String vgdoSerialNumber;
		String sensor;
		String statusBeforeActuation;
		String statusAfterActuation;
		String errText;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		vgdoSerialNumber = testData.get(0).split(",")[2];
		vgdoName = testData.get(0).split(",")[3];
		sensor = testData.get(0).split(",")[4];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			//API calls
			webService.SendGatewayOnlineService(gatewaySerialNumber);
			webService.GatewayStartLearningService(gatewaySerialNumber);
			webService.enterLearnModeForVGDO(vgdoSerialNumber);
			webService.testSensorVGDO(vgdoSerialNumber, sensor);
			//adding hub
			menuPageAndroid.clickMenuLink("Devices");
			if (devicesPageAndroid.verifyPlusButton()) {
				if(devicesPageAndroid.verifyGetStartedButton()){
					devicesPageAndroid.clickGetStartedButton();
				} else {
					devicesPageAndroid.clickPlusButton();
				}
				devicesPageAndroid.addGateway(gatewaySerialNumber,udid)
				.enterGatewayName(gatewayName)
				.clickSaveButton()
				.clickBackButton();	
				//verify device added with default name
				menuPageAndroid.clickMenuLink("Devices");
				if(devicesPageAndroid.verifyDeviceWithHub(gatewayName, "Garage Door Opener", udid)) {
					devicesPageAndroid.tapDeviceForError("Garage Door Opener");
				//verify Monitor Door only error by tapping on device
					if(devicesPageAndroid.verifyErrorPresent()) {
						errText = devicesPageAndroid.getErrorText();
						if(errText.equals("This door is in monitor only mode.")) {
							extentTestAndroid.log(Status.PASS, "Error pop-up present while tapping the device, as expected -> " + errText);
						} else {
							extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
							utilityAndroid.captureScreenshot(androidDriver);
						}
						devicesPageAndroid.clickOnOkButton();
					}
					//completing the setup process of vgdo
					menuPageAndroid.clickMenuLink("Device Management");
					devicesPageAndroid.selectGateway(gatewayName, udid)
					.selectDevice("Garage Door Opener", udid)
					.clickSelectAndProgramDoor()
					.tapToSelect()
					.enterTextToSearch("LiftMaster")
					.clickOnBrandSearched()
					.tapToSelect()
					.selectColorOfProgramButton("Yellow")
					.clickOnProgramDoor()
					.clickOnContinueButton()
					.clickOnPressedProgramButton()
					.enterDeviceName(vgdoName)
					.clickOnNext();
					//verify device is in healty state
					menuPageAndroid.clickMenuLink("Devices");
					if(devicesPageAndroid.verifyDevicePresent(vgdoName, udid)){
						extentTestAndroid.log(Status.PASS, vgdoName + " is added to " + gatewayName);
						if(!devicesPageAndroid.verifyYellowBarPresent(vgdoName, udid)){
							extentTestAndroid.log(Status.PASS, vgdoName + " is in healthy state ");
							statusBeforeActuation = devicesPageAndroid.getDeviceStatus(vgdoName);
							devicesPageAndroid.deviceActuation(vgdoName);
							statusAfterActuation = devicesPageAndroid.getDeviceStatus(vgdoName);
							if(!statusBeforeActuation.equals(statusAfterActuation)) {
								extentTestAndroid.log(Status.PASS, vgdoName + " is now in " + statusAfterActuation);
							} else {
								extentTestAndroid.log(Status.FAIL, vgdoName + " state not changed. Currently in " + statusBeforeActuation);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, vgdoName + " is still in error state ");
						}
						//deleting device
						menuPageAndroid.clickMenuLink("Device Management");
						devicesPageAndroid.selectGateway(gatewayName, udid)
						.deleteDevice(vgdoName)
						.clickBackButton();
					} else {
						extentTestAndroid.log(Status.FAIL, vgdoName + " is not added to " + gatewayName);
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Garage Door Opener is not found for " + gatewayName );
					utilityAndroid.captureScreenshot(androidDriver);
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add deivce ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add deivce ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description : This is an Android test method to add A-Hub in OEM Tx Pairing Option 2. Hub should be added to account as pre-requisite, then triggering necessary API Calls, 
	 * verify device added with default name and in monitor door only mode, verify error , complete the VGDO set-up process 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA534_ValidateOEMTxPairingOption2(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String vgdoName;
		String vgdoSerialNumber;
		String sensor;
		String statusBeforeActuation;
		String statusAfterActuation;
		String errText;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		vgdoSerialNumber = testData.get(0).split(",")[2];
		vgdoName = testData.get(0).split(",")[3];
		sensor = testData.get(0).split(",")[4];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
				//verify hub added , if not procedd with adding the hub (pre-requisite)
				if(!deviceManagementPageAndroid.verifyHubName(gatewayName, udid)) {
					devicesPageAndroid.addGateway(gatewaySerialNumber, udid)
					.enterGatewayName(gatewayName)
					.clickSaveButton()
					.clickBackButton();	
				}
				//API calls
				webService.SendGatewayOnlineService(gatewaySerialNumber);
				webService.GatewayStartLearningService(gatewaySerialNumber);
				webService.enterLearnModeForVGDO(vgdoSerialNumber);
				webService.testSensorVGDO(vgdoSerialNumber, sensor);
				//verify device added with default name
				menuPageAndroid.clickMenuLink("Devices");
				if(devicesPageAndroid.verifyDeviceWithHub(gatewayName, "Garage Door Opener", udid)) {
					//verify Monitor door only error
					devicesPageAndroid.tapDeviceForError("Garage Door Opener");
					if(devicesPageAndroid.verifyErrorPresent()) {
						errText = devicesPageAndroid.getErrorText();
						if(errText.equals("This door is in monitor only mode.")) {
							extentTestAndroid.log(Status.PASS, "Error pop-up present while tapping the device, as expected -> " + errText);
						} else {
							extentTestAndroid.log(Status.FAIL, "Different error pop-up was present :- " + errText);
							utilityAndroid.captureScreenshot(androidDriver);
						}
						devicesPageAndroid.clickOnOkButton();
					}
					//complete the setup process
					menuPageAndroid.clickMenuLink("Device Management");
					devicesPageAndroid.selectGateway(gatewayName, udid)
					.selectDevice("Garage Door Opener", udid)
					.clickSelectAndProgramDoor()
					.tapToSelect()
					.enterTextToSearch("LiftMaster")
					.clickOnBrandSearched()
					.tapToSelect()
					.selectColorOfProgramButton("Yellow")
					.clickOnProgramDoor()
					.clickOnContinueButton()
					.clickOnPressedProgramButton()
					.enterDeviceName(vgdoName)
					.clickOnNext();
					//verify device is in healty state
					menuPageAndroid.clickMenuLink("Devices");
					if(devicesPageAndroid.verifyDevicePresent(vgdoName, udid)){
						extentTestAndroid.log(Status.PASS, vgdoName + " is added to " + gatewayName);
						if(!devicesPageAndroid.verifyYellowBarPresent(vgdoName, udid)){
							extentTestAndroid.log(Status.PASS, vgdoName + " is in healthy state ");
							statusBeforeActuation = devicesPageAndroid.getDeviceStatus(vgdoName);
							devicesPageAndroid.deviceActuation(vgdoName);
							statusAfterActuation = devicesPageAndroid.getDeviceStatus(vgdoName);
							if(!statusBeforeActuation.equals(statusAfterActuation)) {
								extentTestAndroid.log(Status.PASS, vgdoName + " is now in " + statusAfterActuation);
							} else {
								extentTestAndroid.log(Status.FAIL, vgdoName + " state not changed. Currently in " + statusBeforeActuation);
							}
						} else {
							extentTestAndroid.log(Status.FAIL, vgdoName + " is still in error state ");
						}
						//delete device
						menuPageAndroid.clickMenuLink("Device Management");
						devicesPageAndroid.selectGateway(gatewayName, udid)
						.deleteDevice(vgdoName)
						.clickBackButton();
					} else {
						extentTestAndroid.log(Status.FAIL, vgdoName + " is not added to " + gatewayName);
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, "Garage Door Opener is not found for " + gatewayName );
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add deivce ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add deivce ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}
	
	/**
	 * Description : This is an Android test method to verify maximum addition of VGDO to A-hub / Wifi-hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void MTA549_ValidateAddingMaximumVGDOToHub(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		String gatewaySerialNumber;
		String gatewayName;
		String vgdo1;
		String vgdo1SerialNumber;
		String vgdo2;
		String vgdo2SerialNumber;
		String sensor;
		String errText = "You have reached the maximum number of doors. Please add an additional hub or remove an existing door.";

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		vgdo1SerialNumber = testData.get(0).split(",")[2];
		vgdo1 = testData.get(0).split(",")[3];
		vgdo2SerialNumber = testData.get(0).split(",")[4];
		vgdo2 = testData.get(0).split(",")[5];
		sensor = testData.get(0).split(",")[6];

		extentTestAndroid = createTest(deviceTest , scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		deviceManagementPageAndroid = new DeviceManagementPageAndroid(androidDriver, extentTestAndroid);
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		WebServices webService = new WebServices(extentTestAndroid);

		if (roleSelected) {
			menuPageAndroid.clickMenuLink("Devices");
			if(devicesPageAndroid.verifyPlusButton()) {
				//API Call
				webService.SendGatewayOnlineService(gatewaySerialNumber);
				devicesPageAndroid.clickPlusButton();
				devicesPageAndroid.addGateway(gatewaySerialNumber, udid)
				.enterGatewayName(gatewayName)
				.clickSaveButton()
				.clickBackButton();	
				//navigate to Device page
				menuPageAndroid.clickMenuLink("Devices");
				devicesPageAndroid.clickPlusButton();
				//adding first VGDO to the device
				devicesPageAndroid.selectGateway(gatewayName, udid) 
				.addNewDevice()
				.clickGDOButton()
				.clickOnNext() // change as per 3.103 build
				.clickNextButton();
				webService.enterLearnModeForVGDO(vgdo1SerialNumber);
				webService.testSensorVGDO(vgdo1SerialNumber, sensor);
				devicesPageAndroid.tapToSelect()
				.enterTextToSearch("LiftMaster")
				.clickOnBrandSearched()
				.tapToSelect()
				.selectColorOfProgramButton("Yellow")
				.clickOnProgramDoor()
				.clickOnContinueButton()
				.clickOnPressedProgramButton()
				.enterDeviceName(vgdo1)
				.clickOnNext();
				//navigate to device page and verify VGDO1 is added successfully or not
				menuPageAndroid.clickMenuLink("Devices");
				if(devicesPageAndroid.verifyDevicePresent(vgdo1, udid)){
					extentTestAndroid.log(Status.PASS, vgdo1 + " is added to " + gatewayName);
					devicesPageAndroid.clickPlusButton();
					//adding second VGDO to the device
					devicesPageAndroid.selectGateway(gatewayName, udid) 
					.addNewDevice()
					.clickGDOButton()
					.clickOnNext() // change as per 3.103 build
					.clickNextButton();
					webService.enterLearnModeForVGDO(vgdo2SerialNumber);
					webService.testSensorVGDO(vgdo2SerialNumber, sensor);
					devicesPageAndroid.tapToSelect()
					.enterTextToSearch("LiftMaster")
					.clickOnBrandSearched()
					.tapToSelect()
					.selectColorOfProgramButton("Yellow")
					.clickOnProgramDoor()
					.clickOnContinueButton()
					.clickOnPressedProgramButton()
					.enterDeviceName(vgdo2)
					.clickOnNext();
					//navigate to device page and verify VGDO2 is added successfully or not
					menuPageAndroid.clickMenuLink("Devices");
					if(devicesPageAndroid.verifyDevicePresent(vgdo2, udid)){
						extentTestAndroid.log(Status.PASS, vgdo2 + " is added to " + gatewayName);
						//adding 3rd VGDO
						devicesPageAndroid.clickPlusButton();
						//adding second VGDO to the device
						devicesPageAndroid.selectGateway(gatewayName, udid) 
						.addNewDevice()
						.clickGDOButton()
						.clickOnNext();
						//Verify error on adding 3rd VGDO
						if(devicesPageAndroid.verifyErrorPresent()) {
							if(devicesPageAndroid.getErrorText().equalsIgnoreCase(errText)) {
								extentTestAndroid.log(Status.PASS, errText);
							} else {
								extentTestAndroid.log(Status.FAIL, "Different text present :- " + devicesPageAndroid.getErrorText() );
							}
							devicesPageAndroid.clickOnOkButton()
							.clickBackButton()
							.clickBackButton()
							.clickBackButton();
						} else{
							extentTestAndroid.log(Status.FAIL, "No error present");
							devicesPageAndroid.clickBackButton();
						}
					} else {
						extentTestAndroid.log(Status.FAIL, vgdo2 + " is not added to " + gatewayName);
						utilityAndroid.captureScreenshot(androidDriver);
					}
				} else {
					extentTestAndroid.log(Status.FAIL, vgdo1 + " is not added to " + gatewayName);
					utilityAndroid.captureScreenshot(androidDriver);
				}
				//delete hub
				menuPageAndroid.clickMenuLink("Device Management");
				deviceManagementPageAndroid.deleteHub(gatewayName, udid);
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add deivce ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add deivce ");
				}
			}
		} else {
			extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		}
	}

	/**
	 * Description	 :	This is the logout test case
	 */
	public void logoutAndroid(ArrayList<String> testData, String deviceTest, String scenarioName) {
		extentTestAndroid = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		menuPageAndroid.tappingAccountView();
		loginPageAndroid.clickLogout();
	}

	/**
	 * Description : This function would close the driver session
	 */
	public void quitAndroidDriver() {
		androidDriver.quit();
	}

	/**
	 * Description	 :	This function would skip the test cases in Android if, test cases of the groups are not matching with the respective sheet
	 * @param testcase,deviceTest
	 */
	public void skipAndroidTest(String testcase, String deviceTest) {

		extentTestAndroid = createTest(deviceTest,testcase);
		extentTestAndroid.log(Status.SKIP, "Test : " +testcase+ " not found in " +activeUserRole +"_All_Test sheet" );
	}

	/**
	 * Description : This function will start the android driver and login to the application if we get any false failure
	 */
	public synchronized void reportFlushAndroid(String deviceTest) throws InterruptedException {
		loginPageAndroid = new LoginPageAndroid(androidDriver, extentTestAndroid);
		devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		if (deviceTest.equals("Android1")) {
			extentReportsAndroid1.flush();
			try {
				if(devicesPageAndroid.verifyOkButtonPresent()) {
					devicesPageAndroid.clickOnOkButton();
				}
			} catch (Exception e) {
			}
			Thread.sleep(3000);
			if (!menuPageAndroid.verifyHamburgerPresent() && !loginPageAndroid.verifyLoginBtnPresent()) {
				if(!androidDriver.currentActivity().contains("com.chamberlain.myq")) {
					androidDriver.resetApp();
					loginPageAndroid.selectEnvironment(env)
					.selectVersion(userType)
					.login(email, password);
					roleSelected = devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);
				} else {
					androidDriver.resetApp();
					loginPageAndroid.selectEnvironment(env)
					.selectVersion(userType)
					.login(email, password);
					roleSelected = devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);
				}
			}
		} else {
			extentReportsAndroid2.flush();
			Thread.sleep(3000);
			if (!menuPageAndroid.verifyHamburgerPresent() && !loginPageAndroid.verifyLoginBtnPresent()) {
				if(!androidDriver.currentActivity().contains("com.chamberlain.myq")) {
					androidDriver.resetApp();
					loginPageAndroid.selectEnvironment(env)
					.selectVersion(userType)
					.login(email, password);
					roleSelected = devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);
				} else {
					androidDriver.resetApp();
					loginPageAndroid.selectEnvironment(env)
					.selectVersion(userType)
					.login(email, password);
					roleSelected = devicesPageAndroid.verifyAndSelectUserRole(activeUserRole,activeAccountName);
				}
			}
		}
	}

	/**
	 * Description : This function will convert milliseconds to hours, minutes and seconds
	 * @param millisecond
	 */
	public String msToString(long ms) {
		long totalSecs = ms/1000;
		long hours = (totalSecs / 3600);
		long mins = (totalSecs / 60) % 60;
		long secs = totalSecs % 60;
		String minsString = (mins == 0)
				? "00"
						: ((mins < 10)
								? "0" + mins
										: "" + mins);
		String secsString = (secs == 0)
				? "00"
						: ((secs < 10)
								? "0" + secs
										: "" + secs);
		if (hours > 0)
			return hours + "h:" + minsString + "m:" + secsString+"s";
		else if (mins > 0)
			return mins + "m:" + secsString+"s";
		else return secsString+"s";
	}

	/**
	 * Description : This function will apply the custom CSS onto the extent report for total time
	 */ 
	public void applyCustomCSS(String deviceTest, String brandName) throws InterruptedException {
		if (deviceTest.equals("Android1")) {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain"))
			{
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterAndroid1.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterAndroid1.config().setCSS(customCSS);
				extentReportsAndroid1.flush();
			} else {
				extentTestAndroid.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		} else {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain"))
			{
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterAndroid2.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterAndroid2.config().setCSS(customCSS);
				extentReportsAndroid2.flush();
			} else {
				extentTestAndroid.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		}
	}
	
	//Add GDO to Wifi-Hub has been removed from 3.103 build
		/**
		 * Description : This is an Android test method to add GDO to the A-Hub using API(V2) calls. 
		 * @param testData
		 * @param deviceTest
		 * @param scenarioName
		 * @throws ClientProtocolException
		 * @throws IOException
		 * @throws ParseException
		 * @throws InterruptedException
		 * @throws AWTException
		 * @throws org.json.simple.parser.ParseException
		 */
		//		public void MTA428_ValidateAddingGDOToAHub(ArrayList<String> testData, String deviceTest, String scenarioName ) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		//			String aHubSerialNumber;
		//			String aHubName;
		//			String deviceSerialNumber;
		//			String deviceName;
		//
		//			aHubSerialNumber =  testData.get(0).split(",")[0];
		//			aHubName  = testData.get(0).split(",")[1];
		//			deviceSerialNumber = testData.get(0).split(",")[2];
		//			deviceName = testData.get(0).split(",")[3];
		//
		//			extentTestAndroid = createTest(deviceTest , scenarioName); 
		//			GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		//
		//			devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		//			menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		//			utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		//			WebServices webService = new WebServices(extentTestAndroid);
		//
		//			if (roleSelected) {
		//				if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
		//					//adding the GDO to the device
		//					devicesPageAndroid.selectGateway(aHubName) 
		//					.addNewDevice()
		//					.clickGDOButton()
		//					.clickOnNo()
		//					.clickNextButton();
		//
		//					webService.GatewayStartLearningService(aHubSerialNumber)
		//					.LearnGDOToGatewayService(aHubSerialNumber,deviceSerialNumber);
		//
		//					devicesPageAndroid.enterDeviceName(deviceName)
		//					.clickOnNext();
		//					menuPageAndroid.clickMenuLink("Devices");
		//					if(devicesPageAndroid.verifyDevicePresent(deviceName, udid)){
		//						extentTestAndroid.log(Status.PASS, deviceName + " is added to " + aHubName);
		//					} else {
		//						extentTestAndroid.log(Status.FAIL, deviceName + " is not added to " + aHubName);
		//						utilityAndroid.captureScreenshot(androidDriver);
		//					}
		//					if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
		//						extentTestAndroid.log(Status.FAIL, activeUserRole + " user has premission to add GDO ");
		//					} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
		//						extentTestAndroid.log(Status.PASS, activeUserRole + " user has premission to add GDO ");
		//					}
		//				} else {
		//					if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
		//						extentTestAndroid.log(Status.PASS, activeUserRole + " user doesn't have premission to add GDO ");
		//					} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
		//						extentTestAndroid.log(Status.FAIL, activeUserRole + " user doesn't have premission to add GDO ");
		//					}
		//				}
		//			} else {
		//				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		//			}
		//		}
		//Add GDO to Wifi-Hub has been removed from 3.103 build
		/**
		 * Description : This is an Android test method to add GDO to WifiHub using API(V2) calls, and verify if added successfully
		 * @param testData
		 * @param deviceTest
		 * @param scenarioName
		 * @throws ClientProtocolException
		 * @throws IOException
		 * @throws ParseException
		 * @throws InterruptedException
		 * @throws AWTException
		 * @throws org.json.simple.parser.ParseException
		 */
		//		public void MTA396_ValidateAddingGDOInWifiHub(ArrayList<String> testData, String deviceTest, String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {
		//			String wifiHubSerialNumber;
		//			String wifiHubName;
		//			String gdoName;
		//			String gdoSerialNumber;
		//
		//			wifiHubSerialNumber  = testData.get(0).split(",")[0];
		//			wifiHubName =  testData.get(0).split(",")[1];
		//			gdoSerialNumber = testData.get(0).split(",")[2];
		//			gdoName = testData.get(0).split(",")[3];
		//
		//			extentTestAndroid = createTest(deviceTest ,scenarioName); 
		//			GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();
		//
		//			devicesPageAndroid = new DevicesPageAndroid(androidDriver, extentTestAndroid);
		//			menuPageAndroid = new MenuPageAndroid(androidDriver, extentTestAndroid);
		//			utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
		//			WebServices webService = new WebServices(extentTestAndroid);
		//
		//			if (roleSelected) {
		//				if (menuPageAndroid.verifyAndClickMenuLink("Device Management")) {
		//					//adding the light to the device
		//					devicesPageAndroid.selectGateway(wifiHubName) 
		//					.addNewDevice()
		//					.clickGDOButton()
		//					.clickOnNo()
		//					.clickNextButton();
		//
		//					webService.GatewayStartLearningService(wifiHubSerialNumber)
		//					.LearnGDOToGatewayService(wifiHubSerialNumber,gdoSerialNumber);
		//
		//					devicesPageAndroid.enterDeviceName(gdoName)
		//					.clickOnNext();
		//
		//					menuPageAndroid.clickMenuLink("Devices");
		//					if(devicesPageAndroid.verifyDevicePresent(gdoName)){
		//						extentTestAndroid.log(Status.PASS,  gdoName + " is added to " + wifiHubName);
		//					} else {
		//						extentTestAndroid.log(Status.FAIL,  gdoName + " is not added to " + wifiHubName);
		//						utilityAndroid.captureScreenshot(androidDriver);
		//					}
		//
		//					if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
		//						extentTestAndroid.log(Status.FAIL,  activeUserRole + " user has premission to add GDO ");
		//					} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
		//						extentTestAndroid.log(Status.PASS,  activeUserRole + " user has premission to add GDO ");
		//					}
		//				} else {
		//					if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
		//						extentTestAndroid.log(Status.PASS,  activeUserRole + " user doesn't have premission to add GDO ");
		//					} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
		//						extentTestAndroid.log(Status.FAIL,  activeUserRole + " user doesn't have premission to add GDO ");
		//					}
		//				}
		//			} else {
		//				extentTestAndroid.log(Status.FAIL, activeAccountName + " as " + activeUserRole + " is not available  ");
		//			}
		//		}

}