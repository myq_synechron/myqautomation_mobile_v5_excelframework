
package com.liftmaster.myq.testmethods;

import java.awt.AWTException;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import org.apache.http.client.ClientProtocolException;
import org.json.simple.parser.ParseException;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.liftmaster.myq.ios.pages.AlertPageIOS;
import com.liftmaster.myq.ios.pages.DeviceManagementIOS;
import com.liftmaster.myq.ios.pages.DevicesPageIOS;
import com.liftmaster.myq.ios.pages.HelpPageIOS;
import com.liftmaster.myq.ios.pages.HistoryPageIOS;
import com.liftmaster.myq.ios.pages.LoginPageIOS;
import com.liftmaster.myq.ios.pages.ManageUsersPageIOS;
import com.liftmaster.myq.ios.pages.MenuPageIOS;
import com.liftmaster.myq.ios.pages.SchedulePageIOS;
import com.liftmaster.myq.ios.pages.SignUpPageIOS;
import com.liftmaster.myq.ios.pages.UserAccountPageIOS;
import com.liftmaster.myq.utils.AppiumServer;
import com.liftmaster.myq.utils.DriverInitializer;
import com.liftmaster.myq.utils.GlobalVariables;
import com.liftmaster.myq.utils.UtilityIOS;
import com.liftmaster.myq.utils.WebServices;
import com.thoughtworks.selenium.webdriven.commands.DeleteAllVisibleCookies;
import com.liftmaster.myq.utils.UtilityExcel;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;

public class IOS {

	public ExtentHtmlReporter extentHtmlReporterIOS1,extentHtmlReporterIOS2;
	public ExtentReports extentReportsIOS1,extentReportsIOS2;
	public ExtentTest extentTestIOS;

	public static String brandName;
	String platform;
	String deviceName;
	String udid;
	String appiumServer;
	String bundleId;
	String automationName;
	String xcodeOrgId;
	String xcodeSigningId;
	String wdaLocalPort;
	int appiumPort;

	IOSDriver<IOSElement> iOSDriver = null;
	LoginPageIOS loginPageIOS = null;
	MenuPageIOS menuPageIOS = null;
	DeviceManagementIOS deviceManagementIOS = null;
	DevicesPageIOS devicesPageIOS = null;
	HelpPageIOS helpPageIOS = null;
	AlertPageIOS alertPageIOS = null;
	SignUpPageIOS signUpPageIOS = null;
	SchedulePageIOS schedulePageIOS = null;
	ManageUsersPageIOS manageUsersPageIOS = null;
	UserAccountPageIOS userAccountPageIOS = null;
	HistoryPageIOS historyPageIOS = null;
	UtilityIOS utilityIOS = null;
	String device = null;
	boolean multiUser = false;
	boolean expectedRolePresent = false;
	boolean adminInvitationSent = false;
	boolean familyInvitationSent = false;
	boolean guestInvitationSent = false;
	String activeAccountName;	

	public String env;
	public String activeUserRole;
	public String userType;

	String userEmail;
	String password;

	public IOS(IOSDriver<IOSElement> iOSDriver,String env,String activeUserRole,String userType) {
		this.iOSDriver = iOSDriver;
		this.env = env;
		this.activeUserRole = activeUserRole;
		this.userType = userType;
	}


	/**
	 * Description	 :	This function is use to create test reports
	 */
	public ExtentTest createTest(String deviceTest, String testName)
	{
		if (deviceTest.equals("iOS1")) {
			extentTestIOS = extentReportsIOS1.createTest(testName);
		} else {
			extentTestIOS = extentReportsIOS2.createTest(testName);
		}
		return extentTestIOS;
	}

	/**
	 * Description : This function would fetch all the desired capabilities from excel (DeviceInfo sheet)
	 */
	public IOSDriver<IOSElement> setUp(String testName) throws InterruptedException {

		brandName = UtilityExcel.getParameters(testName,1);
		platform = UtilityExcel.getParameters(testName,2);
		deviceName  = UtilityExcel.getParameters(testName,3);
		udid = UtilityExcel.getParameters(testName,4);
		appiumServer = UtilityExcel.getParameters(testName,7);
		appiumPort = Integer.parseInt(UtilityExcel.getParameters(testName,8));

		//bootstrapPort not required for IOS
		//bootstrapPort = Integer.parseInt(UtilityExcel.getParameters(testName,9));//

		bundleId = UtilityExcel.getParameters(testName,10);
		automationName = UtilityExcel.getParameters(testName,11);
		xcodeOrgId = UtilityExcel.getParameters(testName,12);
		xcodeSigningId = UtilityExcel.getParameters(testName,13);
		wdaLocalPort  = UtilityExcel.getParameters(testName,14);

		AppiumServer.startServer(appiumServer,appiumPort);

		iOSDriver = DriverInitializer.getiOSDriver(platform,deviceName,udid,appiumServer,appiumPort,bundleId,automationName,xcodeOrgId,xcodeSigningId,wdaLocalPort);

		return iOSDriver;
	}

	/**
	 * Description	 :	This is the login test case
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void loginIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		
		userEmail = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);

		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		//Selecting the Environment and login
		loginPageIOS.selectEnvironment(env)
		.selectUserType(userType)
		.login(userEmail, password);
		if (userType.equalsIgnoreCase("Multi User")) {
			multiUser = true;
		} else if(userType.equalsIgnoreCase("Single")) {
			expectedRolePresent = true;
		}
	}

	/**
	 * Description	 :	 This function would select and switch to expected user role by tapping on the blue bar.-MTA-64 Jira Ticket
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 */
	public void MTA64_SelectAndValidateUserRole(ArrayList<String> testData, String deviceTest,String scenarioName) {

		int noOfDevicesExpected;
		activeAccountName = testData.get(0).split(",")[0];
		noOfDevicesExpected = Integer.parseInt(testData.get(0).split(",")[1]);

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		if (devicesPageIOS.verifyBlueBarPresent()) {
			devicesPageIOS.clickOnBlueBar();
			if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
				devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
				extentTestIOS.log(Status.PASS, "Test MTA64 :- " + activeUserRole + "user role is selected: PASSED");	
				expectedRolePresent = true;
				if (devicesPageIOS.verifyNumberOfDevices(noOfDevicesExpected)) {
					extentTestIOS.log(Status.PASS, "Test MTA64 :- There are " + noOfDevicesExpected + " number of devices :PASSED");	
				} else {
					extentTestIOS.log(Status.FAIL, "Test MTA64 :- For " + activeUserRole + " the device count is not matching : FAILED");
				}
			} else {
				expectedRolePresent = false;	
				extentTestIOS.log(Status.FAIL, "Test MTA64 :- " + activeUserRole + " is not selected: FAILED");
				devicesPageIOS.tapOnCancelBtn();
			}
		} else {
			if (activeUserRole.equalsIgnoreCase("Creator")) {
				expectedRolePresent = true;
				extentTestIOS.log(Status.PASS, "Test MTA64 :- " + activeUserRole + " is selected: PASSED");	
			} else {
				extentTestIOS.log(Status.FAIL, "Test MTA64 :- " + activeUserRole + " is not selected: FAILED");
				expectedRolePresent = false;
			}
		}
	}

	/**
	 * Description	 :	This function integrates UI and API calling and adds virtual Ethernet Gateway,A-hub or Wifi Hub,verifies after adding that Ethernet gateway
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void AddGatewayIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String gatewayName;
		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			
			if (devicesPageIOS.verifyAddDeviceButton()) {
				//webService.createGateway(gatewaySerialNumber)
				webService.SendGatewayOnlineService(gatewaySerialNumber);

				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				devicesPageIOS.addGateWay(gatewaySerialNumber,gatewayName);
				devicesPageIOS.addNewDevice();
				if(devicesPageIOS.verifyGatewayAvailable(gatewayName)) {
					extentTestIOS.log(Status.PASS, "Hub added successfully");
				} else {
					extentTestIOS.log(Status.FAIL, "Hub not added");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				devicesPageIOS.tapOnBackBtn();
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add Gateway");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This function integrates UI and API calling and adds light to Ethernet Gateway/A-Hub/Wifi-Hub/WGDO
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void AddLightIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String lightSerialNumber;
		String lightName;
		String gatewayName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		lightSerialNumber = testData.get(0).split(",")[2];
		lightName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(devicesPageIOS.verifyGatewayAvailable(gatewayName)){
					devicesPageIOS.selectGateway(gatewayName)
					.clickRemoteLightButton()
					.clickNextButton();
					webService.GatewayStartLearningService(gatewaySerialNumber)
					.LearnLightToGatewayService(gatewaySerialNumber,lightSerialNumber);
					devicesPageIOS.enterDeviceName(lightName);
					devicesPageIOS.clickNextButton();
					if (devicesPageIOS.verifyDeviceNamePresent(lightName)) {
						extentTestIOS.log(Status.PASS, lightName+ " added successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, lightName+ " not added");	
					} 
				} else {
					extentTestIOS.log(Status.FAIL, "Expected Gateway "+gatewaySerialNumber+" not available");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add Light");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This function integrates UI and API calling to add GDO to Ethernet Gateway
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void AddGDOIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String gatewayName;
		String gdoSerialNumber;
		String gdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];;
		gdoSerialNumber = testData.get(0).split(",")[2];
		gdoName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(devicesPageIOS.verifyGatewayAvailable(gatewayName)) {
					devicesPageIOS.selectGateway(gatewayName)
					.clickGDOButton()
					.clickNextButton();
					webService.GatewayStartLearningService(gatewaySerialNumber)
					.LearnGDOToGatewayService(gatewaySerialNumber,gdoSerialNumber);
					devicesPageIOS.enterDeviceName(gdoName);
					devicesPageIOS.clickNextButton();
					if (devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
						extentTestIOS.log(Status.PASS, gdoSerialNumber+ " added successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, gdoSerialNumber+ " not added.");	
					} 
				} else {
					extentTestIOS.log(Status.INFO, "Expected Gateway "+gatewaySerialNumber+" not present");
					devicesPageIOS.tapOnCancelBtn();
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add GDO");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This function integrates UI and API calling to add CDO to Ethernet Gateway/Wifi-hub/A-hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void AddCDOIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String gatewayName;
		String cdoSerialNumber;
		String cdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		cdoSerialNumber = testData.get(0).split(",")[2];
		cdoName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(devicesPageIOS.verifyGatewayAvailable(gatewayName)) {
					devicesPageIOS.selectGateway(gatewayName)
					.clickCDOButton()
					.clickNextButton();
					webService.GatewayStartLearningService(gatewaySerialNumber)
					.LearnCDOToGatewayService(gatewaySerialNumber,cdoSerialNumber);
					devicesPageIOS.enterDeviceName(cdoName);
					devicesPageIOS.clickNextButton();
					if (devicesPageIOS.verifyDeviceNamePresent(cdoName)) {
						extentTestIOS.log(Status.PASS, cdoName+ " added successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, cdoName+ " not added.");	
					} 
				} else {
					extentTestIOS.log(Status.INFO, "Expected hub not present");
					devicesPageIOS.tapOnCancelBtn();
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add CDO");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This function integrates UI and API calling to add Gate to Ethernet Gateway
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void AddGateIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String gateSerialNumber;
		String gatewayName;
		String gateName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		gateSerialNumber = testData.get(0).split(",")[2];
		gateName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(devicesPageIOS.verifyGatewayAvailable(gatewayName)) {
					devicesPageIOS.selectGateway(gatewayName)
					.clickGateButton()
					.clickNextButton();
					webService.GatewayStartLearningService(gatewaySerialNumber)
					.LearnGateToGatewayService(gatewaySerialNumber,gateSerialNumber);
					devicesPageIOS.enterDeviceName(gateName);
					devicesPageIOS.clickNextButton();
					
					if (devicesPageIOS.verifyDeviceNamePresent(gateName)) {
						extentTestIOS.log(Status.PASS, gateName+ " added successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, gateName+ " not added.");	
					} 
				} else {
					extentTestIOS.log(Status.INFO, "Expected hub not present");
					devicesPageIOS.tapOnCancelBtn();
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add Gate");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This iOS test method Renames Hub and devices and verify it on device page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA62_EditHubAndDeviceName(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String placeNameInitial;
		String placeNameChanged;
		String	deviceNameInitial;
		String deviceNameChanged;
		boolean checkName1;
		boolean checkName2;

		deviceNameInitial = testData.get(0).split(",")[0];
		deviceNameChanged = testData.get(0).split(",")[1];
		placeNameInitial = testData.get(0).split(",")[2];
		placeNameChanged = testData.get(0).split(",")[3];
		
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Navigating to DeviceManagement page

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();
				deviceManagementIOS.selectPlaceName(placeNameInitial)
				.setPlaceName(placeNameChanged);
				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();
				checkName1 = devicesPageIOS.getPlaceName(placeNameChanged);
				if (checkName1) {
					extentTestIOS.log(Status.PASS, "Place name succesfully changed from " + placeNameInitial + " to " + placeNameChanged);
				} else {
					extentTestIOS.log(Status.FAIL, "Place name not changed to " + placeNameChanged);
					utilityIOS.captureScreenshot(iOSDriver);
				}

				//Reverting back to the original name
				menuPageIOS.tappingHamburgerMenu()
				.tappingDeviceManagement();
				deviceManagementIOS.selectPlaceName(placeNameChanged)
				.setPlaceName(placeNameInitial);

				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();
				checkName2 =  devicesPageIOS.getPlaceName(placeNameInitial);
				if (checkName2 && checkName1) {
					extentTestIOS.log(Status.PASS, "Place name succesfully changed from " + placeNameChanged + " to " + placeNameInitial);
				} else {
					extentTestIOS.log(Status.FAIL, "Place name not changed to " + placeNameInitial);
					utilityIOS.captureScreenshot(GlobalVariables.iOSDriver);
				}

				//Editing the Device name
				menuPageIOS.tappingHamburgerMenu()
				.tappingDeviceManagement();

				deviceManagementIOS.selectPlaceName(placeNameInitial)
				.selectEndPointDevice(deviceNameInitial)
				.setDeviceName(deviceNameChanged);

				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();
				checkName1 = devicesPageIOS.verifyDeviceNamePresent(deviceNameChanged);
				if (checkName1) {
					extentTestIOS.log(Status.PASS, "Device name succesfully changed from " + deviceNameInitial + " to " + deviceNameChanged);
				} else { 
					extentTestIOS.log(Status.FAIL, "Device name not changed to " + deviceNameChanged); 
					utilityIOS.captureScreenshot(GlobalVariables.iOSDriver);
				}

				//reverting back the name
				menuPageIOS.tappingHamburgerMenu()
				.tappingDeviceManagement();
				deviceManagementIOS
				.selectPlaceName(placeNameInitial)
				.selectEndPointDevice(deviceNameChanged)
				.setDeviceName(deviceNameInitial);
				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();

				checkName2 = devicesPageIOS.verifyDeviceNamePresent(deviceNameInitial);
				if (checkName2 && checkName1) {
					extentTestIOS.log(Status.PASS, "Device name succesfully changed to " + deviceNameInitial + ". Test Case MTA-62 of editing device/place name passed");
				} else {
					extentTestIOS.log(Status.FAIL, "Device name not changed to " +deviceNameInitial + ". Test Case MTA-62 of editing device/place name failed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Rename hub and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user is able to Rename hub and device, but it is not allowed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :This IOS test method deletes the Light/GDO/CDO/Gate added in a hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void DeleteDeviceIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {

		String gatewayName;
		String deviceName;

		gatewayName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();

				devicesPageIOS.selectGateway(gatewayName);
				if(deviceManagementIOS.verifyEndPointDevice(deviceName)) {
					deviceManagementIOS.deleteDevice(deviceName);
					if(!deviceManagementIOS.verifyEndPointDevice(deviceName)) {
					extentTestIOS.log(Status.PASS,""+deviceName+"deleted successfully");
					} else {
						extentTestIOS.log(Status.FAIL,""+deviceName+" did not deleted");
					}
					deviceManagementIOS.tapBackBtn();
				} else {
					extentTestIOS.log(Status.FAIL, ""+deviceName+" device not present, hence could not be deleted.");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Delete Light as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user is able to delete device, but it is not allowed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description	 :	This IOS test method deletes the Gateway/Hub added
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 * @throws org.json.simple.parser.ParseException
	 */
	public void DeleteGatewayIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException, org.json.simple.parser.ParseException {

		String gatewayName;

		gatewayName = testData.get(0).split(",")[0];
		
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();
				if(deviceManagementIOS.verifyHubAvailable(gatewayName)) {
					deviceManagementIOS.deleteHub(gatewayName);
					if(deviceManagementIOS.verifyHubAvailable(gatewayName)) {
						extentTestIOS.log(Status.FAIL,"Gateway "+gatewayName+" not deleted.");
						utilityIOS.captureScreenshot(iOSDriver);
					} else {
						extentTestIOS.log(Status.PASS,"Gateway "+gatewayName+" deleted successfully");
					}
				} else {
					extentTestIOS.log(Status.FAIL,""+gatewayName+" Gateway not available");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to delete Gteway as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user is able to delete Gateway, but it is not allowed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	/**
	 * Description This is an iOS test method is to actuate light added on any hub, validate state change and verify that event is recorded in history.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA61_ValidateLightStateChangeAndHistory(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException   {

		String deviceStatus1;
		String deviceStatus2;
		String deviceName;
		String requiredState;
		String eventName;
		String eventTime = null;

		deviceName = testData.get(0).split(",")[0];
		requiredState = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Devices")) {
				menuPageIOS.tappingDevices();
				if (devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
					deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
					if (!requiredState.equalsIgnoreCase(deviceStatus1)) {
						devicesPageIOS.actuatingDevice(deviceName);
					}
					else {
						devicesPageIOS.actuatingDevice(deviceName);
						devicesPageIOS.actuatingDevice(deviceName);
					}
					eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceName);
					if (requiredState.equalsIgnoreCase(deviceStatus2)) {
						extentTestIOS.log(Status.PASS, "Light state changed from " + deviceStatus1 + " To " + deviceStatus2 + ". "
								+ "Change Light State to " + requiredState + " test case Passed");

						eventName = deviceName + " was Turned " + requiredState;
						menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
								extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
								menuPageIOS.tappingDevices();// To close the menu options
							} else {
								extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {	// Verify history for the remaining user types
							menuPageIOS.tappingHistory();
							if (historyPageIOS.verifyHistory(eventName,eventTime)) {
								extentTestIOS.log(Status.PASS, "History event name: '" + eventName +"' is logged successfully");	
							} else {
								extentTestIOS.log(Status.FAIL, "History event name: '" + eventName +"' is not logged ");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
							extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Light state Unchanged from " + deviceStatus1 + ". "
								+ "Change Light status test case Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					if(activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, "Light is not visible to Guest User");
					} else {
						extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present"); 
						utilityIOS.captureScreenshot(iOSDriver);
					}
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, "Expected Devices section is available to " + activeUserRole + " user");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, "Expected Devices section should be available, but not present to " + activeUserRole + " user");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	} 

	/**
	 * Description This is an iOS test method is to actuate GDO/CDO/GATE/VGDO added on any hub, validate state change and verify that event is recorded in history.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA61_ValidateDeviceStateChangeAndHistory(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException   {

		String deviceStatus1;
		String deviceStatus2;
		String deviceName;
		String requiredState;
		String eventName;
		String eventTime = null;

		deviceName = testData.get(0).split(",")[0];
		requiredState = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Devices")) {
				menuPageIOS.tappingDevices();	
				if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
					deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
					if (!requiredState.equalsIgnoreCase(deviceStatus1)) {
						devicesPageIOS.actuatingDevice(deviceName);
					}
					else {
						devicesPageIOS.actuatingDevice(deviceName);
						devicesPageIOS.actuatingDevice(deviceName);
					}
					eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceName);
					if (requiredState.equalsIgnoreCase(deviceStatus2)) {
						extentTestIOS.log(Status.PASS, ""+deviceName+" state changed from " + deviceStatus1 + " To " + deviceStatus2 + ". "
								+ "Change "+deviceName+" State to " + requiredState + " test case Passed");

						eventName = deviceName + " was " + requiredState;
						menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
								extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
								menuPageIOS.tappingDevices();// To close the menu options
							} else {
								extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {	// Verify history for the remaining user types
							menuPageIOS.tappingHistory();
							if (historyPageIOS.verifyHistory(eventName,eventTime)) {
								extentTestIOS.log(Status.PASS, "History event name: '" + eventName +"' is logged successfully");	
							} else {
								extentTestIOS.log(Status.FAIL, "History event name: '" + eventName +"' is not logged ");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
							extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
						}
					} else {
						extentTestIOS.log(Status.FAIL, ""+deviceName+" state Unchanged from " + deviceStatus1 + ". "
								+ "Change "+deviceName+" status test case Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, "Expected Devices section is available to " + activeUserRole + " user");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, "Expected Devices section should be available, but not present to " + activeUserRole + " user");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method to create an instant alert when a Light on any hub turns ON/OFF, 
	 * 				verify push notification, dismiss push notification and delete alert.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA92_CreateAndValidateInstantAlertForLight(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String alertMessage;
		String deviceStatus1;
		String deviceLightName;
		String alertName;
		String triggerState;
		String deviceStatus2;
		boolean alertPopupPresent = false; 
		boolean alertPresent = false;
		boolean pushNotification = true;
		boolean noDevicesPresent = false;

		deviceLightName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceLightName)
					.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("On") || triggerState.equalsIgnoreCase("Turned On")) {
						alertPageIOS.toggleLightTurnedOn("Enabled");
					} else if(triggerState.equalsIgnoreCase("Off") || triggerState.equalsIgnoreCase("Turned Off")){
						alertPageIOS.toggleLightTurnedOff("Enabled");
					}
					alertPageIOS.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceLightName,alertName);
					if (alertPresent) {
						extentTestIOS.log(Status.PASS, "Alert " + alertName + " created. Test Case " +scenarioName+ "Passed");
					} else {
						extentTestIOS.log(Status.FAIL, "Alert " + alertName + "not created. Test Case " +scenarioName+ "Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					//Added condition for checking the alert popup by performing action on the device
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceLightName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceLightName);
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceLightName);
							devicesPageIOS.actuatingDevice(deviceLightName);
						} else {
							devicesPageIOS.actuatingDevice(deviceLightName);	
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceLightName.toUpperCase())) {
								extentTestIOS.log(Status.PASS, " Alert Present as expected for the" +deviceLightName);
								extentTestIOS.log(Status.INFO, "Alert message is :" +alertMessage);
							} else {
								extentTestIOS.log(Status.FAIL, " Alert not Present for the expected device ");
							}
						} else { 
							extentTestIOS.log(Status.FAIL, "Alert Not Present for the expected Device");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceLightName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceLightName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceLightName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description  This is an iOS test method to create an instant alert when a GDO/CDO/Gate/VGDO on any hub Opens/Closes, 
	 * 				verify push notification, dismiss push notification and delete alert.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA92_CreateAndValidateInstantAlertForDevice(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String alertMessage;
		String deviceStatus1;
		String deviceStatus2;
		String deviceName;
		String alertName;
		String triggerState;
		boolean alertPopupPresent = false; 
		boolean alertPresent = false;
		boolean pushNotification = true;
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceName)
					.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("Opened") || triggerState.equalsIgnoreCase("Open")){
						alertPageIOS.toggleOpenedButton("Enabled");
					} else if(triggerState.equalsIgnoreCase("Closed") || triggerState.equalsIgnoreCase("Close")) {
						alertPageIOS.toggleClosedButton("Enabled");
					}
					alertPageIOS.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceName,alertName);
					if (alertPresent) {
						extentTestIOS.log(Status.PASS, "Alert " + alertName + " created. Test Case " +scenarioName+ "Passed");
					} else {
						extentTestIOS.log(Status.FAIL, "Alert " + alertName + "not created. Test Case " +scenarioName+ " Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					//Added condition for checking the alert popup by performing action on the device
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceName);
							devicesPageIOS.actuatingDevice(deviceName);
						} else {
							devicesPageIOS.actuatingDevice(deviceName);	
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceName.toUpperCase())) {
								extentTestIOS.log(Status.PASS, " Alert Present as expected for the" +deviceName);
								extentTestIOS.log(Status.INFO, "Alert message is :" +alertMessage);
							} else {
								extentTestIOS.log(Status.FAIL, " Alert not Present for the expected device ");
							}
						} else { 
							extentTestIOS.log(Status.FAIL, "Alert Not Present for the expected Device");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert");
					}
				}  else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method to verify the Error message on Alert Screen
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA124_ValidateErrorsOnAlertCreation(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceLightName;
		String alertName;
		int delayHourDuration = 0;
		int delayMinuteDuration = 0;
		boolean popupPresent = false;
		boolean createRegularAlert = true;
		boolean pushNotification = false;
		boolean noDevicesPresent = false;

		deviceLightName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Creating Alert
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceLightName)
					.setAlertName(alertName)
					.toggleLightTurnedOff("Enabled")
					.toggleImmediateOrDelayed(delayHourDuration,delayMinuteDuration)
					.toggleRegularOrScheduled(createRegularAlert)
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();

					//This function call is as per MTA-124 of validating error message
					popupPresent = 	alertPageIOS.validateErrorMessage();
					if (popupPresent) {
						extentTestIOS.log(Status.INFO, "Alert could not be saved. Error Popup Present");
					} else {
						extentTestIOS.log(Status.INFO, "Alert created without showing the error popup");	
					}
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method to create an alert on Future time and all days when a Light on any hub turns ON/OFF. This method creates an alert, 
		verifies that no push notification is received as PASS criteria, clears push notification if received and deletes alert at the end.
	 * @throws InterruptedException
	 */
	public void MTA157_CreateAndValidateAlertWithCustomTimeAndAllDaysForLight(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceLightName;
		String customAlertName;
		String triggerState;
		boolean alertPresent = false;
		boolean createRegularAlert = false;
		boolean pushNotification = true;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceLightName = testData.get(0).split(",")[0];
		customAlertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		//Creating Alert
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceLightName);
					alertPageIOS.setAlertName(customAlertName);
					if(triggerState.equalsIgnoreCase("On") || triggerState.equalsIgnoreCase("Turned On")) {
						alertPageIOS.toggleLightTurnedOn("Enabled");
					} else if(triggerState.equalsIgnoreCase("Off") || triggerState.equalsIgnoreCase("Turned Off")){
						alertPageIOS.toggleLightTurnedOff("Enabled");
					}
					alertPageIOS.toggleRegularOrScheduled(createRegularAlert)
					.setFromTime()
					.setToTime()
					.togglePushNotification(pushNotification);
					alertPageIOS.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceLightName, customAlertName);//change the alert name to more appropriate
					if (alertPresent) {
						extentTestIOS.log(Status.INFO, "Alert Created successfully");
					} else {
						extentTestIOS.log(Status.FAIL, "Alert could not be created");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					//performing action for device state change
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceLightName)) {

						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceLightName);
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceLightName);
							devicesPageIOS.actuatingDevice(deviceLightName);
						} else {
							devicesPageIOS.actuatingDevice(deviceLightName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							String alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceName.toUpperCase())) {
								extentTestIOS.log(Status.INFO, "Alert message is : "+alertMessage);
								extentTestIOS.log(Status.FAIL, " Alert notification not expected ,but received");
								utilityIOS.captureScreenshot(iOSDriver);
							} else {
								extentTestIOS.log(Status.PASS, "Alert notification not received as expected");
							}
						} else {
							extentTestIOS.log(Status.PASS, "Alert notification not received as expected");
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceLightName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceLightName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceLightName, customAlertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method to create an alert on Future time and all days when a GDO/CDO/Gate on any hub Opens/Closes. This method creates an alert, 
		           verifies that no push notification is received as PASS criteria, clears push notification if received and deletes alert at the end.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA157_CreateAndValidateAlertWithCustomTimeAndAllDaysForDevice(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceName;
		String customAlertName;
		String triggerState;
		boolean alertPresent = false;
		boolean createRegularAlert = false;
		boolean pushNotification = true;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		customAlertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Creating Alert
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceName);
					alertPageIOS.setAlertName(customAlertName);
					if(triggerState.equalsIgnoreCase("Opened") || triggerState.equalsIgnoreCase("Open")){
						alertPageIOS.toggleOpenedButton("Enabled");
					} else if(triggerState.equalsIgnoreCase("Closed") || triggerState.equalsIgnoreCase("Close")) {
						alertPageIOS.toggleClosedButton("Enabled");
					}
					alertPageIOS.toggleRegularOrScheduled(createRegularAlert)
					.setFromTime()
					.setToTime()
					.togglePushNotification(pushNotification);
					alertPageIOS.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceName, customAlertName);//change the alert name to more appropriate
					if (alertPresent)
					{
						extentTestIOS.log(Status.INFO, "Alert Created successfully");
					} else {
						extentTestIOS.log(Status.FAIL, "Alert could not be created");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					//performing action for device state change
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceName);
							devicesPageIOS.actuatingDevice(deviceName);
						} else {
							devicesPageIOS.actuatingDevice(deviceName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							String alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceName.toUpperCase())) {
								extentTestIOS.log(Status.INFO, "Alert message is : "+alertMessage);
								extentTestIOS.log(Status.FAIL, " Alert notification not expected ,but received");
								utilityIOS.captureScreenshot(iOSDriver);
							} else {
								extentTestIOS.log(Status.PASS, "Alert notification not received as expected");
							}
						} else {
							extentTestIOS.log(Status.PASS, "Alert notification not received as expected");
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceName, customAlertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description: This is an iOS test method to create an instant alert for all days but today when a Light on any hub Turns On/Turns OFF. This method creates an alert, 
		            verifies that no push notification is received as PASS criteria, clears push notification if received and deletes alert at the end
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA153_CreateAndValidateAlertWithSpecificDaysForLight(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceLightName;
		String alertName;
		String triggerState;
		int delayHourDuration = 0;
		int delayMinuteDuration = 0;
		boolean alertPresent = false;
		boolean createRegularAlert = true;
		boolean pushNotification = true;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceLightName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Creating Alert
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
				alertPageIOS.selectDeviceForAlert(deviceLightName)
				.setAlertName(alertName);
				if(triggerState.equalsIgnoreCase("On") || triggerState.equalsIgnoreCase("Turned On")) {
					alertPageIOS.toggleLightTurnedOn("Enabled");
				} else if(triggerState.equalsIgnoreCase("Off") || triggerState.equalsIgnoreCase("Turned Off")){
					alertPageIOS.toggleLightTurnedOff("Enabled");
				}
				alertPageIOS	.toggleImmediateOrDelayed(delayHourDuration,delayMinuteDuration)
				.toggleRegularOrScheduled(createRegularAlert)
				.deselectCurrentDay()
				.togglePushNotification(pushNotification)
				.tapSaveAlertBtn();
				alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceLightName, alertName);
				if (alertPresent) {
					extentTestIOS.log(Status.INFO, "Alert Created successfully");
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceLightName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceLightName);
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceLightName);
							devicesPageIOS.actuatingDevice(deviceLightName);
						} else {
							devicesPageIOS.actuatingDevice(deviceLightName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (!alertPopupPresent) {
							extentTestIOS.log(Status.PASS, " Alert not present as expected for current day");
						} else {
							extentTestIOS.log(Status.FAIL, " Alert present for the current day");
							devicesPageIOS.getAlertMessage();
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Alert is not saved");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceLightName);
				extentTestIOS.log(Status.INFO, "Device state of "+deviceName+ "is "+deviceStatus2+"");
				menuPageIOS.tappingHamburgerMenu()
				.tappingAlerts();
				alertPageIOS.deleteAlert(deviceLightName, alertName);
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
				}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  Description: This is an iOS test method to create an instant alert for all days but today when a GDO/CDO/Gate on any hub Opens/Closes. This method creates an alert, 
		verifies that no push notification is received as PASS criteria, clears push notification if received and deletes alert at the end.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA153_CreateAndValidateAlertWithSpecificDaysForDevice(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceName;
		String alertName;
		String triggerState;
		int delayHourDuration = 0;
		int delayMinuteDuration = 0;
		boolean alertPresent = false;
		boolean createRegularAlert = true;
		boolean pushNotification = true;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Creating Alert
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceName)
					.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("Opened") || triggerState.equalsIgnoreCase("Open") ){
						alertPageIOS.toggleOpenedButton("Enabled");
					} else if(triggerState.equalsIgnoreCase("Closed") || triggerState.equalsIgnoreCase("Close")) {
						alertPageIOS.toggleClosedButton("Enabled");
					}
					alertPageIOS.toggleImmediateOrDelayed(delayHourDuration,delayMinuteDuration)
					.toggleRegularOrScheduled(createRegularAlert)
					.deselectCurrentDay()
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceName, alertName);
					if (alertPresent) {
						extentTestIOS.log(Status.INFO, "Alert Created successfully");
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices();
						if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
							deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
							if (deviceStatus1.equalsIgnoreCase(triggerState)) {
								devicesPageIOS.actuatingDevice(deviceName);
								devicesPageIOS.actuatingDevice(deviceName);
							} else {
								devicesPageIOS.actuatingDevice(deviceName);
							}
							alertPopupPresent = devicesPageIOS.validateAlert();
							if (!alertPopupPresent) {
								extentTestIOS.log(Status.PASS, " Alert not present as expected for current day");
							} else {
								extentTestIOS.log(Status.FAIL, " Alert present for the current day");
								devicesPageIOS.getAlertMessage();
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							extentTestIOS.log(Status.FAIL, " Expected device is not visible");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Alert is not saved");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method that created an alert delayed by 1 minutes when a Light on any hub Turns On/Off. This method creates an alert, 
	  	           verifies push notification, clears push notification if received and deletes alert at the end.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA155_CreateAndValidateDelayedAlertForLight(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceLightName;
		String alertName;
		String alertMessage;
		String triggerState;
		int delayHourDuration = 0;
		int delayMinuteDuration = 1;
		boolean pushNotification = true;
		boolean alertPresent = false;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceLightName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceLightName)
					.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("On") || triggerState.equalsIgnoreCase("Turned On")) {
						alertPageIOS.toggleLightTurnedOn("Enabled");
					} else if(triggerState.equalsIgnoreCase("Off") || triggerState.equalsIgnoreCase("Turned Off")){
						alertPageIOS.toggleLightTurnedOff("Enabled");
					}
					alertPageIOS.toggleImmediateOrDelayed(delayHourDuration,delayMinuteDuration)
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceLightName,alertName);
					if (alertPresent) {
						extentTestIOS.log(Status.PASS, "Alert created succcessfully");	
					} else {
						extentTestIOS.log(Status.FAIL, "Expected alert not created");	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();	
					if(devicesPageIOS.verifyDeviceNamePresent(deviceLightName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceLightName);
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceLightName);
							devicesPageIOS.actuatingDevice(deviceLightName);
						} else {
							devicesPageIOS.actuatingDevice(deviceLightName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							extentTestIOS.log(Status.FAIL, "Alert Present before the duration");
						} else {
							devicesPageIOS.validateDeviceTimer(deviceLightName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceLightName.toUpperCase()) && (alertMessage.contains("remained"))) {
								extentTestIOS.log(Status.PASS, "Alert present as expected  for the device "+deviceLightName);
							} else {
								extentTestIOS.log(Status.FAIL, "Alert shown is not either for the expected device or alert message is not proper");
							}
						} else {
							extentTestIOS.log(Status.FAIL, " Expected alert not present for the device");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceLightName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceLightName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceLightName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method that created an alert delayed by 1 minutes when a GDO/CDO/Gate on any hub Opens. This method creates an alert, 
	  	           verifies push notification, clears push notification if received and deletes alert at the end.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA155_CreateAndValidateDelayedAlertForDevice(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceName;
		String alertName;
		String alertMessage;
		String triggerState;
		int delayHourDuration = 0;
		int delayMinuteDuration = 1;
		boolean pushNotification = true;
		boolean alertPresent = false;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceName)
					.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("Open") || triggerState.equalsIgnoreCase("Opened")){
						alertPageIOS.toggleOpenedButton("Enabled");
					} else if(triggerState.equalsIgnoreCase("Closed") || triggerState.equalsIgnoreCase("Close")) {
						alertPageIOS.toggleClosedButton("Enabled");
					}
					alertPageIOS.toggleImmediateOrDelayed(delayHourDuration,delayMinuteDuration)
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceName,alertName);
					if (alertPresent) {
						extentTestIOS.log(Status.PASS, "Alert created succcessfully");	
					} else {
						extentTestIOS.log(Status.FAIL, "Expected alert not created");	
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
						extentTestIOS.log(Status.INFO, "Current State for Device "+deviceName+" is "+deviceStatus1+"");
						if (deviceStatus1.equalsIgnoreCase(triggerState)) {
							devicesPageIOS.actuatingDevice(deviceName);
							devicesPageIOS.actuatingDevice(deviceName);
						} else {
							devicesPageIOS.actuatingDevice(deviceName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							extentTestIOS.log(Status.FAIL, "Alert Present before the duration");
						} else {
							devicesPageIOS.validateDeviceTimer(deviceName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (alertPopupPresent) {
							alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceName.toUpperCase()) && (alertMessage.contains("remained"))) {
								extentTestIOS.log(Status.PASS, "Alert present as expected  for the device "+deviceName);
							} else {
								extentTestIOS.log(Status.FAIL, "Alert shown is not either for the expected device or alert message is not proper");
							}
						} else {
							extentTestIOS.log(Status.FAIL, " Expected alert not present for the device");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * DescriptionThis iOS test method creates an instant alert, Renames it and then Deletes it
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA127_ValidateRenamingAlert(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceName;
		String alertName;
		String alertRename;
		boolean pushNotification = true;
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		alertRename = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest, scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceName)
					.setAlertName(alertName)
					.toggleLightTurnedOn("Enabled")
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();

					if (alertPageIOS.verifyAlertAvailableForDevice(deviceName,alertName)) {
						extentTestIOS.log(Status.PASS, "Expected Alert was not present: New Alert created "+alertName);
					} else {
						extentTestIOS.log(Status.FAIL, "Expected Alert was not present: New Alert not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					alertPageIOS.tapAlert(deviceName, alertName)
					.setAlertName(alertRename)
					.tapBackBtn();

					if (alertPageIOS.verifyAlertAvailableForDevice(deviceName, alertRename)) {
						extentTestIOS.log(Status.PASS, "Alert name successfully changed from " + alertName + " to " + alertRename);
					} else {
						extentTestIOS.log(Status.FAIL, "Test MTA-127 :- " + alertName + " not renamed to " + alertRename + " : FAILED");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					alertPageIOS.deleteAlert(deviceName, alertRename);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to rename alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to rename alert, but it is not allowed");
					}
				} 
				else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				} 
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	} 

	/**
	 * Description This is an iOS test method to create an alert on Future time and all days when a Light on Wi-Fi hub/A-Hub/WGDO Turns On. This method creates an alert, verifies push notification, clears push notification if received and deletes alert at the end.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA401_CreateAndValidateInstantCurrentDayAlertForLight(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceStatus1;
		String deviceStatus2;
		String deviceLightName;
		String alertName;
		String triggerState;
		boolean alertPresent = false;
		boolean createRegularAlert = true;
		boolean pushNotification = true;
		boolean alertPopupPresent = false; 
		boolean noDevicesPresent = false;

		deviceLightName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];
		triggerState = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Creating Alert
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceLightName)
					.setAlertName(alertName);
					if(triggerState.equalsIgnoreCase("On") || triggerState.equalsIgnoreCase("Turned On")) {
						alertPageIOS.toggleLightTurnedOn("Enabled");
					} else if(triggerState.equalsIgnoreCase("Off") || triggerState.equalsIgnoreCase("Turned Off")){
						alertPageIOS.toggleLightTurnedOff("Enabled");
					}
					alertPageIOS.toggleRegularOrScheduled(createRegularAlert)
					.selectCurrentDay()
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(deviceLightName, alertName);
					if (alertPresent) {
						extentTestIOS.log(Status.INFO, "Alert Created successfully");
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices();
						if(devicesPageIOS.verifyDeviceNamePresent(deviceLightName)) {
							deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceLightName);
							extentTestIOS.log(Status.INFO, "Current State for Device "+deviceLightName+" is "+deviceStatus1+"");
							if (deviceStatus1.equalsIgnoreCase("ON")) {
								devicesPageIOS.actuatingDevice(deviceLightName);
								devicesPageIOS.actuatingDevice(deviceLightName);
							} else {
								devicesPageIOS.actuatingDevice(deviceLightName);
							}
							alertPopupPresent = devicesPageIOS.validateAlert();
							if (alertPopupPresent) {
								devicesPageIOS.getAlertMessage();
								extentTestIOS.log(Status.PASS, " Alert present as expected for current day");
							} else {
								extentTestIOS.log(Status.FAIL, " Alert not present for the current day");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Alert is not saved");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(deviceLightName);
					extentTestIOS.log(Status.INFO, "Device state of "+deviceLightName+ "is "+deviceStatus2+"");
					menuPageIOS.tappingHamburgerMenu()
					.tappingAlerts();
					alertPageIOS.deleteAlert(deviceLightName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create alert, but it is not allowed");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This iOS test method creates an instant alert, Disable it and then Deletes it
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA131_ValidateDisablingAlert(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceName;
		String deviceStatus1;
		String alertName;
		boolean pushNotification = true;
		boolean enableAlert = false;
		boolean alertPopupPresent = false;
		boolean isAlertEnabled = false;
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();

				//verify if alert exist or not
				if (!(alertPageIOS.validateNoAlertText()) && (alertPageIOS.verifyAlertAvailableForDevice(deviceName, alertName))) {
					extentTestIOS.log(Status.INFO, "Expected Alert was present: "+alertName);
				} else {
					extentTestIOS.log(Status.INFO, "Expected Alert was not present: "+alertName );

					alertPageIOS.tapAddAlert();
					noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
					if(!noDevicesPresent) {
						alertPageIOS.selectDeviceForAlert(deviceName)
						.setAlertName(alertName)
						.toggleLightTurnedOn("Enabled")
						.togglePushNotification(pushNotification)
						.tapSaveAlertBtn();

						if (alertPageIOS.verifyAlertAvailableForDevice(deviceName,alertName)) {
							extentTestIOS.log(Status.PASS, "Expected Alert was not present: New Alert created "+alertName);
						} else {
							extentTestIOS.log(Status.FAIL, "Expected Alert was not present: New Alert not created");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}

					if (alertPageIOS.verifyAlertAvailableForDevice(deviceName, alertName)) {
						isAlertEnabled = alertPageIOS.toggleDisableEnableAlert(deviceName, alertName, enableAlert);
						if (!isAlertEnabled && !enableAlert) {
							extentTestIOS.log(Status.PASS, "Alert " + alertName + " Disabled. Test Case MTA-131 of Disabling the alert Passed");
						} else if (isAlertEnabled && enableAlert) {
							extentTestIOS.log(Status.PASS, "Alert " + alertName + " Enabled. Test Case MTA-131 of Enabling the alert Passed");	
						} else {
							extentTestIOS.log(Status.FAIL, "Alert " + alertName + " state not changed. Test Case MTA-131 of Enabling/Disabling the alert Failed");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Alert " + alertName + "not Present. Test Case MTA-131 of Disabling the alert Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
						deviceStatus1 = devicesPageIOS.getDeviceStatus(deviceName);
						if (deviceStatus1.equalsIgnoreCase("ON")) {
							devicesPageIOS.actuatingDevice(deviceName);
							devicesPageIOS.actuatingDevice(deviceName);
						} else {
							devicesPageIOS.actuatingDevice(deviceName);
						}
						alertPopupPresent = devicesPageIOS.validateAlert();
						if (!alertPopupPresent) {
							extentTestIOS.log(Status.PASS, " Alert not present as expected");
						} else { 
							String alertMessage = devicesPageIOS.getAlertMessage();
							if (alertMessage.toUpperCase().contains(deviceName.toUpperCase())) {
								extentTestIOS.log(Status.INFO, "Alert message is : "+alertMessage);
								extentTestIOS.log(Status.FAIL, "Alert popup not expected, but present");
								utilityIOS.captureScreenshot(iOSDriver);
							} else {
								extentTestIOS.log(Status.PASS, "Alert popup Not Present for the expected Device");
							}
						}
					} else {
						extentTestIOS.log(Status.FAIL, " Expected device is not visible");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingAlerts();
					alertPageIOS.deleteAlert(deviceName, alertName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Disable alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Disable alert, but it is not allowed");
					}else {
						extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
					} 
				} 
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This iOS test method creates an instant alert,and then verifies the Delete Alert functionality by deleting the Alert
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA129_ValidateDeletingAlert(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String deviceName;
		String alertName;
		boolean pushNotification = true;
		boolean noDevicesPresent = false;

		deviceName = testData.get(0).split(",")[0];
		alertName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Alerts")) {
				menuPageIOS.tappingAlerts();
				//verify if alert exist or not
				alertPageIOS.tapAddAlert();
				noDevicesPresent = alertPageIOS.validateNoDeviceAlert();
				if(!noDevicesPresent) {
					alertPageIOS.selectDeviceForAlert(deviceName)
					.setAlertName(alertName)
					.toggleLightTurnedOn("Enabled")
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();

					if (alertPageIOS.verifyAlertAvailableForDevice(deviceName,alertName)) {
						extentTestIOS.log(Status.PASS, "Expected Alert was not present: New Alert created "+alertName);
					} else {
						extentTestIOS.log(Status.FAIL, "Expected Alert was not present: New Alert not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					//verify if alert is deleted or not
					if (alertPageIOS.deleteAlert(deviceName, alertName)) {
						extentTestIOS.log(Status.PASS, "Alert " + alertName + " Deleted. Test Case MTA-129 of Deleting the alert Passed");
					} else {
						extentTestIOS.log(Status.FAIL, "Alert " + alertName + "not Deleted. Test Case MTA-129 of Deleting the alert Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Delete alert as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Delete alert, but it is not allowed");
					} else {
						extentTestIOS.log(Status.FAIL, "There are no devices added in the account");
					} 
				} 
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  alert link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  alerts link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}		

	/**
	 *  Description :This is an iOS test method that validates schedule notification is not received when the Schedule is created for the state same as the current state of the device. The method verifies device state, 
	    	            Creates Schedule, validates push notification is not received and then deletes the schedule.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA488_CreateAndValidateScheduleNotTriggered(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String scheduleName;
		String deviceName;
		String deviceState1;
		boolean enableNotification = true;
		boolean allDays = true;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
				if(!devicesPageIOS.verifyYellowBarOnDevice(deviceName)) {
					extentTestIOS.log(Status.INFO, "Current State for Device "+deviceName+" is "+devicesPageIOS.getDeviceStatus(deviceName)+"");
					if(!devicesPageIOS.getDeviceStatus(deviceName).toUpperCase().contains(deviceState1.toUpperCase())) {
						devicesPageIOS.actuatingDevice(deviceName);
					} else {
						extentTestIOS.log(Status.INFO, "Device state is as expected");
					}
					menuPageIOS.tappingHamburgerMenu();
					if (menuPageIOS.verifyMenuLink("Schedules")) {
						menuPageIOS.tappingSchedules();
						schedulePageIOS.tapAddScheduleBtn()
						.tapCancelBtn()
						.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
						if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
							extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
							menuPageIOS.tappingHamburgerMenu()
							.tappingDevices();
							if (!schedulePageIOS.validateScheduleTrigger()) {
								extentTestIOS.log(Status.PASS,  "Since the device was CLOSED "+scheduleName +" not triggered as expected");
							} else {
								extentTestIOS.log(Status.FAIL, scheduleName + "got triggered, when the device was in CLOSED state ");
								devicesPageIOS.getAlertMessage();
							}
							menuPageIOS.tappingHamburgerMenu()
							.tappingSchedules();
							schedulePageIOS.deleteSchedule(scheduleName);
						} else {
							extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
						} else if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
							utilityIOS.captureScreenshot(iOSDriver);
						} 
						// To bring back to the Home screen
						menuPageIOS.tappingDevices();
					}
				}else {
					extentTestIOS.log(Status.FAIL, "Device is in offline / error state");
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected device is not visible");
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method that creates a schedule for a Light/GDO/CDO/GATE/VGDO to Close/On after 2 minutes of current system time. The method creates a schedule, 
	  	verifies expected device state,verifies push notification, clears push notification if received, verifies record in history and deletes the schedule.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA35_CreateAndValidateScheduleForDeviceAndHistory(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String scheduleName;
		String deviceName1;
		String deviceState1;
		boolean enableNotification = true;
		boolean allDays = true;
		boolean scheduleTriggered = false;
		String eventName;
		String eventTime = null;

		scheduleName = testData.get(0).split(",")[0];
		deviceName1 = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName1, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(deviceName1)) {
				extentTestIOS.log(Status.INFO, "Current State for Device "+deviceName1+" is "+devicesPageIOS.getDeviceStatus(deviceName1)+"");
				if(!devicesPageIOS.verifyYellowBarOnDevice(deviceName1)) {
					if(deviceState1.equalsIgnoreCase("Close")) {
						deviceState1 = "Closed";
					}
					if(deviceState1.equalsIgnoreCase("Turn On")) {
						deviceState1 = "On";
						}
					if(devicesPageIOS.getDeviceStatus(deviceName1).toUpperCase().contains(deviceState1.toUpperCase())) {
						devicesPageIOS.actuatingDevice(deviceName1);
					} else {
						extentTestIOS.log(Status.INFO, "Device state is as expected");
					}
					menuPageIOS.tappingHamburgerMenu();
					if (menuPageIOS.verifyMenuLink("Schedules")) {
						menuPageIOS.tappingSchedules();
						schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
						if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
							extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
							menuPageIOS.tappingHamburgerMenu()
							.tappingDevices();	

							if (schedulePageIOS.validateScheduleTrigger()) {
								utilityIOS.captureScreenshot(iOSDriver);
								if (devicesPageIOS.getAlertMessage().contains(scheduleName)) {
									extentTestIOS.log(Status.PASS,  scheduleName +" Triggered successfully");
									scheduleTriggered = true;
								}
							} else {
								extentTestIOS.log(Status.FAIL, scheduleName + " did not triggered");
							}
							eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
							Set set = deviceStatePairList.entrySet();
							Iterator iterator = set.iterator();
							while(iterator.hasNext()) {
								Map.Entry deviceStatePair = (Map.Entry)iterator.next();
								String deviceName = (String) deviceStatePair.getKey();
								String deviceState = (String) deviceStatePair.getValue();
								if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
									if (devicesPageIOS.validateDeviceStatus(deviceName,deviceState)) {
										extentTestIOS.log(Status.PASS, "State of " +deviceName + " changed successfully"); 
										scheduleTriggered = true;
									} else {
										extentTestIOS.log(Status.FAIL, "State of " +deviceName + "did not changed to " +deviceState);
										utilityIOS.captureScreenshot(iOSDriver);
									}
								} else {
									extentTestIOS.log(Status.FAIL, " Expected device is not visible");
									utilityIOS.captureScreenshot(iOSDriver);
								}
							}
							if (scheduleTriggered) { // Check for history only if Schedule is triggered or device state is changed
								eventName = scheduleName + " was triggered";
								menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
								if (activeUserRole.equalsIgnoreCase("Guest")) {
									if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
										extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
										menuPageIOS.tappingDevices();// To close the menu options
									} else {
										extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
										utilityIOS.captureScreenshot(iOSDriver);
									}
								} else {	// Verify history for the remaining user types
									menuPageIOS.tappingHistory();
									if (historyPageIOS.verifyHistory(eventName,eventTime)) {
										extentTestIOS.log(Status.PASS, "History event name: '" + eventName +"' is logged successfully");	
									} else {
										extentTestIOS.log(Status.FAIL, "History event name: '" + eventName +"' is not logged ");	
										utilityIOS.captureScreenshot(iOSDriver);
									}
									extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
								}
							}
						} else {
							extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						menuPageIOS.tappingHamburgerMenu()
						.tappingSchedules();
						schedulePageIOS.deleteSchedule(scheduleName);
						if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
						} else if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
							utilityIOS.captureScreenshot(iOSDriver);
						} 
						// To bring back to the Home screen
						menuPageIOS.tappingDevices();
					} 
				}else {
					extentTestIOS.log(Status.FAIL, "Device is in offline / error state");
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected device is not visible");
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description To validate the Error message while creating schedule
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA42_ValidateErrorOnScheduleCreation(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String scheduleName;
		String deviceName1;
		String deviceState1;
		String ErrorPopupMsg;
		boolean WeekSelection = true;

		scheduleName = testData.get(0).split(",")[0];
		deviceName1 = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				schedulePageIOS.tapAddScheduleBtn()
				.enterScheduleName("")
				.tapSaveBtn();
				if (schedulePageIOS.validateErrorMessage()) {
					ErrorPopupMsg = schedulePageIOS.getErrorMessage();
					if (ErrorPopupMsg.contains("Please enter a name for the schedule.")) {
						extentTestIOS.log(Status.INFO, "Error popup present while entering the device name ");
					} else {
						extentTestIOS.log(Status.FAIL, "Different Popup message shown, the text is: " +ErrorPopupMsg);
						//here screenshot is not captured because alert is dismissed in the previous step and taking screenshot won't capture the error scenario
					}
				}
				schedulePageIOS.enterScheduleName(scheduleName);
				schedulePageIOS.tapSaveBtn();
				if (schedulePageIOS.validateErrorMessage()) {
					ErrorPopupMsg = schedulePageIOS.getErrorMessage();
					if (ErrorPopupMsg.contains("Please select a device and state for the schedule")) {
						extentTestIOS.log(Status.INFO, "Error popup present for selecting the device and state");
					} else {
						extentTestIOS.log(Status.FAIL, "Different Popup message shown, the text is: " +ErrorPopupMsg);
						//here screenshot is not captured because alert is dismissed in the previous step and taking screenshot won't capture the error scenario
					}
				}
				schedulePageIOS.selectDeviceStateForSchedule(deviceName1, deviceState1);
				schedulePageIOS.SelectAllOrNoDays(!WeekSelection)
				.tapSaveBtn();
				if (schedulePageIOS.validateErrorMessage()) {
					ErrorPopupMsg = schedulePageIOS.getErrorMessage();
					if (ErrorPopupMsg.contains("Please select one or more days that you would like the scheduled events to occur.")) {
						extentTestIOS.log(Status.INFO, "Error popup present for No days selected");
					} else {
						extentTestIOS.log(Status.FAIL, "Different Popup message shown, the text is: " +ErrorPopupMsg);
						//here screenshot is not captured because alert is dismissed in the previous step and taking screenshot won't capture the error scenario
					}
				}
				schedulePageIOS.SelectAllOrNoDays(WeekSelection);
				schedulePageIOS.tapSaveBtn();
				if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
					extentTestIOS.log(Status.PASS, "Schedule with name " + scheduleName + " created");	
				} else {
					extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + " not created");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				menuPageIOS.tappingHamburgerMenu()
				.tappingSchedules();
				schedulePageIOS.deleteSchedule(scheduleName);
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to create Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to create Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method that creates a schedule for a Light/GDO/CDO/Gate/VGDO ‘ON/Open/Close’ on all days but today, 
	  	after 2 minutes of current system time. The method creates a schedule, verifies that no push notification is received as a PASS criteria, 
	  	clears push notification if received and deletes the schedule
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA39_CreateAndValidateSpecificDayScheduleForDevice(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		boolean enableNotification=true;
		String scheduleName;
		String deviceName;
		String deviceState;
		boolean allDays = false;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		deviceState = testData.get(0).split(",")[2];

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState);

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
				if(!devicesPageIOS.verifyYellowBarOnDevice(deviceName)) {
					if(deviceState.equalsIgnoreCase("Close")) {
						deviceState = "Closed";
					}
					if(devicesPageIOS.getDeviceStatus(deviceName).toUpperCase().contains(deviceState.toUpperCase())) {
						devicesPageIOS.actuatingDevice(deviceName);
					} else {
						extentTestIOS.log(Status.INFO, "Device state is as expected");
					}
					menuPageIOS.tappingHamburgerMenu();
					if (menuPageIOS.verifyMenuLink("Schedules")) {
						menuPageIOS.tappingSchedules();
						schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification, allDays);
						if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
							extentTestIOS.log(Status.PASS, "Schedule with name " + scheduleName + " created");
							menuPageIOS.tappingHamburgerMenu()
							.tappingDevices();	
							if (!schedulePageIOS.validateScheduleTrigger()) {
								extentTestIOS.log(Status.PASS, "Schedule Did not get Triggered as expected");
							} else if (!(devicesPageIOS.getAlertMessage().contains(scheduleName))) {
								extentTestIOS.log(Status.INFO, "Popup present is not for the schedule");
							} else {
								extentTestIOS.log(Status.FAIL, "Schedule triggered. Test case of custom schedule for specific days failed");
								//here screenshot is not captured because alert is dismissed in the previous step and taking screenshot won't capture the error scenario
							}
							menuPageIOS.tappingHamburgerMenu()
							.tappingSchedules();
							schedulePageIOS.deleteSchedule(scheduleName);
						} else {
							extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to create Schedule as Expected");
						} else if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to create Schedule, but it is not allowed");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
							utilityIOS.captureScreenshot(iOSDriver);
						} 
						// To bring back to the Home screen
						menuPageIOS.tappingDevices();
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Device is in offline / error state");
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected device is not visible");
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method that creates a schedule for a Device after 2 minutes of current system time, saves it and then Remove the added device from the schedule
	   and adds a new device ,set new time after 2 minutes the current system time and saves the schedule, Verifies that Schedule is triggered and the state of the Device is as expected
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA163_ValidateRemoveDeviceFromSchedule(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enableNotification=true;
		String scheduleName;
		String deviceName1;
		String deviceName2;
		String deviceState1;
		String deviceState2;
		boolean allDays = true;

		scheduleName = testData.get(0).split(",")[0];
		deviceName1 = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];
		deviceName2 = testData.get(0).split(",")[3];
		deviceState2 = testData.get(0).split(",")[4];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		HashMap<String, String> deviceStatePairList1 = new HashMap<String, String>();
		deviceStatePairList1.put(deviceName1, deviceState1);

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			if(devicesPageIOS.getDeviceStatus(deviceName2).toUpperCase().contains(deviceState2.toUpperCase())) {
				devicesPageIOS.actuatingDevice(deviceName2);
			} else {
				extentTestIOS.log(Status.INFO, "Device state is as expected");
			}
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				schedulePageIOS.createSchedule(scheduleName, deviceStatePairList1, enableNotification, allDays);
				if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
					schedulePageIOS.tapSchedule(scheduleName)
					.removeDeviceFromSchedule()
					.selectDeviceStateForSchedule(deviceName2, deviceState2)
					.setTime()
					.tapBackBtn();
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "Schedule modified successfully");
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule not modified");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if (schedulePageIOS.validateScheduleTrigger() && (devicesPageIOS.getAlertMessage().contains(scheduleName))) {
						extentTestIOS.log(Status.PASS, "Schedule got Triggered as expected");
					} else { 
						extentTestIOS.log(Status.FAIL, "Schedule did not trigger");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					if (devicesPageIOS.validateDeviceStatus(deviceName2,deviceState2)) {
						extentTestIOS.log(Status.PASS, "State of " +deviceName2 + " changed successfully"); 
					} else {
						extentTestIOS.log(Status.FAIL, "State of " +deviceName2 + " did not changed" );
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Schedule not created successfully");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to create Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to create Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This iOS method Creates the schedule, Rename it, check the renamed Schedule and then delete the schedule
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA47_ValidateRenamingSchedule(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String scheduleName;
		String scheduleRename;
		String deviceName;
		String deviceState1;
		boolean enableNotification = false;
		boolean allDays = true;

		deviceName = testData.get(0).split(",")[0];
		deviceState1 = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];
		scheduleRename = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				//check for schedule is present or not 
				if (!(schedulePageIOS.validateNoScheduleText()) && (schedulePageIOS.verifySchedulePresent(scheduleName))) {
					extentTestIOS.log(Status.INFO, "Expected Schedule was present: "+scheduleName);
				} else {
					extentTestIOS.log(Status.INFO, "Expected Schedule was not present: "+scheduleName);
					schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "Expected Schedule was not present: New Schedule created "+scheduleName);
					} else {
						extentTestIOS.log(Status.FAIL, "Expected Schedule was not present: New Schedule not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				}
				//rename the schedule
				schedulePageIOS.tapSchedule(scheduleName)
				.enterScheduleName(scheduleRename)
				.tapBackBtn();
				if (schedulePageIOS.verifySchedulePresent(scheduleRename)) {
					extentTestIOS.log(Status.PASS, "Schedule with name " + scheduleRename + " present");
					schedulePageIOS.tapSchedule(scheduleRename)
					.enterScheduleName(scheduleName)
					.tapBackBtn();
					schedulePageIOS.deleteSchedule(scheduleName);
				} 
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Rename Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Rename Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}	

	/**
	 * Description This iOS method Creates the schedule, then delete the schedule
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA55_ValidateDeletingSchedule(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String scheduleName;
		String deviceName;
		String deviceState1;
		boolean enableNotification = true;
		boolean allDays = true;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		//navigating to schedule section

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				//check for schedule is present or not 
				if (!(schedulePageIOS.validateNoScheduleText()) && (schedulePageIOS.verifySchedulePresent(scheduleName))) {
					extentTestIOS.log(Status.INFO, "Expected Schedule was present: "+scheduleName);
				} else {
					extentTestIOS.log(Status.INFO, "Expected Schedule was not present: "+scheduleName);
					schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "Expected Schedule was not present: New Schedule created "+scheduleName);
					} else {
						extentTestIOS.log(Status.FAIL, "Expected Schedule was not present: New Schedule not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				}
				if (schedulePageIOS.deleteSchedule(scheduleName)) {
					extentTestIOS.log(Status.PASS, "Schedule with name " + scheduleName + " deleted successfully");	
				} else {
					extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + " could not be deleted");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Delete Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Delete Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	} 

	/**
	 * Description This iOS method Creates the schedule, then Disables the schedule
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA51_ValidateDisablingSchedule(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String scheduleName;
		String deviceName;
		String deviceState1;
		boolean enableSchedule = false;
		boolean enableNotification = true;
		boolean allDays = true;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		//navigating to schedule section
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				//check for schedule is present or not 
				if (!(schedulePageIOS.validateNoScheduleText()) && (schedulePageIOS.verifySchedulePresent(scheduleName))) {
					extentTestIOS.log(Status.INFO, "Expected Schedule was present: "+scheduleName);
				} else {
					extentTestIOS.log(Status.INFO, "Expected Schedule was not present: "+scheduleName);
					schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "Expected Schedule was not present: New Schedule created "+scheduleName);
					} else {
						extentTestIOS.log(Status.FAIL, "Expected Schedule was not present: New Schedule not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				}
				schedulePageIOS.tapSchedule(scheduleName)
				.setTime()
				.tapBackBtn()
				.toggleDisableEnableAlert(scheduleName, enableSchedule);

				if (!schedulePageIOS.validateScheduleTrigger()) {
					extentTestIOS.log(Status.PASS, "MTA-55:Schedule Did not get Triggered as expected.Test case of Disable Schedule Passed");
				} else if (!(devicesPageIOS.getAlertMessage().contains(scheduleName))) {
					extentTestIOS.log(Status.INFO, "Popup present is not for the schedule");
				} else {
					extentTestIOS.log(Status.FAIL, "MTA-55:Schedule triggered. Test case of Disable Schedule Failed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				schedulePageIOS.deleteSchedule(scheduleName);
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Disable Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Disable Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description: This is an iOS test method to send an invite with ‘Admin/Family/Guest’ privileges to the specified email id.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA59_ValidateAddUser(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String email;
		String role;

		email = testData.get(0).split(",")[0];
		role = testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				manageUsersPageIOS.tapAddBtn();
				if (!manageUsersPageIOS.validateErrorPopup()) {
					manageUsersPageIOS.enterUserEmail(email)
					.reEnterUserEmail(email)
					.openRolesDropDown()
					.selectRole(role)
					.tapSendBtn();

					if (!manageUsersPageIOS.validateErrorPopup()) {
						manageUsersPageIOS.tapOkayBtn();
						if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(email)) {
							extentTestIOS.log(Status.PASS, "Invitation sent successfully to the user: " +email);
							if(role.equalsIgnoreCase("Admin")) {
								adminInvitationSent = true;
							} else if(role.equalsIgnoreCase("Family")) {
								familyInvitationSent = true;
							} else if(role.equalsIgnoreCase("Guest")) {
								guestInvitationSent = true;
							}
						} else {
							extentTestIOS.log(Status.FAIL, "Invitation is not sent to the desired user");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						manageUsersPageIOS.getErrorMessage();
						manageUsersPageIOS.tapCancelBtn();
						extentTestIOS.log(Status.FAIL, "Invitation could not be sent to the desired user");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					manageUsersPageIOS.getErrorMessage();
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Send invite as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to send invite, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description: This is an iOS test method to accept or decline an invitation with Admin/Family/Guest privileges to creator’s account.
	   The test method logs out of creator’s account, logs in to the invitation recipient’s account and accepts/decline the invitation (if received) as per test input data.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA70_ValidateUserInvitationStatus(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String email;
		String password;
		String userName;
		String role;
		String invitationDecision;

		email = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];
		userName = testData.get(0).split(",")[2];
		role = testData.get(0).split(",")[3];
		invitationDecision = testData.get(0).split(",")[4];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.logout();
		iOSDriver.resetApp();
		loginPageIOS.login(email, password);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		//Verify Badge icon for invitation sent - MTA-300
		if (userAccountPageIOS.verifyAccountPage(email)) {
			if(userAccountPageIOS.verifyBadgeIcon() && (adminInvitationSent || familyInvitationSent || guestInvitationSent)) {
				extentTestIOS.log(Status.PASS, "Badge Icon shown for the user");
			} else {
				if(activeUserRole.equalsIgnoreCase("Family")||activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, "Badge Icon not shown for the user");
				} else {
				extentTestIOS.log(Status.FAIL, "Badge Icon not shown for the user");
				utilityIOS.captureScreenshot(iOSDriver);
				}
			}
			userAccountPageIOS.tapAccountInvitationsLink();
			if (!userAccountPageIOS.validateNoAccountInvitation() && userAccountPageIOS.verifyPendingInvite(userName, role)) {
				userAccountPageIOS.tapInvite(userName, role);
				if (invitationDecision.equalsIgnoreCase("Accept")) {
					userAccountPageIOS.acceptorDeclineInvitation(true);	
				} else {
					userAccountPageIOS.acceptorDeclineInvitation(false);	
				}
				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();
				if (invitationDecision.equalsIgnoreCase("Accept") && devicesPageIOS.verifyBlueBarPresent()) {
					devicesPageIOS.clickOnBlueBar();
					if (devicesPageIOS.verifyUserPresent(userName, role)) {
						devicesPageIOS.selectUserRole(userName, role);
						extentTestIOS.log(Status.PASS, "Successfully switched to the accepted invite");
						expectedRolePresent = true;
					} else {
						devicesPageIOS.tapOnCancelBtn();
						extentTestIOS.log(Status.FAIL, "The Accepted invite is not shown in this account");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else if (!invitationDecision.equalsIgnoreCase("Accept")) {
					extentTestIOS.log(Status.INFO, "Since the invitation is rejected verification of the switching user role is skipped");
				}
			} else { 
				if(!(adminInvitationSent || familyInvitationSent || guestInvitationSent)) {
					extentTestIOS.log(Status.PASS, "As invitation was not sent earlier the invitation is not available");
				} else {
					extentTestIOS.log(Status.FAIL, "Expected invitation not Available");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				userAccountPageIOS.tapBackBtn();
			}
		}
		else {
			extentTestIOS.log(Status.FAIL, "User Profile is not of the expected user");
			utilityIOS.captureScreenshot(iOSDriver);
		}
	}
	
	/**
	 * Description This method verifies that stronghold GDO shield icon is shown for the Admin/Family/Guest user
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA268_ValidateShieldIconLockForInvitedUser(ArrayList<String> testData, String deviceTest,String scenarioName)  throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException  {

		String roleUserName;
		String gdoSerialNumber;
		String strGdoName;
		String role;

		roleUserName = testData.get(0).split(",")[0];
		role = testData.get(0).split(",")[1];
		gdoSerialNumber = testData.get(0).split(",")[2];
		strGdoName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			if((adminInvitationSent || familyInvitationSent || guestInvitationSent)) {
				if(devicesPageIOS.verifyBlueBarPresent()){
					devicesPageIOS.clickOnBlueBar()
					.selectUserRole(roleUserName, role);
					menuPageIOS.tappingHamburgerMenu();
					if (menuPageIOS.verifyMenuLink("Devices")) {
						menuPageIOS.tappingDevices();
						if(devicesPageIOS.verifyDeviceNamePresent(strGdoName)) {
							webService.attachLockGDO(gdoSerialNumber);
							devicesPageIOS.verifyAlertForLock();
							if(devicesPageIOS.verifyLockOnSGDO(strGdoName)) {
								extentTestIOS.log(Status.PASS, "Lock attached on the StrongHold GDO for "+role+" user");
							} else {
								extentTestIOS.log(Status.FAIL, "Lock not available on the StrongHold GDO for "+role+" user");
								utilityIOS.captureScreenshot(iOSDriver);
							}	
							webService.detachLockGDO(gdoSerialNumber);
							menuPageIOS.tappingHamburgerMenu();
							menuPageIOS.tappingDevices();
							if(!devicesPageIOS.verifyLockOnSGDO(strGdoName)) {
								extentTestIOS.log(Status.PASS, "Lock is not available on the StrongHold GDO for "+role+" user");
							} else {
								extentTestIOS.log(Status.FAIL, "Lock attached on the StrongHold GDO for "+role+" user");
								utilityIOS.captureScreenshot(iOSDriver);
							}	
						} else {
							extentTestIOS.log(Status.FAIL, "Expected device not present");
						}
						if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to verify StrongHold Lock");
						} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to verify StrongHold Lock, but it is not allowed");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
							extentTestIOS.log(Status.FAIL, "Expected Devices section should be available, but not present to " + activeUserRole + " user");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}
				} else {
					if((adminInvitationSent || familyInvitationSent || guestInvitationSent)){
						extentTestIOS.log(Status.FAIL, "Blue bar not present");
						utilityIOS.captureScreenshot(iOSDriver);
					} else {
						extentTestIOS.log(Status.PASS, "As invitation was not sent earlier the invitation is not shown");
					}	
					extentTestIOS.log(Status.FAIL, "Blue bar not present");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				extentTestIOS.log(Status.INFO, "User was not invited");
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method that logs in to the creator account change the existing user role as specified in test data.
		logs in to the invitation recipient’s account and verifies the changed role logs out, logs back in to the creator account and the reverts back the user role to earlier role
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA67_ValidateChangeUserRole(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String hostEmail, invitedUserEmail;
		String hostPassword, invitedUserPassword;
		String hostUserName, invitedUserName;
		String initialRole,changedRole;

		hostEmail = testData.get(0).split(",")[0];
		hostPassword = testData.get(0).split(",")[1];
		hostUserName = testData.get(0).split(",")[2];
		initialRole = testData.get(0).split(",")[3];
		changedRole = testData.get(0).split(",")[4];
		invitedUserEmail = testData.get(0).split(",")[5];
		invitedUserPassword = testData.get(0).split(",")[6];
		invitedUserName = testData.get(0).split(",")[7];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.logout();
		iOSDriver.resetApp();

		loginPageIOS.login(hostEmail, hostPassword);
		if (devicesPageIOS.verifyBlueBarPresent()) {
			devicesPageIOS.clickOnBlueBar();
			if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
				devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
				expectedRolePresent = true;
			} else {
				expectedRolePresent = false;	
				devicesPageIOS.tapOnCancelBtn();
			}
		} else {
			if (activeUserRole.equalsIgnoreCase("Creator")) {
				expectedRolePresent = true;
			} else {
				expectedRolePresent = false;
			}
		}
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				if (!manageUsersPageIOS.validateNoAddedUser() && (manageUsersPageIOS.verifyAddedUser(hostUserName, initialRole))) {
					manageUsersPageIOS.tapOnUser(hostUserName, initialRole)
					.openRolesDropDown()
					.selectRole(changedRole)
					.tapBackBtn();

					//.tapConfirmationPopup();
					menuPageIOS.logout();
					iOSDriver.resetApp();
					loginPageIOS.login(invitedUserEmail, invitedUserPassword);
					menuPageIOS.tappingHamburgerMenu()
					.tappingUserProfile();
					userAccountPageIOS.tapAccountInvitationsLink();
					if (userAccountPageIOS.verifyAcceptedInvite(invitedUserName, changedRole)) {
						extentTestIOS.log(Status.PASS, "Role Changed successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, "Role did not changes to expected role" +changedRole);	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					userAccountPageIOS.tapBackBtn();
				} else {
					extentTestIOS.log(Status.FAIL, "Expected user with expected role not available");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				menuPageIOS.logout();
				iOSDriver.resetApp();
				loginPageIOS.login(hostEmail, hostPassword);
				if (devicesPageIOS.verifyBlueBarPresent()) {
					devicesPageIOS.clickOnBlueBar();
					if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
						devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
						expectedRolePresent = true;
					} else {
						expectedRolePresent = false;	
						devicesPageIOS.tapOnCancelBtn();
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Creator")) {
						expectedRolePresent = true;
					} else {
						expectedRolePresent = false;
					}
				}
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Users")) {
					menuPageIOS.tappingUsers();
					if (!manageUsersPageIOS.validateNoAddedUser() && (manageUsersPageIOS.verifyAddedUser(hostUserName, changedRole))) {
						manageUsersPageIOS.tapOnUser(hostUserName, changedRole)
						.openRolesDropDown()
						.selectRole(initialRole)
						.tapBackBtn();
					}
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Change user role Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Change user role, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method that Deletes the user for which the invitation  was accepted as Admin/Family/Guest's role in earlier MTA59_ValidateAddUser test method.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA82_ValidateInvitedUserIsDeleted(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String email;
		String password;
		String userName;
		String role;

		email = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];
		userName = testData.get(0).split(",")[2];
		role = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.logout();
		iOSDriver.resetApp();
		loginPageIOS.login(email, password);
		if (devicesPageIOS.verifyBlueBarPresent()) {
			devicesPageIOS.clickOnBlueBar();
			if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
				devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
				expectedRolePresent = true;
			} else {
				expectedRolePresent = false;	
				devicesPageIOS.tapOnCancelBtn();
			}
		} else {
			if (activeUserRole.equalsIgnoreCase("Creator")) {
				expectedRolePresent = true;
			} else {
				expectedRolePresent = false;
			}
		}
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				if (!manageUsersPageIOS.validateNoAddedUser() && (manageUsersPageIOS.verifyAddedUser(userName, role))) {
					manageUsersPageIOS.tapOnUser(userName, role)
					.tapDeleteBtn();
					if (manageUsersPageIOS.validateNoAddedUser() || !manageUsersPageIOS.verifyAddedUser(userName, role)) {
						extentTestIOS.log(Status.PASS, "User deleted successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, "User not deleted successfully");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					if((!adminInvitationSent && role.equalsIgnoreCase("Admin")) || (!familyInvitationSent && role.equalsIgnoreCase("Family")) || (!guestInvitationSent && role.equalsIgnoreCase("Guest"))) {
						extentTestIOS.log(Status.PASS, "As invitation was not sent earlier, the invitation is not shown and hence user could not be deleted");
					} else {
						extentTestIOS.log(Status.FAIL, "Expected invitation not Available");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Delete user role as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Delete user role, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description :This iOS test methods sends an invite and then deletes the invitation, verifies that the invitation is deleted successfully
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA184_ValidateDeleteInvitation(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String hostEmail;
		String hostPassword;
		String invitedEmail;
		String role;

		hostEmail = testData.get(0).split(",")[0];
		hostPassword = testData.get(0).split(",")[1];
		invitedEmail = testData.get(0).split(",")[2];
		role = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				manageUsersPageIOS.tapAddBtn();
				if (!manageUsersPageIOS.validateErrorPopup()) {
					manageUsersPageIOS.enterUserEmail(invitedEmail)
					.reEnterUserEmail(invitedEmail)
					.openRolesDropDown()
					.selectRole(role)
					.tapSendBtn()
					.tapOkayBtn();
					if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
						extentTestIOS.log(Status.PASS, "Invitation sent successfully to the user: " +invitedEmail);
						manageUsersPageIOS.tapOnInvitation(invitedEmail)
						.tapDeleteBtn();
						if (manageUsersPageIOS.validateNoAddedUser() || !manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
							extentTestIOS.log(Status.PASS, "Invitation deleted succuessfully");
						} else {
							extentTestIOS.log(Status.FAIL, "Invitation could not be deleted");
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Invitation is not sent to the desired user");
					}
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Delete Invitation as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Delete Invitation, but it is not allowed");
					}

				}
				else {
					manageUsersPageIOS.getErrorMessage();
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This iOS test method validates the error messages on Add user scenario.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA188_ValidateErrorMessageOnInviteUser(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String correctEmail;
		String hostAccountEmail;
		String hostPassword;
		String errorMessage;
		String role;

		correctEmail = testData.get(0).split(",")[0];
		hostAccountEmail = testData.get(0).split(",")[1];
		hostPassword = testData.get(0).split(",")[2];
		role = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				menuPageIOS.tappingHamburgerMenu()
				.tappingUsers();

				// Validating error message without entering anything in the email field
				manageUsersPageIOS.tapAddBtn()
				.enterUserEmail("")
				.reEnterUserEmail("")
				.openRolesDropDown()
				.selectRole(role)
				.tapSendBtn();

				if (manageUsersPageIOS.validateErrorPopup()) {
					errorMessage = manageUsersPageIOS.getErrorMessage();
					if (errorMessage.contains("Please fill in the fields")) {
						extentTestIOS.log(Status.PASS, "popup present for field being blank");
					} else {
						extentTestIOS.log(Status.FAIL, "Popup Not present for blank fields, the text of the of the popup is: " +errorMessage);
						//here screenshot is not captured because the popup would be dismissed in the previous step and capturing screenshot would not capture the error condition
					}
				} else { 
					extentTestIOS.log(Status.FAIL, "Error popup not present");
					utilityIOS.captureScreenshot(iOSDriver);
				}

				// Validating sending invite with space in email field
				manageUsersPageIOS.enterUserEmail(" ")
				.reEnterUserEmail(" ")
				.openRolesDropDown()
				.selectRole(role)
				.tapSendBtn();

				if (manageUsersPageIOS.validateErrorPopup()) {
					errorMessage = manageUsersPageIOS.getErrorMessage();
					if (errorMessage.contains("Please enter a valid email address.")) {
						extentTestIOS.log(Status.PASS, "popup present for Not a valid email address");
					} else {
						extentTestIOS.log(Status.FAIL, "Popup Not present for invalid email address, the text of the of the popup is: " +errorMessage);
						//here screenshot is not captured because the popup would be dismissed in the previous step and capturing screenshot would not capture the error condition
					}
				} else { 
					extentTestIOS.log(Status.FAIL, "Error popup not present");
					utilityIOS.captureScreenshot(iOSDriver);
				}

				// Validating error message when sending invite to yourself
				manageUsersPageIOS.enterUserEmail(hostAccountEmail)
				.reEnterUserEmail(hostAccountEmail)
				.openRolesDropDown()
				.selectRole(role)
				.tapSendBtn();

				if (manageUsersPageIOS.validateErrorPopup()) {
					errorMessage = manageUsersPageIOS.getErrorMessage();
					if (errorMessage.contains("You can't invite yourself!")) {
						extentTestIOS.log(Status.PASS, "Popup present for invitation sent to yourself");
					} else {
						extentTestIOS.log(Status.FAIL, "Popup not present for inviting yourself, the text of the of the popup is:" +errorMessage);
						//here screenshot is not captured because the popup would be dismissed in the previous step and capturing screenshot would not capture the error condition
					}
				} else { 
					extentTestIOS.log(Status.FAIL, "Error popup not present");
					utilityIOS.captureScreenshot(iOSDriver);
				}

				//Validating error message when email address is not same in the email field and Re-enter Email field
				manageUsersPageIOS.enterUserEmail(correctEmail)
				.reEnterUserEmail(hostAccountEmail)
				.openRolesDropDown()
				.selectRole(role)
				.tapSendBtn();

				if (manageUsersPageIOS.validateErrorPopup()) {
					errorMessage = manageUsersPageIOS.getErrorMessage();
					if (errorMessage.contains("The emails you have entered do not match.")) {
						extentTestIOS.log(Status.PASS, "popup present for email address not matching");
					} else {
						extentTestIOS.log(Status.FAIL, "Popup not present of email addresses not matching, the text of the of the popup is: " +errorMessage);
						//here screenshot is not captured because the popup would be dismissed in the previous step and capturing screenshot would not capture the error condition
					}
				} else { 
					extentTestIOS.log(Status.FAIL, "Error popup not present");
					utilityIOS.captureScreenshot(iOSDriver);
				}

				//Validating error message when we send the invite to already invited user
				manageUsersPageIOS.enterUserEmail(correctEmail)
				.reEnterUserEmail(correctEmail)
				.openRolesDropDown()
				.selectRole(role)
				.tapSendBtn();

				if (!manageUsersPageIOS.validateErrorPopup()) {
					manageUsersPageIOS.tapOkayBtn();
					if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(correctEmail)) {
						extentTestIOS.log(Status.PASS, "Invitation sent successfully to the user: " +correctEmail);
						manageUsersPageIOS.tapAddBtn();
						if (!manageUsersPageIOS.validateErrorPopup()) {
							manageUsersPageIOS.enterUserEmail(correctEmail)
							.reEnterUserEmail(correctEmail)
							.openRolesDropDown()
							.selectRole(role)
							.tapSendBtn();
							if (manageUsersPageIOS.validateErrorPopup()) {
								errorMessage = manageUsersPageIOS.getErrorMessage();
								if (errorMessage.contains("It looks like you have already sent this user an invitation.")) {
									extentTestIOS.log(Status.PASS, "popup present for email address not matching");
								} else {
									extentTestIOS.log(Status.FAIL, "Popup not present for the sending invitation to already invited user, the text of the of the popup is: " +errorMessage);
									//here screenshot is not captured because the popup would be dismissed in the previous step and capturing screenshot would not capture the error condition
								}
								manageUsersPageIOS.tapCancelBtn();
							} else { 
								extentTestIOS.log(Status.FAIL, "Error popup not present");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							extentTestIOS.log(Status.FAIL, "popup present for maximum invite sent could not verify the error for the inviting the already invited user");
						}
						manageUsersPageIOS.tapOnInvitation(correctEmail)
						.tapDeleteBtn();
					} else {
						extentTestIOS.log(Status.FAIL, "Invitation is not sent to the desired user");
					}
				} else {
					manageUsersPageIOS.getErrorMessage();
					manageUsersPageIOS.tapCancelBtn();
					extentTestIOS.log(Status.FAIL, "Invitation could not be sent to the desired user");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Delete Invitation as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Delete Invitation, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This iOS test method first sends a invite a user then swipes on the invite and taps on resend invite and verify the resent invite
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA102_ValidateResendInvitation(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String invitedEmail;
		String role;

		invitedEmail = testData.get(0).split(",")[0];
		role = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				if (manageUsersPageIOS.validateNoAddedUser() || !manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
					manageUsersPageIOS.tapAddBtn();
					if (!manageUsersPageIOS.validateErrorPopup()) {
						manageUsersPageIOS.enterUserEmail(invitedEmail)
						.reEnterUserEmail(invitedEmail)
						.openRolesDropDown()
						.selectRole(role)
						.tapSendBtn();
						if (!manageUsersPageIOS.validateErrorPopup()) {
							manageUsersPageIOS.tapOkayBtn();
							if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
								extentTestIOS.log(Status.PASS, "Invitation sent successfully to the user: " +invitedEmail);
							} else {
								extentTestIOS.log(Status.FAIL, "Invitation is not sent to the desired user");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							manageUsersPageIOS.getErrorMessage();
							manageUsersPageIOS.tapCancelBtn();
							extentTestIOS.log(Status.FAIL, "Invitation could not be sent to the desired user");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						if (manageUsersPageIOS.getErrorMessage().contains("maximum number of users allowed")) {
							extentTestIOS.log(Status.FAIL, "Could not send the Invitation as the number of invitation is exhausted");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}
				} 
				if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
					manageUsersPageIOS.reSendInvitation(invitedEmail);
					if (manageUsersPageIOS.verifyInvitationResentStatus(invitedEmail)) {
						extentTestIOS.log(Status.PASS, "Invitation Resent successfully to the user: " +invitedEmail);	
					} else {
						extentTestIOS.log(Status.FAIL, "Invitation could not be Resent to the desired user");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					manageUsersPageIOS.tapOnInvitation(invitedEmail)
					.tapDeleteBtn();
				}

				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Send invite as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to send invite, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description :  This iOS method verifies the functionality of declining admin/family/guest invitation. The method sends the admin/family/guest invitation, logs out, logs in 
	 *  to the invited user and then invitation is declined and the verification is done on the host user that invitation is not shown
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA301_ValidateDecliningPendingInvitationForUser(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String hostEmail;
		String hostPassword;
		String hostUserRole;
		String primaryAccountName;
		String invitedEmail;
		String invitedPassword; 

		hostEmail = testData.get(0).split(",")[0];
		hostPassword = testData.get(0).split(",")[1];
		hostUserRole = testData.get(0).split(",")[2];
		primaryAccountName = testData.get(0).split(",")[3];
		invitedEmail = testData.get(0).split(",")[4];
		invitedPassword = testData.get(0).split(",")[5];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (multiUser) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				
				//Sending the invite
				if (manageUsersPageIOS.validateNoAddedUser() || !manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
					manageUsersPageIOS.tapAddBtn();
					if (!manageUsersPageIOS.validateErrorPopup()) {
						manageUsersPageIOS.enterUserEmail(invitedEmail)
						.reEnterUserEmail(invitedEmail)
						.openRolesDropDown()
						.selectRole(hostUserRole)
						.tapSendBtn();
						if (!manageUsersPageIOS.validateErrorPopup()) {
							manageUsersPageIOS.tapOkayBtn();
							if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
								extentTestIOS.log(Status.PASS, "Invitation sent successfully to the user: " +invitedEmail);
								menuPageIOS.logout();
								iOSDriver.resetApp();
								loginPageIOS.login(invitedEmail, invitedPassword);
								extentTestIOS.log(Status.INFO, "login to the secondary user account "+invitedEmail);

								menuPageIOS.tappingHamburgerMenu()
								.tappingUserProfile();
								//Declining the invitation
								if (userAccountPageIOS.verifyAccountPage(invitedEmail )) {
									extentTestIOS.log(Status.INFO, "Secondary email ID verified on user account page- "+invitedEmail);
									userAccountPageIOS.tapAccountInvitationsLink();
									if (!userAccountPageIOS.validateNoAccountInvitation() && userAccountPageIOS.verifyPendingInvite(primaryAccountName, hostUserRole)) {
										extentTestIOS.log(Status.INFO, "Invitation received successfully from - "+hostEmail);
										userAccountPageIOS.tapInvite(primaryAccountName, hostUserRole);
										userAccountPageIOS.acceptorDeclineInvitation(false);	
										if (userAccountPageIOS.validateNoAccountInvitation()) {
											extentTestIOS.log(Status.PASS, "Secondary user successfully decline the pending invitation.");
										} else {
											extentTestIOS.log(Status.FAIL, "Secondary user did not decline the pending invitation.");
										}
										userAccountPageIOS.tapBackBtn();
									} else {
										extentTestIOS.log(Status.FAIL, "Invitation did not receive from - "+hostEmail);
									} 
								}
								else {							
									extentTestIOS.log(Status.FAIL, "Secondary email ID not verified - "+invitedEmail);
								} 

								menuPageIOS.logout();
								iOSDriver.resetApp();
								loginPageIOS.login(hostEmail, hostPassword);
								extentTestIOS.log(Status.INFO, "Login to the primary user account "+hostEmail);

								//	verifying that invitation is not shown on the host user account
								menuPageIOS.tappingHamburgerMenu()
								.tappingDevices();
								
								if (devicesPageIOS.verifyBlueBarPresent()) {
									devicesPageIOS.clickOnBlueBar();
									devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
								}
								menuPageIOS.tappingHamburgerMenu()
								.tappingUsers();
								if (!manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail) || manageUsersPageIOS.validateNoAddedUser()) {
									extentTestIOS.log(Status.PASS, "Primary user not able to see the the invitation after declining pending invitation from the secondary user, as expected");
								} else {
									extentTestIOS.log(Status.FAIL, "Primary user able to see the the invitation after declining the pending invitation from the secondary user.");
								}
								menuPageIOS.tappingHamburgerMenu()
								.tappingDevices();
							} else {
								extentTestIOS.log(Status.FAIL, "Invitation is not sent to the desired user");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							manageUsersPageIOS.getErrorMessage();
							manageUsersPageIOS.tapCancelBtn();
							extentTestIOS.log(Status.FAIL, "Invitation could not be sent to the desired user");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						if (manageUsersPageIOS.getErrorMessage().contains("maximum number of users allowed")) {
							extentTestIOS.log(Status.FAIL, "Could not send the Invitation as the number of invitation is exhausted");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}
				} 

				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to decline invite as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to decline invite, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * * Description :  This iOS method verifies the functionality of deleting the accepted Admin/Family/Guest invitation. The method sends the admin/family/guest invitation, logs out, logs in 
	 *  to the invited user and then invitation is accepted and then deleted. The verification is done on the host user that invitation is not shown.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA310_ValidateDeletionOfAcceptedInviteByUser(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String hostEmail;
		String hostPassword;
		String userRole;
		String hostAccountName;
		String invitedEmail;
		String invitedPassword;
		String invitedUserName;

		hostEmail = testData.get(0).split(",")[0];
		hostPassword = testData.get(0).split(",")[1];
		userRole = testData.get(0).split(",")[2];
		hostAccountName = testData.get(0).split(",")[3];
		invitedEmail = testData.get(0).split(",")[4];
		invitedPassword = testData.get(0).split(",")[5];
		invitedUserName = testData.get(0).split(",")[6];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (multiUser) {
			if (devicesPageIOS.verifyBlueBarPresent()) {
				devicesPageIOS.clickOnBlueBar();
				if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
					devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
				}
			}
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Users")) {
				menuPageIOS.tappingUsers();
				if (manageUsersPageIOS.validateNoAddedUser() || !manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
					manageUsersPageIOS.tapAddBtn();
					if (!manageUsersPageIOS.validateErrorPopup()) {
						manageUsersPageIOS.enterUserEmail(invitedEmail)
						.reEnterUserEmail(invitedEmail)
						.openRolesDropDown()
						.selectRole(userRole)
						.tapSendBtn();
						if (!manageUsersPageIOS.validateErrorPopup()) {
							manageUsersPageIOS.tapOkayBtn();
							if (!manageUsersPageIOS.validateNoAddedUser() && manageUsersPageIOS.verifyInvitedUsersEmail(invitedEmail)) {
								extentTestIOS.log(Status.PASS, "Invitation sent successfully to the user: " +invitedEmail);
							} else {
								extentTestIOS.log(Status.FAIL, "Invitation is not sent to the desired user");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							manageUsersPageIOS.getErrorMessage();
							manageUsersPageIOS.tapCancelBtn();
							extentTestIOS.log(Status.FAIL, "Invitation could not be sent to the desired user");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						if (manageUsersPageIOS.getErrorMessage().contains("maximum number of users allowed")) {
							extentTestIOS.log(Status.FAIL, "Could not send the Invitation as the number of invitation is exhausted");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}
				} 

				menuPageIOS.logout();
				iOSDriver.resetApp();
				loginPageIOS.login(invitedEmail, invitedPassword);
				extentTestIOS.log(Status.INFO, "Login to the secondary user account "+invitedEmail);

				menuPageIOS.tappingHamburgerMenu()
				.tappingUserProfile();
				if (userAccountPageIOS.verifyAccountPage(invitedEmail)) {
					extentTestIOS.log(Status.INFO, "Secondary email ID verified on user account page- "+invitedEmail);
					userAccountPageIOS.tapAccountInvitationsLink();
					if (!userAccountPageIOS.validateNoAccountInvitation() && userAccountPageIOS.verifyPendingInvite(hostAccountName, userRole)) {
						extentTestIOS.log(Status.INFO, "Invitation received successfully from - "+hostEmail);
						userAccountPageIOS.tapInvite(hostAccountName, userRole);
						userAccountPageIOS.acceptorDeclineInvitation(true);	

						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices();
						if (devicesPageIOS.verifyBlueBarPresent()) {
							devicesPageIOS.clickOnBlueBar();
							if (devicesPageIOS.verifyUserPresent(hostAccountName, userRole)) {
								devicesPageIOS.selectUserRole(hostAccountName, userRole);
								extentTestIOS.log(Status.PASS, "Successfully switched to the accepted invite as admin");
							} else {
								devicesPageIOS.tapOnCancelBtn();
								extentTestIOS.log(Status.FAIL, "The Accepted admin invite is not shown in this account");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} 

						menuPageIOS.tappingHamburgerMenu()
						.tappingUserProfile();
						userAccountPageIOS.tapAccountInvitationsLink();

						if (userAccountPageIOS.tapAcceptedInvite(hostAccountName, userRole))
						{
							userAccountPageIOS.clickRemove();
							extentTestIOS.log(Status.PASS, "Secondary user successfully delete the accepted admin invitation.");
						} else {
							extentTestIOS.log(Status.FAIL, "Secondary user did not delete the accepted admin invitation.");
						}
						userAccountPageIOS.tapBackBtn();
					} else {
						extentTestIOS.log(Status.FAIL, "Invitation did not receive from - "+hostEmail);
					} 
				}
				else {							
					extentTestIOS.log(Status.FAIL, "Secondary email ID not verified - "+invitedEmail);
				} 

				menuPageIOS.logout();
				iOSDriver.resetApp();
				loginPageIOS.login(hostEmail, hostPassword);
				extentTestIOS.log(Status.INFO, "Login to the primary user account "+hostEmail);
				
				if (devicesPageIOS.verifyBlueBarPresent()) {
					devicesPageIOS.clickOnBlueBar();
					if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
						devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
					}
				}
				menuPageIOS.tappingHamburgerMenu();
				
				menuPageIOS.tappingUsers();
				if (manageUsersPageIOS.validateNoAddedUser() || !manageUsersPageIOS.verifyAddedUser(invitedUserName,userRole)) {
					extentTestIOS.log(Status.PASS, "User deleted successfully,after deleteing the accepted admin invitation from the secondary user,as expected");	
				} else {
					extentTestIOS.log(Status.FAIL, "User not deleted successfully,after deleteing the accepted admin invitation from the secondary user");
					utilityIOS.captureScreenshot(iOSDriver);
				}

				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to delete invite as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to delete invite, but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Manage users link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Manage users link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 *  Description This is an iOS test method to change all the fields on edit user info page , verify all the chanegs done 
     *               and revert all the field data to original
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA86_ValidateEditInfo(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String accountName;
		String firstName;
		String lastName;
		String phone;
		String country;
		String addressLine1;
		String addressLine2;
		String city;
		String state;
		String zip;
		String timeZone;
		String language;

		String accountNameOld;
		String firstNameOld;
		String lastNameOld;
		String phoneOld;
		String countryOld;
		String addressLine1Old;
		String addressLine2Old;
		String cityOld;
		String stateOld;
		String zipOld;
		String timeZoneOld;
		String languageOld;

		accountName = testData.get(0).split(",")[0];
		firstName = testData.get(0).split(",")[1];
		lastName = testData.get(0).split(",")[2];
		phone = testData.get(0).split(",")[3];
		country = testData.get(0).split(",")[4];
		addressLine1 = testData.get(0).split(",")[5];
		addressLine2 = testData.get(0).split(",")[6];
		city = testData.get(0).split(",")[7];
		state = testData.get(0).split(",")[8];
		zip = testData.get(0).split(",")[9];
		timeZone = testData.get(0).split(",")[10];
		language = testData.get(0).split(",")[11];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//loginPageIOS.login(email, password);
		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();

		userAccountPageIOS.tapEditInfoLink();
		accountNameOld = userAccountPageIOS.getAccountName();
		firstNameOld = userAccountPageIOS.getFirstName();
		lastNameOld = userAccountPageIOS.getLastName();
		phoneOld = userAccountPageIOS.getPhone();
		countryOld = userAccountPageIOS.getCountry();
		addressLine1Old = userAccountPageIOS.getAddressLine1();
		addressLine2Old = userAccountPageIOS.getAddressLine2();
		cityOld = userAccountPageIOS.getCity();
		stateOld = userAccountPageIOS.getState();
		zipOld = userAccountPageIOS.getZip();
		timeZoneOld = userAccountPageIOS.getTimeZone();
		languageOld = userAccountPageIOS.getLanguage();

		userAccountPageIOS.enterAccountName(accountName)
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterPhone(phone)
		// Below code is commented as per discussion with Onsite SPOC
		//.selectCountry(country) 
		.enterAddressLine1(addressLine1)
		.enterAddressLine2(addressLine2)
		.enterCity(city)
		.enterState(state)
		.enterZip(zip)
		.selectTimeZone(timeZone)
		.selectLanguage(language)
		.tapEditInfoSaveBtn();
		
		if (!userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getUserNameAccountInfo().trim().equalsIgnoreCase(firstName + " " + lastName)) {
				extentTestIOS.log(Status.PASS, "Account User Name matches on the account info page.");
			} else {
				extentTestIOS.log(Status.FAIL, "Account User Name doesn't match on the account info page."); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.getUserNameMenuLink().trim().equalsIgnoreCase(firstName+" "+lastName)) {
				extentTestIOS.log(Status.PASS, "Account User Name is matches on the Menulinks");
			} else {
				extentTestIOS.log(Status.FAIL, "Account User Name doesn't match on the Menulinks"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
			menuPageIOS.tappingHamburgerMenu()
			.tappingUserProfile();
			userAccountPageIOS.tapEditInfoLink();
			if (userAccountPageIOS.getAccountName().trim().equalsIgnoreCase(accountName)) {
				extentTestIOS.log(Status.PASS, "Account Name is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Account Name is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getFirstName().trim().equalsIgnoreCase(firstName)) {
				extentTestIOS.log(Status.PASS, "First Name is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "First Name is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getLastName().trim().equalsIgnoreCase(lastName)) {
				extentTestIOS.log(Status.PASS, "Last Name is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Last Name is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (!userAccountPageIOS.verifyEmail()) {
				extentTestIOS.log(Status.PASS, "Email field is not enabled as expected");
			} else {
				extentTestIOS.log(Status.FAIL, "Email field is enabled"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getPhone().trim().equalsIgnoreCase(phone)) {
				extentTestIOS.log(Status.PASS, "Phone Number is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Phone Number is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getCountry().trim().equalsIgnoreCase(country)) {
				extentTestIOS.log(Status.PASS, "Country is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "country is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getAddressLine1().trim().equalsIgnoreCase(addressLine1)) {
				extentTestIOS.log(Status.PASS, "Address line 1 is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Address line 1 is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getAddressLine2().trim().equalsIgnoreCase(addressLine2)) {
				extentTestIOS.log(Status.PASS, "Address line 2 is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Address line 2 is not matching");
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getCity().trim().equalsIgnoreCase(city)) {
				extentTestIOS.log(Status.PASS, "City is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "City is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getState().trim().equalsIgnoreCase(state)) {
				extentTestIOS.log(Status.PASS, "State is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "State is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getZip().trim().equalsIgnoreCase(zip)) {
				extentTestIOS.log(Status.PASS, "Zip is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Zip is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getTimeZone().trim().equalsIgnoreCase(timeZone)) {
				extentTestIOS.log(Status.PASS, "Time Zone is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Time Zone is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			if (userAccountPageIOS.getLanguage().trim().equalsIgnoreCase(language)) {
				extentTestIOS.log(Status.PASS, "Language is matching");
			} else {
				extentTestIOS.log(Status.FAIL, "Language is not matching"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}

			userAccountPageIOS.enterAccountName(accountNameOld)
			.enterFirstName(firstNameOld)
			.enterLastName(lastNameOld)
			.enterPhone(phoneOld)
			//.selectCountry(countryOld)
			.enterAddressLine1(addressLine1Old)
			.enterAddressLine2(addressLine2Old)
			.enterCity(cityOld)
			.enterState(stateOld)
			.enterZip(zipOld)
			.selectTimeZone(timeZoneOld)
			.selectLanguage(languageOld)
			.tapEditInfoSaveBtn();
		} else {
			extentTestIOS.log(Status.FAIL, "Edited user info profile could not be saved"); 
			utilityIOS.captureScreenshot(iOSDriver);
			userAccountPageIOS.getErrorMessage();
			userAccountPageIOS.tapEditInfoCancelBtn();
		}
	}

	/**
	 * Description To validate the error messages on Edit info screen
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA86_ValidateErrorMessageOnEditInfo(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String message1;
		message1 = "Please enter all required fields.\nPlease enter all required fields.";

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapEditInfoLink();

		userAccountPageIOS.enterAccountName("")
		.tapEditInfoSaveBtn();
		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message1)) {
				extentTestIOS.log(Status.PASS, "Popup present when the Account name is kept blank"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present for first being blank"); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 
			utilityIOS.captureScreenshot(iOSDriver);
		}
		userAccountPageIOS.enterAccountName("xyz")
		.enterFirstName("")
		.tapEditInfoSaveBtn();

		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message1)) {
				extentTestIOS.log(Status.PASS, "Popup present when the First name is kept blank"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present for first being blank"); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
			utilityIOS.captureScreenshot(iOSDriver);
		}

		userAccountPageIOS.enterFirstName("xyz")
		.enterLastName("")
		.tapEditInfoSaveBtn();

		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message1)) {
				extentTestIOS.log(Status.PASS, "Popup present when the Last name is kept blank"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present for Last name being blank"); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}
		userAccountPageIOS.enterLastName("xyz")
		.enterZip("")
		.tapEditInfoSaveBtn();
		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message1)) {
				extentTestIOS.log(Status.PASS, "Popup present when the zip name is kept blank"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present for zip name being balnk"); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 
			utilityIOS.captureScreenshot(iOSDriver);
		}
		userAccountPageIOS.tapEditInfoCancelBtn();
	}

	/**
	 * Description : This iOS test method validates the Account name, First name, Last name and zip code text fields on the Edit info page
	 * @param testData
	 * @param deviceTest
	 * @throws Exception
	 */
	public void MTA347_ValidateTextInputLimitOnEditInfo(ArrayList<String> testData, String deviceTest,String scenarioName) throws Exception {

		String accountName = "MyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprodMyQPreprod";
		String firstName = "myQautomationmyQautomationmyQautomation";
		String lastName = "synechronmyqsynechronmyqsynechronmyq";
		String zipCode = "1234567890112";
		String textEntered = "";
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();;

		// object for page factory class
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapEditInfoLink();

		//Verify Account Name
		userAccountPageIOS.enterAccountName(accountName);
		textEntered = userAccountPageIOS.account_name_text_box.getText();
		int lenAccountName = textEntered.length();
		if (lenAccountName == 255) {
			extentTestIOS.log(Status.PASS, "The Account name field accepts only 255 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "Account name Text Entered: " + accountName + " ,text retrieved after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}

		// Verify first name 
		userAccountPageIOS.enterFirstName(firstName);
		textEntered = userAccountPageIOS.first_name_text_box.getText();
		int lenFirstName = textEntered.length();
		if (lenFirstName == 30) {
			extentTestIOS.log(Status.PASS, "The First name field accepts only 30 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "First name Text Entered: " + firstName + " ,text retrieved after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Verify Last Name
		userAccountPageIOS.enterLastName(lastName);
		textEntered = userAccountPageIOS.last_name_text_box.getText();
		int lenLastName = textEntered.length();
		if (lenLastName == 30) {
			extentTestIOS.log(Status.PASS, "The Last name field accepts only 30 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "Last name Text Entered: " + lastName + " ,text retrieved after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}

		// Verify Zipcode
		userAccountPageIOS.enterZip(zipCode);
		textEntered = userAccountPageIOS.zip_text_box.getText();
		int lenZipCode = textEntered.length();
		if (lenZipCode == 10) {
			extentTestIOS.log(Status.PASS, "The Zipcode field accepts only 10 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "Zipcode Text Entered: " + zipCode + " ,text retrieved after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}
		userAccountPageIOS.tapEditInfoCancelBtn();
	}
	
	/**
	 * Description This is an iOS test method that enters new email in the change email field and saves it and verifies that the appropriate
	 * popup is present, but do not go into email and actually confirms the new email
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA90_ValidateChangeEmail(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String newEmail;
		String password;
		String confirmationMessage;

		newEmail = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		confirmationMessage = "Check Email\nAn email has been sent to "+userEmail+". Please click the link in the email to confirm your change."; 

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapChangeEmailLink()
		.enterNewEmail(newEmail)
		.enterConfirmNewEmail(newEmail)
		.enterPasswordInChangeEmail(password)
		.tapChangeEmailSubmitBtn();

		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(confirmationMessage)) {
				extentTestIOS.log(Status.PASS, "Change Email Test case passed. An email has been sent to the desired new changed email"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "Change Email Test case Failed. popup test is not as expected"); 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}
	}

	/**
	 * Description This is an iOS test method that verifies the Error messages on the change Email Feature
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	
	public void MTA90_ValidateErrorMessagesOnChangeEmail(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String newEmail;
		String password;
		String message1,message2,message3,message4,message5;

		newEmail = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];

		message1 = "Missing Information\nPlease enter all required fields.";
		message2 = "Error\nInvalid email";
		message3 = "Error\nIncorrect password";
		message4 = "Error\nBoth email address must match";
		message5 = "Error\nThis email is already in use. (703)"; 

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Error verification for fields kept blank
		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapChangeEmailLink()
		.enterNewEmail("")
		.enterConfirmNewEmail("")
		.enterPasswordInChangeEmail("")
		.tapChangeEmailSubmitBtn();

		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message1)) {
				extentTestIOS.log(Status.PASS, "Popup present for Entering all the required fields"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup text not matching the expected message"); 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}

		// Error Verification for invalid email
		userAccountPageIOS.enterNewEmail(" ")
		.enterConfirmNewEmail(" ")
		.enterPasswordInChangeEmail(password)
		.tapChangeEmailSubmitBtn();
		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message2)) {
				extentTestIOS.log(Status.PASS, "Popup present for Invalid Email"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present as expected for invalid email"); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}

		//Error verification for invalid password
		userAccountPageIOS.enterNewEmail(newEmail)
		.enterConfirmNewEmail(newEmail)
		.enterPasswordInChangeEmail(" ")
		.tapChangeEmailSubmitBtn();
		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message3)) {
				extentTestIOS.log(Status.PASS, "Popup present for Wrong Password"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present as expected for incorrect password "); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}

		//Error verification for email address must match
		userAccountPageIOS.enterNewEmail(newEmail)
		.enterConfirmNewEmail(" ")
		.enterPasswordInChangeEmail(password)
		.tapChangeEmailSubmitBtn();
		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message4)) {
				extentTestIOS.log(Status.PASS, "Popup present for emails in the text field not matching"); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present as expected for email not matching"); 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}

		//Error verification for email already in use
		userAccountPageIOS.enterNewEmail(newEmail)
		.enterConfirmNewEmail(newEmail)
		.enterPasswordInChangeEmail(password)
		.tapChangeEmailSubmitBtn();
		if (userAccountPageIOS.validatePopupPresent()) {
			if (userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message5)) {
				extentTestIOS.log(Status.PASS, "Popup present for emails already in use."); 	
				extentTestIOS.log(Status.PASS, "Verified device error code (703)."); 	
			} else {
				extentTestIOS.log(Status.FAIL, "popup not present as expected for email already in use"); 
				extentTestIOS.log(Status.FAIL, "Device error code (703) not verified."); 	
			}
		} else {
			extentTestIOS.log(Status.FAIL, "No confirmation popup present"); 	
		}
		userAccountPageIOS.tapBackBtn();
	}

	/**
	 * Description This is an iOS test method that enters new Password in the change password field and saves it and logs out and logs back
	 * in using the new password and revert the password to original value
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA98_ValidateChangePassword(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String currentEmail;
		String currentPassword;
		String newPassword;

		currentEmail = testData.get(0).split(",")[0];
		currentPassword = testData.get(0).split(",")[1];
		newPassword = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		//select account user
		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile(); 
		userAccountPageIOS.tappingChangePassword()
		.enterPassword(currentPassword)
		.enterNewPassword(newPassword);

		//Verify the password passed in the test data is valid
		if (userAccountPageIOS.passwordCriteriaValidation(newPassword)) {
			extentTestIOS.log(Status.INFO, "Password contains valid combination of characters");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-98 Validate Change Password: '" + newPassword + "' New Password doesn't contains the required combination of characters");
		}

		userAccountPageIOS.enterConfirmPassword(newPassword);
		userAccountPageIOS.clickSubmit();
		menuPageIOS.logout();

		// Create another driver instance as this is the workaround for IOS whenever re login is required
		loginPageIOS.login(currentEmail,  newPassword);

		if (devicesPageIOS.verifyHomeScreen()) { // Verify Login successful using new password
			extentTestIOS.log(Status.PASS, "MTA-98 Validate Change Password: Login successful using updated password");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-98 Validate Change Password: Login unsuccessful using updated password");
		}

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingUserProfile(); 
		//Reset the password back to the original one
		userAccountPageIOS.tappingChangePassword()
		.enterPassword(newPassword)
		.enterNewPassword(currentPassword)
		.enterConfirmPassword(currentPassword)
		.clickSubmit();
		menuPageIOS.logout();
		loginPageIOS.login(currentEmail,  currentPassword);
	}

	/**
	 * Description This is an iOS test method that verifies the Error messages on the change Password Feature
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA98_ValidateErrorMessagesOnChangePassword(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String currentPassword;
		String validPassword;
		String message1,message2,message3,message4;

		currentPassword = testData.get(0).split(",")[0];
		validPassword = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		
		message1 = "Missing Information\nPlease enter all required fields.";
		message2 = "Error\nYour current password does not match.";
		message3 = "Error\nBoth passwords must match.";
		message4 = "Password Strength:  Too weak\nPasswords need to be at least 8 characters in length and must contain at least 3 of the following 4 types of characters:  uppercase letter, lowercase letter, number or symbol.";

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		manageUsersPageIOS = new ManageUsersPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Error verification for fields kept blank
		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingUserProfile(); 
		userAccountPageIOS.tappingChangePassword()
		.enterPassword("")
		.enterNewPassword("")
		.enterConfirmPassword("")
		.clickSubmit();

		if (userAccountPageIOS.validatePopupPresent()) {
			if(userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message1)) {
				extentTestIOS.log(Status.PASS, "Popup present to verify entry of all required fields.");  
			} else {
				extentTestIOS.log(Status.FAIL, "An error popup is displayed but not with required message: " + message1); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else{
			extentTestIOS.log(Status.FAIL, "No confirmation popup present for entering all the required fields");  
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Error verification for entering wrong current password 
		userAccountPageIOS.enterPassword("Syne@123")
		.enterNewPassword(validPassword)
		.enterConfirmPassword(validPassword)
		.clickSubmit();

		if (userAccountPageIOS.validatePopupPresent()) {
			if(userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message2)) {
				extentTestIOS.log(Status.PASS, "Popup present for current password mismatch");  
			} else {
				extentTestIOS.log(Status.FAIL, "An error popup is displayed but not with required message: " + message2); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else{
			extentTestIOS.log(Status.FAIL, "No confirmation popup present for current password mismatch");  
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Error verification for entering different passwords in create password text box and verify password text box 
		userAccountPageIOS.enterPassword(currentPassword)
		.enterNewPassword("Synechron@12345")
		.enterConfirmPassword("Synechron@123")
		.clickSubmit();

		if (userAccountPageIOS.validatePopupPresent()) {
			if(userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message3)) {
				extentTestIOS.log(Status.PASS, "Popup present for new & confirm passwords mismatch");  
			} else {
				extentTestIOS.log(Status.FAIL, "An error popup is displayed but not with required message: " + message3); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else{
			extentTestIOS.log(Status.FAIL, "No confirmation popup present for new & confirm password mismatch");  
			utilityIOS.captureScreenshot(iOSDriver);
		}

		//Error verification for entering weak password 
		userAccountPageIOS.enterPassword(currentPassword)
		.enterNewPassword("MyQsynechron")
		.enterConfirmPassword("MyQsynechron")
		.clickSubmit();

		if (userAccountPageIOS.validatePopupPresent()) {
			if(userAccountPageIOS.getErrorMessage().equalsIgnoreCase(message4)) {
				extentTestIOS.log(Status.PASS, "Popup present for valid password criteria");  
			} else {
				extentTestIOS.log(Status.FAIL, "An error popup is displayed but not with required message: " + message4); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else{
			extentTestIOS.log(Status.FAIL, "No confirmation popup present while entering weak password");  
			utilityIOS.captureScreenshot(iOSDriver);
		}
		userAccountPageIOS.tapBackBtn();
	}

	/**
	 * Description This iOS test method validates the Automatic login feature
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA146_ValidateAutomaticLogin(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enableAutomaticLogin = true;

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();

		userAccountPageIOS.tapSecurityLink()
		.toggleAutomaticLogin(enableAutomaticLogin)
		.tapBackBtn();
		iOSDriver.resetApp();

		if (loginPageIOS.verifyLogin()) {
			extentTestIOS.log(Status.PASS, "Login happened successfully"); 	
		} else {
			extentTestIOS.log(Status.FAIL, "Login did not happened successfully"); 	
			utilityIOS.captureScreenshot(iOSDriver);
		}
		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink()
		.toggleAutomaticLogin(false)
		.tapBackBtn();
	}

	/**
	 * Description This iOS test method validates the Passcode login feature
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA192_ValidatePasscodeLogin(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enablePasscode = true; 
		String passcode;

		passcode = testData.get(0).split(",")[0];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink();
		if (userAccountPageIOS.verifyIfPasscodeSet()) {
			userAccountPageIOS.togglePasscodeLogin(enablePasscode)
			.tapChangePasscode()
			.settingPasscode(passcode);
		} else {
			userAccountPageIOS.togglePasscodeLogin(enablePasscode);
			userAccountPageIOS.settingPasscode(passcode);
		}
		userAccountPageIOS.tapBackBtn();
		iOSDriver.resetApp();
		loginPageIOS.passcodeLogin(passcode);
		if (loginPageIOS.verifyLogin()) {
			extentTestIOS.log(Status.PASS, "Login happened successfully"); 	
			menuPageIOS.tappingHamburgerMenu()
			.tappingUserProfile();
			userAccountPageIOS.tapSecurityLink()
			.togglePasscodeLogin(false)
			.tapBackBtn();
		} else {
			extentTestIOS.log(Status.FAIL, "Login did not happened successfully"); 	
			utilityIOS.captureScreenshot(iOSDriver);
		}
	}

	/**
	 * Description This iOS test method validates the Passcode for account view feature
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA207_ValidatePasscodeForAccountView(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enablePasscode = true; 
		String passcode;

		passcode = testData.get(0).split(",")[0];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink();
		if (userAccountPageIOS.verifyIfPasscodeSet()) {
			userAccountPageIOS.togglePasscodeAccountView(enablePasscode)
			.tapChangePasscode()
			.settingPasscode(passcode);
		} else {
			userAccountPageIOS.togglePasscodeAccountView(enablePasscode);
			userAccountPageIOS.settingPasscode(passcode);
		}
		userAccountPageIOS.tapBackBtn();
		menuPageIOS.tappingHamburgerMenu()
		.accessUserProfileWithPasscode(passcode);
		if (menuPageIOS.verifyUserAccountShown()) {
			extentTestIOS.log(Status.PASS, "Test case passed for accessing user account with passcode"); 
			userAccountPageIOS.tapSecurityLink()
			.togglePasscodeAccountView(false)
			.tapBackBtn();
		} else {
			extentTestIOS.log(Status.FAIL, "Test case failed for accessing user account with passcode"); 
			utilityIOS.captureScreenshot(iOSDriver);
		}
	}

	/**
	 * Description This iOS test method validates the Password for account view feature
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA213_ValidatePasswordForAccountView(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enablePasswordToggle = true; 
		String password;

		password = testData.get(0).split(",")[0];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink()
		.toggleEmailAndPasswordAccountView(enablePasswordToggle)
		.tapBackBtn();
		menuPageIOS.tappingHamburgerMenu()
		.accessUserProfileWithPassword(password);
		if (menuPageIOS.verifyUserAccountShown()) {
			extentTestIOS.log(Status.PASS, "Test case passed for accessing user account with password"); 
			userAccountPageIOS.tapSecurityLink()
			.toggleEmailAndPasswordAccountView(false)
			.tapBackBtn();
		} else {
			extentTestIOS.log(Status.FAIL, "Test case failed for accessing user account with password"); 
			utilityIOS.captureScreenshot(iOSDriver);
		}
	}

	/**
	 * Description To validate opening closing the devices with passcode
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA210_ValidatePasscodeForDevices(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enablePasscodeToggle = true; 
		String passcode;
		String deviceName;

		passcode = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink();
		if (userAccountPageIOS.verifyIfPasscodeSet()) {
			userAccountPageIOS.togglePasscodeDeviceStateChange(enablePasscodeToggle)
			.tapChangePasscode()
			.settingPasscode(passcode);
		} else {
			userAccountPageIOS.togglePasscodeDeviceStateChange(enablePasscodeToggle);
			userAccountPageIOS.settingPasscode(passcode);
		}
		userAccountPageIOS.tapBackBtn();
		menuPageIOS.tappingHamburgerMenu()
		.tappingDevices();
		if (devicesPageIOS.verifyBlueBarPresent()) {
			devicesPageIOS.clickOnBlueBar();
			if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
				devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
			} else {
				devicesPageIOS.tapOnCancelBtn();
			}
		} 
		if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
			if (devicesPageIOS.getDeviceStatus(deviceName).equalsIgnoreCase("OPEN")) {
				devicesPageIOS.actuatingDevice(deviceName);
			}

			devicesPageIOS.tappingDeviceWithPasscode(deviceName, passcode);
			if (devicesPageIOS.getDeviceStatus(deviceName).equalsIgnoreCase("OPEN")) {
				extentTestIOS.log(Status.PASS, "Test case passed for performing action on devices with passcode"); 
				devicesPageIOS.actuatingDevice(deviceName);
			} else {
				extentTestIOS.log(Status.PASS, "Device State did not change, hence the test case failed"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, " Expected device is not visible");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink()
		.togglePasscodeDeviceStateChange(false)
		.tapBackBtn();
	}

	/**
	 * Description To validate opening closing the devices with password
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA212_ValidatePasswordForDevices(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		boolean enablePasswordToggle = true; 
		String password;
		String deviceName;
		
		password = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		userAccountPageIOS = new UserAccountPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink()
		.togglePasswordDeviceStateChange(enablePasswordToggle)
		.tapBackBtn();
		menuPageIOS.tappingHamburgerMenu()
		.tappingDevices();
		if (devicesPageIOS.verifyBlueBarPresent()) {
			devicesPageIOS.clickOnBlueBar();
			if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
				devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
			} else {
				devicesPageIOS.tapOnCancelBtn();
			}
		} 
		if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
			if (devicesPageIOS.getDeviceStatus(deviceName).equalsIgnoreCase("OPEN")) {
				devicesPageIOS.actuatingDevice(deviceName);
			}
			devicesPageIOS.tappingDeviceWithPassword(deviceName, password);
			if (devicesPageIOS.getDeviceStatus(deviceName).equalsIgnoreCase("OPEN")) {
				extentTestIOS.log(Status.PASS, "Test case passed for performing action on devices with password"); 
				devicesPageIOS.actuatingDevice(deviceName);
			} else {
				extentTestIOS.log(Status.PASS, "Device State did not change, hence the test case failed"); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, " Expected device is not visible");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		menuPageIOS.tappingHamburgerMenu()
		.tappingUserProfile();
		userAccountPageIOS.tapSecurityLink()
		.togglePasswordDeviceStateChange(false)
		.tapBackBtn();
	}

	/**
	 * Description To verify User's Guide in Help section
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA171_ValidateTextOnUserGuideInHelp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		helpPageIOS = new HelpPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHelp();
		helpPageIOS.clickOnUserGuideLink();

		if (helpPageIOS.verifyUserGuidePresent()) { // Verify the text is displayed after clicking on the user guide link
			extentTestIOS.log(Status.PASS, "MTA-171 Validate Help User Guide: User guide Page has launched successfully");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-171 Validate Help User Guide: User guide Page has not launched");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		helpPageIOS.clickBackButton();
	}

	/**
	 * Description To verify FAQ in Help section
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA173_ValidateTextOnMyQSupportInHelp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);  
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		helpPageIOS = new HelpPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHelp();

		helpPageIOS.clickMyQSupportLink(); // Verify the required links are displayed after clicking on the FAQ link
		if (helpPageIOS.verifyUserMyQAppPresent() && helpPageIOS.verifyUserMyQProductPresent()) { 
			extentTestIOS.log(Status.PASS, "MTA-173 Validate MyQ Support : MyQ APP/MyQ Products link are displayed successfully on the MyQ Support Section");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-173 Validate MyQ Support : MyQ APP/MyQ Products link are not displayed on the MyQ Support Section");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		helpPageIOS.clickBackButton();
	}

	/**
	 * Description To verify license and terms of use in Help section
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA175_ValidateTextOnLicenseInHelp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		helpPageIOS = new HelpPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHelp();
		helpPageIOS.clickOnUserLicenseLink(); 
		// Verify the required links are displayed after clicking on the license link in help 
		if (helpPageIOS.verifyLicense()) { 
			extentTestIOS.log(Status.PASS, "MTA-175 Validate License in Help : The LiftMaster agreement and terms of use is displayed successfully");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-175 Validate License in Help : The LiftMaster agreement and terms of use is not displayed");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		helpPageIOS.clickBackButton();
	}

	/**
	 * Description To verify Privacy Statement in Help section
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA177_ValidateTextOnPrivacyStatementInHelp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		helpPageIOS = new HelpPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHelp();

		helpPageIOS.clickOnPrivacyStatementLink();
		// Verify the required links are displayed after clicking on the privacy statement link
		if (helpPageIOS.verifyPrivacyStatement()) { 
			extentTestIOS.log(Status.PASS, "MTA-177 Validate Privacy Statement : The Lift Master Privacy statement is displayed successfully");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-177 Validate Privacy Statement: The Lift Master Privacy statement is not displayed");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		helpPageIOS.clickBackButton();
	}

	/**
	 * Description To verify Legal Disclaimer in Help
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA179_ValidateTextOnLegalDisclaimerInHelp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		helpPageIOS = new HelpPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHelp();

		helpPageIOS.clickOnLegalDisclaimerLink(); 
		// Verify the required links are displayed after clicking on the legal disclaimer link
		if (helpPageIOS.verifyLegalDisclaimer()) { 
			extentTestIOS.log(Status.PASS, "MTA-179 Validate Legal Disclaimer : The Lift Master Legal Disclaimer is displayed successfully");
		} else {
			extentTestIOS.log(Status.FAIL, "MTA-179 Validate Legal Disclaimer : The Lift Master Legal Disclaimer is not displayed");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		helpPageIOS.clickBackButton();
	}
		
	/**
	 * Description This iOS method validates Signup using random Email.After Successful Signup,same email is used in login and validates the user not activated popup
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */

	public void MTA20_ValidateSignUp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String firstName;
		String lastName;
		String password;
		String country;
		String zip;
		String language;
		String emailGenerated;
		String UserNotActivatedMsg = "Please check for an activation email and click the link to confirm your email address.";

		firstName = testData.get(0).split(",")[0];
		lastName = testData.get(0).split(",")[1];
		password = testData.get(0).split(",")[2];
		zip = testData.get(0).split(",")[3];
		country = testData.get(0).split(",")[4];
		language = testData.get(0).split(",")[5];
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		signUpPageIOS =new SignUpPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//signup form fill up
		signUpPageIOS.clickSignup()
		.selectCountry(country)
		.enterFirstName(firstName)
		.enterLastName(lastName);
		emailGenerated = signUpPageIOS.enterRandomEmail();
		signUpPageIOS.enterPassword(password)
		.enterVerifyPassword(password)
		.selectCountry(country)
		.enterZip(zip)
		.selectLanguage(language)
		.clickSubmit();

		String popupMessage =signUpPageIOS.validateSignUp();
		//validations on signup page
		if (popupMessage.contains("An email has been sent to you. Please check your email inbox to complete the registration process.")) {
			extentTestIOS.log(Status.PASS, "Signup Successful. Please open your inbox to complete the registration process ");
			loginPageIOS.enterAccountEmailId(emailGenerated)
			.enterAccountPassword(password)
			.clickLoginButton();
			if(loginPageIOS.validatePopupPresent()) {
				popupMessage = loginPageIOS.getPopupMessage();
				if(popupMessage.contains(UserNotActivatedMsg)) {
					extentTestIOS.log(Status.PASS, "Popup Present for User not Activated");
					loginPageIOS.tapYesButton();
					if(loginPageIOS.validatePopupPresent()) {
						loginPageIOS.getPopupMessage();
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Popup not present for user not activated"); 
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
		} else if(popupMessage.contains("Password Strength: Too weak Passwords")) {
			extentTestIOS.log(Status.FAIL, "Password Strength: Too weak");
			signUpPageIOS.tapCancelBtn();
		} else if(popupMessage.contains("Please enter all required fields.")) {
			extentTestIOS.log(Status.FAIL, "All Mandatory fields are not field");
			signUpPageIOS.tapCancelBtn();
		} else if(popupMessage.contains("Please enter a valid email address.")) {
			extentTestIOS.log(Status.FAIL, "Email address entered is not valid");
			signUpPageIOS.tapCancelBtn();
		} else if(popupMessage.contains("That username already exists.")) {
			extentTestIOS.log(Status.FAIL, "Email passed has already been registered");
			signUpPageIOS.tapCancelBtn();
		} else {
			extentTestIOS.log(Status.FAIL, "alert popup message is" +popupMessage);
			signUpPageIOS.tapCancelBtn();
		}
	}

	/**
	 * Description This iOS method validates the Error on Signup Page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */

	public void MTA20_ValidateErrorMessageOnSignUp(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String firstName;
		String lastName;
		String password;
		String country;
		String zip;
		String language;
		String popupMessage;
		String currentEmail;
		firstName = "test1";
		lastName = "test2";
		password = "Synechron@123";
		country = "United States";
		zip = "41100";
		language = "English";

		currentEmail = testData.get(0).split(",")[0];
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		signUpPageIOS =new SignUpPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		signUpPageIOS.clickSignup()
		.selectCountry(country)
		.clickSubmit();
		popupMessage = signUpPageIOS.validateSignUp();
		//validation for fields being blank
		if(popupMessage.contains("Please enter all required fields.")) {
			extentTestIOS.log(Status.PASS, "All Mandatory fields are not filled");
		} else {
			extentTestIOS.log(Status.FAIL, "Popup Message for fields being blank not present, actual message is : " +popupMessage);
		}
		//validation for Email not valid
		signUpPageIOS.enterFirstName(firstName)
		.enterLastName(lastName)
		.enterEmail("testemail")
		.enterPassword(password)
		.enterVerifyPassword(password)
		.selectCountry(country)
		.enterZip(zip)
		.selectLanguage(language)
		.clickSubmit();

		popupMessage = signUpPageIOS.validateSignUp();
		if(popupMessage.contains("Please enter a valid email address.")) {
			extentTestIOS.log(Status.PASS, "Email address entered is not valid");
		} else {
			extentTestIOS.log(Status.FAIL, "Popup Message expected for valid email address, actual message is : " +popupMessage);
		}
		//validations for email already exist and verify device error code (201)
		signUpPageIOS.enterEmail(currentEmail)
		.clickSubmit();
		popupMessage = signUpPageIOS.validateSignUp();
		if(popupMessage.contains("That username already exists. Check username or use another username. (201)")) {
			extentTestIOS.log(Status.PASS, "Email address already registered");
			extentTestIOS.log(Status.PASS, "Verify device error code (201) for username already exists");
		} else {
			extentTestIOS.log(Status.FAIL, "Popup Message expected for username already registered, actual message is : " +popupMessage);
			extentTestIOS.log(Status.FAIL, "Device error code (201) not verified for username already exists");
		}
		//validations for password strength
		signUpPageIOS.enterPassword("1234")
		.enterVerifyPassword("1234")
		.clickSubmit();

		popupMessage = signUpPageIOS.validateSignUp();
		if(popupMessage.contains("Password Strength:  Too weak\nPasswords ")) {
			extentTestIOS.log(Status.PASS, "Password Strength: Too weak");
		} else {
			extentTestIOS.log(Status.FAIL, "Popup Message  not present as expected for weak password, actual message is : " +popupMessage);
		}
		//Validation for password not matching

		signUpPageIOS.enterVerifyPassword("Synechron@123")
		.clickSubmit();
		popupMessage = signUpPageIOS.validateSignUp();
		if(popupMessage.contains("Both passwords must match")) {
			extentTestIOS.log(Status.PASS, "Password in password and verify password field is not matching");
		} else {
			extentTestIOS.log(Status.FAIL, "Popup Message  not present as expected, actual message is : " +popupMessage);
		}
		signUpPageIOS.tapCancelBtn();
	}


	/**
	 * Description : This iOS test method verifies the Fields like First name, Last name and Zip code on Signup Page.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */

	public void MTA282_ValidateTextInputLimitOnSignUp(ArrayList<String> testData, String deviceTest,String scenarioName) throws Exception {
		String firstName = "myQautomationmyQautomationmyQautomation";
		String lastName = "synechronmyqsynechronmyqsynechronmyq";
		String zipCode = "1234567890112";
		String textEntered = "";

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();;

		// object for page factory class
		loginPageIOS = new LoginPageIOS(iOSDriver, extentTestIOS);
		signUpPageIOS = new SignUpPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);

		loginPageIOS.selectEnvironment(env)
		.selectUserType(userType);
		// Verify first name 
		signUpPageIOS.clickSignup()
		.selectCountry("United States");
		signUpPageIOS.enterFirstName(firstName);
		textEntered = signUpPageIOS.first_name_textbox.getText();
		int lenFirstName = textEntered.length();
		if (lenFirstName == 30) {
			extentTestIOS.log(Status.PASS, "The First name field accepts only 30 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "First name Text Entered: " + firstName + " ,text retreived after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}
		//Verify Last Name
		signUpPageIOS.enterLastName(lastName);
		textEntered = signUpPageIOS.last_name_textbox.getText();
		int lenLastName = textEntered.length();
		if (lenLastName == 30) {
			extentTestIOS.log(Status.PASS, "The Last name field accepts only 30 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "Last name Text Entered: " + lastName + " ,text retreived after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}

		// Verify Zipcode
		signUpPageIOS.enterZip(zipCode);
		textEntered = signUpPageIOS.zip_textbox.getText();
		int lenZipCode = textEntered.length();

		if (lenZipCode == 10) {
			extentTestIOS.log(Status.PASS, "The Zipcode field accepts only 10 characters.");
		} else {
			extentTestIOS.log(Status.FAIL, "Zipcode Text Entered: " + zipCode + " ,text retreived after entry: " + textEntered);
			utilityIOS.captureScreenshot(iOSDriver);
		}
		signUpPageIOS.tapCancelBtn();
	}

	/**
	 * Description This iOS method verifies Remember me functionality on login
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA23_ValidateRememberMe(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String userName;
		String password;

		userName = testData.get(0).split(",")[0];
		password = testData.get(0).split(",")[1];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		loginPageIOS.clickRememberMe()
		.login(userName, password);
		iOSDriver.resetApp();
		if(loginPageIOS.verifyLogin()) {
			extentTestIOS.log(Status.PASS, "The Remember Me functionality is working as expected");
		} else {
			extentTestIOS.log(Status.FAIL, "The Remember Me functionality is not working as expected");
			utilityIOS.captureScreenshot(iOSDriver);
		}
		menuPageIOS.logout();
	}

	/**
	 * Description This iOS test verifies the Forgot password functionality. Email is entered in the Forgot password link and popup message is validated.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA32_ValidateForgotPassword(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String forgotPasswordMsg;
		String emailID;
		String popupText = "Password Reset\nAn email has been sent to abc@xyz.com. Please check your email inbox to complete the reset process.";
		emailID = testData.get(0).split(",")[0];
		extentTestIOS = createTest(deviceTest,scenarioName);

		loginPageIOS = new LoginPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		loginPageIOS.tapForgotPassword();
		loginPageIOS.enterEmailForgotPassword(emailID);
		if(loginPageIOS.validatePopupPresent()) {
			forgotPasswordMsg = loginPageIOS.getPopupMessage();
			if (forgotPasswordMsg.equalsIgnoreCase(popupText)) {
				extentTestIOS.log(Status.PASS, "Forgot password feature is working as expected "); 
			} else {
				extentTestIOS.log(Status.FAIL, "Forgot password feature not working as expected"); 
				utilityIOS.captureScreenshot(iOSDriver); 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected popup for forget password not present");
			utilityIOS.captureScreenshot(iOSDriver);
		}
	}

	/**
	 * Description This iOS test method verifies the Error messages shown on the Login Screen.
	 * @throws InterruptedException
	 */
	public void MTA194_ValidateErrorMessagesOnLogin(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String currentErrorMessage;
		String errorMessage1 = "Error\nPlease enter your email to login.";
		String errorMessage2 = "Error\nPlease enter your password to login.";
		String errorMessage3 = "Error\nThe username or password you entered is incorrect. Try again. (203)";
		String errorMessage4 = "Error\nThe user is locked out. (207)";
		String errorMessage5 = "Error\nThe user will be locked out. (205)";
        String emailID ;
		extentTestIOS = createTest(deviceTest,scenarioName); 
		emailID = testData.get(0).split(",")[0];
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		loginPageIOS = new LoginPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);

		loginPageIOS.clickLoginButton();
		if(loginPageIOS.validatePopupPresent()) {
			currentErrorMessage= loginPageIOS.getPopupMessage();
			if (currentErrorMessage.equalsIgnoreCase(errorMessage1)) {
				extentTestIOS.log(Status.PASS, "Error Message shown as expected for  email not entered"); 
			} else {
				extentTestIOS.log(Status.FAIL, "Error Message not shown for the email not entered, Actual error message is : "+currentErrorMessage ); 
				utilityIOS.captureScreenshot(iOSDriver); 
			}
		}

		loginPageIOS.enterAccountEmailId(emailID);
		loginPageIOS.clickLoginButton();
		if(loginPageIOS.validatePopupPresent()) {
			currentErrorMessage = loginPageIOS.getPopupMessage();
			if (currentErrorMessage.equalsIgnoreCase(errorMessage2)) {
				extentTestIOS.log(Status.PASS, "Error Message shown as expected for Password not entered"); 
			} else {
				extentTestIOS.log(Status.FAIL, "Error Message not shown for the password not entered, Actual error message is : "+currentErrorMessage ); 
				utilityIOS.captureScreenshot(iOSDriver); 
			}
		}

		//Verify device error code (203)
		loginPageIOS.enterAccountEmailId(emailID);
		loginPageIOS.enterAccountPassword("12345");
		loginPageIOS.clickLoginButton();
		if(loginPageIOS.validatePopupPresent()) {
			currentErrorMessage = loginPageIOS.getPopupMessage();
			if (currentErrorMessage.equalsIgnoreCase(errorMessage3)) {
				extentTestIOS.log(Status.INFO, "Error message :-"+currentErrorMessage); 
				extentTestIOS.log(Status.PASS, "verify device error code (203) for invalid username and password "); 
			} else {
				extentTestIOS.log(Status.FAIL, "device error code (203) not verified : "+currentErrorMessage ); 
				utilityIOS.captureScreenshot(iOSDriver); 
			}
		}
		for(int i=0; i<=4;i++) {
		loginPageIOS.enterAccountEmailId(emailID);
		loginPageIOS.enterAccountPassword("12345");
		loginPageIOS.clickLoginButton();
		loginPageIOS.validatePopupPresent();
		loginPageIOS.getPopupMessage();
		}
		
		loginPageIOS.enterAccountEmailId(emailID);
		loginPageIOS.enterAccountPassword("12345");
		loginPageIOS.clickLoginButton();
		if(loginPageIOS.validatePopupPresent()) {
			currentErrorMessage = loginPageIOS.getPopupMessage();
			if (currentErrorMessage.equalsIgnoreCase(errorMessage5)) {
				extentTestIOS.log(Status.INFO, "Error message :-"+currentErrorMessage); 
				extentTestIOS.log(Status.PASS, "verify device error code (205) for user will be locked out"); 
			} else {
				extentTestIOS.log(Status.FAIL, "device error code (205) not verified : "+currentErrorMessage ); 
				utilityIOS.captureScreenshot(iOSDriver); 
			}
		}
		
		//Verify device error code (207)
		loginPageIOS.enterAccountEmailId(emailID);
		loginPageIOS.enterAccountPassword("12345");
		loginPageIOS.clickLoginButton();
		if(loginPageIOS.validatePopupPresent()) {
			currentErrorMessage = loginPageIOS.getPopupMessage();
			if (currentErrorMessage.equalsIgnoreCase(errorMessage4)) {
				extentTestIOS.log(Status.INFO, "Expected user was already locked.Just to verify error code."); 
				extentTestIOS.log(Status.INFO, "Error message :-"+currentErrorMessage); 
				extentTestIOS.log(Status.PASS, "verify device error code (207) for valid username and invalid password "); 
			} else {
				extentTestIOS.log(Status.FAIL, "device error code (207) not verified : "+currentErrorMessage ); 
				utilityIOS.captureScreenshot(iOSDriver); 
			}
		}
	}

	
	
    /**
     * Description: This iOS test Method adds the stronghold Lock to GDO added in Ethernet gateway. verifies if the Gateway is added, if not then adds it and then adds GDO and then attaches 
	  the Lock to the GDO. After Lock is attached, verification is done on the popup message appearing for lock attached and lock icon on UI
     * @param testData
     * @param deviceTest
     * @param scenarioName
     * @throws ClientProtocolException
     * @throws IOException
     * @throws ParseException
     * @throws InterruptedException
     * @throws AWTException
     */
	public void MTA257_ValidateAddingStrGDOToEthernetGW(ArrayList<String> testData, String deviceTest,String scenarioName)  throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException  {

		String gatewaySerialNumber;
		String gdoSerialNumber;
		String gatewayName;
		String gdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		gdoSerialNumber = testData.get(0).split(",")[2];
		gdoName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(!devicesPageIOS.verifyGatewayAvailable(gatewayName)){
					devicesPageIOS.tapOnBackBtn();
					webService.SendGatewayOnlineService(gatewaySerialNumber);
					devicesPageIOS.addNewDevice();
					devicesPageIOS.addGateWay(gatewaySerialNumber,gatewayName);
					devicesPageIOS.addNewDevice();
				}
				devicesPageIOS.selectGateway(gatewayName)
				.clickGDOButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnGDOToGatewayService(gatewaySerialNumber,gdoSerialNumber);
				devicesPageIOS.enterDeviceName(gdoName);
				devicesPageIOS.clickNextButton();
				webService.detachLockGDO(gdoSerialNumber);
				if(devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
					extentTestIOS.log(Status.PASS, "Device Added Successfully and shown on UI");	
					webService.attachLockGDO(gdoSerialNumber);
					// Below code is commented as per discussion with Onsite SPOC
//					if(devicesPageIOS.verifyAlertForLock()) {
//						extentTestIOS.log(Status.PASS, "Alert appeared for lock attached");
//					} else {
//						extentTestIOS.log(Status.FAIL, "Alert did not appear for lock attached");
//						utilityIOS.captureScreenshot(iOSDriver);
//					}
					devicesPageIOS.actuatingDevice(gdoName)
					.actuatingDevice(gdoName);
					if(devicesPageIOS.verifyLockOnSGDO(gdoName)) {
						extentTestIOS.log(Status.PASS, "Lock attached to GDO");
					} else {
						extentTestIOS.log(Status.FAIL, "Lock not attached");
						utilityIOS.captureScreenshot(iOSDriver);
					}	
				} else {
					extentTestIOS.log(Status.FAIL, "Device Not added");
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device  as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					utilityIOS.captureScreenshot(iOSDriver);
				} 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method to add the Stronghold lock to WGDO. verifies popup for lock attached and the lock icon on the UI
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA251_ValidateAddingStrGDOToWGDO(ArrayList<String> testData, String deviceTest,String scenarioName)  throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException  {

		String wgdoSerialNumber;
		String wgdoName;
		String wgdoDeviceSerialNumber;
		String wgdoDeviceName;
		wgdoSerialNumber = testData.get(0).split(",")[0];
		wgdoName = testData.get(0).split(",")[1];
		wgdoDeviceSerialNumber = testData.get(0).split(",")[2];
		wgdoDeviceName = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(!devicesPageIOS.verifyGatewayAvailable(wgdoName)) {
					webService.SendGatewayOnlineService(wgdoSerialNumber);
					devicesPageIOS.addWGDOAndGDO(wgdoName,wgdoDeviceName);
					webService.detachLockGDO(wgdoDeviceSerialNumber);
				} else {
					devicesPageIOS.tapOnCancelBtn();
				}
				
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				if(devicesPageIOS.verifyDeviceNamePresent(wgdoDeviceName)) {
					extentTestIOS.log(Status.PASS, "Device Added Successfully and shown on UI"); 
				} else {
					extentTestIOS.log(Status.FAIL, "Device Not added");
				}
				webService.attachLockGDO(wgdoDeviceSerialNumber);
				// Below code is commented as per discussion with Onsite SPOC
				if(devicesPageIOS.verifyAlertForLock()) {
					extentTestIOS.log(Status.PASS, "Alert appeared for lock attached");
				} else {
					extentTestIOS.log(Status.FAIL, "Alert did not appear for lock attached");
				}
				devicesPageIOS.actuatingDevice(wgdoDeviceName)
				.actuatingDevice(wgdoDeviceName);
				if(devicesPageIOS.verifyLockOnSGDO(wgdoDeviceName)) {
					extentTestIOS.log(Status.PASS, "Lock attached to GDO");
				} else {
					extentTestIOS.log(Status.FAIL, "Lock not attached");
					utilityIOS.captureScreenshot(iOSDriver);
				}	
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and WGDO as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and WGDO , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add WGDO and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					utilityIOS.captureScreenshot(iOSDriver);
				} 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 *  Description This is an iOS test method that renames the strong hold WGDO and then verifies if that is reflected in Devices page and the revert
	 * back the name of the Stronghold GDo to the original name. 
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA292_ValidateRenamingStrGDOInWGDO(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String wgdoNameInitial;
		String strGdoNameInitial;
		String strGdoNameChanged;

		boolean checkName1;

		wgdoNameInitial = testData.get(0).split(",")[0];
		strGdoNameInitial = testData.get(0).split(",")[1];
		strGdoNameChanged = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		//Navigating to DeviceManagement page

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();
				deviceManagementIOS.selectPlaceName(wgdoNameInitial)
				.selectEndPointDevice(strGdoNameInitial)
				.setDeviceName(strGdoNameChanged);

				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();
				checkName1 = devicesPageIOS.verifyDeviceNamePresent(strGdoNameChanged);
				if (checkName1) {
					extentTestIOS.log(Status.PASS, "Device name succesfully changed from " + strGdoNameInitial + " to " + strGdoNameChanged);
				} else { 
					extentTestIOS.log(Status.FAIL, "Device name not changed to " + strGdoNameChanged); 
					utilityIOS.captureScreenshot(GlobalVariables.iOSDriver);
				}

				//reverting back the name
				menuPageIOS.tappingHamburgerMenu()
				.tappingDeviceManagement();
				deviceManagementIOS
				.selectPlaceName(wgdoNameInitial)
				.selectEndPointDevice(strGdoNameChanged)
				.setDeviceName(strGdoNameInitial);
				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();

				
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Rename hub and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user is able to Rename hub and device, but it is not allowed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method that verifies lock icon on Stronghold GDO while the device state is changing
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA255_ValidateStrGDOLockIconWhenChangingState(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException   {

		String deviceName;
		String currentState;
		boolean deviceOpen;

		deviceName = testData.get(0).split(",")[0];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Devices")) {
				menuPageIOS.tappingDevices();
				if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
					currentState = devicesPageIOS.getDeviceStatus(deviceName);
					if(currentState.equalsIgnoreCase("Open")) {
						deviceOpen = true;
					} else{
						deviceOpen = false;
					}
					//Verify Lock Icon while device state is changing
					if (devicesPageIOS.validatingLockIconWhileChangingState(deviceName)) {
						extentTestIOS.log(Status.PASS, "Lock Icon present while door is " + (deviceOpen? "Closing" :"Opening"));		
					} else {
						extentTestIOS.log(Status.FAIL, "Lock Icon not present while door is " + (deviceOpen? "Closing" :"Opening"));	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					//verify lock icon when device state is changed
					if (devicesPageIOS.verifyLockOnSGDO(deviceName)) {
						extentTestIOS.log(Status.PASS, "Lock Icon present when Door is " + (deviceOpen? "Closed" :"Open"));		
					} else {
						extentTestIOS.log(Status.FAIL, "Lock Icon not present while door is " + (deviceOpen? "Closed" :"Open"));	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					//Verify Lock Icon while device state is changing
					if (devicesPageIOS.validatingLockIconWhileChangingState(deviceName)) {
						extentTestIOS.log(Status.PASS, "Lock Icon present while door is " + (deviceOpen? "Opening" :"Closing"));		
					} else {
						extentTestIOS.log(Status.FAIL, "Lock Icon not present while door is " + (deviceOpen? "Opening" :"Closing"));	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					//verify lock icon when device state is changed
					if (devicesPageIOS.verifyLockOnSGDO(deviceName)) {
						extentTestIOS.log(Status.PASS, "Lock Icon present when Door is " + (deviceOpen? "Open" :"Closed"));		
					} else {
						extentTestIOS.log(Status.FAIL, "Lock Icon not present while door is "+ (deviceOpen? "Open" :"Closed"));	
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Expected device not available");	
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, "Expected Devices section is available to " + activeUserRole + " user");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, "Expected Devices section should be available, but not present to " + activeUserRole + " user");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method that verifies the lock icon on Stronghold GDO in grid view on device Page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA262_ValidateStrGDOLockIconInGridView(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String deviceName;
		String hubName;

		hubName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Devices")) {
				menuPageIOS.tappingDevices();
				devicesPageIOS.toggleDeviceLayout(true);
				if(devicesPageIOS.checkLockIconInGridView(hubName, deviceName)) {
					extentTestIOS.log(Status.PASS, "Device Lock is present on the SGDO");
				} else {
					extentTestIOS.log(Status.FAIL, "Device Lock is not present on the SGDO");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				devicesPageIOS.toggleDeviceLayout(false);
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")||activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access the device section");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")||activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This is an iOS test method that verifies the lock icon on Stronghold GDO in gallery view on device Page
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA537_ValidateStrGDOLockIconInGalleryView(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String deviceName;
		String hubName;

		hubName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Devices")) {
				menuPageIOS.tappingDevices();
				devicesPageIOS.toggleGalleryView(true);
				if(devicesPageIOS.checkLockIconInGalleryView(hubName, deviceName)) {
					extentTestIOS.log(Status.PASS, "Device Lock is present on the SGDO");
					utilityIOS.captureScreenshot(iOSDriver);
				} else {
					extentTestIOS.log(Status.FAIL, "Device Lock is not present on the SGDO");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				devicesPageIOS.toggleGalleryView(false);
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")||activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access the device section");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")||activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 *  Description This is an iOS test method that verifies that lock Icon is not shown on the UI when the lock is removed/Detached from Strong hold GDO
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA267_ValidateStrGDOLockIconWhenDetached(ArrayList<String> testData, String deviceTest,String scenarioName)  throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException  {

		String gdoSerialNumber;
		String strGdoName;

		gdoSerialNumber = testData.get(0).split(",")[0];
		strGdoName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Devices")) {
				menuPageIOS.tappingDevices();
				if(devicesPageIOS.verifyDeviceNamePresent(strGdoName)) {
					webService.detachLockGDO(gdoSerialNumber);
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if(!devicesPageIOS.verifyLockOnSGDO(strGdoName)) {
						extentTestIOS.log(Status.PASS, "Lock not available on the SGDO");
					} else {
						extentTestIOS.log(Status.FAIL, "Lock attached on the SGDO");
						utilityIOS.captureScreenshot(iOSDriver);
					}	
					webService.attachLockGDO(gdoSerialNumber);
					devicesPageIOS.verifyAlertForLock();
				} else {
					extentTestIOS.log(Status.FAIL, "Expected device not present");
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Device section as Expected");
				} 
			} else {
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, "Expected Devices section should be available, but not present to " + activeUserRole + " user");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description : This function deletes the Light device and validates that the schedule and Alerts are also deleted. The methods adds a virtual light
	 * create schedule and Alert for the added light and then deletes the added light and then verifies whether the associated schedule and Alert are also deleted.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA294_ValidateScheduleAlertOnDeletingLight(ArrayList<String> testData, String deviceTest,String scenarioName) throws Exception {
		boolean enableNotification=false;
		String gatewaySerialNumber;
		String lightSerialNumber;
		String gatewayName;
		String lightName;
		boolean allDays = true;
		boolean alertPresent = false;
		boolean pushNotification = true;
		HashMap<String, String> deviceStatePairList = new HashMap<>();

		gatewayName = testData.get(0).split(",")[0];
		gatewaySerialNumber = testData.get(0).split(",")[1];
		lightName = testData.get(0).split(",")[2];
		lightSerialNumber = testData.get(0).split(",")[3];
		
		deviceStatePairList.put(lightName, "Off");
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver, extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver, extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) { 
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				devicesPageIOS.addNewDevice(); // First add Light Device
				if(!devicesPageIOS.verifyGatewayAvailable(gatewayName)){
					devicesPageIOS.tapOnCancelBtn();
					devicesPageIOS.addGateWay(gatewaySerialNumber,gatewayName);	
					devicesPageIOS.addNewDevice();
				}
				devicesPageIOS.selectGateway(gatewayName)
				.clickRemoteLightButton()
				.clickNextButton();

				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnLightToGatewayService(gatewaySerialNumber,lightSerialNumber);

				devicesPageIOS.enterDeviceName(lightName);	// Rename the Light device 
				devicesPageIOS.clickNextButton();

				menuPageIOS.tappingHamburgerMenu();  //Create Schedule
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.createSchedule("ScheduleDelete",deviceStatePairList,enableNotification,allDays);

					if (!schedulePageIOS.validateNoScheduleText() && schedulePageIOS.verifySchedulePresent("ScheduleDelete")) {
						extentTestIOS.log(Status.PASS, "schedule with name 'ScheduleDelete' created");
					} else {
						extentTestIOS.log(Status.FAIL, "schedule with name 'ScheduleDelete' not created");
					}
				} else {
					extentTestIOS.log(Status.INFO, "User does not have access to Schedule section");
					utilityIOS.captureScreenshot(iOSDriver);
					menuPageIOS.tappingDevices();
				}

				menuPageIOS.tappingHamburgerMenu();// Create Alert
				if (menuPageIOS.verifyMenuLink("Alerts")) {
					menuPageIOS.tappingAlerts();
					alertPageIOS.tapAddAlert()
					.selectDeviceForAlert(lightName)
					.setAlertName("AlertDelete")
					.toggleLightTurnedOn("Enabled")
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(lightName,"AlertDelete");
					if (alertPresent) {
						extentTestIOS.log(Status.PASS, "Alert 'AlertDelete' created.");
					} else {
						extentTestIOS.log(Status.FAIL, "Alert 'AlertDelete' not created.");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
				}else {
					extentTestIOS.log(Status.INFO, "User does not have access to Alert section");
					utilityIOS.captureScreenshot(iOSDriver);
					menuPageIOS.tappingDevices();
				}
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDeviceManagement();
				devicesPageIOS.selectGateway(gatewayName);
				deviceManagementIOS.deleteDevice(lightName)
				.tapBackBtn();

				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingSchedules();

				if (schedulePageIOS.validateNoScheduleText() || !schedulePageIOS.verifySchedulePresent("ScheduleDelete")) {
					extentTestIOS.log(Status.PASS, "On deleting the device Light the schedule 'ScheduleDelete' got deleted successfully.");
				} else {
					extentTestIOS.log(Status.FAIL, "On deleting the device still Light schedule 'ScheduleDelete' exist.");
				}

				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingAlerts();
				if (alertPageIOS.validateNoAlertText() || !alertPageIOS.verifyAlertAvailableForDevice(lightName,"AlertDelete")) {
					extentTestIOS.log(Status.PASS, "Alert 'AlertDelete' deleted on deleting device.");
				} else {
					extentTestIOS.log(Status.FAIL, "Alert 'AlertDelete' not deleted even on deleting device");
					utilityIOS.captureScreenshot(iOSDriver);
				}

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestIOS.log(Status.FAIL, activeUserRole + " User was able to access device Management section, but it is not allowed ");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole + " user was able to access device Management section as expected");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestIOS.log(Status.PASS, activeUserRole + " User doesn't have the premission to add devices as expected");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestIOS.log(Status.FAIL, activeUserRole + " User doesn't have permission to add devices, but should be available");
				}
			}
		}
		else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}	
	}

	/**Description : This function deletes the Gateway and verifies device,schedule and alerts associated with the Device are also deleted.
	 * 				 This method adds virtual gateway, adds virtual light and then creates alert,schedule.Before deleting the hub verification is done on the Schedule trigger and history entry of the schedule.
	 * 				After Deletion verifications are done on device, Alert and schedule getting deleted, but history entry is still present.
	 * 
	 * @param testData
	 * @param deviceTest
	 * @throws Exception
	 */
	public void MTA339_ValidateEPDOnDeletingGateway(ArrayList<String> testData, String deviceTest,String scenarioName) throws Exception {
		boolean enableNotification=true;
		String gatewaySerialNumber;
		String lightSerialNumber;
		boolean allDays = true;
		boolean alertPresent = false;
		boolean pushNotification = true;
		boolean scheduleTriggered = false;
		String lightName;
		String eventName;
		String eventTime = null;
		String gatewayName;
		
		HashMap<String, String> deviceStatePairList = new HashMap<>();

		gatewayName = testData.get(0).split(",")[0];
		gatewaySerialNumber = testData.get(0).split(",")[1];
		lightName = testData.get(0).split(",")[2];
		lightSerialNumber = testData.get(0).split(",")[3];
		
		deviceStatePairList.put(lightName, "Off");

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver, extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver, extentTestIOS);
		alertPageIOS = new AlertPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);
		
		menuPageIOS.tappingHamburgerMenu()
		.tappingDevices();
		if (expectedRolePresent) { 
			if (devicesPageIOS.verifyAddDeviceButton()){
				webService.SendGatewayOnlineService(gatewaySerialNumber);
				devicesPageIOS.addGateWay(gatewaySerialNumber,gatewayName);
				menuPageIOS.tappingHamburgerMenu()
				.tappingDevices();
				devicesPageIOS.addNewDevice() // First add Light Device
				.selectGateway(gatewayName)
				.clickRemoteLightButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber)
				.LearnLightToGatewayService(gatewaySerialNumber,lightSerialNumber);

				Thread.sleep(3000);
				devicesPageIOS.enterDeviceName(lightName);	// Rename the Light device 
				devicesPageIOS.clickNextButton();

				menuPageIOS.tappingHamburgerMenu();// Create Alert
				if (menuPageIOS.verifyMenuLink("Alerts")) {
					menuPageIOS.tappingAlerts();
					alertPageIOS.tapAddAlert()
					.selectDeviceForAlert(lightName)
					.setAlertName("AlertDelete")
					.toggleLightTurnedOn("Enabled")
					.togglePushNotification(pushNotification)
					.tapSaveAlertBtn();
					alertPresent = alertPageIOS.verifyAlertAvailableForDevice(lightName,"AlertDelete");
					if (alertPresent) {
						extentTestIOS.log(Status.PASS, "For device " + lightName + " the Alert 'AlertDelete' created.");
					} else {
						extentTestIOS.log(Status.FAIL, "For device "+ lightName + " the Alert 'AlertDelete' is not created.");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.INFO, "Alert Link is not available.");
					utilityIOS.captureScreenshot(iOSDriver);
					menuPageIOS.tappingDevices();
				}
				
				menuPageIOS.tappingHamburgerMenu();  //Create Schedule
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.createSchedule("ScheduleDelete",deviceStatePairList,enableNotification,allDays);

					if (!schedulePageIOS.validateNoScheduleText() && schedulePageIOS.verifySchedulePresent("ScheduleDelete")) {
						extentTestIOS.log(Status.PASS, "Schedule with name 'ScheduleDelete' created");
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name 'ScheduleDelete' not created");
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingDevices();
					if (schedulePageIOS.validateScheduleTrigger()) {
						if (devicesPageIOS.getAlertMessage().contains("ScheduleDelete")) {
							extentTestIOS.log(Status.PASS,  "ScheduleDelete" +" Triggered successfully");
							scheduleTriggered = true;
						}
					} else {
						extentTestIOS.log(Status.FAIL, "ScheduleDelete" + " did not triggered");
					}
					
					eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
					Set set = deviceStatePairList.entrySet();
					Iterator iterator = set.iterator();
					while(iterator.hasNext()) {
						Map.Entry deviceStatePair = (Map.Entry)iterator.next();
						String deviceName = (String) deviceStatePair.getKey();
						String deviceState = (String) deviceStatePair.getValue();
						if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
							if (devicesPageIOS.validateDeviceStatus(deviceName,deviceState)) {
								extentTestIOS.log(Status.PASS, "State of " + deviceName + " changed successfully"); 
								scheduleTriggered = true;
							} else {
								extentTestIOS.log(Status.FAIL, "State of " + deviceName + " did not change to " +deviceState);
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {
							extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}
					//Verify History before deletion of Gateway
					if (scheduleTriggered) { 
						eventName = "ScheduleDelete" + " was triggered";
						menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
								extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
								menuPageIOS.tappingDevices();
							} else {
								extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
						} else {	// Verify history for the remaining user types
							menuPageIOS.tappingHistory();
							if (historyPageIOS.verifyHistory(eventName,eventTime)) {
								extentTestIOS.log(Status.PASS, "Before deletion of Gateway History event name: '" + eventName +"' is logged successfully");	
							} else {
								extentTestIOS.log(Status.FAIL, "Before deletion of Gateway History event name: '" + eventName +"' is not logged ");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
							//Verify Light state change
							eventName = deviceName + " just turned On";
							if (historyPageIOS.verifyHistory(eventName,eventTime)) {
								extentTestIOS.log(Status.PASS, "Before deleting Gateway History event name: '" + eventName +"' is logged successfully");	
							} else {
								extentTestIOS.log(Status.FAIL, "Before deleting Gateway History event name: '" + eventName +"' is not logged ");	
								utilityIOS.captureScreenshot(iOSDriver);
							}
							extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule not triggerred hence History could not be verified.");
					}
				} else {
					extentTestIOS.log(Status.INFO, "Schedule Link is not available.");
					utilityIOS.captureScreenshot(iOSDriver);
					menuPageIOS.tappingDevices();
				}
				menuPageIOS.tappingHamburgerMenu();// Deleting the Gateway
				menuPageIOS.tappingDeviceManagement();
				deviceManagementIOS.deleteHub(gatewayName);

				menuPageIOS.tappingHamburgerMenu() // Verify light device deleted
				.tappingDevices();
				if(!devicesPageIOS.verifyDeviceNamePresent(lightName)){
					extentTestIOS.log(Status.PASS, "On deleting the Gateway light device " + lightName + " also got deleted successfully.");
				} else {
					extentTestIOS.log(Status.FAIL, "On deleting the Gateway still Light device " + lightName + " exist.");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Verify Schedule is also deleted automatically
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingSchedules();
				if (schedulePageIOS.validateNoScheduleText() || !schedulePageIOS.verifySchedulePresent("ScheduleDelete")) {
					extentTestIOS.log(Status.PASS, "On deleting the Gateway the schedule 'ScheduleDelete' got deleted successfully.");
				} else {
					extentTestIOS.log(Status.FAIL, "On deleting the Gateway still Light schedule 'ScheduleDelete' exist.");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Verify Alert is also deleted		
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingAlerts();
				if (alertPageIOS.validateNoAlertText() || !alertPageIOS.verifyAlertAvailableForDevice(lightName,"AlertDelete")) {
					extentTestIOS.log(Status.PASS, "Alert 'AlertDelete' deleted on deleting gateway.");
				} else {
					extentTestIOS.log(Status.FAIL, "Alert 'AlertDelete' not deleted even on deleting gateway");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Verify History After deletion of Gateway
				if (scheduleTriggered) { 
					eventName = "ScheduleDelete" + " was triggered";
					menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
							extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
							menuPageIOS.tappingDevices();
						} else {
							extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {	// Verify history for the remaining user types
						menuPageIOS.tappingHistory();
						if (historyPageIOS.verifyHistory(eventName,eventTime)) {
							extentTestIOS.log(Status.PASS, "After deletion of Gateway History event name: '" + eventName +"' is logged successfully");	
						} else {
							extentTestIOS.log(Status.FAIL, "After deletion of Gateway History event name: '" + eventName +"' is not logged ");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
						//Verify Light state change
						eventName = deviceName + " just turned On";
						if (historyPageIOS.verifyHistory(eventName,eventTime)) {
							extentTestIOS.log(Status.PASS, "After deleting Gateway History event name: '" + eventName +"' is logged successfully");	
						} else {
							extentTestIOS.log(Status.FAIL, "After deleting Gateway History event name: '" + eventName +"' is not logged ");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
						extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
					} // Else condition skipped, refer section '//Verify History before deletion of Gateway'
				}

				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestIOS.log(Status.FAIL, activeUserRole + " User was able to add/delete Gateway");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole + " User was able to add/delete Gateway");
				}
			} else {
				if ((activeUserRole.equals("Guest")) ||  (activeUserRole.equals("Family"))) {
					extentTestIOS.log(Status.PASS, "User doesn't have the premission to add/delete Gateway");
				} else if (activeUserRole.equals("Admin") || activeUserRole.equals("Creator")) { 
					extentTestIOS.log(Status.FAIL, activeUserRole + " User doesn't have the premission to add/delete Gateway");
				}
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}	
	}

	/**
	 * Description	 :	This iOS test method add VGDO into A-hub/Wifi-hub and verifies whether the VGDO is added successfully in A-hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA312_ValidateAddingVGDOIntoAhubOrWifiHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String gatewayName;
		String vgdoSerialNumber;
		String vgdoName;
		String sensorType;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName =testData.get(0).split(",")[1];
		vgdoSerialNumber =testData.get(0).split(",")[2];
		vgdoName = testData.get(0).split(",")[3];
		sensorType =  testData.get(0).split(",")[4];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(devicesPageIOS.verifyGatewayAvailable(gatewayName)) {
					extentTestIOS.log(Status.PASS, "Expected hub Present");	
					devicesPageIOS.selectGateway(gatewayName)
					.clickGDOButton()
					.clickNextButton()
					.clickNextButton();
					webService.enterLearnModeForVGDO(vgdoSerialNumber);
					iOSDriver.getCapabilities();
					webService.testSensorVGDO(vgdoSerialNumber, sensorType);
					devicesPageIOS.selectGDOBrand("LiftMaster")
					.selectProgramButtonColor("Yellow")
					.tapProgramDoorOpener();
					devicesPageIOS.enterDeviceName(vgdoName);
					devicesPageIOS.clickNextButton();
					devicesPageIOS.clickFinishButton();
					if(devicesPageIOS.verifyDeviceNamePresent(vgdoName)) {
						extentTestIOS.log(Status.PASS, "VGDO added successfully in hub");	
					} else {
						extentTestIOS.log(Status.FAIL, "Unable to Add VGDO in hub ");	
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Expected hub not present");	
					utilityIOS.captureScreenshot(iOSDriver);
					devicesPageIOS.tapOnCancelBtn();
				} if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  Description    :   This iOS test method verifies the sensor communication error shown on UI for VGDO added in A-hub. Through API call, VGDO is set to DPS communication error state 
	 * 					and then error on the VGDO is verified aafter that through API we are exiting the sensor communication error.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA317_ValidateSensorCommnErrorForVGDO(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String vgdoSerialNumber;
		String vgdoName;
		String scheduleName;
		String deviceState1;
		boolean enableNotification = true;
		boolean allDays = true;
		String sensorCommnErrorMessage = "The sensor for this garage door is not communicating, so it cannot be controlled.";
		vgdoSerialNumber = testData.get(0).split(",")[0];
		vgdoName = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];
		deviceState1 = testData.get(0).split(",")[3];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(vgdoName, deviceState1);

		WebServices webService = new WebServices(extentTestIOS);
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				webService.enterDPSNoCommunication(vgdoSerialNumber);
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				if(devicesPageIOS.verifyDeviceNamePresent(vgdoName)) { 
					if(devicesPageIOS.verifyErrorOnDevice(vgdoName, "Sensor Error", sensorCommnErrorMessage)) {
						extentTestIOS.log(Status.PASS, "Sensor Error is shown on UI");
					} else {
						extentTestIOS.log(Status.FAIL, "Sensor error is  not shown in UI");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					//MTA-488 Implementation: Schedule should not get triggered when the parent gateway is offline
					menuPageIOS.tappingHamburgerMenu();
					if (menuPageIOS.verifyMenuLink("Schedules")) {
						menuPageIOS.tappingSchedules();
						schedulePageIOS.tapAddScheduleBtn()
						.tapCancelBtn()
						.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
						if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
							extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");

							if (!schedulePageIOS.validateScheduleTrigger()) {
								extentTestIOS.log(Status.PASS,  "Since the VGDO "+ vgdoName +" in Sensor Error state "+scheduleName +" did not triggered as expected");
							} else {
								extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when the VGDO "+ vgdoName +" in Sensor Error state");
								utilityIOS.captureScreenshot(iOSDriver);
								devicesPageIOS.getAlertMessage();
							}
						} else {
							extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						menuPageIOS.tappingHamburgerMenu()
						.tappingSchedules();
						schedulePageIOS.deleteSchedule(scheduleName);

						if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
						} else if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
						}
					} else {
						if (activeUserRole.equalsIgnoreCase("Guest")) {
							extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
						} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
							extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
							utilityIOS.captureScreenshot(iOSDriver);
						} 
						// To bring back to the Home screen
						menuPageIOS.tappingDevices();
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				webService.exitDPSNoCommunication(vgdoSerialNumber);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * 	 * Description    :   This iOS test method verifies the monitor only error shown on UI for VGDO added in A-hub. Through API call, VGDO is set to monitor only error state 
	 * 					      and then error on the VGDO is verified after that through API we are exiting the sensor communication error.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA318_ValidateMonitorOnlyErrorForVGDO(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		
		String vgdoSerialNumber;
		String monitorOnlyError = "This door is in monitor only mode.";
		String vgdoName;
		String scheduleName;
		String ErrorPopupMsg;
		
		vgdoSerialNumber =testData.get(0).split(",")[0];
		vgdoName = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);

		WebServices webService = new WebServices(extentTestIOS);
		if (expectedRolePresent) {
			webService.setDpsActionableOnly(vgdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			webService.setDpsMonitorOnly(vgdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(vgdoName)) { 
				if(devicesPageIOS.verifyErrorOnDevice(vgdoName, "Monitor Door Only", monitorOnlyError)) {
					extentTestIOS.log(Status.PASS, "Sensor Error is shown on UI");
				} else {
					extentTestIOS.log(Status.FAIL, "sensor error not shown in UI");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should not get triggered when the VGDO is in Sensor error state
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.enterScheduleName(scheduleName);
					if(!schedulePageIOS.verifyDeviceForScheduleCreation(vgdoName)) {
						extentTestIOS.log(Status.PASS,  ""+ vgdoName +" device is not present for schedule creation, as the VGDO  is in monitor only state.");
						schedulePageIOS.tapSaveBtn();
						if (schedulePageIOS.validateErrorMessage()) {
							ErrorPopupMsg = schedulePageIOS.getErrorMessage();
							if (ErrorPopupMsg.contains("Please select a device and state for the schedule")) {
								extentTestIOS.log(Status.INFO, "Error popup present for selecting the device and state, since "+ vgdoName +"was not available as expected.");
								schedulePageIOS.tapCancelBtn();
							} else {
								extentTestIOS.log(Status.FAIL, "Different Popup message shown, the text is: " +ErrorPopupMsg);
								//here screenshot is not captured because alert is dismissed in the previous step and taking screenshot won't capture the error scenario
							}
						}
					} else {
						extentTestIOS.log(Status.FAIL,  ""+ vgdoName +" device is present for schedule creation, when the VGDO  is in monitor only state.");
						schedulePageIOS.tapCancelBtn();
					}
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			webService.setDpsActionableOnly(vgdoSerialNumber);
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description This iOS test method transfers Light from Ethernet Gateway to A-hub.
	 * verification is done for the transferred device and then the device is transferred back to Ethernet Hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws AWTException
	 */
	public void MTA321_ValidatingTransferLightBetweenEthernetAndAHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException, ClientProtocolException, IOException, AWTException {

		String toHubSerialNumber;
		String toHubName;
		String fromHubSerialNumber;
		String fromHubName;
		String lightName;
		String lightSerialNumber;

		toHubSerialNumber = testData.get(0).split(",")[0];
		toHubName = testData.get(0).split(",")[1];
		fromHubSerialNumber = testData.get(0).split(",")[2];
		fromHubName = testData.get(0).split(",")[3];
		lightSerialNumber = testData.get(0).split(",")[4];
		lightName = testData.get(0).split(",")[5];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		if (expectedRolePresent) { 
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				devicesPageIOS.addNewDevice();
				//Verifying the whether the hub1 with the expected device is present
				if (devicesPageIOS.verifyGatewayAvailable(fromHubName)){
					extentTestIOS.log(Status.INFO, "hub1 " +fromHubName + " is available");
					devicesPageIOS.tapOnBackBtn();
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDeviceManagement();
					deviceManagementIOS.selectPlaceName(fromHubName);
					if(deviceManagementIOS.verifyEndPointDevice(lightName)) {
						extentTestIOS.log(Status.INFO, "Device "+lightName + "is present" );	 
						deviceManagementIOS.tapBackBtn();
					} else {
						extentTestIOS.log(Status.INFO, "As Device "+lightName + "is not present, hence adding." );	
						devicesPageIOS.tapOnBackBtn();
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices();
						devicesPageIOS.addNewDevice()// adding the device to be transfered
						.selectGateway(fromHubName)
						.clickRemoteLightButton()
						.clickNextButton();
						webService.GatewayStartLearningService(fromHubSerialNumber);
						webService.LearnLightToGatewayService(fromHubSerialNumber,lightSerialNumber);
						devicesPageIOS.enterDeviceName(lightName);
						devicesPageIOS.clickNextButton();
					}
				} else { 
					extentTestIOS.log(Status.INFO, "As hub1 " +fromHubSerialNumber + " is not available, adding the hub1");
					devicesPageIOS.tapOnBackBtn()
					.addNewDevice();
					devicesPageIOS.addGateWay(fromHubSerialNumber,fromHubName) // Adding the hub1 
					.addNewDevice()
					.selectGateway(fromHubName)
					.clickRemoteLightButton()
					.clickNextButton();
					webService.GatewayStartLearningService(fromHubSerialNumber);
					webService.LearnLightToGatewayService(fromHubSerialNumber,lightSerialNumber);
					devicesPageIOS.enterDeviceName(lightName);
					devicesPageIOS.clickNextButton();
				}
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				devicesPageIOS.addNewDevice();
				//Verifying the hub2 is added and adding if not added
				if(!devicesPageIOS.verifyGatewayAvailable(toHubName)) {
					extentTestIOS.log(Status.INFO, "As hub1 " +toHubName + " is not available, adding the hub1");
					devicesPageIOS.tapOnBackBtn()
					.addNewDevice()
					.addGateWay(toHubSerialNumber,toHubName);
				} else {
					extentTestIOS.log(Status.INFO, "As hub2 " +toHubName + " is available");
					devicesPageIOS.tapOnBackBtn();
				}
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				devicesPageIOS.addNewDevice();
				devicesPageIOS.selectGateway(toHubName);

				devicesPageIOS.clickDeviceTransferButton();
				if(devicesPageIOS.verfiyDeviceAndClick(lightName)) 
				{
					extentTestIOS.log(Status.INFO, "Expected device are available in Hub-"+fromHubSerialNumber);
					devicesPageIOS.clickNextButton();

					webService.GatewayStartLearningService(toHubSerialNumber);
					webService.LearnLightToGatewayService(toHubSerialNumber,lightSerialNumber);
					devicesPageIOS.tapSubmitButtonForTransferDevice();
					if (	devicesPageIOS.validateAlert())
					{
						if (devicesPageIOS.getAlertMessage().contains("Pairing Complete"))
						{
							extentTestIOS.log(Status.INFO, "Popup shown for transfer pairing complete" );
						}
					} else {
						extentTestIOS.log(Status.INFO, "Popup for pairing complete is not shown" );
						utilityIOS.captureScreenshot(iOSDriver);
					}
					devicesPageIOS.tapOnCancelBtn();
					devicesPageIOS.tapOnBackBtn();
					if (!devicesPageIOS.verifyGatewayAvailable(fromHubName)) {
						extentTestIOS.log(Status.INFO, "Gateway "+fromHubName+" not appeared after successful pairing" );
					} else {
						extentTestIOS.log(Status.INFO, "Gateway "+fromHubName+" appeared after successful pairing because other devices are presents" );
					}
					devicesPageIOS.tapOnBackBtn();
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDeviceManagement();
					deviceManagementIOS.selectPlaceName(toHubName);
					if(deviceManagementIOS.verifyEndPointDevice(lightName)) {
						extentTestIOS.log(Status.PASS, "Device "+lightName + "successfully transferred" );	 
					} else {
						extentTestIOS.log(Status.FAIL, "Device "+lightName + " not transferred" );	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					devicesPageIOS.tapOnBackBtn();
					// Verifying whether the remaining devices are available once the expected device is transfered
					if(deviceManagementIOS.verifyHubAvailable(fromHubName)) {
						deviceManagementIOS.selectPlaceName(fromHubName);
						if(deviceManagementIOS.verifyDeviceAvailableOnHub()) {
							extentTestIOS.log(Status.PASS, "Device Present in the hub " +fromHubName);
						} else {
							extentTestIOS.log(Status.FAIL, "No device available in the Hub  after device transfer, but the hub " +fromHubName+ " is still shown");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						devicesPageIOS.tapOnBackBtn();
					} else {
						extentTestIOS.log(Status.PASS, "Hub got removed from the account after device was transfered");
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices(); 
						devicesPageIOS.addGateWay(fromHubSerialNumber,fromHubName);
					}
					//Transfering back the light device to Ethernet Hub
					extentTestIOS.log(Status.INFO, "Start reverting device to the previous gateway" );
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDevices();
					devicesPageIOS.addNewDevice();
					devicesPageIOS.selectGateway(fromHubName);
					devicesPageIOS.clickDeviceTransferButton();
					if(devicesPageIOS.verfiyDeviceAndClick(lightName)) {
						extentTestIOS.log(Status.INFO, "Expected device are available in Hub-"+toHubName);
						devicesPageIOS.clickNextButton();
						webService.GatewayStartLearningService(fromHubSerialNumber);
						webService.LearnLightToGatewayService(fromHubSerialNumber,lightSerialNumber);

						devicesPageIOS.tapSubmitButtonForTransferDevice();
						if (	devicesPageIOS.validateAlert())
						{
							if (devicesPageIOS.getAlertMessage().contains("Pairing Complete"))
							{
								extentTestIOS.log(Status.INFO, "Popup shown for transfer pairing complete" );
							}
						}
						devicesPageIOS.tapOnCancelBtn();
						devicesPageIOS.tapOnBackBtn();
						devicesPageIOS.tapOnCancelBtn();
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Expected device are not available in Hub-"+fromHubName);
					utilityIOS.captureScreenshot(iOSDriver);
				}

				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "User has permission to transfer devices as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  User has permission has to transfer device, but it is not allowed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}	 else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Transfer device as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Transfer device");
					utilityIOS.captureScreenshot(iOSDriver);
				} 

			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description  * Description This iOS test method transfers GDO from Ethernet Gateway to A-hub.
	 * verification is done for the transferred device and then the GDO is transferred back to Ethernet Hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws AWTException
	 */
	public void MTA321_ValidateTransferGDOBetweenEthernetAndAHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException, ClientProtocolException, IOException, AWTException {

		String toGatewaySerialNumber;
		String toGatewayName;
		String fromGatewaySerialNumber;
		String fromGatewayName;
		String GDOSerialNumber;
		String GDOName;

		toGatewaySerialNumber = testData.get(0).split(",")[0];
		toGatewayName = testData.get(0).split(",")[1];
		fromGatewaySerialNumber = testData.get(0).split(",")[2];
		fromGatewayName = testData.get(0).split(",")[3];
		GDOSerialNumber = testData.get(0).split(",")[4];
		GDOName = testData.get(0).split(",")[5];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) { 
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				devicesPageIOS.addNewDevice();
				//Verifying the whether the hub1 with the expected device is present
				if (devicesPageIOS.verifyGatewayAvailable(fromGatewayName)){
					extentTestIOS.log(Status.INFO, "hub1 " +fromGatewayName + " is available");
					devicesPageIOS.tapOnBackBtn();
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDeviceManagement();
					deviceManagementIOS.selectPlaceName(fromGatewayName);
					if(deviceManagementIOS.verifyEndPointDevice(GDOName)) {
						extentTestIOS.log(Status.INFO, "Device "+GDOName + "is present" );	 
						deviceManagementIOS.tapBackBtn();
					} else {
						extentTestIOS.log(Status.INFO, "As Device "+GDOName + "is not present, hence adding." );	
						devicesPageIOS.tapOnBackBtn();
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices();
						devicesPageIOS.addNewDevice() //adding the device to be transfered
						.selectGateway(fromGatewayName)
						.clickGDOButton()
						.clickNextButton();
						webService.GatewayStartLearningService(fromGatewaySerialNumber);
						webService.LearnGDOToGatewayService(fromGatewaySerialNumber,GDOSerialNumber);
						devicesPageIOS.enterDeviceName(GDOName);
						devicesPageIOS.clickNextButton();
					}
				} else { 
					extentTestIOS.log(Status.INFO, "As hub1 " +fromGatewaySerialNumber + " is not available, adding the hub2");
					devicesPageIOS.tapOnBackBtn();
					// Adding the hub1 and device to be transfered
					devicesPageIOS.addGateWay(fromGatewaySerialNumber,fromGatewayName)
					.tapOnBackBtn()
					.addNewDevice()
					.selectGateway(fromGatewayName)
					.clickGDOButton()
					.clickNextButton();
					webService.GatewayStartLearningService(fromGatewaySerialNumber);
					webService.LearnGDOToGatewayService(fromGatewaySerialNumber,GDOSerialNumber);
					devicesPageIOS.enterDeviceName(GDOName);
					devicesPageIOS.clickNextButton();
				}
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				devicesPageIOS.addNewDevice();
				//Verifying the hub2 is added and adding the same if not present
				if(!devicesPageIOS.verifyGatewayAvailable(toGatewayName)) {
					extentTestIOS.log(Status.INFO, "As hub2 " +toGatewayName + " is not available, adding the hub2");
					devicesPageIOS.tapOnCancelBtn()
					.addGateWay(toGatewaySerialNumber,toGatewayName);
				} else {
					extentTestIOS.log(Status.INFO, "As hub2 " +toGatewayName + " is available");
					devicesPageIOS.tapOnCancelBtn();
				}
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				devicesPageIOS.addNewDevice();
				devicesPageIOS.selectGateway(toGatewayName);

				devicesPageIOS.clickDeviceTransferButton();
				if(devicesPageIOS.verfiyDeviceAndClick(GDOName)) 
				{
					extentTestIOS.log(Status.INFO, "Expected device are available in Hub-"+fromGatewayName);
					devicesPageIOS.clickNextButton();

					webService.GatewayStartLearningService(toGatewaySerialNumber);
					webService.LearnGDOToGatewayService(toGatewaySerialNumber,GDOSerialNumber);
					devicesPageIOS.tapSubmitButtonForTransferDevice();
					if (	devicesPageIOS.validateAlert())
					{
						if (devicesPageIOS.getAlertMessage().contains("Pairing Complete"))
						{
							extentTestIOS.log(Status.INFO, "Popup shown for transfer pairing complete" );
						}
					} else {
						extentTestIOS.log(Status.INFO, "Popup for pairing complete is not shown" );
						utilityIOS.captureScreenshot(iOSDriver);
					}
					devicesPageIOS.tapOnCancelBtn();
					devicesPageIOS.tapOnBackBtn();
					if (!devicesPageIOS.verifyGatewayAvailable(fromGatewayName)) {
						extentTestIOS.log(Status.INFO, "Gateway "+fromGatewayName+" not appeared after successful pairing" );
					} else {
						extentTestIOS.log(Status.INFO, "Gateway "+fromGatewayName+" appeared after successful pairing because other devices are presents" );
					}
					devicesPageIOS.tapOnCancelBtn();
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDeviceManagement();
					deviceManagementIOS.selectPlaceName(toGatewayName);
					if(deviceManagementIOS.verifyEndPointDevice(GDOName)) {
						extentTestIOS.log(Status.PASS, "Device "+GDOName + "successfully transferred" );	 
					} else {
						extentTestIOS.log(Status.FAIL, "Device "+GDOName + " not transferred" );	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					devicesPageIOS.tapOnBackBtn();
					if(deviceManagementIOS.verifyHubAvailable(fromGatewayName)) {
						deviceManagementIOS.selectPlaceName(fromGatewayName);
						// Verifying whether the remaining devices are available once the expected device is transfered	
						if(deviceManagementIOS.verifyDeviceAvailableOnHub()) {
							extentTestIOS.log(Status.PASS, "Remaining devices are present in the hub" +fromGatewayName);
							devicesPageIOS.tapOnBackBtn();
						} else {
							extentTestIOS.log(Status.FAIL, "Remaining devices are not present in the hub" +fromGatewayName);
							utilityIOS.captureScreenshot(iOSDriver);
						}
					} else {
						extentTestIOS.log(Status.PASS, "Hub got removed from the account after device was transfered");
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices(); 
						devicesPageIOS.addGateWay(fromGatewaySerialNumber,fromGatewayName);
					}
					//Transferring back the GDO device to Ethernet Hub
					extentTestIOS.log(Status.INFO, "Start reverting device to the previous gateway" );
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDevices();
					devicesPageIOS.addNewDevice();
					devicesPageIOS.selectGateway(fromGatewayName);
					devicesPageIOS.clickDeviceTransferButton();
					if(devicesPageIOS.verfiyDeviceAndClick(GDOName)) {
						extentTestIOS.log(Status.INFO, "Expected device are available in Hub-"+toGatewayName);
						devicesPageIOS.clickNextButton();
						webService.GatewayStartLearningService(fromGatewaySerialNumber);
						webService.LearnGDOToGatewayService(fromGatewaySerialNumber,GDOSerialNumber);

						devicesPageIOS.tapSubmitButtonForTransferDevice();
						if (	devicesPageIOS.validateAlert())
						{
							if (devicesPageIOS.getAlertMessage().contains("Pairing Complete"))
							{
								extentTestIOS.log(Status.INFO, "Popup shown for transfer pairing complete" );
							}
						}
						devicesPageIOS.tapOnCancelBtn()
						.tapOnBackBtn()
						.tapOnCancelBtn();
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Expected device are not available in Hub-"+fromGatewayName);
					utilityIOS.captureScreenshot(iOSDriver);
				}

				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "User has permission to transfer devices as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  User has permission has to transfer device, but it is not allowed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}	 else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Transfer device as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Transfer device");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description	 :	This IOS test method adds virtual GDO to Wi-Fi Hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA364_ValidateAddingGDOInWifiHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String gdoSerialNumber;
		String gdoName;

		gatewaySerialNumber = testData.get(0).split(",")[0];
		gdoSerialNumber = testData.get(0).split(",")[1];
		gdoName = testData.get(0).split(",")[2];
		
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			if (devicesPageIOS.verifyAddDeviceButton()) {
				devicesPageIOS.addNewDevice();
				devicesPageIOS.selectGateway(gatewaySerialNumber)
				.clickGDOButton()
				.tapNoButton()
				.clickNextButton();
				webService.GatewayStartLearningService(gatewaySerialNumber);
				iOSDriver.getCapabilities();
				webService.LearnGDOToGatewayService(gatewaySerialNumber,gdoSerialNumber);
				devicesPageIOS.enterDeviceName(gdoName);
				devicesPageIOS.clickNextButton();
				if(devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
					extentTestIOS.log(Status.PASS, "GDO added successfully in wi-fi Hub");	
				} else {
					extentTestIOS.log(Status.FAIL, "Unable to Add GDO in Wi-Fi hub ");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add GDO");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description This is an iOS test method that check whether Schedule could be created for VGDO added in Wifi HUb. 
	 * The method verifies that a popup is shown with Error code 707 on trying to create the schedule for VGDO in Wi-Fi hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA360_CheckNoScheduleVgdoInWifiHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String errorMessage = "For your security, schedules to close a garage have been disabled until further notice. We apologize for the inconvenience. (707)";
		String scheduleName;
		String deviceName;
		String deviceState1;
		boolean enableNotification = true;

		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				schedulePageIOS.tapAddScheduleBtn()
				.enterScheduleName(scheduleName)
				.selectDeviceStateForSchedule(deviceName,deviceState1)	
				.setTime()
				.togglePushNotification(enableNotification);
				if (schedulePageIOS.saveAndVerifyError().contains(errorMessage)) {
					extentTestIOS.log(Status.PASS, "Error message as expected with error code 707");
				} else  {
					extentTestIOS.log(Status.FAIL, "Error message not shown as expected");
				}
		
				if (schedulePageIOS.validateNoScheduleText() || !schedulePageIOS.verifySchedulePresent(scheduleName)) {
					extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " not created as expected");
				} else {
					extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + " created, but not expected");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  Description This is an iOS test method that check whether Schedule with all days but today could be created for VGDO added in Wifi HUb. 
	 * The method verifies that a popup is shown with Error code 707 on trying to create the schedule for VGDO in Wi-Fi hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA360_CheckNoSpecificDaysScheduleVgdoInWifiHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {
		String errorMessage = "For your security, schedules to close a garage have been disabled until further notice. We apologize for the inconvenience. (707)";
		String scheduleName;
		String deviceName;
		String deviceState1;
		boolean enableNotification = true;
				
		scheduleName = testData.get(0).split(",")[0];
		deviceName = testData.get(0).split(",")[1];
		deviceState1 = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Schedules")) {
				menuPageIOS.tappingSchedules();
				schedulePageIOS.tapAddScheduleBtn()
				.enterScheduleName(scheduleName)
				.selectDeviceStateForSchedule(deviceName,deviceState1)	
				.setTime()
				.deselectCurrentDay()
				.togglePushNotification(enableNotification);
				if (schedulePageIOS.saveAndVerifyError().contains(errorMessage)) {
					extentTestIOS.log(Status.PASS, "Error message as expected with error code 707");
				} else  {
					extentTestIOS.log(Status.FAIL, "Error message not shown as expected");
				}
				if (schedulePageIOS.validateNoScheduleText() || !schedulePageIOS.verifySchedulePresent(scheduleName)) {
					extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " not created as expected");
				} else {
					extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + " created, but not expected");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This methods adds virtual Wi-FI GDO
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA383_ValidateAddingWGDO(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String wgdoSerialNumber;
		String wgdoName;
		String wgdoDeviceName;
		
		wgdoSerialNumber = testData.get(0).split(",")[0];
		wgdoName = testData.get(0).split(",")[1];
		wgdoDeviceName = testData.get(0).split(",")[2];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.verifyAddDeviceButton()) {
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				if(!devicesPageIOS.verifyGatewayAvailable(wgdoName)){
					webService.SendGatewayOnlineService(wgdoSerialNumber);
					devicesPageIOS.addWGDOAndGDO(wgdoName,wgdoDeviceName);
					devicesPageIOS.addNewDevice();
					if(devicesPageIOS.verifyGatewayAvailable(wgdoName)){
						extentTestIOS.log(Status.PASS,wgdoName+ " Added Successfully");	
					} else {
						extentTestIOS.log(Status.FAIL, "Unable to Add WGDO");	
						utilityIOS.captureScreenshot(iOSDriver);
						devicesPageIOS.tapOnCancelBtn();
					}
				} else {
					extentTestIOS.log(Status.INFO, "WGDO is already present");
					devicesPageIOS.tapOnCancelBtn();
				}
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
					utilityIOS.captureScreenshot(iOSDriver);
				} 
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This iOS test method verifies the error message and error code shown when trying to delete the GDO attached to WGDO
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws Exception
	 */
	public void MTA403_ValidateWGDOAsParentChild(ArrayList<String> testData, String deviceTest,String scenarioName) throws Exception {

		String wgdoName;
		String wgdoDeviceName;
		wgdoName = testData.get(0).split(",")[0];
		wgdoDeviceName = testData.get(0).split(",")[1];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver,extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		if (expectedRolePresent) { 
			if (menuPageIOS.verifyMenuLink("Device Management")){
				menuPageIOS.tappingDeviceManagement();
				devicesPageIOS.selectGateway(wgdoName);
				deviceManagementIOS.deleteGDODeviceForError(wgdoDeviceName);

				if(deviceManagementIOS.verifyAlertForGDODelete()){
					extentTestIOS.log(Status.PASS, "Popup 'This GDO is linked to its hub and cannot be removed.' is displayed successfully");
				} else {
					extentTestIOS.log(Status.FAIL, "No Error popup was displayed while deleting the GDO");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				deviceManagementIOS.tapBackBtn();
				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to device management link as expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user does not have access to device managemnet link as expected");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * * Description This iOS test method validates the the help text that appears on clicking the link shown on selecting a gateway
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA215_ValidateHelpTextOnDevicePage(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException  {

		String gatewayName;
		String helptext1; 
		String helptext2; 

		gatewayName = testData.get(0).split(",")[0];
		helptext1 = testData.get(0).split(",")[1]; // 'How to Use the MyQ App'	 --> Text to verify	
		helptext2 = testData.get(0).split(",")[2]; // 'Closed Manage Devices' --> Text to verify	

		extentTestIOS = createTest(deviceTest, scenarioName); 
		GlobalVariables.currentTestIOS  =  Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		menuPageIOS.tappingHamburgerMenu()
		.tappingDevices();
		if(devicesPageIOS.verifyAddDeviceButton()) {
			devicesPageIOS.addNewDevice()
			.selectGateway(gatewayName)
			.tapHelplink();
			if (devicesPageIOS.VerifyHelpText(helptext1,helptext2)) { 
				extentTestIOS.log(Status.PASS, "MTA-215 Validate Help text on device screen : How to use MyQ APP is displayed successfully on the device screen");
			} else {
				extentTestIOS.log(Status.FAIL, "MTA-215 Validate Help text on device screen : How to use MyQ APP is not displayed on the device screen");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			devicesPageIOS.tapOnBackBtn()
			.tapOnBackBtn()
			.tapOnBackBtn();
		} else {
			if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
			} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
				extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
				utilityIOS.captureScreenshot(iOSDriver);
			} 
		}
	}

	/**
	 * * Description	: This is an IOS test method to verify the warning icon, Offline text and error pop-up when user tap on light after triggering the API(V2) for Gateway Offline [Edge case]
	 * 				  This iOS method also verifies that the schedule is not triggered when the parent gateway is sent offline.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA418_ValidateErrorOnGatewayOffline(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String deviceName;
		String gatewaySerialNumber;
		String scheduleName;
		String deviceState1;
		boolean enableNotification = true;
		boolean allDays = true;
	
		String errText = "Your device is offline. Please check the power and your network connections. (309)";
		
		deviceName = testData.get(0).split(",")[0];
		gatewaySerialNumber = testData.get(0).split(",")[1];
		scheduleName = testData.get(0).split(",")[2];
		deviceState1 = testData.get(0).split(",")[3];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(deviceName, deviceState1);

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			webService.SendGatewayOfflineService(gatewaySerialNumber); // Send GW offline
			if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
				if(devicesPageIOS.verifyWarningImageOnDevice(deviceName)){ // Verify Error state for Gateway
					extentTestIOS.log(Status.PASS, "The Error symbol is displayed on the device - " +deviceName);
				} else {
					extentTestIOS.log(Status.FAIL, "The Error symbol is not displayed on the device - " + deviceName);
					utilityIOS.captureScreenshot(iOSDriver);
				}

				if(devicesPageIOS.verifyErrorOnDevice(deviceName, "Offline", errText)){ //Verify the yellow bar and popup 
					extentTestIOS.log(Status.PASS, "'" + errText + "' error is displayed while tapping the device.");
				} else {
					extentTestIOS.log(Status.FAIL, "Error popup not present while tapping on device when gateway is offline "+deviceName);
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should not get triggered when the parent gateway is offline
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						
						if (!schedulePageIOS.validateScheduleTrigger()) {
							extentTestIOS.log(Status.PASS,  "Since the parent Gateway "+ gatewaySerialNumber +" is offline "+scheduleName +" did not triggered as expected");
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when the parent Gateway"+ gatewaySerialNumber+"is offline.");
							utilityIOS.captureScreenshot(iOSDriver);
							devicesPageIOS.getAlertMessage();
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			webService.SendGatewayOnlineService(gatewaySerialNumber); //Send Gateway Online
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, "Expected Devices section is available to " + activeUserRole + " user");
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  * Description	:	This iOS test method verifies that entire history is deleted  from history section
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 */
	public void MTA434_ValidateDeletingHistory(ArrayList<String> testData, String deviceTest,String scenarioName) {

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestAndroid = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);

		if (expectedRolePresent) {
			if (activeUserRole.equalsIgnoreCase("Guest")) {
				menuPageIOS.tappingHamburgerMenu();
				if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
					extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
				} else {
					extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
					utilityIOS.captureScreenshot(iOSDriver);
				}
				menuPageIOS.tappingDevices();
			} else {	// Verify history for the remaining user types
				if (historyPageIOS.deleteHistory()) {
					extentTestIOS.log(Status.PASS, "Deleted history successfully");	
				} else {
					extentTestIOS.log(Status.FAIL, "History was not deleted. There are still events logged.");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description	:	This IOS test method verifies the error message/code when trying to enter invalid serial number while adding Gateway
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA416_ValidateGWSerialNumberError301(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String errorMessage="The serial number was invalid. Please try again. (301)";
		String GWSerialNumber = testData.get(0);
		
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();
				devicesPageIOS.enterGWDetails(GWSerialNumber); // enter GW serial number
				if (devicesPageIOS.verifyErrorMessage(errorMessage)) {
					extentTestIOS.log(Status.PASS, "'" + errorMessage + "' error code displayed successfully");
				} else {
					extentTestIOS.log(Status.FAIL, "'" + errorMessage + "' error code NOT displayed.");
				}
				devicesPageIOS.tapOnBackBtn();
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
			
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	:	This IOS test method verifies the error message/code when trying to enter serial number of the gateway which is added in some other user account
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA420_ValidateGWSerialNumberError310(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String errorMessage="That device is on another user's account. Unable to add (310)";
		String GWSerialNumber = testData.get(0); //1000,006,C97
		
		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		
		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();
				devicesPageIOS.enterGWDetails(GWSerialNumber); // enter GW serial number
				if (devicesPageIOS.verifyErrorMessage(errorMessage)) {
					extentTestIOS.log(Status.PASS, "'" + errorMessage + "' error code displayed successfully");
				} else {
					extentTestIOS.log(Status.FAIL, "'" + errorMessage + "' error code NOT displayed.");
				}
				devicesPageIOS.tapOnBackBtn();
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}
			
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * * Description	:	This IOS test method verifies the error message/code when brand of gateway is not recognized
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws InterruptedException
	 */
	public void MTA240_ValidateGWSerialNumberError302(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		String errorMessage="That brand of gateway or hub can't be registered to your account. Check the serial number and try again (302)";
		String GWSerialNumber = testData.get(0);

		extentTestIOS = createTest(deviceTest,scenarioName); 
		GlobalVariables.currentTestIOS = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			if (menuPageIOS.verifyMenuLink("Device Management")) {
				menuPageIOS.tappingDeviceManagement();
				devicesPageIOS.enterGWDetails(GWSerialNumber); // enter GW serial number
				if (devicesPageIOS.verifyErrorMessage(errorMessage)) {
					extentTestIOS.log(Status.PASS, "'" + errorMessage + "' error code displayed successfully");
				} else {
					extentTestIOS.log(Status.FAIL, "'" + errorMessage + "' error code NOT displayed.");
				}
				devicesPageIOS.tapOnBackBtn();
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  device management");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  device management");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
				menuPageIOS.tappingDevices();
			}

		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  * Description :This is an IOS test method to bring Gate to Close state, trigger API(V2) for open obstruction,  verify 'STOPPED' state and 
	 *               tap on device and verify the error message, creates and validate the schedule, then exit from Open obstruction.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA334_ValidateGateOpenObstructionError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gateSerialNumber;
		String gateName;
		String scheduleName = "ScheduleGateOpenObstruction";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		boolean scheduleTriggered = false;
		String eventName;
		String eventTime = null;

		gateSerialNumber =testData.get(0).split(",")[0];
		gateName =testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(gateName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gateName)) {
				if (devicesPageIOS.getDeviceStatus(gateName).equalsIgnoreCase("OPEN")) {
					devicesPageIOS.tappingDevice(gateName);
				}
				//API call to Enter Open obstruction
				webService.enterOpenObstructionGate(gateSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				//validating the 'STOPPED' state and 'No Response' text for open obstruction
				if (devicesPageIOS.getDeviceStatus(gateName).equalsIgnoreCase("STOPPED")) {
					extentTestIOS.log(Status.PASS, "Gate State is stopped");
					devicesPageIOS.actuatingDevice(gateName);
					if(devicesPageIOS.verifyDeviceNoResponse(gateName)) {
						extentTestIOS.log(Status.PASS, "Gate not responding error is shown");
					} else {
						extentTestIOS.log(Status.FAIL, "Gate not responding error is not shown");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "The status of the Gate is not stopped");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should get triggered, device state should change and update history when open obstruction error is shown.
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						menuPageIOS.tappingHamburgerMenu().tappingDevices();	

						if (schedulePageIOS.validateScheduleTrigger()) {
							utilityIOS.captureScreenshot(iOSDriver);
							if (devicesPageIOS.getAlertMessage().contains(scheduleName)) {
								extentTestIOS.log(Status.PASS,  scheduleName +" Triggered successfully");
								scheduleTriggered = true;
							}
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " did not triggered");
						}
						eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
						Set set = deviceStatePairList.entrySet();
						Iterator iterator = set.iterator();
						while(iterator.hasNext()) {
							Map.Entry deviceStatePair = (Map.Entry)iterator.next();
							String deviceName = (String) deviceStatePair.getKey();
							String deviceState = (String) deviceStatePair.getValue();
							if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
								if (devicesPageIOS.validateDeviceStatus(deviceName,deviceState)) {
									extentTestIOS.log(Status.PASS, "State of " +deviceName + " changed successfully"); 
									scheduleTriggered = true;
								} else {
									extentTestIOS.log(Status.FAIL, "State of " +deviceName + "did not changed to " +deviceState);
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {
								extentTestIOS.log(Status.FAIL, " Expected device is not visible");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						}
						if (scheduleTriggered) { // Check for history only if Schedule is triggered or device state is changed
							eventName = scheduleName + " was triggered";
							menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
							if (activeUserRole.equalsIgnoreCase("Guest")) {
								if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
									extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
									menuPageIOS.tappingDevices();// To close the menu options
								} else {
									extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {	// Verify history for the remaining user types
								menuPageIOS.tappingHistory();
								if (historyPageIOS.verifyHistory(eventName,eventTime)) {
									extentTestIOS.log(Status.PASS, "History event name: '" + eventName +"' is logged successfully");	
								} else {
									extentTestIOS.log(Status.FAIL, "History event name: '" + eventName +"' is not logged ");	
									utilityIOS.captureScreenshot(iOSDriver);
								}
								extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
							}
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to Exit Open Obstruction 
			webService.exitOpenObstructionGate(gateSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gateName);
			if(!devicesPageIOS.verifyDeviceNoResponse(gateName)) {
				extentTestIOS.log(Status.PASS, "Gate is back in healthy state");
			} else {
				extentTestIOS.log(Status.FAIL, "Gate is still in error state");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 *  Description : This is an IOS test method to bring GDO to Close state, trigger API(V2) for open obstruction, verify 'STOPPED' state and 
	 *               tap on device and verify the error message,creates and validate the schedule, then exit from Open obstruction.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA335_ValidateGDOOpenObstructionError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String gdoSerialNumber;
		String gdoName;
		String scheduleName = "ScheduleGDOOpenObstruction";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		boolean scheduleTriggered = false;
		String eventName;
		String eventTime = null;

		gdoSerialNumber =testData.get(0).split(",")[0];
		gdoName =testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(gdoName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
				if (devicesPageIOS.getDeviceStatus(gdoName).equalsIgnoreCase("OPEN")) {
					devicesPageIOS.tappingDevice(gdoName);
				}
				//API call for entering the Open obstruction
				webService.enterOpenObstructionGDO(gdoSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				//validating the 'STOPPED' state and 'No Response' text for open obstruction
				if (devicesPageIOS.getDeviceStatus(gdoName).equalsIgnoreCase("STOPPED")) {
					extentTestIOS.log(Status.PASS, "Garage Door Opener State is stopped");
					devicesPageIOS.actuatingDevice(gdoName);
					if(devicesPageIOS.verifyDeviceNoResponse(gdoName)) {
						extentTestIOS.log(Status.PASS, "Garage Door Opener not responding error is shown");
					} else {
						extentTestIOS.log(Status.FAIL, "Garage Door Opener not responding error is not shown");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "The status of the Garage Door Opener is not stopped");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should get triggered, device state should change and update history when open obstruction error is shown.
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						menuPageIOS.tappingHamburgerMenu().tappingDevices();	

						if (schedulePageIOS.validateScheduleTrigger()) {
							utilityIOS.captureScreenshot(iOSDriver);
							if (devicesPageIOS.getAlertMessage().contains(scheduleName)) {
								extentTestIOS.log(Status.PASS,  scheduleName +" Triggered successfully");
								scheduleTriggered = true;
							}
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " did not triggered");
						}
						eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
						Set set = deviceStatePairList.entrySet();
						Iterator iterator = set.iterator();
						while(iterator.hasNext()) {
							Map.Entry deviceStatePair = (Map.Entry)iterator.next();
							String deviceName = (String) deviceStatePair.getKey();
							String deviceState = (String) deviceStatePair.getValue();
							if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
								if (devicesPageIOS.validateDeviceStatus(deviceName,deviceState)) {
									extentTestIOS.log(Status.PASS, "State of " +deviceName + " changed successfully"); 
									scheduleTriggered = true;
								} else {
									extentTestIOS.log(Status.FAIL, "State of " +deviceName + "did not changed to " +deviceState);
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {
								extentTestIOS.log(Status.FAIL, " Expected device is not visible");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						}
						if (scheduleTriggered) { // Check for history only if Schedule is triggered or device state is changed
							eventName = scheduleName + " was triggered";
							menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
							if (activeUserRole.equalsIgnoreCase("Guest")) {
								if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
									extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
									menuPageIOS.tappingDevices();// To close the menu options
								} else {
									extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {	// Verify history for the remaining user types
								menuPageIOS.tappingHistory();
								if (historyPageIOS.verifyHistory(eventName,eventTime)) {
									extentTestIOS.log(Status.PASS, "History event name: '" + eventName +"' is logged successfully");	
								} else {
									extentTestIOS.log(Status.FAIL, "History event name: '" + eventName +"' is not logged ");	
									utilityIOS.captureScreenshot(iOSDriver);
								}
								extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
							}
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call for exit Open obstruction
			webService.exitOpenObstructionGDO(gdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gdoName);
			if(!devicesPageIOS.verifyDeviceNoResponse(gdoName)) {
				extentTestIOS.log(Status.PASS, "GDO is back in healthy state");
			} else {
				extentTestIOS.log(Status.FAIL, "GDO is still in error state");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description : This is an IOS test method to bring CDO to Close state, trigger API(V2) for open obstruction,  verify 'STOPPED' state and 
	 *               tap on device and verify the error message, creates and validate the schedule , then exit from Open obstruction.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA336_ValidateCDOOpenObstructionError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String cdoSerialNumber;
		String cdoName;
		String scheduleName = "ScheduleCDOOpenObstruction";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		boolean scheduleTriggered = false;
		String eventName;
		String eventTime = null;
		
		cdoSerialNumber =testData.get(0).split(",")[0];
		cdoName =testData.get(0).split(",")[1];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS =  new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(cdoName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(cdoName)) {
				if (devicesPageIOS.getDeviceStatus(cdoName).equalsIgnoreCase("OPEN")) {
					devicesPageIOS.tappingDevice(cdoName);
				}
				//API call for entering the Open obstruction
				webService.enterOpenObstructionCDO(cdoSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
//				//validating the 'STOPPED' state and 'No Response' text for open obstruction
				if (devicesPageIOS.getDeviceStatus(cdoName).equalsIgnoreCase("STOPPED")) {
					extentTestIOS.log(Status.PASS, "Commercial Door Opener State is stopped");
					devicesPageIOS.actuatingDevice(cdoName);
					if(devicesPageIOS.verifyDeviceNoResponse(cdoName)) {
						extentTestIOS.log(Status.PASS, "Commercial Door Opener not responding error is shown");
					} else {
						extentTestIOS.log(Status.FAIL, "Commercial Door Opener not responding error is not shown");
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "The status of the Commercial Door Opener is not stopped");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should get triggered, device state should change and update history when open obstruction error is shown.
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						menuPageIOS.tappingHamburgerMenu().tappingDevices();	
						if (schedulePageIOS.validateScheduleTrigger()) {
							utilityIOS.captureScreenshot(iOSDriver);
							if (devicesPageIOS.getAlertMessage().contains(scheduleName)) {
								extentTestIOS.log(Status.PASS,  scheduleName +" Triggered successfully");
								scheduleTriggered = true;
							}
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " did not triggered");
						}
						eventTime = String.valueOf(utilityIOS.getCurrentTimeInFormat());
						Set set = deviceStatePairList.entrySet();
						Iterator iterator = set.iterator();
						while(iterator.hasNext()) {
							Map.Entry deviceStatePair = (Map.Entry)iterator.next();
							String deviceName = (String) deviceStatePair.getKey();
							String deviceState = (String) deviceStatePair.getValue();
							if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
								if (devicesPageIOS.validateDeviceStatus(deviceName,deviceState)) {
									extentTestIOS.log(Status.PASS, "State of " +deviceName + " changed successfully"); 
									scheduleTriggered = true;
								} else {
									extentTestIOS.log(Status.FAIL, "State of " +deviceName + "did not changed to " +deviceState);
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {
								extentTestIOS.log(Status.FAIL, " Expected device is not visible");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						}
						if (scheduleTriggered) { // Check for history only if Schedule is triggered or device state is changed
							eventName = scheduleName + " was triggered";
							menuPageIOS.tappingHamburgerMenu(); // Navigate to history page and check for event log
							if (activeUserRole.equalsIgnoreCase("Guest")) {
								if ((menuPageIOS.verifyMenuLink("History")) == false ) { // History should not be available for Guest
									extentTestIOS.log(Status.PASS, "History link is not present on the menu for user type 'Guest'");	
									menuPageIOS.tappingDevices();// To close the menu options
								} else {
									extentTestIOS.log(Status.FAIL, "History link is present on the menu for user type 'Guest'");	
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {	// Verify history for the remaining user types
								menuPageIOS.tappingHistory();
								if (historyPageIOS.verifyHistory(eventName,eventTime)) {
									extentTestIOS.log(Status.PASS, "History event name: '" + eventName +"' is logged successfully");	
								} else {
									extentTestIOS.log(Status.FAIL, "History event name: '" + eventName +"' is not logged ");	
									utilityIOS.captureScreenshot(iOSDriver);
								}
								extentTestIOS.log(Status.PASS, "History section is available to " + activeUserRole + " user");
							}
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call for exit Open obstruction
			webService.exitOpenObstructionCDO(cdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(cdoName);
			if(!devicesPageIOS.verifyDeviceNoResponse(cdoName)) {
				extentTestIOS.log(Status.PASS, "CDO is back in healthy state");
			} else {
				extentTestIOS.log(Status.FAIL, "CDO is still in error state");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This is an IOS test method to bring GDO to Open state, trigger API(V2) for close obstruction, verify yellow bar and text present on it, 
	 *               exclamation mark on device, tap on device and verify the error message,creates schedule and verifies that the schedule is not triggered when the close obstruction error is shown, then
	 *               exit from Close obstruction.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA444_ValidateGDOCloseObstructionError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String gdoSerialNumber;
		String gdoName;
		String scheduleName = "ScheduleGDOCloseObstruction";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		String errorMessage = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		
		gdoSerialNumber =testData.get(0).split(",")[0];
		gdoName =testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(gdoName, deviceState1);
		
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if (devicesPageIOS.getDeviceStatus(gdoName).equalsIgnoreCase("CLOSED")) {
				devicesPageIOS.tappingDevice(gdoName);
			}
			//API call for entering the Close obstruction
			webService.enterCloseObstructionGDO(gdoSerialNumber);
			//Just to refresh the view
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
				//Validating the "!" warning icon on the device
				if(devicesPageIOS.verifyWarningImageOnDevice(gdoName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating the 'Close Error' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDevice(gdoName, "Close Error", errorMessage)) {
					extentTestIOS.log(Status.PASS, "Close obstruction error popup and the Close error is shown for GDO ");
				} else {
					extentTestIOS.log(Status.FAIL, "Close obstruction error popup and the Close error is  not shown for GDO");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should get triggered but the device state should remanin unchanged when the close obstruction error is shown for GDO 
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						
						if (!schedulePageIOS.validateScheduleTrigger()) {
							extentTestIOS.log(Status.PASS,  "Since the "+ gdoName+" has Close obstruction error "+scheduleName +" did not triggered as expected");
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when "+ gdoName+ " Close obstruction error.");
							utilityIOS.captureScreenshot(iOSDriver);
							devicesPageIOS.getAlertMessage();
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to exit Close obstruction
			webService.exitCloseObstructionGDO(gdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gdoName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(gdoName)) {
				extentTestIOS.log(Status.PASS, "GDO is back in healthy state after Exit Close obstruction API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "GDO is still in error state after Exit Close obstruction API is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
		
	/**
	 * Description : This is an IOS test method to bring the Gate to the Open state, trigger the API(V2)for close obstruction, 
	 *  			 tap on device to verify the error message appearing after 2 minutes, creates schedule and verifies that the schedule is not triggered when the close obstruction error is shown
	 *  	         and then exit from close obstruction [Edge Case]
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA460_ValidateGateCloseObstructionError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String gateSerialNumber;
		String gateName;
		boolean scheduleTriggered = false;
		String scheduleName = "ScheduleGateCloseObstruction";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;

		gateSerialNumber =testData.get(0).split(",")[0];
		gateName =testData.get(0).split(",")[1];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(gateName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gateName)) {
				if (devicesPageIOS.getDeviceStatus(gateName).equalsIgnoreCase("CLOSED")) {
					devicesPageIOS.actuatingDevice(gateName);
				}
				//API call for entering the Close obstruction
				webService.enterCloseObstructionGate(gateSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				if(devicesPageIOS.verifyDeviceNoResponse(gateName)) {
					extentTestIOS.log(Status.PASS, "Gate not responding error is shown");
				} else {
					extentTestIOS.log(Status.FAIL, "Gate not responding error is not shown");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should not get triggered when the close obstruction error is shown for Gate 
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						menuPageIOS.tappingHamburgerMenu()
						.tappingDevices();	
						if (schedulePageIOS.validateScheduleTrigger()) {
							utilityIOS.captureScreenshot(iOSDriver);
							if (devicesPageIOS.getAlertMessage().contains(scheduleName)) {
								extentTestIOS.log(Status.PASS,  scheduleName +" Triggered successfully");
								scheduleTriggered = true;
							}
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " did not triggered");
						}
						Set set = deviceStatePairList.entrySet();
						Iterator iterator = set.iterator();
						while(iterator.hasNext()) {
							Map.Entry deviceStatePair = (Map.Entry)iterator.next();
							String deviceName = (String) deviceStatePair.getKey();
							String deviceState = (String) deviceStatePair.getValue();
							if(devicesPageIOS.verifyDeviceNamePresent(deviceName)) {
								if (devicesPageIOS.getDeviceStatus(deviceName).equalsIgnoreCase("Open")) {
									extentTestIOS.log(Status.PASS, "State of " +deviceName + " did not chnaged"); 
									scheduleTriggered = true;
								} else {
									extentTestIOS.log(Status.FAIL, "State of " +deviceName + " changed to " +deviceState);
									utilityIOS.captureScreenshot(iOSDriver);
								}
							} else {
								extentTestIOS.log(Status.FAIL, " Expected device is not visible");
								utilityIOS.captureScreenshot(iOSDriver);
							}
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			webService.exitCloseObstructionGate(gateSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gateName);
			if(!devicesPageIOS.verifyDeviceNoResponse(gateName)) {
				extentTestIOS.log(Status.PASS, "Gate is back in healthy state after Exit Close obstruction API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "Gate is still in error state after Exit Close obstruction API is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This is an iOS test method to bring CDO to Open state, trigger API(V2) for close obstruction, verify yellow bar and text present on it, 
	 * 				 exclamation mark on device, tap on device and verify the error message, creates schedule and verifies that the schedule is not triggered when the close obstruction error is shown
	 * 				 and then exit from Close obstruction
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA450_ValidateCDOCloseObstructionError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String cdoSerialNumber;
		String cdoName;
		String scheduleName = "ScheduleCDOCloseObstruction";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		String errorMessage = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";

		cdoSerialNumber =testData.get(0).split(",")[0];
		cdoName =testData.get(0).split(",")[1];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(cdoName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(cdoName)) {
				if (devicesPageIOS.getDeviceStatus(cdoName).equalsIgnoreCase("CLOSED")) {
					devicesPageIOS.tappingDevice(cdoName);
				}
				//API call for entering the Close obstruction
				webService.enterCloseObstructionCDO(cdoSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				//Validating the "!" warning icon on the device
				if(devicesPageIOS.verifyWarningImageOnDevice(cdoName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating the 'Close Error' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDevice(cdoName, "Close Error", errorMessage)) {
					extentTestIOS.log(Status.PASS, "Close obstruction error popup and the Close error is shown for CDO ");
				} else {
					extentTestIOS.log(Status.FAIL, "Close obstruction error popup and the Close error is  not shown for CDO");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should not get triggered when the close obstruction error is shown for Gate 
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						
						if (!schedulePageIOS.validateScheduleTrigger()) {
							extentTestIOS.log(Status.PASS,  "Since the "+ cdoName+" has Close obstruction error "+scheduleName +" did not triggered as expected");
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when "+ cdoName+ " Close obstruction error.");
							utilityIOS.captureScreenshot(iOSDriver);
							devicesPageIOS.getAlertMessage();
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to exit close obstruction
			webService.exitCloseObstructionCDO(cdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(cdoName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(cdoName)) {
				extentTestIOS.log(Status.PASS, "CDO is back in healthy state after Exit Close obstruction API is triggered ");
			} else {
				extentTestIOS.log(Status.FAIL, "CDO is still in error state after Exit Close obstruction API is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  Description : This is an IOS test method to verify Warning symbol, yellow bar, 'Close Error' and error popup for GDO Photo Eye Block edge case.
	 *               This iOS method also verifies that the schedule is not triggered when the photo eye block error is shown.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA446_ValidateGDOPhotoEyeBlockError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gdoSerialNumber;
		String gdoName;
		String scheduleName = "ScheduleGDOPhotoEye";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		String errorMessage = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		
		gdoSerialNumber =testData.get(0).split(",")[0];
		gdoName =testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(gdoName, deviceState1);

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
				if (devicesPageIOS.getDeviceStatus(gdoName).equalsIgnoreCase("CLOSED")) {
					devicesPageIOS.actuatingDevice(gdoName);
				}
				//API call to Block photo eye
				webService.blockPhotoEyeGDO(gdoSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				//Validating the "!" warning icon on the device
				if(devicesPageIOS.verifyWarningImageOnDevice(gdoName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating the 'Close Error' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDevice(gdoName, "Close Error", errorMessage)) {
					extentTestIOS.log(Status.PASS, "Photo Eye block error popup and the Close error is shown ");
				} else {
					extentTestIOS.log(Status.FAIL, "Photo Eye block error popup and the Close error is  not shown");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should not get triggered when the photo eye block error is shown for GDO
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						
						if (!schedulePageIOS.validateScheduleTrigger()) {
							extentTestIOS.log(Status.PASS,  "Since the "+ gdoName+" has photo eye block error "+scheduleName +" did not triggered as expected");
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when "+ gdoName+ " has photo eye block error.");
							utilityIOS.captureScreenshot(iOSDriver);
							devicesPageIOS.getAlertMessage();
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to unblock photo eye
			webService.unblockPhotoEyeGDO(gdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gdoName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(gdoName)) {
				extentTestIOS.log(Status.PASS, "GDO is back in healthy state after Unblock Photoeye API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "GDO is back in still is error state after Unblock Photoeye API is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  * Description : This is an IOS test method to verify Warning symbol, yellow bar, 'Close Error' and error popup for CDO Photo Eye Block edge case.
	 * 				    This iOS method also verifies that the schedule is not triggered when the photo eye block error is shown.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA452_ValidateCDOPhotoEyeBlockError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String cdoSerialNumber;
		String cdoName;
		String scheduleName = "ScheduleCDOPhotoEye";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		String errorMessage = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";
		cdoSerialNumber =testData.get(0).split(",")[0];
		cdoName =testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(cdoName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(cdoName)) {
				if (devicesPageIOS.getDeviceStatus(cdoName).equalsIgnoreCase("CLOSED")) {
					devicesPageIOS.actuatingDevice(cdoName);
				}
				//API call to Block photo eye
				webService.blockPhotoEyeCDO(cdoSerialNumber);
				////Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				//Validating the "!" warning icon on the device
				if(devicesPageIOS.verifyWarningImageOnDevice(cdoName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating the 'Close Error' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDevice(cdoName, "Close Error", errorMessage)) {
					extentTestIOS.log(Status.PASS, "Photo Eye block error popup and the Close error is shown ");
				} else {
					extentTestIOS.log(Status.FAIL, "Photo Eye block error popup and the Close error is  not shown");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//MTA-488 Implementation: Schedule should not get triggered when the photo eye block error is shown for CDO
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						
						if (!schedulePageIOS.validateScheduleTrigger()) {
							extentTestIOS.log(Status.PASS,  "Since the "+ cdoName+" has photo eye block error "+scheduleName +" did not triggered as expected");
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when "+ cdoName+ " has photo eye block error.");
							utilityIOS.captureScreenshot(iOSDriver);
							devicesPageIOS.getAlertMessage();
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to unblock photo eye
			webService.unblockPhotoEyeCDO(cdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(cdoName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(cdoName)) {
				extentTestIOS.log(Status.PASS, "CDO is back in healthy state after Unblock Photoeye API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "CDO is back in still is error state after Unblock Photoeye API is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This is an IOS test method to verify the error pop-up when user tap on Light after triggering the API(V2) for ACLoss [Edge case]
	 * 				This IOS method also verifies that schedule is not triggered when the device is offline.
	 * @throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA456_ValidateLightACLossError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String lampSerialNumber;
		String lampName;
		
		lampSerialNumber =testData.get(0).split(",")[0];
		lampName =testData.get(0).split(",")[1];
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(lampName)) {
				//API call to Enter ac loss
				webService.enterAcLossForLight(lampSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				if(devicesPageIOS.verifyDeviceNoResponse(lampName)){
					extentTestIOS.log(Status.PASS, "No respose error popup is shown for light AC loss ");
				} else {
					extentTestIOS.log(Status.FAIL, "No respose error popup is not shown for light AC loss ");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if(activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, "Light is not visible to Guest User");
				} else {
					extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present"); 
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
			//API call to exit ac loss 
			webService.exitAcLossForLight(lampSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(lampName);
			if(!devicesPageIOS.verifyDeviceNoResponse(lampName)){
				extentTestIOS.log(Status.PASS, "Light is back in healthy state after exit AC loss API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "Light is still in error state after exit AC loss API is triggered ");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This is an iOS test method to verify the error pop-up when user tap on GDO after triggering the API(V2) for ACLoss [Edge case]
	 * 				 This IOS method also verifies that schedule is not triggered when the device is offline.
	 * @throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA448_ValidateGDOACLossError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gdoSerialNumber;
		String gdoName;
		
		gdoSerialNumber =testData.get(0).split(",")[0];
		gdoName =testData.get(0).split(",")[1];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			//API call to enter ac loss
			webService.enterAcLossForGDO(gdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
				if(devicesPageIOS.verifyDeviceNoResponse(gdoName)){
					extentTestIOS.log(Status.PASS, "No respose error popup is shown for GDO AC loss ");
				} else {
					extentTestIOS.log(Status.FAIL, "No respose error popup is not shown for GDO AC loss ");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to Exit ac loss
			webService.exitAcLossForGDO(gdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gdoName);
			if(!devicesPageIOS.verifyDeviceNoResponse(gdoName)){
				extentTestIOS.log(Status.PASS, "GDO is back in healthy state after exit AC loss API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "GDO is still in error state after exit AC loss API is triggered ");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}

	/**
	 * Description : This is an iOS test method to verify the error pop-up when user tap on CDO after triggering the API(V2) for ACLoss [Edge case]
	 * 				 This IOS method also verifies that schedule is not triggered when the device is offline.
	 * @throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA454_ValidateCDOACLossError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String cdoSerialNumber;
		String cdoName;
		
		cdoSerialNumber =testData.get(0).split(",")[0];
		cdoName =testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			//API call to enter ac loss
			webService.enterAcLossForCDO(cdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(cdoName)) {
				if(devicesPageIOS.verifyDeviceNoResponse(cdoName)){
					extentTestIOS.log(Status.PASS, "No respose error popup is shown for CDO AC loss ");
				} else {
					extentTestIOS.log(Status.FAIL, "No respose error popup is not shown for CDO AC loss ");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to Exit ac loss
			webService.exitAcLossForCDO(cdoSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(cdoName);
			if(!devicesPageIOS.verifyDeviceNoResponse(cdoName)){
				extentTestIOS.log(Status.PASS, "CDO is back in healthy state after exit AC loss API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "CDO is still in error state after exit AC loss API is triggered ");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This is an IOS test method to verify the error pop-up when user tap on Gate after triggering the API(V2) for ACLoss [Edge case]
	 * 				 This IOS method also verifies that schedule is not triggered when the device is offline
	 * @throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA463_ValidateGateACLossError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gateSerialNumber;
		String gateName;
		
		gateSerialNumber =testData.get(0).split(",")[0];
		gateName = testData.get(0).split(",")[1];
				
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			//API call to enter ac loss
			webService.enterAcLossForGate(gateSerialNumber);
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gateName)) {
				if(devicesPageIOS.verifyDeviceNoResponse(gateName)){
					extentTestIOS.log(Status.PASS, "No respose error popup is shown for Gate AC loss ");
				} else {
					extentTestIOS.log(Status.FAIL, "No respose error popup is not shown for Gate AC loss ");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to Exit ac loss
			webService.exitAcLossForGate(gateSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gateName);
			if(!devicesPageIOS.verifyDeviceNoResponse(gateName)){
				extentTestIOS.log(Status.PASS, "Gate is back in healthy state after exit AC loss API is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "Gate is still in error state after exit AC loss API is triggered ");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This is an IOS test method to verify the warning icon, Offline text and error pop-up when user tap on light after triggering the API(V2) for Gateway ACLoss [Edge case]
	 * @throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA456_ValidateGatewayACLossError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewaySerialNumber;
		String lightName;
		String errorMessage = "Your device is offline. Please check the power and your network connections. (309)";

		gatewaySerialNumber =testData.get(0).split(",")[0];
		lightName = testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			//API Call to Send Gateway Offline
			webService.SendGatewayOfflineService(gatewaySerialNumber);
			//Just to refresh the view
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(lightName)) {
				//Validating the "!" warning icon on the device	
				if(devicesPageIOS.verifyWarningImageOnDevice(lightName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating the 'Offline' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDevice(lightName, "Offline", errorMessage)) {
					extentTestIOS.log(Status.PASS, "Gateway offline error popup and the offline error is shown ");
				} else {
					extentTestIOS.log(Status.FAIL, "Gateway offline error popup and the offline error is  not shown");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating MyQ trouble shooting text for device offline state 
				devicesPageIOS.tapOnErrorTab(lightName);
				if(devicesPageIOS.validateTroubleShootingText()) {
					extentTestIOS.log(Status.PASS, "MyQ Troubleshooting text is displayed");
				} else {
					extentTestIOS.log(Status.FAIL, "MyQ Troubleshooting text is not displayed");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if(activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, "Light is not visible to Guest User");
				} else {
					extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present"); 
					utilityIOS.captureScreenshot(iOSDriver);
				}
			}
			devicesPageIOS.tapOnBackBtn();//to return on devices page
			//API call to Send Gateway Online
			webService.SendGatewayOnlineService(gatewaySerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(lightName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(lightName)) {
				extentTestIOS.log(Status.PASS, "Gateway is back in healthy state after the Gateway online api is triggered");
			} else {
				extentTestIOS.log(Status.FAIL, "Gateway is still in error state after the Gateway online api is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description : This test method verifies the copy right text on the login screen
	 * @throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException
	 * @param testData
	 * @param deviceTest
	 */
	public void MTA529_VerifyCopyRightText(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String copyRightTextLiftmaster = "© 2018 LiftMaster®. All Rights Reserved.";
		String copyRightTextChamberlain = "© 2018 The Chamberlain Group, Inc.";
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		loginPageIOS = new LoginPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);

		if(brandName.equalsIgnoreCase("LiftMaster")){
			if(loginPageIOS.verifyCopyRightText(copyRightTextLiftmaster)) {
				extentTestIOS.log(Status.PASS, copyRightTextLiftmaster+ " text is displayed on the Login screen");
			} else {
				extentTestIOS.log(Status.FAIL, copyRightTextLiftmaster+ " text is not displayed on the Login screen");
			}
		}
		if(brandName.equalsIgnoreCase("Chamberlain")){
			if(loginPageIOS.verifyCopyRightText(copyRightTextChamberlain)) {
				extentTestIOS.log(Status.PASS, copyRightTextChamberlain+ " text is displayed on the Login screen");
			} else {
				extentTestIOS.log(Status.FAIL, copyRightTextChamberlain+ " text is not displayed on the Login screen");
			}
		}
	}
	
	/**
	 *  Description : This is an IOS test method to bring gate to Close state, trigger API(V2) for double, verify 'STOPPED' state and 
	 *               tap on device and verify the error message,creates and validate the schedule, then exit from double entrapment and verifies the healthy state.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA527_ValidateGateDoubleEntrapmentError(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {
		String gateSerialNumber;
		String gateName;
		String scheduleName = "ScheduleTest";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		boolean allDays = true;
		String errorMessage = "It looks like there have been too many failed attempts. Please operate the door locally to reset.";

		gateSerialNumber =testData.get(0).split(",")[0];
		gateName =testData.get(0).split(",")[1];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		historyPageIOS = new HistoryPageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(gateName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(gateName)) {
				if (devicesPageIOS.getDeviceStatus(gateName).equalsIgnoreCase("OPEN")) {
					devicesPageIOS.tappingDevice(gateName);
				}
				//API call for entering the Open obstruction
				webService.enterDoubleEntrapmentGate(gateSerialNumber);
				//Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();

				//validating the 'STOPPED' state and 'No Response' text for open obstruction
				if (devicesPageIOS.getDeviceStatus(gateName).equalsIgnoreCase("STOPPED")) {
					extentTestIOS.log(Status.PASS, "Gate State is stopped");
				} else {
					extentTestIOS.log(Status.FAIL, "The status of the Gate is not stopped");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				
				//Validating the "!" warning icon on the device
				if(devicesPageIOS.verifyWarningImageOnDevice(gateName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				//Validating the 'Close Error' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDevice(gateName, "Close Error", errorMessage)) {
					extentTestIOS.log(Status.PASS, "Double Entrapment error popup and the Close error is shown for Gate ");
				} else {
					extentTestIOS.log(Status.FAIL, "Double Entrapment error popup and the Close error is  not shown for Gate");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				
				//MTA-488 Implementation: Schedule should not get triggered when the close obstruction error is shown for GDO 
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.tapCancelBtn()
					.createSchedule(scheduleName, deviceStatePairList, enableNotification,allDays);
					if (schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " created");
						
						if (!schedulePageIOS.validateScheduleTrigger()) {
							extentTestIOS.log(Status.PASS,  "Since the "+ gateName+" has Double Entrapment error "+scheduleName +" did not triggered as expected");
						} else {
							extentTestIOS.log(Status.FAIL, scheduleName + " got triggered, when "+ gateName+ " Double Entrapment error.");
							utilityIOS.captureScreenshot(iOSDriver);
							devicesPageIOS.getAlertMessage();
						}
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + "  not created");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					menuPageIOS.tappingHamburgerMenu()
					.tappingSchedules();
					schedulePageIOS.deleteSchedule(scheduleName);

					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to exit Close obstruction
			webService.exitDoubleEntrapmentGate(gateSerialNumber);
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(gateName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(gateName)) {
				extentTestIOS.log(Status.PASS, "Gate is back in healthy state after Exit Double Entrapment API is triggered");
				devicesPageIOS.actuatingDevice(gateName);
			} else {
				extentTestIOS.log(Status.FAIL, "Gate is still in error state after Exit Double Entrapment API is triggered");
				utilityIOS.captureScreenshot(iOSDriver);
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  * Description : This is an IOS test method to verify Warning symbol, yellow bar, 'Low battery' and error popup for weak and critical battery state.
	 * 				    This iOS method also verifies that the schedule is not triggered when the battery is weak and critical state.
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA476_ValidateErrorForVGDOInLowBattery(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String vgdoSerialNumber;
		String vgdoName;
		String scheduleName = "ScheduleTest";
		String deviceState1 = "Close";
		boolean enableNotification = true;
		String cyclecount;
		String voltage;
		vgdoName =testData.get(0).split(",")[0];
		vgdoSerialNumber =testData.get(0).split(",")[1];
		voltage = testData.get(0).split(",")[2];
		cyclecount = testData.get(0).split(",")[3];
		String errorMessage1 = "Important MyQ Alert\nUrgent: The battery in "+vgdoName+" door sensor needs to be replaced in order to continue using "+vgdoName+" with your MyQ app.";
		String errorMessage2 = "Important MyQ Alert\nThe battery in "+vgdoName+" door sensor is running low. Replace it as soon as possible.";
		String errorMessage3 = "Your door sensor battery is low. For security, app door control and schedules will not work until the battery is replaced. (708)";
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		schedulePageIOS = new SchedulePageIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		
		HashMap<String, String> deviceStatePairList = new HashMap<String, String>();
		deviceStatePairList.put(vgdoName, deviceState1);

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingDevices();
			if(devicesPageIOS.verifyDeviceNamePresent(vgdoName)) {
				//API call to set VGDO to Low Battery state
				webService.setCycleCountVGDO(vgdoSerialNumber, cyclecount)
				.setVoltageVGDO(vgdoSerialNumber, voltage);
				
				////Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				
				//Validating the 'Low Battery' bar and popup message for the device
				if(devicesPageIOS.verifyErrorOnDeviceForLowBattery(vgdoName, "Low Battery", errorMessage1)) {
					extentTestIOS.log(Status.PASS, "Low battery error popup and error label is shown successfully, when the battery state is weak");
				} else if (devicesPageIOS.verifyErrorOnDeviceForLowBattery(vgdoName, "Low Battery", errorMessage2)) {
					extentTestIOS.log(Status.PASS, "First Low battery error popup and error label is shown successfully, when the battery state is critcal");
					if (devicesPageIOS.verifyErrorOnDeviceForLowBattery(vgdoName, "Low Battery", errorMessage2)){
						extentTestIOS.log(Status.PASS, "Second Low Battery error popup and error label is shown, when when the battery state is critcal");
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Low Battery error popup and the Low Battery error label is not shown");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				
				//Validating the "!" warning icon on the device
				if(devicesPageIOS.verifyWarningImageOnDevice(vgdoName)) {
					extentTestIOS.log(Status.PASS, "Warning symbol is shown on the device");
				} else {
					extentTestIOS.log(Status.FAIL, "Warning symbol is not shown on the device");
					utilityIOS.captureScreenshot(iOSDriver);
				}
				menuPageIOS.tappingHamburgerMenu();
				if (menuPageIOS.verifyMenuLink("Schedules")) {
					menuPageIOS.tappingSchedules();
					schedulePageIOS.tapAddScheduleBtn()
					.enterScheduleName(scheduleName)
					.selectDeviceStateForSchedule(vgdoName,deviceState1)	
					.setTime()
					.togglePushNotification(enableNotification);
					if (schedulePageIOS.saveAndVerifyError().contains(errorMessage3)) {
						extentTestIOS.log(Status.PASS, "Error message as expected with error code 708");
					} else  {
						extentTestIOS.log(Status.FAIL, "Error message not shown as expected");
					}
			
					if (schedulePageIOS.validateNoScheduleText() || !schedulePageIOS.verifySchedulePresent(scheduleName)) {
						extentTestIOS.log(Status.PASS, "schedule with name " + scheduleName + " not created as expected");
					} else {
						extentTestIOS.log(Status.FAIL, "Schedule with name " + scheduleName + " created, but not expected");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					
					if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Create Schedule as Expected");
					} else if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Create Schedule, but it is not allowed");
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Guest")) {
						extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Schedule link in menu items as expected");
					} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
						extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to  schedule link in menu items");
						utilityIOS.captureScreenshot(iOSDriver);
					} 
					// To bring back to the Home screen
					menuPageIOS.tappingDevices();
				}
				
			} else {
				extentTestIOS.log(Status.FAIL, "Expected Devices not visible or present");	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			//API call to unblock photo eye
			webService.setVoltageVGDO(vgdoSerialNumber, "270")
			.setCycleCountVGDO(vgdoSerialNumber, "0");
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			devicesPageIOS.verifyDeviceNamePresent(vgdoName);
			if(!devicesPageIOS.verifyYellowBarOnDevice(vgdoName)) {
				extentTestIOS.log(Status.PASS, "VGDO is back in normal state");
			} else {
				extentTestIOS.log(Status.FAIL, "VGDO in still is error state");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")||activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
				extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to access Devices Section");
			} 
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  * Description : This is an IOS test method to verify adding the VGDO to AHub in OEM Tx Pairing Option 1
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA533_ValidateOEMTxPairingOption1(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String vgdoSerialNumber;
		String vgdoName;
		String hubName;
		String hubSerialNumber;
		String sensor;
		String deviceStatus1;
		String deviceStatus2;
		String requiredState = "Closed";
		String monitorOnlyError = "This door is in monitor only mode.";
		
		hubName = testData.get(0).split(",")[0];
		hubSerialNumber = testData.get(0).split(",")[1];
		vgdoSerialNumber =testData.get(0).split(",")[2];
		vgdoName =testData.get(0).split(",")[3];
		sensor = testData.get(0).split(",")[4];
		
		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);
		

		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			if(devicesPageIOS.verifyAddDeviceButton()) {
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				
				webService.SendGatewayOnlineService(hubSerialNumber)
				.GatewayStartLearningService(hubSerialNumber)
				.enterLearnModeForVGDO(vgdoSerialNumber)
				.testSensorVGDO(vgdoSerialNumber, sensor);

				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				devicesPageIOS.addGateWay(hubSerialNumber,hubName);
				devicesPageIOS.addNewDevice()
				.tapOnBackBtn();
				////Just to refresh the view
				menuPageIOS.tappingHamburgerMenu();
				menuPageIOS.tappingDevices();
				if(devicesPageIOS.verifyDeviceNamePresentWithHub("Garage Door Opener", hubName)) {
					if(devicesPageIOS.verifyMonitorOnlyErrorOnDevice("Garage Door Opener", monitorOnlyError)) {
						extentTestIOS.log(Status.PASS, "Sensor Error is shown on UI");
					} else {
						extentTestIOS.log(Status.FAIL, "Sensor error not shown in UI");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					devicesPageIOS.tapOnMonitorOnly("Garage Door Opener");
					devicesPageIOS.selectGDOBrand("LiftMaster")
					.selectProgramButtonColor("Yellow")
					.tapProgramDoorOpener();
					devicesPageIOS.enterDeviceName(vgdoName);
					devicesPageIOS.verifySuccessText();
					devicesPageIOS.clickNextButton();
					devicesPageIOS.clickFinishButton();
					if(devicesPageIOS.verifyDeviceNamePresent(vgdoName)) {
						extentTestIOS.log(Status.PASS, "VGDO added successfully in A-hub");	
					} else {
						extentTestIOS.log(Status.FAIL, "Unable to Add VGDO in A-hub ");	
						utilityIOS.captureScreenshot(iOSDriver);
					}
					if(!devicesPageIOS.verifyYellowBarOnDevice(vgdoName)) {
						extentTestIOS.log(Status.PASS, "VGDO is back in normal state");
					} else {
						extentTestIOS.log(Status.FAIL, "VGDO in still is error state");
						utilityIOS.captureScreenshot(iOSDriver);
					}
					deviceStatus1 = devicesPageIOS.getDeviceStatus(vgdoName);
					if (!requiredState.equalsIgnoreCase(deviceStatus1)) {
						devicesPageIOS.actuatingDevice(vgdoName);
					}
					else {
						devicesPageIOS.actuatingDevice(vgdoName);
						devicesPageIOS.actuatingDevice(vgdoName);
					}
					deviceStatus2 = devicesPageIOS.getDeviceStatus(vgdoName);
					if (requiredState.equalsIgnoreCase(deviceStatus2)) {
						extentTestIOS.log(Status.PASS, ""+vgdoName+" state changed from " + deviceStatus1 + " To " + deviceStatus2 + ". "
								+ "Change "+vgdoName+" State to " + requiredState + " test case Passed");
					} else {
						extentTestIOS.log(Status.FAIL, ""+vgdoName+" state Unchanged from " + deviceStatus1 + ". "
								+ "Change "+vgdoName+" status test case Failed");
						utilityIOS.captureScreenshot(iOSDriver);
					}

					menuPageIOS.tappingHamburgerMenu()
					.tappingDeviceManagement();
					devicesPageIOS.selectGateway(hubName);
					deviceManagementIOS.deleteDevice(vgdoName);
					if(!deviceManagementIOS.verifyEndPointDevice(vgdoName)) {
						extentTestIOS.log(Status.PASS,""+vgdoName+"deleted successfully");
					} else {
						extentTestIOS.log(Status.FAIL,""+vgdoName+" did not deleted");
					}
					deviceManagementIOS.tapBackBtn();
				} else {
					extentTestIOS.log(Status.FAIL,"Device not added with default name Garage Door Opener.");
					utilityIOS.captureScreenshot(iOSDriver);
				}
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add Gateway");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 *  * Description : This is an IOS test method to verify adding the VGDO to AHub in OEM Tx Pairing Option 2
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA535_ValidateOEMTxPairingOption2(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String vgdoSerialNumber;
		String vgdoName;
		String hubName;
		String hubSerialNumber;
		String sensor;
		String deviceStatus1;
		String deviceStatus2;
		String requiredState =  "Closed";
		String monitorOnlyError = "This door is in monitor only mode.";
		hubName = testData.get(0).split(",")[0];
		hubSerialNumber = testData.get(0).split(",")[1];
		vgdoSerialNumber =testData.get(0).split(",")[2];
		vgdoName =testData.get(0).split(",")[3];
		sensor = testData.get(0).split(",")[4];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		deviceManagementIOS = new DeviceManagementIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);


		if (expectedRolePresent) {
			menuPageIOS.tappingHamburgerMenu()
			.tappingDevices();
			if(devicesPageIOS.verifyAddDeviceButton()) {
				devicesPageIOS.addNewDevice();
				if(!devicesPageIOS.verifyGatewayAvailable(hubName)) {
					devicesPageIOS.tapOnBackBtn();
					if(devicesPageIOS.verifyGetStartedButton()) {
						devicesPageIOS.tapOnGetStartedBtn();
						extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
					} else {
						devicesPageIOS.addNewDevice();
					}
					devicesPageIOS.addGateWay(hubSerialNumber,hubName);
				} 
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDevices();

					webService.SendGatewayOnlineService(hubSerialNumber)
					.GatewayStartLearningService(hubSerialNumber)
					.enterLearnModeForVGDO(vgdoSerialNumber)
					.testSensorVGDO(vgdoSerialNumber, sensor);

					////Just to refresh the view
					menuPageIOS.tappingHamburgerMenu();
					menuPageIOS.tappingDevices();

					if(devicesPageIOS.verifyDeviceNamePresentWithHub("Garage Door Opener", hubName)) {
						if(devicesPageIOS.verifyMonitorOnlyErrorOnDevice("Garage Door Opener", monitorOnlyError)) {
							extentTestIOS.log(Status.PASS, "Sensor Error is shown on UI");
						} else {
							extentTestIOS.log(Status.FAIL, "Sensor error not shown in UI");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						devicesPageIOS.tapOnMonitorOnly("Garage Door Opener");
						devicesPageIOS.selectGDOBrand("LiftMaster")
						.selectProgramButtonColor("Yellow")
						.tapProgramDoorOpener();
						devicesPageIOS.enterDeviceName(vgdoName);
						devicesPageIOS.verifySuccessText();
						devicesPageIOS.clickNextButton();
						devicesPageIOS.clickFinishButton();
						if(devicesPageIOS.verifyDeviceNamePresent(vgdoName)) {
							extentTestIOS.log(Status.PASS, "VGDO added successfully in A-hub");	
						} else {
							extentTestIOS.log(Status.FAIL, "Unable to Add VGDO in A-hub ");	
							utilityIOS.captureScreenshot(iOSDriver);
						}
						if(!devicesPageIOS.verifyYellowBarOnDevice(vgdoName)) {
							extentTestIOS.log(Status.PASS, "VGDO is back in normal state");
						} else {
							extentTestIOS.log(Status.FAIL, "VGDO in still is error state");
							utilityIOS.captureScreenshot(iOSDriver);
						}
						deviceStatus1 = devicesPageIOS.getDeviceStatus(vgdoName);
						if (!requiredState.equalsIgnoreCase(deviceStatus1)) {
							devicesPageIOS.actuatingDevice(vgdoName);
						}
						else {
							devicesPageIOS.actuatingDevice(vgdoName);
							devicesPageIOS.actuatingDevice(vgdoName);
						}
						deviceStatus2 = devicesPageIOS.getDeviceStatus(vgdoName);
						if (requiredState.equalsIgnoreCase(deviceStatus2)) {
							extentTestIOS.log(Status.PASS, ""+vgdoName+" state changed from " + deviceStatus1 + " To " + deviceStatus2 + ". "
									+ "Change "+vgdoName+" State to " + requiredState + " test case Passed");
						} else {
							extentTestIOS.log(Status.FAIL, ""+vgdoName+" state Unchanged from " + deviceStatus1 + ". "
									+ "Change "+vgdoName+" status test case Failed");
							utilityIOS.captureScreenshot(iOSDriver);
						}

						menuPageIOS.tappingHamburgerMenu()
						.tappingDeviceManagement();
						devicesPageIOS.selectGateway(hubName);
						deviceManagementIOS.deleteDevice(vgdoName);
						if(!deviceManagementIOS.verifyEndPointDevice(vgdoName)) {
							extentTestIOS.log(Status.PASS,""+vgdoName+"deleted successfully");
						} else {
							extentTestIOS.log(Status.FAIL,""+vgdoName+" did not deleted");
						}
						deviceManagementIOS.tapBackBtn()
						.deleteHub(hubName);
					} else {
						extentTestIOS.log(Status.FAIL,"Device not added with default name Garage Door Opener.");
					}
				
			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add Gateway");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	/**
	 * Description	 :	This IOS test method adds virtual VGDO to Wi-Fi Hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA548_ValidateAddingMaximumVGDOToHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

		String gatewayName;
		String vgdoSerialNumber1;
		String vgdoSerialNumber2;
		String vgdoName1;
		String vgdoName2;
		String gatewaySerialNumber;
		String sensor;
		
		gatewaySerialNumber = testData.get(0).split(",")[0];
		gatewayName = testData.get(0).split(",")[1];
		vgdoSerialNumber1 = testData.get(0).split(",")[2];
		vgdoName1 = testData.get(0).split(",")[3];
		vgdoSerialNumber2 = testData.get(0).split(",")[4];
		vgdoName2 = testData.get(0).split(",")[5];
		sensor =  testData.get(0).split(",")[6];

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		deviceManagementIOS =  new DeviceManagementIOS(iOSDriver, extentTestIOS);
		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
		WebServices webService = new WebServices(extentTestIOS);

		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingDevices();
		if (expectedRolePresent) {
			if (devicesPageIOS.verifyAddDeviceButton()) {
				devicesPageIOS.addNewDevice();
				if(!devicesPageIOS.verifyGatewayAvailable(gatewayName)) {
					devicesPageIOS.tapOnBackBtn()
			        .addNewDevice();
					devicesPageIOS.addGateWay(gatewaySerialNumber,gatewayName);
				} 
				//Adding VGDO 1
				if(devicesPageIOS.verifyGetStartedButton()) {
					devicesPageIOS.tapOnGetStartedBtn();
					extentTestIOS.log(Status.INFO,"Get Started button is displayed successfully.");
				} else {
					devicesPageIOS.addNewDevice();
				}
				devicesPageIOS.selectGateway(gatewayName)
				.clickGDOButton()
				.clickNextButton()
				.clickNextButton();
				webService.enterLearnModeForVGDO(vgdoSerialNumber1);
				iOSDriver.getCapabilities();
				webService.testSensorVGDO(vgdoSerialNumber1, sensor);
				devicesPageIOS.selectGDOBrand("LiftMaster")
				.selectProgramButtonColor("Yellow")
				.tapProgramDoorOpener();
				devicesPageIOS.enterDeviceName(vgdoName1);
				devicesPageIOS.clickNextButton();
				devicesPageIOS.clickFinishButton();
				if(devicesPageIOS.verifyDeviceNamePresent(vgdoName1)) {
					extentTestIOS.log(Status.PASS, "VGDO1 added successfully in Wi-Fi hub");	
					// Adding VGDO 2
					devicesPageIOS.addNewDevice();
					devicesPageIOS.selectGateway(gatewayName)
					.clickGDOButton()
					.clickNextButton()
					.clickNextButton();
					webService.enterLearnModeForVGDO(vgdoSerialNumber2);
					iOSDriver.getCapabilities();
					webService.testSensorVGDO(vgdoSerialNumber2, sensor);
					devicesPageIOS.selectGDOBrand("LiftMaster")
					.selectProgramButtonColor("Yellow")
					.tapProgramDoorOpener();
					devicesPageIOS.enterDeviceName(vgdoName2);
					devicesPageIOS.clickNextButton();
					devicesPageIOS.clickFinishButton();
					if(devicesPageIOS.verifyDeviceNamePresent(vgdoName2)) {
						extentTestIOS.log(Status.PASS, "VGDO1 added successfully in Wi-Fi hub");
						//Adding 3rd VGDO
						devicesPageIOS.addNewDevice();
						devicesPageIOS.selectGateway(gatewayName)
						.clickGDOButton()
						.clickNextButton();
						if(deviceManagementIOS.verifyAlertForAddingMoreThan2VGDO()) {
							extentTestIOS.log(Status.PASS, "Error Popup displayed successfully, while adding 3rd VGDO");
							deviceManagementIOS.tapBackBtn()
							.tapBackBtn()
							.tapBackBtn();
							//Deleting the added hub
							menuPageIOS.tappingHamburgerMenu()
							.tappingDeviceManagement();
							deviceManagementIOS.deleteHub(gatewayName);
						} else {
							extentTestIOS.log(Status.FAIL, "Error Popup not displayed");
							utilityIOS.captureScreenshot(iOSDriver);
						}
					}
					else {
						extentTestIOS.log(Status.FAIL, "Unable to Add VGDO1 in Wi-Fi hub ");	
						utilityIOS.captureScreenshot(iOSDriver);
					}
				} else {
					extentTestIOS.log(Status.FAIL, "Unable to Add VGDO1 in Wi-Fi hub ");	
					utilityIOS.captureScreenshot(iOSDriver);
				}

				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
				}

			} else {
				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
					extentTestIOS.log(Status.FAIL, activeUserRole+ " user does not have access to Add VGDO");
					utilityIOS.captureScreenshot(iOSDriver);
				} 
				// To bring back to the Home screen
			}
		} else {
			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
		}
	}
	
	
	//Add GDO to Wifi-Hub has been removed from 3.103 build
	/**
	 * Description	 :	This iOS test method adds virtual GDO to A-hub/Wifi-hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA376_ValidateAddingGDOToAHuborWifiHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

//		String hubSerialNumber;
//		String gdoSerialNumber;
//		String gdoName;
//
//		hubSerialNumber = testData.get(0).split(",")[0];
//		gdoSerialNumber = testData.get(0).split(",")[1];
//		gdoName = testData.get(0).split(",")[2];
//
//		extentTestIOS = createTest(deviceTest,scenarioName); 
//		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
//
//		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
//		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
//		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
//		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
//
//		WebServices webService = new WebServices(extentTestIOS);
//		if (expectedRolePresent) {
//			menuPageIOS.tappingHamburgerMenu();
//			menuPageIOS.tappingDevices();
//			if (devicesPageIOS.verifyAddDeviceButton()) {
//				devicesPageIOS.addNewDevice();
//				if(devicesPageIOS.verifyGatewayAvailable(hubSerialNumber)){
//					devicesPageIOS.selectGateway(hubSerialNumber)
//					.clickGDOButton()
//					.tapNoButton()
//					.clickNextButton();
//					webService.GatewayStartLearningService(hubSerialNumber)
//					.LearnLightToGatewayService(hubSerialNumber,gdoSerialNumber);
//					devicesPageIOS.enterDeviceName(gdoName);
//					devicesPageIOS.clickNextButton();
//					if (devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
//						extentTestIOS.log(Status.PASS, gdoName+ " added successfully");	
//					} else {
//						extentTestIOS.log(Status.FAIL, gdoName+ " not added.");	
//					} 
//				} else {
//					extentTestIOS.log(Status.INFO, "Expected hub not present");
//					devicesPageIOS.tapOnCancelBtn();
//				}
//				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
//					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
//				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
//					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
//				}
//			} else {
//				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
//					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
//				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
//					utilityIOS.captureScreenshot(iOSDriver);
//				} 
//			}
//		} else {
//			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
//		}
	}
	//Add GDO to A-Hub has been removed from 3.103 build
	/**
	 * Description	 :	This iOS test method adds virtual GDO to A-hub
	 * @param testData
	 * @param deviceTest
	 * @param scenarioName
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws ParseException
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	public void MTA376_ValidateAddingGDOToAHub(ArrayList<String> testData, String deviceTest,String scenarioName) throws ClientProtocolException, IOException, ParseException, InterruptedException, AWTException {

//		String aHubSerialNumber;
//		String aHubName;
//		String gdoSerialNumber;
//		String gdoName;
//
//		aHubSerialNumber = testData.get(0).split(",")[0];
//		aHubName = testData.get(0).split(",")[1];
//		gdoSerialNumber = testData.get(0).split(",")[2];
//		gdoName = testData.get(0).split(",")[3];
//
//		extentTestIOS = createTest(deviceTest,scenarioName); 
//		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();
//
//		devicesPageIOS = new DevicesPageIOS(iOSDriver, extentTestIOS);
//		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
//		deviceManagementIOS = new DeviceManagementIOS(iOSDriver,extentTestIOS);
//		utilityIOS = new  UtilityIOS(iOSDriver, extentTestIOS);
//
//		WebServices webService = new WebServices(extentTestIOS);
//		if (expectedRolePresent) {
//			menuPageIOS.tappingHamburgerMenu();
//			menuPageIOS.tappingDevices();
//			if (devicesPageIOS.verifyAddDeviceButton()) {
//				devicesPageIOS.addNewDevice();
//				if(devicesPageIOS.verifyGatewayAvailable(aHubName)){
//					devicesPageIOS.selectGateway(aHubName)
//					.clickGDOButton()
//					.tapNoButton()
//					.clickNextButton();
//					webService.GatewayStartLearningService(aHubSerialNumber)
//					.LearnGDOToGatewayService(aHubSerialNumber,gdoSerialNumber);
//					devicesPageIOS.enterDeviceName(gdoName);
//					devicesPageIOS.clickNextButton();
//					if (devicesPageIOS.verifyDeviceNamePresent(gdoName)) {
//						extentTestIOS.log(Status.PASS, gdoName+ " added successfully in A-Hub");	
//					} else {
//						extentTestIOS.log(Status.FAIL, gdoName+ " not added in A-Hub");	
//					} 
//				} else {
//					extentTestIOS.log(Status.INFO, "Expected A-hub not present");
//					devicesPageIOS.tapOnCancelBtn();
//				}
//				if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator")) {
//					extentTestIOS.log(Status.PASS, activeUserRole+ " user has permission to Add Device and gateway as Expected");
//				} else if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
//					extentTestIOS.log(Status.FAIL, activeUserRole+ "  user has permission to Add Device and gateway , but it is not allowed");
//				}
//			} else {
//				if (activeUserRole.equalsIgnoreCase("Family") || activeUserRole.equalsIgnoreCase("Guest")) {
//					extentTestIOS.log(Status.PASS, activeUserRole+ "  user does not have access to  Add Gateway and device as expected");
//				} else if (activeUserRole.equalsIgnoreCase("Admin") || activeUserRole.equalsIgnoreCase("Creator") ||activeUserRole.equalsIgnoreCase("Family")) {
//					utilityIOS.captureScreenshot(iOSDriver);
//				} 
//			}
//		} else {
//			extentTestIOS.log(Status.FAIL, "Expected user role is not available");
//		}
	}
	
	/**
	 * Description	 :	This is the logout test case
	 * @param testData
	 * @param deviceTest
	 * @throws InterruptedException
	 */
	public void logoutIOS(ArrayList<String> testData, String deviceTest,String scenarioName) throws InterruptedException {

		extentTestIOS = createTest(deviceTest,scenarioName);
		GlobalVariables.currentTestIOS  = Thread.currentThread().getStackTrace()[1].getMethodName();

		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);

		//logout
		menuPageIOS.logout();
		iOSDriver.resetApp();
	}

	/**
	 * Description	 :	This function is for configuring reports
	 * @throws IOException 
	 */
	public void configReport(String testName,String environmentName,String deviceTest,String brandName) throws InterruptedException
	{
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String strDate = formatter.format(date);
		File file = new File(".//Reports//"+strDate);
		if (!file.exists()) {
			file.mkdirs();
		}

		String dateTime = new SimpleDateFormat("dd-MM-yyyy-hhmmss").format(new Date());
		if (deviceTest.equals("iOS1"))
		{
			extentHtmlReporterIOS1 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			extentReportsIOS1 = new ExtentReports();		
			extentReportsIOS1.attachReporter(extentHtmlReporterIOS1);
			extentReportsIOS1.setSystemInfo("Environment",environmentName);
			extentHtmlReporterIOS1.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterIOS1.config().setReportName("MyQ iOS Report");
			extentHtmlReporterIOS1.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterIOS1.config().setTheme(Theme.STANDARD);
		} else {
			extentHtmlReporterIOS2 = new ExtentHtmlReporter(file+"//Reports_"+brandName+"_"+deviceTest+"_"+dateTime+".html");
			extentReportsIOS2 = new ExtentReports();		
			extentReportsIOS2.attachReporter(extentHtmlReporterIOS2);
			extentReportsIOS2.setSystemInfo("Environment",environmentName);
			extentHtmlReporterIOS2.config().setDocumentTitle("MyQ automation test");
			extentHtmlReporterIOS2.config().setReportName("MyQ iOS Report");
			extentHtmlReporterIOS2.config().setTestViewChartLocation(ChartLocation.TOP);
			extentHtmlReporterIOS2.config().setTheme(Theme.STANDARD);
		}
	}

	/**
	 * Description : This function would close the driver session
	 */
	public void quitIOSDriver() {
		iOSDriver.quit();
	}

	/**
	 * Description	 :	This function would skip the test cases in IOS, if test cases of the groups are not matching with the respective sheet
	 * @param testcase,deviceTest
	 */
	public void skipIOSTest(String testcase, String deviceTest) {

		extentTestIOS = createTest(deviceTest,testcase);
		extentTestIOS.log(Status.SKIP, "Test : " +testcase+ " not found in " +activeUserRole +"_All_Test sheet" );
	}

	/**
	 * Description : This function will start the iOS driver and login to the application if we get any false failure
	 */
	public synchronized void reportFlushIOS(String deviceTest) {
		loginPageIOS = new LoginPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		devicesPageIOS = new DevicesPageIOS(iOSDriver,extentTestIOS);
		if (deviceTest.equals("iOS1")) {
			extentReportsIOS1.flush();
			try {
				iOSDriver.switchTo().alert().accept();
			} catch (Exception e) {
			}
			if (!menuPageIOS.verifyHamburgerMenu() && !(loginPageIOS.verifyLoginButton())) {
				iOSDriver.launchApp();
				loginPageIOS.login(userEmail,password);
				if (devicesPageIOS.verifyBlueBarPresent()) {
					devicesPageIOS.clickOnBlueBar();
					if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
						devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
						expectedRolePresent = true;
					} else {
						expectedRolePresent = false;	
						devicesPageIOS.tapOnCancelBtn();
					} 
				} else {
					if (activeUserRole.equalsIgnoreCase("Creator")) {
						expectedRolePresent = true;
					} else {
						expectedRolePresent = false;
					}
				} 
			}

		} else {
			extentReportsIOS2.flush();
			try {
				iOSDriver.switchTo().alert().accept();
			} catch (Exception e) {
			}
			if (!menuPageIOS.verifyHamburgerMenu() && !(loginPageIOS.verifyLoginButton())) {
				iOSDriver.launchApp();
				loginPageIOS.login(userEmail,password);
				if (devicesPageIOS.verifyBlueBarPresent()) {
					devicesPageIOS.clickOnBlueBar();
					if (devicesPageIOS.verifyUserPresent(activeAccountName, activeUserRole)) {
						devicesPageIOS.selectUserRole(activeAccountName,activeUserRole);
						expectedRolePresent = true;
					} else {
						expectedRolePresent = false;	
						devicesPageIOS.tapOnCancelBtn();
					}
				} else {
					if (activeUserRole.equalsIgnoreCase("Creator")) {
						expectedRolePresent = true;
					} else {
						expectedRolePresent = false;
					}
				}
			}

		}
	}

	/**
	 * Description : This function will convert milliseconds to hours, minutes and seconds
	 */
	public String msToString(long ms) {
		long totalSecs = ms/1000;
		long hours = (totalSecs / 3600);
		long mins = (totalSecs / 60) % 60;
		long secs = totalSecs % 60;
		String minsString = (mins == 0)
				? "00"
						: ((mins < 10)
								? "0" + mins
										: "" + mins);
		String secsString = (secs == 0)
				? "00"
						: ((secs < 10)
								? "0" + secs
										: "" + secs);
		if (hours > 0)
			return hours + "h:" + minsString + "m:" + secsString+"s";
		else if (mins > 0)
			return mins + "m:" + secsString+"s";
		else return secsString+"s";
	}

	/**
	 * Description : This function will apply the custom CSS onto the extent report for total time
	 */
	public void applyCustomCSS(String deviceTest,String brandName) throws InterruptedException {
		if (deviceTest.equals("iOS1")) {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain"))
			{
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterIOS1.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterIOS1.config().setCSS(customCSS);
				extentReportsIOS1.flush();
			} else {
				extentTestIOS.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		} else {
			if (brandName.equalsIgnoreCase("LiftMaster") || brandName.equalsIgnoreCase("Chamberlain")) {
				String reqPath = System.getProperty("user.dir")+"//Resources//"+brandName+".jpg";
				long time = extentHtmlReporterIOS2.getRunDuration();
				String customCSS = "a.brand-logo.blue.darken-3{color: transparent; background: url(file:///"+reqPath+") no-repeat;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div{visibility: hidden;}\n#dashboard-view > div > div:nth-of-type(2) > div:nth-of-type(5) > div > div:before{content: '"+msToString(time)+"'; visibility: visible; display: block; position: absolute;}";
				extentHtmlReporterIOS2.config().setCSS(customCSS);
				extentReportsIOS2.flush();
			} else {
				extentTestIOS.log(Status.FAIL, brandName +"image is not available in resource folder");
			}
		}
	}
}
