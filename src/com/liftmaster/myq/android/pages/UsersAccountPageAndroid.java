package com.liftmaster.myq.android.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class UsersAccountPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	//Page factory for Users Account Page Android
	@AndroidFindBy(id = "image_toolbar_nav_icon")
	public AndroidElement hamburger_icon;

	@AndroidFindBy(id = "layout_account_invitations")
	public AndroidElement account_invitation_link;

	@AndroidFindBy(id = "item_title")
	public List<AndroidElement> invitations_list;

	@AndroidFindBy(id = "button_left")
	public AndroidElement decline_btn;

	@AndroidFindBy(id = "button_right")
	public AndroidElement accept_btn;

	@AndroidFindBy(id = "access_level")
	public AndroidElement user_role;

	@AndroidFindBy(id = "item_status")
	public List<AndroidElement> status;

	@AndroidFindBy(id = "Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "account_email")
	public AndroidElement account_email;

	@AndroidFindBy(id = "text_emptymsg_text")
	public AndroidElement no_invitation_text;

	@AndroidFindBy(className = "android.widget.ImageButton")
	public AndroidElement back_btn;

	@AndroidFindBy(id = "edit")
	public AndroidElement edit_info_account;

	@AndroidFindBy(id = "account_first_name")
	public AndroidElement edit_first_name;

	@AndroidFindBy(id = "account_last_name")
	public AndroidElement edit_last_name;

	@AndroidFindBy(id = "account_name")
	public AndroidElement edit_account_name;

	@AndroidFindBy(id = "account_email")
	public AndroidElement edit_account_email;

	@AndroidFindBy(id = "account_address1")
	public AndroidElement edit_street1;

	@AndroidFindBy(id = "account_address2")
	public AndroidElement edit_street2;

	@AndroidFindBy(id = "account_city")
	public AndroidElement edit_city;

	@AndroidFindBy(id = "account_state")
	public AndroidElement edit_state;

	@AndroidFindBy(id = "account_zip")
	public AndroidElement edit_zip_code;

	@AndroidFindBy(id = "account_mobile_phone")
	public AndroidElement edit_mobile_number;

	@AndroidFindBy(id = "fragment_account_edit_spinner_country")
	public AndroidElement edit_country;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Spain']")
	public AndroidElement country_spain;

	@AndroidFindBy(id = "fragment_account_edit_spinner_timezone")
	public AndroidElement edit_time_zone;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Asia/Colombo']")
	public AndroidElement time_zone_asiaColombo;

	@AndroidFindBy(id = "account_language")
	public AndroidElement edit_language;

	@AndroidFindBy(id = "fragment_edit_account_email_checkbox")
	public AndroidElement account_checkbox;

	@AndroidFindBy(className = "android.widget.RadioButton")
	public List<AndroidElement> select_language;

	@AndroidFindBy(xpath = "//android.widget.ImageButton")
	public AndroidElement back_button;

	@AndroidFindBy(id = "save")
	public AndroidElement save_button;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView")
	public List<AndroidElement> country_list;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView")
	public List<AndroidElement> timeZone_list;

	@AndroidFindBy(id = "android:id/progress")
	public AndroidElement spinner_on_save;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement error_text;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement ok_button;

	@AndroidFindBy(id = "account_language")
	public AndroidElement account_language;

	@AndroidFindBy(id = "android:id/text1")
	public List<AndroidElement> list;

	@AndroidFindBy(id = "layout_account_change_email")
	public AndroidElement change_email;

	@AndroidFindBy(id = "edit_email_new")
	public AndroidElement new_email;

	@AndroidFindBy(id = "edit_email_confirm_new")
	public AndroidElement confirm_new_email;

	@AndroidFindBy(id = "edit_email_password")
	public AndroidElement current_password;

	@AndroidFindBy(id = "button_right")
	public AndroidElement next_button;

	@AndroidFindBy(id = "android:id/alertTitle")
	public AndroidElement confirmation_heading;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement message_text;

	@AndroidFindBy(id = "text_account_change_password_label")
	public AndroidElement change_password;

	@AndroidFindBy(id = "edit_password")
	public AndroidElement original_password;

	@AndroidFindBy(id = "edit_password_new")
	public AndroidElement new_password;

	@AndroidFindBy(id = "edit_password_confirm")
	public AndroidElement confirm_password;

	@AndroidFindBy(id = "button_right" )
	public AndroidElement next_btn;

	@AndroidFindBy(id = "text_account_security_label")
	public AndroidElement security;

	@AndroidFindBy(id = "button_right")
	public AndroidElement done_btn;

	@AndroidFindBy(className = "android.widget.TextView")
	public List<AndroidElement> security_list;

	@AndroidFindBy(className = "android.widget.Switch")
	public List<AndroidElement> enable_disable_switch;

	@AndroidFindBy(id ="passcode1")
	public  AndroidElement passcode_textbox_1;

	@AndroidFindBy(id ="passcode2")
	public  AndroidElement passcode_textbox_2;

	@AndroidFindBy(id ="passcode3")
	public  AndroidElement passcode_textbox_3;

	@AndroidFindBy(id ="passcode4")
	public  AndroidElement passcode_textbox_4;

	@AndroidFindBy(id = "text_login_email_greeting")
	public AndroidElement greeting_text;

	@AndroidFindBy(id = "passcode_header")
	public AndroidElement passcode_header;

	@AndroidFindBy(id = "text_toolbar_title")
	public AndroidElement toolbar_text;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Change Passcode']")
	public AndroidElement change_passcode;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Passcode']")
	public AndroidElement passcode_text;

	@AndroidFindBy(id = "text_account_name")
	public AndroidElement user_name;

	@AndroidFindBy(id = "delete")
	public AndroidElement delete_invitation_btn;

	@AndroidFindBy(id = "text_account_invitations_badge")
	public AndroidElement badge_icon_text;

	@AndroidFindBy(id = "button_left")
	public AndroidElement cancel_btn;

	int iIndex = 0;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public UsersAccountPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver=androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
	}

	/**
	 * Description : This Method is to click on account invitations link on accounts page
	 * return page object
	 */
	public UsersAccountPageAndroid clickOnAccountInvitationLink() {
		try {
			utilityAndroid.genericWait(account_invitation_link, "Click");
			utilityAndroid.clickAndroid(account_invitation_link);
			utilityAndroid.waitForGenericSpinner();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on the in
	 * @param userName
	 * @Param userRole
	 * @return Page object
	 */
	public UsersAccountPageAndroid clickOnInvitation(String userName, String userRole) {
		try {
			for (iIndex = 0 ; iIndex < invitations_list.size(); iIndex++) {
				if (invitations_list.get(iIndex).getText().trim().equalsIgnoreCase(userName.trim()) && status.get(iIndex).getText().trim().equalsIgnoreCase(userRole.trim())) {
					utilityAndroid.clickAndroid(invitations_list.get(iIndex));
					break;
				}
			}
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the invitations that are accepted by the user
	 * @param userName
	 * @Param userRole
	 * @return Page object
	 */
	public boolean verifyAcceptedInvitation(String userName, String userRole) {
		boolean invitationPresent = false;
		try {
			utilityAndroid.waitForGenericSpinner();
			for (iIndex = 0 ; iIndex < invitations_list.size(); iIndex++) {
				if (invitations_list.get(iIndex).getText().trim().equalsIgnoreCase(userName.trim()) && status.get(iIndex).getText().trim().equalsIgnoreCase(userRole.trim())) {
					utilityAndroid.clickAndroid(invitations_list.get(iIndex));
					invitationPresent = true;
					break;
				}
			}
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return invitationPresent;
	}

	/**
	 * Description : This Method is to click on account pending invitations link
	 */
	public UsersAccountPageAndroid clickOnPendingInvitations(String userName, String userRole) {		
		try {
			utilityAndroid.waitForGenericSpinner();
			for (iIndex = 0 ; iIndex < invitations_list.size(); iIndex++) {
				if (invitations_list.get(iIndex).getText().trim().equalsIgnoreCase(userName+" - "+userRole)) {
					utilityAndroid.clickAndroid(invitations_list.get(iIndex));
					break;
				} 
			}
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to verify the pending invitations
	 */
	public boolean verifyPendingInvitations(String userName, String userRole) {		
		boolean pendingInvite = false;
		try {
			utilityAndroid.waitForGenericSpinner();
			for (iIndex = 0 ; iIndex < invitations_list.size(); iIndex++) {
				if (invitations_list.get(iIndex).getText().trim().equalsIgnoreCase(userName+" - "+userRole)) {
					utilityAndroid.clickAndroid(invitations_list.get(iIndex));
					pendingInvite = true;
					break;
				}
			}
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return pendingInvite;
	}
	/**
	 * Description : This Method is to click on accept or decline button
	 * return page object 
	 */
	public UsersAccountPageAndroid clickOnAcceptOrDecline(String status) {
		try {
			if (status.equalsIgnoreCase("Accept")) {
				utilityAndroid.clickAndroid(accept_btn);
			} else  if (status.equalsIgnoreCase("Decline"))  {
				utilityAndroid.clickAndroid(decline_btn);
			}
			try {
				if (spinner.isDisplayed()) {
					Thread.sleep(4000);
				}
			} catch (Exception e) {
				// catch exception and proceed with the execution
			}
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This method is to verify the role of the user
	 * @param email
	 * @return Page object
	 */
	public boolean verifyUserRole(String userRole) {
		boolean expectedUserRole = false;
		try {
			if (user_role.getText().trim().equalsIgnoreCase(userRole)) {
				expectedUserRole = true;
			} 
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return expectedUserRole;
	}

	/**
	 * Description : This function would verify the account email
	 * @param email
	 * @return
	 */
	public boolean verifyAccountPage(String email) {
		try { 
			utilityAndroid.genericWait(edit_info_account, "Click");
			if (account_email.getText().trim().equalsIgnoreCase(email)) {
				extentTestAndroid.log(Status.INFO, "Account email is " + account_email.getText());
				return true;
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description : This function would verify no account invitation text
	 * @return
	 */
	public boolean verifyNoInvitationText() {
		try {
			if (no_invitation_text.isDisplayed()) {
				return true;
			} 
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would click on back button
	 * @return
	 */
	public UsersAccountPageAndroid clickOnBackButton () {
		try {
			utilityAndroid.genericWait(back_btn, "Click");
			utilityAndroid.clickAndroid(back_btn);
		}  catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on user edit info
	 */
	public UsersAccountPageAndroid tapEditUserInfo()	{
		try { 
			utilityAndroid.genericWait(edit_info_account, "Click");
			utilityAndroid.clickAndroid(edit_info_account); 
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			//
		}
		return this;
	}

	/**
	 * Description :This function would enter the First Name of user
	 * @param firstName
	 */
	public UsersAccountPageAndroid editFirstName(String firstName) { 
		boolean elementDisplayed = false;
		try {
			if (edit_first_name.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				androidDriver.swipe(0,400,0, 1400, 3000);
			}
			utilityAndroid.clickAndroid(edit_first_name);
			utilityAndroid.clearAndroid(edit_first_name);
			utilityAndroid.enterTextAndroid(edit_first_name, firstName);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getFirstName() {
		String firstName = null;
		try {
			firstName =  edit_first_name.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return firstName;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getLastName() {
		String lastName = null;
		try {
			lastName =  edit_last_name.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return lastName;
	}

	/**
	 * Description :This function would edit the Last Name of user
	 * @param lastName
	 */
	public UsersAccountPageAndroid editLastName(String lastName) { 
		try {
			utilityAndroid.clickAndroid(edit_last_name);
			utilityAndroid.clearAndroid(edit_last_name);
			utilityAndroid.enterTextAndroid(edit_last_name, lastName);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description :This function would edit the Account Name of user
	 * @param accountName
	 */
	public UsersAccountPageAndroid editAccountName(String accountName) { 
		try {
			utilityAndroid.clickAndroid(edit_account_name);
			utilityAndroid.clearAndroid(edit_account_name);
			utilityAndroid.enterTextAndroid(edit_account_name, accountName);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getAccountName() {
		String accountName = null;
		try {
			accountName =  edit_account_name.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return accountName;
	}

	/**
	 * Description :This function would edit the Account Email
	 * @param accountEmail
	 */
	public boolean editAccountEmail() { 
		try {
			if (account_email.getAttribute("clickable").equals("false")){
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description :This function would edit the Address1
	 * @param accountAddress
	 */
	public UsersAccountPageAndroid editStreet1(String accountAddress) { 
		try {
			utilityAndroid.clickAndroid(edit_street1);
			utilityAndroid.enterTextAndroid(edit_street1, accountAddress);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getStreet1() {
		String street = null;
		try {
			street =  edit_street1.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return street;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getStreet2() {
		String street = null;
		try {
			street =  edit_street2.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return street;
	}

	/**
	 * Description :This function would edit the Address2
	 * @param accountAddress2
	 */
	public UsersAccountPageAndroid editStreet2(String accountAddress2) { 
		try {
			utilityAndroid.clickAndroid(edit_street2);
			utilityAndroid.enterTextAndroid(edit_street2, accountAddress2);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description :This function would edit the city
	 * @param accountCity
	 */
	public UsersAccountPageAndroid editAccountcity(String accountCity) { 
		try {
			utilityAndroid.clickAndroid(edit_city);
			utilityAndroid.clearAndroid(edit_city);
			utilityAndroid.enterTextAndroid(edit_city, accountCity);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getCity() {
		String accountCity = null;
		try {
			accountCity =  edit_city.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return accountCity;
	}

	/**
	 * Description :This function would edit the state
	 * @param accountState
	 */
	public UsersAccountPageAndroid editAccountState(String accountState) { 
		try {
			utilityAndroid.clearAndroid(edit_state);
			utilityAndroid.clickAndroid(edit_state);
			utilityAndroid.enterTextAndroid(edit_state, accountState);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getState() {
		String accountState = null;
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			//catch exceptions and continue with the execution flow
		}
		try {
			accountState =  edit_state.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return accountState;
	}

	/**
	 * Description :This function would edit the zip code
	 * @param accountZipCode
	 */
	public UsersAccountPageAndroid editAccountZipCode(String accountZipCode) { 
		try {
			utilityAndroid.clickAndroid(edit_zip_code);
			utilityAndroid.clearAndroid(edit_zip_code);
			utilityAndroid.enterTextAndroid(edit_zip_code, accountZipCode);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getZipCode() {
		String zipCode = null;
		try {
			zipCode =  edit_zip_code.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return zipCode;
	}

	/**
	 * Description :This function would edit the Mobile number
	 * @param mobileNumber
	 */
	public UsersAccountPageAndroid editMobileNumber(String mobileNumber) { 
		boolean elementDisplayed = false;
		try {
			if (edit_mobile_number.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_mobile_number);
			}
			utilityAndroid.clickAndroid(edit_mobile_number);
			utilityAndroid.enterTextAndroid(edit_mobile_number, mobileNumber);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getMobileNumber() {
		String number = null;
		boolean elementDisplayed = false;
		try {
			if (edit_mobile_number.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_mobile_number);
			}
			number =  edit_mobile_number.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return number;
	}

	/**
	 * Description :This function would select account country
	 */
	public UsersAccountPageAndroid selectCountry(String country) throws InterruptedException {
		boolean elementDisplayed = false;
		try {
			if (edit_country.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_country);
			}
			edit_country.click();
			for (int iIndex = 1; iIndex <= country_list.size(); iIndex++) {
				if (country_list.get(iIndex).getText().equalsIgnoreCase(country)) {
					country_list.get(iIndex).click();
					break;
				}
			}
		}catch (Exception e) {
			utilityAndroid.captureScreenshot(androidDriver);
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getCountry() {
		String country = null;
		boolean elementDisplayed = false;
		try {
			if (edit_country.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_country);
			}		
			country =  list.get(0).getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return country;
	}

	/**
	 * Description :This function would select the time zone
	 * @return 
	 */
	public UsersAccountPageAndroid selectTimeZone(String timeZone) throws InterruptedException {
		boolean elementDisplayed = false;
		try {
			if (edit_time_zone.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_time_zone);
			}
			utilityAndroid.clickAndroid(edit_time_zone);
			utilityAndroid.clickAndroid(time_zone_asiaColombo);
		}catch (Exception e) {
			utilityAndroid.captureScreenshot(androidDriver);
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getTimeZone() {
		String timeZone = null;
		boolean elementDisplayed = false;
		try {
			if (edit_time_zone.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_time_zone);
			}		
			timeZone =  list.get(1).getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return timeZone;
	}

	/**
	 * Description :This function would select language
	 */
	public UsersAccountPageAndroid selectLanguage(String language) throws InterruptedException{
		boolean elementDisplayed = false;
		try {
			if (edit_language.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_language);
			}
			utilityAndroid.clickAndroid(edit_language);
			for (int iIndex = 0 ; iIndex < select_language.size(); iIndex++) {
				if (select_language.get(iIndex).getText().equalsIgnoreCase(language)) {
					utilityAndroid.clickAndroid(select_language.get(iIndex));
				}
			}
			utilityAndroid.genericWait(back_button, "Click");
			back_button.click();
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would return the text entered in the field
	 * @return
	 */
	public String getLanguage() {
		String language = null;
		boolean elementDisplayed = false;
		try {
			if (edit_language.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			//continue with the execution
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(edit_language);
			}		
			language =  account_language.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return language;
	}

	/**
	 * Description : This function would click on account email CheckBox
	 */
	public UsersAccountPageAndroid tapAccountEmailCheckBox()	{
		try { 
			utilityAndroid.clearAndroid(account_checkbox);
			utilityAndroid.clickAndroid(account_checkbox); 
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on save button
	 */
	public UsersAccountPageAndroid clickOnSaveButton()	{
		try { 
			utilityAndroid.clickAndroid(save_button);
			for (int iIndex = 0; iIndex < 20 ; iIndex++) {
				try {
					if (spinner_on_save.isDisplayed()) {
						Thread.sleep(2000);
					}
				} catch (Exception e) {
					break;
				}
			}
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the errors present on screen
	 * @return
	 */
	public boolean verifyErrorOnScreen() {
		String noNameError = "Please enter a first and last name.";
		String noZipCode = "Please enter a zip code.";
		String emptyFields = "All fields must be completed before continuing." ;

		String emailAlreadyInUse = "This email is already in use. (703)";
		String invalidEmail = "Please enter a valid email address.";
		String passwordNotMatched = "Your current password does not match.";
		String emailNotMatched = "New email address does not match!";

		String passwordRequirment = "Passwords need to be at least 8 characters in length and must contain at least 3 of the following 4 types of characters: uppercase letter, lowercase letter, number or symbol.";
		String passwordMustMatch = "Both passwords must match.";

		try {
			if (error_text.getText().trim().equalsIgnoreCase(noNameError)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(noZipCode)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(emptyFields)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(emailAlreadyInUse)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(passwordNotMatched)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(invalidEmail)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}  else if (error_text.getText().trim().equalsIgnoreCase(emailNotMatched)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}  else if (error_text.getText().trim().equalsIgnoreCase(passwordMustMatch)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}   else if (error_text.getText().trim().equalsIgnoreCase(passwordRequirment)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.isDisplayed()) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else {
				return false;
			}
		} catch (Exception e ){
			return false;
		}
	}

	/**
	 * Description : This function would click on user change Email
	 */
	public UsersAccountPageAndroid tapChangeEmail()	{
		try { 
			utilityAndroid.clickAndroid(change_email); 
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description :This function would enter the new Email
	 * @param newEmail
	 */
	public UsersAccountPageAndroid setNewEmail(String newEmail) { 
		try {
			utilityAndroid.enterTextAndroid(new_email, newEmail);
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch the exception and continue with execution
		}
		return this;
	}

	/**
	 * Description :This function would enter the confirm new Email
	 * @param confirmNewEmail
	 */
	public UsersAccountPageAndroid setConfirmNewEmail(String confirmNewEmail) { 
		try {
			utilityAndroid.enterTextAndroid(confirm_new_email, confirmNewEmail);
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch the exception and continue with execution
		}
		return this;
	}

	/**
	 * Description :This function would enter the current password
	 * @param currentPassword
	 */
	public UsersAccountPageAndroid enterCurrentPassword (String currentPassword) { 
		try {
			utilityAndroid.enterTextAndroid(current_password, currentPassword);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would verify the password Criteria
	 * @Param newPassword
	 * @return boolean
	 */	
	public boolean validatePasswordCriteria(String newPassword) {
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";

		try {
			if (newPassword.matches(pattern)) {
				return true;
			}
			else {
				return false;
			}
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function is for click on next button
	 */	
	public UsersAccountPageAndroid clickNextButton() {
		try {
			utilityAndroid.genericWait(next_button, "Click");
			utilityAndroid.clickAndroid(next_button);
			utilityAndroid.waitForGenericSpinner();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the confirmation pop up after changing email
	 * @param email
	 * @return
	 */
	public boolean verifyEmailChangeConfimation(String email) {
		String tittle = "Check Email";
		String confirmationMessage = "An email has been sent to " + email + ". Please click the link in the email to confirm your change.";
		try {
			if ( confirmation_heading.getText().trim().equalsIgnoreCase(tittle) && error_text.getText().trim().equalsIgnoreCase(confirmationMessage)) {
				extentTestAndroid.log(Status.INFO, confirmation_heading.getText() + " : " + message_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				for (int iIndex = 0; iIndex < 20 ; iIndex++) {
					try {
						if (spinner.isDisplayed()) {
							Thread.sleep(2000);
						}
					} catch (Exception e) {
						break;
					}
				}
				return true;
			} else {
				extentTestAndroid.log(Status.INFO, message_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return false;
			}
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description :This function would select change password option.
	 * @return page object
	 */
	public UsersAccountPageAndroid tappingChangePassword()	{
		try { 
			utilityAndroid.genericWait(change_password, "Click");
			utilityAndroid.clickAndroid(change_password); 
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description :This function would enter the original password
	 * @param password
	 * @return page object
	 */
	public UsersAccountPageAndroid enterPassword(String password) { 
		try {
			utilityAndroid.genericWait(original_password, "Click");
			utilityAndroid.clickAndroid(original_password);
			utilityAndroid.enterTextAndroid(original_password, password);
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// continue with the execution
		}
		return this;	
	}
	/**
	 * Description :This function would enter the new password
	 * @param newPassword
	 * @return page object
	 */
	public UsersAccountPageAndroid enterNewPassword(String newPassword) { 
		try {				
			utilityAndroid.clickAndroid(new_password);
			utilityAndroid.clearAndroid(new_password);
			utilityAndroid.enterTextAndroid(new_password, newPassword );	
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// continue with the execution
		}
		return this;
	}
	/**
	 * Description :This function would enter the new password again
	 * @param confirmPassword
	 * @return page object
	 */
	public UsersAccountPageAndroid enterConfirmPassword(String confirmPassword) { 
		try {
			utilityAndroid.clickAndroid(confirm_password);
			utilityAndroid.clearAndroid(confirm_password);
			utilityAndroid.enterTextAndroid(confirm_password, confirmPassword );
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would tap on the security option 
	 * @return page object 
	 */
	public UsersAccountPageAndroid tappingSecurity() {
		try { 
			utilityAndroid.genericWait(edit_info_account, "Visible");
			utilityAndroid.clickAndroid(security); 
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enable passcode
	 * @return page object
	 */
	public UsersAccountPageAndroid enablePasscode(boolean expectedState) {
		try {
			utilityAndroid.genericWait(enable_disable_switch.get(0), "Click");
			String currentState = enable_disable_switch.get(0).getText();
			if ((currentState.equalsIgnoreCase("OFF") && expectedState) || (currentState.equalsIgnoreCase("ON") && !expectedState)) {
				utilityAndroid.clickAndroid(enable_disable_switch.get(0));
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enable launching application
	 * @return page object
	 */
	public UsersAccountPageAndroid enableLaunchingApplication(boolean expectedState) {
		try {
			String currentState = enable_disable_switch.get(2).getText();
			if ((currentState.equalsIgnoreCase("OFF") && expectedState) || (currentState.equalsIgnoreCase("ON") && !expectedState)) {
				utilityAndroid.clickAndroid(enable_disable_switch.get(2));
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enable accessing the account view
	 * @return page object
	 */
	public UsersAccountPageAndroid enableAccessingAccountView(boolean expectedState) {
		try {
			String currentState = enable_disable_switch.get(3).getText();
			if ((currentState.equalsIgnoreCase("OFF") && expectedState) || (currentState.equalsIgnoreCase("ON") && !expectedState)) {
				utilityAndroid.clickAndroid(enable_disable_switch.get(3));
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enable Opening doors or gate
	 * @return page object
	 */
	public UsersAccountPageAndroid enableOpeningDoorsOrGate(boolean expectedState) {
		try {
			String currentState = enable_disable_switch.get(4).getText();
			if ((currentState.equalsIgnoreCase("OFF") && expectedState) || (currentState.equalsIgnoreCase("ON") && !expectedState)) {
				utilityAndroid.clickAndroid(enable_disable_switch.get(4));
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on done button
	 * @return page object
	 */
	public UsersAccountPageAndroid clickDoneButton () {
		try {
			utilityAndroid.clickAndroid(done_btn);
			utilityAndroid.waitForGenericSpinner();
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if enter new passcode screen appears
	 * @return
	 */
	public boolean verifyNewPasscodeScreen() {
		try {
			if(passcode_header.getText().contains("Enter a new passcode to continue.")) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would tap on passcode text
	 * @return Page Object
	 */
	public UsersAccountPageAndroid tapOnPasscode() {
		try {
			utilityAndroid.clickAndroid(passcode_text);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on change passcode
	 * @return Page Object
	 */
	public UsersAccountPageAndroid clickOnChangePasscode() {
		try {
			utilityAndroid.genericWait(change_passcode, "Click");
			utilityAndroid.clickAndroid(change_passcode);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would set the Passcode
	 * @return page object
	 */
	public UsersAccountPageAndroid setPasscode(String passcode) {
		try {
			String text[ ] = passcode.split("");
			utilityAndroid.genericWait(passcode_textbox_1, "Visible");
			for (int i = 0; i < 2; i++) {
				utilityAndroid.genericWait(passcode_textbox_1, "Click");
				utilityAndroid.clickAndroid(passcode_textbox_1);
				utilityAndroid.enterTextAndroid(passcode_textbox_1, text[0] );
				utilityAndroid.enterTextAndroid(passcode_textbox_2, text[1] );
				utilityAndroid.enterTextAndroid(passcode_textbox_3, text[2] );
				utilityAndroid.enterTextAndroid(passcode_textbox_4, text[3] );
			}
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			
		}
		return this;
	}

	/**
	 * Description : This function would enter the Passcode
	 * @return page object
	 */
	public UsersAccountPageAndroid enterPasscode(String passcode) {
		try {
			String text[ ] = passcode.split("");
			utilityAndroid.genericWait(passcode_textbox_1, "Visible");
			utilityAndroid.genericWait(passcode_textbox_1, "Click");
			utilityAndroid.clickAndroid(passcode_textbox_1);
			utilityAndroid.enterTextAndroid(passcode_textbox_1, text[0] );
			utilityAndroid.enterTextAndroid(passcode_textbox_2, text[1] );
			utilityAndroid.enterTextAndroid(passcode_textbox_3, text[2] );
			utilityAndroid.enterTextAndroid(passcode_textbox_4, text[3] );
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// 
		}
		for (int i = 0 ; i < 20; i++) {
			try {
				if(spinner_on_save.isDisplayed() || spinner_on_save.isEnabled()) {
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				break;
			}
		}
		return this;

	}

	/**
	 * Description : This function would verify the account page
	 * @return
	 */
	public boolean verifyAccountPage() {
		try {
			utilityAndroid.genericWait(toolbar_text, "Visible");
		} catch (Exception e) {
			//
		}
		try {
			if (toolbar_text.getText().equalsIgnoreCase("Account")) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify the login greeting text
	 * @return
	 */
	public boolean verifyLoginGreetingText() {
		try {
			if (greeting_text.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify the passcode text
	 * @return
	 */
	public boolean verifyPasscodeText() {
		try {
			if (passcode_header.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would User Name from Account info page
	 * @return String
	 */
	public String getUserNameAccountInfo() {
		String userName ="";
		try {
			if (spinner.isDisplayed()) {
				Thread.sleep(3000);
			}
		} catch (Exception e) {
			//No exception is required to catch.
		}
		try {
			userName = user_name.getText();
		} catch (Exception e) {
			utilityAndroid.captureScreenshot(androidDriver);
		}
		return userName;
	}

	/**
	 * Description : This function would click on delete (remove option)
	 * @return page object
	 */
	public UsersAccountPageAndroid deleteAcceptedInvitation(){
		try {
			utilityAndroid.genericWait(delete_invitation_btn, "Click");
			utilityAndroid.clickAndroid(delete_invitation_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if error pop-up is present on screen or not
	 * @return boolean
	 */
	public boolean verifyErrorPresent() {
		try {
			if(error_text.isDisplayed()) {
				return true;
			}
		} catch(Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would get the text of the error pop-up
	 * @return string
	 */
	public String getErrorText() {
		String tempText = null;
		try {
			tempText = error_text.getText();
			return tempText;
		} catch (Exception e) {
			return tempText;
		}
	}

	/**
	 * Description : This function would click on Ok button
	 * @return page object
	 */
	public UsersAccountPageAndroid clickOnOkButton(){
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			utilityAndroid.clickAndroid(ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}


	/**
	 * Description : This function would click on cancel button
	 * @return page object
	 */
	public UsersAccountPageAndroid clickCancelButton(){
		try {
			utilityAndroid.genericWait(cancel_btn, "Click");
			utilityAndroid.clickAndroid(cancel_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description: To verify if the account page has invitation badge icon or not
	 * @return boolean
	 */
	public boolean verifyBadgeIcon() {
		try { 
			if (badge_icon_text.getText().trim().equalsIgnoreCase("1")) {
				extentTestAndroid.log(Status.PASS, "Badge icon verified for this user account - " +account_email.getText());
				return true;
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		extentTestAndroid.log(Status.FAIL, "Badge icon not verified for this user account - " +account_email.getText());
		return false;
	}
}
