package com.liftmaster.myq.android.pages;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Dimension;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;

public class SignUpPageAndroid {
	private static AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	@AndroidFindBy(id = "login_dont_have_account_text_view")
	public AndroidElement signup_button;

	@AndroidFindBy(id = "fragment_account_create_firstname_edittext")
	public AndroidElement first_name_textbox;

	@AndroidFindBy(id = "fragment_account_create_lastname_edittext")
	public AndroidElement last_name_textbox;

	@AndroidFindBy(id = "fragment_account_create_email_edittext")
	public AndroidElement account_email_textbox;

	@AndroidFindBy(id = "fragment_account_create_password_edittext")
	public AndroidElement create_password_textbox;

	@AndroidFindBy(id = "fragment_account_create_confirm_password_edittext")
	public AndroidElement verify_password_textbox;

	@AndroidFindBy(id = "fragment_account_create_zip_edittext")
	public AndroidElement zip_code_textbox;

	@AndroidFindBy(id = "fragment_account_create_spinner_country")
	public AndroidElement country_spinner;

	@AndroidFindBy(xpath = "//*[@class='' and @index='']")
	public AndroidElement time_zone_value;
	
	@AndroidFindBy(id = "fragment_account_create_spinner_time_zone")
	public AndroidElement time_zone_spinner;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView")
	public List<AndroidElement> country_list;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView")
	public List<AndroidElement> time_zone_list;

	@AndroidFindBy(id = "account_language")
	public AndroidElement language_textbox;

	@AndroidFindBy(id = "submit")
	public AndroidElement submit_button;

	@AndroidFindBy(className = "android.widget.RadioButton")
	public List<AndroidElement> select_language;

	@AndroidFindBy(className = "android.widget.ImageButton")
	public AndroidElement back_button;

	@AndroidFindBy(id = "check_acctcreate_agree")
	public AndroidElement iAgree_radioButton;

	@AndroidFindBy(id = "ok")
	public AndroidElement done_button;

	@AndroidFindBy(id = "android.widget.ProgressBar")
	public AndroidElement spinner_sign_up;

	@AndroidFindBy (id = "android:id/message")
	public AndroidElement error_text;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement ok_button;
	

	Dimension size;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public SignUpPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid){
		SignUpPageAndroid.androidDriver=androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function would click on Sign-up button
	 * @return Page object
	 */
	public SignUpPageAndroid clickSignUpButton() {
		try {
			utilityAndroid.genericWait(signup_button, "Visible");
			utilityAndroid.clickAndroid(signup_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enter the first name into the text box
	 * @param firstname
	 * @return Page object
	 */
	public SignUpPageAndroid enterFirstName(String firstName) {
		try {
			utilityAndroid.enterTextAndroid(first_name_textbox,firstName);
		} catch(Exception e) {
		 	utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would enter the last name into the text box
	 * @param lastname
	 * @return Page object
	 */
	public SignUpPageAndroid enterLastName(String lastName) {
		try {
			utilityAndroid.enterTextAndroid(last_name_textbox,lastName);
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would enter the Account Email ID into the text box
	 * @param firstName
	 * @param lastName
	 * @return Page object
	 */
	public String enterAccountEmail(String firstName, String lastName) {
		String email = null;
		try {
			//below code to generate a random number. This number would be appended to the email after last name 
			//to form a unique email id
			Random randomGenerator = new Random();
			int number = randomGenerator.nextInt(10000);  
			email = firstName+lastName+number+"@gmail.com";
			utilityAndroid.enterTextAndroid(account_email_textbox,email);
		}catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return email;
	}

	/**
	 * Description : This function would enter the Account Email ID into the text box
	 * @param email
	 * @return Page object
	 */
	public SignUpPageAndroid enterEmail(String email) {
		try {
			utilityAndroid.enterTextAndroid(account_email_textbox,email);
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would enter the Create Password into the text box
	 * @param password
	 * @return Page object
	 */
	public SignUpPageAndroid enterCreatePassword(String password) {
		try {
			utilityAndroid.enterTextAndroid(create_password_textbox,password);
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would enter the Verify Password into the text box
	 * @param password
	 * @return Page object
	 */
	public SignUpPageAndroid enterVerifyPassword(String password) {
		try{
			utilityAndroid.enterTextAndroid(verify_password_textbox,password);
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would enter the zipCode into the text box
	 * @param zipCode
	 * @return Page object
	 */
	public SignUpPageAndroid enterZipCode(String zipCode) {
		try {
			utilityAndroid.enterTextAndroid(zip_code_textbox,zipCode);
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function click on submit button
	 * @return Page object
	 */
	public SignUpPageAndroid clickSubmitButton() {
		try {
			utilityAndroid.genericWait(submit_button ,"button");
			utilityAndroid.clickAndroid(submit_button);
			waitForSpinner();
		}catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the country
	 * @param country
	 * @return Page object
	 * @throws Exception
	 */
	public SignUpPageAndroid selectCountry(String country, String udid) throws InterruptedException {
		try {
			String currentCountryName;
			String tempText = "";
			int count = country_list.size();
			for(int i = 0 ; i< count; i++ ) {
				currentCountryName = country_list.get(i).getText();
				if(currentCountryName.equalsIgnoreCase(country)) {
					utilityAndroid.clickAndroid(country_list.get(i));
					break;
				} else if(i == count-1){
					if(tempText.equals(currentCountryName)){
						break;
					} else {
						tempText = currentCountryName;
					}
					Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 300 1200 300 400");
					Thread.sleep(250);
					count = country_list.size();
					 i = -1;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select time zone
	 * @param timeZone
	 * @return Page object
	 * @throws InterruptedException
	 */
	public SignUpPageAndroid selectTimeZone(String timeZone) throws InterruptedException {
		try{
			utilityAndroid.clickAndroid(time_zone_spinner);
			utilityAndroid.clearAndroid(time_zone_value);
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;			   
	}

	/**
	 * Description : This function would select language
	 * @param language
	 * @return Page object
	 */
	public SignUpPageAndroid selectLanguage(String language) {
		try {
			utilityAndroid.clickAndroid(language_textbox);
			for(AndroidElement element : select_language) {
				if(element.getText().equalsIgnoreCase(language)) {
					utilityAndroid.clickAndroid(element);
					break;
				}
			}
			utilityAndroid.clickAndroid(back_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on done button
	 * @return Page object
	 */
	public SignUpPageAndroid clickDoneButton() {
		try {
			utilityAndroid.genericWait(done_button, "Visible");
			utilityAndroid.clickAndroid(done_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on back button
	 * @return Page object
	 */
	public SignUpPageAndroid clickBackButton() {
		try {
			utilityAndroid.clickAndroid(back_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would wait for spinner 
	 * @return
	 */
	public SignUpPageAndroid waitForSpinner() {
		for(int iIndex = 0; iIndex < 20 ; iIndex++) {
			try {
				if(spinner_sign_up.isDisplayed()) {
					Thread.sleep(2000);
				}
			} catch (Exception e) {
				break;
			}
		}
		return this;
	}

	/**
	 * Description : This function would verify the errors 
	 * @return
	 */
	public boolean verifyErrorPresent() {
		String emptyFields = "All fields must be completed before continuing.";
		String passwordMatch = "Both passwords must match.";
		String alreadyExists = "That username already exists. Check username or use another username. (201)";
		String validEmail = "Please enter a valid email address.";
		String weakPassword = "Passwords need to be at least 8 characters in length and must contain at least 3 of the following 4 types of characters: uppercase letter, lowercase letter, number or symbol.";
		String termsOfUse = "You must accept the Terms of Use before continuing.";
		try {
			if(error_text.getText().trim().equalsIgnoreCase(emptyFields)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(passwordMatch)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(alreadyExists)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}  else if (error_text.getText().trim().equalsIgnoreCase(validEmail)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}  else if (error_text.getText().trim().equalsIgnoreCase(weakPassword)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			} else if (error_text.getText().trim().equalsIgnoreCase(termsOfUse)) {
				extentTestAndroid.log(Status.INFO, error_text.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}  else {
				return false;
			}
		} catch(Exception e) {
			return false;
		}
	}
	
	/**
	 * Description : This function would verify if error pop-up is present on screen or not
	 * @return boolean
	 */
	public boolean verifyErrorPopupPresent() {
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			if(error_text.isDisplayed()) {
				return true;
			}
		} catch(Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would get the text of the error pop-up
	 * @return string
	 */
	public String getErrorText() {
		String tempText = null;
		try {
			tempText = error_text.getText();
			return tempText;
		} catch (Exception e) {
			return tempText;
		}
	}
	
	/**
	 * Description : This function would click on Ok button
	 * @return page object
	 */
	public SignUpPageAndroid clickOnOkButton(){
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			utilityAndroid.clickAndroid(ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function would click on Choose Country text to display the drop down
	 * @return
	 */
	public SignUpPageAndroid tapOnChooseCountry() {
		try {
			utilityAndroid.genericWait(country_spinner, "Click");
			utilityAndroid.clickAndroid(country_spinner);
		} catch (Exception e) {
		}
		return this;
	}
}
