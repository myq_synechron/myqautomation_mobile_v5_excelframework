package com.liftmaster.myq.android.pages;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SchedulePageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	int iIndex = 0;

	//Page factory for Schedule Page Android
	@AndroidFindBy(id = "create_new_schedule")
	public AndroidElement create_new_schedule;

	@AndroidFindBy(id = "add_new")
	public AndroidElement add_schedule_link;

	@AndroidFindBy(id = "text_emptymsg_text")
	public AndroidElement no_schedule_text;

	@AndroidFindBy(id = "event_name")
	public AndroidElement schedule_name_text_box;

	@AndroidFindBy(id = "set_device_icon")
	public AndroidElement set_device_icon;

	@AndroidFindBy(id = "at_this_time_row")
	public AndroidElement at_this_time;

	@AndroidFindBy(id = "on_these_days_row")
	public AndroidElement on_these_days;

	@AndroidFindBy(id = "switch_pushNotification")
	public AndroidElement toggle_push_notification;

	@AndroidFindBy(id = "switch_email")
	public AndroidElement toggle_email_notification;

	@AndroidFindBy(id = "save")
	public AndroidElement save_button;

	@AndroidFindBy(id = "text_states_device_name")
	public List<AndroidElement> device_name_list;

	@AndroidFindBy(xpath = "//android.widget.ListView")
	public AndroidElement list_of_devices;

	@AndroidFindBy(id = "check_states_device")
	public List<AndroidElement> device_checkbox;

	//this is for on/off button for lights
	@AndroidFindBy(id = "switch_button")
	public List<AndroidElement> state_switch_button;

	@AndroidFindBy(id = "toggle_states_button")
	public AndroidElement toggle_states_button;

	@AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton' and @content-desc='Navigate up']" )
	public AndroidElement back_button;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement back_confirmation_btn;

	@AndroidFindBy(id = "item_title")
	public List<AndroidElement> schedule_list;

	@AndroidFindBy(className = "android.widget.RadialTimePickerView$RadialPickerTouchHelper")
	public List <AndroidElement> radial_time_picker;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='0']")
	public AndroidElement zeroth_minute;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='30']")
	public AndroidElement  thirtyth_minute;

	@AndroidFindBy(id = "text_timepicker_ok_button")
	public AndroidElement clock_ok_button;

	@AndroidFindBy(id = "android:id/button2")
	public AndroidElement days_done_button;

	@AndroidFindBy(className = "android.widget.CheckBox")
	public List<AndroidElement> days_of_week;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='0']")
	public AndroidElement zeroth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='12']")
	public AndroidElement twelfth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='6']")
	public AndroidElement sixth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='13']")
	public AndroidElement thirteenth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='18']")
	public AndroidElement eightenth_hour;

	//	@AndroidFindBy(id = "com.android.systemui:id/clear_button")
	@AndroidFindBy(id = "com.android.systemui:id/dismiss_text") 
	public AndroidElement clear_notification_panel;

	@AndroidFindBy(className = "android.widget.TextView")
	public List<AndroidElement> notification_text;

	@AndroidFindBy(id = "switch_button")
	public List<AndroidElement> enable_disable_schedule;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Delete']")
	public AndroidElement delete_schedule;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement delete_confirmation;

	@AndroidFindBy(id = "android:id/alertTitle")
	public AndroidElement schedule_error_heading;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement schedule_error_text;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement error_ok_button;

	@AndroidFindBy(id = "android:id/button2")
	public AndroidElement later_btn;

	@AndroidFindBy(id = "android:id/hours")
	public AndroidElement selected_hours;

	@AndroidFindBy(id = "android:id/minutes")
	public AndroidElement selected_minutes;

	@AndroidFindBy(id = "Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "image_toolbar_nav_icon")
	public AndroidElement hamburger_icon;

	@AndroidFindBy(className = "android.widget.ListView")
	public AndroidElement list_of_schedule;

	String noDeviceErrHeading = "No Device Error";
	String noDeviceErrText = "At least one device must be selected.";
	String noScheduleNameheading = "No Schedule Name";
	String noScheduleNameText = "Schedule Name Required";
	String noDaySelectedHeading = "No Day Selected";
	String noDaySelectedText = "At least one day must be selected.";

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public SchedulePageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid =  new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function would click on "+" icon to create schedule
	 * @return Page object
	 */
	public SchedulePageAndroid addSchedule() {
		try {
			utilityAndroid.clickAndroid(create_new_schedule);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify no schedule text
	 * @return true / false
	 */
	public boolean verifyNoScheduleText() {
		try {
			if (no_schedule_text.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would clear the schedule name text box
	 * @return Page object
	 */
	public SchedulePageAndroid clearScheduleName() {
		try {
			utilityAndroid.clearAndroid(schedule_name_text_box);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enter the schedule name
	 * @param scheduleName
	 * @return Page object
	 */
	public SchedulePageAndroid setScheduleName(String scheduleName) {
		try {
			utilityAndroid.genericWait(schedule_name_text_box, "Click");
			utilityAndroid.enterTextAndroid(schedule_name_text_box, scheduleName);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			//catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would "+" icon for devices
	 * @return Page object
	 */
	public SchedulePageAndroid clickOnSetDevicesIcon() {
		try {
			utilityAndroid.clickAndroid(set_device_icon);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on time 
	 * @return Page object
	 */
	public SchedulePageAndroid clickOnTime() {
		try {
			utilityAndroid.clickAndroid(at_this_time);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 *  Description : This function would click on OK button on Clock
	 *  @return Page object
	 */
	public SchedulePageAndroid clickClockOkButton() {
		try {
			utilityAndroid.clickAndroid(clock_ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Days
	 * @return Page object
	 */
	public SchedulePageAndroid clickOnDays() {
		try {
			utilityAndroid.clickAndroid(on_these_days);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Done button after selecting days
	 * @return Page object
	 */
	public SchedulePageAndroid clickDaysDoneButton() {
		try {
			utilityAndroid.clickAndroid(days_done_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would set the toggle for push notification
	 * @return Page object
	 */
	public SchedulePageAndroid selectPushNotification() {
		try {
			utilityAndroid.clickAndroid(toggle_push_notification);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would set the toggle for email notification
	 * @return Page object
	 */
	public SchedulePageAndroid selectEmailNotification() {
		try {
			utilityAndroid.clickAndroid(toggle_email_notification);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on save button
	 * @return Page object
	 */
	public SchedulePageAndroid clickSaveButton() {
		try {
			utilityAndroid.clickAndroid(save_button);
			for (int i =0 ; i<25; i++) {
				try {
					if(schedule_error_text.isDisplayed() && schedule_error_text.getText().toLowerCase().contains("saving")){
						Thread.sleep(750);
					}
				} catch (Exception e) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;

	}

	/**
	 * Description : This function would click on back button (to navigate to previous screen)
	 * @return Page object
	 */
	public SchedulePageAndroid clickBackButton() {
		try {
			utilityAndroid.clickAndroid(back_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Back confirmation (Yes) button
	 * @return Page object
	 */
	public SchedulePageAndroid clickBackConfirmation() {
		try {
			utilityAndroid.clickAndroid(back_confirmation_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if the schedule is existing or not
	 * @param scheduleName
	 * @return schedulePresent
	 */
	public boolean verifySchedule(String scheduleName) {
		for(int i = 0; i< 25; i++) {
			try {
				if(spinner.isDisplayed() || spinner.isEnabled()){
					Thread.sleep(700);
				}
			} catch (Exception e) {
				break;
			}
		}
		try {
			utilityAndroid.genericWait(create_new_schedule, "Click");
			if(verifyNoScheduleText()) {
				return false;
			} else {
				int totalNoOfSchedule = list_of_schedule.findElementsByClassName("android.widget.RelativeLayout").size();
				for (int i = 0 ; i < totalNoOfSchedule ; i++) {
					String scheduleText = list_of_schedule.findElementsByClassName("android.widget.RelativeLayout").get(i).findElementsByClassName("android.widget.TextView").get(0).getText();
					if(scheduleText.equalsIgnoreCase(scheduleName)) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 *  Description : This function would set the time based on the current time
	 * @param duration
	 * @return minutes
	 */
	public String setTime(int duration) {
		String time = null ;
		int hours;
		int minutes = 0 ;
		try{
			//get hours and minutes separately
			hours = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[0]);
			minutes = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[1]) + duration;
			if (minutes == 59 || minutes == 60 ) {
				hours=+1;
				minutes = 0;
			} else if(minutes > 60) {
				hours=+1;
				minutes = minutes - 60;
			}
			if (hours >= 1 && hours <= 12) {
				utilityAndroid.selectTimeOnClock(sixth_hour, twelfth_hour , hours , "hours");
			} else if (hours >= 13 && hours <= 24) {
				if(hours == 24) {
					utilityAndroid.clickAndroid(zeroth_hour);
				} else {
					utilityAndroid.selectTimeOnClock(eightenth_hour , zeroth_hour , hours , "hours");
				}
			}
			utilityAndroid.selectTimeOnClock(zeroth_minute, thirtyth_minute, minutes , "minutes");
			time = selected_hours.getText().concat(":").concat(selected_minutes.getText());
			clickClockOkButton();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return time;
	}

	/**
	 * Description : This function would de-select the current day (would skip if it is already de-selected)
	 * @return Page object
	 */
	public SchedulePageAndroid deselectCurrentDay() {
		String currentDay ;
		currentDay = utilityAndroid.getCurrentDay();
		try {
			// below code will perform de-selection for current day
			for (AndroidElement element : days_of_week) {
				if (element.getText().equals(currentDay)) {
					if (element.getAttribute("checked").equals("true")) {
						utilityAndroid.clickAndroid(element);
					} 
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select / de-select the entire week 
	 * @param selectDeselect
	 * @return
	 */
	public SchedulePageAndroid selectDeselectWeek(boolean selectDeselect) {
		try {
			// below code will perform selection or de-selection of entire week
			//if boolean value is true , then select the entire week else deselect
			if (selectDeselect) {
				for (AndroidElement element : days_of_week) {
					if (element.getAttribute("checked").equals("true")) {
					} else {
						utilityAndroid.clickAndroid(element);
					}
				}
			} else {

				for (AndroidElement element : days_of_week) {
					if (element.getAttribute("checked").equals("true")) {
						utilityAndroid.clickAndroid(element);
					} 
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the device and set the state to ON/OFF/CLOSE
	 * @param deviceName
	 * @param expectedState
	 * @return Page object
	 */
	public SchedulePageAndroid selectDeviceAndState(String deviceName , String expectedState, String udid ) {
		boolean deviceFound = false;
		String nameOfDevice = "";
		String tempString = "";
		int countOfDevices = 0;
		int matchCounter = 0;
		try {
			//size of entire elements present on the screen
			countOfDevices = list_of_devices.findElementsByXPath(".//*").size();
			//counter is started from 1 , as at 0th index ListView is present which is a container of all the elements
			for (int i = 1 ; i < countOfDevices; i++) {
				if(list_of_devices.findElementsByXPath(".//*").get(i+2).getAttribute("className").equalsIgnoreCase("android.widget.CheckBox")) {
					//get the name of the device
					nameOfDevice = list_of_devices.findElementsByXPath(".//*").get(i+3).getText();
					//compare the device name 
					if(nameOfDevice.equalsIgnoreCase(deviceName)) {
						deviceFound = true;
						//select the check box for deviceName
						utilityAndroid.clickAndroid(list_of_devices.findElementsByXPath(".//*").get(i+2));
						//select the state		
						if(list_of_devices.findElementsByXPath(".//*").get(i+6).isEnabled()) {
							String currentState = list_of_devices.findElementsByXPath(".//*").get(i+6).getText();
							if((currentState.equalsIgnoreCase("ON") && expectedState.equalsIgnoreCase("OFF")) || (currentState.equalsIgnoreCase("OFF") && expectedState.equalsIgnoreCase("ON"))) {
								utilityAndroid.clickAndroid(list_of_devices.findElementsByXPath(".//*").get(i+6));
								break;
							} else if (currentState.toUpperCase().equalsIgnoreCase("CLOSE") && expectedState.toUpperCase().equalsIgnoreCase("CLOSE") 
									|| currentState.toUpperCase().equalsIgnoreCase("CLOSE") && expectedState.toUpperCase().equals("CLOSED")  
									|| currentState.equalsIgnoreCase(expectedState)) {
								break;
							} else {
								utilityAndroid.clickAndroid(list_of_devices.findElementsByXPath(".//*").get(i+2));
								extentTestAndroid.log(Status.INFO, "Not a valid state to select");
								clickBackButton();
							}
						}  
						break;
					} else {
						//if the device name does not matches, skip the rest of the elements in the layout and increase the counter by 6 times
						i = i + 6;
						//if the counter is 6 less that the device count and device is not found , swipe on screen and reset the loop counter
						if(i>=(countOfDevices-6) && !deviceFound) {
							//if after swipe the last device name remains same for 2 times , exit the loop
							if(nameOfDevice.equals(tempString)) {
								matchCounter = matchCounter +1;
							} else {
								tempString = nameOfDevice;
							} 
							if(matchCounter == 2) {
								break;
							}
							//below is to swipe to the screen
							Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 500 1000 180 180 ");
							Thread.sleep(1000);
							//reset the counter after swipe to start from beginning
							i = 7;
							//take the size again
							countOfDevices = list_of_devices.findElementsByXPath(".//*").size();
						}
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would set the state for push notification toggle
	 * @param expectedState
	 * @return Page object
	 */
	public SchedulePageAndroid setToggleNotification(boolean expectedState) {
		String currentState;
		try {
			currentState = toggle_push_notification.getText();
			if ((expectedState && currentState.equalsIgnoreCase("OFF")) || 	(!expectedState && currentState.equalsIgnoreCase("ON"))) {
				utilityAndroid.clickAndroid(toggle_push_notification);
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would create the schedule based on the parameter passed
	 * @param schedulefordevice
	 * @param deviceAndState
	 * @param duration
	 * @param notificationExpectedState
	 * @return timeSelected
	 */
	public String createSchedule(String schedulefordevice, HashMap<String,String> deviceAndState, int duration , String days , boolean notificationExpectedState , String udid) {
		String timeSelected = null ;
		try {
			//enter the schedule name
			setScheduleName(schedulefordevice);
			//select the device for which schedule is to be created
			clickOnSetDevicesIcon();

			Set hashSet = deviceAndState.entrySet();
			Iterator iterator = hashSet.iterator();
			while (iterator.hasNext()) {
				Map.Entry hashEntry = (Map.Entry) iterator.next();
				String deviceName = (String) hashEntry.getKey();
				String expectedState = (String) hashEntry.getValue();
				selectDeviceAndState(deviceName , expectedState, udid);
			}
			clickBackButton();
			//select the time 
			clickOnTime();
			timeSelected = setTime(duration);
			//if today is passed as parameter, below code would de-select the today's day 
			if (days.equalsIgnoreCase("today")) {
				clickOnDays();
				deselectCurrentDay();
				clickDaysDoneButton(); 
			}
			//set the push notification as true
			setToggleNotification(notificationExpectedState);
			//click on save button
			clickSaveButton();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return timeSelected;
	}

	/**
	 * Description : This function would verify the schedule notification for specific schedule
	 * @param scheduleName
	 * @return notificationReceived
	 */
	public boolean checkPushNotificationReceived(String scheduleName, String udid) {
		boolean notificationReceived = false;
		try {
			Thread.sleep(10000);
			utilityAndroid.openNotificationPanel();
			for (iIndex = 0; iIndex < notification_text.size(); iIndex++) {
				//verify the specific text and the schedule name in the notification panel
				if (notification_text.get(iIndex).getText().contains("MyQ Alert:") && notification_text.get(iIndex).getText().toUpperCase().contains(scheduleName.toUpperCase())) {
					extentTestAndroid.log(Status.INFO, notification_text.get(iIndex).getText() + " : " +  notification_text.get(iIndex+1).getText());
					notificationReceived = true;
					break;
				}
			}
			if(!notificationReceived) {
				utilityAndroid.captureScreenshot(androidDriver);
			}
			//close the notification panel
			utilityAndroid.closeNotificationPanel(udid);
			return notificationReceived;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return notificationReceived;
		}
	}

	/**
	 * Description : This function would wait for till schedule notification appears
	 * @param timeSelected
	 * @return Page object
	 * @throws Exception
	 */
	public SchedulePageAndroid waitForNotification(String timeSelected) throws Exception {
		int currentMinute = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[1]);
		int selectedMinute = Integer.parseInt(timeSelected.split(":") [1]);
		int timeDiffMinute = 0;
		if (currentMinute == 59) {
			currentMinute = 0;
			timeDiffMinute = ( selectedMinute - currentMinute ) + 1;
		} else if (selectedMinute == 0) {
			timeDiffMinute = 60 - currentMinute;
		} else {
			timeDiffMinute = selectedMinute - currentMinute;
		}
		do {
			try {
				currentMinute = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[1]);
				if (selectedMinute == currentMinute) {
					break;
				} else {
					iIndex++;
					Thread.sleep(10000);
				}
				//this command is written so as to avoid timeout of appium server
				androidDriver.currentActivity();
			}
			catch (Exception e) {
				iIndex++;
			}
		} while ( iIndex < timeDiffMinute * 12);
		return this;
	}

	/**
	 * Description : This function would click on specific schedule
	 * @param scheduleName
	 * @return Page object
	 */
	public SchedulePageAndroid tapOnSchedule(String scheduleName) {
		try {
			for (AndroidElement element : schedule_list) {
				if (element.getText().trim().equalsIgnoreCase(scheduleName)) {
					utilityAndroid.clickAndroid(element);
					break;
				} 
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would delete the specific schedule
	 * @param scheduleName
	 * @return Page object
	 */
	public SchedulePageAndroid deleteSchedule(String scheduleName) {
		try {
			for (AndroidElement element : schedule_list) {
				if (element.getText().equalsIgnoreCase(scheduleName)) {
					utilityAndroid.longPress(element);
					utilityAndroid.clickAndroid(delete_schedule);
					utilityAndroid.clickAndroid(delete_confirmation);
					break;
				} 
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enable or disable the specific schedule
	 * @param scheduleName
	 */
	public boolean disableSchedule(String scheduleName, String sechduleExpectedState ) {
		boolean enableDisbaleSchedule = false;
		String currentState = "null";
		int iCounter = 0;
		try {
			for (iIndex = 0; iIndex < schedule_list.size(); iIndex++) {
				if (schedule_list.get(iIndex).getText().trim().equalsIgnoreCase(scheduleName)) {
					currentState = enable_disable_schedule.get(iCounter).getText();
					if ((sechduleExpectedState.equalsIgnoreCase("Enabled") && currentState.equalsIgnoreCase("OFF")) 
							|| (sechduleExpectedState.equalsIgnoreCase("Disabled") && currentState.equalsIgnoreCase("ON")) ) {

						utilityAndroid.clickAndroid(enable_disable_schedule.get(iCounter));
						currentState = enable_disable_schedule.get(iCounter).getText();

						if ((sechduleExpectedState.equalsIgnoreCase("Disabled") && currentState.equalsIgnoreCase("OFF")) 
								|| (sechduleExpectedState.equalsIgnoreCase("Enabled") && currentState.equalsIgnoreCase("ON")) ) {
							enableDisbaleSchedule = true;
						} 
					} else {
						enableDisbaleSchedule = true;
					}
					break;
				} else {
					iCounter++;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return enableDisbaleSchedule;
	}

	/**
	 * Description : This function would verify the error on screen 
	 * @return boolean
	 */
	public boolean verifyError() {
		try {
			if (schedule_error_heading.getText().equals(noScheduleNameheading) && schedule_error_text.getText().equals(noScheduleNameText)) {
				extentTestAndroid.log(Status.INFO, "Please enter a name for this schedule.");
				utilityAndroid.clickAndroid(error_ok_button);
				return true;
			} else if (schedule_error_heading.getText().equals(noDaySelectedHeading) && schedule_error_text.getText().equals(noDaySelectedText)) {
				extentTestAndroid.log(Status.INFO, "At least one day must be selected.");
				utilityAndroid.clickAndroid(error_ok_button);
				return true;
			}
			else if (schedule_error_heading.getText().equals(noDeviceErrHeading) && schedule_error_text.getText().equals(noDeviceErrText)) {
				extentTestAndroid.log(Status.INFO, "At least one device must be selected.");
				utilityAndroid.clickAndroid(error_ok_button);
				return true;
			} else {
				extentTestAndroid.log(Status.INFO, schedule_error_heading.getText() + " : " + schedule_error_text.getText());
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function would remove all the selected device from schedule
	 * @return Page object
	 */
	public SchedulePageAndroid removeAllSelectedDevices() {
		try {
			//this is to un-check all the checked boxes for the device
			for (AndroidElement element : device_name_list) {
				if (element.getAttribute("checked").equalsIgnoreCase("true")) {
					utilityAndroid.clickAndroid(element);
				}
			}
			clickBackButton();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would check if the error is displayed or not
	 * @return boolean 
	 */
	public boolean verifyErrorDispalyed () {
		try {
			if(schedule_error_text.isDisplayed() || schedule_error_text.isEnabled()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would get the text of the error pop-up
	 * @return String 
	 */
	public String getErrorMsg() {
		String tempText = null;
		try {
			tempText = schedule_error_text.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return tempText;
	}

	/**
	 * Description : This function would click on ok button displayed on the page
	 * @return Page object
	 */
	public SchedulePageAndroid clickOkButton () {
		try {
			utilityAndroid.clickAndroid(error_ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Later button displayed on the page
	 * @return Page object
	 */
	public SchedulePageAndroid clickLaterButton () {
		try {
			utilityAndroid.clickAndroid(later_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would check for the device on Device and State page 
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyDevicePresentForSchedule(String deviceName) {
		int countOfDevices;
		String nameOfDevice;
		boolean deviceFound = false;
		try {
			//size of entire elements present on the screen
			countOfDevices = list_of_devices.findElementsByXPath(".//*").size();
			//counter is started from 1 , as at 0th index ListView is present which is a container of all the elements
			for (int i = 1 ; i < countOfDevices; i++) {
				if(list_of_devices.findElementsByXPath(".//*").get(i+2).getAttribute("className").equalsIgnoreCase("android.widget.CheckBox")) {
					//get the name of the device
					nameOfDevice = list_of_devices.findElementsByXPath(".//*").get(i+3).getText();
					//compare the device name 
					if(nameOfDevice.equalsIgnoreCase(deviceName)) {
						deviceFound = true;
					} else {
						deviceFound = false;
					} 
				}
			}
		} catch (Exception e) {
			return deviceFound;
		}
		return deviceFound;
	}
}
