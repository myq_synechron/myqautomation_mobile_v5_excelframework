package com.liftmaster.myq.android.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AlertPageAndroid  {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	String currentState;
	String alertErrText;
	String alertState;
	String noNotificationChosenErr = "Please choose at least one method by which to be notified.";
	String noRuleActionChosenErr = "Please turn on at least one action to trigger this alert.";
	String noAlertName = "Please enter a name for this alert.";
	String noNotificationErrHeading = "No Notifications Chosen";
	String noActionErrHeading = "No Rule Action Chosen";
	String noNameErrHeading = "No Alert Name";
	String noAlertText = "You have no alerts on your account";
	int deviceCount;
	int alertCount = 0;
	int iIndex = 0 ;
	int jIndex = 0;
	int iCounter = 0;
	int alertIndex = 0;
	int hours ;
	int min;
	int minReminder = 0;
	boolean alertPresent = false;
	boolean alertDeleted= false;
	boolean noAlertLink = false;
	boolean alertErrorPresent = false;
	boolean disableAlert = false;
	boolean renameAlert = false;
	boolean tapalert = false;
	boolean elementDisplayed = false;

	//Page factory for Alert Page Android
	@AndroidFindBy(id = "text_emptymsg_text")
	public AndroidElement no_alert_text;

	@AndroidFindBy(id = "add_new")
	public AndroidElement add_alert_link;

	@AndroidFindBy(id = "create_new_rule")
	public AndroidElement add_alert_btn;

	@AndroidFindBy(id = "EditRuleFragment_EditText_RuleName")
	public AndroidElement set_alert_name;

	@AndroidFindBy(id = "EditRule_Toggle_AlertOnOpen")
	public AndroidElement alert_on_open_toggle;

	@AndroidFindBy(id = "EditRule_Toggle_AlertOffClosed")
	public AndroidElement alert_off_closed_toggle;

	@AndroidFindBy(id = "EditRule_Toggle_AlertInstantly")
	public AndroidElement as_soon_as_it_happens_toggle;

	@AndroidFindBy(id = "EditRule_Toggle_AlertAlways")
	public AndroidElement all_times_and_days_toggle;

	@AndroidFindBy(id = "EditRule_Toggle_SendPush")
	public AndroidElement push_notification_toggle;

	@AndroidFindBy(id = "save")
	public AndroidElement save_alert_btn;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Delete']")
	public AndroidElement delete_alert_btn;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement delete_confirmation_btn;

	@AndroidFindBy(id = "list_label")
	public List<AndroidElement> alert_device_list;
	
	@AndroidFindBy(xpath = "//android.widget.ListView")
	public AndroidElement device_list;

	@AndroidFindBy(className = "android.widget.TextView")
	public List<AndroidElement> alert_list;

	@AndroidFindBy(id = "android:id/button3")
	public AndroidElement alert_error_ok_button;

	@AndroidFindBy(id = "android:id/button2")
	public AndroidElement later_btn;

	@AndroidFindBy(id = "RulesList_Toggle_Enabled")
	public List<AndroidElement> alert_enabled_toggle_list;

	@AndroidFindBy(className = "android.widget.ImageButton")
	public AndroidElement back_button;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement back_confirmation_btn;

	@AndroidFindBy(className = "android.widget.RadialTimePickerView$RadialPickerTouchHelper")
	public List <AndroidElement> radial_time_picker;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='0']")
	public AndroidElement zeroth_minute;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='30']")
	public AndroidElement thirtyth_minute;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='0']")
	public AndroidElement zeroth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='12']")
	public AndroidElement twelfth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='6']")
	public AndroidElement sixth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='13']")
	public AndroidElement thirteenth_hour;

	@AndroidFindBy(xpath = "//*[@class='android.widget.RadialTimePickerView$RadialPickerTouchHelper' and @content-desc='18']")
	public AndroidElement eightenth_hour;

	@AndroidFindBy(id = "android:id/hours")
	public AndroidElement hours_text;

	@AndroidFindBy(id = "android:id/minutes")
	public AndroidElement minute_text;

	@AndroidFindBy(id = "longer_than_duration_row")
	public AndroidElement for_longer_than_duration;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement clock_ok_btn;

	@AndroidFindBy(id = "EditRule_FromTimeRow")
	public AndroidElement from_time;

	@AndroidFindBy(id = "EditRule_ToTimeRow")
	public AndroidElement to_time;

	@AndroidFindBy(id = "on_these_days_row")
	public AndroidElement on_these_days;

	@AndroidFindBy(id = "com.android.systemui:id/dismiss_text") // Specific to the Google Pixel as of now
	//@AndroidFindBy(id = "com.android.systemui:id/clear_button")
	public AndroidElement clear_notification_panel;

	@AndroidFindBy(className = "android.widget.TextView")
	public List<AndroidElement> notification_text;

	@AndroidFindBy(className = "android.widget.CheckBox")
	public List<AndroidElement> days_of_week;

	@AndroidFindBy(id = "android:id/button2")
	public AndroidElement days_done_btn;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement alert_error_message;

	@AndroidFindBy(id = "android:id/alertTitle")
	public AndroidElement alert_error_heading;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='No Alert Name']")
	public AndroidElement no_alert_name_error_heading;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Please enter a name for this alert.']")
	public AndroidElement no_alert_name_error_text;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='No Notifications Chosen']")
	public AndroidElement no_notification_error_heading;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Please choose at least one method by which to be notified.']")
	public AndroidElement no_notification_error_text;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='No Rule Action Chosen']")
	public AndroidElement no_rule_action_error_heading;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Please turn on at least one action to trigger this alert.']")
	public AndroidElement no_rule_action_error_text;

	@AndroidFindBy(id = "Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "image_toolbar_nav_icon")
	public AndroidElement hamburger_icon;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 * @return Page object
	 */
	public AlertPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);		
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}
	
	/**
	 * Description : This function would verify no alert text
	 * @return true / false
	 */
	public boolean verifyNoAlertText() {
		try {
			if (no_alert_text.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would click on Back button  
	 * @return Page object
	 */
	public AlertPageAndroid clickBackButton() {
		try {
			utilityAndroid.clickAndroid(back_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Back confirmation (Yes) button
	 * @return Page object
	 */
	public AlertPageAndroid clickBackConfirmation() {
		try {
			utilityAndroid.clickAndroid(back_confirmation_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on  On Theses Days  
	 * @return Page object
	 */
	public AlertPageAndroid clickOnTheseDays() {
		elementDisplayed = false;
		try {
			if (on_these_days.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			elementDisplayed = false;
		} 
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(on_these_days);
			}
			utilityAndroid.clickAndroid(on_these_days);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Done button on Days of the week pop-up screen 
	 * @return Page object
	 */
	public AlertPageAndroid clickDaysDoneBtn() {
		try {
			utilityAndroid.clickAndroid(days_done_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on field below for longer than
	 * @return Page object 
	 */
	public AlertPageAndroid clickForLongerThan() {
		try {
			utilityAndroid.clickAndroid(for_longer_than_duration);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on From, to select custom time
	 * @return Page object 
	 */
	public AlertPageAndroid clickFromTime() {
		try {
			utilityAndroid.clickAndroid(from_time);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on To, to select custom time 
	 * @return Page object
	 */
	public AlertPageAndroid clickToTime() {
		boolean toTimeDisplayed = false;
		try {
			if(to_time.isDisplayed()) {
				toTimeDisplayed = true;
			}
		} catch (Exception e) {
			toTimeDisplayed = false;
		}
		try {
			if(!toTimeDisplayed) {
				utilityAndroid.scrollToElement(to_time);
			}
			utilityAndroid.clickAndroid(to_time);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Ok button present on clock
	 * @return Page object
	 */
	public AlertPageAndroid clickClockOkButtton() {
		try {
			utilityAndroid.clickAndroid(clock_ok_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the device as per the name passed in parameter
	 * @param deviceName
	 * @return Page object
	 * @throws Exception
	 */
	public boolean verifyAndSelectDevice(String deviceName, String udid) throws Exception {
		boolean devicePresent = false;
		int deviceCount = 0 ;
		int matchCounter = 0;
		String nameOfDevice = "";
		String tempString = "";
		try {
			deviceCount = device_list.findElementsByClassName("android.widget.RelativeLayout").size();
			for(int i = 0 ; i < deviceCount ; i ++) {
				nameOfDevice = device_list.findElementsByClassName("android.widget.RelativeLayout").get(i).findElementsByClassName("android.widget.TextView").get(0).getText();
				if(nameOfDevice.equalsIgnoreCase(deviceName)) {
					devicePresent = true;
					utilityAndroid.clickAndroid(device_list.findElementsByClassName("android.widget.RelativeLayout").get(i).findElementsByClassName("android.widget.TextView").get(0));
					break;
				} else {
					if(i == (deviceCount-1) && !devicePresent) {
						//if after swipe the last device name remains same for 2 times , exit the loop
						if(nameOfDevice.equals(tempString)) {
							matchCounter = matchCounter +1;
						} else {
							tempString = nameOfDevice;
						} 
						if(matchCounter == 2) {
							break;
						}
						//below is to swipe to the screen
						Runtime.getRuntime().exec("adb -s "+udid+" shell input swipe 500 1000 180 180 ");
						Thread.sleep(1000);
						//reset the counter after swipe to start from beginning
						i = 0;
						//take the size again
						deviceCount = device_list.findElementsByClassName("android.widget.RelativeLayout").size();
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return devicePresent;
	}

	/**
	 * Description :This function would enter the name for the alert
	 * @param alertName
	 * @return Page object
	 */
	public AlertPageAndroid setAlertName(String alertName) { 
		try {
			utilityAndroid.genericWait(set_alert_name, "Click");
			utilityAndroid.enterTextAndroid(set_alert_name, alertName);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function would click on '+' icon to add alert
	 * @return Page object
	 */
	public AlertPageAndroid addAlert() { 
		try {
			utilityAndroid.clickAndroid(add_alert_btn); 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/** 
	 * Description : This function would Enable or disable toggle for alert
	 * @param element
	 * @param expectedState
	 * @return Page object
	 * @throws Exception
	 */
	public AlertPageAndroid setStateToggle(AndroidElement element, boolean expectedState) throws Exception {
		utilityAndroid.genericWait(save_alert_btn, "Click");
		elementDisplayed = false;
		try {
			if (element.isDisplayed()) {
				elementDisplayed = true;
			}
		} catch (Exception e) {
			elementDisplayed = false;
		}
		try {
			if (!elementDisplayed) {
				utilityAndroid.scrollToElement(element);
				utilityAndroid.genericWait(element, "Click");
				utilityAndroid.genericWait(save_alert_btn, "Click");
			}
			currentState = element.getText();
			if ((expectedState && currentState.equalsIgnoreCase("OFF")) || (!expectedState && currentState.equalsIgnoreCase("ON")) ) {
				utilityAndroid.clickAndroid(element);
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would save the alert created
	 * @return Page object
	 */	
	public AlertPageAndroid clickSave() {
		try {
			utilityAndroid.genericWait(save_alert_btn, "Click");
			utilityAndroid.clickAndroid(save_alert_btn);
			for (int i = 0; i <25; i++) {
			try {
				if(alert_error_message.isDisplayed() && alert_error_message.getText().toLowerCase().contains("saving")){
					Thread.sleep(750);
				}
			} catch (Exception e) {
				break;
			}
			} 
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if there are any alert pop up error messages on screen 
	 * @return alertErrorPresent
	 */
	public boolean verifyAlertErrors() {
		try {
			utilityAndroid.captureScreenshot(androidDriver);
			if ((alert_error_heading.getText().equals(noNameErrHeading)) 
					&&  (alert_error_message.getText().equals(noAlertName))) {
				alertErrorPresent = true;
				extentTestAndroid.log(Status.INFO, "Please enter a name for this alert.");
			}  else if ((alert_error_heading.getText().equals(noActionErrHeading)) 
					&&  (alert_error_message.getText().equals(noRuleActionChosenErr))) {
				alertErrorPresent = true;
				extentTestAndroid.log(Status.INFO, "Please turn on at least one action to trigger this alert.");
			}  else if ((alert_error_heading.getText().equals(noNotificationErrHeading)) 
					&&  (alert_error_message.getText().equals(noNotificationChosenErr))) {
				alertErrorPresent = true;
				extentTestAndroid.log(Status.INFO, "Please choose at least one method by which to be notified.");
			}
			utilityAndroid.clickAndroid(alert_error_ok_button);
			utilityAndroid.clickAndroid(back_button);
			utilityAndroid.clickAndroid(back_confirmation_btn);
			utilityAndroid.clickAndroid(back_button);
			return alertErrorPresent;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return alertErrorPresent;
		}
	}

	/**
	 * Description : This function would verify if the expected alerts is created
	 * @param alertName
	 * @param deviceName
	 * @return alertPresent
	 */	
	public boolean verifyAlertForDevice(String alertName, String deviceName) {
		for(int i =0;i<25; i++) {
		try {
			if(spinner.isDisplayed() || spinner.isEnabled()){
				Thread.sleep(750);
			}
		} catch (Exception e) {
			break;
		}
		}
		try {
			utilityAndroid.genericWait(add_alert_btn, "Click");
			if(verifyNoAlertText()) {
				return false;
			} else {
				//getting the count of the number of device and alert present on the page
				alertCount = alert_list.size();
				//iIndex loop is for Device, once the device name matches , control of execution would go to the next loop
				for (iIndex = 0 ; iIndex < alertCount; iIndex++) {
					if (alert_list.get(iIndex).getAttribute("resourceId").contains("HeadingTextView") && alert_list.get(iIndex).getText().trim().equalsIgnoreCase(deviceName)) {
						//jIndex loop is for the alert present under the above device
						for (jIndex = iIndex+1; jIndex < alertCount; jIndex++) {
							if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView") && alert_list.get(jIndex).getText().trim().equalsIgnoreCase(alertName)) {
								extentTestAndroid.log(Status.INFO, "Alerts  : " + alert_list.get(jIndex).getText());
								alertPresent = true;
								iIndex = jIndex;
								break;
							} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("HeadingTextView")) {
								alertPresent = false;
								iIndex = jIndex;
								break;
							} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView")) {
								alertPresent = false;
								iIndex = jIndex;
								iCounter++;
							}
						} 
					}
					if (alertPresent) {
						break;
					}
				}
				if (iCounter > 0 && !alertPresent) {
					alertPresent = false;
				}
			}
			return alertPresent;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return alertPresent;
		}
	}

	/**
	 * Description : This function would disable the existing specific alert
	 * @param alertName
	 * @param deviceName
	 * @return disableAlert
	 */
	public boolean disableAlert(String alertName, String deviceName) {
		try {
			utilityAndroid.waitForGenericSpinner();
			iCounter = 0;
			disableAlert = false;
			//getting the count of the number of device and alert present on the page
			alertCount = alert_list.size();
			//iIndex loop is for Device, once the device name matches ,the control of execution would go to the next loop
			for (iIndex = 0 ; iIndex < alertCount; iIndex++) {
				if (alert_list.get(iIndex).getAttribute("resourceId").contains("HeadingTextView")&& alert_list.get(iIndex).getText().trim().equalsIgnoreCase(deviceName)) {
					//jIndex loop is for the alert present under the above device
					for (jIndex = iIndex + 1; jIndex < alertCount; jIndex++) {
						if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView") && alert_list.get(jIndex).getText().trim().equalsIgnoreCase(alertName)) {
							alert_enabled_toggle_list.get(iCounter).click();
							//alert_enabled_toggle_list.get(jIndex-3).click();
							alertState = alert_enabled_toggle_list.get(iCounter).getText();
							if (alertState.equalsIgnoreCase("OFF")) {
								disableAlert = true;
								iIndex = jIndex;
								break;
							} else {
								disableAlert = false;
								iIndex = jIndex;
								break;
							}
						} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("HeadingTextView")) {
							iCounter++;
							disableAlert = false;
							iIndex = jIndex;
							break;
						} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView")) {
							iCounter++;
							disableAlert = false;
							iIndex = jIndex;
						}
					} 
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return disableAlert;
	}

	/**
	 * Description : This function would delete the alert created associated with device
	 * @param alertName
	 * @param deviceName
	 * @return alertDeleted
	 */	
	public boolean deleteAlert(String alertName, String deviceName) {
		try {
			alertDeleted = false;
			utilityAndroid.waitForGenericSpinner();
			//getting the count of device and alert on the page
			alertCount = alert_list.size();
			//iIndex loop is for Device, once the device name matches ,the control of execution would go to the next loop
			for (iIndex = 0; iIndex < alertCount; iIndex++) {
				if (alert_list.get(iIndex).getAttribute("resourceId").contains("HeadingTextView") && alert_list.get(iIndex).getText().trim().equalsIgnoreCase(deviceName)) {
					//jIndex loop is for the alert under the above device
					for (jIndex = iIndex + 1; jIndex < alertCount; jIndex++) {
						if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView")&& alert_list.get(jIndex).getText().trim().equalsIgnoreCase(alertName)) {
							utilityAndroid.longPress(alert_list.get(jIndex));
							utilityAndroid.genericWait(delete_alert_btn, "Click");
							utilityAndroid.clickAndroid(delete_alert_btn);
							utilityAndroid.genericWait(delete_confirmation_btn , "Click");
							utilityAndroid.clickAndroid(delete_confirmation_btn);
							alertDeleted = true;
							iIndex = jIndex;
							return alertDeleted;
						} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("HeadingTextView")) {
							iIndex = jIndex;
							break;
						}  else if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView")) {
							iIndex = jIndex;
						}
					}
				}
				if (alertDeleted) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return alertDeleted;
	}

	/**
	 * Description : This function would tap on specific alert
	 * @param alertName
	 * @param deviceName
	 * @return Page object
	 */
	public AlertPageAndroid tapAlert(String alertName, String deviceName) {
		try {
			tapalert = false;
			utilityAndroid.waitForGenericSpinner();
			alertCount = alert_list.size();
			for (iIndex = 0 ; iIndex < alertCount; iIndex++) {
				utilityAndroid.genericWait(add_alert_btn, "Click");
				if (alert_list.get(iIndex).getAttribute("resourceId").contains("HeadingTextView") && alert_list.get(iIndex).getText().trim().equalsIgnoreCase(deviceName)) {
					for (jIndex = iIndex + 1; jIndex < alertCount; jIndex++) {
						if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView")&& alert_list.get(jIndex).getText().trim().equalsIgnoreCase(alertName)) {
							alert_list.get(jIndex).click();
							utilityAndroid.genericWait(set_alert_name, "Visible");
							tapalert = true;
							return this;
						} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("HeadingTextView")) {
							iIndex = jIndex;
							return this;
						} else if (alert_list.get(jIndex).getAttribute("resourceId").contains("RowTextView")) {
							iIndex = jIndex;
						}
					} 
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would set the duration for longer than time
	 * @param hours
	 * @param minutes
	 * @return Page object
	 */
	public AlertPageAndroid setDurationForLongerThan(int hours , int minutes) {
		try {
			clickForLongerThan();
			if (hours >= 1 && hours <= 12) {
				utilityAndroid.selectTimeOnClock(sixth_hour, twelfth_hour , hours , "hours");
			} else if (hours >= 13 && hours <= 24 || hours == 0) {
				if (hours == 24 || hours == 0) {
					utilityAndroid.clickAndroid(zeroth_hour);
				} else {
					utilityAndroid.selectTimeOnClock(eightenth_hour , zeroth_hour , hours , "hours");
				}
			}
			utilityAndroid.selectTimeOnClock(zeroth_minute, thirtyth_minute, minutes , "minutes");
			utilityAndroid.genericWait(clock_ok_btn, "Click");
			utilityAndroid.clickAndroid(clock_ok_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select From Time on clock
	 * @return Page object
	 */
	public AlertPageAndroid selectFromTime() {
		int fromHours = 0;
		int fromMin = 0;
		try {
			//get hours and minutes separately
			fromHours = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[0]);
			fromMin = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[1]);
			fromMin = fromMin + 6 ;
			if(fromMin > 60) {
				fromMin =  fromMin - 60 ;
				fromHours=+1;
			} else if(fromMin == 59 || fromMin == 60) {
				fromMin =  0 ;
				fromHours=+1;
			}
			//select from time
			clickFromTime();
			if (fromHours >= 1 && fromHours <= 12) {
				utilityAndroid.selectTimeOnClock(sixth_hour, twelfth_hour , fromHours , "hours");
			} else if (fromHours >= 13 && hours <= 24) {
				if (fromHours == 24) {
					utilityAndroid.clickAndroid(zeroth_hour);
				} else {
					utilityAndroid.selectTimeOnClock(eightenth_hour , zeroth_hour , fromHours , "hours");
				}
			}
			utilityAndroid.selectTimeOnClock(zeroth_minute, thirtyth_minute, fromMin , "minutes");
			utilityAndroid.clickAndroid(clock_ok_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select To Time on clock
	 * @return Page object
	 */
	public AlertPageAndroid selectToTime() {
		int toHours = 0;
		int toMin = 0;
		try {
			toHours = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[0]);
			toMin = Integer.parseInt(utilityAndroid.getCurrentTime().split(":")[1]);
			toMin = toMin + 10 ;
			if(toMin > 60) {
				toMin =  toMin - 60 ;
				toHours=+1;
			} else if(toMin == 59 || toMin == 60) {
				toMin =  0 ;
				toHours=+1;
			}
			//select to time
			clickToTime();
			if (toHours >= 1 && toHours <= 12) {
				utilityAndroid.selectTimeOnClock(sixth_hour, twelfth_hour , toHours , "hours");
			} else if (toHours >= 13 && toHours <=24) {
				if (toHours == 24) {
					utilityAndroid.clickAndroid(zeroth_hour);
				} else {
					utilityAndroid.selectTimeOnClock(eightenth_hour , zeroth_hour , toHours , "hours");
				}
			}
			utilityAndroid.selectTimeOnClock(zeroth_minute, thirtyth_minute, toMin , "minutes");
			clickClockOkButtton();
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select Days on the week based on the parameter
	 * @param days[]
	 * @return Page object
	 */
	public AlertPageAndroid selectDays(String[] days) {
		try {
			clickOnTheseDays();
			//de-select all the check boxes
			for (iIndex = 0 ; iIndex < days_of_week.size(); iIndex++) {
				utilityAndroid.clickAndroid(days_of_week.get(iIndex));
			}
			//select only the days passed as parameter
			for (iIndex = 0 ; iIndex < days_of_week.size(); iIndex++) {
				for (jIndex = 0 ; jIndex < days.length; jIndex ++) {
					if (days_of_week.get(iIndex).getText().equals(days[jIndex])) {
						utilityAndroid.clickAndroid(days_of_week.get(iIndex));
					}
				}
			}
			clickDaysDoneBtn();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would de-select the current day (would skip if it is already de-selected)
	 * @return Page object
	 */
	public AlertPageAndroid deselectCurrentDay() {
		String currentDay ;
		currentDay = utilityAndroid.getCurrentDay();
		try {
			// below code will perform de-selection for current day
			for (iIndex = 0; iIndex < days_of_week.size(); iIndex++) {
				if (days_of_week.get(iIndex).getText().equals(currentDay)) {
					if (days_of_week.get(iIndex).getAttribute("checked").equals("true")) {
						utilityAndroid.clickAndroid(days_of_week.get(iIndex));
					} 
				}
			}
			clickDaysDoneBtn();
			utilityAndroid.genericWait(save_alert_btn, "Click");
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would check if notification appeared on notification panel
	 * @param deviceName
	 * @param expecetedNotification
	 * @return Page object
	 * @throws InterruptedException 
	 */
	public boolean checkPushNotificationReceived(String deviceName, String text, String udid) {
		boolean notificationReceived = false;
		try {
			Thread.sleep(10000);
			utilityAndroid.openNotificationPanel();
			for (iIndex = 0; iIndex < notification_text.size(); iIndex++) {
				if (notification_text.get(iIndex).getText().trim().equalsIgnoreCase("Alert Occurred")) {
					if (notification_text.get(iIndex+1).getText().toUpperCase().trim().contains(deviceName.toUpperCase()) && notification_text.get(iIndex+1).getText().toUpperCase().trim().contains(text.toUpperCase())) {
						extentTestAndroid.log(Status.INFO,  notification_text.get(iIndex).getText() + " : "+ notification_text.get(iIndex+1).getText());
						notificationReceived = true;
						break;
					} else if (notification_text.get(iIndex+2).getText().toUpperCase().trim().contains(deviceName) && notification_text.get(iIndex+2).getText().toUpperCase().trim().contains(text.toUpperCase())) {
						extentTestAndroid.log(Status.INFO,  notification_text.get(iIndex).getText() + " : " +notification_text.get(iIndex+2).getText());
						notificationReceived = true;
						break;
					}
				}
			}
			if(!notificationReceived) {
				utilityAndroid.captureScreenshot(androidDriver);
			}
			//close the notification panel
			utilityAndroid.closeNotificationPanel(udid);
			return notificationReceived;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return notificationReceived;
		}
	}

	/**
	 * Description : This function would wait for 40 seconds
	 */
	public AlertPageAndroid waitTime() {
		try {
			Thread.sleep(10000);
			androidDriver.currentActivity();
			Thread.sleep(10000);
			androidDriver.currentActivity();
			Thread.sleep(10000);
			androidDriver.currentActivity();
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the current day and de-select all other days
	 * @return Page object
	 */
	public AlertPageAndroid selectCurrentDay() {
		String currentDay ;
		currentDay = utilityAndroid.getCurrentDay();
		try {
			// below code will perform de-selection of the days other than the current day
			for(AndroidElement element : days_of_week) {
				if(!element.getText().equalsIgnoreCase(currentDay)) {
					utilityAndroid.clickAndroid(element);
				}
			}
			clickDaysDoneBtn();
			utilityAndroid.genericWait(save_alert_btn, "Click");
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would check if the error is displayed or not
	 * @return boolean 
	 */
	public boolean verifyErrorDispalyed () {
		try {
			if(alert_error_message.isDisplayed() || alert_error_message.isEnabled()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would get the text of the error pop-up
	 * @return String 
	 */
	public String getErrorMsg() {
		String tempText = null;
		try {
			tempText = alert_error_message.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return tempText;
	}

	/**
	 * Description : This function would click on ok button displayed on the page
	 * @return Page object
	 */
	public AlertPageAndroid clickOkButton () {
		try {
			utilityAndroid.genericWait(alert_error_ok_button, "Click");
			utilityAndroid.clickAndroid(alert_error_ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function would click on Later button displayed on the page
	 * @return Page object
	 */
	public AlertPageAndroid clickLaterButton () {
		try {
			utilityAndroid.clickAndroid(later_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
}
