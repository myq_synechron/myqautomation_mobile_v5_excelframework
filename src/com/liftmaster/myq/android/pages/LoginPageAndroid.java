package com.liftmaster.myq.android.pages;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;

public class LoginPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;
	public String env;

	//Page factory for Login Page Android
	@AndroidFindBy(id = "text_login_server")
	public AndroidElement version_name;

	@AndroidFindBy(id = "text_login_feature_mode")
	public AndroidElement feature_mode;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Multi USer (v5)']")
	public AndroidElement multi_user;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='V5 Only']")
	public AndroidElement v5_only;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Prod: myqexternal.myqdevice.com']")
	public AndroidElement prod_env_select;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='PP: myqexternal-pp.myqdevice.com']")
	public AndroidElement preprod_env_select;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Dev: myqexternaldev.myqdevice.com']")
	public AndroidElement dev_env_select;

	@AndroidFindBy(id = "username")
	public AndroidElement account_email_textbox;

	@AndroidFindBy(id = "password")
	public AndroidElement account_password_textbox;

	@AndroidFindBy(id = "login_button")
	public AndroidElement login_btn;

	@AndroidFindBy(id = "text_toolbar_title")
	public AndroidElement toolbar_text;

	@AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc='Brand logo']")
	public AndroidElement brand_logo;

	@AndroidFindBy(id  = "image_login_brand")
	public AndroidElement liftmaster_logo;

	@AndroidFindBy(id = "image_login_myq_logo")
	public AndroidElement myq_logo;

	@AndroidFindBy(id = "account_view")
	public AndroidElement account_view_link;

	@AndroidFindBy(id = "grid_view")
	public AndroidElement grid_view_icon;

	@AndroidFindBy(id = "device_view")
	public AndroidElement device_view_link;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Logout']")
	public AndroidElement logout_btn;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='OK']")
	public AndroidElement confirm_logout_ok_btn;

	@AndroidFindBy(id = "button_mu_onboarding_remind_me")
	public AndroidElement remind_me_later;

	@AndroidFindBy(id ="passcode1")
	public  AndroidElement passcode_textbox_1;

	@AndroidFindBy(id ="passcode2")
	public  AndroidElement passcode_textbox_2;

	@AndroidFindBy(id ="passcode3")
	public  AndroidElement passcode_textbox_3;

	@AndroidFindBy(id ="passcode4")
	public  AndroidElement passcode_textbox_4;

	@AndroidFindBy(id = "passcode_header")
	public AndroidElement enter_passcode_text;

	@AndroidFindBy(id = "android:id/progress")
	public AndroidElement login_spinner;

	@AndroidFindBy(id = "login_forgot_password_button")
	public AndroidElement forgot_password_button;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement reset_button;

	@AndroidFindBy(xpath = "//android.widget.EditText")
	public AndroidElement email_text_field;

	@AndroidFindBy(id = "android:id/alertTitle")
	public AndroidElement heading;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement message;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement ok_button;

	@AndroidFindBy(id = "android:id/button2")
	public AndroidElement cancel_button;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement error_message;

	@AndroidFindBy(id = "login_remember_checkbox")
	public AndroidElement remember_me_radio_button;

	@AndroidFindBy(id = "text_alert_title")
	public AndroidElement user_not_activated_heading;

	@AndroidFindBy (id = "copy_rights")
	public AndroidElement copy_rights_text;

	String heading_forgetPassword = "Forgot Password";
	String message_forgotPassword = "Enter your email address to receive a new password.";

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public LoginPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid){
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function would verify the environment text present or not
	 * @return boolean
	 */
	public boolean verifyEnvTextPresent() {
		try {
			if(version_name.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description  : This function would select the environment
	 * @return Page object
	 */
	public LoginPageAndroid selectEnvironment(String env) {
		try {
			utilityAndroid.genericWait(liftmaster_logo, "Visible");
			try {
				androidDriver.hideKeyboard();
			} catch (Exception e) {
				// catch exception and continue with the execution
			}
			//selecting environment
			if (env.equalsIgnoreCase("Prod")) {
				if(verifyEnvTextPresent()) {
					utilityAndroid.genericWait(login_btn, "Visible");
					TouchAction Action = new TouchAction(androidDriver);
					Action.longPress(version_name).perform();
					
					utilityAndroid.clickAndroid(prod_env_select);
					extentTestAndroid.log(Status.PASS, env +" environment selected");
				} 
				return this;
			} 
			
			utilityAndroid.genericWait(login_btn, "Visible");
			TouchAction Action = new TouchAction(androidDriver);
			Action.longPress(version_name).perform();
			
			if (env.equalsIgnoreCase("Pre-Prod")) {
				utilityAndroid.clickAndroid(preprod_env_select);
				extentTestAndroid.log(Status.PASS, env +" environment selected");
			} else if (env.equalsIgnoreCase("Dev")) {
				utilityAndroid.clickAndroid(dev_env_select);
				extentTestAndroid.log(Status.PASS, env +" environment selected");
			} else { 
				extentTestAndroid.log(Status.FAIL, env +" environment not selected"); 
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the V5 on app
	 * @return Page object
	 */
	public LoginPageAndroid selectVersion(String userType) {
		try {
			if(verifyEnvTextPresent()) {
				//below is done to Select the feature on Samsung device as well. As tap was not working on Samsung device
				TouchAction action = new TouchAction(androidDriver);
				action.press(version_name).release().perform();
				action.press(version_name).release().perform();

				utilityAndroid.clickAndroid(feature_mode);

				if (userType.equalsIgnoreCase("Single")) {
					utilityAndroid.clickAndroid(v5_only);
					utilityAndroid.clickAndroid(ok_button);
					extentTestAndroid.log(Status.PASS, "Single user Selected");
				} else if (userType.equalsIgnoreCase("Multi User")) {
					utilityAndroid.clickAndroid(multi_user);
					utilityAndroid.clickAndroid(ok_button);
					extentTestAndroid.log(Status.PASS, "Multi user Selected");
				} else { 
					extentTestAndroid.log(Status.FAIL, "Invalid user Selected "); 
				}
			}
		} catch (Exception e) {
			//utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if the feature is present or not
	 * @param userType
	 * @return
	 */
	public boolean verifyFeatureSelected(String userType) {
		String feature = null;
		try {
			if(userType.equals("Single")) {
				feature = "V5 Only";
			} else if(userType.equals("Multi User")) {
				feature = "Multi USer (v5)";
			}
			if(feature_mode.isDisplayed() && feature_mode.getText().equalsIgnoreCase(feature)) {
				return true;
			}
		} catch (Exception e) {
			return false;	
		}
		return false;
	}

	/**
	 * Description : This function would enter the email id into the text box
	 * @param emailID
	 * @return Page object
	 */
	public LoginPageAndroid enterAccountEmailId(String emailID) {
		try {
			utilityAndroid.genericWait(account_email_textbox, "Click");
			utilityAndroid.enterTextAndroid(account_email_textbox, emailID);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enter the password into the text box
	 * @param password
	 * @return Page object
	 */
	public LoginPageAndroid enterAccountPassword(String password) {
		try {
			utilityAndroid.genericWait(account_password_textbox, "Click");
			utilityAndroid.enterTextAndroid(account_password_textbox, password);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function click on login button
	 * @return Page object
	 */
	public LoginPageAndroid clickLoginButton() {
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {

		}
		try {
			utilityAndroid.genericWait(login_btn , "Click");
			utilityAndroid.clickAndroid(login_btn);
			utilityAndroid.genericWait(toolbar_text, "Visible");
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to verify if login button is present or not
	 * @return boolean
	 */
	public boolean verifyLoginBtnPresent() {
		try {
			if(login_btn.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;    
		}
		return false;
	}

	/**
	 * Description : This function is to verify the logo on login screen is present or not
	 * @return boolean
	 */
	public boolean verifyLogoPresent() {
		try {
			if(liftmaster_logo.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;    
		}
		return false;
	}

	/**
	 * Description : This function would login into the application 
	 * @param emailID
	 * @param password
	 * @return Page object
	 */
	public LoginPageAndroid login(String emailID, String password){
		try {
			androidDriver.hideKeyboard();
		} catch(Exception e) {
			//catch the exception and continue with the execution
		}
		try {
			utilityAndroid.clickAndroid(account_email_textbox);
			utilityAndroid.enterTextAndroid(account_email_textbox, emailID);
			utilityAndroid.enterTextAndroid(account_password_textbox, password);
			try {
				androidDriver.hideKeyboard();
			} catch (Exception e) {
				//catch the exception and continue with the execution
			}
			utilityAndroid.genericWait(login_btn , "Click");
			utilityAndroid.clickAndroid(login_btn);
			for (int i = 0 ; i < 20; i++) {
				try {
					if (login_spinner.isDisplayed()) {
						Thread.sleep(2000);
					}
				} catch (Exception e) {
					break;
				}
			}
			try {
				WebDriverWait wait = new WebDriverWait(androidDriver, 20);
				wait.until(ExpectedConditions.visibilityOf(remind_me_later));
				utilityAndroid.clickAndroid(remind_me_later);
			} catch (Exception e) {
				//continue with the execution
			}
		} catch (Exception e) {
		} finally {
			try {

				if (liftmaster_logo.isDisplayed()) {
					utilityAndroid.enterTextAndroid(account_email_textbox, emailID);
					utilityAndroid.enterTextAndroid(account_password_textbox, password);
					try {
						androidDriver.hideKeyboard();
					} catch (Exception e) {
						// catch exception and continue with the execution
					}
					utilityAndroid.genericWait(login_btn , "Click");
					utilityAndroid.clickAndroid(login_btn);
					for (int i = 0 ; i < 20; i++) {
						try {
							if (login_spinner.isDisplayed()) {
								Thread.sleep(2000);
							}
						} catch (Exception e) {
							break;
						}
					}
					try {
						WebDriverWait wait = new WebDriverWait(androidDriver, 20);
						wait.until(ExpectedConditions.visibilityOf(remind_me_later));
						utilityAndroid.clickAndroid(remind_me_later);
					} catch (Exception e) {
						//continue with the execution
					}
				}
			} catch (Exception e) {
			}
		}
		return this;
	}

	/**
	 * Description : This function would verify the logo after login , to verify correct login
	 * @return Page object
	 */
	public boolean verifyLoginPage() {
		try {
			utilityAndroid.genericWait(brand_logo, "Visible");
			Assert.assertTrue(brand_logo.isEnabled(),"failed to verify login page");
			return true;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function would logout from application
	 * @return Page object
	 */
	public LoginPageAndroid clickLogout() {
		try {
			boolean logoutDisplayed= true;
			try {
				if (logout_btn.isDisplayed()) {
					logoutDisplayed = true;
				}
			} catch (Exception e) {
				logoutDisplayed = false;
			}
			if (!logoutDisplayed) {
				utilityAndroid.scrollToElement(logout_btn);
			}
			utilityAndroid.genericWait(logout_btn , "Click");
			utilityAndroid.clickAndroid(logout_btn);
			utilityAndroid.genericWait(confirm_logout_ok_btn , "Click");
			utilityAndroid.clickAndroid(confirm_logout_ok_btn);
			utilityAndroid.genericWait(liftmaster_logo, "Visible");
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();			
		} catch (Exception e){
		}
		return this;
	}

	/**
	 * Description : This function would enter the Passcode
	 * @return page object
	 */
	public LoginPageAndroid enterPasscode(String passcode) {
		try {
			utilityAndroid.genericWait(myq_logo, "Visible");
			String text[ ] = passcode.split("");

			utilityAndroid.genericWait(passcode_textbox_1, "Visible");
			utilityAndroid.genericWait(passcode_textbox_1, "Click");
			utilityAndroid.clickAndroid(passcode_textbox_1);
			utilityAndroid.enterTextAndroid(passcode_textbox_1, text[0] );
			utilityAndroid.enterTextAndroid(passcode_textbox_2, text[1] );
			utilityAndroid.enterTextAndroid(passcode_textbox_3, text[2] );
			utilityAndroid.enterTextAndroid(passcode_textbox_4, text[3] );
		} catch(Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// 
		}
		for (int i = 0 ; i < 20; i++) {
			try {
				if(login_spinner.isDisplayed() || login_spinner.isEnabled()) {
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				break;
			}
		}
		return this;
	}

	/**
	 * Description : This function would verify the logo present on log-in page
	 * @return boolean 
	 */
	public boolean verifyLogoOnLoginPage() {
		try {
			if (liftmaster_logo.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify the passcode text present on login page 
	 * @return boolean
	 */
	public boolean verifyPasscodeTextPresent() {
		try {
			if (enter_passcode_text.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;	
		}
		return false;
	}

	/**
	 * Description : This function would get the passcode text present on login page 
	 * @return String 
	 */
	public String getPasscodeText() {
		try {
			return enter_passcode_text.getText();
		} catch (Exception e) {
			//
		}
		return null;
	}
	/**
	 * Description : This function would verify the forgot password popup and text is displayed when clicked on forget password 
	 * @return Page object
	 */
	public LoginPageAndroid verifyForgotPasswordPopup() {
		try {
			if(heading.getText().equalsIgnoreCase(heading_forgetPassword) && message.getText().equalsIgnoreCase(message_forgotPassword)) {
				extentTestAndroid.log(Status.INFO,message_forgotPassword);
			} else {
				extentTestAndroid.log(Status.INFO,"Popup for forget password is not displayed ");
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;	
	}

	/**
	 * Description :This function would edit the email text field on forget password popup
	 * @return boolean
	 */
	public boolean verifyEditEmailInForgetPassword() { 
		try {
			utilityAndroid.genericWait(email_text_field, "Visible");
			if(email_text_field.getAttribute("clickable").equals("true")){
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description :This function would click on ok button
	 * @return Page object
	 */
	public LoginPageAndroid clickOkButton() { 
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			utilityAndroid.clickAndroid(ok_button);
		}catch(Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description :This function would click on cancel button
	 * @return Page object
	 */
	public LoginPageAndroid clickCancelButton() { 
		try {
			utilityAndroid.genericWait(cancel_button, "Click");
			utilityAndroid.clickAndroid(cancel_button);
		}catch(Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description :This function would edit the email text field on forget password popup
	 * @param accountEmail
	 */
	public boolean verifyEnteredEmail(String email) { 
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			if(email_text_field.getText().equalsIgnoreCase(email)){
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description : This function would verify the confirmation pop up after new password is sent.
	 * @param email
	 * @return boolean
	 */
	public boolean verifyEmailConfimation(String email) {
		String tittle = "Password Reset";
		String confirmationMessage = "An email has been sent to "+email+". Please check your email inbox to complete the reset process.";
		try {
			utilityAndroid.genericWait(heading, "Visible");
			if( heading.getText().trim().equalsIgnoreCase(tittle) && message.getText().trim().equalsIgnoreCase(confirmationMessage)) {
				extentTestAndroid.log(Status.INFO, heading.getText() + " : " + message.getText() );
				return true;
			} else {
				return false;
			}
		}catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description :This function would click on forgot Password
	 * @return Page object
	 */
	public LoginPageAndroid clickForgotPassword() { 
		try {
			utilityAndroid.clickAndroid(forgot_password_button);
		}catch(Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would clear the email id into the text box
	 * @return Page object
	 */
	public LoginPageAndroid clearAccountEmailId() {
		try {
			utilityAndroid.clearAndroid(account_email_textbox);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would clear the password into the text box
	 * @return Page object
	 */
	public LoginPageAndroid clearAccountPassword() {
		try {
			utilityAndroid.clearAndroid(account_password_textbox);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description: This function would verify error on login and client specific error codes
	 * @return errorMessagePresent
	 */
	public boolean validateLoginErrorMessage() {
		String errorMessage1 = "Please enter a valid email address.";
		String errorMessage2 = "Please enter your password to login.";
		String errorMessage3 = "The username or password you entered is incorrect. Try again. (203)";
		String errorMessage4 = "The user will be locked out. (205)";
		String errorMessage5 = "The user will be locked out. (207)";
		String errorMessage6 = "You are not connected to the internet.";
		String errorMessage7 = "There was an error connecting to the server. Please check your connection and try again.";
		boolean errorMessagePresent = false;
		try {
			String code;
			if (error_message.getText().trim().equalsIgnoreCase(errorMessage1)) {
				extentTestAndroid.log(Status.INFO, error_message.getText());
				errorMessagePresent = true;
			} else if (error_message.getText().trim().equalsIgnoreCase(errorMessage2)) {
				extentTestAndroid.log(Status.INFO, error_message.getText());
				errorMessagePresent = true;
			} else if (error_message.getText().trim().equalsIgnoreCase(errorMessage3)){
				extentTestAndroid.log(Status.INFO,error_message.getText());
				code = error_message.getText().substring(error_message.getText().indexOf("(")+1, error_message.getText().indexOf(")"));
				if(code.equalsIgnoreCase("203"))
					extentTestAndroid.log(Status.INFO, "Error Code Validated: "+code);
				errorMessagePresent = true;
			} else if (error_message.getText().trim().equalsIgnoreCase(errorMessage4)){
				extentTestAndroid.log(Status.INFO, error_message.getText());
				code = error_message.getText().substring(error_message.getText().indexOf("(")+1, error_message.getText().indexOf(")"));
				if(code.equalsIgnoreCase("205"))
					extentTestAndroid.log(Status.INFO, "Error Code Validated: "+code);
				errorMessagePresent = true;
			} else if (error_message.getText().trim().equalsIgnoreCase(errorMessage5)){
				extentTestAndroid.log(Status.INFO, error_message.getText());
				code = error_message.getText().substring(error_message.getText().indexOf("(")+1, error_message.getText().indexOf(")"));
				if(code.equalsIgnoreCase("207"))
					extentTestAndroid.log(Status.INFO, "Error Code Validated: "+code);
				errorMessagePresent = true;
			} else if (error_message.getText().trim().equalsIgnoreCase(errorMessage6)) {
				extentTestAndroid.log(Status.INFO, error_message.getText());
				errorMessagePresent = true;
			} else if (error_message.getText().trim().equalsIgnoreCase(errorMessage7)) {
				extentTestAndroid.log(Status.INFO, error_message.getText());
				errorMessagePresent = true;
			}
			utilityAndroid.clickAndroid(ok_button);
			return errorMessagePresent;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return errorMessagePresent;
		}	
	}

	/**
	 * Description : This function would wait till the time spinner is displayed after click on login button
	 * @return Page object
	 */
	public LoginPageAndroid waitForLoginSpinner() {
		for (int i = 0 ; i < 20; i++) {
			try {
				if(login_spinner.isDisplayed() || login_spinner.isEnabled()) {
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				break;
			}
		}
		return this;
	}

	/**
	 * Description : This function would select the remember me  radio button
	 * @return Page Object
	 */
	public LoginPageAndroid clickOnRememberMe() {
		try {
			utilityAndroid.genericWait(remember_me_radio_button, "Click");
			utilityAndroid.clickAndroid(remember_me_radio_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify user is activatied or not
	 * @return boolean
	 */
	public boolean verifyUserNotActivated() {
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			if(user_not_activated_heading.isDisplayed()) {
				extentTestAndroid.log(Status.INFO, user_not_activated_heading.getText() + " : " + error_message.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify the email sent pop-up
	 * @return boolean
	 */
	public boolean verifyEmailSent() {
		try{
			utilityAndroid.genericWait(ok_button, "Click");
			if(heading.isDisplayed()) {
				extentTestAndroid.log(Status.INFO, heading.getText() + " : " +  message.getText());
				utilityAndroid.clickAndroid(ok_button);
				return true;
			}
		} catch(Exception e ) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would select the V5 on app
	 * @return Page object
	 */
	public LoginPageAndroid selectVersion() {
		try {
			androidDriver.tap(1, version_name, 1);
			androidDriver.tap(1, version_name, 1);
			extentTestAndroid.log(Status.PASS, "Version V5 selected"); 
		} catch (Exception e) {
			extentTestAndroid.log(Status.FAIL, "Version V5 not selected");
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the feature V5 only or multi-user
	 * @return Page object
	 */
	public LoginPageAndroid selectFeature(String userType) {
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with execution flow
		}

		try {
			utilityAndroid.clickAndroid(feature_mode);
			if (userType.equalsIgnoreCase("Single")) {
				utilityAndroid.clickAndroid(v5_only);
				utilityAndroid.clickAndroid(ok_button);
				extentTestAndroid.log(Status.PASS, "Single user Selected");
			} else if (userType.equalsIgnoreCase("Multi User")) {
				utilityAndroid.clickAndroid(multi_user);
				utilityAndroid.clickAndroid(ok_button);
				extentTestAndroid.log(Status.PASS, "Multi user Selected");
			} else { 
				extentTestAndroid.log(Status.FAIL, "Invalid user Selected "); 
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if error pop-up is present on screen or not
	 * @return boolean
	 */
	public boolean verifyErrorPresent() {
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			if(error_message.isDisplayed()) {
				return true;
			}
		} catch(Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would get the text of the error pop-up
	 * @return string
	 */
	public String getErrorText() {
		String tempText = null;
		try {
			tempText = error_message.getText();
			return tempText;
		} catch (Exception e) {
			return tempText;
		}
	}

	/**
	 * Description : This function would verify if copy right text is present on login page or not
	 * @return boolean
	 */
	public boolean isCopyRightTextPresent() {
		try {
			if(copy_rights_text.isDisplayed() || copy_rights_text.isEnabled()) {
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would verify the copy right text 
	 * @return boolean
	 */
	public String getCopyRightText() {
		String tempText = null;
		try {
			tempText = copy_rights_text.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return tempText;
	}
}