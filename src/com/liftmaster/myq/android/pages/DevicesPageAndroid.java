package com.liftmaster.myq.android.pages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;


public class DevicesPageAndroid{

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	boolean verifyHubName = false;
	boolean devicePresent = false;
	boolean noDeviceText = false;
	boolean verifyDeviceName = false;
	boolean roleSelected;
	int iIndex = 0;
	int jIndex = 0;
	int hubCount;
	int deviceCount;
	int deviceIndex = 0 ;

	//Page factory for Devices Page Android
	@AndroidFindBy(id = "device_label")
	public List <AndroidElement> device_name_list;

	@AndroidFindBy(id = "place_label")
	public List <AndroidElement> hub_name_list;

	@AndroidFindBy(id = "device_icon")
	public List <AndroidElement> device_icon_list;

	@AndroidFindBy(id = "device_state")
	public List <AndroidElement> device_state_list;

	@AndroidFindBy(id = "device_state_info")
	public List <AndroidElement> device_state_timer_list;

	@AndroidFindBy(id = "include_devicelist_swipe_container")
	public List <AndroidElement> device_container_list;

	@AndroidFindBy(id = "text_devicelist_swipe_control")
	public AndroidElement device_change_button;

	@AndroidFindBy(id = "device_view_progress_indicator")
	public AndroidElement spinner_on_device;

	@AndroidFindBy(id = "grid_view")
	public AndroidElement grid_view_icon;

	@AndroidFindBy(id = "device_view")
	public AndroidElement device_view_link;

	@AndroidFindBy(id = "android:id/alertTitle")
	public AndroidElement error_heading;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement error_text;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement ok_button;

	@AndroidFindBy(id = "layout_acctheader")
	public AndroidElement blue_bar;

	@AndroidFindBy(id = "item_title")
	public List<AndroidElement> role_list;	

	//@AndroidFindBy(id = "text_nodevices_message")
	@AndroidFindBy(id = "text_emptymsg_text")
	public AndroidElement no_device_text;

	@AndroidFindBy(id = "button_cancel")
	public AndroidElement cancel_button;

	@AndroidFindBy(id ="passcode1")
	public  AndroidElement passcode_textbox_1;

	@AndroidFindBy(id ="passcode2")
	public  AndroidElement passcode_textbox_2;

	@AndroidFindBy(id ="passcode3")
	public  AndroidElement passcode_textbox_3;

	@AndroidFindBy(id ="passcode4")
	public  AndroidElement passcode_textbox_4;

	@AndroidFindBy(id = "android:id/progress")
	public AndroidElement login_spinner;

	@AndroidFindBy(id = "Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "passcode_header")
	public AndroidElement passcode_header;

	@AndroidFindBy(id = "text_toolbar_title")
	public AndroidElement devices_text;

	@AndroidFindBy(xpath = "//android.widget.ImageView[@content-desc='Brand logo']")
	public AndroidElement brand_logo;


	//API Specific
	@AndroidFindBy(xpath = "//*[@class = 'android.widget.EditText' and @text = 'Light']")
	public AndroidElement light_name;

	@AndroidFindBy(xpath = "//*[@class='android.widget.EditText' and @index='0']")
	public AndroidElement first_cell_textbox;

	@AndroidFindBy(xpath = "//*[@class='android.widget.EditText' and @index='2']")
	public AndroidElement second_cell_textbox;

	@AndroidFindBy(xpath = "//*[@class='android.widget.EditText' and @index='4']")
	public AndroidElement third_cell_textbox;

	@AndroidFindBy(id = "submit")
	public AndroidElement submit_btn;

	@AndroidFindBy(id = "edit_register_gateway_name")
	public AndroidElement gateway_name_textbox;

	@AndroidFindBy(id = "button_right")
	public AndroidElement save_btn;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Remote Light']")
	public AndroidElement remote_light_btn;

	@AndroidFindBy(id = "button_right")
	public AndroidElement next_btn;

	@AndroidFindBy(xpath = "//*[@class = 'android.widget.EditText' and @text = 'Device Name']")
	public AndroidElement device_name_textbox;

	@AndroidFindBy(id = "delete_gateway")
	public AndroidElement delete_gateway_btn;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Add New']")
	public AndroidElement new_hub_btn;

	@AndroidFindBy(className = "android.widget.TextView")
	public List <AndroidElement> gateway_name;

	@AndroidFindBy(id = "add_device")
	public AndroidElement add_device;

	@AndroidFindBy(xpath = "//*[@class = 'android.widget.TextView' and @text = 'Garage Door Opener']")
	public AndroidElement gdo_name;

	@AndroidFindBy(xpath = "//*[@class = 'android.widget.TextView' and @text = 'Commercial Door Operator']")
	public AndroidElement cdo_name;

	@AndroidFindBy(xpath = "//*[@class = 'android.widget.TextView' and @text = 'Gate Operator']")
	public AndroidElement gate_name;

	@AndroidFindBy(className = "android.widget.TextView")
	public List <AndroidElement> device_list;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Delete Device']")
	public AndroidElement delete_device_btn;

	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Delete']")
	public AndroidElement delete_btn;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement delete_confirmation;

	@AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton' and @content-desc='Navigate up']" )
	public AndroidElement back_button;

	@AndroidFindBy(className = "android.widget.TextView")
	public List <AndroidElement> hub_list;

	@AndroidFindBy(id = "edit_register_device_name")
	public AndroidElement edit_device_name;

	@AndroidFindBy(id = "device_stronghold_icon") 
	public AndroidElement shield_icon;

	@AndroidFindBy(className = "android.widget.TextView")
	public List<AndroidElement> notification_text;

	@AndroidFindBy(xpath ="//*[@content-desc='Clear all']")
	public AndroidElement clear_notification_panel;

	@AndroidFindBy(xpath = "//android.widget.ListView")
	public AndroidElement list_of_device;

	@AndroidFindBy(id = "button_right")
	public AndroidElement yes_button;

	@AndroidFindBy(id = "button_left")
	public AndroidElement no_button;

	@AndroidFindBy (xpath = "//android.widget.TextView[@text='Tap here to select']") 
	public AndroidElement tap_here_to_select;

	@AndroidFindBy (id="edit_filterable_list_filter")
	public AndroidElement type_to_search;

	@AndroidFindBy(xpath="//android.widget.TextView[@text='LiftMaster']")
	public AndroidElement liftmaster_text;

	@AndroidFindBy(xpath ="//android.widget.TextView[@text='Color of program button on operator']")
	public AndroidElement color_of_button;

	@AndroidFindBy (xpath = "//android.widget.ListView")
	public AndroidElement color_selection;

	//@AndroidFindBy(xpath = "//android.widget.Button[@text='PROGRAM DOOR OPENER']")
	@AndroidFindBy(xpath = "//android.widget.Button[@text='Program Door Opener']")
	public AndroidElement program_door;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUE']")
	public AndroidElement continue_button;

	@AndroidFindBy(xpath = "//android.widget.Button[@text='I HAVE PRESSED THE PROGRAM BUTTON']")
	public AndroidElement pressed_program_button;

	@AndroidFindBy(xpath = "//android.widget.TextView[@text='Yellow']")
	public AndroidElement color_yellow;

	@AndroidFindBy (id = "text_gateway_name")
	public  AndroidElement grid_view_gateway_name;

	@AndroidFindBy (xpath = "//android.widget.GridView")
	public AndroidElement grid_view_devices_list;

	@AndroidFindBy (id= "viewPagerDots")
	public AndroidElement view_pager_dots;

	@AndroidFindBy (xpath = "//android.widget.Gallery")
	public AndroidElement gallery_view_devices;

	@AndroidFindBy (id = "edit_register_device_name")
	public AndroidElement textbox_device_name;

	@AndroidFindBy (xpath = "//android.view.View[@text='MyQ Troubleshooting']")
	public AndroidElement myq_troubleShooting_text;

	@AndroidFindBy (id = "EventsTab_ProgressBar")
	public AndroidElement add_new_device_spinner;

	@AndroidFindBy (id = "enter_new_smart_hub")
	public AndroidElement myq_smart_garage_hub_text;

	@AndroidFindBy (id = "gateway_devices_header")
	public AndroidElement devices_heading;
	
	@AndroidFindBy (id = "manage_places")
	public AndroidElement plus_button;
	
	@AndroidFindBy (id = "add_new_device")
	public AndroidElement get_started_button;
	
	@AndroidFindBy (id = "success_text")
	public AndroidElement success_text;
	
	@AndroidFindBy (id = "button_deviceedit_program_opener")
	public AndroidElement select_and_prgram_door_button;
	
	@AndroidFindBy(xpath = "//android.widget.ListView")
	public AndroidElement deviceList;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public DevicesPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function would verify if no device text is present on screen
	 * @return
	 */
	public boolean verifyNoDeviceText() {
		try {
			if (no_device_text.isDisplayed()) {
				noDeviceText = true;
			}
		} catch (Exception e) {
			noDeviceText = false;
		}
		return noDeviceText;
	}

	/**
	 * Description : This function would verify if expected Device is present or not
	 * @param deviceName
	 * @return devicePresent
	 */
	public boolean verifyDevicePresent(String deviceName, String udid) {
		boolean deviceFound = false;
		int deviceCount;
		int iCounter = 0;
		int matchCounter = 0;
		String name;
		String tempString = "";
		try {
			if(verifyNoDeviceText()) {
				return false;
			} else {
				deviceCount = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
				try {
					if(!list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceCount-1).findElementsByClassName("android.widget.TextView").get(3).isDisplayed()) {
						deviceCount = deviceCount - 1;
					}
				} catch (Exception e) {
					deviceCount = deviceCount - 1;
				}
				for(int i = 0; i< deviceCount ; i++) {
					iCounter = iCounter +1;
					//getting device name
					name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
					//if name matched , take the index at which the device was present, this device index would be used in other functions
					if(name.equalsIgnoreCase(deviceName)) {
						deviceIndex = i;
						return true;
					}
					//below code would swipe on device page from bottom to top and would re-set the loop counter, so as loop would start again 
					if(deviceCount >=3 && deviceFound == false && iCounter == deviceCount) {
						//compare the last device name 
						if(name.equals(tempString)) {
							matchCounter = matchCounter +1;
						} else {
							tempString = name;
						} 
						if(matchCounter == 2) {
							break;
						}

						//swipe on device page
						//		Runtime.getRuntime().exec("adb shell input swipe 500 1000 179 179 ");
						Runtime.getRuntime().exec("adb -s " + udid + " shell input swipe 450 800 145 145");
						Thread.sleep(500);
						//resetting the counters
						iCounter = 0;
						i = -1; 
						//taking the device count 
						deviceCount = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
						//check if after swipe , the timer for the last device in list is visible or not
						try {
							if(!list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceCount-1).findElementsByClassName("android.widget.TextView").get(3).isDisplayed()) {
								deviceCount = deviceCount-1;
							}
						} catch (Exception e) {
							deviceCount = deviceCount-1;
						}
						//check if after swipe , the name for first device is visible or not
						try {
							if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(0).findElementsByClassName("android.widget.TextView").get(1).isDisplayed()) {
								i = -1; 
							}
						} catch (Exception e) {
							i = 0; 
							//deviceCount = deviceCount-1;
						}
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return devicePresent;
	}

	/**
	 * Description : This function would verify count of the devices in the account
	 * @param deviceCount
	 * @return boolean
	 */
	public int verifyAccountDeviceCount(String udid) {
		int deviceCount;
		int noOfDevices = 0;
		String name;
		String temp1 = null;
		try {
			deviceCount = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			noOfDevices = deviceCount;
			if(deviceCount >= 4) {
				for(int i = 0 ; i <= 16 ; i ++) {
					Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 1200 0 925 ");
					Thread.sleep(500);
					name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(3).
							findElementsByClassName("android.widget.TextView").get(1).getText();
					//if the name of the device is similar to the previous device name, break the loop 
					//else store the current name to the temp variable
					if(name.equals(temp1)) {
						noOfDevices = noOfDevices - 1;
						break;
					}  else {
						temp1 = name;
						noOfDevices = noOfDevices +1;
					}
				}
			}
			//after counting the device, swiping to the first device 
			for(int i = 0 ; i < 3; i++) {
				Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 300 300 500 1000");
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return noOfDevices;
	}

	/**
	 * Description : This function would verify if expected hub/place name is present or not 
	 * @param hubName
	 * @return verifyHubName
	 */
	public boolean verifyHubName(String hubName) {
		try {
			for(AndroidElement element : hub_name_list) {
				String s1 = element.getText();
				String [] s2 = s1.split(": ");
				if (s2[1].trim().equalsIgnoreCase(hubName)) {
					return true;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description : This function would tap on device when opened with passcode or password
	 * @param deviceName
	 * @return
	 */
	public DevicesPageAndroid tapOnDeviceWithSecurity(String deviceName) {
		try {
			if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1).getText().equalsIgnoreCase(deviceName)) {
				utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1));
				utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementById("text_devicelist_swipe_control"));
			} else {
				int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
				for(int i = 0 ; i < iCounter ; i++ ) {
					String	name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
					if(name.equalsIgnoreCase(deviceName)) {
						utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1));
						utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementById("text_devicelist_swipe_control"));
						break;
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This method is to acutate the device and wait till the device status changes or till any error pop-up appears on screen
	 * @param deviceName
	 * @return
	 */
	public DevicesPageAndroid deviceActuation(String deviceName) {
		String deviceStatus;
		try {
			if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1).getText().equalsIgnoreCase(deviceName)) {

				utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1));
				utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementById("text_devicelist_swipe_control"));
				//checking for any error pop-up that appears on screen , also checking for the status change of the device, if any of the condition is met, break the loop
				//hence the loop is till 120 times , waiting for 1 second  
				for (int j = 0; j< 120; j++) {
					try {
						if (error_text.isDisplayed()) {
							extentTestAndroid.log(Status.FAIL, error_text.getText());
							utilityAndroid.clickAndroid(ok_button);
							break;
						}
					} catch (Exception e) {
						//catch the exception and continue with the execution
					}

					//getting the status 
					deviceStatus = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).
							findElementsByClassName("android.widget.TextView").get(2).getText();

					//if the status is in Opening / Closing state , wait for 1 second , else break
					if(deviceStatus.toUpperCase().equals("OPENING") || deviceStatus.toUpperCase().equals("CLOSING")){ 
						Thread.sleep(1000);
					} else if(deviceStatus.toUpperCase().equals("OPEN") || deviceStatus.toUpperCase().equals("CLOSED")) {
						break;
					} else {
						try { // below is to check if the spinner on light is displayed or not, as light status is either on/off
							if(spinner_on_device.isDisplayed() || spinner_on_device.isEnabled()) {
								Thread.sleep(1000);
							}
						} catch(Exception e) {
							break;
						}
					}
				}
			} else {
				tapOnDevice(deviceName);
			}
		} catch (Exception e) {
			try {
				if (error_text.isDisplayed()) {
					extentTestAndroid.log(Status.FAIL, error_text.getText());
					utilityAndroid.clickAndroid(ok_button);
				}
			} catch (Exception ex) {
				utilityAndroid.catchExceptions(androidDriver, e);
			}
		}
		return this;
	}


	/**
	 * Description : This function would tap on device
	 * @param deviceName
	 * @return
	 */
	public DevicesPageAndroid tapOnDevice(String deviceName) {
		int iCounter;
		String name;
		String deviceStatus;
		try {
			iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				if(name.equalsIgnoreCase(deviceName)) {
					utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1));
					utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementById("text_devicelist_swipe_control"));
					//checking for any error pop-up that appears on screen , also checking for the status change of the device, if any of the condition is met, break the loop
					//hence the loop is till 120 times , waiting for 1 second  
					for (int j = 0; j< 120; j++) {
						try {
							if (error_text.isDisplayed()) {
								extentTestAndroid.log(Status.FAIL, error_text.getText());
								utilityAndroid.captureScreenshot(androidDriver);
								utilityAndroid.clickAndroid(ok_button);
								break;
							}
						} catch (Exception e) {
							//catch the exception and continue with the execution
						}
						//getting the status 
						deviceStatus = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).
								findElementsByClassName("android.widget.TextView").get(2).getText();

						//if the status is in Opening / Closing state , wait for 1 second , else break
						if(deviceStatus.toUpperCase().equals("OPENING") || deviceStatus.toUpperCase().equals("CLOSING")){ 
							Thread.sleep(1000);
						} else if(deviceStatus.toUpperCase().equals("OPEN") || deviceStatus.toUpperCase().equals("CLOSED")) {
							break;
						} else {
							try { // below is to check if the spinner on light is displayed or not, as light status is either on/off
								if(spinner_on_device.isDisplayed() || spinner_on_device.isEnabled()) {
									Thread.sleep(1000);
								}
							} catch(Exception e) {
								break;
							}
						}
					}
					break;
				}
			}
		} catch (Exception e) {
			try {
				if (error_text.isDisplayed()) {
					extentTestAndroid.log(Status.FAIL, error_text.getText());
					utilityAndroid.captureScreenshot(androidDriver);
					utilityAndroid.clickAndroid(ok_button);
				}
			} catch (Exception ex) {
				utilityAndroid.catchExceptions(androidDriver, e);
			}
		}
		return this;
	}

	/**
	 * Description : This function would get the status of the device
	 * @param deviceName
	 * @return deviceStatus
	 */
	public String getDeviceStatus(String deviceName) {
		String deviceStatus = "";
		String name;
		int iCounter;
		try {
			if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1).getText().equalsIgnoreCase(deviceName)) {
				deviceStatus = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(2).getText();
			} else {
				iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
				for(int i = 0 ; i < iCounter ; i++ ) {
					//getting the name of the device
					name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
					//Comparing the device name and getting the status
					if(name.equalsIgnoreCase(deviceName)) {
						deviceStatus = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(2).getText();
						break;
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return deviceStatus;
	}

	/**
	 * Description : This function would validate the timer displayed for devices
	 * @param deviceName
	 * @param duration
	 * @return Page object
	 */
	public DevicesPageAndroid validateDeviceTimer(String deviceName, int duration) {
		int iCounter = 0;
		try {
			deviceCount = device_container_list.size();
			for (iIndex = 0; iIndex < deviceCount; iIndex++ ) {
				//verifying the device name
				if (device_name_list.get(iIndex).getText().equalsIgnoreCase(deviceName)) {
					do {
						try {
							androidDriver.currentActivity();
							if ((device_state_timer_list.get(iIndex).getAttribute("text")).toUpperCase().contains(duration+" "+" MINUTES")) {
								break;
							} 
							//getting the device timer on screen 
							String time = device_state_timer_list.get(iIndex).getAttribute("name").split("\\s")[1];
							int device_time = Integer.parseInt(time);
							if (device_time > duration && ((device_state_timer_list.get(iIndex).getAttribute("text")).toLowerCase().contains("minutes"))){
								break;
							}
							//if the device timer contains hour or day , then break the loop
							if ((device_state_timer_list.get(iIndex).getAttribute("text")).toUpperCase().contains("HOURS") ||
									(device_state_timer_list.get(iIndex).getAttribute("text")).toUpperCase().contains("DAYS") ||
									(device_state_timer_list.get(iIndex).getAttribute("text")).toUpperCase().contains("HOUR") ||
									(device_state_timer_list.get(iIndex).getAttribute("text")).toUpperCase().contains("DAY"))  {
								break;
							}
						} catch (Exception e) {
							androidDriver.currentActivity();
							iCounter++;
							//static wait is kept to wait till the notification appears on screen
							Thread.sleep(5500);
						}
					} while ( iCounter < duration * 12);
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would the layout to grid 
	 * @return Page object
	 */
	public DevicesPageAndroid selectGridView() {
		try {
			if (grid_view_icon.isDisplayed()) {
				utilityAndroid.clickAndroid(grid_view_icon);
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would change the layout to device view
	 * @return Page object
	 */
	public DevicesPageAndroid selectDeviceView() {
		try {
			if (device_view_link.isDisplayed()) {
				utilityAndroid.clickAndroid(device_view_link);
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if blue bar is present or not
	 * @return
	 */
	public boolean verifyBlueBarPresent() {
		try {
			if (blue_bar.isDisplayed()) {
				utilityAndroid.genericWait(blue_bar, "Visible");
				return true;
			} 
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would click on the blue bar on device screen 
	 * @return boolean
	 */
	public boolean clickOnBlueBar() {
		try {
			if (verifyBlueBarPresent()) {
				utilityAndroid.clickAndroid(blue_bar);
				utilityAndroid.genericWait(cancel_button, "Visible");
				return true;
			}
			else {
				extentTestAndroid.log(Status.INFO, "There are no users except for Creator Admin" );
				return false;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description : This function would select the user role 
	 * @param userRole
	 * @return boolean
	 */
	public boolean verifyAndSelectUserRole(String activeUserRole, String activeAccountName) {
		roleSelected = false;
		try {
			if (activeUserRole.equalsIgnoreCase("Creator")) {
				activeUserRole = "Me" ;
			}
			if (clickOnBlueBar()) {
				for(iIndex = 0 ; iIndex < role_list.size(); iIndex++) {
					if (role_list.get(iIndex).getText().toUpperCase().trim().contains(activeUserRole.toUpperCase()) 
							&& role_list.get(iIndex).getText().toUpperCase().trim().contains(activeAccountName.toUpperCase())) {
						utilityAndroid.clickAndroid(role_list.get(iIndex));
						roleSelected = true;
						androidDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
						utilityAndroid.genericWait(blue_bar, "Visible");
						break;
					} else {
						roleSelected = false;
					}
				}
				if (!roleSelected) {
					clickCancelButton();					
				}
			} else {  // when blue bar is not present 
				if (activeUserRole.trim().equalsIgnoreCase("Creator") || activeUserRole.trim().equalsIgnoreCase("Me")) {
					roleSelected = true;
				} else {
					roleSelected = false;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return roleSelected;
	}

	public DevicesPageAndroid clickCancelButton() {
		try {
			utilityAndroid.clickAndroid(cancel_button);
		} catch (Exception e) {

		}
		return this;
	}

	/**
	 * Description: To verify the presence of Device home screen after login
	 * @return boolean
	 */
	public boolean verifyHomeScreen() {
		try {
			if (devices_text.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would enter the Passcode
	 * @return page object
	 */
	public DevicesPageAndroid enterPasscode(String passcode) {
		try {
			String text[ ] = passcode.split("");
			utilityAndroid.genericWait(passcode_textbox_1, "Visible");
			utilityAndroid.genericWait(passcode_textbox_1, "Click");
			utilityAndroid.clickAndroid(passcode_textbox_1);
			utilityAndroid.enterTextAndroid(passcode_textbox_1, text[0] );
			utilityAndroid.enterTextAndroid(passcode_textbox_2, text[1] );
			utilityAndroid.enterTextAndroid(passcode_textbox_3, text[2] );
			utilityAndroid.enterTextAndroid(passcode_textbox_4, text[3] );
		}catch (Exception e)  {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// 
		}
		for (int i = 0 ; i < 20; i++) {
			try {
				if(login_spinner.isDisplayed() || login_spinner.isEnabled()) {
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				break;
			}
		}
		return this;
	}

	/**
	 * Description : This function would verify the logo after login , to verify correct login
	 * @return Page object
	 */
	public boolean verifyLoginPage() {
		try {
			//utilityAndroid.waitForGenericSpinner();
			utilityAndroid.genericWait(brand_logo, "Visible");
			Assert.assertTrue(brand_logo.isEnabled(),"failed to verify login page");
			return true;
		} catch (Exception e) {	
			utilityAndroid.catchExceptions(androidDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function would verify the passcode text
	 * @return
	 */
	public boolean verifyPasscodeText() {
		try {
			if (passcode_header.isDisplayed()) {
				return true;
			}
		} catch (Exception e)  {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would add the gateway into the account on the UI
	 * @param gatewaySerialNumber
	 * @param gatewayName
	 * @return Page object
	 */
	public DevicesPageAndroid addGateway(String gatewaySerialNumber, String udid) throws InterruptedException, IOException {
		boolean addHubDisplayed = false;
		String result = "";
		String result2 = "";
		String line = "";
		StringBuffer output = new StringBuffer();

		String MyQSrNumber = gatewaySerialNumber.substring(2);
		String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
		String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
		String textBox3 = MyQSrNumber.split(" ")[0].substring(7);

		try {
			if(new_hub_btn.isDisplayed()) {
				addHubDisplayed = true;
			}
		} catch (Exception e) {
			addHubDisplayed = false;
		}

		try {
			if(!addHubDisplayed) {
				utilityAndroid.scrollToElement(new_hub_btn);
			}
			utilityAndroid.clickAndroid(new_hub_btn);
			utilityAndroid.genericWait(first_cell_textbox, "Click");
			utilityAndroid.clickAndroid(first_cell_textbox);

			//adb command to get the current(default) device keyboard type
			Process p = Runtime.getRuntime().exec("adb -s "+ udid +" shell settings get secure default_input_method");
			//below code is to convert the output of the command execute to string
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = stdInput.readLine())!= null) {
				output.append(line + "\n");
			}
			result = output.toString();

			//adb command to set the unicode keyboard
			Runtime.getRuntime().exec("adb -s "+ udid +" shell ime set io.appium.android.ime/.UnicodeIME");
			Thread.sleep(2000);
			//to enable entering through unicode keyboard
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input keyevent 23");

			utilityAndroid.enterTextAndroid(first_cell_textbox, textBox1);
			utilityAndroid.enterTextAndroid(second_cell_textbox, textBox2);
			utilityAndroid.enterTextAndroid(third_cell_textbox, textBox3);

			// concat the value of default keyboard to adb command, to set back the to original
			String command = "adb -s "+ udid +" shell ime set ".concat(result);
			//execute the command to set the keyboard to device default
			Process p2 = Runtime.getRuntime().exec(command);

			BufferedReader stdInput1 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
			while ((line = stdInput1.readLine())!= null) {
				output.append(line + "\n");
			}
			result2 = output.toString();

			utilityAndroid.clickAndroid(submit_btn);
			for (int i = 0; i< 25; i++) {
				try {
					if(error_text.isDisplayed() && error_text.getText().toLowerCase().contains("adding")){
						Thread.sleep(550);
					}
				} catch (Exception e) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would add the gateway name into text field
	 * @param name
	 * @return
	 */
	public DevicesPageAndroid enterGatewayName(String name) {
		try {
			utilityAndroid.genericWait(gateway_name_textbox, "Click");
			utilityAndroid.enterTextAndroid(gateway_name_textbox,name);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on the save button 
	 * @return
	 */
	public DevicesPageAndroid clickSaveButton() {
		try{
			utilityAndroid.clickAndroid(save_btn);
			for (int i = 0 ; i< 25 ; i++) {
				try {
					if(error_text.isDisplayed() && error_text.getText().toLowerCase().contains("saving name")){
						Thread.sleep(550);
					}
				} catch (Exception e) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the text 
	 * @return
	 */
	public boolean verifyNewSmartHubText() {
		String text = "Your MyQ Smart Garage Hub successfully connected to your Home Wi-Fi network.";
		try {
			if(myq_smart_garage_hub_text.isEnabled() && myq_smart_garage_hub_text.getText().equals(text)) {
				return true;
			} else {
				return false;
			}
		} catch(Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would add the WGDO into the account on the UI
	 * @param wgdoSerialNumber
	 * @param wgdoName , deviceName
	 * @return Page object
	 * @throws IOException 
	 * @throws ParseException 
	 * @throws ClientProtocolException 
	 */
	public DevicesPageAndroid addWGDOAndGDO(String wgdoSerialNumber,String wgdoName ,String deviceName, String udid) throws InterruptedException, IOException, ParseException {
		String result = "";
		String result2 = "";
		String line = "";
		StringBuffer output = new StringBuffer();
		String MyQSrNumber = wgdoSerialNumber.substring(2);
		String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
		String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
		String textBox3 = MyQSrNumber.split(" ")[0].substring(7);

		try {
			utilityAndroid.genericWait(new_hub_btn, "Click");
			utilityAndroid.clickAndroid(new_hub_btn);
			utilityAndroid.genericWait(first_cell_textbox, "Click");
			utilityAndroid.clickAndroid(first_cell_textbox);

			//adb command to get the current(default) device keyboard type
			Process p = Runtime.getRuntime().exec("adb -s "+ udid +" shell settings get secure default_input_method");
			//below code is to convert the output of the command execute to string
			BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
			while ((line = stdInput.readLine())!= null) {
				output.append(line + "\n");
			}
			result = output.toString();

			//adb command to set the unicode keyboard
			Runtime.getRuntime().exec("adb -s "+ udid +" shell ime set io.appium.android.ime/.UnicodeIME");
			Thread.sleep(2000);
			//to enable entering through unicode keyboard
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input keyevent 23");

			utilityAndroid.enterTextAndroid(first_cell_textbox, textBox1);
			utilityAndroid.enterTextAndroid(second_cell_textbox, textBox2);
			utilityAndroid.enterTextAndroid(third_cell_textbox, textBox3);

			// concat the value of default keyboard to adb command, to set back the to original
			String command = "adb -s "+ udid +" shell ime set ".concat(result);
			//execute the command to set the keyboard to device default
			Process p2 = Runtime.getRuntime().exec(command);
			BufferedReader stdInput1 = new BufferedReader(new InputStreamReader(p2.getInputStream()));
			while ((line = stdInput1.readLine())!= null) {
				output.append(line + "\n");
			}
			Thread.sleep(2000);

			utilityAndroid.genericWait(submit_btn, "Click");
			utilityAndroid.clickAndroid(submit_btn);
			utilityAndroid.genericWait(gateway_name_textbox, "Click");
			utilityAndroid.enterTextAndroid(gateway_name_textbox,wgdoName);
			try {
				androidDriver.hideKeyboard();
			} catch (Exception e) {
				// catch exception and continue with the execution
			}

			utilityAndroid.genericWait(textbox_device_name, "Click");
			utilityAndroid.enterTextAndroid(textbox_device_name, deviceName);
			clickSaveButton();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Remote light from the list of devices to add to a gateway / hub 
	 * @return Page object
	 */
	public DevicesPageAndroid clickRemoteLightButton() throws InterruptedException{
		try {
			utilityAndroid.genericWait(remote_light_btn, "Click");
			utilityAndroid.clearAndroid(remote_light_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the gateway from list
	 * @param gatewayName
	 * @return Page Object
	 */
	public DevicesPageAndroid selectGateway(String gatewayName, String udid){
		try {
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 500 700 1080 900");
		} catch (Exception e) {
		}
		try {
			for(int iIndex = 0; iIndex < gateway_name.size(); iIndex++) {
				if (gateway_name.get(iIndex).getText().equalsIgnoreCase(gatewayName)) {
					utilityAndroid.clickAndroid(gateway_name.get(iIndex));
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on "+" icon to add new devices
	 * @return Page Object
	 */
	public DevicesPageAndroid addNewDevice() throws InterruptedException {
		try {
			utilityAndroid.genericWait(add_device, "Click");
			utilityAndroid.clickAndroid(add_device);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on the GDO add button
	 * @return Page Object
	 */
	public DevicesPageAndroid clickGDOButton() throws InterruptedException {
		try {
			utilityAndroid.genericWait(gdo_name, "Click");
			utilityAndroid.clickAndroid(gdo_name);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on the Gate add button
	 * @return Page Object
	 */
	public DevicesPageAndroid clickGateButton() throws InterruptedException {
		try {
			utilityAndroid.genericWait(gate_name, "Click");
			utilityAndroid.clickAndroid(gate_name);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	/**
	 * Description : This function would click on the CDO add button
	 * @param 
	 * @return
	 */
	public DevicesPageAndroid clickCDOButton() throws InterruptedException {
		try {
			utilityAndroid.genericWait(cdo_name, "Click");
			utilityAndroid.clickAndroid(cdo_name);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would perform the action on the EPD to start learning on UI
	 * @return Page Object
	 */
	public DevicesPageAndroid clickNextButton() throws InterruptedException {
		try {
			utilityAndroid.genericWait(next_btn, "Click");
			utilityAndroid.clickAndroid(next_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would delete the device from the seleted hub 
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageAndroid deleteDevice(String deviceName) {
		try {
			for (iIndex = 0; iIndex < device_list.size(); iIndex++) {
				if (device_list.get(iIndex).getText().equalsIgnoreCase(deviceName)) {
					utilityAndroid.longPress(device_list.get(iIndex));
					utilityAndroid.clickAndroid(delete_device_btn);
					utilityAndroid.clickAndroid(delete_confirmation);
					break;
				} 
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enter the name of the device
	 * @param deviceName
	 * @return Page Object
	 */
	public DevicesPageAndroid enterDeviceName(String deviceName) {
		try {
			utilityAndroid.genericWait(edit_device_name, "Click");
			utilityAndroid.enterTextAndroid(edit_device_name, deviceName);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the shield icon for Strong Hold GDO	
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyShieldIcon(String deviceName) {
		int iCounter = 0;
		String name = null;
		try {
			iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				if(name.equalsIgnoreCase(deviceName)) {
					if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.ImageView").get(1).isDisplayed()) {
						return true;
					}
				}
			}
		} catch(Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify the notification received
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyNotifications(String deviceName, String heading, String text) {
		boolean notificationReceived = false;
		try {
			Thread.sleep(10000);
			for(int iIndex = 0; iIndex<notification_text.size(); iIndex++) {
				if (notification_text.get(iIndex).getText().trim().equalsIgnoreCase(heading)) {
					if (notification_text.get(iIndex+1).getText().trim().equalsIgnoreCase(text)) {
						extentTestAndroid.log(Status.INFO, notification_text.get(iIndex).getText() + " : " + notification_text.get(iIndex+1).getText());
						notificationReceived = true;
					} else if (notification_text.get(iIndex+2).getText().trim().equalsIgnoreCase(text)) {
						extentTestAndroid.log(Status.INFO,notification_text.get(iIndex).getText() + " : " + notification_text.get(iIndex+2).getText());
						notificationReceived = true;
					}
					break;
				}
			}
			if(!notificationReceived) {
				utilityAndroid.captureScreenshot(androidDriver);
			}
			return notificationReceived;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);	
		}
		return false;
	}

	/**
	 * Description : This function would wait for 2 minutes
	 * @return
	 */
	public DevicesPageAndroid waitFor2Minute() {
		try{
			for(int i = 0 ; i< 60 ; i++) {
				androidDriver.currentActivity();
				Thread.sleep(2000);
			}
		} catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the notification received
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifySecondNotifications(String deviceName, String heading, String text) {
		boolean notificationReceived = false;
		try {
			for(int iIndex = 0; iIndex<notification_text.size(); iIndex++) {
				if (notification_text.get(iIndex).getText().trim().equalsIgnoreCase(heading)) {
					if (notification_text.get(iIndex+5).getText().trim().equalsIgnoreCase(heading)) {
						if (notification_text.get(iIndex+6).getText().trim().equalsIgnoreCase(text)) {
							extentTestAndroid.log(Status.INFO, notification_text.get(iIndex).getText() + " : " + notification_text.get(iIndex+1).getText());
							notificationReceived = true;
						} else if (notification_text.get(iIndex+7).getText().trim().equalsIgnoreCase(text)) {
							extentTestAndroid.log(Status.INFO,notification_text.get(iIndex).getText() + " : " + notification_text.get(iIndex+2).getText());
							notificationReceived = true;
						}
						break;
					}
				}
			}
			if(!notificationReceived) {
				utilityAndroid.captureScreenshot(androidDriver);
			}
			return notificationReceived;
		} catch (IndexOutOfBoundsException e) {
			return false;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);	
		}
		return false;
	}

	/**
	 * Description : This function would rename the device
	 * @param deviceName
	 * @return Page Object
	 */
	public DevicesPageAndroid renameDevice(String deviceName) {
		try {
			utilityAndroid.genericWait(edit_device_name, "Click");
			utilityAndroid.clickAndroid(edit_device_name);
			utilityAndroid.enterTextAndroid(edit_device_name, deviceName );
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on yes button
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnYes() {
		try {
			utilityAndroid.genericWait(yes_button, "Click");
			utilityAndroid.clickAndroid(yes_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on tap here to select text on the page
	 * @return Page object
	 */
	public DevicesPageAndroid tapToSelect() {
		try {
			utilityAndroid.genericWait(tap_here_to_select, "Click");
			utilityAndroid.clickAndroid(tap_here_to_select);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Descitpion : This function would enter the brand name to search
	 * @param searchText
	 * @return Page object
	 */
	public DevicesPageAndroid enterTextToSearch(String searchText) {
		try {
			utilityAndroid.genericWait(type_to_search, "Visible");
			utilityAndroid.clickAndroid(type_to_search);
			utilityAndroid.enterTextAndroid(type_to_search, searchText);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on the brand of the device
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnBrandSearched() {
		try {
			utilityAndroid.genericWait(liftmaster_text, "Visible");
			utilityAndroid.clickAndroid(liftmaster_text);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the color of the button	
	 * @param color 
	 * @return Page object
	 */
	public DevicesPageAndroid selectColorOfProgramButton(String color) {
		try {
			utilityAndroid.clickAndroid(color_yellow);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on program door button
	 * @return
	 */
	public DevicesPageAndroid clickOnProgramDoor() {
		try {
			utilityAndroid.genericWait(program_door, "Click");
			utilityAndroid.clickAndroid(program_door);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on continue button
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnContinueButton() {
		try {
			utilityAndroid.genericWait(continue_button, "Click");
			utilityAndroid.clickAndroid(continue_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description  : This function would click on I HAVE PRESSED THE PROGRAM BUTTON
	 * @return  Page object
	 */
	public DevicesPageAndroid clickOnPressedProgramButton() {
		try {
			utilityAndroid.genericWait(pressed_program_button, "Click");
			utilityAndroid.clickAndroid(pressed_program_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on next button
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnNext() {
		try{
			utilityAndroid.genericWait(next_btn, "Click");
			utilityAndroid.clickAndroid(next_btn);
			for (int i = 0 ; i< 25 ; i++) {
				try {
					if(error_text.isDisplayed() && error_text.getText().toLowerCase().contains("saving name")){
						Thread.sleep(550);
					}
				} catch (Exception e) {
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		} 
		return this;
	}

	/**
	 * Description : This function would verify shield icon in grid view
	 * @param gatewayName
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyShieldInGridView(String gatewayName, String deviceName) {
		int noOfSwipes =0;
		try {
			noOfSwipes = view_pager_dots.findElementsByClassName("android.widget.ImageView").size();
		} catch (Exception e) {
			// continue with execution
		}
		try {
			if(noOfSwipes == 0)  {
				if(grid_view_gateway_name.getText().equalsIgnoreCase("HUB: ".concat(gatewayName)) ) {
					int noOfDevices = grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").size();
					for(int iCounter = 0 ; iCounter < noOfDevices; iCounter ++) {
						String name = grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").get(iCounter).findElementsByClassName("android.widget.TextView").get(0).getText();
						if(name.equalsIgnoreCase(deviceName)){
							if(grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").get(iCounter).findElementsByClassName("android.widget.ImageView").get(1).isDisplayed() ||
									grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").get(iCounter).findElementsByClassName("android.widget.ImageView").get(1).isEnabled()) {
								return true;
							}
						}
					}
				}
			} else {
				for(int tempCounter = 0 ; tempCounter < noOfSwipes ; tempCounter ++) {
					if(grid_view_gateway_name.getText().equalsIgnoreCase("HUB: ".concat(gatewayName)) ) {
						int noOfDevices = grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").size();
						for(int iCounter = 0 ; iCounter < noOfDevices; iCounter ++) {
							String name = grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").get(iCounter).findElementsByClassName("android.widget.TextView").get(0).getText();
							if(name.equalsIgnoreCase(deviceName)){
								if(grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").get(iCounter).findElementsByClassName("android.widget.ImageView").get(1).isDisplayed() ||
										grid_view_devices_list.findElementsByClassName("android.widget.LinearLayout").get(iCounter).findElementsByClassName("android.widget.ImageView").get(1).isEnabled()) {
									return true;
								}
							}
						}
						break;
					} else {
						utilityAndroid.swipeRightToLeft();
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description : This function is to verify shield icon in gallery view
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyShieldIconInGalleryView(String deviceName, String udid) throws StaleElementReferenceException {
		int totalCount;
		String nameOfDevice;
		String temp="";
		totalCount = gallery_view_devices.findElementsByXPath("*//.").size();
		for (int i = 1 ; i < totalCount; i++) {
			if(gallery_view_devices.findElementsByXPath("*//.").get(i).getAttribute("className").contains("android.widget.LinearLayout")) {
				nameOfDevice = gallery_view_devices.findElementsByXPath("*//.").get(i+2).getText();
				if(nameOfDevice.equalsIgnoreCase(deviceName)) {
					if(gallery_view_devices.findElementsByXPath("*//.").get(i+6).getAttribute("className").equalsIgnoreCase("android.widget.ImageView")) {
						if(gallery_view_devices.findElementsByXPath("*//.").get(i+6).getAttribute("resourceId").contains("device_stronghold_icon")){
							return true;
						} else	if(gallery_view_devices.findElementsByXPath("*//.").get(i+7).getAttribute("resourceId").contains("device_stronghold_icon")){
							return true;
						}
					}
				} else {
					if(temp.equals(nameOfDevice)){
						break;
					} else{
						temp = nameOfDevice;
					}
					i = 7;
					try{
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 700 770 234 770");
						Thread.sleep(150);
					} catch(Exception e){

					}
					if(gallery_view_devices.findElementsByXPath("*//.").get(i).getAttribute("className").equals("android.widget.LinearLayout")) {
						i = i -1;
					}
				}
			}
		}
		return false;
	}

	/**
	 * Description : This function would verify the yellow bar and the error text displayed 
	 * @param deviceName
	 * @param errorText
	 * @return boolean
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public boolean verifyYellowBarText(String deviceName,String errorText, String udid) throws IOException, InterruptedException {
		int i = 0;
		String name ;
		try {
			name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1).getText();
			//verify the device name
			if(name.equalsIgnoreCase(deviceName)) {
				//verify error text is displayed for this device
				if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(4).getText().equalsIgnoreCase(errorText)) {
					return true;
				}
			} else {
				int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
				for(i = 0 ; i < iCounter ; i++ ) {
					name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
					//verify the device name
					if(name.equalsIgnoreCase(deviceName)) {
						//verify error text is displayed for this device
						if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).getText().equalsIgnoreCase(errorText)) {
							return true;
						} else {
							Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 1200 0 925 ");
							Thread.sleep(150);
							if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).getText().equalsIgnoreCase(errorText)) {
								return true;
							} else {
								return false;
							}
						}
					}
				}		
			}
		} catch(StaleElementReferenceException e1) {
			int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(i = 0 ; i < iCounter ; i++ ) {
				name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				//verify the device name
				if(name.equalsIgnoreCase(deviceName)) {
					//verify error text is displayed for this device
					if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).getText().equalsIgnoreCase(errorText)) {
						return true;
					} else {
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 1200 0 925 ");
						Thread.sleep(250);
						if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).getText().equalsIgnoreCase(errorText)) {
							return true;
						} else {
							return false;
						}
					}
				}
			}	
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify the error on screen for device
	 * @param deviceName
	 * @param errorText
	 * @return boolean
	 * @throws IOException 
	 * @throws InterruptedException 
	 */
	public boolean verifyYellowBarPresent(String deviceName, String udid) throws IOException, InterruptedException {
		String name ;
		try {
			int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				 name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				//verify the device name
				if(name.equalsIgnoreCase(deviceName)) {
					//verify yellow bar present
					if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).isEnabled()) {
						return true;
					} else {
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 1200 0 925 ");
						Thread.sleep(250);
						if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).isEnabled()) {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		} catch(StaleElementReferenceException e1) {
			int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				 name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				//verify the device name
				if(name.equalsIgnoreCase(deviceName)) {
					//verify yellow bar present
					if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).isEnabled()) {
						return true;
					} else {
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 1200 0 925 ");
						Thread.sleep(150);
						if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4).isEnabled()) {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would verify if error pop-up is present on screen or not
	 * @return boolean
	 */
	public boolean verifyErrorPresent() {
		try {
			if(error_text.isDisplayed()) {
				return true;
			}
		} catch(Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would get the text of the error pop-up
	 * @return string
	 */
	public String getErrorText() {
		String tempText = null;
		try {
			tempText = error_text.getText();
			return tempText;
		} catch (Exception e) {
			return tempText;
		}
	}

	/**
	 * Description : This function would click on Ok button
	 * @return page object
	 */
	public boolean verifyOkButtonPresent(){
		try {
			if(ok_button.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/**
	 * Description : This function would click on Ok button
	 * @return page object
	 */
	public DevicesPageAndroid clickOnOkButton(){
		try {
			utilityAndroid.genericWait(ok_button, "Click");
			utilityAndroid.clickAndroid(ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would does long press for deleting the device. 
	 * @param gdoName
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnDeleteForDevice(String gdoName) {
		try {
			for (iIndex = 0; iIndex < device_list.size(); iIndex++) {
				if (device_list.get(iIndex).getText().equalsIgnoreCase(gdoName)) {
					utilityAndroid.longPress(device_list.get(iIndex));
					utilityAndroid.clickAndroid(delete_device_btn);
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on Delete confirmation popup message 
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnDeleteConfirmation(){
		try {
			utilityAndroid.genericWait(delete_confirmation, "Click");
			utilityAndroid.clickAndroid(delete_confirmation);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on no button
	 * @return Page object
	 */
	public DevicesPageAndroid clickOnNo() {
		try {
			utilityAndroid.genericWait(no_button, "Click");
			utilityAndroid.clickAndroid(no_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would perform Back to the previous page
	 * @return Page Object
	 */
	public DevicesPageAndroid clickBackButton() throws InterruptedException {
		try {
			utilityAndroid.genericWait(back_button, "visible");
			utilityAndroid.clickAndroid(back_button);
		} catch (Exception e) {
		}
		return this;
	}

	/**
	 * Description : This function would only enter GW details on the UI
	 * @param GWSerialNumber
	 * @return
	 * @throws InterruptedException
	 */
	public DevicesPageAndroid enterGWDetails(String GWSerialNumber) throws InterruptedException {

		String textBox1 = GWSerialNumber.split(" ")[0].substring(0, 4);
		String textBox2 = GWSerialNumber.split(" ")[0].substring(4, 7);
		String textBox3 = GWSerialNumber.split(" ")[0].substring(7);

		try {
			utilityAndroid.genericWait(new_hub_btn, "Click");
			utilityAndroid.clickAndroid(new_hub_btn);

			utilityAndroid.genericWait(first_cell_textbox, "Click");
			utilityAndroid.clearAndroid(first_cell_textbox);
			utilityAndroid.enterTextAndroid(first_cell_textbox, textBox1);
			utilityAndroid.enterTextAndroid(second_cell_textbox, textBox2);
			utilityAndroid.enterTextAndroid(third_cell_textbox, textBox3);

			utilityAndroid.clickAndroid(submit_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would enter enter GW details on the UI
	 * @param errorMessage
	 * @return boolean
	 * @throws InterruptedException
	 */
	public boolean verifyErrorMessage(String errorMessage) throws InterruptedException {
		boolean messageFound=false;
		try {
			for (int i=0; i<=29; i++) { //wait for popup to appear
				Thread.sleep(1000);
				try {
					if (ok_button.isDisplayed() == false ) {
						continue;
					}
					if (error_text.getText().contains(errorMessage) ) {
						messageFound=true;
					}
				} catch (Exception e) {
					continue; //nothing to log here	
				}
				break;
			}
			if (ok_button.isDisplayed()) {
				ok_button.click();
			} else {
				extentTestAndroid.log(Status.ERROR, "No popup was displayed");	
			}
			back_button.click();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return messageFound;
	}

	/**
	 * Description : This function would verify the error mark on the device
	 * @param deviceName
	 * @return Page Object
	 */
	public boolean verifyExclaimationPoint(String deviceName) {
		String name ;
		try {
			name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.TextView").get(1).getText();
			if(name.equals(deviceName)) {
				//verify error triangle icon is displayed for this device
				if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceIndex).findElementsByClassName("android.widget.ImageView").get(1).isDisplayed()) {
					return true;
				} else {
					return false;
				}
			}
			else {
				int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
				for(int i = 0 ; i < iCounter ; i++ ) {
					name = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
					//verify the device name
					if(name.equalsIgnoreCase(deviceName)) {
						//verify error triangle icon is displayed for this device
						if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.ImageView").get(1).isDisplayed()) {
							return true;
						} else {
							return false;
						}
					}
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/** Description : This function would tap on device
	 * @param deviceName
	 * @return Page Object
	 */
	public DevicesPageAndroid tapDeviceForError(String deviceName) {
		int iCounter;
		String name;
		try {
			iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				if(name.equalsIgnoreCase(deviceName)) {
					utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1));
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if No response error pop-up is displayed on screen (after 2 minutes)
	 * @return
	 * @throws InterruptedException 
	 */
	public boolean verifyErrorForDevice() throws InterruptedException {
		for (int i = 0; i < 120; i++) {
			try {
				androidDriver.currentActivity();
				if (error_heading.isDisplayed() && error_heading.getText().equalsIgnoreCase("No response")) {
					return true;
				}
			} catch (Exception e) {
				i = i + 1;
				Thread.sleep(900);
				//continue with the execution
			}
		}
		return false;
	}

	/**
	 * Description : This function would tap on device when opened with passcode or password
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageAndroid tapDevice(String deviceName) {
		int iCounter;
		String name;
		try {
			iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				if(name.equalsIgnoreCase(deviceName)) {
					utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1));
					utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementById("text_devicelist_swipe_control"));
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is used to click on the error label present below the device icon
	 * @param deviceName
	 * @return
	 */
	public DevicesPageAndroid tapOnErrorLabel(String deviceName) {
		try {
			int iCounter = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
			for(int i = 0 ; i < iCounter ; i++ ) {
				String name   = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
				//verify the device name
				if(name.equalsIgnoreCase(deviceName)) {
					utilityAndroid.clickAndroid(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(4));
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is used to verify the text present on page , and return the boolean true or false
	 * @return boolean
	 */
	public boolean verifyTroubleShootingText() {
		try {
			Thread.sleep(3000);
			utilityAndroid.genericWait(myq_troubleShooting_text, "Visible");
			if(myq_troubleShooting_text.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Description : This function would verify plus button is present or not
	 * @return boolean
	 */
	public boolean verifyPlusButton() {
		try {
			if(plus_button.isDisplayed()){
				return true;
			}
		} catch (Exception e){
			return false;
		}
		return false;
	}
	
	/**
	 * Description : This function would verify get started button is present or not
	 * @return boolean
	 */
	public boolean verifyGetStartedButton() {
		try {
			if(get_started_button.isDisplayed()){
				return true;
			}
		} catch (Exception e){
			return false;
		}
		return false;
	}
	
	/**
	 * Description : This function would click on  plus button
	 * @return Page object
	 */
	public DevicesPageAndroid clickPlusButton() {
		try {
			utilityAndroid.clickAndroid(plus_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function would click on get started button
	 * @return Page object
	 */
	public DevicesPageAndroid clickGetStartedButton() {
		try {
			utilityAndroid.clickAndroid(get_started_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function would verify the device associated with specified hub
	 * @return Page object
	 */
	public boolean verifyDeviceWithHub(String hubName, String deviceName, String udid) {
		boolean deviceFound = false;
		int deviceCount;
		int iCounter = 0;
		int matchCounter = 0;
		String name = null;
		String hub;
		String tempString = "";
		try {
			if(verifyNoDeviceText()) {
				return false;
			} else {
				deviceCount = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
				try {
					if(!list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceCount-1).findElementsByClassName("android.widget.TextView").get(3).isDisplayed()) {
						deviceCount = deviceCount - 1;
					}
				} catch (Exception e) {
					deviceCount = deviceCount - 1;
				}
				for(int i = 0; i< deviceCount ; i++) {
					iCounter = iCounter +1;
					//getting hub name
					hub = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(0).getText();
					if(hub.toUpperCase().contains(hubName.toUpperCase())) {
						//getting device name
						name  = list_of_device.findElementsByClassName("android.widget.FrameLayout").get(i).findElementsByClassName("android.widget.TextView").get(1).getText();
						//if name matched , take the index at which the device was present, this device index would be used in other functions
						if(name.equalsIgnoreCase(deviceName)) {
							deviceIndex = i;
							return true;
						}
					}
					//below code would swipe on device page from bottom to top and would re-set the loop counter, so as loop would start again 
					if(deviceCount >=3 && deviceFound == false && iCounter == deviceCount) {
						//compare the last device name 
						if(name.equals(tempString)) {
							matchCounter = matchCounter +1;
						} else {
							tempString = name;
						} 
						if(matchCounter == 2) {
							break;
						}
						//swipe on device page
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 450 800 145 145");
						Thread.sleep(500);
						//resetting the counters
						iCounter = 0;
						i = -1; 
						//taking the device count 
						deviceCount = list_of_device.findElementsByClassName("android.widget.FrameLayout").size();
						//check if after swipe , the timer for the last device in list is visible or not
						try {
							if(!list_of_device.findElementsByClassName("android.widget.FrameLayout").get(deviceCount-1).findElementsByClassName("android.widget.TextView").get(3).isDisplayed()) {
								deviceCount = deviceCount-1;
							}
						} catch (Exception e) {
							deviceCount = deviceCount-1;
						}
						//check if after swipe , the name for first device is visible or not
						try {
							if(list_of_device.findElementsByClassName("android.widget.FrameLayout").get(0).findElementsByClassName("android.widget.TextView").get(1).isDisplayed()) {
								i = -1; 
							}
						} catch (Exception e) {
							i = 0; 
							//deviceCount = deviceCount-1;
						}
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}
	
	/**
	 * Description : This function would verify if success text is present on the page
	 * @return
	 */
	public boolean verifySuccessText() {
		try {
			if(success_text.isDisplayed() && success_text.getText().equalsIgnoreCase("Success!")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e){
			return false;
		}
	}
	
	/**
	 * Description : This function would click on the select and program door button
	 * @return
	 */
	public DevicesPageAndroid clickSelectAndProgramDoor() {
		try{
			utilityAndroid.genericWait(select_and_prgram_door_button, "Click");
			utilityAndroid.clickAndroid(select_and_prgram_door_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This funciton would select device from the list of device associated to particluar hub , on hub info page
	 * @param deviceName
	 * @return
	 */
	public DevicesPageAndroid selectDevice(String deviceName, String udid) {
		String nameOfDevice;
		String tempString = "";
		int matchCounter = 0;
		boolean deviceFound = false;
		try {
			deviceCount = deviceList.findElementsByClassName("android.widget.RelativeLayout").size();
			for (jIndex = 0; jIndex < deviceCount ; jIndex++) {
				nameOfDevice = deviceList.findElementsByClassName("android.widget.RelativeLayout").get(jIndex).findElementsByClassName("android.widget.TextView").get(0).getText();
				if(nameOfDevice.equalsIgnoreCase(deviceName)) {
					utilityAndroid.clickAndroid(deviceList.findElementsByClassName("android.widget.RelativeLayout").get(jIndex).findElementsByClassName("android.widget.TextView").get(0));
					break;
				} else {
					if(jIndex==(deviceCount-1) && !deviceFound) {
						String name = deviceList.findElementsByClassName("android.widget.RelativeLayout").get(deviceCount-1).findElementsByClassName("android.widget.TextView").get(0).getText();
						if(name.equals(tempString)) {
							matchCounter = matchCounter +1;
						} else {
							tempString = name;
						} // break the loop if twice the last device name is same
						if(matchCounter == 2) {
							break;
						}
						//below is to swipe to the screen
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 350 850 150 150");
						Thread.sleep(1000);
						//take the size again
						deviceCount = deviceList.findElementsByClassName("android.widget.RelativeLayout").size();
						//reset the counter after swipe to start from beginning
						if(deviceCount > 6) {
							jIndex = 0;		
						} else {
							jIndex = -1;
						}
					}
				}
			}
		} catch (Exception e) {
			
		} return this;
	}
}