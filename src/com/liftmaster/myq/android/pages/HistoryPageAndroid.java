package com.liftmaster.myq.android.pages;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;

public class HistoryPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	@AndroidFindBy(className = "android.widget.ImageView")
	public AndroidElement spinner_history;

	@AndroidFindBy(id = "text_emptymsg_text")
	public AndroidElement no_history_text;
	
	@AndroidFindBy(xpath = "//android.widget.ListView")
	public AndroidElement history;

	@AndroidFindBy(id = "delete")
	public AndroidElement delete_history;
	
	@AndroidFindBy(id = "android:id/message")
	public AndroidElement error_text;
	
	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement delete_button;
	
	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public HistoryPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function would verify the history
	 * @param time
	 * @param event
	 * @param eventText
	 */
	public boolean verifyHistory(String eventTime , String event, String eventText) {
		boolean historyPresent = false;
		String headingId;
		String headingText;
		String timeId;
		String timeText;
		String descriptionId;
		String descriptionText;
		int jIndex;
		int count;
		long timeInMilliSeconds;
		long StartTime;
		long EndTime;
		
		try {
			if(spinner_history.isDisplayed()) {
				WebDriverWait wait = new WebDriverWait(androidDriver, 30);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ImageView")));
			}
		} catch (Exception e) {
			// catch the exception and continue with execution
		}
		try{ 
			if(no_history_text.isDisplayed()) {
				extentTestAndroid.log(Status.INFO, no_history_text.getText());
				historyPresent = false;
			}
		} catch (Exception e) {
			historyPresent = true;
		}
		try {
			if(historyPresent) {
				count = history.findElementsByClassName("android.widget.TextView").size();
				headingId = history.findElementsByClassName("android.widget.TextView").get(0).getAttribute("resourceId");
				headingText= history.findElementsByClassName("android.widget.TextView").get(0).getText();

				if(headingId.contains("text_event_heading_header") && headingText.equalsIgnoreCase("TODAY")) {
					for(jIndex = 1; jIndex < count; jIndex++ ) {

						timeId = history.findElementsByClassName("android.widget.TextView").get(jIndex).getAttribute("resourceId");
						timeText = history.findElementsByClassName("android.widget.TextView").get(jIndex).getText();

						if(timeId.contains("text_event_listitem_time")) {
							//convert the time from history to milliseconds
							timeInMilliSeconds = Long.parseLong(milliseconds(timeText));
							//convert the eventTime captured to milliseconds and  + / - 120000(2 minutes) to compare
							StartTime = Long.parseLong(milliseconds(eventTime)) - 120000;
							EndTime = Long.parseLong(milliseconds(eventTime)) + 120000;

							if(timeInMilliSeconds >= StartTime && timeInMilliSeconds <= EndTime) {

								descriptionId = history.findElementsByClassName("android.widget.TextView").get(jIndex+1).getAttribute("resourceId");
								descriptionText = history.findElementsByClassName("android.widget.TextView").get(jIndex+1).getText();

								if(descriptionId.contains("text_event_listitem_description") && descriptionText.toUpperCase().contains(event.toUpperCase()) ) {
									extentTestAndroid.log(Status.INFO, descriptionText  + " at " + timeText );
									return true;
								}
							}
						}
						if(timeId.contains("text_event_heading_header")) {
							break;
						}
					}
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}

	/**
	 * Description : This function would convert Time (in String) into milliseconds
	 * @param dateString
	 * @return dateInMilliSeconds
	 */
	public String milliseconds(String dateString) {
		//Specifying the pattern of input date and time
		SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
		String dateInMilliSeconds = null;
		try{
			Date date = sdf.parse(dateString);
			//convert dateString to milliseconds and then in String
			dateInMilliSeconds = String.valueOf(date.getTime());
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return dateInMilliSeconds;
	}

	/**
	 * Description : This function would tap on delete history and verify history is deleted
	 * @return boolean
	 */
	public boolean deleteHistory() {
		boolean deleteHistory = false;
		String errorText = "This will permanently delete your entire history.";
		String successMessage = "You have no event history on your account";

		try {
			if(spinner_history.isDisplayed()) {
				WebDriverWait wait = new WebDriverWait(androidDriver, 30);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ImageView")));
			}
		} catch (Exception e) {
			// catch the exception and continue with execution
		}

		try{
			delete_history.click();
			try {
				utilityAndroid.genericWait(delete_button, "Click");
			} catch (Exception e) {
			}
			if (error_text.getText().contains(errorText)) {
				delete_button.click();
			} else {
				extentTestAndroid.log(Status.ERROR, "'" + errorText + "' popup has not appeared on the page after tapping delete button");
				return deleteHistory;
			}
			// Wait for spinner on the page
			try {
				if(spinner_history.isDisplayed()) {
					WebDriverWait wait = new WebDriverWait(androidDriver, 30);
					wait.until(ExpectedConditions.invisibilityOfElementLocated(By.className("android.widget.ImageView")));
				}
			} catch (Exception e) {
				// catch the exception and continue with execution
			}

			if (no_history_text.getText().contains(successMessage)) {
				extentTestAndroid.log(Status.INFO, "'" + successMessage + "' has appeared after clicking the delete button.");
				deleteHistory = true;
			} else {
				extentTestAndroid.log(Status.ERROR, successMessage + " has not appeared after clicking the delete button.");
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return deleteHistory;
	}
}
