package com.liftmaster.myq.android.pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class MenuPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;
	
	boolean menulinkPresent = false;
	int iIndex = 0;

	//Page factory for Menu Page Android
	@AndroidFindBy(id = "image_toolbar_nav_icon")
	public AndroidElement hamburger_icon;

	@AndroidFindBy(id = "account_view")
	public AndroidElement account_view_link;

	@AndroidFindBy(xpath = "//android.widget.ListView/android.widget.RelativeLayout/android.widget.TextView")
	public List<AndroidElement> menu_items;

	@AndroidFindBy(id = "text_toolbar_title")
	public AndroidElement toolbar_text;

	@AndroidFindBy(id = "button_mu_onboarding_skip")
	public AndroidElement skip_button;

	@AndroidFindBy(id = "Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "image_mu_onboarding_overlay")
	public AndroidElement onboarding_screen;

	@AndroidFindBy(id = "account_view")
	public AndroidElement user_account_name;
	
	/**
	 * Description : Class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public MenuPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description :  This function would click in hamburger icon (menu icon)
	 * @return Page object
	 */
	public MenuPageAndroid clickHamburger() {
		try {
			utilityAndroid.clickAndroid(hamburger_icon);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function is to verify if hamburger is present or not
	 * @return boolean
	 */
	public boolean verifyHamburgerPresent() {
        try {
            if(hamburger_icon.isDisplayed()) {
                return true;
            }
        } catch (Exception e) {
            return false;    
        }
        return false;
    }
	
	/**
	 * Description : This function would verify and click on specific link under menu from
	 * : Device Management / Devices / History / Users / Alerts / Schedules / Help
	 * @param menuLink
	 * @return boolean
	 */
	public boolean verifyAndClickMenuLink(String menuLink) {
		boolean menulinkPresent = false;
		try {
				utilityAndroid.clickAndroid(hamburger_icon);
				try {
					if (onboarding_screen.isDisplayed()) {
						utilityAndroid.clickAndroid(skip_button);
						androidDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
					}
				} catch (Exception e) {
					// catch exception and proceed with the execution
				}
				for(AndroidElement element : menu_items) {
					if (element.getText().trim().equals(menuLink)) {
						utilityAndroid.clickAndroid(element);
						try {
							if (onboarding_screen.isDisplayed()) {
								utilityAndroid.clickAndroid(skip_button);
								utilityAndroid.genericWait(hamburger_icon, "Click");
							}
						} catch (Exception e) {
							// catch exception and proceed with the execution
						}
						menulinkPresent = true;
						break;
					}	
				}
			if (!menulinkPresent) {
				utilityAndroid.clickAndroid(hamburger_icon);
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return menulinkPresent;
	}
	
	public MenuPageAndroid clickMenuLink(String menuLink) {
		try {
			utilityAndroid.clickAndroid(hamburger_icon);
			try {
				if (onboarding_screen.isDisplayed()) {
					utilityAndroid.clickAndroid(skip_button);
					androidDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
				}
			} catch (Exception e) {
				// catch exception and proceed with the execution
			}
			Thread.sleep(1000);
			for(AndroidElement element : menu_items) {
				if (element.getText().trim().equals(menuLink)) {
					utilityAndroid.clickAndroid(element);
					try {
						if (onboarding_screen.isDisplayed()) {
							utilityAndroid.clickAndroid(skip_button);
							androidDriver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
						}
					} catch (Exception e) {
						// catch exception and proceed with the execution
					}
					break;
					
				}	
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would click on user account view
	 * @return Page object
	 */
	public MenuPageAndroid tappingAccountView() {
		try { 
			utilityAndroid.genericWait(hamburger_icon, "Click");
			utilityAndroid.clickAndroid(hamburger_icon);
			try {
				if (onboarding_screen.isDisplayed()) {
					utilityAndroid.clickAndroid(skip_button);
					utilityAndroid.genericWait(hamburger_icon, "Click");
				}
			} catch (Exception e) {
				// catch exception and proceed with the execution
			}
			utilityAndroid.clickAndroid(account_view_link); 
			utilityAndroid.waitForGenericSpinner();
			try {
				if (spinner.isDisplayed()) {
					utilityAndroid.genericWait(hamburger_icon, "Click");
				}
			} catch (Exception e) {
				// catch exception and proceed with the execution
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function would verify user name text at bottom of hamburger menu
	 * @return String
	 */
	public String getUserName() {	
		String text = null;
		try {
			text = user_account_name.findElementsByXPath(".//*").get(2).getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return text;
	}
}