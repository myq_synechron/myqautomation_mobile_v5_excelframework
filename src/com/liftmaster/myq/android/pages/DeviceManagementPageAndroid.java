package com.liftmaster.myq.android.pages;

import java.util.List;
import java.util.Set;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DeviceManagementPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	int hubCount;
	int deviceCount;
	int iIndex;
	int jIndex;
	boolean editSuccess = false;
	boolean editDeviceNameSuccess = false;

	//Page factory for Device Management Page Android
	@AndroidFindBy(id = "item_title")
	public List<AndroidElement> hub_list;

	@AndroidFindBy(id = "item_title")
	public List<AndroidElement> device_list;

	@AndroidFindBy(className = "android.widget.EditText")
	public AndroidElement hub_name_text_box;

	@AndroidFindBy(id = "edit_gateway")
	public AndroidElement pencil_icon;

	@AndroidFindBy(id = "edit_deviceedit_device_name")
	public AndroidElement device_name_text_box;

	@AndroidFindBy(id = "button_right")
	public AndroidElement save_button;

	@AndroidFindBy(xpath = "//*[@class='android.widget.ImageButton' and @content-desc='Navigate up']" )
	public AndroidElement back_btn;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement error_popup;
	
	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement ok_button;
	
	@AndroidFindBy(id = "text_add_device_help")
	public AndroidElement help_link;
	
	@AndroidFindBy(id = "fragment_webview_progressbar_spinner")
	public AndroidElement spinner;
	
	@AndroidFindBy(xpath = "//android.widget.ListView")
	public AndroidElement deviceList;
	
	@AndroidFindBy(xpath = "//android.widget.ScrollView")
	public AndroidElement hubinfo;
	
	@AndroidFindBy(xpath = "//android.widget.LinearLayout")
	public AndroidElement listofhubs;
	
	@AndroidFindBy(id = "android:id/empty")
	public AndroidElement no_device_text;
	
	@AndroidFindBy(xpath = "//*[@class='android.widget.TextView' and @text='Delete']")
	public AndroidElement delete_btn;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement delete_confirmation;
	
	@AndroidFindBy (id = "gateway_devices_header")
	public AndroidElement devices_heading;
	
	/**
	 * Description : Class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public DeviceManagementPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver,extentTestAndroid);
	}

	/**
	 * Description : This function would verify if no device text is present or not
	 * @return boolean
	 */
	public boolean verifyNoDeviceTextPresent() {
		try {
			if(no_device_text.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;			
		}
		return false;
	}
	
	/**
	 * Description : This function would verify if the device is present after deletion
	 * @param deviceName
	 * @return
	 */
	public boolean verifyDeviceDeleted(String deviceName, String udid) {
		String nameOfDevice;
		String tempString = null;
		int matchCounter = 0 ;
		boolean deviceFound = false;
		try {
			if(verifyNoDeviceTextPresent()) {
				return true;
			} else {
				deviceCount = deviceList.findElementsByClassName("android.widget.RelativeLayout").size();
				for (jIndex = 0; jIndex < deviceCount ; jIndex++) {
					nameOfDevice = deviceList.findElementsByClassName("android.widget.RelativeLayout").get(jIndex).findElementsByClassName("android.widget.TextView").get(0).getText();
					if(nameOfDevice.equalsIgnoreCase(deviceName)) {
						return false;
					} else {
						if(jIndex==(deviceCount-1) && !deviceFound) {
							String name = deviceList.findElementsByClassName("android.widget.RelativeLayout").get(deviceCount-1).findElementsByClassName("android.widget.TextView").get(0).getText();
							if(name.equals(tempString)) {
								matchCounter = matchCounter +1;
							} else {
								tempString = name;
							}
							// if twice the last device name is same, return true as device not found
							if(matchCounter == 2) {
								return true;
							}
							//below is to swipe to the screen
							Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 350 850 150 150");
							Thread.sleep(1000);
							//take the size again
							deviceCount = deviceList.findElementsByClassName("android.widget.RelativeLayout").size();
							//reset the counter after swipe to start from beginning
							if(deviceCount > 6) {
								jIndex = 0;		
							} else {
								jIndex = -1;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return false;
	}
	
	/**
	 * Description : This function would enter the place name into text field
	 * @param hubName
	 * @return Page object
	 */
	public DeviceManagementPageAndroid enterPlaceName(String hubName){
		try {
			utilityAndroid.genericWait(hub_name_text_box, "Click");
			utilityAndroid.clearAndroid(hub_name_text_box);
			utilityAndroid.enterTextAndroid(hub_name_text_box , hubName);
		} catch (Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();			
		} catch (Exception e) {
		}
		return this;
	}
	
	/**
	 * Description : This function would enter the end point name into text field
	 * @param deviceName
	 * @return Page object
	 */
	public DeviceManagementPageAndroid enterEndPointDeviceName(String deviceName){
		try {
			utilityAndroid.genericWait(device_name_text_box, "Click");
			utilityAndroid.clearAndroid(device_name_text_box);
			utilityAndroid.enterTextAndroid(device_name_text_box , deviceName);
		} catch (Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();			
		} catch (Exception e) {
		}
		return this;
	}
	/**
	 * Description : This function would click on save button
	 * @return Page object
	 */
	public DeviceManagementPageAndroid clickSave(){
		try {
			utilityAndroid.clickAndroid(save_button);
			for (int i = 0 ; i< 25 ; i++) {
				try {
					if(error_popup.isDisplayed() && error_popup.getText().toLowerCase().contains("saving name")){
						Thread.sleep(550);
					}
				} catch (Exception e) {
					break;
				}
			}
		} catch (Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function would select the specified place name
	 * @param hubName
	 * @param udid
	 * @return Page Object
	 */
	public DeviceManagementPageAndroid selectPlace(String hubName, String udid) {
		boolean hubFound = false;
		String tempString= "";
		String name ; 
		int matchCounter = 0;
		try {
			hubCount = hub_list.size();
			for (iIndex = 0; iIndex < hubCount-1; iIndex++) {
				//check if the Hub name exist in the list or not
				if (hub_list.get(iIndex).getText().trim().equalsIgnoreCase(hubName)) {
					utilityAndroid.clickAndroid(hub_list.get(iIndex));
					break;
				} else {
					if(iIndex==(hubCount-2) && !hubFound) {
						name = hub_list.get(iIndex).getText();
						if(name.equals(tempString)) {
							matchCounter = matchCounter + 1;
						} else {
							tempString = name;
						} // break the loop if twice the last device name is same
						if(matchCounter == 2) {
							break;
						} else {
							//below is to swipe to the screen
							Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 350 850 150 150");
							Thread.sleep(1000); 
							//take the size again
							hubCount =  hub_list.size();
							//reset the counter after swipe to start from beginning
							iIndex = 0;		
						}
					}
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	

	/**
	 * Description : This function would select the specified end pint device name
	 * @param deviceName
	 * @param udid
	 * @return Page Object
	 */
	public DeviceManagementPageAndroid selectEndPontDevice(String deviceName, String udid) {
		int deviceCount = 0 ; 
		int matchCounter = 0;
		boolean deviceFound = false;
		String nameOfDevice ;
		String tempString = "";
		deviceCount = device_list.size();
		try {
		for (iIndex = 0; iIndex < deviceCount; iIndex++) {
			nameOfDevice = device_list.get(iIndex).getText();
			//check if the Hub name exist in the list or not
			if (nameOfDevice.trim().equalsIgnoreCase(deviceName)) {
				utilityAndroid.clickAndroid(device_list.get(iIndex));
				break;
			} else {
				if(iIndex==(deviceCount-2) && !deviceFound) {
					nameOfDevice = hub_list.get(iIndex).getText();
					if(nameOfDevice.equals(tempString)) {
						matchCounter = matchCounter + 1;
					} else {
						tempString = nameOfDevice;
					} // break the loop if twice the last device name is same
					if(matchCounter == 2) {
						break;
					} else {
						//below is to swipe to the screen
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 350 850 150 150");
						Thread.sleep(1000); 
						//take the size again
						deviceCount = device_list.size();
						//reset the counter after swipe to start from beginning
						iIndex = 0;		
					}
				}
			}
		}
	} catch (Exception e) {
		utilityAndroid.catchExceptions(androidDriver, e);
	}
	return this;
}

	/**
	 * Description : This function would verify if expected hub name is present or not 
	 * @param hubName
	 * @param udid
	 * @return boolean
	 */
	public boolean verifyHubName(String hubName, String udid) {
		String currentName;
		String tempString = null;
		int matchCounter = 0;
		boolean hubFound = false;
		try {
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 500 700 1080 900");
			utilityAndroid.genericWait(devices_heading, "Visible");
		} catch (Exception e) {
		}
		try {
			hubCount = hub_list.size();
			for (iIndex = 0; iIndex < hubCount-1; iIndex++) {
				//check if the Hub name exist in the list or not
				currentName = hub_list.get(iIndex).getText().trim(); 
				if (currentName.equalsIgnoreCase(hubName)) {
					hubFound = true;
					return hubFound;
				} else {
					if(iIndex==(hubCount-2) && !hubFound) {
						if(currentName.equals(tempString)) {
							matchCounter = matchCounter + 1;
						} else {
							tempString = currentName;
						} // break the loop if twice the last device name is same
						if(matchCounter == 2) {
							break;
						} else {
							//below is to swipe to the screen
							Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 350 850 150 150");
							Thread.sleep(1000);
							//take the size again
							hubCount =  hub_list.size();
							//reset the counter after swipe to start from beginning
							iIndex = 0;		
						}
					}
				}
			}
			return hubFound;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would verify if expected device name is present or not 
	 * @param deviceName
	 *  @param udid
	 * @return boolean
	 */
	public boolean verifyDevice(String deviceName, String udid) {
		int deviceCount = 0 ; 
		int matchCounter = 0;
		boolean deviceFound = false;
		String nameOfDevice ;
		String tempString = "";
		deviceCount = device_list.size();
		try {
		for (iIndex = 0; iIndex < deviceCount; iIndex++) {
			nameOfDevice = device_list.get(iIndex).getText();
			//check if the Hub name exist in the list or not
			if (nameOfDevice.trim().equalsIgnoreCase(deviceName)) {
				deviceFound =true;
				break;
			} else {
				if(iIndex==(deviceCount-2) && !deviceFound) {
					nameOfDevice = hub_list.get(iIndex).getText();
					if(nameOfDevice.equals(tempString)) {
						matchCounter = matchCounter + 1;
					} else {
						tempString = nameOfDevice;
					} // break the loop if twice the last device name is same
					if(matchCounter == 2) {
						break;
					} else {
						//below is to swipe to the screen
						Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 350 850 150 150");
						Thread.sleep(1000); 
						//take the size again
						deviceCount = device_list.size();
						//reset the counter after swipe to start from beginning
						iIndex = 0;		
					}
				}
			}
		}
		return deviceFound;
		} catch (Exception e) {
			return false;
		}
	}

	
	/**
	 * Description : This function would delete the hub / gateway
	 * @param hubName
	 * @return
	 */
	public DeviceManagementPageAndroid deleteHub(String hubName, String udid) {
		try {
			for (iIndex = 0; iIndex < hub_list.size(); iIndex++) {
				if (device_list.get(iIndex).getText().equalsIgnoreCase(hubName)) {
					utilityAndroid.longPress(hub_list.get(iIndex));
					utilityAndroid.clickAndroid(delete_btn);
					utilityAndroid.clickAndroid(delete_confirmation);
					break;
				} 
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 500 700 1080 900");
		} catch (Exception e) {
			
		}
		return this;
	}
	
	/**
	 * Description : This function would tap on HubName
	 * @param hubName
	 * @return Page Object
	 */
	public DeviceManagementPageAndroid tapOnHubName(String hubName, String udid) {
		try {
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 500 700 1080 900");
		} catch (Exception e) {
		}
		try {
			for(int iIndex = 0; iIndex < hub_list.size(); iIndex++) {
				if (hub_list.get(iIndex).getText().equalsIgnoreCase(hubName)) {
					utilityAndroid.clickAndroid(hub_list.get(iIndex));
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}
	
	/**
	 * Description : This function is to verify error popup on deleting the GDO form WGDO 
	 * @return boolean
	 */
	public boolean verifyErrorPopup() {
		//below would wait for the error message pop-up
		try {
			utilityAndroid.genericWait(ok_button, "Visible");			
		} catch (Exception e) {
		}
		try {
			if(error_popup.isDisplayed() || error_popup.isEnabled()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Description : This function would get the text of the error pop and return the string
	 * @return text
	 */
	public String getPopUpText() {
		String text = null;
		try {
			text = error_popup.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return text;
	}

	/**
	 * Description : This function would click on OK button 
	 * @return
	 */
	public DeviceManagementPageAndroid clickOkButton() {
		try {
			utilityAndroid.clickAndroid(ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function will click on help link on device management screen
	 * @return page object
	 */
	public DeviceManagementPageAndroid clickOnHelpLink(){
		try{
			utilityAndroid.clickAndroid(help_link);
			utilityAndroid.waitForGenericSpinner();
		} catch(Exception e){
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the text passed on the LiftMaster help page navigated from Device Management
	 * 				help link available from the bottom  
	 * @param helpText
	 * @return boolean
	 */
	public boolean verifyTextOnPage(String helpText){
		boolean textPresent = false;
		try {
			//The below wait is required as the page launch takes time to load
			Thread.sleep(10000);
			Set<String> contextName = androidDriver.getContextHandles();
			for (String contexts : contextName) {
				if (contextName.contains("NATIVE_APP")) {
					extentTestAndroid.log(Status.INFO,"Context is: " + contextName);
					waitForHelpSpinner();
					androidDriver.context("NATIVE_APP");
						if (androidDriver.findElementByXPath("//*[@class='android.view.View' and @text='" + helpText + "']").isDisplayed()) {
							extentTestAndroid.log(Status.INFO, "'" + helpText + "' is present on the page." );
							textPresent = true;
						} else {
							extentTestAndroid.log(Status.INFO, "'" + helpText + "' is NOT present on the page." );
							textPresent = false;
							break;
						}
					return textPresent;
				} else {
					extentTestAndroid.log(Status.INFO, "How to Use the MyQ�App is not present on the page" );
					return textPresent;
				}
			}
		} catch (Exception e) {
			utilityAndroid.captureScreenshot(androidDriver);
		}
		return textPresent;
	}

	/**
	 * Description : This function would wait for the spinner appearing on web page 
	 */
	public void waitForHelpSpinner() {
		for (int jIndex = 0; jIndex < 25 ; jIndex++) {
			try {
				if(spinner.isDisplayed()) {
					Thread.sleep(2000);
				}
			} catch (Exception e) {
				//No exception log required
			}
		}
	}

	/**
	 * Description : This function is to click on help link on device management screen
	 * @return page 
	 */
	public DeviceManagementPageAndroid clickOnBackButton(){
		try{
			utilityAndroid.clickAndroid(back_btn);
		} catch(Exception e){
		}
		return this;
	}

}