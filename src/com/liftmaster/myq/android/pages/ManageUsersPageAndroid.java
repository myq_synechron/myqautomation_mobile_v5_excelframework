package com.liftmaster.myq.android.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ManageUsersPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;

	//Page factory for Manage Users Page Android
	@AndroidFindBy(id = "invite_new_user")
	public AndroidElement add_user_btn;

	@AndroidFindBy(id = "user_email")
	public AndroidElement user_email_textbox;

	@AndroidFindBy(id = "confirm_email")
	public AndroidElement confirm_user_email_textbox;

	@AndroidFindBy(id = "send")
	public AndroidElement send_btn;

	@AndroidFindBy(id = "spinner_access_level")
	public AndroidElement user_dropdown;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Family']")
	public AndroidElement user_family;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Guest']")
	public AndroidElement user_guest;

	@AndroidFindBy(xpath = "//android.widget.CheckedTextView[@text='Admin']")
	public AndroidElement user_admin;

	@AndroidFindBy(id = "ok")
	public AndroidElement done_btn;

	@AndroidFindBy(id = "delete")
	public AndroidElement deleteUser_btn;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement delete_yes_btn;

	@AndroidFindBy(id = "android:id/button2")
	public AndroidElement delete_cancel_btn;

	@AndroidFindBy(className = "android.widget.ImageButton")
	public AndroidElement back_btn;

	@AndroidFindBy(id = "contacts")
	public AndroidElement select_from_contact_link;

	@AndroidFindBy(id = "com.samsung.android.contacts:id/dialpad_search_result_layout")
	public AndroidElement phone_contact;

	@AndroidFindBy(id = "android:id/message")
	public AndroidElement error_message;

	@AndroidFindBy(id = "android:id/alertTitle")
	public AndroidElement error_heading;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement ok_button;

	@AndroidFindBy(id = "android:id/text1")
	public List<AndroidElement> user_list;

	@AndroidFindBy(id = "android:id/button1")
	public AndroidElement changerole_yes_btn;

	@AndroidFindBy(id = "item_status")
	public AndroidElement current_role;

	@AndroidFindBy(id = "android:id/text1")
	public AndroidElement role_before_change;

	@AndroidFindBy(id = "item_title")
	public List<AndroidElement> added_user_list;

	@AndroidFindBy(id = "item_status")
	public List<AndroidElement> added_user_status;

	@AndroidFindBy(id = "text_emptymsg_text")
	public AndroidElement no_user_text;

	@AndroidFindBy(id = "Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "resend")
	public AndroidElement resend_invitation_button;


	String invalidEmailError  = "Please enter a valid email address.";
	String emailMismatchError = "Email addresses do not match.";
	String alreadySentError   = "It looks like you already sent this user an invitation.";
	String errorHeading       = "Error";
	String alreadySentHeading = "Invitation already Sent";
	String selfSentInviteError = "You can't invite yourself!";
	String no_added_user_text = "You have no users invited to your account";
	String maxUserErrorHeading = "Maximum Users Allowed";
	String maxUserErrorText = "You have reached the maximum number of users allowed. Delete an existing user to invite a new one.";
	String alreadyAcceptedError = "It looks like this user aleady accepted invitation from you.";
	String alreadyAcceptedHeading = "User already Accepted";
	int iIndex = 0;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public ManageUsersPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver = androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
	}

	/**
	 * Description : This function enters email 
	 * @param invitation_Email
	 *  @return Page object
	 */
	public ManageUsersPageAndroid enterUserEmail(String invitation_Email) {
		try {
			utilityAndroid.enterTextAndroid(user_email_textbox, invitation_Email);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function enters confirmation email in text box
	 * @param confirm_Invitation_Email
	 *  @return Page object
	 */
	public ManageUsersPageAndroid enterConfirmUserEmail(String confirm_Invitation_Email) {
		try {
			utilityAndroid.enterTextAndroid(confirm_user_email_textbox, confirm_Invitation_Email);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		try {
			androidDriver.hideKeyboard();
		} catch (Exception e) {
			// catch exception and continue with the execution
		}
		return this;
	}

	/**
	 * Description : This function clicks on list
	 * @param confirm_Invitation_Email
	 *  @return Page object
	 */
	public ManageUsersPageAndroid clickOnUserDropdown() {
		try {
			utilityAndroid.clickAndroid(user_dropdown);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This Method is to select user as Guest/Admin/Family from the drop down list
	 *  @param role
	 *  @return Page object
	 */
	public ManageUsersPageAndroid selectUserRole(String role) {
		try {
			for(iIndex = 0 ; iIndex < user_list.size(); iIndex++) {
				if (user_list.get(iIndex).getText().trim().equalsIgnoreCase(role)) {
					utilityAndroid.clickAndroid(user_list.get(iIndex));
					break;
				}
			}
		}catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This Method is to click on "+" symbol on User's page
	 *  @return Page object
	 */
	public ManageUsersPageAndroid clickOnAddUserbtn() {
		try {
			utilityAndroid.genericWait(add_user_btn, "Click");
			utilityAndroid.clickAndroid(add_user_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function taps on send
	 *  @return Page object
	 */
	public ManageUsersPageAndroid clickOnSendButton() {
		try {
			utilityAndroid.clickAndroid(send_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function taps on done button
	 *  @return Page object
	 */
	public ManageUsersPageAndroid clickOnDoneButton() {		
		try {
			utilityAndroid.genericWait(done_btn, "Click");
			utilityAndroid.clickAndroid(done_btn);
			utilityAndroid.waitForGenericSpinner();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;

	}

	/**
	 * Description : This Method is to click on back button
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnBackButton() {
		try {
			utilityAndroid.waitForGenericSpinner();
			utilityAndroid.clickAndroid(back_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the email to which invitation was sent is present on users page 
	 * @param email
	 * @return invitedEmail
	 */
	public boolean verifyInvitedUsersEmail(String email) {
		boolean invitedEmail= false;
		try {
			utilityAndroid.waitForGenericSpinner();
			for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
				if (added_user_list.get(iIndex).getText().trim().equalsIgnoreCase(email)) {
					invitedEmail = true;
					break;
				} 
			}
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return invitedEmail;
	}

	/**
	 * Description : This Method is to click on Select from contact link
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnSelectFromContacts() {
		try {
			utilityAndroid.clickAndroid(select_from_contact_link);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This Method is to click on Phone contact
	 */
	public ManageUsersPageAndroid clickOnPhoneContact() {
		try {
			utilityAndroid.clickAndroid(phone_contact);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function clicks on OK button
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnOkButton() {
		try {
			utilityAndroid.clickAndroid(ok_button);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description: This function verifies the error messages that are received while sending request
	 * @return Page object
	 */
	public boolean verifyErrorMessage() {
		boolean errorMessagePresent = false;
		try {
			if (error_heading.getText().equalsIgnoreCase(errorHeading) && error_message.getText().trim().equalsIgnoreCase(invalidEmailError)) {
				extentTestAndroid.log(Status.INFO, invalidEmailError);
				errorMessagePresent = true;
			} else if (error_heading.getText().equalsIgnoreCase(errorHeading) && error_message.getText().trim().equalsIgnoreCase(emailMismatchError)) {
				extentTestAndroid.log(Status.INFO, emailMismatchError);
				errorMessagePresent = true;
			} else if (error_heading.getText().equalsIgnoreCase(alreadySentHeading) && error_message.getText().trim().equalsIgnoreCase(alreadySentError)) {
				extentTestAndroid.log(Status.INFO, alreadySentError);
				errorMessagePresent = true;
			} else if (error_heading.getText().equalsIgnoreCase(errorHeading) && error_message.getText().trim().equalsIgnoreCase(selfSentInviteError)) {
				extentTestAndroid.log(Status.INFO,selfSentInviteError);
				errorMessagePresent = true;
			} else if (error_heading.getText().equalsIgnoreCase(maxUserErrorHeading) &&  error_message.getText().equalsIgnoreCase(maxUserErrorText)) {
				extentTestAndroid.log(Status.INFO,error_message.getText());
				errorMessagePresent = true;
			} else if (error_heading.getText().equalsIgnoreCase(alreadyAcceptedHeading) &&  error_message.getText().equalsIgnoreCase(alreadyAcceptedError)) {
                extentTestAndroid.log(Status.INFO,error_message.getText());
                errorMessagePresent = true;
            }
			utilityAndroid.genericWait(ok_button, "Visible");
			utilityAndroid.clickAndroid(ok_button);
			return errorMessagePresent;
		} catch (Exception e) {
			return errorMessagePresent;
		}	
	}
	
	/**
	 * Description : This method is to verify the error pop-up present on screen
	 * @return
	 */
	public boolean verifyErrorPopUp() {
		try {
			if(error_message.isDisplayed()) {
				return true;
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Description : This method is to get the error message text present on screen
	 * @return
	 */
	public String getErrorMessage() {
		String errorText = null ;
		try {
		errorText = error_message.getText();
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return errorText;
	}

	/**
	 * Description : This Method is to click on 'Delete' button
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnDeleteButton() {		
		try {
			utilityAndroid.genericWait(deleteUser_btn, "Click");
			utilityAndroid.clickAndroid(deleteUser_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This Method is to click on 'Confirmation Delete' button
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickDeleteConfirmation() {
		try {
			utilityAndroid.genericWait(delete_yes_btn, "Click");
			utilityAndroid.clickAndroid(delete_yes_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This Method is to verify deleted user 
	 * @param userNameOrEmail
	 * @return deletedUser
	 */
	public boolean verifyUserDeleted(String userNameOrEmail) {
		boolean deletedUser = false;
		boolean noUserText = false;
		int iIndex = 0;
		try {
			utilityAndroid.waitForGenericSpinner();
			if (no_user_text.isDisplayed() && 
					no_user_text.getText().equalsIgnoreCase(no_added_user_text)) {
				extentTestAndroid.log(Status.INFO, no_user_text.getText()); 
				noUserText = true;
				deletedUser = true;
			}
		} catch (Exception e ) {
			// catch exception and proceed with the execution
		}
		try {
			if (!noUserText) {
				for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
					if (added_user_list.get(iIndex).getText().trim().equalsIgnoreCase(userNameOrEmail)) {
						deletedUser = false;
						break;
					} else {
						deletedUser = true; 
					}
				}
			}
			return deletedUser;
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return deletedUser;
		}
	}

	/**
	 * Description : This Method is to click on added user
	 * @param userName,userRole
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnUser(String userName,String userRole) {
		try {
			utilityAndroid.waitForGenericSpinner();
			for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
				if (added_user_list.get(iIndex).getText().equalsIgnoreCase(userName) && 
						added_user_status.get(iIndex).getText().equalsIgnoreCase(userRole)) {
					utilityAndroid.clickAndroid(added_user_list.get(iIndex));
					break;
				}
			}
			return this;
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return this;
		}
	}

	/**
	 * Description : This Method is to click on added user
	 * @param email
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnInvitationSent(String emailId) {
		try {
			for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
				if (added_user_list.get(iIndex).getText().trim().equalsIgnoreCase(emailId)) {
					utilityAndroid.clickAndroid(added_user_list.get(iIndex));
					break;
				}
			}
			return this;
		}
		catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return this;
		}
	}

	/**
	 * Description : This Method is to click on confirmation button for changing the user role
	 * @param status
	 * @return Page object
	 */
	public ManageUsersPageAndroid changeRoleConfirmation() {
		try {
			utilityAndroid.clickAndroid(changerole_yes_btn);
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this; 
	}

	/**
	 * Description : This method would verify the existing role before changing
	 * @param status
	 * @return Page object
	 */
	public boolean verifyUserRole(String userRole) {
		boolean noRoleChange = false;
		if (role_before_change.getText().equalsIgnoreCase(userRole)) {
			try {
				noRoleChange = true;	
			} catch (Exception e) {
				noRoleChange = false;
				utilityAndroid.catchExceptions(androidDriver, e);
			}
		}
		return noRoleChange; 
	}

	/**
	 * Description : This Method is to verify users present
	 * @param userName
	 * @param userRole
	 * @return invitedUser
	 */
	public boolean verifyUserPresent(String userName, String userRole) {
		boolean invitedUser = false;
		int iIndex = 0;
		try {
			utilityAndroid.waitForGenericSpinner();
			for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
				if (added_user_list.get(iIndex).getText().equalsIgnoreCase(userName) &&
						added_user_status.get(iIndex).getText().equalsIgnoreCase(userRole)) {
					return true;
				}
			}
			return invitedUser;
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
			return invitedUser;
		}
	}

	/**
	 * Description : This function would verify no user invited text on manage user screen
	 * @return boolean
	 */
	public boolean verifyNoUserInvitedText() {
		try {
			if (no_user_text.isDisplayed()) {
				return true;
			} 
		} catch (Exception e) {
			return false;
		}
		return false;
	}

	/** Description : This function is to resend the invite
	 * @param email
	 * @return Page object
	 */
	public ManageUsersPageAndroid clickOnResendInvitation(String emailId) {
		int iIndex = 0;
		try {
			for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
				if (added_user_list.get(iIndex).getText().trim().equalsIgnoreCase(emailId)) {
					if (added_user_status.get(iIndex).getText().contains("Expires on")) {
						extentTestAndroid.log(Status.INFO,"Invitation is active, resending the invitation to "+emailId);						
					} else if (added_user_status.get(iIndex).getText().contains("Expired")) {
						extentTestAndroid.log(Status.INFO,"Invitation is expired,hence resending the invitation to "+emailId);	
					}
					//swipe to display the resend button
					utilityAndroid.swipingHorizontal(added_user_list.get(iIndex));
					utilityAndroid.genericWait(resend_invitation_button, "Visible");
					utilityAndroid.clickAndroid(resend_invitation_button);
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function verifies expiry date of the invitation after re-send
	 * @param email
	 * @return boolean
	 */
	public boolean verifyExpiryDate(String emailId) {
		String sysDate = "null";
		String month;
		String spiltText;
		String date;
		String year;
		int iIndex = 0;
		int counter = 0;
		boolean inviteResend = false;
		try {
			sysDate = utilityAndroid.getSystemDate();
			month = sysDate.split("\\s")[0];
			spiltText = sysDate.split("\\s")[1];
			date = spiltText.split(",")[0];
			//if the date is in single digit, system date give 01 while , on user page the date is 1
			//hence taking the substring from system date 
			if(Integer.valueOf(date)<10) {
				date = spiltText.split(",")[0].substring(1);
			}
			year = spiltText.split(",")[1];
			for (iIndex = 0 ; iIndex < added_user_list.size(); iIndex++) {
				if (added_user_list.get(iIndex).getText().trim().equalsIgnoreCase(emailId) ) {
					if ( added_user_status.get(iIndex).getText().equals("Expires on " + month +" "+ date + ", " + year)) 
					{
						counter = iIndex;
						extentTestAndroid.log(Status.INFO,"New expiry date " +added_user_status.get(iIndex).getText());
						inviteResend = true;
						break;
					}
				}
			}
			if (!inviteResend) {
				extentTestAndroid.log(Status.INFO, "Invitation status is "+added_user_status.get(counter).getText()+ " even after resending invitation to "+emailId) ;
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return inviteResend;
	}
}