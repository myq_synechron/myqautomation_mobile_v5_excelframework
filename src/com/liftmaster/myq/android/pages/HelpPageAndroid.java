package com.liftmaster.myq.android.pages;

import java.util.List;
import java.util.Set;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityAndroid;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class HelpPageAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	public ExtentTest extentTestAndroid;
	public UtilityAndroid utilityAndroid;
	public RemoteWebDriver remoteDriver;
	public AppiumDriver<AndroidElement> driver;

	@AndroidFindBy(className = "android.widget.RelativeLayout")
	public AndroidElement help;

	@AndroidFindBy(className = "android.widget.ImageButton")
	public AndroidElement back_btn;

	@AndroidFindBy(id = "fragment_webview_progressbar_spinner")
	public AndroidElement spinner;

	@AndroidFindBy(id = "item_title")
	public List <AndroidElement> help_links;

	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public HelpPageAndroid(AndroidDriver<AndroidElement> androidDriver,ExtentTest extentTestAndroid) {
		this.androidDriver=androidDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(androidDriver), this);			
		this.extentTestAndroid = extentTestAndroid;
		utilityAndroid = new UtilityAndroid(androidDriver, extentTestAndroid);
	}

	/**
	 * Description : This function would click on the back button.
	 * @return page object
	 * @throws Exception
	 */
	public HelpPageAndroid clickBackButton() {	
		try { 
			utilityAndroid.clickAndroid(back_btn);
		}catch(Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would select the Help links
	 * @param linkText
	 * @throws Exception
	 */
	public HelpPageAndroid clickOnHelpLink(String LinkText) throws InterruptedException {	
		try{
			for(AndroidElement element : help_links ) {
				if(element.getText().equalsIgnoreCase(LinkText)) {
					utilityAndroid.clickAndroid(element);
					waitForHelpSpinner();
					break;
				}
			}
		} catch (Exception e) {
			utilityAndroid.catchExceptions(androidDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify the text  
	 * @param text
	 * @return boolean
	 * @throws InterruptedException
	 */
	public boolean verifyTextOnPage(String text) throws InterruptedException {
		try {
			//The below is wait till the page opens in browser
			Thread.sleep(7000);
			Set<String> contextName = androidDriver.getContextHandles();
			for (String contexts : contextName) {
				if (contextName.contains("NATIVE_APP")) {
					extentTestAndroid.log(Status.INFO,"Context is: " + contextName);
					waitForHelpSpinner();
					androidDriver.context("NATIVE_APP");
					androidDriver.findElementByXPath("//*[@class='android.view.View' and @text='" +text+ "']").isDisplayed();
					extentTestAndroid.log(Status.INFO, text+" is Present on the page" );
					return true;
				} else if (contextName.contains("WEBVIEW")) {
					waitForHelpSpinner();
					extentTestAndroid.log(Status.INFO,"Context is: " + contextName);
					androidDriver.context("WEBVIEW");
					androidDriver.findElementByXPath("//*[@class='android.view.View' and @text='" +text+ "']").isDisplayed();
					extentTestAndroid.log(Status.INFO, text+" is Present on the page" );
					return true;	
				} else {
					extentTestAndroid.log(Status.INFO, text+" is not present on the page" );
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	/**
	 * Description : This function would verify the text specific to FAQ page
	 * @param text
	 * @return boolean
	 * @throws InterruptedException
	 */
	public boolean verifyFAQTextOnPage(String text) throws InterruptedException {
		try {
			//The below is wait till the page opens in browser
			Thread.sleep(7000);
			Set<String> contextName = androidDriver.getContextHandles();
			for (String contexts : contextName) {
				if (contextName.contains("NATIVE_APP")) {
					extentTestAndroid.log(Status.INFO,"Context is: " + contextName);
					waitForHelpSpinner();
					androidDriver.context("NATIVE_APP");
					androidDriver.findElementByXPath("//*[@class='android.widget.Button' and @text='" +text+ "']").isDisplayed();
					extentTestAndroid.log(Status.INFO, text+" is Present on the page" );
					return true;
				} else if (contextName.contains("WEBVIEW")) {
					waitForHelpSpinner();
					extentTestAndroid.log(Status.INFO,"Context is: " + contextName);
					androidDriver.context("WEBVIEW");
					androidDriver.findElementByXPath("//*[@class='android.widget.Button' and @text='" +text+ "']").isDisplayed();
					extentTestAndroid.log(Status.INFO, text+" is Present on the page" );
					return true;	
				} else {
					extentTestAndroid.log(Status.INFO, text+" is not present on the page" );
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
		return false;
	}
	
	
	/**
	 * Description : This function would wait for the spinner appearing on web page 
	 */
	public void waitForHelpSpinner() {
		for(int jIndex = 0; jIndex < 25 ; jIndex++) {
			try {
				if(spinner.isDisplayed()) {
					Thread.sleep(2000);
				}
			} catch (Exception e) {
				break;
			}
		}
	}
}
