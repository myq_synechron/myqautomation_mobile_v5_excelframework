package com.liftmaster.myq.ios.pages;

import java.util.List;

import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.testmethods.IOS;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class UserAccountPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	//Page factory for Manage Users page IOS
	@iOSFindBy(accessibility = "account_invitations_label")	
	public IOSElement account_invitations_link;

	@iOSFindBy(accessibility = "edit_info_label")	
	public IOSElement edit_info_link;

	@iOSFindBy(accessibility = "change_email_label")	
	public IOSElement change_email_link;

	@iOSFindBy(accessibility = "change_password_label")	
	public IOSElement change_password_link;

	@iOSFindBy(accessibility = "security_label")	
	public IOSElement security_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")	
	public IOSElement invitation_list;

	@iOSFindBy(accessibility = "accept_button")	
	public IOSElement accept_btn;

	@iOSFindBy(accessibility = "decline_button")	
	public IOSElement decline_btn;

	@iOSFindBy(accessibility = "You have no account invitations")	
	public IOSElement no_invitation_text;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement users_invitation_list;

	@iOSFindBy(accessibility = "name_label")
	public List<IOSElement> username_label_list;

	@iOSFindBy(accessibility = "detail_label")
	public List<IOSElement> user_role_list;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeStaticText")
	public IOSElement accepted_user_role;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[2]")
	public IOSElement account_user_email;

	@iOSFindBy(accessibility = "Save")
	public IOSElement edit_info_save_btn;

	@iOSFindBy(accessibility = "account_name_text_field")	
	public IOSElement account_name_text_box;

	@iOSFindBy(accessibility = "first_name_text_field")	
	public IOSElement first_name_text_box;

	@iOSFindBy(accessibility = "last_name_text_field")	
	public IOSElement last_name_text_box;

	@iOSFindBy(accessibility = "email_text_field")	
	public IOSElement email_text_box;

	@iOSFindBy(accessibility = "phone_text_field")	
	public IOSElement phone_text_box;

	@iOSFindBy(accessibility = "address_line_one_text_field")	
	public IOSElement address_line1_text_box;

	@iOSFindBy(accessibility = "address_line_two_text_field")	
	public IOSElement address_line2_text_box;

	@iOSFindBy(accessibility = "city_text_field")	
	public IOSElement city_text_box;

	@iOSFindBy(accessibility = "state_text_field")	
	public IOSElement state_text_box;

	@iOSFindBy(accessibility = "zip_text_field")	
	public IOSElement zip_text_box;

	@iOSFindBy(accessibility = "country_value_label")	
	public IOSElement country_link;

	@iOSFindBy(xpath = "//XCUIElementTypeScrollView/XCUIElementTypeOther/XCUIElementTypeStaticText[2]")	
	public IOSElement country_selected;

	@iOSFindBy(accessibility = "time_zone_value_label")	
	public IOSElement time_zone_link;

	@iOSFindBy(xpath = "//XCUIElementTypeScrollView/XCUIElementTypeOther[3]/XCUIElementTypeStaticText[2]")	
	public IOSElement time_zone_selected;

	@iOSFindBy(accessibility = "language_value_label")	
	public IOSElement language_link;

	@iOSFindBy(xpath = "//XCUIElementTypeScrollView/XCUIElementTypeOther[4]/XCUIElementTypeStaticText[2]")	
	public IOSElement language_selected;

	@iOSFindBy(accessibility = "Done")
	public IOSElement Done_btn;

	@iOSFindBy(accessibility = "United States")
	public IOSElement first_country_item;

	@iOSFindBy(accessibility = "Africa/Abidjan")
	public IOSElement first_time_zone_item;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_countries;

	@iOSFindBy(accessibility = "Search")
	public IOSElement search_box;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_time_zones;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_languages;

	@iOSFindBy(accessibility = "new_email_text_field")
	public IOSElement new_email_textbox;

	@iOSFindBy(accessibility = "new_email_confirm_text_field")
	public IOSElement confirm_new_email_textbox;

	@iOSFindBy(accessibility = "password_text_field")
	public IOSElement password_textbox;

	@iOSFindBy(accessibility = "submit_button")
	public IOSElement submit_btn;

	@iOSFindBy(accessibility = "Cancel")
	public IOSElement cancel_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@name ='Automatic Login']")
	public IOSElement automatic_login_toggle;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[2]/XCUIElementTypeSwitch[@name ='Passcode']")
	public IOSElement login_passcode_toggle;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[4]/XCUIElementTypeSwitch[@name ='Passcode']")
	public IOSElement account_view_passcode_toggle;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[3]/XCUIElementTypeSwitch[@name ='Email and Password']")
	public IOSElement account_view_password_toggle;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[6]/XCUIElementTypeSwitch[@name ='Passcode']")
	public IOSElement device_state_passcode_toggle;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[5]/XCUIElementTypeSwitch[@name ='Email and Password']")
	public IOSElement device_state_password_toggle;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement liftmaster_logo_in_passcode;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement chamberlain_logo_in_passcode;

	@iOSFindBy(accessibility = "Enter a new passcode to continue.")
	public IOSElement new_passcode_text;

	@iOSFindBy(className = "XCUIElementTypeSecureTextField")
	public List<IOSElement> passcode_fields;

	@iOSFindBy(accessibility = "Change Passcode")
	public IOSElement change_passcode_link ;

	@iOSFindBy(accessibility = "Enter your passcode to access your account")
	public IOSElement user_profile_passcode_msg;

	@iOSFindBy(accessibility = "current_password_text_field")
	public IOSElement current_password;

	@iOSFindBy(accessibility = "new_password_text_field")
	public IOSElement create_password;

	@iOSFindBy(accessibility = "verify_new_password_text_field")
	public IOSElement verify_password;

	@iOSFindBy(accessibility = "HamburgerMenu")	
	public IOSElement hamburger_menu_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	public IOSElement user_name_textbox;

	@iOSFindBy(accessibility = "Remove")	
	public IOSElement remove_btn;

	@iOSFindBy(accessibility = "account_invitations_badge_count")	
	public IOSElement badge_icon_text;	

	/**
	 * Description:This is the Initializer method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public UserAccountPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}	

	/**
	 * Description: To tap on the account invitations option in user profile page
	 * @return Page object
	 */
	public UserAccountPageIOS tapAccountInvitationsLink() { 
		try {
			utilityIOS.clickIOS(account_invitations_link);	
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the Edit info option in user profile page
	 * @return Page object
	 */
	public UserAccountPageIOS tapEditInfoLink() { 
		try {
			utilityIOS.clickIOS(edit_info_link);	
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the Security link option in user profile page
	 * @return Page object
	 */
	public UserAccountPageIOS tapSecurityLink() { 
		try {
			utilityIOS.clickIOS(security_link);	
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the change Email option in user profile page
	 * @return Page object
	 */
	public UserAccountPageIOS tapChangeEmailLink() { 
		try {
			utilityIOS.clickIOS(change_email_link);	
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: This function would select change password option
	 * @return page object
	 */
	public UserAccountPageIOS tappingChangePassword() {
		try { 
			utilityIOS.clickIOS(change_password_link);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
		}
		return this;
	}

	/**
	 * Description: To tap on the Invitation matching the passed username
	 * @param username
	 * @return Page object
	 */
	public UserAccountPageIOS tapOnInvitation(String username) { 
		try {
			int noOfInvitations =	invitation_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int iIndex = 0; iIndex <= noOfInvitations; iIndex++) {
				if (invitation_list.findElementByXPath("//XCUIElementTypeCell["+iIndex+"]/XCUIElementTypeStaticText[2]")
						.getText().split(" -")[0].trim().contains(username.toUpperCase())) {
					utilityIOS.clickIOS(invitation_list.findElementByXPath("//XCUIElementTypeCell["+iIndex+"]/XCUIElementTypeStaticText[2]"));
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To validate no account invitation text
	 * @return boolean
	 */
	public boolean validateNoAccountInvitation() { 
		try {
			if (no_invitation_text.isEnabled()){
				return true;
			}
		} catch (Exception e) {
			//just to catch the exception and continue with execution
			return false;
		}
		return false;
	}

	/**
	 * Description clicking on back button
	 * @return Page object
	 */
	public UserAccountPageIOS tapBackBtn() {
		try {
			utilityIOS.clickIOS(back_btn);
			utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}


	/**
	 * Description: To accept or decline the invitation
	 * @param acceptInvitation
	 * @return Page object
	 */
	public UserAccountPageIOS acceptorDeclineInvitation(boolean acceptInvitation) { 
		try {
			if (acceptInvitation) {
				utilityIOS.clickIOS(accept_btn);
				try {
					if (activity_indicator.isDisplayed()) {
						utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
					}
				} catch (Exception e) {
					//just to catch exception and proceed further
				}
			} else {
				utilityIOS.clickIOS(decline_btn);
				try {
					if (activity_indicator.isDisplayed()) {
						utilityIOS.waitForElementIOS(back_btn, "clickable");
					}
				} catch (Exception e) {
					//just to catch exception and proceed further
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To verify if the invited user userName is matching the passed UserName
	 * @param userName
	 * @param role
	 * @return invitedEmail
	 * @throws InterruptedException 
	 */
	public boolean verifyPendingInvite(String userName, String role) throws InterruptedException { 
		boolean invitedEmail = false;
		List<MobileElement> manageUserChildElements = users_invitation_list.findElementsByXPath(".//*");
		try {
			int noOfElements = manageUserChildElements.size();
			for (int iIndex = 0; iIndex < noOfElements - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")){
					if(manageUserChildElements.get(iIndex+1).getAttribute("type").equalsIgnoreCase("XCUIElementTypeStaticText")) {
						if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Pending Invitations")) {
							int noOfInvitations = username_label_list.size();
							for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
								String currentUserName = username_label_list.get(jIndex).getText().trim();
								if (currentUserName.toUpperCase().trim().contains(userName.toUpperCase().trim()) && currentUserName.toUpperCase().trim().contains(role.toUpperCase().trim())) {
									invitedEmail = true;
									break;
								}
							}
						}
					}
				}
				if (invitedEmail){
					break;
				}
			}
			return invitedEmail;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return invitedEmail;
		}	 
	}

	/**
	 * Description: To tap on the invitations matching the userName and role
	 * @param userName
	 * @param role
	 * @return Page object
	 */
	public UserAccountPageIOS tapInvite(String userName, String role) { 
		boolean invitedEmail = false;
		List<MobileElement> manageUserChildElements = users_invitation_list.findElementsByXPath(".//*");
		try {
			int noOfElements = manageUserChildElements.size();
			for (int iIndex = 0; iIndex < noOfElements - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")){
					if (manageUserChildElements.get(iIndex+2).getText().equalsIgnoreCase("Pending Invitations")) {
						int noOfInvitations = username_label_list.size();
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							String currentUserName = username_label_list.get(jIndex).getText().trim();
							if (currentUserName.toUpperCase().trim().contains(userName.toUpperCase().trim()) && currentUserName.toUpperCase().trim().contains(role.toUpperCase())) {
								utilityIOS.clickIOS(username_label_list.get(jIndex));
								invitedEmail = true;
								break;
							}
						}
					}
				}
				if (invitedEmail){
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To verify if the invited user mail is matching the passed email
	 * @param userName
	 * @param role
	 * @return acceptedInvitation
	 */
	public boolean verifyAcceptedInvite(String userName, String role) { 
		boolean acceptedInvitation = false;
		List<MobileElement> manageUserChildElements = users_invitation_list.findElementsByXPath(".//*");
		try {
			int noOfElements = manageUserChildElements.size();
			for (int iIndex = 0; iIndex < noOfElements - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")){
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Accepted Invitations")) {
						int noOfInvitations = username_label_list.size();
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							String currentUserName = username_label_list.get(jIndex).getText().trim();
							String currentRole = user_role_list.get(jIndex).getText().trim();
							if (currentUserName.toUpperCase().trim().contains(userName.toUpperCase().trim()) && currentRole.toUpperCase().trim().contains(role.toUpperCase())){
								acceptedInvitation = true;
								break;
							}
						}
					}
				}
				if (acceptedInvitation){
					break;
				}
			}
			return acceptedInvitation;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return acceptedInvitation;
		}	 
	}

	/**
	 * Description: To tap on the accepted invite matching userName and role passe
	 * @param userName
	 * @param role
	 * @return acceptedInvitation
	 */
	public boolean tapAcceptedInvite(String userName, String role) { 
		boolean acceptedInvitation = false;
		List<MobileElement> manageUserChildElements = users_invitation_list.findElementsByXPath(".//*");
		try {
			int noOfElements = manageUserChildElements.size();
			for (int iIndex = 0; iIndex < noOfElements - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")){
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Accepted Invitations")) {
						int noOfInvitations = username_label_list.size();
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							String currentUserName = username_label_list.get(jIndex).getText();
							String CurrentRole = user_role_list.get(jIndex).getText();
							if ((currentUserName.trim().equalsIgnoreCase(userName.toUpperCase().trim())) && 
									(CurrentRole.trim().equalsIgnoreCase(role.trim()))) {
								utilityIOS.clickIOS(username_label_list.get(jIndex));
								acceptedInvitation = true;
								break;
							}
						}
					}
				}
				if (acceptedInvitation){
					break;
				}
			}
			return acceptedInvitation;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return acceptedInvitation;
		}	 
	}

	/**
	 * Description: To verify if the user role is same as passed 
	 * @param role
	 * @return boolean
	 */
	public boolean verifyUserRole(String role) {
		try { 
			if (accepted_user_role.getText().trim().equalsIgnoreCase("role")) {
				utilityIOS.clickIOS(back_btn);
				return true;
			} else{
				utilityIOS.clickIOS(back_btn);
				return false;  
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return false;
		}

	}

	/**
	 * Description: To verify if the account page accessed has the email of the expected user
	 * @param email
	 * @return boolean
	 */
	public boolean verifyAccountPage(String email) {
		try { 
			if (account_user_email.getText().trim().equalsIgnoreCase(email)) {
				extentTestIOS.log(Status.INFO, "Account email is " +account_user_email.getText());
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return false;
		}
	}

	/**
	 * Description: Tapping on save button
	 * @return Page object
	 */
	public UserAccountPageIOS tapEditInfoSaveBtn() {
		try {
			utilityIOS.clickIOS(edit_info_save_btn);
			try {
				if (activity_indicator.isEnabled()) {
					utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
				}
			} catch (Exception e) {
				//To handle if a popup comes because of an error and then continuing
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: Tapping on cancel button on the Edit info section
	 * @return Page object
	 */
	public UserAccountPageIOS tapEditInfoCancelBtn() {
		try {
			utilityIOS.clickIOS(cancel_btn);
			utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To enter the Account name in the text field
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterAccountName(String newValue) {
		try { 
			utilityIOS.scroll("up");
			utilityIOS.clickIOS(account_name_text_box);
			utilityIOS.clearIOS(account_name_text_box);
			utilityIOS.enterTextIOS(account_name_text_box, newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the AccountName
	 * @return currentValue
	 */
	public String getAccountName() {
		String currentValue = null;
		try {
			currentValue = account_name_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter the First name in the text field
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterFirstName(String newValue) {
		try {
			utilityIOS.clickIOS(first_name_text_box);
			utilityIOS.clearIOS(first_name_text_box);
			utilityIOS.enterTextIOS(first_name_text_box,newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the First Name
	 * @return currentValue
	 */
	public String getFirstName() {
		String currentValue = null;
		try {
			currentValue = first_name_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter the last name in the text field
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterLastName(String newValue) {
		try {
			utilityIOS.clickIOS(last_name_text_box);
			utilityIOS.clearIOS(last_name_text_box);
			utilityIOS.enterTextIOS(last_name_text_box, newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the Last Name
	 * @return currentValue
	 */
	public String getLastName() {
		String currentValue = null;
		try {
			currentValue = last_name_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter if Email field could be edited
	 * @return boolean
	 */
	public boolean verifyEmail() {
		try {
			if (email_text_box.isEnabled()){
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}	
	}

	/**
	 * Description: To enter if phone could be edited
	 * @return Page object
	 */
	public UserAccountPageIOS enterPhone(String newValue) {
		try {
			utilityIOS.clickIOS(phone_text_box);
			utilityIOS.clearIOS(phone_text_box);
			utilityIOS.enterTextIOS(phone_text_box, newValue);
			utilityIOS.clickIOS(Done_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}
	/**
	 * Description: To get the value in Phone Field
	 * @return currentValue
	 */
	public String getPhone() {
		String currentValue = null;
		try {
			currentValue = phone_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To select the Country
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS selectCountry(String newValue) {
		String currentCountry;
		boolean CountryDisplayed = false;
		try {
			utilityIOS.clickIOS(country_link);
			utilityIOS.waitForElementIOS(first_country_item, "clickable");
			//To check if the country is displayed
			try {
				CountryDisplayed = list_of_countries.findElementByAccessibilityId(newValue).isDisplayed();
			} catch (Exception e) {
			}

			if (CountryDisplayed){
				utilityIOS.clickIOS(list_of_countries.findElementByAccessibilityId(newValue));
			}
			else{
				utilityIOS.enterTextIOS(search_box,newValue);
				int noOfCountries = list_of_countries.findElementsByXPath("XCUIElementTypeCell").size();
				if (noOfCountries>=1){
					for (int i= 1; i <= noOfCountries; i++) {
						currentCountry = list_of_countries
								.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
						if (newValue.equalsIgnoreCase(currentCountry)) {
							utilityIOS.clickIOS(list_of_countries.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
							break;
						}
					}
				} else {
					extentTestIOS.log(Status.INFO,"Country passed is not matching any items in the list");
					utilityIOS.captureScreenshot(iOSDriver);
					utilityIOS.clickIOS(back_btn);
				}
			}
			utilityIOS.waitForElementIOS(edit_info_save_btn, "Clickable");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	} 

	/**
	 * Description: To get the value of country
	 * @return currentValue
	 */
	public String getCountry() {

		String currentValue = null;
		try {
			currentValue = country_link.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter the Address line 1 
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterAddressLine1(String newValue) {
		try {
			utilityIOS.clickIOS(address_line1_text_box);
			utilityIOS.clearIOS(address_line1_text_box);
			utilityIOS.enterTextIOS(address_line1_text_box, newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the value in Address line 1
	 * @return currentValue
	 */
	public String getAddressLine1() {
		String currentValue = null;
		try {
			currentValue = address_line1_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}


	/**
	 * Description: To enter the address line 2
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterAddressLine2(String newValue) {
		try {
			utilityIOS.clickIOS(address_line2_text_box);
			utilityIOS.clearIOS(address_line2_text_box);
			utilityIOS.enterTextIOS(address_line2_text_box, newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the value in Address line 2
	 * @return currentValue
	 */
	public String getAddressLine2() {
		String currentValue = null;
		try {
			currentValue = address_line2_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter the city in the City text box
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterCity(String newValue) {
		try {
			utilityIOS.clickIOS(city_text_box);
			utilityIOS.clearIOS(city_text_box);
			utilityIOS.enterTextIOS(city_text_box, newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the value in city
	 * @return currentValue
	 */
	public String getCity() {
		String currentValue = null;
		try {
			currentValue = city_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter the State
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterState(String newValue) {
		try {
			utilityIOS.scroll("down");
			utilityIOS.clickIOS(state_text_box);
			utilityIOS.clearIOS(state_text_box);
			utilityIOS.enterTextIOS(state_text_box, newValue);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the value in State
	 * @return currentValue
	 */
	public String getState() {
		String currentValue = null;
		utilityIOS.scroll("down");
		try {
			currentValue = state_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter the Zip text box
	 * @param newValue
	 * @return Page object
	 */
	public UserAccountPageIOS enterZip(String newValue) {
		try {
			utilityIOS.scroll("down");
			utilityIOS.clickIOS(zip_text_box);
			utilityIOS.clearIOS(zip_text_box);
			utilityIOS.enterTextIOS(zip_text_box, newValue);
			utilityIOS.clickIOS(Done_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To get the value in Zip
	 * @return Page object
	 */
	public String getZip() {
		String currentValue = null;
		utilityIOS.scroll("down");
		try {
			currentValue = zip_text_box.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To select the Country
	 * @return Page object
	 */
	public UserAccountPageIOS selectTimeZone(String newValue) {
		String currentTimeZone;
		boolean timeZoneDisplayed = false;
		try {
			utilityIOS.clickIOS(time_zone_link);
			utilityIOS.waitForElementIOS(first_time_zone_item, "clickable");
			//To check if the country is displayed
			try {
				timeZoneDisplayed = list_of_time_zones.findElementByAccessibilityId(newValue).isDisplayed();
			} catch (Exception e) {
			}

			if (timeZoneDisplayed){
				utilityIOS.clickIOS(list_of_time_zones.findElementByAccessibilityId(newValue));
			}
			else{
				utilityIOS.enterTextIOS(search_box,newValue);
				int noOfTimeZones = list_of_time_zones.findElementsByXPath("XCUIElementTypeCell").size();
				if (noOfTimeZones >= 1){
					for (int i= 1; i <= noOfTimeZones; i++) {
						currentTimeZone = list_of_time_zones
								.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
						if (newValue.equalsIgnoreCase(currentTimeZone)) {
							utilityIOS.clickIOS(list_of_countries.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
							break;
						}
					}
				} else {
					extentTestIOS.log(Status.INFO,"Time Zone passed is not matching any items in the list");
					utilityIOS.captureScreenshot(iOSDriver);
					utilityIOS.clickIOS(back_btn);
				}
			}
			utilityIOS.waitForElementIOS(edit_info_save_btn, "Clickable");
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	} 

	/**
	 * Description: To get the value in time zone field
	 * @return currentValue
	 */
	public String getTimeZone() {

		String currentValue = null;
		try {
			currentValue = time_zone_link.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description To select language from the list of languages
	 * @param language
	 * @throws InterruptedException
	 */
	public UserAccountPageIOS selectLanguage(String language) {
		String currentLanguage;
		try {
			utilityIOS.clickIOS(language_link);
			int noOfLanguages = list_of_languages.findElementsByXPath("XCUIElementTypeCell").size();
			if (noOfLanguages >= 1){
				for (int i= 1; i <= noOfLanguages; i++) {
					currentLanguage =	list_of_languages.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
					if (language.equalsIgnoreCase(currentLanguage)) {
						utilityIOS.clickIOS(list_of_languages.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
						break;
					} else if ( i == noOfLanguages) {
						extentTestIOS.log(Status.INFO,"Language passded is not matching any items in the list");
						utilityIOS.captureScreenshot(iOSDriver);
						utilityIOS.clickIOS(back_btn);
					}
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}

	}


	/**
	 * Description: To get the value in language in Edit Info section
	 * @return currentValue
	 */
	public String getLanguage() {
		String currentValue = null;
		utilityIOS.scroll("down");
		try {
			currentValue = language_link.getAttribute("value");
			return currentValue;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return currentValue;
		}	
	}

	/**
	 * Description: To enter new Email in the new Email Text box
	 * @param newEmail
	 * @return Page object
	 */
	public UserAccountPageIOS enterNewEmail(String newEmail) {
		try {
			utilityIOS.clickIOS(new_email_textbox);
			utilityIOS.clearIOS(new_email_textbox);
			utilityIOS.enterTextIOS(new_email_textbox, newEmail);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}		
	}

	/**
	 * Description: To enter email in confirm new Email text box
	 * @param newEmail
	 * @return Page object
	 */
	public UserAccountPageIOS enterConfirmNewEmail(String  newEmail) {
		try {
			utilityIOS.clickIOS(confirm_new_email_textbox);
			utilityIOS.clearIOS(confirm_new_email_textbox);
			utilityIOS.enterTextIOS(confirm_new_email_textbox, newEmail);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To enter email in confirm new Email text box
	 * @param password
	 * @return Page object
	 */
	public UserAccountPageIOS enterPasswordInChangeEmail(String  password) {
		try {
			utilityIOS.clickIOS(password_textbox);
			utilityIOS.clearIOS(password_textbox);
			utilityIOS.enterTextIOS(password_textbox, password);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To click on the submit button in change Email section
	 * @return Page object
	 */
	public UserAccountPageIOS tapChangeEmailSubmitBtn() {
		try {
			utilityIOS.clickIOS(submit_btn);
			for (int i = 0; i <= 5; i++){
				try {                        
					if ((activity_indicator).isEnabled()){
						Thread.sleep(2000);
						//Here it will wait for maximum 10 second till activity indicator disappears and a popup comes
					}
				} catch (Exception e) {
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: This function would enter original password
	 * @param password
	 * @return page object
	 */
	public UserAccountPageIOS enterPassword(String password) {
		try { 
			utilityIOS.clickIOS(current_password);
			utilityIOS.enterTextIOS(current_password, password);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
		}
		return this;
	}

	/**
	 * Description: This function would enter new password
	 * @param newPassword
	 * @return page object
	 */
	public UserAccountPageIOS enterNewPassword(String newPassword) {
		try { 
			utilityIOS.clickIOS(create_password);
			utilityIOS.enterTextIOS(create_password, newPassword);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
		}
		return this;
	}

	/**
	 * Description: This function would enter in the confirm new password
	 * @param confirmPassword
	 * @return page object
	 */
	public UserAccountPageIOS enterConfirmPassword(String confirmPassword) {
		try { 
			utilityIOS.clickIOS(verify_password);
			utilityIOS.enterTextIOS(verify_password, confirmPassword);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
		}
		return this;
	}

	/**
	 * Description : This function would verify the password Criteria
	 * @Param newPassword
	 * @return boolean
	 */   
	public boolean passwordCriteriaValidation(String newPassword) {
		String pattern = "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}";
		try {
			if (newPassword.matches(pattern)) {
				return true;
			}
			else {
				return false;
			}
		}catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return false;
		}
	}

	/**
	 * Description To validate if the popup message is shown 
	 * @return alertPresent
	 */
	public boolean validatePopupPresent() {
		boolean alertPresent =false;
		try {
			iOSDriver.switchTo().alert();
			alertPresent = true;
			return alertPresent;	
		} catch (Exception e) {
			//Here catch is not done through the re-usable exception handler, as the alert might be present or not present
			return alertPresent;
		}	 
	}

	/**
	 * Description: This function would click on the submit button
	 * @return page object
	 */
	public UserAccountPageIOS clickSubmit() {

		try { 
			utilityIOS.clickIOS(submit_btn);
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
		}
		return this;
	}

	/**
	 * Description To get the text of the popup 
	 * @return alertMessage
	 */
	public String getErrorMessage() {
		String alertMessage = null;
		alertMessage = iOSDriver.switchTo().alert().getText();
		try {
			extentTestIOS.log(Status.INFO,"Alert Text is : " +alertMessage);	
			iOSDriver.switchTo().alert().accept();
			return alertMessage;
		} catch (Exception e) {
			return alertMessage;
		}	 
	}

	/**
	 * Description To set the state of Automatic login toggle
	 * @param enableAutomaticLogin
	 * @return Page object
	 */
	public UserAccountPageIOS toggleAutomaticLogin(boolean enableAutomaticLogin) {
		boolean toggleEnabled =	((automatic_login_toggle.getAttribute("value").equalsIgnoreCase("0"))? false : true);
		try {
			if ((toggleEnabled && !enableAutomaticLogin) || (!toggleEnabled && enableAutomaticLogin)) {
				utilityIOS.clickIOS(automatic_login_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the state of passcode login toggle
	 * @param enablePasscode
	 * @return Page object
	 */
	public UserAccountPageIOS togglePasscodeLogin(boolean enablePasscode) {
		boolean toggleEnabled =	((login_passcode_toggle.getAttribute("value").equalsIgnoreCase("0"))? false : true);
		try {
			if ((toggleEnabled && !enablePasscode) || (!toggleEnabled && enablePasscode)) {
				utilityIOS.clickIOS(login_passcode_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To verify if the passcode is already set 
	 * @param enableAutomaticLogin
	 * @return Page object
	 */
	public boolean verifyIfPasscodeSet() {
		try {
			if (change_passcode_link.isEnabled()) {
				return true;	
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description To tap on the Change Passcode link in security option
	 * @return Page object
	 */
	public UserAccountPageIOS tapChangePasscode() {
		try {
			utilityIOS.clickIOS(change_passcode_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the passcode under security link
	 * @param passcode
	 * @return Page object
	 */
	public UserAccountPageIOS settingPasscode(String passcode) {
		try {
			boolean liftmaster;
			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
				liftmaster = true;
			} else {
				liftmaster = false;
			}
			if ((liftmaster ? liftmaster_logo_in_passcode:chamberlain_logo_in_passcode).isEnabled() && new_passcode_text.isEnabled()) {
				int sizeOfPasscodeFields = passcode_fields.size();
				int sizeOfPasscode = passcode.length();
				int i,j;
				if (sizeOfPasscode==sizeOfPasscodeFields) {
					for (j = 0; j <= 1;j ++) {
						for (i = 0; i < sizeOfPasscodeFields; i++) {
							utilityIOS.enterTextIOS(passcode_fields.get(i),String.valueOf(passcode.charAt(i)));
						}
					}
				} else {
					extentTestIOS.log(Status.INFO, "Number of expected length for passcode and the length passed is not matching"); 	
				}
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the state of passocde toggle button for Account view
	 * @param enablePasscode
	 * @return Page object
	 */
	public UserAccountPageIOS togglePasscodeAccountView(boolean enablePasscode) {
		boolean toggleEnabled =	((account_view_passcode_toggle.getAttribute("value").equalsIgnoreCase("0"))? false : true);
		try {
			if ((toggleEnabled && !enablePasscode) || (!toggleEnabled && enablePasscode)) {
				utilityIOS.clickIOS(account_view_passcode_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}	

	/**
	 * Description To set the state of password toggle button for Account view
	 * @param enablePasswordToggle
	 * @return Page object
	 */
	public UserAccountPageIOS toggleEmailAndPasswordAccountView(boolean enablePasswordToggle) {
		boolean toggleEnabled =	((account_view_password_toggle.getAttribute("value").equalsIgnoreCase("0"))? false : true);
		try {
			if ((toggleEnabled && !enablePasswordToggle) || (!toggleEnabled && enablePasswordToggle)) {
				utilityIOS.clickIOS(account_view_password_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}	

	/**
	 * Description To set the state of passcode toggle button for device state change
	 * @param enablePasscodeToggle
	 * @return Page object
	 */
	public UserAccountPageIOS togglePasscodeDeviceStateChange(boolean enablePasscodeToggle) {
		boolean toggleEnabled =	((device_state_passcode_toggle.getAttribute("value").equalsIgnoreCase("0"))? false : true);	
		try {
			if ((toggleEnabled && !enablePasscodeToggle) || (!toggleEnabled && enablePasscodeToggle)) {
				utilityIOS.clickIOS(device_state_passcode_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the state of password toggle button for device state change
	 * @param enablePasswordToggle
	 * @return Page object
	 */
	public UserAccountPageIOS togglePasswordDeviceStateChange(boolean enablePasswordToggle) {
		boolean toggleEnabled =	((device_state_password_toggle.getAttribute("value").equalsIgnoreCase("0"))? false : true);	
		try {
			if ((toggleEnabled && !enablePasswordToggle) || (!toggleEnabled && enablePasswordToggle)) {
				utilityIOS.clickIOS(device_state_password_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To get the User Name from account info screen
	 * @return String
	 */
	public String getUserNameAccountInfo() {
		String userName = null;
		try {
			userName = user_name_textbox.getAttribute("value");
			return userName;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return userName;
		}	
	}

	/**
	 * Description: To verify if the account page has invitation badge icon or not
	 * @param email
	 * @return boolean
	 */
	public boolean verifyBadgeIcon() {
		try { 
			extentTestIOS.log(Status.INFO, "Number of invitation(s) shown in badge = "+badge_icon_text.getAttribute("label"));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description: This function would click on the remove button
	 * @return page object
	 */
	public UserAccountPageIOS clickRemove() {

		try { 
			utilityIOS.clickIOS(remove_btn);
			utilityIOS.waitForElementIOS(back_btn, "clickable");

		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
		}
		return this;
	}
}