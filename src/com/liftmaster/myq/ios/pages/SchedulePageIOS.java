package com.liftmaster.myq.ios.pages;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SchedulePageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	//Page factory for Schedule Page IOS
	@iOSFindBy(accessibility = "You have no Schedules on your account")	
	public IOSElement no_schedule_text;

	@iOSFindBy(accessibility = "schedule_add_bar_button")	
	public IOSElement add_schedule_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTextField")	
	public IOSElement schedule_name_textbox;

	@iOSFindBy(accessibility = "Add Device")	
	public IOSElement add_device_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement device_list_for_schedule;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeStaticText[@name='SELECT A STATE']")
	public IOSElement select_state_text;

	@iOSFindBy(accessibility = "Turn On")
	public IOSElement turn_on_link;

	@iOSFindBy(accessibility = "Turn Off")
	public IOSElement turn_off_link;

	@iOSFindBy(accessibility = "time_value_label")
	public IOSElement time_select;

	@iOSFindBy(accessibility = "time_zone_value_label")
	public IOSElement time_zone_select;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement time_zone_country_list;

	@iOSFindBy(accessibility = "Save")
	public IOSElement save_btn;

	@iOSFindBy(accessibility = "Cancel")
	public IOSElement cancel_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeSwitch[@label ='Push Notification']")
	public IOSElement push_notification_toggle;

	@iOSFindBy(accessibility = "Turn On")
	public IOSElement turn_on_selection;

	@iOSFindBy(accessibility = "Turn Off")
	public IOSElement turn_off_selection;

	@iOSFindBy(accessibility = "Close")
	public IOSElement close_selection;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	//	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	//	public IOSElement schedule_list;

	@iOSFindBy(accessibility = "switch_cell_name_label")
	public List<IOSElement> schedule_list;

	@iOSFindBy(accessibility = "switch_cell_switch")
	public List<IOSElement> schedule_toggle_list;

	@iOSFindBy(accessibility = "Delete")
	public IOSElement delete_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn;

	@iOSFindBy(accessibility = "days_value_label")
	public IOSElement days_selection_link;

	@iOSFindBy(xpath = "//XCUIElementTypeCell[7]/XCUIElementTypeStaticText[2]")
	public IOSElement days_selected_text;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText")
	public List<IOSElement> days_of_week;

	@iOSFindBy(xpath = "//XCUIElementTypeCell[2]")
	public IOSElement device_added_list;

	@iOSFindBy(accessibility = "Remove Device From Schedule")
	public IOSElement remove_device_btn;

	@iOSFindBy(accessibility = "HamburgerMenu")	
	public  IOSElement hamburger_menu_btn;

	/**
	 *  Description This is the Initialize method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public SchedulePageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description To Validate if no Schedules has been created on account
	 * @return boolean
	 */
	public boolean validateNoScheduleText() {
		try {
			if (no_schedule_text.isEnabled())
				return true;
		} catch (Exception e) {
			//To continue execution after capturing the exception
			return false;
		}
		return false;
	}

	/**
	 * Description To tap on the add schedule button
	 * @return Page object
	 */
	public SchedulePageIOS tapAddScheduleBtn() {
		try {
			utilityIOS.clickIOS(add_schedule_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To enter the schedule name
	 * @param scheduleName
	 * @return Page object
	 */
	public SchedulePageIOS enterScheduleName(String scheduleName) {
		try {
			utilityIOS.clearIOS(schedule_name_textbox);
			utilityIOS.enterTextIOS(schedule_name_textbox, scheduleName);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To tap on the save button on schedule screen
	 * @return Page object
	 */
	public SchedulePageIOS tapSaveBtn() {
		try {
			utilityIOS.clickIOS(save_btn);
			try {
				if (activity_indicator.isDisplayed()) {
					utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
				}
			} catch (Exception e) {
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To save the Schedule and verify Error
	 * @return Page object
	 */
	public String saveAndVerifyError() {
		String alertText =null;
		try {
			utilityIOS.clickIOS(save_btn);
			//	This below code is kept commented intentionally till a fix is provided by Appium. The version which have issue is 1.7.2
			//			WebDriverWait wait = new WebDriverWait(iOSDriver, 10);
			//			wait.until(ExpectedConditions.alertIsPresent());

			//Custom wait logic till alert is present

			for (int i = 0; i <= 10; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			alertText = iOSDriver.switchTo().alert().getText();
			extentTestIOS.log(Status.INFO,"Alert Text is : " +iOSDriver.switchTo().alert().getText());
			iOSDriver.switchTo().alert().accept();
			utilityIOS.clickIOS(cancel_btn);
			return alertText;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return alertText;
		}

	}
	/**
	 * Description To tap on the cancel button on schedule screen
	 * @return Page object
	 */
	public SchedulePageIOS tapCancelBtn() {
		try {
			utilityIOS.clickIOS(cancel_btn);
			utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description clicking on back button
	 * @return Page object
	 */
	public SchedulePageIOS tapBackBtn() {
		try {
			utilityIOS.clickIOS(back_btn);
			utilityIOS.waitForElementIOS(add_schedule_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description To Select the device and state for Schedule
	 * @param deviceName
	 * @param state
	 * @return Page object
	 */
	public SchedulePageIOS selectDeviceStateForSchedule(String deviceName, String state) throws InterruptedException {
		String currentDevice;
		int  value =1;
		boolean devicePresent = false;
		try {
			if (!(deviceName.isEmpty()) && !(state.isEmpty())) {
				//selecting the device
				utilityIOS.clickIOS(add_device_btn);
				int noOfDevice = device_list_for_schedule.findElementsByXPath("XCUIElementTypeCell").size();
				for (int iVal = 1; iVal <= noOfDevice; iVal++) {
					currentDevice = device_list_for_schedule
							.findElementByXPath("XCUIElementTypeCell[" + iVal + "]/XCUIElementTypeStaticText").getText();
					if (deviceName.equalsIgnoreCase(currentDevice)) {
						utilityIOS.clickIOS(device_list_for_schedule.findElementByXPath("XCUIElementTypeCell[" + iVal + "]"));
						devicePresent = true;
						break;
					}
					if(iVal==13) {
						utilityIOS.swipe(device_list_for_schedule, "up");
						Thread.sleep(4000);
						value++;
						noOfDevice = device_list_for_schedule.findElementsByXPath("XCUIElementTypeCell").size();
						if(value==3) {
							break;
						}
					}
				}

				//selecting the state
				if (devicePresent) {
					utilityIOS.waitForElementIOS(select_state_text,"visibility");	
					state = state.toUpperCase();
					if (state.contains("ON")) {
						utilityIOS.clickIOS(turn_on_selection);	
					} else if (state.contains("OFF")) {
						utilityIOS.clickIOS(turn_off_selection);	
					} else if (state.contains("CLOSE")) {
						utilityIOS.clickIOS(close_selection);	
					} else {
						extentTestIOS.log(Status.INFO, "Not a valid state");
						utilityIOS.clickIOS(back_btn);
						utilityIOS.clickIOS(back_btn);
					}
				} else {
					utilityIOS.clickIOS(back_btn);
					extentTestIOS.log(Status.INFO, "Device not present");
				}
			} else {
				extentTestIOS.log(Status.INFO, "Not a valid state name or device name");
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To Remove the device
	 * @return Page object
	 */
	public SchedulePageIOS removeDeviceFromSchedule() throws InterruptedException {
		int noOfDevice;
		try {
			noOfDevice = device_added_list.findElementsByXPath("XCUIElementTypeStaticText").size();
			if (noOfDevice > 1) {
				for (int i = 1; i <= (noOfDevice/2); i++) {
					utilityIOS.clickIOS(device_added_list.findElementByXPath("//XCUIElementTypeStaticText[1]"));
					utilityIOS.clickIOS(remove_device_btn);
					iOSDriver.switchTo().alert().accept();
				}
			} else {
				extentTestIOS.log(Status.INFO, "No device added");
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;	
		}
	}

	/**
	 * Description To set the state of push notification toggle
	 * @param enablePushNotification
	 * @return Page object
	 */
	public SchedulePageIOS togglePushNotification(boolean enablePushNotification) {
		boolean toggleEnabled =	((push_notification_toggle.getAttribute("value").equalsIgnoreCase("false"))? false : true);
		try {
			if ((toggleEnabled && !enablePushNotification) || (!toggleEnabled && enablePushNotification)) {
				utilityIOS.clickIOS(push_notification_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To set the schedule trigger Time, 2 minutes after the current system time
	 * @return Page object
	 */
	public SchedulePageIOS setTime() {
		try {
			String currentTime;
			int hour;
			int minute;
			String midday;
			utilityIOS.scroll("down");
			utilityIOS.clickIOS(time_select);
			currentTime = utilityIOS.getCurrentTime();
			hour = Integer.parseInt(currentTime.split(":")[0].trim());
			minute = Integer.parseInt(currentTime.split(":")[1].trim());
			midday = (hour < 12 ? "AM" : "PM");
			// To set the time in hours including corner case when the time is 11:59 and 23:59
			hour = (hour <= 12 ? ((hour - 12) < 0 ? (hour) :12): (hour - 12));
			minute = ((minute + 2) >= 60 ? (1) : (minute + 2));
			if ((hour != 12) && (minute == 1)) {
				hour++;
			}
			if ((hour == 12) && (minute == 1)) {
				if (midday.equalsIgnoreCase("AM")) {
					midday = "PM";
				} else {
					midday = "AM";
				}
			} 
			utilityIOS.selectTime(hour,minute, midday, "from");
			utilityIOS.clickIOS(time_select);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To deselect the current day of the week.
	 */
	public SchedulePageIOS deselectCurrentDay() {
		String currentDay;
		try {
			currentDay = utilityIOS.getCurrentDay();
			String daySelected = days_selected_text.getText();
			utilityIOS.clickIOS(days_selection_link);
			for (int iIndex = 0 ; iIndex < days_of_week.size(); iIndex++) {
				if ((daySelected.equalsIgnoreCase("Everyday")) 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Mon") 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Tues") 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Wed") 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Thurs")  
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Fri") 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				}else if (daySelected.contains("Sat") 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				}else if (daySelected.contains("Sunday") 
						&& (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				}
			}
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To Select All days or No days in the week
	 * @param alldays
	 * @return Page object
	 */
	public SchedulePageIOS SelectAllOrNoDays(boolean alldays) {
		String daySelected = days_selected_text.getText();	
		utilityIOS.clickIOS(days_selection_link);
		try {
			if ((!alldays && daySelected.equalsIgnoreCase("Everyday"))||(alldays && daySelected.equalsIgnoreCase("None"))) {
				for (int iIndex = 0 ; iIndex < days_of_week.size(); iIndex++) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
				}
			}
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To check if the schedule exist
	 * @param scheduleName
	 * @return schedulePresent 
	 */
	public boolean verifySchedulePresent(String scheduleName) {
		String currentScheduleName;
		boolean schedulePresent = false;
		try {
			int noOfSchedules = schedule_list.size();
			for (int counter = 0; counter < noOfSchedules; counter++) {
				currentScheduleName = schedule_list.get(counter).getText();
				if (currentScheduleName.equalsIgnoreCase(scheduleName)) {
					schedulePresent = true;
					break;
				}
			}
			return schedulePresent;
		} catch (Exception e) {	
			utilityIOS.catchExceptions(iOSDriver, e);
			return schedulePresent;
		}
	}

	/**
	 * Description: To wait and check if the schedule got triggered
	 * @return scheduleTriggered 
	 */
	public boolean validateScheduleTrigger() {
		boolean scheduleTriggered = false;
		try {
			//wait till 2 minutes, if popup comes then break
			for (int i = 0; i <= 17; i++) {
				Thread.sleep(10000);	
				try {
					iOSDriver.switchTo().alert();
					scheduleTriggered = true;	
					break;
				} catch (Exception e) {	
					// No statement in cqtch block, to continue the exception even if the alert is not present
				}
			}	
			return scheduleTriggered;
		} catch (Exception e) {	
			utilityIOS.catchExceptions(iOSDriver, e);
			return scheduleTriggered;
		}
	}

	/**
	 * Description To validate if the error message is shown on saving the schedule
	 * @return alertPresent
	 */
	public boolean validateErrorMessage() {
		boolean alertPresent =false;
		try {
			WebDriverWait wait = new WebDriverWait(iOSDriver, 3);
			wait.until(ExpectedConditions.alertIsPresent());
			iOSDriver.switchTo().alert();
			alertPresent = true;
			return alertPresent;	
		} catch (Exception e) {
			//Here catch is not done through the re-usable exception handler, as the alert might be present or not present
			return alertPresent;
		}	 
	}

	/**
	 * Description To get the Error message after saving the schedule
	 * @return alertMessage
	 */
	public String getErrorMessage() {
		String alertMessage = null;
		alertMessage = iOSDriver.switchTo().alert().getText();
		try {
			extentTestIOS.log(Status.INFO,"Alert Text is : " +alertMessage);	
			iOSDriver.switchTo().alert().accept();
			return alertMessage;
		} catch (Exception e) {
			return alertMessage;
		}	 
	}

	/**
	 * Description: To create schedule based on parameter passed 
	 * @param scheduleName
	 * @param deviceStatePairList
	 * @param enableNotification
	 * @param allDays
	 * @return Page object
	 */
	public SchedulePageIOS createSchedule(String scheduleName,  HashMap<String, String> deviceStatePairList, boolean enableNotification, boolean allDays) {
		try {
			tapAddScheduleBtn();
			enterScheduleName(scheduleName);
			//To select the device and its state for the schedules
			Set set = deviceStatePairList.entrySet();
			Iterator iterator = set.iterator();
			while(iterator.hasNext()) {
				Map.Entry deviceStatePair = (Map.Entry)iterator.next();
				String deviceName = (String) deviceStatePair.getKey();
				String state =  (String) deviceStatePair.getValue();
				selectDeviceStateForSchedule(deviceName,state);	
			}
			setTime();
			if (!allDays) {
				deselectCurrentDay();	
			}
			togglePushNotification(enableNotification);
			tapSaveBtn();
			return this;
		} catch (Exception e) {			
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To tap on the schedule if it exist
	 * @param scheduleName
	 * @return Page object
	 */
	public SchedulePageIOS tapSchedule(String scheduleName) {
		try {
			int noOfSchedules = schedule_list.size();
			for (int counter = 0; counter <= noOfSchedules; counter++) {
				String currentScheduleName = schedule_list.get(counter).getText();
				if (currentScheduleName.equalsIgnoreCase(scheduleName)) {
					utilityIOS.clickIOS(schedule_list.get(counter));
					break;
				}
			}
			return this;
		} catch (Exception e) {	
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To delete an existing schedule
	 * @param scheduleName
	 * @return scheduleDeleted
	 */
	public boolean deleteSchedule(String scheduleName) {
		boolean scheduleDeleted = false;
		boolean deleteBtnPresent = false;
		String currentScheduleName;
		try {
			int noOfSchedules = schedule_list.size();
			for (int counter = 0; counter <= noOfSchedules; counter++) {
				currentScheduleName = schedule_list.get(counter).getText();
				if (currentScheduleName.toUpperCase().contains(scheduleName.toUpperCase())) {
					//Once the Schedule name matches then doing a swipe action on it and pressing on delete and then confirming on the delete confirmation popup
					for (int kIndex = 0; kIndex < 5; kIndex++) { 	//here the swipe retry mechanism of five times is done as swipe method is not reliable from Appium
						utilityIOS.swipeLeft(schedule_list.get(counter)); 
						try {
							if (delete_btn.isDisplayed() || delete_btn.isEnabled()) {
								deleteBtnPresent =true;
							}
						} catch (Exception e) {
						}
						if (deleteBtnPresent) {
							utilityIOS.clickIOS(delete_btn);
							scheduleDeleted = true;
							break;
						} else {
							utilityIOS.clickIOS(back_btn);
							utilityIOS.waitForElementIOS(add_schedule_btn, "clickable");
						}
					}
				}
				if (scheduleDeleted) {
					try {
						iOSDriver.switchTo().alert().accept();
						utilityIOS.waitForElementIOS(add_schedule_btn, "clickable");	
					} catch (Exception e) {
					}	
					break;
				}
			}
			if (!scheduleDeleted) {
				utilityIOS.clickIOS(back_btn);
				utilityIOS.waitForElementIOS(add_schedule_btn, "clickable");
			}
			return scheduleDeleted;
		} catch (Exception e) {	
			utilityIOS.catchExceptions(iOSDriver, e);
			return scheduleDeleted;
		}
	}

	/**
	 * Description: This function disable or enables the Alert
	 * @param scheduleName
	 * @param enableSchedule
	 * @return Page object
	 */
	public SchedulePageIOS toggleDisableEnableAlert(String scheduleName, boolean enableSchedule) {
		int noOfSchedulesSwitch;
		try {
			noOfSchedulesSwitch = schedule_toggle_list.size();
			for (int iIndex = 0; iIndex <= noOfSchedulesSwitch; iIndex++) {
				String switchForSchedule = schedule_toggle_list.get(iIndex).getAttribute("label");
				if (switchForSchedule.toUpperCase().contains(scheduleName.toUpperCase())) {
					utilityIOS.clickIOS(schedule_toggle_list.get(iIndex));
					break;
				}
			}
			return this;
		} catch (Exception e) {	
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To Select the device is present on Schedule page for schedule creation
	 * @param deviceName
	 * @param state
	 * @return Page object
	 */
	public boolean verifyDeviceForScheduleCreation(String deviceName) throws InterruptedException {
		String currentDevice;
		boolean devicePresent = false;
		try {
			if (!(deviceName.isEmpty())) {
				//selecting the device
				utilityIOS.clickIOS(add_device_btn);
				int noOfDevice = device_list_for_schedule.findElementsByXPath("XCUIElementTypeCell").size();
				for (int iVal = 1; iVal <= noOfDevice; iVal++) {
					currentDevice = device_list_for_schedule
							.findElementByXPath("XCUIElementTypeCell[" + iVal + "]/XCUIElementTypeStaticText").getText();
					if (deviceName.equalsIgnoreCase(currentDevice)) {
						devicePresent = true;
						break;
					} else {
						devicePresent = false;
					}
				}
			}
		} catch (Exception e) {	
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		utilityIOS.clickIOS(back_btn);
		return devicePresent;
	}
}
