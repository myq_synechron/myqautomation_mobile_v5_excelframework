package com.liftmaster.myq.ios.pages;

import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.testmethods.IOS;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class HelpPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public UtilityIOS utilityIOS;
	public ExtentTest extentTestIOS;

	//Page factory for Help page IOS
	@iOSFindBy(accessibility = "User's Guide")	
	public IOSElement users_guide_link;

	@iOSFindBy(accessibility = "FAQ")	
	public IOSElement faq_link;

	@iOSFindBy(accessibility = "MyQ Support")	
	public IOSElement myq_support_link;

	@iOSFindBy(accessibility = "License and Terms of Use")	
	public IOSElement license_link;

	@iOSFindBy(accessibility = "Privacy Statement")	
	public IOSElement privacystatement_link;

	@iOSFindBy(accessibility = "Legal Disclaimer")	
	public IOSElement legaldisclaimer_link;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText[@label='User’s Guides']")	
	public IOSElement user_guide_heading;

	@iOSFindBy(accessibility = "MyQ App")	
	public IOSElement myqapp_link;

	@iOSFindBy(accessibility = "MyQ Products")	
	public IOSElement myqproducts_link;

	@iOSFindBy(accessibility = "LIFTMASTER® MyQ® LICENSE")	
	public IOSElement license_heading;

	@iOSFindBy(accessibility = "CHAMBERLAIN® MyQ® LICENSE")	
	public IOSElement chamberlain_license_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeLink/XCUIElementTypeStaticText[@label='LiftMaster Privacy Statement (English)']")	
	public IOSElement liftmaster_privacy_statement_text;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeStaticText[@label='Chamberlain Privacy Statement']")	
	public IOSElement chamberlain_privacy_statement_text;

	@iOSFindBy(accessibility = "Legal Disclaimer")	
	public IOSElement legal_disclaimer_text;

	@iOSFindBy(accessibility = "Top Issues")	
	public IOSElement myq_support_text;

	@iOSFindBy(accessibility = "Closed 1. Were you able to create a MyQ account?")	
	public IOSElement chamberlain_text;


	/**
	 *  Description This is the Initialize method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public HelpPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description : To click on user guide link on the help page 
	 * @return Page object
	 */
	public HelpPageIOS clickOnUserGuideLink() {
		try { 
			utilityIOS.clickIOS(users_guide_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To verify user guide is launched successfully
	 * @return boolean
	 */
	public boolean verifyUserGuidePresent() {
		try { 
			utilityIOS.waitForElementIOS(user_guide_heading, "visibility");
			if (user_guide_heading.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : To click on the back button
	 * @return page object
	 */
	public HelpPageIOS clickBackButton() {
		try { 
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To verify MyQ App link is displayed successfully
	 * @return boolean
	 */
	public boolean verifyUserMyQAppPresent() {
		try { 
			if(IOS.brandName.equalsIgnoreCase("Liftmaster")) {
				utilityIOS.waitForElementIOS(myqapp_link, "visibility");
				if (myqapp_link.isDisplayed()) {
					return true;
				} else {
					return false;
				} 
			} else  {
				utilityIOS.waitForElementIOS(myq_support_text, "visibility");
				if (myq_support_text.isDisplayed()) {
					return true;
				} else {
					return false;
				}	
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : To verify MyQ App link is displayed successfully
	 * @return boolean
	 */
	public boolean verifyUserMyQProductPresent() {
		try { 
			if(IOS.brandName.equalsIgnoreCase("Liftmaster")) {
				utilityIOS.waitForElementIOS(myqproducts_link, "visibility");
				if (myqproducts_link.isDisplayed()) {
					return true;
				}  else {
					return false;
				}	
			} else  {
				utilityIOS.waitForElementIOS(chamberlain_text, "visibility");
				if (chamberlain_text.isDisplayed()) {
					return true;
				} else {
					return false;
				}	
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : To click on License link on the help page 
	 * @return Page object
	 */
	public HelpPageIOS clickOnUserLicenseLink() {
		try { 
			utilityIOS.clickIOS(license_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To click on FAQ link on the help page 
	 * @return Page object
	 */
	public HelpPageIOS clickMyQSupportLink() {
		try { 
			utilityIOS.clickIOS(myq_support_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To click on Privacy Statement link on the help page 
	 * @return Page object
	 */
	public HelpPageIOS clickOnPrivacyStatementLink() {
		try { 
			utilityIOS.clickIOS(privacystatement_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To click on Legal Disclaimer link on the help page 
	 * @return Page object
	 */
	public HelpPageIOS clickOnLegalDisclaimerLink() {
		try { 
			utilityIOS.clickIOS(legaldisclaimer_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To verify links are displayed successfully after clicking on license link in help section
	 * @return boolean
	 */
	public boolean verifyLicense() {
		try { 
			if(IOS.brandName.equalsIgnoreCase("Liftmaster")) {
				utilityIOS.waitForElementIOS(license_heading, "visibility");
				if (license_heading.getAttribute("label").contains("LIFTMASTER® MyQ® LICENSE")) {
					return true;
				} else {
					return false;
				}
			} else  {
				utilityIOS.waitForElementIOS(chamberlain_license_heading, "visibility");
				if (chamberlain_license_heading.getAttribute("label").contains("CHAMBERLAIN® MyQ® LICENSE")) {
					return true;
				} else {
					return false;
				}	
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : To verify links are displayed successfully after clicking on privacy statement link in help section
	 * @return boolean
	 */
	public boolean verifyPrivacyStatement() {
		try { 
			if(IOS.brandName.equalsIgnoreCase("Liftmaster")) {
				utilityIOS.waitForElementIOS(liftmaster_privacy_statement_text, "visibility");
				if (liftmaster_privacy_statement_text.getAttribute("label").contains("LiftMaster Privacy Statement")) {
					return true;
				} else {
					return false;
				}
			} else  {
				utilityIOS.waitForElementIOS(chamberlain_privacy_statement_text, "visibility");
				if (chamberlain_privacy_statement_text.getAttribute("label").contains("Chamberlain Privacy Statement")) {
					return true;
				} else {
					return false;
				}	
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : To verify links are displayed successfully after clicking on Legal disclaimer link in help section
	 * @return boolean
	 */
	public boolean verifyLegalDisclaimer() {
		try { 
			utilityIOS.waitForElementIOS(legal_disclaimer_text, "visibility");
			if (legal_disclaimer_text.getAttribute("label").contains("Legal Disclaimer")) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}
}