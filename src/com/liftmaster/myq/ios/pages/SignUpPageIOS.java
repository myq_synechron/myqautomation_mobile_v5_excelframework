package com.liftmaster.myq.ios.pages;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

import java.util.Random;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.iOSFindBy;

public class SignUpPageIOS {
	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	@iOSFindBy(accessibility = "sign_up_button")
	public IOSElement sign_up_btn;

	@iOSFindBy(accessibility = "Sign Up")
	public IOSElement sign_up_heading;

	@iOSFindBy(accessibility = "Cancel")
	public IOSElement cancel_btn;

	@iOSFindBy(accessibility = "Submit")
	public IOSElement submit_btn;

	@iOSFindBy(accessibility = "first_name_text_field")
	public IOSElement first_name_textbox;

	@iOSFindBy(accessibility = "last_name_text_field")
	public IOSElement last_name_textbox;

	@iOSFindBy(accessibility = "email_text_field")
	public IOSElement email_textbox;

	@iOSFindBy(accessibility = "password_text_field")
	public IOSElement password_textbox;

	@iOSFindBy(accessibility = "verify_password_text_field")
	public IOSElement verify_password_textbox;

	@iOSFindBy(accessibility = "Country:")
	public IOSElement country_link;

	@iOSFindBy(accessibility = "Language:")
	public IOSElement language_link;

	@iOSFindBy(accessibility = "zip_text_field")
	public IOSElement zip_textbox;

	@iOSFindBy(accessibility = "Country")
	public IOSElement country_heading;

	@iOSFindBy(accessibility = "Select a country:")
	public IOSElement select_country_heading;

	@iOSFindBy(accessibility = "Select a country:")
	public IOSElement select_language_heading;

	@iOSFindBy(xpath = "XCUIElementTypeScrollView")
	public IOSElement loader_icon;

	@iOSFindBy(accessibility = "Select a country:")
	public IOSElement Select_country_text;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_countries;

	@iOSFindBy(accessibility = "Language")
	public IOSElement language_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_languages;

	@iOSFindBy(accessibility = "Next")
	public IOSElement next_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn;

	@iOSFindBy(accessibility = "Search")
	public IOSElement search_box;

	@iOSFindBy(accessibility = "United States")
	public IOSElement first_list_item;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	@iOSFindBy(accessibility = "registration_successful_okay_button")
	public IOSElement registration_ok_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeStaticText")
	public IOSElement registration_success_msg;

	@iOSFindBy(xpath = "//XCUIElementTypeScrollView")
	public IOSElement signup_page_view;

	/**
	 * Description:This is the Initializer method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public SignUpPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description Click on signup button
	 * @throws InterruptedException
	 * @return Page Object
	 */
	public SignUpPageIOS clickSignup() throws InterruptedException {
		try{
			utilityIOS.clickIOS(sign_up_btn);
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description clicking on cancel button
	 * @return Page object
	 */
	public SignUpPageIOS tapCancelBtn() {
		try {
			utilityIOS.clickIOS(cancel_btn);
			utilityIOS.waitForElementIOS(sign_up_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description Enter first name in the First Name  text box
	 * @param firstName
	 * @throws InterruptedException
	 */
	public SignUpPageIOS enterFirstName(String firstName) throws InterruptedException {
		try{
			utilityIOS.enterTextIOS(first_name_textbox, firstName);
			iOSDriver.hideKeyboard();
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description Enter Last name in the Last Name text box
	 * @param lastName
	 * @throws InterruptedException
	 */
	public SignUpPageIOS enterLastName(String lastName) throws InterruptedException {
		try{
			utilityIOS.enterTextIOS(last_name_textbox, lastName);
			iOSDriver.hideKeyboard();
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description Generate randomemail and enter it in the email text box
	 * @return email
	 * @throws InterruptedException
	 */
	public String enterRandomEmail() throws InterruptedException {
		String email = null;
		try{
			Random randomNumber = new Random();
			int number = randomNumber.nextInt(10000);
			email = "abc"+number+"@xyz.com";
			utilityIOS.enterTextIOS(email_textbox, email);
			iOSDriver.hideKeyboard();
			return email;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return email;
		}
	}

	/**
	 * Description Enter Email in Email address text box
	 * @throws InterruptedException
	 * @return Page Object
	 */
	public SignUpPageIOS enterEmail(String email) throws InterruptedException {
		try{
			utilityIOS.clearIOS(email_textbox);
			utilityIOS.enterTextIOS(email_textbox, email);
			iOSDriver.hideKeyboard();
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description Enter Password in Password text box
	 * @throws InterruptedException
	 * @return Page Object
	 */
	public SignUpPageIOS enterPassword(String password) throws InterruptedException {
		try{
			utilityIOS.enterTextIOS(password_textbox, password);
			iOSDriver.hideKeyboard();
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description Enter Password in verify Password text box
	 * @return Page Object
	 * @param password
	 * @throws InterruptedException
	 */
	public SignUpPageIOS enterVerifyPassword(String password) throws InterruptedException {
		try{
			utilityIOS.enterTextIOS(verify_password_textbox, password);
			iOSDriver.hideKeyboard();
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To select country from the list of countries
	 * @throws InterruptedException
	 * @param country
	 * @return Page Object
	 */
	public SignUpPageIOS selectCountry(String country) throws InterruptedException {
		String currentCountry;
		boolean CountryDisplayed = false;
		try{
			try {
				if(country_link.isDisplayed()) {
					utilityIOS.clickIOS(country_link);
				}
			} catch (Exception e) {

			}
			//To check if the country is displayed
			try{
				CountryDisplayed = list_of_countries.findElementByAccessibilityId(country).isDisplayed();
			}catch (Exception e){
			}

			if(CountryDisplayed){
				utilityIOS.clickIOS(list_of_countries.findElementByAccessibilityId(country));
			}
			else{
				utilityIOS.enterTextIOS(search_box,country);
				int noOfCountries = list_of_countries.findElementsByXPath("XCUIElementTypeCell").size();
				if(noOfCountries>=1){
					for(int i= 1; i <= noOfCountries; i++) {
						currentCountry = list_of_countries
								.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
						if (country.equalsIgnoreCase(currentCountry)) {
							utilityIOS.clickIOS(list_of_countries.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
							break;
						}
					}
				}
			}
			utilityIOS.waitForElementIOS(submit_btn, "Visibility");
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To Enter zip code  in the zip code text box
	 * @throws InterruptedException
	 * @return Page Object
	 * @param zip
	 */
	public SignUpPageIOS enterZip(String zip) throws InterruptedException {
		try{
			if (!zip_textbox.isDisplayed()) {
				utilityIOS.swipe(signup_page_view, "up");
			}
			utilityIOS.clickIOS(zip_textbox);
			utilityIOS.clearIOS(zip_textbox);
			utilityIOS.enterTextIOS(zip_textbox, zip);
			utilityIOS.clickIOS(next_btn);
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To select language from the list of languages
	 * @throws InterruptedException
	 * @param language
	 * @return Page object
	 */
	public SignUpPageIOS selectLanguage(String language) throws InterruptedException {
		String currentLanguage;
		try{
			utilityIOS.clickIOS(language_link);
			int noOfCountries = list_of_countries.findElementsByXPath("XCUIElementTypeCell").size();
			if(noOfCountries>=1){
				for(int i= 1; i <= noOfCountries; i++) {
					currentLanguage =	list_of_languages.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
					if (language.equalsIgnoreCase(currentLanguage)) {
						utilityIOS.clickIOS(list_of_languages.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
						break;
					}
				}
			}
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description :To click on the submit button
	 * @return Page Object
	 * @throws InterruptedException
	 */
	public SignUpPageIOS clickSubmit() throws InterruptedException {
		try{
			utilityIOS.clickIOS(submit_btn);
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description :To validate if the signup was successful or an error message was shown
	 * @throws InterruptedException
	 */
	public String validateSignUp(){
		String popupMessage = null;
		try {
			utilityIOS.waitForElementIOS(registration_ok_btn, "clickable");
			popupMessage = registration_success_msg.getText();
			utilityIOS.clickIOS(registration_ok_btn);
		} catch (Exception e) {

		}
		try{

			// This below code is kept commented intentionally till a fix is provided by Appium. The version which have issue is 1.7.2
			// WebDriverWait wait = new WebDriverWait(iOSDriver, 10);
			// wait.until(ExpectedConditions.alertIsPresent());
			for (int i = 0; i <= 15; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			utilityIOS.captureScreenshot(iOSDriver);
			popupMessage = iOSDriver.switchTo().alert().getText();
			iOSDriver.switchTo().alert().accept();
			return popupMessage;
		}catch (Exception e) {
			return popupMessage;
		}  
	} 
}
