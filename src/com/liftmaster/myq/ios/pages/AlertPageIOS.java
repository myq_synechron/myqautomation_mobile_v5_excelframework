package com.liftmaster.myq.ios.pages;

import java.util.List;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class AlertPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS = null;

	String midday;
	String currentDay;
	String errorMessage1 = "Please enter a name for the alert.";
	String errorMessage2 = "Please select one or more device states that will trigger this alert.";
	String errorMessage3 = "Please select how you would like to be alerted (push notification or email).";
	int hour;
	int minute;
	public boolean deletBtnPresent = false;
	public boolean popupPresent = false;
	public boolean noAlertMsgPresent = false;

	//Page factory for Alert Page IOS
	@iOSFindBy(accessibility = "alert_add_bar_button")
	public IOSElement alert_add_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell/XCUIElementTypeTextField")
	public IOSElement alert_name_textbox;

	@iOSFindBy(accessibility = "Opened")
	public IOSElement state_opened_toggle;

	@iOSFindBy(accessibility = "Stopped")
	public IOSElement state_stopped_toggle;

	@iOSFindBy(accessibility = "Closed")
	public IOSElement state_closed_toggle;

	@iOSFindBy(accessibility = "Turned On")
	public IOSElement state_lighton_toggle;

	@iOSFindBy(accessibility = "Turned Off")
	public IOSElement state_lightoff_toggle;

	@iOSFindBy(accessibility = "push_notification_switch")
	public IOSElement push_notification_toggle;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement device_list_for_alert;

	@iOSFindBy(accessibility = "Save")
	public IOSElement save_btn;

	@iOSFindBy(accessibility = "required_duration_description_label")
	public IOSElement required_duration_text;

	@iOSFindBy(accessibility = "between_certain_times_switch")
	public IOSElement specific_times_switch;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeTable")
	public IOSElement alert_list;

	@iOSFindBy(accessibility = "Delete")
	public IOSElement delete_btn;

	@iOSFindBy(accessibility = "New Alert")
	public IOSElement new_alert_heading;

	@iOSFindBy(accessibility = "Select a Device:")
	public IOSElement device_select_heading;

	@iOSFindBy(accessibility = "You have no Alerts configured")
	public IOSElement no_alert_text;

	@iOSFindBy(className = "XCUIElementTypePickerWheel")
	public List<IOSElement> picker_wheel;

	@iOSFindBy(accessibility = "from_time_value_label")
	public IOSElement from_time;

	@iOSFindBy(accessibility = "to_time_value_label")
	public IOSElement to_time;

	@iOSFindBy(accessibility = "days_value_label")
	public IOSElement days;

	@iOSFindBy(xpath = "//XCUIElementTypeCell[9]/XCUIElementTypeStaticText[2]")
	public IOSElement days_selected;

	@iOSFindBy(xpath = "//XCUIElementTypeStaticText")
	public List<IOSElement> days_of_week;

	@iOSFindBy(accessibility = "Cancel")
	public IOSElement cancel_btn;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	@iOSFindBy(accessibility = "HamburgerMenu")	
	public  IOSElement hamburger_menu_btn;
	
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeButton[@name= 'Later']")	
	public  IOSElement later_btn;

	/**
	 * Description This is the Initialize method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public AlertPageIOS(IOSDriver<IOSElement> iOSDriver, ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver;
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description clicking on Add alert "+" button
	 * @throws InterruptedException
	 */
	public AlertPageIOS tapAddAlert() throws InterruptedException {
		try {
			utilityIOS.clickIOS(alert_add_btn);
			utilityIOS.waitForElementIOS(device_select_heading, "visibility");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description clicking on back button
	 */
	public AlertPageIOS tapBackBtn() {
		try {
			utilityIOS.clickIOS(back_btn);
			utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description clicking on cancel button
	 * @return Page object
	 */
	public AlertPageIOS tapCancelBtn() {
		try {
			utilityIOS.clickIOS(cancel_btn);
			utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description entering the alert name in the text field
	 * @param alertName
	 * @throws InterruptedException
	 * @return Page object
	 */
	public AlertPageIOS setAlertName(String alertName) throws InterruptedException {
		try {
			utilityIOS.clickIOS(alert_name_textbox);
			utilityIOS.clearIOS(alert_name_textbox);
			utilityIOS.enterTextIOS(alert_name_textbox, alertName);
			iOSDriver.hideKeyboard();
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description Selecting the device for which alert is to be created
	 * @param deviceName
	 * @throws InterruptedException
	 * @return Page object
	 */
	public AlertPageIOS selectDeviceForAlert(String deviceName) throws InterruptedException {
		try {
			int  value =1;
			int noOfDevice = device_list_for_alert.findElementsByXPath("XCUIElementTypeCell").size();
			for (int iVal = 1; iVal <= noOfDevice; iVal++) {
				String currentDevice = device_list_for_alert
						.findElementByXPath("XCUIElementTypeCell[" + iVal + "]/XCUIElementTypeStaticText").getText();
				if (deviceName.equalsIgnoreCase(currentDevice)) {
					utilityIOS.clickIOS(device_list_for_alert.findElementByXPath("XCUIElementTypeCell[" + iVal + "]"));
					break;
				}
				if(iVal==13) {
					utilityIOS.swipe(device_list_for_alert, "up");
					Thread.sleep(4000);
					value++;
					noOfDevice = device_list_for_alert.findElementsByXPath("XCUIElementTypeCell").size();
					if(value==3) {
						break;
					}
				}
			}
			utilityIOS.waitForElementIOS(new_alert_heading,"visibility");	
			return this;
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	
	/**
	 * Description Selecting the device for which alert is to be created
	 * @param deviceName
	 * @throws InterruptedException
	 * @return Page object
	 */
	public boolean selectDeviceForAlertCreation(String deviceName) throws InterruptedException {
		try {
			int noOfDevice = device_list_for_alert.findElementsByXPath("XCUIElementTypeCell").size();
			for (int iVal = 1; iVal <= noOfDevice; iVal++) {
				if(iVal>=10) {
					utilityIOS.scroll("down");
					iVal = 1;
				}
				String currentDevice = device_list_for_alert
						.findElementByXPath("XCUIElementTypeCell[" + iVal + "]/XCUIElementTypeStaticText").getText();
				if (deviceName.equalsIgnoreCase(currentDevice)) {
					utilityIOS.clickIOS(device_list_for_alert.findElementByXPath("XCUIElementTypeCell[" + iVal + "]"));
					break;
				}
			}
			utilityIOS.waitForElementIOS(new_alert_heading,"visibility");	
			return true;
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description For ENABLING OR DISABLING the "Turned On" Button
	 * @param stateNeeded
	 * @return Page object
	 */
	public AlertPageIOS toggleLightTurnedOn(String stateNeeded) {
		String currentState = String.valueOf(state_lighton_toggle.getAttribute("value"));
		try {
			if ((currentState.equalsIgnoreCase("1")) && (stateNeeded.equalsIgnoreCase("Disabled"))) {
				utilityIOS.clickIOS(state_lighton_toggle);
			} else if (currentState.equalsIgnoreCase("null")&&stateNeeded.equalsIgnoreCase("Enabled")) {
				utilityIOS.clickIOS(state_lighton_toggle);
			}
			currentState = String.valueOf(state_lighton_toggle.getAttribute("value"));
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description For ENABLING OR DISABLING the "Turned Off" Button
	 * @param stateNeeded
	 * @return Page object
	 */
	public AlertPageIOS toggleLightTurnedOff(String stateNeeded) {
		String currentState = String.valueOf(state_lightoff_toggle.getAttribute("value"));
		try {
			if ((currentState.equalsIgnoreCase("1")) && (stateNeeded.equalsIgnoreCase("Disabled"))) {
				utilityIOS.clickIOS(state_lightoff_toggle);
			} else if ((currentState.equalsIgnoreCase("null")) && (stateNeeded.equalsIgnoreCase("Enabled"))) {
				utilityIOS.clickIOS(state_lightoff_toggle);
			}
			currentState = String.valueOf(state_lightoff_toggle.getAttribute("value"));
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	/**
	 * Description For ENABLING OR DISABLING the "Opened" Button
	 * @param stateNeeded
	 * @return Page object
	 */
	public AlertPageIOS toggleOpenedButton(String stateNeeded) {
		String currentState = String.valueOf(state_opened_toggle.getAttribute("value"));
		try {
			if ((currentState.equalsIgnoreCase("1")) && (stateNeeded.equalsIgnoreCase("Disabled"))) {
				utilityIOS.clickIOS(state_opened_toggle);
			} else if ((currentState.equalsIgnoreCase("null")) && (stateNeeded.equalsIgnoreCase("Enabled"))) {
				utilityIOS.clickIOS(state_opened_toggle);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description For ENABLING OR DISABLING the "Closed" Button
	 * @param stateNeeded
	 * @return Page object
	 */
	public AlertPageIOS toggleClosedButton(String stateNeeded) {
		String currentState = String.valueOf(state_closed_toggle.getAttribute("value"));
		try {
			if ((currentState.equalsIgnoreCase("1")) && (stateNeeded.equalsIgnoreCase("Disabled"))) {
				utilityIOS.clickIOS(state_closed_toggle);
			} else if ((currentState.equalsIgnoreCase("null")) && (stateNeeded.equalsIgnoreCase("Enabled"))) {
				utilityIOS.clickIOS(state_closed_toggle);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description For ENABLING OR DISABLING the "Stopped" Button
	 * @param stateNeeded
	 * @return Page object
	 */
	public AlertPageIOS toggleStoppedButton(String stateNeeded) {
		String currentState = String.valueOf(state_stopped_toggle.getAttribute("value"));
		try {
			if ((currentState.equalsIgnoreCase("1")) && (stateNeeded.equalsIgnoreCase("Disabled"))) {
				utilityIOS.clickIOS(state_stopped_toggle);
			} else if ((currentState.equalsIgnoreCase("null")) && (stateNeeded.equalsIgnoreCase("Enabled"))) {
				utilityIOS.clickIOS(state_stopped_toggle);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	/**
	 *Description To set the alert to Immediate or Delayed 
	 *@param hours
	 *@param minutes
	 *@return Page object
	 */
	public AlertPageIOS toggleImmediateOrDelayed(int hours, int minutes) {
		try {
			String duration = required_duration_text.getText();
			if (duration.equalsIgnoreCase("None")) {
				if ((hours != 0) || (minutes != 0)) {
					utilityIOS.clickIOS(required_duration_text);			
					setDelayDuration(hours, minutes);
				}
			} else {
				utilityIOS.clickIOS(required_duration_text);			
				setDelayDuration(hours, minutes);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To set the duration after which alert should get executed
	 * @param delayHours
	 * @param delayMinutes
	 * @return Page object
	 */
	public AlertPageIOS setDelayDuration(int delayHours, int delayMinutes) {
		try {
			if ((delayHours == 0) && (delayMinutes == 0)) {
				//Here to set value zero does not changes the value, it has to be done by first changing to 1 then zero
				picker_wheel.get(0).sendKeys(""+(delayHours+1)+" hour");	
				picker_wheel.get(0).sendKeys(""+delayHours+" hours");	
			} else {
				for (int i = 1; i <= delayHours; i++) {
					picker_wheel.get(0).sendKeys(""+i+(i == 1?" hour":" hours"));
					//Here the send keys have been used rather than the reusable function to enter text to avoid too many logs for each swipe on report
				}	
				for (int i = 1; i <= delayMinutes; i++) {
					picker_wheel.get(1).sendKeys(""+i+(i == 1 ? " minute" : " minutes"));
				}
			}
			extentTestIOS.log(Status.INFO, "Set the Delay duration to " +delayHours+ " and " +delayMinutes);
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To set the From Time by getting the current system time
	 * @return Page object
	 */
	public AlertPageIOS setFromTime() {
		try {
			utilityIOS.scroll("down");
			utilityIOS.clickIOS(from_time);
			String currentTime = utilityIOS.getCurrentTime();
			hour = Integer.parseInt(currentTime.split(":")[0].trim());
			minute = Integer.parseInt(currentTime.split(":")[1].trim());
			midday = (hour < 12 ? "AM" : "PM");
			// To set the time in hours including corner case when the time is 11:59 and 23:59
			hour = (hour <= 12 ? ((hour - 12) < 0 ? (hour) : 12) : (hour - 12));
			minute = ((minute + 8) >= 60 ? (5) : (minute + 8));
			if ((hour != 12) && (minute == 5)) {
				hour++;
			}
			if ((hour == 12) && (minute == 5)) {
				if (midday.equalsIgnoreCase("AM")) {
					midday = "PM";
				} else if (midday.equalsIgnoreCase("PM")) {
					midday = "AM";
				}
			} 
			utilityIOS.selectTime(hour,minute, midday, "from");
			utilityIOS.clickIOS(from_time);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To set the TO Time by getting the current system time
	 * @return Page object
	 */
	public AlertPageIOS setToTime() {
		try {
			//utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
			utilityIOS.scroll("down");
			utilityIOS.clickIOS(to_time);
			String currentTime = utilityIOS.getCurrentTime();
			hour = Integer.parseInt(currentTime.split(":")[0].trim());
			minute = Integer.parseInt(currentTime.split(":")[1].trim());
			midday = (hour < 12 ? "AM" : "PM");
			// To set the time in hours including corner case when the time is 11:59 and 23:59
			hour = (hour <= 12 ? ((hour - 12) < 0 ? (hour) : 12) : (hour - 12));
			minute = ((minute + 10) >= 60 ? (7) : (minute + 10));
			if ((hour != 12) && (minute == 7)){
				hour++;
			}
			if ((hour == 12) && (minute == 7)){
				if (midday.equalsIgnoreCase("AM")){
					midday = "PM";
				} else {
					midday = "AM";
				}
			}
			utilityIOS.selectTime(hour,minute, midday, "to");
			utilityIOS.clickIOS(to_time);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the Alert at all day and times or on specific times
	 * @param createRegularAlert
	 * @return Page object
	 */
	public AlertPageIOS toggleRegularOrScheduled(boolean createRegularAlert) {
		boolean toggleEnabled =Boolean.valueOf(specific_times_switch.getAttribute("value"));
		try {
			if ((!(toggleEnabled) && !(createRegularAlert)) 
					|| ((toggleEnabled) && (createRegularAlert))) {
				utilityIOS.clickIOS(specific_times_switch);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To deselect the current day of the week.
	 * @return Page object
	 */
	public AlertPageIOS deselectCurrentDay() {
		try {
			currentDay = utilityIOS.getCurrentDay();
			String daySelected = days_selected.getText();
			utilityIOS.clickIOS(days);
			for (int iIndex = 0; iIndex < days_of_week.size(); iIndex++) {
				if ((daySelected.equalsIgnoreCase("Everyday")) && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Mon") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Tues") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Wed") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Thurs") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Fri") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Sat") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				} else if (daySelected.contains("Sunday") && (days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
					break;
				}
			}
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	
	/**
	 * Description To select the current day of the week.
	 * @return Page object
	 */
	public AlertPageIOS selectCurrentDay() {
		try {
			currentDay = utilityIOS.getCurrentDay();
			utilityIOS.clickIOS(days);	
			for (int iIndex = 0; iIndex < days_of_week.size(); iIndex++) {
				if( !(days_of_week.get(iIndex).getText().equals(currentDay))) {
					utilityIOS.clickIOS(days_of_week.get(iIndex));
				} else {
                    extentTestIOS.log(Status.INFO, days_of_week.get(iIndex).getText() + " is selected");
				}
			}
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	
	/**
	 * Description To set the state of push notification toggle
	 * @param enablePushNotification
	 * @return Page object
	 */
	public AlertPageIOS togglePushNotification(boolean enablePushNotification) {
		boolean toggleEnabled =	((push_notification_toggle.getAttribute("value").equalsIgnoreCase("false"))? false : true);
		try {
			if ((toggleEnabled && !enablePushNotification) || (!toggleEnabled && enablePushNotification)) {
				utilityIOS.clickIOS(push_notification_toggle);
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To click on the save Button
	 * @return Page object
	 */
	public AlertPageIOS tapSaveAlertBtn() {
		try {
			utilityIOS.clickIOS(save_btn);
			try {
				if (activity_indicator.isDisplayed()) {
					utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
				}
			} catch (Exception e) {
				//just to catch exception and proceed further
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To Validate the error message shown on the Alert creation screen
	 * @return popupPresent
	 */
	public boolean validateErrorMessage() {
		try {
			try {
				iOSDriver.switchTo().alert();
				popupPresent = true;
			} catch (Exception e) {
				popupPresent = false;	
			}
			if (popupPresent) {
				String popUpMessageText = iOSDriver.switchTo().alert().getText();	
				popUpMessageText = popUpMessageText.substring(6);
				if (popUpMessageText.equalsIgnoreCase(errorMessage1)) {
					extentTestIOS.log(Status.INFO, "Popup Present for Text Box empty");
					utilityIOS.captureScreenshot(iOSDriver); 
				} else if (popUpMessageText.equalsIgnoreCase(errorMessage2)) {
					extentTestIOS.log(Status.INFO, "Popup Present for no state selection ");
					utilityIOS.captureScreenshot(iOSDriver); 
				} else if (popUpMessageText.equalsIgnoreCase(errorMessage3)) {
					extentTestIOS.log(Status.INFO, "Popup Present for no notification Method selected ");
					utilityIOS.captureScreenshot(iOSDriver); 
				}
				iOSDriver.switchTo().alert().accept();
				utilityIOS.clickIOS(back_btn);
				utilityIOS.clickIOS(cancel_btn);
				utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
			}
			return popupPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return popupPresent;
		}	
	}

	/**
	 * Description To Validate if no alerts has been created on account
	 * @return noAlertMsgPresent
	 */
	public boolean validateNoAlertText() {
		try {
			noAlertMsgPresent = no_alert_text.isDisplayed();
			return noAlertMsgPresent;
		} catch(Exception e) {
			return noAlertMsgPresent;
		}
	}

	/**
	 * Description To fetch the whether alert name passed is available against the device
	 * @param deviceName
	 * @param alertName
	 * @return alertPresent
	 */
	public boolean verifyAlertAvailableForDevice(String deviceName, String alertName) {
		boolean alertPresent = false;
		try {
			List<MobileElement> alertListChildElements = alert_list.findElementsByXPath(".//*");
			int noOfElements = alertListChildElements.size();
			String currentDeviceName;
			//the two  loops are for finding the device name and alert name
			for (int iIndex = 0; iIndex < noOfElements - 1; iIndex++) {
				String elementType = alertListChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					currentDeviceName = alertListChildElements.get(iIndex+1).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						for (int jIndex = ++iIndex; jIndex < noOfElements - 1; jIndex++) {
							String elementType1 = alertListChildElements.get(jIndex).getAttribute("type");
							//If alert name is found then bollean is set to tue else the boolean is false
							if (elementType1.equalsIgnoreCase("XCUIElementTypeSwitch")) {
								String currentAlertName = alertListChildElements.get(jIndex).getAttribute("label");
								if (currentAlertName.equalsIgnoreCase(alertName)) {
									alertPresent = true;
									break;
								}
							}
						}
					}
				}
				if (alertPresent) {
					break;
				}
			}
			return alertPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return alertPresent;
		}
	}

	/**
	 * Description:To tap on the alert from the list of alerts
	 * @param deviceName
	 * @param alertName
	 * @return Page object
	 */
	public AlertPageIOS tapAlert(String deviceName, String alertName) {
		boolean alertPresent = false;
		try {
			List<MobileElement> alertListChildElements = alert_list.findElementsByXPath(".//*");
			int noOfElementsInHierarchy = alertListChildElements.size();
			String currentDeviceName;
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				//First Finding the heading with Device name
				String elementType = alertListChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					currentDeviceName = alertListChildElements.get(iIndex+1).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						//Once the device name is matched then looping to find the alert name
						for (int jIndex = ++iIndex; jIndex < noOfElementsInHierarchy - 1; jIndex++)	{
							String elementType1 = alertListChildElements.get(jIndex).getAttribute("type");
							if (elementType1.equalsIgnoreCase("XCUIElementTypeSwitch")) {
								String currentAlertName = alertListChildElements.get(jIndex).getAttribute("label");
								// once Alert found then clicking on the alert
								if (currentAlertName.equalsIgnoreCase(alertName)) {									
									utilityIOS.clickIOS(alertListChildElements.get(jIndex - 2));
									alertPresent = true;
									break;
								}
							}
						}
					}
				}
				if (alertPresent) {
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:To Disable or enable the alert
	 * @param deviceName
	 * @param alertName
	 * @param enableAlert
	 * @return toggleEnabled
	 */
	public boolean toggleDisableEnableAlert(String deviceName, String alertName, boolean enableAlert) {
		boolean alertPresent = false;
		boolean toggleEnabled = true;
		try {
			List<MobileElement> alertListChildElements = alert_list.findElementsByXPath(".//*");
			int noOfElementsInHierarchy = alertListChildElements.size();
			String currentDeviceName;
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy; iIndex++) {
				//First Finding the heading with Device name
				String elementType = alertListChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					currentDeviceName = alertListChildElements.get(iIndex+1).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						//Once the device name is matched then looping to find the alert name
						for (int jIndex = ++iIndex ; jIndex < noOfElementsInHierarchy; jIndex++) {
							String elementType1 = alertListChildElements.get(jIndex).getAttribute("type");
							if (elementType1.equalsIgnoreCase("XCUIElementTypeSwitch")) {
								String currentAlertName = alertListChildElements.get(jIndex).getAttribute("label");
								//once alert name matches then finding the current state of the switch and based on that setting the state of the Toggle button
								if (currentAlertName.equalsIgnoreCase(alertName)) {
									alertPresent = true;
									toggleEnabled =	((alertListChildElements.get(iIndex).getAttribute("value").equalsIgnoreCase("0"))? false : true);
									if (toggleEnabled && !enableAlert) {
										utilityIOS.clickIOS(alertListChildElements.get(jIndex));
									} else if (!toggleEnabled && enableAlert) {
										utilityIOS.clickIOS(alertListChildElements.get(jIndex));
									}
									utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
									toggleEnabled = Boolean.valueOf( alertListChildElements.get(jIndex).getAttribute("value"));
									break;
								}
							}
						}
					}
				}
				if (alertPresent) {
					break;
				}
			}
			return toggleEnabled;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return toggleEnabled;
		}
	}

	/**
	 * Description:To delete the alert name passed
	 * @param deviceName
	 * @param alertName
	 * @throws InterruptedException
	 * @return alertPresent
	 */
	public boolean deleteAlert(String deviceName, String alertName) throws InterruptedException{
		boolean alertPresent = false;
		try {
			List<MobileElement> alertListChildElements =  alert_list.findElementsByXPath(".//*");
			int noOfElements = alertListChildElements.size();
			String currentDeviceName;
			for (int iIndex = 0;iIndex < noOfElements - 1; iIndex++)
			{
				//First Finding the heading with Device name
				String elementType = alertListChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					currentDeviceName = alertListChildElements.get(iIndex+1).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						//Once the device name is matched then looping to find the alert name
						for (int jIndex = ++iIndex; jIndex < noOfElements - 1; jIndex++) {
							String elementType1 = alertListChildElements.get(jIndex).getAttribute("type"); 
							if (elementType1.equalsIgnoreCase("XCUIElementTypeSwitch")) {
								String currentAlertName = alertListChildElements.get(jIndex).getAttribute("label");
								if (currentAlertName.equalsIgnoreCase(alertName)) {	
									//Once the Alert name matches then doing a swipe action on it and pressing on delete and then confirming on the delete confirmation popup
									//The Looping is done to have the retry mechanism on delete.In case the swipe left method does not work
									for (int kIndex = 0; kIndex < 5; kIndex++) { 	
										//Here hard coded value jIndex-2 has been used to traverse through the hierarchy. As the attributes of some elements is null 
										utilityIOS.swipeLeft(alertListChildElements.get(jIndex-2)); 
										try {
											deletBtnPresent =  delete_btn.isDisplayed() || delete_btn.isEnabled();
										} catch (Exception e) {
										}	
										if (deletBtnPresent) {
											break;
										} else {
											utilityIOS.clickIOS(back_btn);
											utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
										}
									}
									if (deletBtnPresent) {
										utilityIOS.clickIOS(delete_btn);
										extentTestIOS.log(Status.INFO,  "Clicked on Delete button");
										alertPresent = true;
										break;
									} else {
										utilityIOS.clickIOS(back_btn);
										utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
									}
								}
							}
						}
					}
				}
				if (alertPresent) {
					try {
						iOSDriver.switchTo().alert().accept();
						utilityIOS.waitForElementIOS(alert_add_btn, "clickable");
					} catch (Exception e) {
					}	 
					break;
				}
			}
			return alertPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return alertPresent;
		}	
	}
	/**
	 * Description: This function will return true or false depending if alert is present
	 * @return alertPresent
	 */
	public boolean validateNoDeviceAlert() { 
		try {
//			WebDriverWait wait = new WebDriverWait(iOSDriver, 10);
//			wait.until(ExpectedConditions.alertIsPresent());
			//Custom wait logic till alert is present
			//for (int i = 0; i <= 15; i++) {
				try {
					iOSDriver.switchTo().alert().dismiss();
					return true;	
				} catch (Exception e) {
				}
		//	}
			//Here catch is not done through the re-usable exception handler, as the alert might be present or not present
		} catch (Exception e) {
			return false;
		}
		return false;
	}
}