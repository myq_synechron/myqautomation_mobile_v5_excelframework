package com.liftmaster.myq.ios.pages;

import com.liftmaster.myq.utils.UtilityIOS;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import com.liftmaster.myq.ios.pages.MenuPageIOS;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.liftmaster.myq.ios.pages.MenuPageIOS;

public class HistoryPageIOS {

	public static IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	//Page factory for History Page IOS

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]")
	public IOSElement history_list_items;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeOther[1]/XCUIElementTypeStaticText[1]")
	public IOSElement today_heading;

	@iOSFindBy(id = "Your account has no history")
	public IOSElement no_history_text;
	
	@iOSFindBy(id = "history_trash_bar_button")
	public IOSElement delete_history;
	
	@iOSFindBy(id = "Delete History?")
	public IOSElement delete_label;
	
	@iOSFindBy(id = "Delete")
	public IOSElement delete_button;
	
	MenuPageIOS menuPageIOS = null;
	/**
	 * Description This is the Initialize method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public HistoryPageIOS(IOSDriver<IOSElement> iOSDriver, ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver;
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description: This method will verify the History for the event name and time
	 * @return boolean
	 * @throws InterruptedException 
	 */
	public boolean verifyHistory(String eventName, String eventTime) throws InterruptedException {

		boolean eventFound = false;
		boolean todayFound = false; 
		String actualEventTime = null;
		String actualEventName;
		Long eventStartTime = null;
		Long eventEndTime = null;
		Long eventHistoryTime = null;
		eventName = eventName.toUpperCase();
		String xml = null;
		String nodeName = null;
		String fileName = "/Applications/History.xml";
		BufferedWriter bw = null;
		FileWriter fw = null;
		String allTime="";
		String futureDay="";
		
		Calendar cal = Calendar.getInstance();
		futureDay = (new SimpleDateFormat("MMM").format(cal.getTime())) + "-" + (new SimpleDateFormat("YYYY").format(cal.getTime()));
		
		Thread.sleep(10000); // Since we are checking history for today this hard code wait will ensure today's history is retrieved
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHistory();
		utilityIOS.swipe(history_list_items,"down"); // This is done to refresh the page
		File file = new File(fileName); //Delete the xml file created
		if (file.exists()) {
			//file.delete();
		}
		try
		{
			xml = iOSDriver.getPageSource();
			fw = new FileWriter(fileName);
			bw = new BufferedWriter(fw);
			bw.write(xml);
			bw.close();

			File xmlFile = new File(fileName); // Since history might be from few to many records the below implementation has been done using xml verification
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance(); // the event name is iterated through the xml file and verified against
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder(); //given event name and time
			org.w3c.dom.Document doc = dBuilder.parse(xmlFile);
			doc.getDocumentElement().normalize();

			NodeList nList = doc.getElementsByTagName("XCUIElementTypeStaticText");

			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				nodeName =  nNode.getNodeName();
				Element eElement = (Element) nNode;
				actualEventName = eElement.getAttribute("value").toUpperCase();
				
				if (actualEventName.equalsIgnoreCase("Today")) {
					todayFound = true;
					continue;
				}

				if ((actualEventName.equalsIgnoreCase("Yesterday")) || (actualEventName.contains(futureDay)))  {
					break;
				}
				if (actualEventName.toUpperCase().contains(eventName) && todayFound) { // If the event name is found today then verify time
					eElement = (Element) nList.item(temp - 1);
					actualEventTime  = eElement.getAttribute("value");
					eventStartTime = utilityIOS.getTimeInMilliSeconds(eventTime) - 60000; // Subtract one minute * 1000 to convert to milliseconds
					eventEndTime  = eventStartTime + 60000; // Add one minute * 1000 to convert to milliseconds
					eventHistoryTime = utilityIOS.getTimeInMilliSeconds(actualEventTime); 
					
					if (utilityIOS.verifyTimeRange(eventHistoryTime, eventStartTime, eventEndTime)) {
						eventFound = true; // Verify both Event Name and time, time should match within a range i.e. +- One Minute
						extentTestIOS.log(Status.INFO, "Expected event Name: '" + eventName +" at Time: " + eventTime + "' was found successfully");
					} else {
						allTime = allTime + actualEventTime +",";
					}
				}
			}
			if (eventFound == false) {
				extentTestIOS.log(Status.INFO, "Expected event Name: " + eventName +", Expected event Time: " + eventTime +" not found in today's history");
			}
		}  catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		if (file.exists()) {
			//file.delete();
		}
		if (eventFound == false) { 
			if (allTime.length() > 1) { // This will log only if event found today but for different time, will help to analyze failures
				extentTestIOS.log(Status.INFO, eventName + ": Event was found but time mismatch, Time(s): " + allTime +" Against time :" + eventTime) ;
			}
		}
		return eventFound;		
	}
	
	/**
	 * Description : This function would tap on delete history and verify history is deleted
	 * @return boolean
	 */
	public boolean deleteHistory() {
		boolean deleteHistory = false;
		String deleteText = "Delete History?";
		String successMessage = "Your account has no history";
		menuPageIOS = new MenuPageIOS(iOSDriver,extentTestIOS);
		menuPageIOS.tappingHamburgerMenu();
		menuPageIOS.tappingHistory(); // This is done to refresh the page
		
		try{
			Thread.sleep(3000); // For page to load
			utilityIOS.clickIOS(delete_history);
			Thread.sleep(2000); // For popup to appear
			if (delete_label.getText().contains(deleteText)) {
				utilityIOS.clickIOS(delete_button);
			} else {
				extentTestIOS.log(Status.ERROR, "'" + deleteText + "' popup has not appeared on the page after tapping delete button");
				return deleteHistory;
			}
			Thread.sleep(5000); // Wait for history to get deleted
			menuPageIOS.tappingHamburgerMenu(); // Navigate to Device page to refresh history page
			menuPageIOS.tappingDevices();
			menuPageIOS.tappingHamburgerMenu();
			menuPageIOS.tappingHistory(); 
			
			Thread.sleep(5000); // Wait for history page to refresh
			if (no_history_text.getText().contains(successMessage)) {
				extentTestIOS.log(Status.INFO, "'" + successMessage + "' has appeared after clicking the delete button.");
				deleteHistory = true;
			} else {
				extentTestIOS.log(Status.ERROR, successMessage + " has not appeared after clicking the delete button.");
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return deleteHistory;
	}

}