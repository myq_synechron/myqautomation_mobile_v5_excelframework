package com.liftmaster.myq.ios.pages;

import java.util.List;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.testmethods.IOS;
import com.liftmaster.myq.utils.UtilityExcel;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class MenuPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS = null;

	//Page factory for Menu Page IOS
	@iOSFindBy(accessibility = "HamburgerMenu")	
	public  IOSElement hamburger_menu_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[1]/XCUIElementTypeStaticText[@label='Devices']")	
	public  IOSElement devices_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[2]/XCUIElementTypeStaticText[@label='History']")	
	public IOSElement history_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[3]/XCUIElementTypeStaticText[@label='Device Management']")	
	public IOSElement device_management_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[4]/XCUIElementTypeStaticText[@label='Users']")	
	public IOSElement users_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[6]/XCUIElementTypeStaticText[@label='Alerts']")	
	public IOSElement alerts_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[7]/XCUIElementTypeStaticText[@label='Schedules']")	
	public IOSElement schedules_link;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]/XCUIElementTypeCell[8]/XCUIElementTypeStaticText")	
	public IOSElement help_link;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	public IOSElement user_profile_link;

	@iOSFindBy(accessibility = "nav_icon_accounts")	
	public IOSElement user_icon;

	@iOSFindBy(accessibility = "Logout")	
	public IOSElement logout_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='Device Management']")
	public IOSElement device_management_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='Devices']")
	public IOSElement device_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='Alerts']")
	public IOSElement alert_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='Account']")
	public IOSElement account_info_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='Schedules']")
	public IOSElement schedules_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeStaticText[@name='Manage Users']")
	public IOSElement manage_users_heading;

	@iOSFindBy(xpath = "//XCUIElementTypeTable[1]")
	public IOSElement menu_items_list;

	@iOSFindBy(accessibility = "Skip")	
	public IOSElement v5_skip_image_btn;

	@iOSFindBy(accessibility = "Enter your passcode to access your account")
	public IOSElement user_profile_passcode_msg;

	@iOSFindBy(accessibility = "Please re-enter your sign-in information to edit your account")
	public IOSElement user_profile_password_msg;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement liftmaster_logo;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement chamberlain_logo;

	@iOSFindBy(accessibility = "login_button")
	public IOSElement login_btn;

	@iOSFindBy(className = "XCUIElementTypeSecureTextField")
	public List<IOSElement> passcode_fields;

	@iOSFindBy(accessibility = "password_text_field")
	public IOSElement password_field;

	@iOSFindBy(accessibility = "user_name_label")
	public IOSElement user_name_label;

	/**
	 *  Description This is the Initialize method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public MenuPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description To verify the Hamburger Menu
	 * @return Page object
	 */
	public boolean verifyHamburgerMenu() {
		try { 
			Thread.sleep(5000);
			if(hamburger_menu_btn.isEnabled() || hamburger_menu_btn.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description To tap on the Hamburger Menu
	 * @return Page object
	 */
	public MenuPageIOS tappingHamburgerMenu() {
		try { 
			utilityIOS.clickIOS(hamburger_menu_btn);
			try {
				if (v5_skip_image_btn.isEnabled()) {
					utilityIOS.clickIOS(v5_skip_image_btn);
				}
			} catch (Exception e) {
				//To tap on the sip button if the image is shown otherwise continuing with execution	
			}
			//utilityIOS.waitForElementIOS(user_icon, "visibility");
			Thread.sleep(2000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : To click on devices options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingDevices() {
		try { 
			utilityIOS.clickIOS(devices_link);
			//	utilityIOS.waitForElementIOS(device_heading, "visibility");
			Thread.sleep(2000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To click on History options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingHistory() {
		try { 
			utilityIOS.clickIOS(history_link);

			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:To click on Alerts options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingAlerts() {
		try {
			utilityIOS.clickIOS(alerts_link);
			//utilityIOS.waitForElementIOS(alert_heading, "visibility");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:To click on Device Management options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingDeviceManagement() {
		try {
			utilityIOS.clickIOS(device_management_link);
			try {
				iOSDriver.switchTo().alert().accept();	
			} catch (Exception e) {
				//For accepting the Alert for DeviceManagement for external device when navigating for the first time
			}
			//	utilityIOS.waitForElementIOS(device_management_heading, "visibility");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:To click on Users options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingUsers() {
		try {
			utilityIOS.clickIOS(users_link);
			//utilityIOS.waitForElementIOS(manage_users_heading, "visibility");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:To click on Schedules options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingSchedules() {
		try {
			utilityIOS.clickIOS(schedules_link);
			//utilityIOS.waitForElementIOS(schedules_heading, "visibility");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:To click on Help options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingHelp() {
		try {
			utilityIOS.clickIOS(help_link);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}

	}

	/**
	 * Description:To click on User Profile options from menu options 
	 * @return Page object
	 */
	public MenuPageIOS tappingUserProfile() {
		try {
			utilityIOS.clickIOS(user_profile_link);
			//utilityIOS.waitForElementIOS(account_info_heading, "visibility");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To Click on logout button
	 * @return Page object
	 */
	public MenuPageIOS tappingLogout() throws InterruptedException {
		try {
			utilityIOS.clickIOS(logout_btn);
			Thread.sleep(5000);
			Thread.sleep(5000);
			//			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
			//				//utilityIOS.waitForElementIOS(liftmaster_logo, "visibility");
			//			} else {
			//				utilityIOS.waitForElementIOS(chamberlain_logo, "visibility");
			//			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To verify the list in elements 
	 * @return Page object
	 */
	public boolean verifyMenuLink(String menuItemName) {
		boolean menuItemPresent= false;
		switch (menuItemName) {
		case "Devices":
			if(devices_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		case "History":
			if(history_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		case "Device Management":
			if(device_management_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		case "Users":
			if(users_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		case "Partners":
			if(iOSDriver.findElementByXPath("//XCUIElementTypeTable[1]/XCUIElementTypeCell[5]/XCUIElementTypeStaticText").isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		case "Alerts":
			if(alerts_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			}
		case "Schedules":
			if(schedules_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		case "Help":
			if(help_link.isDisplayed()) {
				menuItemPresent = true;
				break;
			} else {
				menuItemPresent = false;
				break; 
			}
		}
		return menuItemPresent;
	}

	/**
	 * Description: To logout from the App
	 * @return Page object
	 */
	public MenuPageIOS logout() throws InterruptedException {
		try {
			utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
			utilityIOS.clickIOS(hamburger_menu_btn);
			//utilityIOS.waitForElementIOS(user_icon, "visibility");
			Thread.sleep(5000);
			utilityIOS.clickIOS(user_profile_link);
			//utilityIOS.waitForElementIOS(account_info_heading, "visibility");
			Thread.sleep(5000);
			utilityIOS.clickIOS(logout_btn);
			try {
				iOSDriver.switchTo().alert().accept();	
			} catch (Exception e) {
			}

			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
				utilityIOS.waitForElementIOS(liftmaster_logo, "visibility");
			} else {
				utilityIOS.waitForElementIOS(chamberlain_logo, "visibility");
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the passcode
	 * @param passcode
	 * @return Page object
	 */
	public MenuPageIOS accessUserProfileWithPasscode(String passcode) {
		try {
			boolean liftmaster;
			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
				liftmaster = true;
			} else {
				liftmaster = false;
			}
			utilityIOS.clickIOS(user_profile_link);
			if((liftmaster ? liftmaster_logo:chamberlain_logo).isEnabled()&& user_profile_passcode_msg.isEnabled()) {
				int sizeOfPasscodeFields = passcode_fields.size();
				int sizeOfPasscode = passcode.length();
				int i;
				if (sizeOfPasscode == sizeOfPasscodeFields) {
					for(i = 0; i < sizeOfPasscodeFields; i++) {
						utilityIOS.enterTextIOS(passcode_fields.get(i),String.valueOf(passcode.charAt(i)));
					}
				} else {
					extentTestIOS.log(Status.INFO, "Number of expected length for  passcode and the length passed is not matching"); 	
				}
			} 
			utilityIOS.waitForElementIOS(user_name_label, "visibility");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To set the passcode
	 * @param passcode
	 * @return Page object
	 */
	public MenuPageIOS accessUserProfileWithPassword(String password) {
		try {
			boolean liftmaster;
			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
				liftmaster = true;
			} else {
				liftmaster = false;
			}
			utilityIOS.clickIOS(user_profile_link);
			if ((liftmaster ? liftmaster_logo:chamberlain_logo).isEnabled() && user_profile_password_msg.isEnabled()) {
				utilityIOS.enterTextIOS(password_field, password);
				utilityIOS.clickIOS(login_btn);
				utilityIOS.waitForElementIOS(user_name_label, "visibility");
			} else {
				extentTestIOS.log(Status.INFO, "view for accessing the Account view is not shown"); 	
				utilityIOS.captureScreenshot(iOSDriver);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}


	/**
	 * Description: To verify if the user account page is shown
	 * @return Page object
	 */
	public boolean verifyUserAccountShown() {
		try { 
			if (user_name_label.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description: To get the User Name from MenuLink
	 * @return currentValue
	 */
	public String getUserNameMenuLink() {
		String userName = null;
		try {
			userName = user_profile_link.getText();
			return userName;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return userName;
		}	
	}

}