package com.liftmaster.myq.ios.pages;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.google.common.base.Function;
import com.liftmaster.myq.testmethods.IOS;
import com.liftmaster.myq.utils.UtilityIOS;
import com.liftmaster.myq.utils.WebServices;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import sun.awt.image.ShortInterleavedRaster;

public class DevicesPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS = null;
	static MobileElement actuatingDevice;
	public static int deviceIndex = 1;

	String currentPlaceName;
	String deviceStatus;
	String deviceTimer;
	boolean deviceNamePresent = false;
	boolean placeNamePresent = false;

	//Page factory for Devices Page IOS
	//	@iOSFindBy(xpath = "//XCUIElementTypeCollectionView")	
	//	public IOSElement devices_list;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")	
	public IOSElement devices_list;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell")	
	public IOSElement cell_1;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_hub_label")	
	public List<IOSElement> hub_name_list;

	@iOSFindBy(accessibility = "dashboard_error_triangle_icon")	
	public List<IOSElement> error_icon_list;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_device_label")	
	public List<IOSElement> device_name_list;

	@iOSFindBy(accessibility = "dashboard_error_message_label")	
	public List<IOSElement> error_label_list;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_time_label")	
	public List<IOSElement> device_time_list;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_control_label")	
	public List<IOSElement> device_control_list;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_activity_indicator")	
	public List<IOSElement> activity_indicator_list;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_state_label")	
	public List<IOSElement> device_state_list;

	@iOSFindBy(accessibility = "dashboard_stronghold_icon")	
	public List<IOSElement> stronghold_icon_list;

	@iOSFindBy(accessibility = "TURN ON")	
	public IOSElement device_on_toggle;

	@iOSFindBy(accessibility = "TURN OFF")	
	public IOSElement device_off_toggle; 

	@iOSFindBy(accessibility = "dots_image_view")	
	public IOSElement blue_bar;

	@iOSFindBy(xpath = "//XCUIElementTypeSheet//./XCUIElementTypeOther/XCUIElementTypeButton")	
	public List<IOSElement> roles_list;

	@iOSFindBy(accessibility = "Devices")
	public IOSElement home_screen;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement liftmaster_logo;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement chamberlain_logo;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar[@accessibility='Login']")	
	public IOSElement password_window_heading;

	@iOSFindBy(className = "XCUIElementTypeSecureTextField")
	public List<IOSElement> passcode_fields;

	@iOSFindBy(accessibility = "Cancel")	
	public IOSElement cancel_btn;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	@iOSFindBy(accessibility = "password_text_field")
	public IOSElement password_field;

	@iOSFindBy(accessibility = "login_button")
	public IOSElement login_btn;

	//API Specific
	@iOSFindBy(xpath = "//XCUIElementTypeTable//./XCUIElementTypeStaticText[@label ='Add New']")	
	public IOSElement add_new_hub;

	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value = '1234']")	
	public IOSElement first_cell_textbox;

	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value = '567']")	
	public IOSElement second_cell_textbox;

	@iOSFindBy(xpath = "//XCUIElementTypeTextField[@value = '89A']")	
	public IOSElement third_cell_textbox;

	@iOSFindBy(accessibility = "Next")	
	public IOSElement next_btn;

	@iOSFindBy(accessibility = "Pair")	
	public IOSElement pair_btn;

	@iOSFindBy(accessibility = "Finish")	
	public IOSElement finish_btn;

	@iOSFindBy(accessibility = "name_text_field")	
	public IOSElement gateway_name_textbox;

	@iOSFindBy(accessibility = "Submit")	
	public IOSElement submit_btn;

	@iOSFindBy(accessibility = "Light Control")	
	public IOSElement add_light;

	@iOSFindBy(xpath = "//XCUIElementTypeTextField")	
	public IOSElement elem_name;

	@iOSFindBy(accessibility = "Delete")	
	public IOSElement delete_btn;

	@iOSFindBy(accessibility = "dashboard_add_button")
	public IOSElement add_btn;

	@iOSFindBy(accessibility = "device_management_add_bar_button")
	public IOSElement hub_add_button;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")	
	public IOSElement gateway_name;

	@iOSFindBy(accessibility = "Garage Door Opener")	
	public IOSElement add_gdo;

	@iOSFindBy(accessibility = "Commercial Door Opener")	
	public IOSElement add_cdo;

	@iOSFindBy(accessibility = "Gate Operator")	
	public IOSElement add_gate;

	@iOSFindBy(xpath = "//XCUIElementTypeButton")	
	public IOSElement back_btn;

	@iOSFindBy(accessibility = "HamburgerMenu")	
	public  IOSElement hamburger_menu_btn;

	@iOSFindBy(accessibility = "name_text_field")	
	public IOSElement device_name_text_box;

	@iOSFindBy(accessibility = "icon_stronghold")	
	public  IOSElement gdo_lock_image;

	@iOSFindBy(accessibility = "Waiting for device...")	
	public  IOSElement waiting_for_device_text;

	@iOSFindBy(accessibility = "dashboard_switch_layout_button_0")	
	public  IOSElement device_list_button;

	@iOSFindBy(accessibility = "dashboard_switch_layout_button_1")	
	public  IOSElement device_grid_button;

	@iOSFindBy(accessibility = "dashboard_switch_layout_button_2")	
	public  IOSElement device_single_view_button;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText[1]")	
	public IOSElement hub_name_in_grid_view;

	@iOSFindBy(xpath = "//XCUIElementTypePageIndicator")	
	public  IOSElement page_indicator_in_grid_view;

	@iOSFindBy(accessibility = "Yes")	
	public  IOSElement yes_sensor_btn;

	@iOSFindBy(accessibility = "No")	
	public  IOSElement no_sensor_btn;

	@iOSFindBy(accessibility = "Are you using a door sensor?")	
	public IOSElement sensor_confirmation_text;

	@iOSFindBy(accessibility = "Select the Brand/Model of your garage door opener.")	
	public IOSElement gdo_brand_selection_screen;

	@iOSFindBy(accessibility = "(tap to select)")	
	public IOSElement brand_select_button;

	@iOSFindBy(accessibility = "(tap to select)")	
	public IOSElement color_select_button;

	@iOSFindBy(accessibility = "Search")	
	public IOSElement search_field_gdo_brands;

	@iOSFindBy(accessibility = "AccessMaster")	
	public IOSElement first_gdo_in_list;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_gdo_brands;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement list_of_button_colors;

	@iOSFindBy(accessibility = "program_button")	
	public IOSElement program_gdo_button;

	@iOSFindBy(accessibility = "Continue")	
	public IOSElement continue_button;

	@iOSFindBy(accessibility = "I have pressed the program button")	
	public IOSElement program_button_confirmation_popup;

	@iOSFindBy(accessibility = "Transfer Device")	
	public IOSElement transfer_device_button;

	@iOSFindBy(xpath = "//XCUIElementTypeApplication/XCUIElementTypeWindow/XCUIElementTypeOther[2]/XCUIElementTypeOther[2]/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther[2]/XCUIElementTypeTable")
	public IOSElement gateway_and_device_list;

	@iOSFindBy(accessibility = "child_device_text_field")
	public IOSElement gdo_device_text_box;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeButton")	
	public IOSElement help_link;

	@iOSFindBy(xpath = "//XCUIElementTypeOther[2]/XCUIElementTypeStaticText[@value='1']")
	public IOSElement help_text;

	@iOSFindBy(accessibility = "OK")	
	public IOSElement ok_button;

	@iOSFindBy(accessibility = "Error")	
	public IOSElement error_title;

	@iOSFindBy(accessibility = "MyQ Troubleshooting")	
	public IOSElement myq_troubleshooting_text;

	@iOSFindBy(accessibility = "You have no devices on your account")
	public IOSElement no_device_text;

	@iOSFindBy(accessibility = "dashboard_switch_layout_button")
	public IOSElement layout_change_switch;
	// to be removed....
	@iOSFindBy(accessibility = "Home Data")
	public IOSElement home_data_button;

	@iOSFindBy(xpath = "//XCUIElementTypeButton[@name='Settings']")
	public IOSElement settings_back;

	@iOSFindBy(xpath = "//XCUIElementTypeCell[@name='Wi-Fi']")
	public IOSElement wifi_btn;

	@iOSFindBy(accessibility = "Return to Chamberlain")
	public IOSElement return_btn;

	@iOSFindBy(accessibility = "dashboard_vertical_device_list_hub_label")
	public IOSElement gridview_hubname_label;

	@iOSFindBy(accessibility = "dashboard_horizontal_device_list_parent_device_name_label")
	public IOSElement galleryview_hubname_label;

	@iOSFindBy(accessibility = "main_label")
	public List<IOSElement> hubs_added;

	@iOSFindBy(accessibility = "add_button")
	public IOSElement get_started_button;

	@iOSFindBy(accessibility = "dashboard_horizontal_device_list_device_name_label")
	public IOSElement galleryview_device_name;

	@iOSFindBy(accessibility = "dashboard_stronghold_icon")
	public IOSElement galleryview_stronghold_icon;

	@iOSFindBy(accessibility = "success_label")
	public IOSElement success_text;
	/**  
	 * Description This is the Initialize method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public DevicesPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}


	/**
	 * Description: To find out the expected device name exist in the Devices page
	 * @param deviceName
	 * @return deviceNamePresent
	 */
	public boolean verifyDeviceNamePresent(String deviceName) { 
		try {
			boolean deviceVisible;
			boolean topitem;
			do {
				topitem = device_name_list.get(0).isDisplayed();
				utilityIOS.scroll("up");
			} while (!topitem);
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter < noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					if(device_name_list.get(counter).isDisplayed()) {
						deviceNamePresent = true;
						deviceIndex = counter;
						extentTestIOS.log(Status.INFO, "device found , device name is " +currentDeviceName);
						break;
					}  else {
						do {
							utilityIOS.swipe(devices_list,"up");
							deviceVisible = device_time_list.get(counter).isDisplayed();
							try {
								if((deviceVisible) && (!device_name_list.get(counter).isDisplayed())) {
									utilityIOS.scroll("up");
								}
							} catch (Exception e) {

							}
							extentTestIOS.log(Status.INFO, "device found , device name is " +currentDeviceName);
						} while (!deviceVisible);
						if(deviceVisible) {
							deviceNamePresent = true;
							deviceIndex = counter;
							break;
						} else {
							break;
						}
					}
				} 
			}
			return deviceNamePresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return deviceNamePresent;
		}
	}

	/**
	 * Description: To find out the expected device name exist in the Devices page
	 * @param deviceName
	 * @return deviceNamePresent
	 */
	public DevicesPageIOS tapOnCancelBtn() { 
		try {
			utilityIOS.clickIOS(cancel_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To tap on the device to perform an action
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageIOS tappingDevice(String deviceName) { 
		String currentDeviceName;
		try {
			//Finding the number of devices present in hierarchy
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				currentDeviceName = device_name_list.get(counter).getText();
				//once found perform the device actutaion
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					utilityIOS.clickIOS(device_name_list.get(counter));
					utilityIOS.waitForElementIOS(device_control_list.get(counter), "clickable");
					utilityIOS.clickIOS(device_control_list.get(counter));
					for (int i = 0; i <= 60; i++) {
						try {                        
							if ((activity_indicator_list.get(counter).isEnabled())) {
								Thread.sleep(2000);
								//Here it will wait for maximum 120 second,  the maximum timeout till the device not responding popup comes. if the device responds earlier then it will come out of loop
							}
						} catch (Exception e) {
							break;
						}
					}
					Thread.sleep(5000);
					try {
						if (iOSDriver.switchTo().alert().getText().contains("No Response")) {
							extentTestIOS.log(Status.INFO, "Device is not responding");
							iOSDriver.switchTo().alert().accept();    
						}
					} catch (Exception e) {
					}
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To actuate the device based on the index found by verify device method
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageIOS actuatingDevice(String deviceName) { 
		try { 
			if(device_name_list.get(deviceIndex).getText().equalsIgnoreCase(deviceName)) {
				utilityIOS.clickIOS(device_name_list.get(deviceIndex));
				utilityIOS.waitForElementIOS(device_control_list.get(deviceIndex), "clickable");
				utilityIOS.clickIOS(device_control_list.get(deviceIndex));
				for (int i = 0; i <= 60; i++) {
					try {                        
						if ((activity_indicator_list.get(deviceIndex).isDisplayed())) {
							Thread.sleep(2000);
							//Here it will wait for maximum 120 second,  the maximum timeout till the device not responding popup comes. if the device responds earlier then it will come out of loop
						}
					} catch (Exception e) {
						break;
					}
				}
				Thread.sleep(5000);
				try {
					if (iOSDriver.switchTo().alert().getText().contains("No Response")) {
						extentTestIOS.log(Status.INFO, "Device is not responding");
						iOSDriver.switchTo().alert().accept();    
					}
				} catch (Exception e) {
				}
			} else { 
				tappingDevice(deviceName);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description:  This methods gets the current state of the device based on the index of the device found by verify device method
	 				and if the device is not matching it first finds the device and then its status
	 * @param deviceName
	 * @return deviceStatus
	 */
	public String getDeviceStatus(String deviceName) { 
		try {
			String currentDeviceName = device_name_list.get(deviceIndex).getText();
			if (currentDeviceName.equalsIgnoreCase(deviceName)) {
				deviceStatus = device_state_list.get(deviceIndex).getText();
			} else {
				int noOfDevices = device_name_list.size();
				for (int counter = 0; counter < noOfDevices; counter++) {
					currentDeviceName = device_name_list.get(counter).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						deviceStatus = device_state_list.get(counter).getText();
						break;
					}
				}
			}
			return deviceStatus;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return deviceStatus;
		}	 
	}

	/**
	 * Description: This function will return true or false depending if alert is present
	 * @return alertPresent
	 */
	public boolean validateAlert() { 
		try {
			//			WebDriverWait wait = new WebDriverWait(iOSDriver, 10);
			//			wait.until(ExpectedConditions.alertIsPresent());
			//Custom wait logic till alert is present
			for (int i = 0; i <= 15; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			iOSDriver.switchTo().alert();
			return true;	
			//Here catch is not done through the re-usable exception handler, as the alert might be present or not present
		} catch (Exception e) {
			return false;
		}	 
	}

	/**
	 * Description: This function will do polling on the device timer for duration of 1 minute
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageIOS validateDeviceTimer(String deviceName) { 
		try {
			int noOfDevices = devices_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int counter = 1; counter <= noOfDevices; counter++) {
				String currentDeviceName = devices_list.findElementByXPath("//XCUIElementTypeCell[" + counter + "]//./XCUIElementTypeStaticText[2]").getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					MobileElement countdownTimer =  devices_list.findElementByXPath("//XCUIElementTypeCell[" + counter + "]//./XCUIElementTypeStaticText[4]");
					//fluent wait for 1 minute
					try { 
						FluentWait<IOSDriver> wait = new FluentWait<IOSDriver>(iOSDriver);
						wait.pollingEvery(1000, TimeUnit.MILLISECONDS);
						wait.withTimeout(60, TimeUnit.SECONDS);
						Function<IOSDriver, Boolean> function = new Function<IOSDriver, Boolean>() {
							public Boolean apply(IOSDriver arg0) {
								String timerText = countdownTimer.getText();
								if (timerText.contains("MINUTE")) {
									return true;
								}
								return false;
							}
						};
						wait.until(function);
					} catch (Exception e) {
						//There has been no statement in catch block as I just want to continue the execution after catching the exception
					}
					break;
				}
			}
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: This function will do polling on the device status for duration of 20 seconds
	 * @param deviceName
	 */
	public boolean validateDeviceStatus(String deviceName, String state) { 
		boolean deviceStatusUpdated = false;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					MobileElement deviceState =  device_state_list.get(counter);
					state = state.toUpperCase();
					if (state.contains("ON")) {
						state = "ON";
					} else if (state.contains("OFF")) {
						state = "OFF";
					} else if (state.contains("CLOSE")) {
						state = "CLOSED";
					}
					String stateNeeded = state;
					//fluent wait for 20 Seconds
					try { 
						FluentWait<IOSDriver> wait = new FluentWait<IOSDriver>(iOSDriver);
						wait.pollingEvery(1000, TimeUnit.MILLISECONDS);
						wait.withTimeout(20, TimeUnit.SECONDS);
						Function<IOSDriver, Boolean> function = new Function<IOSDriver, Boolean>() {
							public Boolean apply(IOSDriver arg0) {
								String statusText = deviceState.getText();
								if (statusText.equalsIgnoreCase(stateNeeded)) {
									return true;
								}
								return false;
							}
						};
						deviceStatusUpdated = wait.until(function);
					} catch (Exception e) {
						//There has been no statement in catch block as I just want to continue the execution after catching the exception
					}
					break;
				}
			}
			return deviceStatusUpdated;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return deviceStatusUpdated;
		}	 
	}

	/**
	 * Description: To get the alert message and to accept the alert
	 * @return alertMessage
	 */
	public String getAlertMessage() { 
		String alertMessage = null;
		alertMessage = iOSDriver.switchTo().alert().getText();
		try {
			extentTestIOS.log(Status.INFO,"Alert Text is : " +alertMessage);	
			iOSDriver.switchTo().alert().accept();
			return alertMessage;
		} catch (Exception e) {
			return alertMessage;
		}	 
	}


	/**
	 * Description: To get the alert message and to accept the alert
	 * @return alertMessage
	 */
	public DevicesPageIOS checkFeasubility() { 
		try {
			utilityIOS.clickIOS(add_btn);
			Thread.sleep(2000);
			utilityIOS.clickIOS(home_data_button);
			Thread.sleep(2000);
			utilityIOS.clickIOS(settings_back);
			Thread.sleep(2000);
			utilityIOS.swipe(devices_list, "down");
			Thread.sleep(5000);
			utilityIOS.clickIOS(wifi_btn);
			Thread.sleep(2000);
			utilityIOS.clickIOS(return_btn);
			return this;
		} catch (Exception e) {
			return this;
		}	 
	}
	/**
	 *Description: To get the the Place Name 
	 * @param placeName
	 * @return deviceNamePresent
	 */
	public boolean getPlaceName(String placeName) { 
		try {
			int noOfDevices = hub_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				currentPlaceName = hub_name_list.get(counter).getText();
				currentPlaceName = currentPlaceName.split(": ")[1];
				if (currentPlaceName.equalsIgnoreCase(placeName)) {
					placeNamePresent = true;
					break;
				}
			}
			return placeNamePresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return placeNamePresent;
		}	 
	}

	/**
	 * Description : This function would click on the blue bar on device screen 
	 * @return
	 */
	public DevicesPageIOS clickOnBlueBar() {
		try {
			utilityIOS.waitForElementIOS(blue_bar, "clickable");
			if (blue_bar.isEnabled()) {
				utilityIOS.clickIOS(blue_bar);
			}
		} catch (Exception e) {
			//catch the exception and continue with the execution.
		}
		return this;
	}

	/**
	 * Description : This function would verify if the Blue bar is present or not
	 * @return blueBarPresent
	 */
	public boolean verifyBlueBarPresent() {
		boolean blueBarPresent = false;
		try {
			utilityIOS.waitForElementIOS(blue_bar, "visibility");
			if (blue_bar.isEnabled()) {
				blueBarPresent = true;
			}
		} catch (Exception e) {
			//catch the exception and continue with the execution.
		}
		return blueBarPresent;
	}

	/**
	 * Description : This function would select the user role 
	 * @param accountName
	 * @param role
	 * @return Page Object
	 */
	public DevicesPageIOS selectUserRole(String accountName, String role) {
		int iIndex = 0;
		try {
			for (iIndex = 0 ; iIndex < roles_list.size(); iIndex++) {
				if (role.trim().equalsIgnoreCase("Creator")
						&& roles_list.get(iIndex).getText().toUpperCase().trim().contains("ME")) {
					utilityIOS.clickIOS(roles_list.get(iIndex));
					utilityIOS.waitForElementIOS(blue_bar, "clickable");
					break;
				} else if (roles_list.get(iIndex).getText().toUpperCase().trim().contains(accountName.trim().toUpperCase())
						&& roles_list.get(iIndex).getText().toUpperCase().trim().contains(role.trim().toUpperCase())) {
					utilityIOS.clickIOS(roles_list.get(iIndex));
					utilityIOS.waitForElementIOS(blue_bar, "clickable");
					break;
				}
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description : This function would verify if the Account name and role are matching with the passed parameter
	 * @param role
	 * @param accountName
	 * @return userPresent;
	 */
	public boolean verifyUserPresent(String accountName, String role) {
		int iIndex = 0;
		boolean userPresent = false;
		try {
			for (iIndex = 0 ; iIndex < roles_list.size(); iIndex++) {
				if (role.trim().equalsIgnoreCase("Creator") 
						&& roles_list.get(iIndex).getText().toUpperCase().trim().contains("ME")) {
					userPresent = true;
					break;
				} else if (roles_list.get(iIndex).getText().toUpperCase().trim().contains(accountName.trim().toUpperCase())
						&& roles_list.get(iIndex).getText().toUpperCase().trim().contains(role.trim().toUpperCase())) {
					userPresent = true;
					break;
				}
			}
			return userPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return userPresent;
		}

	}

	/**
	 * Description : This function would verify the number of devices
	 * @param noOfDevicesExpected
	 * @return boolean
	 */
	public boolean verifyNumberOfDevices(int noOfDevicesExpected) { 
		try {
			try {
				if(noOfDevicesExpected==0 && no_device_text.isDisplayed()) {
					return true;
				}
			} catch (Exception e) {

			}
			utilityIOS.scroll("down");
			int noOfDevices = device_name_list.size();
			if (noOfDevicesExpected == noOfDevices+1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description: To verify the presence of Add button 
	 * @return boolean
	 */
	public boolean verifyAddDeviceButton() {
		try {
			if (add_btn.isDisplayed() && add_btn.isEnabled()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description: To verify the presence of home screen after login
	 * @return boolean
	 */
	public boolean verifyHomeScreen() {
		try {
			if (home_screen.isDisplayed()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description: To tap on the device to perform an action after entering the passcode
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageIOS tappingDeviceWithPasscode(String deviceName, String passcode) { 
		String currentDeviceName;
		try {
			boolean liftmaster;
			//to check if the execution is running for LiftMaster or chamberlain
			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
				liftmaster = true;
			} else {
				liftmaster = false;
			}
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					//Tapping on device for actuation
					utilityIOS.clickIOS(device_name_list.get(counter));
					utilityIOS.waitForElementIOS(device_control_list.get(counter), "clickable");
					utilityIOS.clickIOS(device_control_list.get(counter));
					//Before entering passcode verifying if the Passcode screen is shown or not
					if ((liftmaster ? liftmaster_logo:chamberlain_logo).isEnabled()) {
						//Getting the Passcode field size and entering the Passcode
						int sizeOfPasscodeFields = passcode_fields.size();

						for (int i=0; i < sizeOfPasscodeFields; i++) {
							utilityIOS.enterTextIOS(passcode_fields.get(i),String.valueOf(passcode.charAt(i)));
						}
						if (activity_indicator.isDisplayed()) {
							utilityIOS.waitForElementIOS(devices_list, "visibility");
						}
					} 
					for (int i = 0; i <= 60; i++) {
						try {    
							//waiting for the activity indicator to disappear
							if ((activity_indicator_list.get(counter)).isEnabled()) {
								Thread.sleep(2000);
								//Here it will wait for maximum 120 second,  the maximum timeout till the device not responding popup comes. if the device responds earlier then it will come out of loop
							}
						} catch (Exception e) {
							break;
						}
					}
					// A generic wait if the device is not responding and wait for an alert to come
					Thread.sleep(5000);
					try {
						if (iOSDriver.switchTo().alert().getText().contains("No Response")) {
							extentTestIOS.log(Status.INFO, "Device is not responding");
							iOSDriver.switchTo().alert().accept();    
						}
					} catch (Exception e) {
					}
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To tap on the device to perform an action after entering the Password
	 * @param deviceName
	 * @return Page object
	 */
	public DevicesPageIOS tappingDeviceWithPassword(String deviceName, String password) { 
		String currentDeviceName;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					//Tapping on device for actuation
					utilityIOS.clickIOS(device_name_list.get(counter));
					utilityIOS.waitForElementIOS(device_control_list.get(counter), "clickable");
					utilityIOS.clickIOS(device_control_list.get(counter));
					//Before entering password verifying if the login button is shown or not
					if (login_btn.isEnabled()) {
						//Entering the Password
						utilityIOS.enterTextIOS(password_field, password);
						utilityIOS.clickIOS(login_btn);
						if (activity_indicator.isDisplayed()) {
							utilityIOS.waitForElementIOS(devices_list, "Visibility");
						}
					} 
					for (int i = 0; i <= 60; i++) {
						try {              
							//waiting for the activity indicator to disappear
							if ((devices_list.findElementByXPath("//XCUIElementTypeCell[" + counter + "]//./XCUIElementTypeActivityIndicator").isEnabled())) {
								Thread.sleep(2000);
								//Here it will wait for maximum 120 second,  the maximum timeout till the device not responding popup comes. if the device responds earlier then it will come out of loop
							}
						} catch (Exception e) {
							break;
						}
					}
					// A generic wait if the device is not responding and wait for an alert to come
					Thread.sleep(5000);
					try {
						if (iOSDriver.switchTo().alert().getText().contains("No Response")) {
							extentTestIOS.log(Status.INFO, "Device is not responding");
							iOSDriver.switchTo().alert().accept();    
						}
					} catch (Exception e) {
					}
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	//API Specific
	/**
	 * Description : This function would add the gateway into the account on the UI
	 * @param gatewaySerialNumber
	 * @return Page Object
	 */
	public DevicesPageIOS addGateWay(String gatewaySerialNumber,String gatewayName) throws InterruptedException, AWTException {
		try {
			Thread.sleep(5000);
			utilityIOS.clickIOS(add_new_hub);
			Thread.sleep(1000);

			String MyQSrNumber = gatewaySerialNumber.substring(2);
			String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
			String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
			String textBox3 = MyQSrNumber.split(" ")[0].substring(7);


			utilityIOS.clickIOS(first_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox1);
			Thread.sleep(500);

			utilityIOS.enterGWSerialNoInTextBox(textBox2);
			Thread.sleep(500);

			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(textBox3), null);
			utilityIOS.clickIOS(third_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox3);
			Thread.sleep(500);
			utilityIOS.clickIOS(next_btn);
			Thread.sleep(10000);
			// Adding name to Gateway and saving it
			utilityIOS.clearIOS(gateway_name_textbox);
			utilityIOS.enterTextIOS(gateway_name_textbox, gatewayName);
			iOSDriver.hideKeyboard();
			utilityIOS.clickIOS(next_btn);
			Thread.sleep(5000);
			tapOnCancelBtn();
			Thread.sleep(3000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select Light device from the list of device to be added in Gateway
	 * @return Page object
	 */
	public DevicesPageIOS clickRemoteLightButton() throws InterruptedException {
		try {
			utilityIOS.clickIOS(add_light);
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would click on the next button that appears while adding an EPD
	 * @return Page object
	 */
	public DevicesPageIOS clickNextButton() throws InterruptedException {
		try {
			// Adding new Device and waiting for learn button to press
			utilityIOS.waitForElementIOS(next_btn,"clickable");
			utilityIOS.clickIOS(next_btn);
			for (int i = 0; i <= 60; i++) {
				try {                        
					if (waiting_for_device_text.isEnabled()) {
						Thread.sleep(1000);
						//Here it will wait for maximum 60 second for the device to get added
					}
				} catch (Exception e) {
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would enter the device Name after adding
	 * @return Page object
	 */
	public DevicesPageIOS enterDeviceName(String deviceName) throws InterruptedException {
		try {
			utilityIOS.waitForElementIOS(next_btn,"clickable");
			if(next_btn.isEnabled()) {
				utilityIOS.clearIOS(device_name_text_box);
				utilityIOS.enterTextIOS(device_name_text_box, deviceName);
				iOSDriver.hideKeyboard();
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would click on the finish button that appears after adding an VGDO
	 * @return Page object
	 */
	public DevicesPageIOS clickFinishButton() throws InterruptedException {
		try {
			utilityIOS.waitForElementIOS(finish_btn,"clickable");
			utilityIOS.clickIOS(finish_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}


	/**
	 * Description : This function would check whether the notification popup was received as soon as the lock was attached to the GDO
	 * @return boolean
	 */
	public boolean verifyAlertForLock() throws InterruptedException {
		try {
			String lockAttachMessage = "Your LiftMaster Garage Door Lock is ready on";
			// This below code is kept commented intentionally till a fix is provided by Appium. The version which have issue is 1.7.2	
			//	WebDriverWait wait = new WebDriverWait(iOSDriver, 20);
			//	wait.until(ExpectedConditions.alertIsPresent());

			//Custom wait logic till alert is present
			for (int i = 0; i <= 20; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			String alertText =	iOSDriver.switchTo().alert().getText();
			iOSDriver.switchTo().alert().accept();
			if(alertText.contains(lockAttachMessage)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description : This function would check if the lock is shown on the GDO
	 * @return Page object
	 */
	public boolean verifyLockOnSGDO(String deviceName) throws InterruptedException {
		boolean gdoLockPresent = false;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					try {
						if(stronghold_icon_list.get(counter).isDisplayed()) {
							gdoLockPresent = true;
							break;
						} else {
							break;
						}
					} catch (Exception e) {
						// If the lock is not present then also continue with execution	
					}
				}
			}
			return gdoLockPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description : This function would click on the submit button
	 * @return Page object
	 */
	public DevicesPageIOS clickSubmitButton() throws InterruptedException {
		try {
			// Adding new Device and waiting for learn button to press
			utilityIOS.waitForElementIOS(submit_btn, "clickable");
			utilityIOS.clickIOS(submit_btn);
			utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would click on "+" icon to add new devices
	 * @param 
	 * @return
	 */
	public DevicesPageIOS addNewDevice() throws InterruptedException {
		try {
			utilityIOS.clickIOS(add_btn);
			try {
				iOSDriver.switchTo().alert().accept();	
			} catch (Exception e) {
				//For accepting the Alert for external device when navigating for the first time
			}
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select the GDO device while adding the device on a hub
	 * @return Page Object
	 */
	public DevicesPageIOS clickGDOButton() throws InterruptedException {
		try {
			utilityIOS.clickIOS(add_gdo);
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select the CDO device while adding the device on a hub
	 * @return Page Object
	 */
	public DevicesPageIOS clickCDOButton() throws InterruptedException {
		try {
			utilityIOS.clickIOS(add_cdo);
			utilityIOS.waitForElementIOS(next_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select on the Gate add button while adding devices on the hub
	 * @param 
	 * @return Page Object
	 */
	public DevicesPageIOS clickGateButton() throws InterruptedException {
		try {
			utilityIOS.clickIOS(add_gate);
			utilityIOS.waitForElementIOS(next_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select the gateway from list
	 * @param 
	 * @return
	 */
	public DevicesPageIOS selectGateway(String gatewayName) throws InterruptedException {
		try {
			String currentHubName;
			int noOfHubs = hubs_added.size();
			for (int counter = 0; counter <= noOfHubs; counter++) {
				currentHubName = hubs_added.get(counter).getText();
				if (currentHubName.equalsIgnoreCase(gatewayName)) {
					utilityIOS.clickIOS(hubs_added.get(counter));
					break;
				}
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
		return this;
	}

	/**
	 * Description : This function would verify if the gateway is shown
	 * @param gatewayName
	 * @return boolean
	 */
	public boolean verifyGatewayAvailable(String gatewayName) throws InterruptedException {
		boolean gatewayAdded = false;
		try {
			String currentHubName;
			int noOfHubs = hubs_added.size();
			for (int counter = 0; counter < noOfHubs; counter++) {
				currentHubName = hubs_added.get(counter).getText();
				if (currentHubName.equalsIgnoreCase(gatewayName)) {
					gatewayAdded = true;
					break;
				}
			}
			return gatewayAdded;
		} catch (Exception e) {
			return gatewayAdded;
		}
	}

	/**
	 * Description : This function would tap on the toggle button to change the view
	 * @param gridView
	 * @return page object
	 */
	public DevicesPageIOS toggleDeviceLayout(boolean gridView) throws InterruptedException {
		try {
			try{
				if(gridView && device_list_button.isDisplayed()) {
					utilityIOS.clickIOS(device_list_button);	
				} 
			} catch (Exception e) {
			}
			try{
				if (!gridView && device_grid_button.isDisplayed()) {
					utilityIOS.clickIOS(device_grid_button);
					utilityIOS.clickIOS(device_single_view_button);
				}
			} catch (Exception e) {
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}


	/**
	 * Description : checking the lock icon on SGDO in grid view
	 * @param hubName
	 * @param deviceName
	 * @return boolean
	 */
	public boolean checkLockIconInGridView(String hubName, String deviceName) throws InterruptedException {
		boolean gdoLockPresent = false;
		boolean hubPresent= false;
		try {
			int loopindex = Integer.parseInt(page_indicator_in_grid_view.getAttribute("value").split("of ")[1]);
			for(int i=1; i<=loopindex; i++) {
				try {
					if(gridview_hubname_label.getAttribute("label").toUpperCase().contains(hubName.toUpperCase())) {
						hubPresent =true;
					} 
				} catch (Exception e1) {
				}
				if(hubPresent) {
					int noOfDevices = device_name_list.size();
					for (int counter = 0; counter < noOfDevices; counter++) {
						String currentDeviceName = device_name_list.get(counter).getText();
						if (currentDeviceName.equalsIgnoreCase(deviceName)) {
							if(stronghold_icon_list.get(counter).isDisplayed()) {
								gdoLockPresent = true;
								break;
							}
						}
					}
				} else {
					utilityIOS.swipe(devices_list, "left");
				}
			}
			return gdoLockPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return gdoLockPresent;
		}
	}


	/**
	 * Description: To validate whether the lock icon is present while changing the Device state
	 * @param deviceName
	 * @return boolean
	 */
	public boolean validatingLockIconWhileChangingState(String deviceName) { 
		String currentDeviceName;
		boolean gdoLockPresent = false;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					utilityIOS.clickIOS(device_name_list.get(counter));
					utilityIOS.waitForElementIOS(device_control_list.get(counter), "clickable");
					utilityIOS.clickIOS(device_control_list.get(counter));
					Thread.sleep(3000);
					//Here sleep has been added as the UI does not immediately reflect the changes after tapping on the button	 
					try {
						utilityIOS.captureScreenshot(iOSDriver);
						if(stronghold_icon_list.get(counter).isDisplayed()) {
							gdoLockPresent = true;
						}
					} catch (Exception e) {
						// If the lock is not present then alos continue with execution	
					}
					for (int i = 0; i <= 60; i++) {
						try {                        
							if ((activity_indicator_list.get(counter).isDisplayed())) {
								Thread.sleep(2000);
								//Here it will wait for maximum 120 second,  the maximum timeout till the device not responding popup comes. if the device responds earlier then it will come out of loop
							}
						} catch (Exception e) {
							break;
						}
					}
					Thread.sleep(5000);
					try {
						if (iOSDriver.switchTo().alert().getText().contains("No Response")) {
							extentTestIOS.log(Status.INFO, "Device is not responding");
							iOSDriver.switchTo().alert().accept();    
						}
					} catch (Exception e) {
					}
					break;
				}
			}
			return gdoLockPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return gdoLockPresent;
		}
	}


	/**
	 * Description : This function would add the WGDO/GDO into the account
	 * @param wgdoSerialNumber, gatewaySerialNumber
	 * @return Page Object
	 */
	public DevicesPageIOS addWGDOAndGDO(String wgdoSerialNumber,String wgdoDeviceSerialNumber) throws InterruptedException, AWTException {
		try {
			utilityIOS.clickIOS(add_new_hub);
			Thread.sleep(1000);
			String MyQSrNumber = wgdoSerialNumber.substring(2);
			String textBox1 = MyQSrNumber.split(" ")[0].substring(0, 4);
			String textBox2 = MyQSrNumber.split(" ")[0].substring(4, 7);
			String textBox3 = MyQSrNumber.split(" ")[0].substring(7);
			utilityIOS.enterGWSerialNoInTextBox(textBox1);
			Thread.sleep(1000);
			utilityIOS.clickIOS(second_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox2);
			Thread.sleep(500);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(textBox3), null);
			utilityIOS.clickIOS(third_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox3);
			Thread.sleep(500);
			utilityIOS.clickIOS(next_btn);
			Thread.sleep(10000);
			// Adding name to WGDO and saving it
			utilityIOS.clearIOS(gateway_name_textbox);
			utilityIOS.enterTextIOS(gateway_name_textbox, wgdoSerialNumber);
			utilityIOS.clearIOS(gdo_device_text_box);
			utilityIOS.enterTextIOS(gdo_device_text_box, wgdoDeviceSerialNumber);
			iOSDriver.hideKeyboard();
			utilityIOS.clickIOS(next_btn);
			if (activity_indicator.isDisplayed()) {
				utilityIOS.waitForElementIOS(add_btn, "Visibility");
			}

			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would rename the device
	 * @param deviceName
	 * @return Page Object
	 */
	public DevicesPageIOS renameDevice(String deviceName) {
		try {
			utilityIOS.clickIOS(elem_name);
			utilityIOS.clearIOS(elem_name);
			utilityIOS.enterTextIOS(elem_name, deviceName );
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}
	/**
	 * Description : This function tap on the YES button while adding sensor VGDO
	 * @return page object
	 */
	public DevicesPageIOS tapYesButton() throws InterruptedException {
		try {
			utilityIOS.waitForElementIOS(sensor_confirmation_text, "visibility");
			utilityIOS.clickIOS(yes_sensor_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function tap on the No button while adding sensor VGDO
	 * @return page object
	 */
	public DevicesPageIOS tapNoButton() throws InterruptedException {
		try {
			utilityIOS.waitForElementIOS(sensor_confirmation_text, "visibility");
			utilityIOS.clickIOS(no_sensor_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}


	/**
	 * Description : This function would select the GDO brand from the list of the brand
	 * @param gdoBrand
	 * @return Page object
	 */
	public DevicesPageIOS selectGDOBrand(String gdoBrand) throws InterruptedException {
		String currentGDO;
		try {
			utilityIOS.waitForElementIOS(gdo_brand_selection_screen, "visibility");
			utilityIOS.clickIOS(brand_select_button);
			utilityIOS.waitForElementIOS(first_gdo_in_list, "clickable");
			utilityIOS.clickIOS(search_field_gdo_brands);
			utilityIOS.enterTextIOS(search_field_gdo_brands, gdoBrand);
			int noOfGdos = list_of_gdo_brands.findElementsByXPath("XCUIElementTypeCell").size();
			for(int i= 1; i <= noOfGdos; i++) {
				currentGDO = list_of_gdo_brands
						.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
				if (gdoBrand.equalsIgnoreCase(currentGDO)) {
					utilityIOS.clickIOS(list_of_gdo_brands.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select the color of the program button from the list of color
	 * @param color
	 * @return Page object
	 */
	public DevicesPageIOS selectProgramButtonColor(String color) {
		String currentColor;
		try{
			utilityIOS.waitForElementIOS(color_select_button, "clickable");
			utilityIOS.clickIOS(color_select_button);
			int noOfColors = list_of_button_colors.findElementsByXPath("XCUIElementTypeCell").size();
			if(noOfColors>=1){
				for(int i= 1; i <= noOfColors; i++) {
					currentColor =	list_of_button_colors.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText").getText();
					if (color.equalsIgnoreCase(currentColor)) {
						utilityIOS.clickIOS(list_of_button_colors.findElementByXPath("XCUIElementTypeCell[" + i + "]/XCUIElementTypeStaticText"));
						break;
					}
				}
			}
			return this;
		}catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would click on the Program door opener button and click on the confirmation popup
	 * @return Page object
	 */
	public DevicesPageIOS tapProgramDoorOpener() throws InterruptedException {
		try {
			utilityIOS.waitForElementIOS(program_gdo_button, "clickable");
			if(program_gdo_button.isEnabled()) {
				utilityIOS.clickIOS(program_gdo_button);
				utilityIOS.waitForElementIOS(next_btn, "clickable");
				utilityIOS.clickIOS(next_btn);
				utilityIOS.waitForElementIOS(pair_btn, "clickable");
				utilityIOS.clickIOS(pair_btn);
			} else {
				extentTestIOS.log(Status.INFO, "Program dooor opener button not enabled");
				utilityIOS.captureScreenshot(iOSDriver);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}


	/**
	 * Description: To find out the error on the yellow bar of a device
	 * @param deviceName
	 * @param errorType
	 * @param errorMessage
	 * @return errorPresent
	 */
	public boolean verifyErrorOnDevice(String deviceName, String errorType, String errorMessage) {
		boolean errorPresent = false;
		boolean popupShown =false;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					utilityIOS.clickIOS(device_name_list.get(counter));
					try {
						if (iOSDriver.switchTo().alert().getText().contains(errorMessage)) {
							extentTestIOS.log(Status.INFO, " Error PopUp message text is: " +iOSDriver.switchTo().alert().getText());
							iOSDriver.switchTo().alert().accept();
							popupShown = true;
						}
					} catch (Exception e) {
					}
					String deviceError = error_label_list.get(counter).getText();
					if(deviceError.equalsIgnoreCase(errorType) && popupShown) {
						errorPresent = true;
						break;
					}
				}
			}
			return errorPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return errorPresent;
		}
	}

	/**
	 * Description: To verify the presence of Add button and click
	 * @return boolean
	 */
	public boolean verifyAddDeviceButtonAndClick() {
		try {
			if (add_btn.isDisplayed() && add_btn.isEnabled()) {
				utilityIOS.clickIOS(add_btn);
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}


	/**
	 * Description : This function would click on the Gateway once found
	 * @param gatewayName
	 * @return Page Object
	 */
	public DevicesPageIOS clickGateway(String gatewayName) throws InterruptedException {
		try {
			List<MobileElement> gatewayNames =  gateway_name.findElementsByXPath(".//*");
			int noOfElements = gatewayNames.size();
			for (int iIndex = 0;iIndex < noOfElements - 1; iIndex++)
			{ 
				String elementType = gatewayNames.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeCell")) {
					String hubName = gatewayNames.get(iIndex+1).getText();
					if (hubName.equalsIgnoreCase(gatewayName)) {
						utilityIOS.clickIOS(gatewayNames.get(iIndex+1));
						break;
					}
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would click on the transfer device button
	 * @return Page Object
	 */
	public DevicesPageIOS clickDeviceTransferButton() throws InterruptedException {
		try {
			utilityIOS.clickIOS(transfer_device_button);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would select device from gateway and click on it.
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verfiyDeviceAndClick(String deviceName) throws InterruptedException {
		try {
			utilityIOS.clickIOS(iOSDriver.findElementByAccessibilityId(deviceName));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description: To click on the back button
	 * @return this
	 */
	public DevicesPageIOS tapOnBackBtn() { 
		try {
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : This function would tap on help link on add new device screen page
	 * @return Page object
	 */
	public DevicesPageIOS tapHelplink(){
		try{
			utilityIOS.clickIOS(help_link);
		} catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description : To verify Help text is displayed successfully on screen
	 * @param text1
	 * @param text2
	 * @return boolean
	 */
	public boolean VerifyHelpText(String text1,String text2) {
		try { 
			utilityIOS.waitForElementIOS(help_text, "visibility");
			if (help_text.getAttribute("label").equalsIgnoreCase(text1) &&
					(iOSDriver.findElementByAccessibilityId(text2).getAttribute("label").equalsIgnoreCase(text2))) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return false;
		}
	}

	/**
	 * Description: To find out the error on the expected device
	 * @param deviceName
	 * @return errorPresent
	 */
	public boolean verifyWarningImageOnDevice(String deviceName) {
		boolean warningPresent = false;
		String currentDeviceName;
		try {
			currentDeviceName =device_name_list.get(deviceIndex).getText();
			if (deviceNamePresent) {
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					if(error_icon_list.get(deviceIndex).isDisplayed()) {
						warningPresent = true;
					}	
				}
			} else {
				int noOfDevices = device_name_list.size();
				for (int counter = 0; counter <= noOfDevices; counter++) {
					currentDeviceName = device_name_list.get(counter).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						if(error_icon_list.get(counter).isDisplayed()) {
							warningPresent = true;
							break;
						}
					}
				}
			}
			return warningPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return warningPresent;
		}
	}

	/**
	 * Description : This function would enter GW details on the UI
	 * @param GWSerialNumber
	 * @return
	 * @throws InterruptedException
	 */
	public DevicesPageIOS enterGWDetails(String GWSerialNumber) throws InterruptedException {

		String textBox1 = GWSerialNumber.split(",")[0];
		String textBox2 = GWSerialNumber.split(",")[1];
		String textBox3 = GWSerialNumber.split(",")[2];
		try {
			utilityIOS.clickIOS(hub_add_button);
			Thread.sleep(1000);
			utilityIOS.clickIOS(add_new_hub);
			Thread.sleep(1000);
			utilityIOS.clickIOS(first_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox1);
			Thread.sleep(500);
			utilityIOS.clickIOS(second_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox2);
			Thread.sleep(500);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(textBox3), null);
			utilityIOS.clickIOS(third_cell_textbox);
			utilityIOS.enterGWSerialNoInTextBox(textBox3);
			Thread.sleep(500);
			utilityIOS.clickIOS(next_btn);

		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description : verifying error message while adding the gateways with invalid serial number
	 * @param errorMessage
	 * @return boolean
	 * @throws InterruptedException
	 */
	public boolean verifyErrorMessage(String errorMessage) throws InterruptedException {

		boolean messageFound=false;

		try {
			for (int i=0; i<=30; i++) { //wait for popup to appear
				Thread.sleep(1000);
				try{
					if (iOSDriver.findElementByAccessibilityId(errorMessage).isDisplayed() == false ) {
						continue;
					} else {
						messageFound=true;
					}
					break;
				} catch (Exception e) {
					continue;//no need to log
				}
			}
			Thread.sleep(5000); // To ensure popup is displayed after 30 seconds implicit wait
			if (ok_button.isDisplayed()) {
				ok_button.click();
			} else {
				extentTestIOS.log(Status.ERROR, "No popup was displayed");	
			}
			back_btn.click();
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return messageFound;
	}
	/**
	 * Description: This function will return true or false depending if troubleshooting text is present on clicking the offline tab
	 * @return alertPresent
	 */
	public boolean validateTroubleShootingText() { 
		boolean textPresent = false;
		try {
			utilityIOS.waitForElementIOS(myq_troubleshooting_text, "visibility");
			if(myq_troubleshooting_text.getAttribute("label").equalsIgnoreCase("MyQ Troubleshooting")){
				textPresent = true;
				return textPresent;	
			} else {
				textPresent = false;
				return textPresent;
			}	
			//Here catch is not done through the re-usable exception handler, as the alert might be present or not present
		} catch (Exception e) {
			return textPresent;
		}	 
	}

	/**
	 * Description: To click on the error tab
	 * @return Page object
	 */
	public DevicesPageIOS tapOnErrorTab(String deviceName) { 
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter < noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					if(!error_label_list.get(counter).isDisplayed()) {
						utilityIOS.scroll("up");
					}
					utilityIOS.clickIOS(error_label_list.get(counter));  
					break;
				}
			}

		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description: To tap on the device and validate if error pop-up is present for No Response
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyDeviceNoResponse(String deviceName) { 
		String currentDeviceName;
		boolean errorPresent = false;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter <= noOfDevices; counter++) {
				currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					utilityIOS.clickIOS(device_name_list.get(counter));
					utilityIOS.waitForElementIOS(device_control_list.get(counter), "clickable");
					utilityIOS.clickIOS(device_control_list.get(counter));
					for (int i = 0; i <= 60; i++) {
						try {                        
							if ((activity_indicator_list.get(counter).isDisplayed())) {
								Thread.sleep(2000);
								//Here it will wait for maximum 120 second,  the maximum timeout till the device not responding popup comes. if the device responds earlier then it will come out of loop
							}
						} catch (Exception e) {
							break;
						}
					}
					Thread.sleep(5000);
					try {
						if (iOSDriver.switchTo().alert().getText().contains("No Response")) {
							extentTestIOS.log(Status.INFO, "Device is not responding");
							utilityIOS.captureScreenshot(iOSDriver);
							iOSDriver.switchTo().alert().accept();    
							errorPresent =  true;
						}
					} catch (Exception e) {
					}
					break;
				}
			}
			return errorPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return errorPresent;
		}
	}

	/**
	 * Description : This function would tap on submit button after the device is transfered from one gateway to another
	 * @return page object
	 */
	public DevicesPageIOS tapSubmitButtonForTransferDevice() {
		try{
			utilityIOS.clickIOS(submit_btn);
		} catch(Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
		}
		return this;
	}

	/**
	 * Description: To find if Yellow bar is present on the device
	 * @param deviceName
	 * @return errorPresent
	 */

	public boolean verifyYellowBarOnDevice(String deviceName) {

		boolean yellowBarPresent = false;
		String currentDeviceName;
		try {
			if(device_name_list.get(deviceIndex).getText().equalsIgnoreCase(deviceName)) {
				if(error_label_list.get(deviceIndex).isDisplayed()) {
					yellowBarPresent = true;
				} else {
					if((error_icon_list.get(deviceIndex).isDisplayed() && (!yellowBarPresent))){ 
						utilityIOS.scroll("up");
					}
				}
			}
			if(!yellowBarPresent) {
				int noOfDevices = device_name_list.size();
				for (int counter = 0; counter< noOfDevices; counter++) {
					currentDeviceName = device_name_list.get(counter).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						if(error_label_list.get(deviceIndex).isDisplayed()) {
							yellowBarPresent = true;
							break;
						}
					}
				}
			}
			return yellowBarPresent;
		} catch (Exception e) {
			return yellowBarPresent;
		}
	}

	/**
	 * Description: To find out the error on the yellow bar of a device for low battery
	 * @param deviceName
	 * @param errorType
	 * @param errorMessage
	 * @return errorPresent
	 */
	public boolean verifyErrorOnDeviceForLowBattery(String deviceName, String errorType, String errorMessage) {
		boolean errorPresent = false;
		boolean popupShown =false;
		try {
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter < noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					for(int i=0;i<60;i++) {
						try {
							if(ok_button.isDisplayed()) {
								if (iOSDriver.switchTo().alert().getText().equalsIgnoreCase(errorMessage)) {
									extentTestIOS.log(Status.INFO, " Error PopUp message text is: " +iOSDriver.switchTo().alert().getText());
									utilityIOS.clickIOS(ok_button);
									popupShown = true;
								} else {
									popupShown = false;
								}
								break;
							}
						} catch (Exception e) {
							Thread.sleep(1000);
						}
					}
					String deviceError = error_label_list.get(counter).getText();
					if(deviceError.equalsIgnoreCase(errorType) && popupShown) {
						errorPresent = true;
						break;
					}
				}
			}
			return errorPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return errorPresent;
		}
	}

	/**
	 * Description: To tap on get started button in devices page
	 *
	 */
	public DevicesPageIOS tapOnGetStartedBtn() { 
		try {
			utilityIOS.clickIOS(get_started_button);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To verify that the get started button is present on devices page
	 * @param 
	 * @return getStartedButtonPresent
	 */
	public boolean verifyGetStartedButton() { 
		boolean getStartedButtonPresent = false;
		try {
			if(get_started_button.isDisplayed()) {
				getStartedButtonPresent=true;
			}
		} catch (Exception e) {
			getStartedButtonPresent = false;
		}
		return getStartedButtonPresent;
	}

	/**
	 * Description : This function would tap on the toggle button to change the view
	 * @param gridView
	 * @return page object
	 */
	public DevicesPageIOS toggleGalleryView(boolean galleryView) throws InterruptedException {
		try {
			try{
				if(galleryView && device_list_button.isDisplayed()) {
					utilityIOS.clickIOS(device_list_button);	
					utilityIOS.clickIOS(device_grid_button);
				} 
			} catch (Exception e) {
			}
			try{
				if (!galleryView && device_single_view_button.isDisplayed()) {
					utilityIOS.clickIOS(device_single_view_button);
				}
			} catch (Exception e) {
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description : checking the lock icon on SGDO in gallery view
	 * @param hubName
	 * @param deviceName
	 * @return boolean
	 */
	public boolean checkLockIconInGalleryView(String hubName, String deviceName) throws InterruptedException {
		boolean gdoLockPresent = false;
		boolean hubPresent= false;
		try {
			int loopindex = Integer.parseInt(page_indicator_in_grid_view.getAttribute("value").split("of ")[1]);
			for(int i=1; i<=loopindex; i++) {
				try {
					if(galleryview_hubname_label.getAttribute("label").toUpperCase().contains(hubName.toUpperCase())) {
						hubPresent =true;
					} 
				} catch (Exception e1) {
				}
				if(hubPresent) {
					String currentDeviceName = galleryview_device_name.getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						if(galleryview_stronghold_icon.isDisplayed()) {
							gdoLockPresent = true;
							break;
						}
					}
				} else {
					utilityIOS.swipe(galleryview_hubname_label, "left");
				}
			}
			return gdoLockPresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return gdoLockPresent;
		}
	}
	
	/**
	 * Description: To find out the expected device name with hub exist in the Devices page
	 * @param deviceName
	 * @return deviceNamePresent
	 */
	 public boolean verifyDeviceNamePresentWithHub(String deviceName,String hubName) { 
		try {
			boolean deviceVisible;
			boolean topitem;
			do {
				topitem = device_name_list.get(0).isDisplayed();
				utilityIOS.scroll("up");
			} while (!topitem);
			int noOfDevices = device_name_list.size();
			for (int counter = 0; counter < noOfDevices; counter++) {
				String currentDeviceName = device_name_list.get(counter).getText();
				String currentHubName = hub_name_list.get(counter).getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName) && (currentHubName.toUpperCase()).contains(hubName.toUpperCase())) {
					if(device_name_list.get(counter).isDisplayed()) {
						deviceNamePresent = true;
						deviceIndex = counter;
						extentTestIOS.log(Status.INFO, "device found , device name is " +currentDeviceName);
						break;
					}  else {
						do {
							utilityIOS.swipe(devices_list,"up");
							deviceVisible = device_time_list.get(counter).isDisplayed();
							try {
								if((deviceVisible) && (!device_name_list.get(counter).isDisplayed())) {
									utilityIOS.scroll("up");
								}
							} catch (Exception e) {

							}
							extentTestIOS.log(Status.INFO, "device found , device name is " +currentDeviceName);
						} while (!deviceVisible);
						if(deviceVisible) {
							deviceNamePresent = true;
							deviceIndex = counter;
							break;
						} else {
							break;
						}
					}
				} 
			}
			return deviceNamePresent;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return deviceNamePresent;
		}
	}
	 
		/**
		 * Description: To click on the error tab
		 * @return Page object
		 */
		public DevicesPageIOS tapOnMonitorOnly(String deviceName) { 
			try {
					String currentDeviceName = device_name_list.get(deviceIndex).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						if(!error_label_list.get(deviceIndex).isDisplayed()) {
							utilityIOS.scroll("up");
						}
						utilityIOS.clickIOS(error_label_list.get(deviceIndex));  
					}

			} catch (Exception e) {
				utilityIOS.catchExceptions(iOSDriver, e);
			}
			return this;
		}
	
		/**
		 * Description: To find out the error on the Monitor Door Only error of a device
		 * @param deviceName
		 * @param errorType
		 * @param errorMessage
		 */
		public boolean verifyMonitorOnlyErrorOnDevice(String deviceName,String errorMessage) {
			boolean errorPresent = false;
			boolean popupShown =false;
			try {
					String currentDeviceName = device_name_list.get(deviceIndex).getText();
					if (currentDeviceName.equalsIgnoreCase(deviceName)) {
						utilityIOS.clickIOS(device_name_list.get(deviceIndex));
						try {
							if (iOSDriver.switchTo().alert().getText().contains(errorMessage)) {
								extentTestIOS.log(Status.INFO, " Error PopUp message text is: " +iOSDriver.switchTo().alert().getText());
								iOSDriver.switchTo().alert().accept();
								popupShown = true;
							}
						} catch (Exception e) {
						}
						String deviceError = error_label_list.get(deviceIndex).getText();
						if(deviceError.equalsIgnoreCase("Monitor Door Only") && popupShown) {
							errorPresent = true;
						}
					}
				return errorPresent;
			} catch (Exception e) {
				utilityIOS.catchExceptions(iOSDriver, e);
				return errorPresent;
			}
		}
		
		/**
		 * Description: To verify Success text after adding EPD
		 *
		 */
		public boolean verifySuccessText() { 
			boolean successTextPresent = false;
			try {
				utilityIOS.waitForElementIOS(success_text, "visibility");
				if(success_text.getText().equalsIgnoreCase("Success!"))
					extentTestIOS.log(Status.INFO, "Success text is displayed");
					successTextPresent = true; 
			} catch (Exception e) {
				utilityIOS.catchExceptions(iOSDriver, e);
			}
			return successTextPresent;
		}
}
