package com.liftmaster.myq.ios.pages;

import java.util.List;
import org.openqa.selenium.support.PageFactory;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class ManageUsersPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	//Page factory for Manage Users page IOS
	@iOSFindBy(accessibility = "Add")	
	public IOSElement add_users_btn;

	@iOSFindBy(accessibility = "header_label")	
	public IOSElement no_users_text;

	@iOSFindBy(accessibility = "Cancel")	
	public IOSElement cancel_btn;

	@iOSFindBy(accessibility = "Send")	
	public IOSElement send_btn;

	@iOSFindBy(accessibility = "user_email_text_field")	
	public IOSElement enter_email_textbox;

	@iOSFindBy(accessibility = "re_enter_user_email_text_field")	
	public IOSElement reenter_email_textbox;

	@iOSFindBy(accessibility = "access_level_label")	
	public IOSElement select_user_type_dropdown;

	@iOSFindBy(accessibility = "drop_down_arrow_image_view")	
	public IOSElement close_user_type_dropdown;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeOther/XCUIElementTypeStaticText")
	public IOSElement user_selected_text;

	@iOSFindBy(xpath = "//XCUIElementTypePickerWheel")
	public IOSElement user_roles_options;

	@iOSFindBy(accessibility = "Okay")	
	public IOSElement okay_btn;

	@iOSFindBy(accessibility = "Resend Invitation")	
	public IOSElement resend_invitation_btn;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	@iOSFindBy(accessibility = "Invitation Sent")
	public IOSElement invitation_sent_screen;

	@iOSFindBy(accessibility = "topbar icon trash")
	public IOSElement delete_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTable")
	public IOSElement users_added_invited_list;

	@iOSFindBy(accessibility = "role_or_expires_label")
	public List<IOSElement> added_user_role_list;

	@iOSFindBy(accessibility = "user_name_label")
	public List<IOSElement> invited_user_list;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton[1]")
	public IOSElement back_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeOther[3]/XCUIElementTypeButton")
	public IOSElement yes_btn;

	@iOSFindBy(accessibility = "role_or_expires_label")
	public List<IOSElement> expiry_status_list;

	/**
	 * Description:This is the Initializer method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public ManageUsersPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description: To tap on the Add button on the top right side on the Manage Users page
	 * @return Page object
	 */
	public ManageUsersPageIOS tapAddBtn() { 
		try {
			utilityIOS.clickIOS(add_users_btn);	 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: validate no users added text
	 * @param username
	 * @return Page object
	 */
	public boolean validateNoAddedUser() { 
		try {
			if (no_users_text.isEnabled()) {
				return true;
			}
		} catch (Exception e) {
			//just to catch the exception and continue with execution
			return false;
		}
		return false;
	}

	/**
	 * Description: To tap on the cancel button on the Add user screen
	 * @return Page object
	 */
	public ManageUsersPageIOS tapCancelBtn() { 
		try {
			utilityIOS.clickIOS(cancel_btn);
			utilityIOS.waitForElementIOS(add_users_btn, "clickable");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description:  To tap on the send button on the Add user screen
	 * @return Page object
	 */
	public ManageUsersPageIOS tapSendBtn() { 
		try {
			utilityIOS.clickIOS(send_btn);
			try {
				if (activity_indicator.isDisplayed()) {
					utilityIOS.waitForElementIOS(invitation_sent_screen, "visibility");
				}
			} catch (Exception e) {
				//just to catch exception and proceed further
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}


	/**
	 * Description:  To check if any error popup is present
	 * @return boolean
	 */
	public boolean validateErrorPopup() {
		boolean errorPresent = false;
		try {
			iOSDriver.switchTo().alert();
			errorPresent = true;
			if (!errorPresent) {
				extentTestIOS.log(Status.INFO,"popup present as " +iOSDriver.switchTo().alert().getText());
				iOSDriver.switchTo().alert().accept();
			}
			return errorPresent;
		}catch (Exception e) {
			return errorPresent;
			//just to catch exception and proceed further
		}
	}

	/**
	 * Description To get the Error message when popup is present and accept the message
	 * @return alertMessage
	 */
	public String getErrorMessage() {
		String errorMessage = null;
		errorMessage = iOSDriver.switchTo().alert().getText();
		try {
			extentTestIOS.log(Status.INFO,"Popup Text is : " +errorMessage);	
			iOSDriver.switchTo().alert().accept();
			return errorMessage;
		}catch (Exception e) {
			return errorMessage;
		}	 
	}

	/**
	 * Description to tap on the back button to navigate to previous screen
	 */
	public ManageUsersPageIOS tapBackBtn() {
		try {
			utilityIOS.clickIOS(back_btn);
			try {
				iOSDriver.switchTo().alert().accept();
				utilityIOS.waitForElementIOS(add_users_btn, "clickables");
			} catch (Exception e) {
				utilityIOS.waitForElementIOS(add_users_btn, "clickables");
				//just to confirm the popup and proceed further
			}	 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To enter the email in the User's Email Text box on the Add user screen
	 * @param email
	 * @return Page object
	 */
	public ManageUsersPageIOS enterUserEmail(String email) { 
		try {
			utilityIOS.clearIOS(enter_email_textbox);
			utilityIOS.enterTextIOS(enter_email_textbox, email);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To enter the email in the Re-Enter User's Email Text box on the Add user screen
	 * @param email
	 * @return Page object
	 */
	public ManageUsersPageIOS reEnterUserEmail(String email) { 
		try {
			utilityIOS.clearIOS(reenter_email_textbox);
			utilityIOS.enterTextIOS(reenter_email_textbox, email);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the DropDown Arrow for opening User role on the Add user screen
	 * @return Page object
	 */
	public ManageUsersPageIOS openRolesDropDown() { 
		try {
			utilityIOS.clickIOS(select_user_type_dropdown);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}


	/**
	 * Description: To tap on the Collapse drop down Arrow for Closing User role on the Add user screen
	 * @return Page object
	 */
	public ManageUsersPageIOS closeRolesDropDown() { 
		try {
			utilityIOS.clickIOS(close_user_type_dropdown);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the confirmation popup 
	 * @return Page object
	 */
	public ManageUsersPageIOS tapConfirmationPopup() { 
		try {

			iOSDriver.switchTo().alert().accept();
			utilityIOS.waitForElementIOS(add_users_btn, "clickables");
			return this;
		} catch (Exception e) {
			//just to confirm the popup and proceed further
			return this;
		}	 
	}

	/**
	 * Description: To select role from the picker wheel having options Admin, Family and Guest
	 * @Param role
	 * @return Page object
	 */
	public ManageUsersPageIOS selectRole(String role) { 
		try {
			//Here the Family is sent first as it is the middle of the picker wheel and the we can navigate up or down to Admin or Guest
			utilityIOS.enterTextIOS(user_roles_options, "Family");
			utilityIOS.enterTextIOS(user_roles_options, role);
			utilityIOS.clickIOS(close_user_type_dropdown);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the Okay button on the Invitation sent screen
	 * @Param role
	 * @return Page object
	 */
	public ManageUsersPageIOS tapOkayBtn() { 
		try {
			utilityIOS.clickIOS(okay_btn);
			try {
				if (activity_indicator.isDisplayed()) {
					utilityIOS.waitForElementIOS(add_users_btn, "clickable");
				}
			} catch (Exception e) {
				//just to catch exception and proceed further
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To verify if the invited user mail is matching the passed email
	 * @param email
	 * @return invitedEmail
	 */
	public boolean verifyInvitedUsersEmail(String email) { 
		boolean invitedEmail = false;
		List<MobileElement> manageUserChildElements = users_added_invited_list.findElementsByXPath(".//*");
		try {
			int noOfElementsInHierarchy = manageUserChildElements.size();
			//Finding the heading Invitations
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Invitations")) {
						int noOfInvitations = invited_user_list.size();
						//Finding the expected Email
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							if (invited_user_list.get(jIndex).getText().trim().equalsIgnoreCase(email)) {
								invitedEmail = true;
								break;
							}
						}
					}
				}
				if (invitedEmail) {
					break;
				}
			}
			return invitedEmail;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return invitedEmail;
		}	 
	}

	/**
	 * Description: To verify if the user Added is matching the passed username and role
	 * @param userName
	 * @return addedUser
	 */
	public boolean verifyAddedUser(String userName, String role) { 
		boolean addedUser = false;
		List<MobileElement> manageUserChildElements = users_added_invited_list.findElementsByXPath(".//*");
		try {
			int noOfElementsInHierarchy = manageUserChildElements.size();
			//Finding the Users heading
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("USERS")) {
						int noOfInvitations = invited_user_list.size();
						//Finding the expected username and role
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							String currentUserName = invited_user_list.get(jIndex).getText();
							String CurrentRole = added_user_role_list.get(jIndex).getText();
							if ((currentUserName.trim().equalsIgnoreCase(userName.trim())) && 
									(CurrentRole.trim().equalsIgnoreCase(role.trim()))) {
								addedUser = true;
								break;
							}
						}
					}
				}
				if (addedUser) {
					break;
				}
			}
			return addedUser;
		} catch (Exception e) {
			//utilityIOS.catchExceptions(iOSDriver, e);
			return addedUser;
		}	 
	}

	/**
	 * Description: To verify if the invited user mail is matching the passed email
	 * @param email
	 * @return invitedEmail
	 */
	public ManageUsersPageIOS tapOnInvitation(String email) { 
		boolean invitedEmail = false;
		List<MobileElement> manageUserChildElements = users_added_invited_list.findElementsByXPath(".//*");
		try {
			int noOfElementsInHierarchy = manageUserChildElements.size();
			//Finding the heading of invitation
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Invitations")) {
						int noOfInvitations = invited_user_list.size();
						//Finding and taping on the expected invitation by matching the email
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							if (invited_user_list.get(jIndex).getText().trim().equalsIgnoreCase(email)) {
								utilityIOS.clickIOS(invited_user_list.get(jIndex));
								invitedEmail = true;
								break;
							}
						}
					}
				}
				if (invitedEmail) {
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the user matching the passed user name and role
	 * @param email
	 * @return page object
	 */
	public ManageUsersPageIOS tapOnUser(String userName, String role) { 
		boolean addedUser= false;
		List<MobileElement> manageUserChildElements = users_added_invited_list.findElementsByXPath(".//*");
		try {
			int noOfElementsInHierarchy = manageUserChildElements.size();
			//Finding the heading of Users
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("USERS")) {
						int noOfInvitations = invited_user_list.size();
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							//Finding the username alongwith role and then clicking on it
							String currentUserName = invited_user_list.get(jIndex).getText();
							String CurrentRole = added_user_role_list.get(jIndex).getText();
							if (currentUserName.trim().equalsIgnoreCase(userName.trim()) 
									&& CurrentRole.trim().equalsIgnoreCase(role.trim())) {
								utilityIOS.clickIOS(invited_user_list.get(jIndex));
								addedUser = true;
								break;
							}
						}
					}
				}
				if (addedUser) {
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description: To verify if the invited user mail is matching the passed email
	 * @param email
	 * @return invitedEmail
	 */
	public ManageUsersPageIOS reSendInvitation(String email) { 
		boolean resendInvite = false;
		boolean resendBtnPresent = false;
		List<MobileElement> manageUserChildElements = users_added_invited_list.findElementsByXPath(".//*");
		try {
			int noOfElementsInHierarchy = manageUserChildElements.size();
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				//Finding the Invitations heading
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Invitations")) {
						//finding the number on invitations under the 'Invitation' heading and looping through them
						int noOfInvitations = invited_user_list.size();
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							if (invited_user_list.get(jIndex).getText().trim().equalsIgnoreCase(email)) {
								for (int kIndex = 0; kIndex < 5; kIndex ++) { 	//here the 	swipe retry mechanism of five times is done as swipe method is not reliable from Appium
									utilityIOS.swipeLeft(invited_user_list.get(jIndex));
									try {
										if (resend_invitation_btn.isDisplayed() || resend_invitation_btn.isEnabled()) {
											resendBtnPresent =true;
										}
									} catch (Exception e) {
									}
									if (resendBtnPresent) {
										utilityIOS.clickIOS(resend_invitation_btn);
										try {
											if (activity_indicator.isDisplayed()) {
												utilityIOS.waitForElementIOS(add_users_btn, "clickable");	
											}
										} catch (Exception e) {
											//just wait till the activity indicator disappears and proceed further	
										}
										resendInvite = true;
										break;	 
									} else {
										utilityIOS.clickIOS(back_btn);
										utilityIOS.waitForElementIOS(add_users_btn, "clickable");
									}
								}
							}
							if (resendInvite) {
								break;
							}
						}
					}
				}
				if (resendInvite) {
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	 
	}

	/**
	 * Description: To tap on the delete button on the invitation and user screen
	 */
	public ManageUsersPageIOS tapDeleteBtn() { 
		utilityIOS.clickIOS(delete_btn);
		try {
			iOSDriver.switchTo().alert().accept();
			if (activity_indicator.isDisplayed()) {
				utilityIOS.waitForElementIOS(add_users_btn, "clickable");
			}
			return this;
		} catch (Exception e) {
			return this;
		}

	}

	/**
	 * Description: To verify the invitation resent status
	 */
	public boolean verifyInvitationResentStatus(String invitedEmail) { 
		boolean reSentInvitationStatus = false;
		List<MobileElement> manageUserChildElements = users_added_invited_list.findElementsByXPath(".//*");
		String invitationExpiryStatus;
		String todaysDate;
		try {
			int noOfElementsInHierarchy = manageUserChildElements.size();
			for (int iIndex = 0; iIndex < noOfElementsInHierarchy - 1; iIndex++) {
				//Finding the Invitations heading
				String elementType = manageUserChildElements.get(iIndex).getAttribute("type");
				if (elementType.equalsIgnoreCase("XCUIElementTypeOther")) {
					if (manageUserChildElements.get(iIndex+1).getText().equalsIgnoreCase("Invitations")) {
						//finding the number on invitations under the 'Invitation' heading and looping through them
						int noOfInvitations = invited_user_list.size();
						for (int jIndex = 0; jIndex < noOfInvitations; jIndex++) {
							//Validating the date
							if (invited_user_list.get(jIndex).getText().trim().equalsIgnoreCase(invitedEmail)) {
								invitationExpiryStatus = expiry_status_list.get(jIndex).getText();
								String currentSysDate = utilityIOS.getCurrentDayInFormat();
								todaysDate = currentSysDate.split(",")[0].split("\\s")[1];
								if(Integer.valueOf(todaysDate)<10) {
									todaysDate =currentSysDate.split(",")[0].split("\\s")[1].substring(1);
								}
								currentSysDate = currentSysDate.split(",")[0].split("\\s")[0] + " " + todaysDate + ", " + (currentSysDate.split(",")[1]);
								if (invitationExpiryStatus.equalsIgnoreCase("Expires on "+currentSysDate)) {
									reSentInvitationStatus = true;
									break;
								}
							}
						}

					}
				}
				if (reSentInvitationStatus) {
					break;
				}
			}
			return reSentInvitationStatus;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return reSentInvitationStatus;
		}  
	}
}
