package com.liftmaster.myq.ios.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;

public class DeviceManagementIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public UtilityIOS utilityIOS = null;
	public ExtentTest extentTestIOS;

	//Page factory for Device Management Page IOS
	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeTable")	
	public IOSElement hubs_list;
	
	@iOSFindBy(accessibility = "main_label")	
	public List<IOSElement> gateway_list;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeTextField")	
	public IOSElement hubname_textbox;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeTable")	
	public IOSElement endpoint_devices_list;

	@iOSFindBy(xpath = "//XCUIElementTypeOther/XCUIElementTypeTextField")	
	public IOSElement endpointname_textbox;

	@iOSFindBy(accessibility = "Clear text")	
	public IOSElement clear_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")	
	public IOSElement back_btn;

	@iOSFindBy(accessibility = "HamburgerMenu")	
	public  IOSElement hamburger_menu_btn;

	@iOSFindBy(accessibility = "Delete")
	public IOSElement delete_btn;

	@iOSFindBy(accessibility = "Save")
	public IOSElement save_btn;

	/**
	 * Description Driver Initialize
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public DeviceManagementIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);	
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 * Description To get the list of places(hubs) available
	 * @param placeName
	 * @return listPlaceNames
	 */
	public ArrayList<String> getPlacesNames(String placeName) {
		ArrayList<String> listPlaceNames = new ArrayList<String>();

		try {
			int numbHubs = hubs_list.findElementsByXPath("/XCUIElementTypeCell").size();				
			for (int counter = 1; counter <= numbHubs; counter++) {
				placeName = hubs_list.findElementByXPath("//XCUIElementTypeCell[" + counter + "]/XCUIElementTypeStaticText").getText();
				listPlaceNames.add(placeName);
			}
			return listPlaceNames;	
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return listPlaceNames;
		}
	}

	/**
	 * Description To select the Passed Place(hub)
	 * @param placeName
	 * @return Page object
	 */
	public DeviceManagementIOS selectPlaceName(String placeName) {
		try {
			int noOfHubs = hubs_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int counter = 1; counter <= noOfHubs; counter++) {
				String currentPlaceName = hubs_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]/XCUIElementTypeStaticText").getText();
				if (currentPlaceName.equalsIgnoreCase(placeName)) {
					utilityIOS.clickIOS(hubs_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]"));
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return this;
		}
	}

	/**
	 * Description To set place name
	 * @param placenameChanged
	 * @throws InterruptedException
	 * @return Page object
	 */
	public DeviceManagementIOS setPlaceName(String placenameChanged) throws InterruptedException {
		try {
			utilityIOS.clickIOS(hubname_textbox);
			utilityIOS.clickIOS(clear_btn);
			utilityIOS.enterTextIOS(hubname_textbox, placenameChanged);
			utilityIOS.clickIOS(back_btn);
			utilityIOS.waitForElementIOS(hamburger_menu_btn, "clickable");
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return this;
		}
	}

	/**
	 * Description To Select device based on name
	 * @param deviceName
	 * @return Page object
	 */
	public DeviceManagementIOS selectEndPointDevice(String deviceName) {
		try {
			int noOfDevices = endpoint_devices_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int counter = 1; counter <= noOfDevices; counter++) {
				String currentDeviceName = endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]/XCUIElementTypeStaticText").getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					utilityIOS.clickIOS(endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]"));
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return this;
		}
	}

	/**
	 * Description To set the Name of the device
	 * @param deviceNameChanged
	 * @throws InterruptedException
	 * @return Page object
	 */
	public DeviceManagementIOS setDeviceName(String deviceNameChanged) throws InterruptedException {
		try {
			utilityIOS.clickIOS(endpointname_textbox);
			utilityIOS.clickIOS(clear_btn);
			utilityIOS.enterTextIOS(endpointname_textbox, deviceNameChanged);
			utilityIOS.clickIOS(save_btn);
			utilityIOS.clickIOS(back_btn);
			utilityIOS.waitForElementIOS(back_btn, "clickable");
			utilityIOS.clickIOS(back_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return this;
		}
	}

	/**
	 * Description clicking on back button
	 * @return Page object
	 */
	public DeviceManagementIOS tapBackBtn() {
		try {
			utilityIOS.clickIOS(back_btn);
			Thread.sleep(5000);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}	
	}

	/**
	 * Description : This function would delete the specific Device
	 * @param deviceName
	 * @return Page object
	 */
	public DeviceManagementIOS deleteDevice(String deviceName) {
		boolean deleteBtnPresent = false;
		boolean deviceDeleted = false;
		try {
			int noOfDevices = endpoint_devices_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int counter = 1; counter <= noOfDevices; counter++) {
				String currentDeviceName = endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]/XCUIElementTypeStaticText").getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					for (int kIndex = 0; kIndex < 5; kIndex++) { 
						utilityIOS.swipeLeft(endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]"));
						try {
							if (delete_btn.isDisplayed() || delete_btn.isEnabled()) {
								deleteBtnPresent =true;
							}
						} catch (Exception e) {
						}
						if (deleteBtnPresent) {
							utilityIOS.clickIOS(delete_btn);
							Thread.sleep(3000);
							deviceDeleted = true;
							break;
						} else {
							utilityIOS.clickIOS(back_btn);
							utilityIOS.waitForElementIOS(hubname_textbox, "visibility");
						}
					}
				}
				if (deviceDeleted) {
					iOSDriver.switchTo().alert().accept();	
					utilityIOS.waitForElementIOS(hubname_textbox, "visibility");
					Thread.sleep(3000);
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return this;
		}
	}


	/**
	 * Description : This function would delete the hub
	 * @param hubName
	 * @return Page object
	 */
	public DeviceManagementIOS deleteHub(String hubName) throws InterruptedException{
		boolean deleteBtnPresent = false;
		boolean hubDeleted = false;
		
		try {
				int noOfDevices = gateway_list.size();
				for (int counter = 0; counter < noOfDevices; counter++) {
					String currentDeviceName = gateway_list.get(counter).getText();
					if (currentDeviceName.equalsIgnoreCase(hubName)) {
						for (int kIndex = 0; kIndex < 5; kIndex++) { 
							utilityIOS.swipeLeft(gateway_list.get(counter));
							try {
								if (delete_btn.isDisplayed() || delete_btn.isEnabled()) {
									deleteBtnPresent =true;
								}
							} catch (Exception e) {
							}
							if (deleteBtnPresent) {
								utilityIOS.clickIOS(delete_btn);
								hubDeleted = true;
								break;
							} else {
								utilityIOS.clickIOS(back_btn);
								utilityIOS.waitForElementIOS(hamburger_menu_btn, "visibility");
							}
						}
					}
					if (hubDeleted) {
						iOSDriver.switchTo().alert().accept();	
						utilityIOS.waitForElementIOS(hamburger_menu_btn, "visibility");
						break;
					}
				}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To verify if the Endpoint device is available
	 * @param deviceName
	 * @return boolean
	 */
	public boolean verifyEndPointDevice(String deviceName) {
		boolean deviceAvailable = false;
		try {
			int noOfDevices = endpoint_devices_list.findElementsByXPath("XCUIElementTypeCell").size();
			for (int counter = 1; counter <= noOfDevices; counter++) {
				String currentDeviceName = endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]/XCUIElementTypeStaticText").getText();
				if (currentDeviceName.equalsIgnoreCase(deviceName)) {
					deviceAvailable = true;
					break;
				}
			}
			return deviceAvailable;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return deviceAvailable;
		}
	}

	/**
	 * Description To verify if the hub is shown
	 * @param placeName
	 * @return Page object
	 */
	public boolean verifyHubAvailable(String placeName) {
		boolean hubAvailable = false;
		try{
			int noOfHubs = gateway_list.size();
			for (int counter = 0; counter < noOfHubs; counter++) {
				String currentPlaceName = gateway_list.get(counter).getText();
				if (currentPlaceName.equalsIgnoreCase(placeName)) {
					hubAvailable = true;
					break;
				}
			}
			return hubAvailable;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return hubAvailable;
		}
	}

	/**
	 * Description : This function would delete the specific GDO Device from WGDO to validate error popup message
	 * @param gdoName
	 * @return Page object
	 */
	public DeviceManagementIOS deleteGDODeviceForError(String gdoName) {
		boolean deleteBtnPresent = false;
		boolean deviceDeleted = false;

		try {
			int noOfDevices = endpoint_devices_list.findElementsByXPath("XCUIElementTypeCell").size();

			for (int counter = 1; counter <= noOfDevices; counter++) {
				String currentDeviceName = endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]/XCUIElementTypeStaticText").getText();

				if (currentDeviceName.equalsIgnoreCase(gdoName)) {
					for (int kIndex = 0; kIndex < 5; kIndex++) { 
						utilityIOS.swipeLeft(endpoint_devices_list.findElementByXPath("XCUIElementTypeCell[" + counter + "]"));
						try {
							if (delete_btn.isDisplayed()) {
								deleteBtnPresent =true;
							}
						} catch (Exception e) {
						}

						if (deleteBtnPresent) {
							utilityIOS.clickIOS(delete_btn);
							deviceDeleted = true;
							break;
						} else {
							utilityIOS.clickIOS(back_btn);
							utilityIOS.waitForElementIOS(hubname_textbox, "visibility");
						}
					}
				}
				if (deviceDeleted) {
					iOSDriver.switchTo().alert().accept();	
					break;
				}
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);	
			return this;
		}
	}

	/**
	 * Description : This function would check whether the notification was received as soon as the GDO was tried to delete from the WGDO
	 * @return boolean
	 */
	public boolean verifyAlertForGDODelete() throws InterruptedException {
		try {
			String errorMessage ="This garage door is permanently linked to its hub and cannot be removed. To remove garage door, go to Manage Places and remove the hub. (601)";
			String alertText;
			//			This below code is kept commented intentionally till a fix is provided by Appium. The version which have issue is 1.7.2
			//			WebDriverWait wait = new WebDriverWait(iOSDriver, 20);
			//			wait.until(ExpectedConditions.alertIsPresent());

			//Custom wait logic till alert is present
			for (int i = 0; i <= 15; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			alertText =	iOSDriver.switchTo().alert().getText();
			iOSDriver.switchTo().alert().accept();
			if(alertText.contains(errorMessage)) {
				extentTestIOS.log(Status.INFO, "MTA-422 Error '" + alertText + "' is displayed successfully");
				return true;
			} else {
				extentTestIOS.log(Status.INFO, "MTA-422 Error '" + alertText + "' is NOT displayed");
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			utilityIOS.captureScreenshot(iOSDriver);
			return false;
		}
	}

	/**
	 * Description : This function would check whether any devices are present in the hub
	 * @return boolean
	 */
	public boolean verifyDeviceAvailableOnHub() throws InterruptedException{
		try {
			int noOfDevices = endpoint_devices_list.findElementsByXPath("XCUIElementTypeCell").size();
			if(noOfDevices>=2) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Description : This function would check whether the notification was received when clicked on next button to add the 3rd VGDO
	 * @return boolean
	 */
	public boolean verifyAlertForAddingMoreThan2VGDO() throws InterruptedException {
		try {
			String errorMessage ="You have reached the maximum number of doors. Please add an additional hub or remove an existing door.";
			String alertText;
			//			This below code is kept commented intentionally till a fix is provided by Appium. The version which have issue is 1.7.2
			//			WebDriverWait wait = new WebDriverWait(iOSDriver, 20);
			//			wait.until(ExpectedConditions.alertIsPresent());

			//Custom wait logic till alert is present
			for (int i = 0; i <= 15; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			alertText =	iOSDriver.switchTo().alert().getText();
			iOSDriver.switchTo().alert().accept();
			if(alertText.contains(errorMessage)) {
				return true;
			} else {
				utilityIOS.captureScreenshot(iOSDriver);
				return false;
			}
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			utilityIOS.captureScreenshot(iOSDriver);
			return false;
		}
	}
}