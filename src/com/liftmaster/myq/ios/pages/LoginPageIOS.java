package com.liftmaster.myq.ios.pages;

import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import java.util.List;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.testmethods.IOS;
import com.liftmaster.myq.utils.UtilityIOS;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.pagefactory.iOSFindBy;

public class LoginPageIOS {

	public IOSDriver<IOSElement> iOSDriver;
	public ExtentTest extentTestIOS;
	public UtilityIOS utilityIOS;

	//Page factory for Login Page IOS
	@iOSFindBy(accessibility = "email_text_field")	
	public IOSElement account_email_textbox;

	@iOSFindBy(accessibility = "password_text_field")
	public IOSElement password_textbox;

	@iOSFindBy(accessibility = "login_button")
	public IOSElement login_btn;

	@iOSFindBy(accessibility = "Configure Testing Settings")
	public IOSElement configure_settings_link;

	@iOSFindBy(accessibility = "sign_up_button")
	public IOSElement signup_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[1]")
	public IOSElement env_type_select;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[2]")
	public IOSElement user_type_select;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[3]")
	public IOSElement account_select;

	@iOSFindBy(accessibility = "Development")
	public IOSElement dev_env_select;

	@iOSFindBy(accessibility = "Pre-Production")
	public IOSElement preprod_env_select;

	@iOSFindBy(accessibility = "Production")
	public IOSElement prod_env_select;

	@iOSFindBy(accessibility = "Dev Azure")
	public IOSElement azure_env_select;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[1]")
	public IOSElement user_v4;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[2]")
	public IOSElement user_v5;
	
	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell[3]")
	public IOSElement user_v5_only;

	@iOSFindBy(xpath = "//XCUIElementTypeTable/XCUIElementTypeCell")
	public IOSElement acccount1;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement liftmaster_logo;

	@iOSFindBy(accessibility = "brand_image_view")
	public IOSElement chamberlain_logo;

	@iOSFindBy(accessibility = "Done")
	public IOSElement done_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeNavigationBar/XCUIElementTypeButton")
	public IOSElement back_btn;

	@iOSFindBy(accessibility = "Devices")
	public IOSElement home_screen;

	@iOSFindBy(accessibility = "SVProgressHUD")
	public IOSElement activity_indicator;

	@iOSFindBy(accessibility = "Please enter your myQ passcode to login")
	public IOSElement passcode_login_msg;
	
	@iOSFindBy(accessibility = "Skip")	
	public IOSElement v5_skip_image_btn;

	@iOSFindBy(className = "XCUIElementTypeSecureTextField")
	public List<IOSElement> passcode_fields;
	
	@iOSFindBy(accessibility = "remember_login_button")
	public IOSElement remember_me_checkbox;

	@iOSFindBy(accessibility = "forgot_password_button")
	public IOSElement forgot_password_btn;

	@iOSFindBy(accessibility = "Reset")
	public IOSElement reset_password_btn;

	@iOSFindBy(accessibility = "Yes")
	public IOSElement yes_btn;

	@iOSFindBy(xpath = "//XCUIElementTypeCollectionView//./XCUIElementTypeTextField")
	public IOSElement forgot_password_email_textbox;
	
	@iOSFindBy(accessibility = "v5_onboarding_image")
	public IOSElement multiuser_image;

	@iOSFindBy(accessibility = "Remind me later")
	public IOSElement remind_me_later_btn;
	
	@iOSFindBy(accessibility = "copyright_label")
	public IOSElement copyright_text;


	/**
	 * Description:This is the Initializer method
	 * @param iOSDriver
	 * @param extentTestIOS
	 */
	public LoginPageIOS(IOSDriver<IOSElement> iOSDriver,ExtentTest extentTestIOS) {
		this.iOSDriver = iOSDriver; 
		PageFactory.initElements(new AppiumFieldDecorator(iOSDriver), this);			
		this.extentTestIOS = extentTestIOS;
		utilityIOS = new UtilityIOS(iOSDriver, extentTestIOS);
	}

	/**
	 *	Description: To enter Email in the Email Text field
	 * @param userName
	 * @return Page object
	 */
	public LoginPageIOS enterAccountEmailId(String userName) { 
		try {
			utilityIOS.clearIOS(account_email_textbox);
			utilityIOS.enterTextIOS(account_email_textbox,userName);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To enter Password in the Password Text field
	 * @param password
	 * @return Page object
	 */
	public LoginPageIOS enterAccountPassword(String password) {
		try {
			utilityIOS.clearIOS(password_textbox);
			utilityIOS.enterTextIOS(password_textbox, password);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To click the login Button
	 * @return Page object
	 */
	public LoginPageIOS clickLoginButton(){
		try {
			utilityIOS.clickIOS(login_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To login to App by passing username and password
	 * @param userName
	 * @param password
	 * @return Page object
	 */
	public LoginPageIOS login(String userName, String password ) {
		try {
			utilityIOS.clearIOS(account_email_textbox);
			utilityIOS.enterTextIOS(account_email_textbox, userName);
			utilityIOS.clearIOS(password_textbox);
			utilityIOS.enterTextIOS(password_textbox, password);
			utilityIOS.clickIOS(login_btn);
			try {
				WebDriverWait wait = new WebDriverWait(iOSDriver, 20);
				wait.until(ExpectedConditions.alertIsPresent());
				iOSDriver.switchTo().alert().dismiss();
			} catch  (Exception e) {
				//check if the login alert  for touch ID comes
			}
			try {
				WebDriverWait wait = new WebDriverWait(iOSDriver, 10);
				wait.until(ExpectedConditions.visibilityOf(multiuser_image));
				utilityIOS.clickIOS(remind_me_later_btn);
			} catch (Exception e) {
			}
			try{
				if(v5_skip_image_btn.isEnabled()) {
					utilityIOS.clickIOS(v5_skip_image_btn);
				}
			} catch (Exception e) {
				//To tap on the skip button if the image is shown otherwise continuing with execution	
			}
			
			utilityIOS.waitForElementIOS(home_screen, "visibility");
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To select user type V4 or V5
	 * @param userType
	 * @return Page object
	 */
	public LoginPageIOS selectUserType(String userType) {
		try {
			utilityIOS.clickIOS(configure_settings_link);
			utilityIOS.waitForElementIOS(done_btn, "visibility");
			utilityIOS.clickIOS(user_type_select);
			utilityIOS.waitForElementIOS(back_btn, "clickable");
			if (userType.equalsIgnoreCase("Single")) {
				utilityIOS.clickIOS(user_v5);
				utilityIOS.clickIOS(done_btn);
				extentTestIOS.log(Status.PASS, "Single user Selected");
			} else if (userType.equalsIgnoreCase("Multi User")) {
				utilityIOS.clickIOS(user_v5);
				utilityIOS.clickIOS(done_btn);
				extentTestIOS.log(Status.PASS, "Multi user Selected");
			} else { 
				extentTestIOS.log(Status.FAIL, "Invalid user Selected "); 
				utilityIOS.captureScreenshot(iOSDriver);
			}
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To select the Environment Preprod(pp), Dev and Prod
	 * @return Page object
	 */
	public LoginPageIOS selectEnvironment(String env){
		try {
			 try {
				 iOSDriver.switchTo().alert().accept();
			 } catch  (Exception e) {
				 //to handle the popup for allowing notification
			 }
			utilityIOS.clickIOS(configure_settings_link);
			utilityIOS.waitForElementIOS(done_btn, "visibility");
			utilityIOS.clickIOS(env_type_select);
			utilityIOS.waitForElementIOS(back_btn, "clickable");
			if (env.equalsIgnoreCase("Prod")) {
				utilityIOS.clickIOS(prod_env_select);
			} else if (env.equalsIgnoreCase("Pre-Prod")) {
				utilityIOS.clickIOS(preprod_env_select);
			} else if (env.equalsIgnoreCase("Dev")) {
				utilityIOS.clickIOS(dev_env_select);
			}
			extentTestIOS.log(Status.INFO, "Environment is selected");
			utilityIOS.waitForElementIOS(done_btn, "visibility");
			utilityIOS.clickIOS(done_btn);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To verify if the login happened 
	 * @return boolean
	 */
	public boolean verifyLogin() {
		try {
			if (activity_indicator.isDisplayed()) {
				utilityIOS.waitForElementIOS(home_screen, "visibility");
			}
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Description To set the passcode
	 * @param passcode
	 * @return Page object
	 */
	public LoginPageIOS passcodeLogin(String passcode) {
		try {
			boolean liftmaster;
			if (IOS.brandName.equalsIgnoreCase("LiftMaster")) {
				liftmaster = true;
			} else {
				liftmaster = false;
			}
			if ((liftmaster ? liftmaster_logo:chamberlain_logo).isEnabled() && passcode_login_msg.isEnabled()) {
				int sizeOfPasscodeFields = passcode_fields.size();
				int sizeOfPasscode = passcode.length();
				int i;
				if (sizeOfPasscode==sizeOfPasscodeFields) {
					for(i=0; i < sizeOfPasscodeFields; i++) {
						utilityIOS.enterTextIOS(passcode_fields.get(i),String.valueOf(passcode.charAt(i)));
					}
				} else {
					extentTestIOS.log(Status.INFO, "Number of expected length for  passcode and the length passed is not matching"); 	
				}
			} 
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	
	/**
	 * Description: To tap on the forgot password button
	 * @return Page object
	 */
	public LoginPageIOS tapForgotPassword() {
		try {	
			utilityIOS.clickIOS(forgot_password_btn);
			return this;
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description: To enter email in the forgot password popup
	 * @return Page object
	 */
	public LoginPageIOS enterEmailForgotPassword(String emailID) {
		try {	
			utilityIOS.clearIOS(forgot_password_email_textbox);
			utilityIOS.enterTextIOS(forgot_password_email_textbox, "abc@xyz.com");
			utilityIOS.clickIOS(reset_password_btn);
			return this;
		} catch (Exception e){
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}

	/**
	 * Description To validate if the popup is present
	 * @return poupPresent
	 */
	public boolean validatePopupPresent() {
		boolean poupPresent =false;
		try{
		//	This below code is kept commented intentionally till a fix is provided by Appium. The version which have issue is 1.7.2
//			WebDriverWait wait = new WebDriverWait(iOSDriver, 15); 
//			wait.until(ExpectedConditions.alertIsPresent());
			
			//Custom wait logic till alert is present
			for (int i = 0; i <= 15; i++) {
				try {
					Thread.sleep(1000);
					iOSDriver.switchTo().alert();
					break;
				} catch (Exception e) {
				}
			}
			iOSDriver.switchTo().alert();
			poupPresent = true;
			return poupPresent;	
		} catch (Exception e) {
			//Here catch is not done through the re-usable exception handler, as the alert might be present or not present
			return poupPresent;
		}	 
	}

	/**
	 * Description To get the popup text
	 * @return popupText
	 */
	public String getPopupMessage() {
		String popupText = null;
		try {
			popupText = iOSDriver.switchTo().alert().getText();
			extentTestIOS.log(Status.INFO,"Alert Text is : " +popupText);	
			iOSDriver.switchTo().alert().accept();
			return popupText;
		}catch (Exception e) {
			return popupText;
		}	 
	}

	/**
	 * Description: To tap on the Yes Button on popup
	 * @return Page object
	 */
	public LoginPageIOS tapYesButton() {
		try {	
			iOSDriver.switchTo().alert().accept();
			WebDriverWait wait = new WebDriverWait(iOSDriver,5);
			wait.until(ExpectedConditions.alertIsPresent());
			return this;
		} catch (Exception e){
			return this;
		}
	}
	
	/**
	 * Description: To click on the remember me check box
	 * @return Page object
	 */
	public LoginPageIOS clickRememberMe(){
		try {
			utilityIOS.clickIOS(remember_me_checkbox);
			return this;
		} catch (Exception e) {
			utilityIOS.catchExceptions(iOSDriver, e);
			return this;
		}
	}
	
	/**
	 * Description To verify the login button is visible or not
	 * @return Page object
	 */
	public boolean verifyLoginButton() {
		try { 
			if(login_btn.isDisplayed()) {
				return true;	
			} else {
				return false;	
			}
		} catch (Exception e) {
			return false;
		}
	}
	
	/**
	 * Description To verify the copyright text on the login screen
	 * @return Page object
	 */
	public boolean verifyCopyRightText(String text) {
		try { 
			if(copyright_text.getAttribute("label").equalsIgnoreCase(text)) {
				return true;	
			} else {
				return false;	
			}
		} catch (Exception e) {
			return false;
		}
	}
	
}