package com.liftmaster.myq.utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.liftmaster.myq.utils.GlobalVariables;


public class WebServices {

	public HttpClient httpclient = null;
	GlobalVariables gbVariables = null;
	private ExtentTest extentTest;
	/**
	 *
	 */
	public WebServices(ExtentTest extentTest) {
		// TODO Auto-generated constructor stub
		gbVariables = new GlobalVariables();
		httpclient = HttpClientBuilder.create().build();
		this.extentTest = extentTest;
	}

	/**
	 * Description :- Create new Gateway using Service API
	 * @param gatewaySerialNumber
	 * @throws ClientProtocolException,IOException
	 * @throws IOException
	 */
	public WebServices createGateway(String gatewaySerialNumber) throws ClientProtocolException, IOException, ParseException{
		gbVariables.setEnv();
		String displayName = "MyQTest1";
		String deviceType = "ethernet_gateway";

		Map<String, Object> gatewayservicevals = new HashMap<String, Object>();
		gatewayservicevals.put("display_name", displayName);
		gatewayservicevals.put("device_type", deviceType);
		JSONObject gatewayjson = new JSONObject(gatewayservicevals);

		StringEntity gatewayserviceEntity = new StringEntity(gatewayjson.toString(), "UTF8");
		gatewayserviceEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPost createnewGatewayPost = new HttpPost(gbVariables.CreateGatewayServiceURL);
		extentTest.log(Status.INFO, "Authorization Code is = " + gbVariables.AuthorizationCode);
		createnewGatewayPost.setHeader("Authorization", gbVariables.AuthorizationCode);
		createnewGatewayPost.setHeader("Content-Type", "application/json");
		createnewGatewayPost.setEntity(gatewayserviceEntity);
		HttpResponse GateWayServiceResponse = null;
		GateWayServiceResponse = httpclient.execute(createnewGatewayPost);
		String WebserviceJsonEntity = EntityUtils.toString(GateWayServiceResponse.getEntity());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(WebserviceJsonEntity);
		gatewaySerialNumber = (String) jsonObject.get("serial_number");
		extentTest.log(Status.INFO, "Gateway serial number is = " + gatewaySerialNumber);
		return this;
	}	

	/**
	 * Description :- Send Gateway Online Service
	 * @param gatewaySerialNumber
	 * @throws ClientProtocolException,IOException
	 * @throws IOException
	 */
	public WebServices SendGatewayOnlineService(String gatewaySerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		String gateWayServiceURL = gbVariables.SendGatewayOnlineWebserviceURL + gatewaySerialNumber + "/action";
		String actionType = "send_online";
		Map<String, Object> gatewaySendOnlinevals = new HashMap<String, Object>();
		gatewaySendOnlinevals.put("action_type", actionType);
		JSONObject gatewayjson = new JSONObject(gatewaySendOnlinevals);

		StringEntity gateWaySendOnlineEntity = new StringEntity(gatewayjson.toString(), "UTF8");
		gateWaySendOnlineEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut gateWaySendOnlineReq = new HttpPut(gateWayServiceURL);
		gateWaySendOnlineReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		gateWaySendOnlineReq.setEntity(gateWaySendOnlineEntity);
		HttpResponse GateWaySendOnlineServiceResponse = null;
		GateWaySendOnlineServiceResponse = httpclient.execute(gateWaySendOnlineReq);
		extentTest.log(Status.INFO,"Status code for Gateway Send online = "
				+ GateWaySendOnlineServiceResponse.getStatusLine().getStatusCode());
		return this;
	}
	/**
	 * Description :- Add Light Service 
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws ParseException 
	 */
	public WebServices AddLightService(String lightSerialNumber) throws ClientProtocolException, IOException, ParseException{
		gbVariables.setEnv();
		
		String displayName = "APITest_Light";
		// String deviceType = "ethernet_gateway";
		Map<String, Object> addLightServVals = new HashMap<String, Object>();
		addLightServVals.put("display_name", displayName);
		JSONObject addLightJson = new JSONObject(addLightServVals);

		StringEntity addLightServiceEntity = new StringEntity(addLightJson.toString(), "UTF8");
		addLightServiceEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPost addLightPostRequest = new HttpPost(gbVariables.AddLightWebserviceURL);
		extentTest.log(Status.INFO,"Authorization Code is = " + gbVariables.AuthorizationCode);
		addLightPostRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		addLightPostRequest.setHeader("Content-Type", "application/json");
		addLightPostRequest.setEntity(addLightServiceEntity);
		HttpResponse addLightServiceResponse = null;
		addLightServiceResponse = httpclient.execute(addLightPostRequest);
		String addLightJsonEntity = EntityUtils.toString(addLightServiceResponse.getEntity());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(addLightJsonEntity);
		lightSerialNumber = (String) jsonObject.get("serial_number");
		extentTest.log(Status.INFO,"Light serial number is = " +lightSerialNumber);

		return this;
	}

	/**
	 * Description :- Gateway Start Learning Service
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public WebServices GatewayStartLearningService(String gatewaySerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		String gateWayServiceURL = gbVariables.SendGatewayOnlineWebserviceURL + gatewaySerialNumber + "/action";
		
		String actionType = "enter_learn_mode";
		Map<String, Object> gatewaySendOnlinevals = new HashMap<String, Object>();
		gatewaySendOnlinevals.put("action_type", actionType);
		JSONObject gatewayjson = new JSONObject(gatewaySendOnlinevals);

		StringEntity gateWaySendOnlineEntity = new StringEntity(gatewayjson.toString(), "UTF8");
		gateWaySendOnlineEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut gateWaySendOnlineReq = new HttpPut(gateWayServiceURL);
		gateWaySendOnlineReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		gateWaySendOnlineReq.setEntity(gateWaySendOnlineEntity);
		HttpResponse GateWaySendOnlineServiceResponse = null;
		GateWaySendOnlineServiceResponse = httpclient.execute(gateWaySendOnlineReq);
		extentTest.log(Status.INFO,"Status code for Gateway Start Learning= "
				+ GateWaySendOnlineServiceResponse.getStatusLine().getStatusCode());

		return this;
	}


	/**
	 * Description :- Learn Light To Gateway Service
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public WebServices LearnLightToGatewayService(String gatewaySerialNumber, String lightSerialNumber) throws ClientProtocolException, IOException, InterruptedException{
		gbVariables.setEnv();
		String learnLightToGatewayServiceUrl = gbVariables.AddLightWebserviceURL + "/" + lightSerialNumber + "/learn/"
				+ gatewaySerialNumber;
		// String actionType = "enter_learn_mode";
		extentTest.log(Status.INFO,"Learn Light to Gateway url is = " + learnLightToGatewayServiceUrl);
		HttpPut learnLightToGatewayPutRequest = new HttpPut(learnLightToGatewayServiceUrl);
		learnLightToGatewayPutRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		// learnLightToGatewayPutRequest.setEntity(gateWaySendOnlineEntity);
		HttpResponse learnLightToGatewayResponse = null;
		learnLightToGatewayResponse = httpclient.execute(learnLightToGatewayPutRequest);
		extentTest.log(Status.INFO,"Learn Light to Gateway service status code = "
				+ learnLightToGatewayResponse.getStatusLine().getStatusCode());
		Thread.sleep(20000);
		return this;
	}

	/*
	 * Add GDO
	 */
	public WebServices AddGDOService(String gdoSerialNumber) throws ClientProtocolException, IOException, ParseException{
		gbVariables.setEnv();
		String AddGarageDoorWebserviceUrl = gbVariables.AddGDOWebserviceURL;
		
		String displayName = "GDO_2";
		Map<String, Object> addGDOServiceVals = new HashMap<String, Object>();
		addGDOServiceVals.put("display_name", displayName);
		JSONObject addGDOJson = new JSONObject(addGDOServiceVals);

		StringEntity addGDOServiceEntity = new StringEntity(addGDOJson.toString(), "UTF8");
		addGDOServiceEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

		HttpPost addGDOServicePostRequest = new HttpPost(AddGarageDoorWebserviceUrl);

		addGDOServicePostRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		addGDOServicePostRequest.setEntity(addGDOServiceEntity);

		HttpResponse addGDOServiceResponse = null;

		addGDOServiceResponse = httpclient.execute(addGDOServicePostRequest);

		String addGDOJsonEntity = EntityUtils.toString(addGDOServiceResponse.getEntity());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(addGDOJsonEntity);
		gdoSerialNumber = (String) jsonObject.get("serial_number");
		extentTest.log(Status.INFO,"GDO serial number is = " + gdoSerialNumber);

		return this;
	}

	/*
	 * Add CDO
	 */
	public WebServices AddCDOService(String cdoSerialNumber) throws ClientProtocolException, IOException, ParseException{
		gbVariables.setEnv();
		String AddCommecialDoorWebserviceUrl = gbVariables.AddCDOWebserviceURL;
		
		String displayName = "CDO_2";
		Map<String, Object> addCDOServiceVals = new HashMap<String, Object>();
		addCDOServiceVals.put("display_name", displayName);
		JSONObject addCDOJson = new JSONObject(addCDOServiceVals);

		StringEntity addCDOServiceEntity = new StringEntity(addCDOJson.toString(), "UTF8");
		addCDOServiceEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

		HttpPost addCDOServicePostRequest = new HttpPost(AddCommecialDoorWebserviceUrl);

		addCDOServicePostRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		addCDOServicePostRequest.setEntity(addCDOServiceEntity);

		HttpResponse addCDOServiceResponse = null;

		addCDOServiceResponse = httpclient.execute(addCDOServicePostRequest);

		String addCDOJsonEntity = EntityUtils.toString(addCDOServiceResponse.getEntity());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(addCDOJsonEntity);
		cdoSerialNumber = (String) jsonObject.get("serial_number");
		extentTest.log(Status.INFO,"CDO serial number is = " + cdoSerialNumber);

		return this;
	}

	/*
	 * Add gate
	 */
	public WebServices AddGateService(String gateSerialNumber) throws ClientProtocolException, IOException, ParseException{
		gbVariables.setEnv();
		String AddGateWebserviceUrl = gbVariables.AddGateWebserviceURL;
		
		String displayName = "Gate_2";
		Map<String, Object> addGateServiceVals = new HashMap<String, Object>();
		addGateServiceVals.put("display_name", displayName);
		JSONObject addGateJson = new JSONObject(addGateServiceVals);

		StringEntity addGateServiceEntity = new StringEntity(addGateJson.toString(), "UTF8");
		addGateServiceEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

		HttpPost addGateServicePostRequest = new HttpPost(AddGateWebserviceUrl);

		addGateServicePostRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		addGateServicePostRequest.setEntity(addGateServiceEntity);

		HttpResponse addGateServiceResponse = null;

		addGateServiceResponse = httpclient.execute(addGateServicePostRequest);

		String addGateJsonEntity = EntityUtils.toString(addGateServiceResponse.getEntity());
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(addGateJsonEntity);
		gateSerialNumber = (String) jsonObject.get("serial_number");
		extentTest.log(Status.INFO,"Gate serial number is = " + gateSerialNumber);

		return this;
	}

	/*
	 * Get GDO State
	 */
	public WebServices getGDOState(String gdoSerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		
		String AddGarageDoorWebserviceUrl = gbVariables.AddGDOWebserviceURL + gdoSerialNumber;
		HttpGet getGDOStateRequest = new HttpGet(AddGarageDoorWebserviceUrl);
		getGDOStateRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		HttpResponse getGDOStateResponse = httpclient.execute(getGDOStateRequest);
		extentTest.log(Status.INFO, "Response code is " +getGDOStateResponse.getStatusLine().getStatusCode());

		return this;
	}

	/*
	 *Perform Garage Door Actions 
	 */
	public WebServices GarageDoorActions(String ActionType,String gdoSerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		
		String GarageDoorActionURL = gbVariables.GDOWebserviceURL +"/"+gdoSerialNumber+ "/action";

		Map<String, Object> GDOServiceVals = new HashMap<String, Object>();
		GDOServiceVals.put("action_type", ActionType);
		JSONObject GDOJson = new JSONObject(GDOServiceVals);

		StringEntity GDOServiceEntity = new StringEntity(GDOJson.toString(), "UTF8");
		GDOServiceEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

		HttpPut GdoServicePutRequest = new HttpPut(GarageDoorActionURL);

		GdoServicePutRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		GdoServicePutRequest.setEntity(GDOServiceEntity);

		HttpResponse gdoServiceResponse = httpclient.execute(GdoServicePutRequest);

		extentTest.log(Status.INFO,"Status Code value is :- "+gdoServiceResponse.getStatusLine().getStatusCode());

		return this;
	}

	/*
	 * Learn / UnLearn Garage Door Opener
	 */
	public WebServices LearnGDOToGatewayService(String gatewaySerialNumber, String gdoSerialNumber) throws ClientProtocolException, IOException, InterruptedException {
		gbVariables.setEnv();
		
		String learnGDOWebservceURL = gbVariables.GDOWebserviceURL + "/" + gdoSerialNumber + "/learn/"+gatewaySerialNumber;
		extentTest.log(Status.INFO,"learn GDO Webservice URL is ="+learnGDOWebservceURL);
		HttpPut learnGDOPutRequest = new HttpPut(learnGDOWebservceURL);

		learnGDOPutRequest.setHeader("Authorization", gbVariables.AuthorizationCode);

		HttpResponse learnGDOResponse = httpclient.execute(learnGDOPutRequest);

		extentTest.log(Status.INFO,"Learn GDO Status Code :- "+learnGDOResponse.getStatusLine().getStatusCode());

		Thread.sleep(20000);

		return this;
	}

	/*
	 * Learn / UnLearn Commercial Door Opener
	 */
	public WebServices LearnCDOToGatewayService(String gatewaySerialNumber, String cdoSerialNumber ) throws ClientProtocolException, IOException, InterruptedException {
		gbVariables.setEnv();
		
		String learnCDOWebservceURL = gbVariables.CDOWebserviceURL + "/" + cdoSerialNumber + "/learn/"+gatewaySerialNumber;

		extentTest.log(Status.INFO,"learn CDO Webservice URL is = "+learnCDOWebservceURL);
		HttpPut learnCDOPutRequest = new HttpPut(learnCDOWebservceURL);

		learnCDOPutRequest.setHeader("Authorization", gbVariables.AuthorizationCode);

		HttpResponse learnCDOResponse = httpclient.execute(learnCDOPutRequest);

		extentTest.log(Status.INFO,"Learn CDO Status Code :- "+learnCDOResponse.getStatusLine().getStatusCode());

		Thread.sleep(20000);

		return this;
	}

	/*
	 * Learn / UnLearn Gate
	 */
	public WebServices LearnGateToGatewayService(String gatewaySerialNumber,String gateSerialNumber) throws ClientProtocolException, IOException, InterruptedException {
		gbVariables.setEnv();
		
		String learnGateWebservceURL = gbVariables.GateWebserviceURL + "/" + gateSerialNumber + "/learn/"+gatewaySerialNumber;
		extentTest.log(Status.INFO,"learn Gate Webservice URL is = "+learnGateWebservceURL);
		HttpPut learnGatePutRequest = new HttpPut(learnGateWebservceURL);
		learnGatePutRequest.setHeader("Authorization", gbVariables.AuthorizationCode);
		HttpResponse learnGateResponse = httpclient.execute(learnGatePutRequest);
		extentTest.log(Status.INFO,"Learn Gate Status Code :- "+learnGateResponse.getStatusLine().getStatusCode());
		Thread.sleep(20000);
		return this;
	}
	/**
	 * Description :- To attach lock for GDO
	 * @return
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public WebServices attachLockGDO(String GDOSerialNumber) throws ClientProtocolException, IOException,ParseException{
		gbVariables.setEnv();
		
		String attachLockGDOURL = gbVariables.GDOWebserviceURL + GDOSerialNumber + "/action";
		extentTest.log(Status.INFO,"attachLockGDOURL--"+attachLockGDOURL);
		String actionType = "attach_gdo_lock";
		Map<String, Object> attachLockGDOvals = new HashMap<String, Object>();
		attachLockGDOvals.put("action_type", actionType);
		JSONObject attachLockGDOjson = new JSONObject(attachLockGDOvals);

		StringEntity attachLockGDOEntity = new StringEntity(attachLockGDOjson.toString(), "UTF8");
		attachLockGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut attachLockGDOReq = new HttpPut(attachLockGDOURL);
		attachLockGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		attachLockGDOReq.setEntity(attachLockGDOEntity);
		HttpResponse attachLockGDOResponse = null;
		attachLockGDOResponse = httpclient.execute(attachLockGDOReq);
		extentTest.log(Status.INFO,"attach status code = "
				+ attachLockGDOResponse.getStatusLine().getStatusCode());

		return this;
	}

	/**
	 * Description :- To detach lock for GDO
	 * @param GDOSerialNumber
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public WebServices detachLockGDO(String GDOSerialNumber) throws ClientProtocolException, IOException,ParseException{
		gbVariables.setEnv();
		
		String detachLockGDOURL = gbVariables.GDOWebserviceURL + GDOSerialNumber + "/action";
		
		extentTest.log(Status.INFO,"deatach Lock GDO URL--"+detachLockGDOURL);
		String actionType = "detach_gdo_lock";
		Map<String, Object> detachLockGDOvals = new HashMap<String, Object>();
		detachLockGDOvals.put("action_type", actionType);
		JSONObject attachLockGDOjson = new JSONObject(detachLockGDOvals);

		StringEntity detachLockGDOEntity = new StringEntity(attachLockGDOjson.toString(), "UTF8");
		detachLockGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut detachLockGDOReq = new HttpPut(detachLockGDOURL);
		detachLockGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		detachLockGDOReq.setEntity(detachLockGDOEntity);
		HttpResponse detachLockGDOResponse = null;
		detachLockGDOResponse = httpclient.execute(detachLockGDOReq);
		extentTest.log(Status.INFO," detach status code = "
				+ detachLockGDOResponse.getStatusLine().getStatusCode());

		return this;
	}

	/**
	 * Description : To enter learn mode for VGDO
	 * @param vgdoSerialNumber
	 * @return Page Object
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public WebServices enterLearnModeForVGDO(String vgdoSerialNumber ) throws ClientProtocolException, IOException {
		gbVariables.setEnv();
		
		String enterlearnmodeGDOURL = gbVariables.GDOWebserviceURL+ vgdoSerialNumber + "/action";
		extentTest.log(Status.INFO,"Enter learn mode VGDO URL --"+enterlearnmodeGDOURL);
		String actionType = "enter_learn_mode";
		Map<String, Object> enterlearnmodeGDOvals = new HashMap<String, Object>();
		enterlearnmodeGDOvals.put("action_type", actionType);
		JSONObject enterlearnmodeGDOjson = new JSONObject(enterlearnmodeGDOvals);

		StringEntity enterlearnmodeGDOEntity = new StringEntity(enterlearnmodeGDOjson.toString(), "UTF8");
		enterlearnmodeGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut enterlearnmodeGDOReq = new HttpPut(enterlearnmodeGDOURL);
		enterlearnmodeGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		enterlearnmodeGDOReq.setEntity(enterlearnmodeGDOEntity);
		HttpResponse enterlearnmodeGDOResponse = null;
		enterlearnmodeGDOResponse = httpclient.execute(enterlearnmodeGDOReq);
		extentTest.log(Status.INFO," Enter learn mode for VGDO Status code  = "
				+ enterlearnmodeGDOResponse.getStatusLine().getStatusCode());

		return this;
	}

	/**
	 * Description :- To map the sensor onto the VGDO
	 * @aparam gdoSerialNumber,sensorType
	 * @return class object
	 * @throws IOException,ClientProtocolException
	 */
	public WebServices testSensorVGDO(String gdoSerialNumber,String sensorType) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		
		String testSensorVGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
		extentTest.log(Status.INFO,"Enter test sensor VGDO URL --"+testSensorVGDOURL);
		String actionType = "test_door_position_sensor";
		String actionParam ;
		if(sensorType.equalsIgnoreCase("BlueTooth")){
			actionParam = "80";
		} else {
			actionParam = "81";
		}

		Map<String, Object> testSensorGDOvals = new HashMap<String, Object>();
		testSensorGDOvals.put("action_type", actionType);
		testSensorGDOvals.put("action_param",actionParam );
		JSONObject testSensorGDOjson = new JSONObject(testSensorGDOvals);

		StringEntity testSensorGDOEntity = new StringEntity(testSensorGDOjson.toString(), "UTF8");
		testSensorGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut testSensorGDOReq = new HttpPut(testSensorVGDOURL);
		testSensorGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		testSensorGDOReq.setEntity(testSensorGDOEntity);
		HttpResponse testSensorGDOResponse = null;
		testSensorGDOResponse = httpclient.execute(testSensorGDOReq);
		extentTest.log(Status.INFO,"Status code = "
				+ testSensorGDOResponse.getStatusLine().getStatusCode());
		return this;
	}

	/**
	 * Description :- Stops communication between the DPS sensor and the VGDO
	 * @param vgdoSerialNumber
	 * @return class object
	 * @throws IOException,ClientProtocolException
	 */
	public WebServices enterDPSNoCommunication(String vgdoSerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		
		String setDpsMonitorOnlyURL = gbVariables.GDOWebserviceURL+ vgdoSerialNumber + "/action";
		extentTest.log(Status.INFO,"Enter DPS monitor only URL --"+setDpsMonitorOnlyURL);
		String actionType = "enter_dps_no_communication";
		Map<String, Object> enterDpsNoCommnvals = new HashMap<String, Object>();
		enterDpsNoCommnvals.put("action_type", actionType);
		JSONObject enterDpsNoCommnjson = new JSONObject(enterDpsNoCommnvals);

		StringEntity enterDpsNoCommnEntity = new StringEntity(enterDpsNoCommnjson.toString(), "UTF8");
		enterDpsNoCommnEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut enterDpsNoCommnReq = new HttpPut(setDpsMonitorOnlyURL);
		enterDpsNoCommnReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		enterDpsNoCommnReq.setEntity(enterDpsNoCommnEntity);
		HttpResponse enterDpsNoCommnResponse = null;
		enterDpsNoCommnResponse = httpclient.execute(enterDpsNoCommnReq);
		extentTest.log(Status.INFO,"Status code  = "
				+ enterDpsNoCommnResponse.getStatusLine().getStatusCode());
		return this;
	}

	/**
	 * Description :- Reestablishes communication between the DPS sensor and the VGDO
	 * @param vgdoSerialNumber
	 * @return class object
	 * @throws IOException,ClientProtocolException
	 */
	public WebServices exitDPSNoCommunication(String vgdoSerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();

		String setDpsMonitorOnlyURL = gbVariables.GDOWebserviceURL+ vgdoSerialNumber + "/action";
		extentTest.log(Status.INFO,"Exit DPS no commn URL --"+setDpsMonitorOnlyURL);
		
		String actionType = "exit_dps_no_communication";
		Map<String, Object> exitDpsNoCommnvals = new HashMap<String, Object>();
		exitDpsNoCommnvals.put("action_type", actionType);
		JSONObject exitDpsNoCommnjson = new JSONObject(exitDpsNoCommnvals);

		StringEntity exitDpsNoCommnEntity = new StringEntity(exitDpsNoCommnjson.toString(), "UTF8");
		exitDpsNoCommnEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut exitDpsNoCommnReq = new HttpPut(setDpsMonitorOnlyURL);
		exitDpsNoCommnReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		exitDpsNoCommnReq.setEntity(exitDpsNoCommnEntity);
		HttpResponse exitDpsNoCommnResponse = null;
		exitDpsNoCommnResponse = httpclient.execute(exitDpsNoCommnReq);
		extentTest.log(Status.INFO,"Status code  = "
				+ exitDpsNoCommnResponse.getStatusLine().getStatusCode());
		return this;
	}

	/**
	 * Description :- Set DPS monitor Only
	 * @param vgdoSerialNumber
	 * @return class object
	 * @throws IOException,ClientProtocolException
	 */
	public WebServices setDpsMonitorOnly(String vgdoSerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		
		String setDpsMonitorOnlyURL = gbVariables.GDOWebserviceURL+ vgdoSerialNumber + "/action";
		extentTest.log(Status.INFO,"Set DPS monitor only URL--"+setDpsMonitorOnlyURL);
		String actionType = "set_dps_monitor_only";
		Map<String, Object> testSensorGDOvals = new HashMap<String, Object>();
		testSensorGDOvals.put("action_type", actionType);
		JSONObject setDpsMonitorOnlyjson = new JSONObject(testSensorGDOvals);

		StringEntity setDpsMonitorOnlyEntity = new StringEntity(setDpsMonitorOnlyjson.toString(), "UTF8");
		setDpsMonitorOnlyEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut setDpsMonitorOnlyReq = new HttpPut(setDpsMonitorOnlyURL);
		setDpsMonitorOnlyReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		setDpsMonitorOnlyReq.setEntity(setDpsMonitorOnlyEntity);
		HttpResponse setDpsMonitorOnlyResponse = null;
		setDpsMonitorOnlyResponse = httpclient.execute(setDpsMonitorOnlyReq);
		extentTest.log(Status.INFO,"Status code  = "
				+ setDpsMonitorOnlyResponse.getStatusLine().getStatusCode());
		return this;
	}


	/**
	 * Description :- Set DPS Actionable 
	 * @param vgdoSerialNumber
	 * @return class object
	 * @throws  ClientProtocolException, IOException
	 */
	public WebServices setDpsActionableOnly(String vgdoSerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		
		String setDpsActionableOnlyURL = gbVariables.GDOWebserviceURL+ vgdoSerialNumber + "/action";
		extentTest.log(Status.INFO,"Set DPS Sctionable only URL --"+setDpsActionableOnlyURL);
		String actionType = "set_dps_actionable";
		Map<String, Object> testSensorGDOvals = new HashMap<String, Object>();
		testSensorGDOvals.put("action_type", actionType);
		JSONObject setDpsActionableOnlyjson = new JSONObject(testSensorGDOvals);

		StringEntity setDpsActionableOnlyEntity = new StringEntity(setDpsActionableOnlyjson.toString(), "UTF8");
		setDpsActionableOnlyEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
		HttpPut setDpsActionableOnlyReq = new HttpPut(setDpsActionableOnlyURL);
		setDpsActionableOnlyReq.setHeader("Authorization", gbVariables.AuthorizationCode);
		setDpsActionableOnlyReq.setEntity(setDpsActionableOnlyEntity);
		HttpResponse setDpsActionableOnlyResponse = null;
		setDpsActionableOnlyResponse = httpclient.execute(setDpsActionableOnlyReq);
		extentTest.log(Status.INFO,"Status code  = "
				+ setDpsActionableOnlyResponse.getStatusLine().getStatusCode());
		return this;
	}

	/**
	 * Description : Sends the Gateway Offline
	 * @param gatewaySerialNumber
	 * @return class object
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public WebServices SendGatewayOfflineService(String gatewaySerialNumber) throws ClientProtocolException, IOException{
		gbVariables.setEnv();
		String gateWayServiceURL = gbVariables.SendGatewayOnlineWebserviceURL + gatewaySerialNumber + "/action";
		extentTest.log(Status.INFO,"gateWay webservice URL "+gateWayServiceURL);
		String actionType = "send_offline";
		String errorText ="Error: During sending Gatway Offline ";
		try
		{
			
			Map<String, Object> gatewaySendOfflinevals = new HashMap<String, Object>();
			gatewaySendOfflinevals.put("action_type", actionType);
			JSONObject gatewayjson = new JSONObject(gatewaySendOfflinevals);

			StringEntity gateWaySendOfflineEntity = new StringEntity(gatewayjson.toString(), "UTF8");
			gateWaySendOfflineEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut gateWaySendOfflineReq = new HttpPut(gateWayServiceURL);
			gateWaySendOfflineReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			gateWaySendOfflineReq.setEntity(gateWaySendOfflineEntity);
			HttpResponse GateWaySendOfflineServiceResponse = null;
			GateWaySendOfflineServiceResponse = httpclient.execute(gateWaySendOfflineReq);
			extentTest.log(Status.INFO,"Status code for Gateway Send Offline = "
					+ GateWaySendOfflineServiceResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description This function will log errors occurring during execution
	 * @param e
	 * @param errorText
	 */
	private void catchWebServiceException(Exception e, String errorText) {
		extentTest.log(Status.ERROR, errorText + " - " + e.getClass().getName());
	}

	/**
	 * Description :- To Enter Open Obstruction Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterOpenObstructionGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During open gate obstruction";
		try {
			gbVariables.setEnv();
			
			String enterOpenObstructionGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter open obstruction URl is "+enterOpenObstructionGateURL);
			String actionType = "enter_open_obstruction";
			Map<String, Object> enterOpenObstructionGatevals = new HashMap<String, Object>();
			enterOpenObstructionGatevals.put("action_type", actionType);
			JSONObject enterOpenObstructionjson = new JSONObject(enterOpenObstructionGatevals);

			StringEntity enterOpenObstructionGateEntity = new StringEntity(enterOpenObstructionjson.toString(), "UTF8");
			enterOpenObstructionGateEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterOpenObstructionGateReq = new HttpPut(enterOpenObstructionGateURL);
			enterOpenObstructionGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterOpenObstructionGateReq.setEntity(enterOpenObstructionGateEntity);
			HttpResponse enterOpenObstructionGateResponse = null;
			enterOpenObstructionGateResponse = httpclient.execute(enterOpenObstructionGateReq);
			extentTest.log(Status.INFO, "Status code for Enter Open Obstruction= "
					+ enterOpenObstructionGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Open Obstruction Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitOpenObstructionGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exit open gate obstruction ";
		try {
			gbVariables.setEnv();
			
			String exitOpenObstructionGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			extentTest.log(Status.INFO,"exit open obstruction URl is "+exitOpenObstructionGateURL);
			String actionType = "exit_open_obstruction";
			Map<String, Object> exitOpenObstructionGatevals = new HashMap<String, Object>();
			exitOpenObstructionGatevals.put("action_type", actionType);
			JSONObject exitOpenObstructionGatejson = new JSONObject(exitOpenObstructionGatevals);

			StringEntity exitOpenObstructionGDOEntity = new StringEntity(exitOpenObstructionGatejson.toString(), "UTF8");
			exitOpenObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitOpenObstructionGateReq = new HttpPut(exitOpenObstructionGateURL);
			exitOpenObstructionGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitOpenObstructionGateReq.setEntity(exitOpenObstructionGDOEntity);
			HttpResponse exitOpenObstructionGateResponse = null;
			exitOpenObstructionGateResponse = httpclient.execute(exitOpenObstructionGateReq);
			extentTest.log(Status.INFO, "Status code for Exit Open Obstruction= "
					+ exitOpenObstructionGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter Open Obstruction GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterOpenObstructionGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During enter open GDO obstruction ";
		try {
			gbVariables.setEnv();
			
			String enterOpenObstructionGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter open obstruction URl is "+enterOpenObstructionGDOURL);
			String actionType = "enter_open_obstruction";
			Map<String, Object> enterOpenObstructionGDOvals = new HashMap<String, Object>();
			enterOpenObstructionGDOvals.put("action_type", actionType);
			JSONObject enterOpenObstructionGDOjson = new JSONObject(enterOpenObstructionGDOvals);

			StringEntity enterOpenObstructionGDOEntity = new StringEntity(enterOpenObstructionGDOjson.toString(), "UTF8");
			enterOpenObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterOpenObstructionGDOReq = new HttpPut(enterOpenObstructionGDOURL);
			enterOpenObstructionGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterOpenObstructionGDOReq.setEntity(enterOpenObstructionGDOEntity);
			HttpResponse enterOpenObstructionGDOResponse = null;
			enterOpenObstructionGDOResponse = httpclient.execute(enterOpenObstructionGDOReq);
			extentTest.log(Status.INFO, "Status code for Enter Open Obstruction= "+ enterOpenObstructionGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Open Obstruction GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitOpenObstructionGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exit open GDO obstruction ";
		try {
			gbVariables.setEnv();
			
			String exitOpenObstructionGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit open obstruction URl is "+exitOpenObstructionGDOURL);
			String actionType = "exit_open_obstruction";
			Map<String, Object> exitOpenObstructionGDOvals = new HashMap<String, Object>();
			exitOpenObstructionGDOvals.put("action_type", actionType);
			JSONObject exitOpenObstructionGDOjson = new JSONObject(exitOpenObstructionGDOvals);

			StringEntity exitOpenObstructionGDOEntity = new StringEntity(exitOpenObstructionGDOjson.toString(), "UTF8");
			exitOpenObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitOpenObstructionGDOReq = new HttpPut(exitOpenObstructionGDOURL);
			exitOpenObstructionGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitOpenObstructionGDOReq.setEntity(exitOpenObstructionGDOEntity);
			HttpResponse exitOpenObstructionGDOResponse = null;
			exitOpenObstructionGDOResponse = httpclient.execute(exitOpenObstructionGDOReq);
			extentTest.log(Status.INFO, "Status code for Exit Open Obstruction= "+ exitOpenObstructionGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter Open Obstruction CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterOpenObstructionCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During enter open CDO obstruction ";
		try { 
			gbVariables.setEnv();
			
			String enterOpenObstructionCDOURL = gbVariables.CDOWebserviceURL+ cdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter open obstruction URl is "+enterOpenObstructionCDOURL);
			String actionType = "enter_open_obstruction";
			Map<String, Object> enterOpenObstructionCDOvals = new HashMap<String, Object>();
			enterOpenObstructionCDOvals.put("action_type", actionType);
			JSONObject enterOpenObstructionjson = new JSONObject(enterOpenObstructionCDOvals);

			StringEntity enterOpenObstructionCDOEntity = new StringEntity(enterOpenObstructionjson.toString(), "UTF8");
			enterOpenObstructionCDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterOpenObstructionCDOReq = new HttpPut(enterOpenObstructionCDOURL);
			enterOpenObstructionCDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterOpenObstructionCDOReq.setEntity(enterOpenObstructionCDOEntity);
			HttpResponse enterOpenObstructionCDOResponse = null;
			enterOpenObstructionCDOResponse = httpclient.execute(enterOpenObstructionCDOReq);
			extentTest.log(Status.INFO, "Status code for Enter Open Obstruction= "
					+ enterOpenObstructionCDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Open Obstruction CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitOpenObstructionCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exit open CDO obstruction ";
		try { 
			gbVariables.setEnv();
			
			String exitOpenObstructionCDOURL = gbVariables.CDOWebserviceURL+ cdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit open obstruction URl is "+exitOpenObstructionCDOURL);
			String actionType = "exit_open_obstruction";
			Map<String, Object> exitOpenObstructionCDOvals = new HashMap<String, Object>();
			exitOpenObstructionCDOvals.put("action_type", actionType);
			JSONObject exitOpenObstructionCDOjson = new JSONObject(exitOpenObstructionCDOvals);

			StringEntity exitOpenObstructionGDOEntity = new StringEntity(exitOpenObstructionCDOjson.toString(), "UTF8");
			exitOpenObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitOpenObstructionCDOReq = new HttpPut(exitOpenObstructionCDOURL);
			exitOpenObstructionCDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitOpenObstructionCDOReq.setEntity(exitOpenObstructionGDOEntity);
			HttpResponse exitOpenObstructionCDOResponse = null;
			exitOpenObstructionCDOResponse = httpclient.execute(exitOpenObstructionCDOReq);
			extentTest.log(Status.INFO, "Status code for Exit Open Obstruction= "
					+ exitOpenObstructionCDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :-  To Block Photo eye on the GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices blockPhotoEyeGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During GDO photo eye block";
		try {
			gbVariables.setEnv();
			
			String blockPhotoEyeGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Block photo Eye URL is "+blockPhotoEyeGDOURL);
			String actionType = "block_photo_eye";
			Map<String, Object> blockPhotoEyeGDOvals = new HashMap<String, Object>();
			blockPhotoEyeGDOvals.put("action_type", actionType);
			JSONObject blockPhotoEyeGDOjson = new JSONObject(blockPhotoEyeGDOvals);

			StringEntity blockPhotoEyeGDOEntity = new StringEntity(blockPhotoEyeGDOjson.toString(), "UTF8");
			blockPhotoEyeGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut blockPhotoEyeGDOReq = new HttpPut(blockPhotoEyeGDOURL);
			blockPhotoEyeGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			blockPhotoEyeGDOReq.setEntity(blockPhotoEyeGDOEntity);
			HttpResponse blockPhotoEyeGDOResponse = null;
			blockPhotoEyeGDOResponse = httpclient.execute(blockPhotoEyeGDOReq);
			extentTest.log(Status.INFO,"Status code  = "
					+ blockPhotoEyeGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Unblock the Photo eye  on GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices unblockPhotoEyeGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During GDO photo eye  un-block";
		try { 
			gbVariables.setEnv();
			
			String unblockPhotoEyeGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"un-Block photo Eye URL is "+unblockPhotoEyeGDOURL);
			String actionType = "unblock_photo_eye";
			Map<String, Object> unblockPhotoEyeGDOvals = new HashMap<String, Object>();
			unblockPhotoEyeGDOvals.put("action_type", actionType);
			JSONObject unblockPhotoEyeGDOjson = new JSONObject(unblockPhotoEyeGDOvals);

			StringEntity unblockPhotoEyeGDOEntity = new StringEntity(unblockPhotoEyeGDOjson.toString(), "UTF8");
			unblockPhotoEyeGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut unblockPhotoEyeGDOReq = new HttpPut(unblockPhotoEyeGDOURL);
			unblockPhotoEyeGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			unblockPhotoEyeGDOReq.setEntity(unblockPhotoEyeGDOEntity);
			HttpResponse unblockPhotoEyeGDOResponse = null;
			unblockPhotoEyeGDOResponse = httpclient.execute(unblockPhotoEyeGDOReq);
			extentTest.log(Status.INFO,"Status code  = "
					+ unblockPhotoEyeGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter AC loss for light
	 * @param lampSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterAcLossForLight(String lampSerialNumber) throws ClientProtocolException, IOException{

		String errorText ="Error: During entering AC loss for light ";
		try {
			gbVariables.setEnv();
			
			String enterAcLossForLightURL = gbVariables.LightWebserviceURL+ lampSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Ac Loss for light URL is "+enterAcLossForLightURL);

			String actionType = "enter_ac_loss";
			Map<String, Object> enterAcLossForLight = new HashMap<String, Object>();
			enterAcLossForLight.put("action_type", actionType);
			JSONObject enterAcLossjson = new JSONObject(enterAcLossForLight);

			StringEntity enterAcLossEntity = new StringEntity(enterAcLossjson.toString(), "UTF8");
			enterAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterAcLossForLighReq = new HttpPut(enterAcLossForLightURL);
			enterAcLossForLighReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterAcLossForLighReq.setEntity(enterAcLossEntity);
			HttpResponse enterAcLossForLighResponse = null;
			enterAcLossForLighResponse = httpclient.execute(enterAcLossForLighReq);
			extentTest.log(Status.INFO,"Status code for enter ac loss = "
					+ enterAcLossForLighResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit AC loss for light
	 * @param lampSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitAcLossForLight(String lampSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting AC loss for light ";
		try {
			gbVariables.setEnv();
			
			String exitAcLossForLightURL = gbVariables.LightWebserviceURL + lampSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit Ac Loss for light URL is "+exitAcLossForLightURL);
			String actionType = "exit_ac_loss";
			Map<String, Object> exitAcLossvals = new HashMap<String, Object>();
			exitAcLossvals.put("action_type", actionType);
			JSONObject exitAcLossjson = new JSONObject(exitAcLossvals);

			StringEntity exitAcLossEntity = new StringEntity(exitAcLossjson.toString(), "UTF8");
			exitAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitAcLossForLighReq = new HttpPut(exitAcLossForLightURL);
			exitAcLossForLighReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitAcLossForLighReq.setEntity(exitAcLossEntity);
			HttpResponse exitAcLossForLighResponse = null;
			exitAcLossForLighResponse = httpclient.execute(exitAcLossForLighReq);
			extentTest.log(Status.INFO,"Status code for exit ac loss = "
					+ exitAcLossForLighResponse.getStatusLine().getStatusCode());

		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter AC loss for GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterAcLossForGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering AC loss for GDO ";
		try {
			gbVariables.setEnv();
			
			String enterAcLossForGdoURL = gbVariables.GDOWebserviceURL + gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Ac Loss for GDO URL is "+enterAcLossForGdoURL);
			String actionType = "set_ac_loss_true";
			Map<String, Object> enterAcLossvals = new HashMap<String, Object>();
			enterAcLossvals.put("action_type", actionType);
			JSONObject enterAcLossjson = new JSONObject(enterAcLossvals);

			StringEntity enterAcLossEntity = new StringEntity(enterAcLossjson.toString(), "UTF8");
			enterAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterAcLossForGdoReq = new HttpPut(enterAcLossForGdoURL);
			enterAcLossForGdoReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterAcLossForGdoReq.setEntity(enterAcLossEntity);
			HttpResponse eterAcLossForGdoResponse = null;
			eterAcLossForGdoResponse = httpclient.execute(enterAcLossForGdoReq);
			extentTest.log(Status.INFO,"Status code for enter ac loss = "
					+ eterAcLossForGdoResponse.getStatusLine().getStatusCode());

		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit AC loss for GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitAcLossForGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting AC loss for GDO ";
		try {
			gbVariables.setEnv();
			
			String exitAcLossForGdoURL = gbVariables.GDOWebserviceURL + gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit Ac Loss for GDO URL is "+exitAcLossForGdoURL);
			String actionType = "set_ac_loss_false";
			Map<String, Object> exitAcLossvals = new HashMap<String, Object>();
			exitAcLossvals.put("action_type", actionType);
			JSONObject exitAcLossjson = new JSONObject(exitAcLossvals);

			StringEntity exitAcLossEntity = new StringEntity(exitAcLossjson.toString(), "UTF8");
			exitAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitAcLossForGdoReq = new HttpPut(exitAcLossForGdoURL);
			exitAcLossForGdoReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitAcLossForGdoReq.setEntity(exitAcLossEntity);
			HttpResponse exitAcLossForGdoResponse = null;
			exitAcLossForGdoResponse = httpclient.execute(exitAcLossForGdoReq);
			extentTest.log(Status.INFO,"Status code for exit ac loss = "
					+ exitAcLossForGdoResponse.getStatusLine().getStatusCode());

		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter AC loss for CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterAcLossForCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering AC loss for CDO ";
		try {
			gbVariables.setEnv();
			
			String enterAcLossForCdoURL = gbVariables.CDOWebserviceURL+ cdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Ac Loss for CDO URL is "+enterAcLossForCdoURL);
			String actionType = "enter_ac_loss";
			Map<String, Object> enterAcLossForCdo = new HashMap<String, Object>();
			enterAcLossForCdo.put("action_type", actionType);
			JSONObject enterAcLossjson = new JSONObject(enterAcLossForCdo);

			StringEntity enterAcLossEntity = new StringEntity(enterAcLossjson.toString(), "UTF8");
			enterAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterAcLossForCdoReq = new HttpPut(enterAcLossForCdoURL);
			enterAcLossForCdoReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterAcLossForCdoReq.setEntity(enterAcLossEntity);
			HttpResponse enterAcLossForCdoResponse = null;
			enterAcLossForCdoResponse = httpclient.execute(enterAcLossForCdoReq);
			extentTest.log(Status.INFO,"Status code for enter ac loss = "
					+ enterAcLossForCdoResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit AC loss for CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitAcLossForCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting AC loss for CDO ";
		try {
			gbVariables.setEnv();
			
			String exitAcLossForCdoURL = gbVariables.CDOWebserviceURL + cdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit Ac Loss for CDO URL is "+exitAcLossForCdoURL);
			String actionType = "exit_ac_loss";
			Map<String, Object> exitAcLossvals = new HashMap<String, Object>();
			exitAcLossvals.put("action_type", actionType);
			JSONObject exitAcLossjson = new JSONObject(exitAcLossvals);

			StringEntity exitAcLossEntity = new StringEntity(exitAcLossjson.toString(), "UTF8");
			exitAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitAcLossForCdoReq = new HttpPut(exitAcLossForCdoURL);
			exitAcLossForCdoReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitAcLossForCdoReq.setEntity(exitAcLossEntity);
			HttpResponse exitAcLossForCdoResponse = null;
			exitAcLossForCdoResponse = httpclient.execute(exitAcLossForCdoReq);
			extentTest.log(Status.INFO,"Status code for exit ac loss = "
					+ exitAcLossForCdoResponse.getStatusLine().getStatusCode());

		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter AC loss for GATE
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterAcLossForGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering AC loss for Gate ";
		try {
			gbVariables.setEnv();
			
			String enterAcLossForGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Ac Loss for Gate URL is "+enterAcLossForGateURL);
			String actionType = "enter_ac_loss";
			Map<String, Object> enterAcLossForGate = new HashMap<String, Object>();
			enterAcLossForGate.put("action_type", actionType);
			JSONObject enterAcLossjson = new JSONObject(enterAcLossForGate);

			StringEntity enterAcLossEntity = new StringEntity(enterAcLossjson.toString(), "UTF8");
			enterAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterAcLossForGateReq = new HttpPut(enterAcLossForGateURL);
			enterAcLossForGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterAcLossForGateReq.setEntity(enterAcLossEntity);
			HttpResponse enterAcLossForGateResponse = null;
			enterAcLossForGateResponse = httpclient.execute(enterAcLossForGateReq);
			extentTest.log(Status.INFO,"Status code for enter ac loss = "
					+ enterAcLossForGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit AC loss for Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitAcLossForGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting AC loss for Gate ";
		try {
			gbVariables.setEnv();
			
			String exitAcLossForGateURL = gbVariables.GateWebserviceURL + gateSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit Ac Loss for Gate URL is "+exitAcLossForGateURL);
			String actionType = "exit_ac_loss";
			Map<String, Object> exitAcLossvals = new HashMap<String, Object>();
			exitAcLossvals.put("action_type", actionType);
			JSONObject exitAcLossjson = new JSONObject(exitAcLossvals);

			StringEntity exitAcLossEntity = new StringEntity(exitAcLossjson.toString(), "UTF8");
			exitAcLossEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitAcLossForGateReq = new HttpPut(exitAcLossForGateURL);
			exitAcLossForGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitAcLossForGateReq.setEntity(exitAcLossEntity);
			HttpResponse exitAcLossForGateResponse = null;
			exitAcLossForGateResponse = httpclient.execute(exitAcLossForGateReq);
			extentTest.log(Status.INFO,"Status code for exit ac loss = "
					+ exitAcLossForGateResponse.getStatusLine().getStatusCode());

		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter Close Obstruction GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterCloseObstructionGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering close obstruction GDO ";
		try {
			gbVariables.setEnv();
			
			String enterCloseObstructionGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Close obstruction URL is "+enterCloseObstructionGDOURL);
			
			String actionType = "enter_close_obstruction";
			Map<String, Object> enterCloseObstructionGDOvals = new HashMap<String, Object>();
			enterCloseObstructionGDOvals.put("action_type", actionType);
			JSONObject enterCloseObstructionGDOjson = new JSONObject(enterCloseObstructionGDOvals);

			StringEntity enterCloseObstructionGDOEntity = new StringEntity(enterCloseObstructionGDOjson.toString(), "UTF8");
			enterCloseObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterCloseObstructionGDOReq = new HttpPut(enterCloseObstructionGDOURL);
			enterCloseObstructionGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterCloseObstructionGDOReq.setEntity(enterCloseObstructionGDOEntity);
			HttpResponse enterCloseObstructionGDOResponse = null;
			enterCloseObstructionGDOResponse = httpclient.execute(enterCloseObstructionGDOReq);
			extentTest.log(Status.INFO, "Status code for Enter Close Obstruction= "+ enterCloseObstructionGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Close Obstruction GDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitCloseObstructionGDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting close obstruction GDO ";
		try {
			gbVariables.setEnv();
			
			String exitCloseObstructionGDOURL = gbVariables.GDOWebserviceURL+ gdoSerialNumber + "/action";
			extentTest.log(Status.INFO,"Exit Close obstruction URL is "+exitCloseObstructionGDOURL);
			String actionType = "exit_close_obstruction";
			Map<String, Object> exitCloseObstructionGDOvals = new HashMap<String, Object>();
			exitCloseObstructionGDOvals.put("action_type", actionType);
			JSONObject exitCloseObstructionGDOjson = new JSONObject(exitCloseObstructionGDOvals);

			StringEntity exitCloseObstructionGDOEntity = new StringEntity(exitCloseObstructionGDOjson.toString(), "UTF8");
			exitCloseObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitCloseObstructionGDOReq = new HttpPut(exitCloseObstructionGDOURL);
			exitCloseObstructionGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitCloseObstructionGDOReq.setEntity(exitCloseObstructionGDOEntity);
			HttpResponse exitCloseObstructionGDOResponse = null;
			exitCloseObstructionGDOResponse = httpclient.execute(exitCloseObstructionGDOReq);
			extentTest.log(Status.INFO, "Status code for Exit Close Obstruction= "+ exitCloseObstructionGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Enter Close Obstruction CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterCloseObstructionCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering close obstruction CDO ";
		try {
			gbVariables.setEnv();
			
			String enterCloseObstructionCDOURL = gbVariables.CDOWebserviceURL+ cdoSerialNumber + "/action";
			String actionType = "enter_close_obstruction";
			extentTest.log(Status.INFO,"Enter Close obstruction URL is "+enterCloseObstructionCDOURL);
			Map<String, Object> enterCloseObstructionCDOvals = new HashMap<String, Object>();
			enterCloseObstructionCDOvals.put("action_type", actionType);
			JSONObject enterCloseObstructionjson = new JSONObject(enterCloseObstructionCDOvals);

			StringEntity enterCloseObstructionCDOEntity = new StringEntity(enterCloseObstructionjson.toString(), "UTF8");
			enterCloseObstructionCDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterCloseObstructionCDOReq = new HttpPut(enterCloseObstructionCDOURL);
			enterCloseObstructionCDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterCloseObstructionCDOReq.setEntity(enterCloseObstructionCDOEntity);
			HttpResponse enterCloseObstructionCDOResponse = null;
			enterCloseObstructionCDOResponse = httpclient.execute(enterCloseObstructionCDOReq);
			extentTest.log(Status.INFO, "Status code for Enter Close Obstruction= "
					+ enterCloseObstructionCDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Close Obstruction CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitCloseObstructionCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting close obstruction CDO ";
		try {
			gbVariables.setEnv();
			
			String exitCloseObstructionCDOURL = gbVariables.CDOWebserviceURL+ cdoSerialNumber + "/action";
			String actionType = "exit_close_obstruction";
			extentTest.log(Status.INFO,"Exit Close obstruction URL is "+exitCloseObstructionCDOURL);
			Map<String, Object> exitCloseObstructionCDOvals = new HashMap<String, Object>();
			exitCloseObstructionCDOvals.put("action_type", actionType);
			JSONObject exitCloseObstructionCDOjson = new JSONObject(exitCloseObstructionCDOvals);

			StringEntity exitCloseObstructionGDOEntity = new StringEntity(exitCloseObstructionCDOjson.toString(), "UTF8");
			exitCloseObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitCloseObstructionCDOReq = new HttpPut(exitCloseObstructionCDOURL);
			exitCloseObstructionCDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitCloseObstructionCDOReq.setEntity(exitCloseObstructionGDOEntity);
			HttpResponse exitCloseObstructionCDOResponse = null;
			exitCloseObstructionCDOResponse = httpclient.execute(exitCloseObstructionCDOReq);
			extentTest.log(Status.INFO, "Status code for Exit Close Obstruction= "
					+ exitCloseObstructionCDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}
	/**
	 * Description :- To Enter Close Obstruction Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterCloseObstructionGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering close obstruction Gate ";
		try {
			gbVariables.setEnv();
			
			String enterCloseObstructionGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Close obstruction URL is "+enterCloseObstructionGateURL);
			String actionType = "enter_close_obstruction";
			Map<String, Object> enterCloseObstructionGatevals = new HashMap<String, Object>();
			enterCloseObstructionGatevals.put("action_type", actionType);
			JSONObject enterCloseObstructionjson = new JSONObject(enterCloseObstructionGatevals);

			StringEntity enterCloseObstructionGateEntity = new StringEntity(enterCloseObstructionjson.toString(), "UTF8");
			enterCloseObstructionGateEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterCloseObstructionGateReq = new HttpPut(enterCloseObstructionGateURL);
			enterCloseObstructionGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterCloseObstructionGateReq.setEntity(enterCloseObstructionGateEntity);
			HttpResponse enterCloseObstructionGateResponse = null;
			enterCloseObstructionGateResponse = httpclient.execute(enterCloseObstructionGateReq);
			extentTest.log(Status.INFO, "Status code for Enter Close Obstruction= "
					+ enterCloseObstructionGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Close Obstruction Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitCloseObstructionGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting close obstruction Gate ";
		try {
			gbVariables.setEnv();
			
			String exitCloseObstructionGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			String actionType = "exit_close_obstruction";
			extentTest.log(Status.INFO,"Exit Close obstruction URL is "+exitCloseObstructionGateURL);
			Map<String, Object> exitCloseObstructionGatevals = new HashMap<String, Object>();
			exitCloseObstructionGatevals.put("action_type", actionType);
			JSONObject exitCloseObstructionGatejson = new JSONObject(exitCloseObstructionGatevals);

			StringEntity exitCloseObstructionGDOEntity = new StringEntity(exitCloseObstructionGatejson.toString(), "UTF8");
			exitCloseObstructionGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitCloseObstructionGateReq = new HttpPut(exitCloseObstructionGateURL);
			exitCloseObstructionGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitCloseObstructionGateReq.setEntity(exitCloseObstructionGDOEntity);
			HttpResponse exitCloseObstructionGateResponse = null;
			exitCloseObstructionGateResponse = httpclient.execute(exitCloseObstructionGateReq);
			extentTest.log(Status.INFO, "Status code for Exit Close Obstruction= "
					+ exitCloseObstructionGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :-  To Block Photo eye  on the CDO
	 * @param gdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices blockPhotoEyeCDO(String gdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During photo eye block for CDO ";
		try {
			gbVariables.setEnv();
			
			String blockPhotoEyeCDOURL = gbVariables.CDOWebserviceURL+ gdoSerialNumber + "/action";
			String actionType = "block_photo_eye";
			extentTest.log(Status.INFO,"Block phot Eye URL is "+blockPhotoEyeCDOURL);
			Map<String, Object> blockPhotoEyeCDOvals = new HashMap<String, Object>();
			blockPhotoEyeCDOvals.put("action_type", actionType);
			JSONObject blockPhotoEyeCDOjson = new JSONObject(blockPhotoEyeCDOvals);

			StringEntity blockPhotoEyeCDOEntity = new StringEntity(blockPhotoEyeCDOjson.toString(), "UTF8");
			blockPhotoEyeCDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut blockPhotoEyeCDOReq = new HttpPut(blockPhotoEyeCDOURL);
			blockPhotoEyeCDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			blockPhotoEyeCDOReq.setEntity(blockPhotoEyeCDOEntity);
			HttpResponse blockPhotoEyeCDOResponse = null;
			blockPhotoEyeCDOResponse = httpclient.execute(blockPhotoEyeCDOReq);
			extentTest.log(Status.INFO, "Status code for Block photoeye= "
					+ blockPhotoEyeCDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Unblock Photo eye  on the CDO
	 * @param cdoSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices unblockPhotoEyeCDO(String cdoSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During photo eye un-block for CDO ";
		try {
			gbVariables.setEnv();
			
			String unblockPhotoEyeCDOURL = gbVariables.CDOWebserviceURL+ cdoSerialNumber + "/action";
			String actionType = "unblock_photo_eye";
			Map<String, Object> unblockPhotoEyeCDOvals = new HashMap<String, Object>();
			unblockPhotoEyeCDOvals.put("action_type", actionType);
			JSONObject unblockPhotoEyeCDOjson = new JSONObject(unblockPhotoEyeCDOvals);

			StringEntity unblockPhotoEyeCDOEntity = new StringEntity(unblockPhotoEyeCDOjson.toString(), "UTF8");
			unblockPhotoEyeCDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut unblockPhotoEyeCDOReq = new HttpPut(unblockPhotoEyeCDOURL);
			unblockPhotoEyeCDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			unblockPhotoEyeCDOReq.setEntity(unblockPhotoEyeCDOEntity);
			HttpResponse unblockPhotoEyeCDOResponse = null;
			unblockPhotoEyeCDOResponse = httpclient.execute(unblockPhotoEyeCDOReq);
			extentTest.log(Status.INFO, "Status code for Unblock photoeye= "
					+ unblockPhotoEyeCDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Send Gateway offline Service
	 * @param gatewaySerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices SendGatewayOfflineService1(String gatewaySerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During sending gateway offline ";
		try {
			gbVariables.setEnv();
			
			String gateWayServiceURL = gbVariables.SendGatewayOnlineWebserviceURL + gatewaySerialNumber + "/action";
			String actionType = "send_offline";
			Map<String, Object> gatewaySendOfflinevals = new HashMap<String, Object>();
			gatewaySendOfflinevals.put("action_type", actionType);
			JSONObject gatewayjson = new JSONObject(gatewaySendOfflinevals);

			StringEntity gateWaySendOfflineEntity = new StringEntity(gatewayjson.toString(), "UTF8");
			gateWaySendOfflineEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut gateWaySendOfflineReq = new HttpPut(gateWayServiceURL);
			gateWaySendOfflineReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			gateWaySendOfflineReq.setEntity(gateWaySendOfflineEntity);
			HttpResponse GateWaySendOfflineServiceResponse = null;
			GateWaySendOfflineServiceResponse = httpclient.execute(gateWaySendOfflineReq);
			extentTest.log(Status.INFO, "Status code for Gateway Send offline = "
					+ GateWaySendOfflineServiceResponse.getStatusLine().getStatusCode());

		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}
	
	/**
	 * Description :- To Enter Double Entrapment Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices enterDoubleEntrapmentGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During entering Double Entrapment Gate ";
		try {
			gbVariables.setEnv();
			
			String enterDoubleEntrapmentGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			extentTest.log(Status.INFO,"Enter Double Entrapment URL is "+enterDoubleEntrapmentGateURL);
			String actionType = "enter_double_entrapment";
			Map<String, Object> enterDoubleEntrapmentGatevals = new HashMap<String, Object>();
			enterDoubleEntrapmentGatevals.put("action_type", actionType);
			JSONObject enterDoubleEntrapmentjson = new JSONObject(enterDoubleEntrapmentGatevals);
			StringEntity enterDoubleEntrapmentGateEntity = new StringEntity(enterDoubleEntrapmentjson.toString(), "UTF8");
			enterDoubleEntrapmentGateEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut enterDoubleEntrapmentGateReq = new HttpPut(enterDoubleEntrapmentGateURL);
			enterDoubleEntrapmentGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			enterDoubleEntrapmentGateReq.setEntity(enterDoubleEntrapmentGateEntity);
			HttpResponse enterDoubleEntrapmentGateResponse = null;
			enterDoubleEntrapmentGateResponse = httpclient.execute(enterDoubleEntrapmentGateReq);
			extentTest.log(Status.INFO, "Status code for Enter Double Entrapment= " + enterDoubleEntrapmentGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}

	/**
	 * Description :- To Exit Double Entrapment Gate
	 * @param gateSerialNumber
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices exitDoubleEntrapmentGate(String gateSerialNumber) throws ClientProtocolException, IOException{
		String errorText ="Error: During exiting Double Entrapment Gate ";
		try {
			gbVariables.setEnv();
			
			String exitDoubleEntrapmentGateURL = gbVariables.GateWebserviceURL+ gateSerialNumber + "/action";
			String actionType = "exit_double_entrapment";
			extentTest.log(Status.INFO,"Exit Double Entrapment URL is "+exitDoubleEntrapmentGateURL);
			Map<String, Object> exitDoubleEntrapmentGatevals = new HashMap<String, Object>();
			exitDoubleEntrapmentGatevals.put("action_type", actionType);
			JSONObject exitDoubleEntrapmentGatejson = new JSONObject(exitDoubleEntrapmentGatevals);
			StringEntity exitDoubleEntrapmentGDOEntity = new StringEntity(exitDoubleEntrapmentGatejson.toString(), "UTF8");
			exitDoubleEntrapmentGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut exitDoubleEntrapmentGateReq = new HttpPut(exitDoubleEntrapmentGateURL);
			exitDoubleEntrapmentGateReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			exitDoubleEntrapmentGateReq.setEntity(exitDoubleEntrapmentGDOEntity);
			HttpResponse exitDoubleEntrapmentGateResponse = null;
			exitDoubleEntrapmentGateResponse = httpclient.execute(exitDoubleEntrapmentGateReq);
			extentTest.log(Status.INFO, "Status code for Exit Double Entrapment= "	+ exitDoubleEntrapmentGateResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}
	
	/**
	 * Description :- To set the cycle count for VGDO 
	 * @param VGDOSerialNumber, cyclecount
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices setCycleCountVGDO(String VGDOSerialNumber, String cycleCount) throws ClientProtocolException, IOException{
		String errorText ="Error: During set cycle count for VGDO ";
		try {
			gbVariables.setEnv();
			
			String setCycleCountVGDOURL = gbVariables.GDOWebserviceURL+ VGDOSerialNumber + "/action";
			String actionType = "set_dps_cycle_count";
			String action_param = cycleCount;
			extentTest.log(Status.INFO,"Set Cycle Count URL is "+setCycleCountVGDOURL);
			Map<String, Object> setCycleCountVGDOvals = new HashMap<String, Object>();
			setCycleCountVGDOvals.put("action_type", actionType);
			setCycleCountVGDOvals.put("action_param", action_param);
			
			JSONObject setCycleCountVGDOjson = new JSONObject(setCycleCountVGDOvals);

			StringEntity setCycleCountGDOEntity = new StringEntity(setCycleCountVGDOjson.toString(), "UTF8");
			setCycleCountGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut setCycleCountVGDOReq = new HttpPut(setCycleCountVGDOURL);
			setCycleCountVGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			setCycleCountVGDOReq.setEntity(setCycleCountGDOEntity);
			HttpResponse setCycleCountVGDOResponse = null;
			setCycleCountVGDOResponse = httpclient.execute(setCycleCountVGDOReq);
			extentTest.log(Status.INFO, "Status code for Set cycle count for VGDO= "
					+ setCycleCountVGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}
	
	/**
	 * Description :- To set the voltage for VGDO 
	 * @param VGDOSerialNumber, cyclecount
	 * @throws ClientProtocolException, IOException
	 * @return class object
	 */
	public WebServices setVoltageVGDO(String VGDOSerialNumber, String voltage) throws ClientProtocolException, IOException{
		String errorText ="Error: During set voltage for VGDO ";
		try {
			gbVariables.setEnv();
			
			String setVoltageVGDOURL = gbVariables.GDOWebserviceURL+ VGDOSerialNumber + "/action";
			String actionType = "set_voltage";
			String action_param = voltage;
			extentTest.log(Status.INFO,"Set Cycle Count URL is "+setVoltageVGDOURL);
			Map<String, Object> setVoltageVGDOvals = new HashMap<String, Object>();
			setVoltageVGDOvals.put("action_type", actionType);
			setVoltageVGDOvals.put("action_param", action_param);
			JSONObject setVoltageVGDOjson = new JSONObject(setVoltageVGDOvals);

			StringEntity setVoltageGDOEntity = new StringEntity(setVoltageVGDOjson.toString(), "UTF8");
			setVoltageGDOEntity.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
			HttpPut setVoltageVGDOReq = new HttpPut(setVoltageVGDOURL);
			setVoltageVGDOReq.setHeader("Authorization", gbVariables.AuthorizationCode);
			setVoltageVGDOReq.setEntity(setVoltageGDOEntity);
			HttpResponse setVoltageVGDOResponse = null;
			setVoltageVGDOResponse = httpclient.execute(setVoltageVGDOReq);
			extentTest.log(Status.INFO, "Status code for Set voltage for VGDO= "
					+ setVoltageVGDOResponse.getStatusLine().getStatusCode());
		}catch ( Exception e) {
			catchWebServiceException(e, errorText);
		}
		return this;
	}
}