package com.liftmaster.myq.utils;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.AutomationName;

public class DriverInitializer{

	public static DesiredCapabilities capabilities = new DesiredCapabilities();
	public static AndroidDriver<AndroidElement> androidDiver;
	public static IOSDriver<IOSElement> iOSDriver;

	/**
	 * Description : This function would set the capabilities for Android
	 * @param platform
	 * @param devName
	 * @param udid
	 * @param appPackage
	 * @param appActivity
	 * @param appiumServer
	 * @param appiumPort
	 * @return
	 */
	public static AndroidDriver<AndroidElement> getAndroidDriver(String platform,String devName,String udid, String appPackage,String appActivity,String appiumServer, int appiumPort,int bootstrapPort) {

		if (platform.equalsIgnoreCase("Android")) {
			System.out.println("MyQ Android");
			capabilities.setCapability("deviceName",devName);
			capabilities.setCapability("udid",udid);				
			capabilities.setCapability("platformName",platform);
			capabilities.setCapability("appPackage", appPackage);
			capabilities.setCapability("appActivity", appActivity);
			capabilities.setCapability("systemPort", bootstrapPort);
			capabilities.setCapability("automationName", "UiAutomator2");
			try {
				URL serverUrl = new URL("http://"+ appiumServer + ":" + appiumPort + "/wd/hub");
				androidDiver = new AndroidDriver<AndroidElement>(serverUrl, capabilities);
				androidDiver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);	
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}	
		return androidDiver;
	}

	/**
	 * Description : This function would set the capabilities for iOS
	 * @param platform
	 * @param devName
	 * @param udid
	 * @param app
	 * @param appiumServer
	 * @param appiumPort
	 * @param bundleid
	 * @param AutomationName
	 * @param xcodeOrgId
	 * @param xcodeSigningId
	 * @return
	 */
	public static IOSDriver<IOSElement> getiOSDriver(String platform,String devName,String udid,String appiumServer,int appiumPort,String bundleid,String AutomationName,String xcodeOrgId,String xcodeSigningId, String wdaLocalPort) {

		if (platform.equalsIgnoreCase("iOS")) {
			System.out.println("MyQ iOS");	 
			capabilities.setCapability("platformName", platform);
			capabilities.setCapability("platformVersion", "10.3");
			capabilities.setCapability("deviceName", devName);
			capabilities.setCapability("udid",udid);
			capabilities.setCapability("AutomationName",AutomationName);
			capabilities.setCapability("xcodeOrgId",xcodeOrgId);
			capabilities.setCapability("xcodeSigningId",xcodeSigningId);
			capabilities.setCapability("bundleId", bundleid);
			capabilities.setCapability("wdaLocalPort", wdaLocalPort);
			try {
				URL serverUrl = new URL("http://"+ appiumServer + ":" + appiumPort + "/wd/hub");
				iOSDriver = new IOSDriver<IOSElement>(serverUrl, capabilities);
				iOSDriver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
		}	
		return iOSDriver;
	}
}

