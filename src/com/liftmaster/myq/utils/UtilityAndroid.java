package com.liftmaster.myq.utils;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class UtilityAndroid {

	private AndroidDriver<AndroidElement> androidDriver;
	private ExtentTest extentTestAndroid;
	String elementText;
	String currentMTA = GlobalVariables.currentTestAndroid;
	Dimension size;

	@AndroidFindBy(id = "com.chamberlain.android.liftmaster.myq:id/Rules_List_Fragment_ProgressBar")
	public AndroidElement spinner;

	@AndroidFindBy(id = "android:id/progress")
	public AndroidElement spinner_on_save;


	/**
	 * Description : class constructor
	 * @param androidDriver
	 * @param extentTestAndroid
	 */
	public UtilityAndroid(AndroidDriver<AndroidElement> androidDriver, ExtentTest extentTestAndroid) {
		this.androidDriver=androidDriver;
		this.extentTestAndroid = extentTestAndroid;
	}

	/**
	 * Description : This function is used click on element
	 * @param mobileElement
	 */
	public void clickAndroid(MobileElement mobileElement) {
		try {
			elementText = mobileElement.getText();
			genericWait(mobileElement, "Click");
			mobileElement.click();
			if(elementText.equals("")){
				extentTestAndroid.log(Status.INFO, "Clicked on : " + elementText );
			} else {
				extentTestAndroid.log(Status.INFO, "Clicked on : "+mobileElement);
			}
		} catch (Exception e) {
			extentTestAndroid.log(Status.FAIL, "Failed to click on : " + elementText);
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function is used clear the field
	 * @param element
	 */
	public void clearAndroid(AndroidElement element) {
		try {
			elementText = element.getText();
			if(elementText.equals("")){
				elementText = element.getAttribute("contentDescription");
			}
			element.clear();
			extentTestAndroid.log(Status.INFO, "field clear for : " + elementText);
		} catch (Exception e) {
			extentTestAndroid.log(Status.FAIL, "Unable to clear field for : " + elementText);
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description :  This function is used enter text into the field
	 * @param element
	 * @param text
	 */
	public void enterTextAndroid(AndroidElement element , String text) {
		try {
			elementText = element.getText();
			if(elementText.equals("")){
				elementText = element.getAttribute("contentDescription");
			}
			element.sendKeys(text);
			extentTestAndroid.log(Status.INFO, "Text entered -> " + text);
		} catch (Exception e ) {
			extentTestAndroid.log(Status.FAIL, "Failed to entered text in field : " + elementText);
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function is used scroll from bottom to top one time
	 * @param element
	 */
	public void scrollToElement(AndroidElement element) {
		size = androidDriver.manage().window().getSize();
		int scrollStart = (int) (size.getHeight() * 0.5);
		int scrollEnd = (int) (size.getHeight() * 0.2);
		try {
			androidDriver.swipe(0,scrollStart,0,scrollEnd,5000);
			genericWait(element, "Click");
			//androidDriver.swipe(0, 2106, 0, 341, 3000);
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function would wait for specific action such as element be click-able or visible ,etc
	 * @param mobileElement
	 * @param elementType
	 */
	public void genericWait(MobileElement mobileElement, String elementType) {
		WebDriverWait wait = new WebDriverWait(androidDriver, 30);
		try {
			if (elementType.equalsIgnoreCase("Click")) { 
				wait.until(ExpectedConditions.elementToBeClickable(mobileElement));
			} else if (elementType.equalsIgnoreCase("Visible")) { 
				wait.until(ExpectedConditions.visibilityOf(mobileElement));	
			} else if (elementType.equalsIgnoreCase("Pop-up")) { 
				wait.until(ExpectedConditions.alertIsPresent());	
			}
		} catch (Exception e) {
		}
	}

	/**
	 * Description : This function is would capture screenshots
	 * @param androidDriver
	 */
	public void captureScreenshot(AndroidDriver<AndroidElement> androidDriver ) {
		String fileName = new SimpleDateFormat("dd-MM-yyyy-hhmmss").format(new Date());
		String folderName = System.getProperty("user.dir")+"//Screenshots//";
		try {
			File srcFile = ((TakesScreenshot)androidDriver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(srcFile, new File(folderName + currentMTA + "-"+ fileName+ ".png"));
			extentTestAndroid.pass("Screenshot captured -> " + "<a href='file:///"+folderName + currentMTA + "-"+ fileName+ ".png'>Screenshot</a>");
			extentTestAndroid.log(Status.INFO, "Screenshot captured with file name " + " " + fileName +  ".png"+ " for ticket " + currentMTA );
		} catch (IOException e) {
			extentTestAndroid.log(Status.INFO, "Screenshot not captured");
		}
	}

	/**
	 * Description : This function to handle exceptions occurred
	 * @param androidDriver
	 * @param e
	 */
	public void catchExceptions(AndroidDriver<AndroidElement> androidDriver, Exception e) {
		captureScreenshot(androidDriver);
		String notFoundElement;
		if (e.getClass().getName().equals("org.openqa.selenium.UnhandledAlertException")) {
			androidDriver.switchTo().alert().dismiss();
			extentTestAndroid.log(Status.INFO, "Unhandled Alert Exception occurred");
		} else if (e.getClass().getName().equals("org.openqa.selenium.NoSuchElementException")) {
			extentTestAndroid.log(Status.FAIL, "No Such Element Exception occured : " + " Test Case " + currentMTA + " failed"); 
		} else if (e.getClass().getName().equals("java.lang.NullPointerException")) {
			notFoundElement= (e.getMessage().substring((e.getMessage().indexOf("html")), (e.getMessage().indexOf("For")))).split(": ")[1];
			notFoundElement = notFoundElement.split("name ")[1];
			extentTestAndroid.log(Status.FAIL, "No Such Element Exception occured for element "+ notFoundElement + ". Hence Test Case " + currentMTA + " failed");
		} else if (e.getClass().getName().equals("java.lang.IndexOutOfBoundsException")) {
			extentTestAndroid.log(Status.FAIL, " Index out of Bound occured : " + " Test Case " + currentMTA + " failed"); 
		} else if (e.getClass().getName().equals("org.openqa.selenium.TimeoutException")) {
			extentTestAndroid.log(Status.FAIL, " Timeout Exception occured :" + " Test Case " + currentMTA + " failed"); 
		} else if (e.getClass().getName().trim().equals("org.openqa.selenium.WebDriverException")) {
			extentTestAndroid.log(Status.FAIL, " Webdriver Exception occured : " + " Test Case " + currentMTA + " failed"); 
			e.printStackTrace();
		} else {
			extentTestAndroid.log(Status.FAIL, " Exception" +e.getClass().getName()+ "occured : " + " Test Case " + currentMTA + " failed"); 
		}
	}

	/**
	 * Description : This function would set the required time 
	 * @param element
	 * @param duration
	 */
	public void setHours(List<AndroidElement> element , int duration) {
		try {
			int iIndex;
			for (iIndex = 0 ; iIndex < element.size(); iIndex++) {
				if (element.get(iIndex).getAttribute("contentDescription").equals(""+duration)) {
					clickAndroid(element.get(iIndex));	
					break;
				}
			}
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function would get the current time of mobile device
	 * @return formattedDate
	 */
	public String getCurrentTime() {
		SimpleDateFormat dateFormat;
		String formattedDate = null;
		try {
			Calendar cal = Calendar.getInstance();
			dateFormat = new SimpleDateFormat("HH:mm");
			formattedDate = dateFormat.format(cal.getTime());
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
		return formattedDate;
	}

	/**
	 * Description : This function is used swipe upwards
	 */
	public void swipeUp() {
		size = androidDriver.manage().window().getSize();
		int scrollStart = (int) (size.getHeight() * 0.8);
		int scrollEnd = (int) (size.getHeight() * 0.2);
		try {
			androidDriver.swipe(0, scrollStart, 0, scrollEnd, 3000);
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function would get the current day of the week from device
	 * @return dayOfTheWeek
	 */
	public String getCurrentDay() {
		SimpleDateFormat dateFormat;
		String dayOfTheWeek  = null;
		try {
			//"EEEE" is passed to get the complete day name as "Monday"
			dateFormat = new SimpleDateFormat("EEEE");
			Date date = new Date();
			dayOfTheWeek = dateFormat.format(date);

		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
		return dayOfTheWeek;
	}

	/**
	 * Description : This function would open the notification panel
	 */
	public void openNotificationPanel() {
		try {
			androidDriver.openNotifications();
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function would clear / close the notification panel
	 * @param element
	 * @throws InterruptedException 
	 */
	public void closeNotificationPanel(String udid) throws InterruptedException {
		//below is to clear the notification
		try {
			Thread.sleep(2000);
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 400 500 400");
			Thread.sleep(2000);
		} catch (IOException e1) {
			// catch Exception and continue with execution 
		}
		//below is to close the notification panel
		try {
			Thread.sleep(2000);
			Runtime.getRuntime().exec("adb -s "+ udid +" shell input swipe 0 400 0 0");
			Thread.sleep(2000);
		} catch (IOException e1) {
			// catch Exception and continue with execution
		}
	}

	/**
	 * Description : This function would select minutes on the clock
	 * @param zeroth_minute
	 * @param thirtyth_minute
	 * @param duration
	 * @throws InterruptedException
	 */
	public void selectTimeOnClock(MobileElement zeroth_minute,MobileElement thirtyth_minute,int duration , String durationType) throws InterruptedException{
		try {
			int leftX1;
			int rightX1;
			int middleX1;
			int upperY1;
			int lowerY1;
			int middleY1;
			int leftX2;
			int rightX2;
			int middleX2;
			int upperY2;
			int lowerY2;
			int middleY2;
			int centreX;
			int centreY;
			int term1;
			int term2;
			int diameter;
			int radius;
			double angle;
			double cosAngle;
			double sinAngle;
			double X;
			double Y;

			// Calculate center coordinates of first element
			leftX1 = zeroth_minute.getLocation().getX();
			rightX1 = leftX1 + zeroth_minute.getSize().getWidth();
			middleX1 = (rightX1 + leftX1) / 2;
			upperY1 = zeroth_minute.getLocation().getY();
			lowerY1 = upperY1 + zeroth_minute.getSize().getHeight();
			middleY1 = (upperY1 + lowerY1) / 2;

			// Calculate center coordinates of second element
			leftX2 = thirtyth_minute.getLocation().getX(); 
			rightX2 = leftX2 + thirtyth_minute.getSize().getWidth();
			middleX2 = (rightX2 + leftX2) / 2;
			upperY2 = thirtyth_minute.getLocation().getY();
			lowerY2 = upperY2 + thirtyth_minute.getSize().getHeight();
			middleY2 = (upperY2 + lowerY2) / 2;

			centreX=(middleX1+middleX2)/2;
			centreY=(middleY1+middleY2)/2;
			term1=(middleX2-middleX1);
			term2=(middleY2-middleY1);
			diameter=(int) Math.sqrt((Math.pow(term1, 2))+(Math.pow(term2, 2)));
			radius=diameter/2;
			if (durationType.equalsIgnoreCase("HOURS")) {
				angle = (360 + ((duration * 5) - 15) * 6);
			} else {
				angle =(360 + ((duration - 15) * 6));
			}
			cosAngle = Math.cos(Math.toRadians(angle));
			sinAngle = Math.sin(Math.toRadians(angle));
			X = (centreX + (radius * cosAngle));
			Y = (centreY + (radius * sinAngle)); 

			//selecting the minute
			TouchAction tapOnMinute = new TouchAction(androidDriver);
			tapOnMinute.tap((int)X,(int)Y).perform();
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}

	public void longPress(AndroidElement element) {
		TouchAction Action = new TouchAction(androidDriver);
		Action.longPress(element).perform();
	}

	/**
	 * Description : This function would swipe horizontally (right to left)
	 * @param element
	 */
	public void swipingHorizontal(AndroidElement element) {
		try {
			//Get the size of screen.
			Point p = element.getCenter();
			int y2 = p.getY();
			Dimension  size = androidDriver.manage().window().getSize();
			int x1 = (int) (size.width * 0.20);

			TouchAction action = new TouchAction(androidDriver);
			action.longPress(element).moveTo(x1,y2).release().perform();
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}

	/**
	 * Description : This function would get the system date
	 * @return
	 */
	public String getSystemDate() {
		String sysDate = "null";
		try {
			DateFormat dateFormat = new SimpleDateFormat("MMM dd,yyyy");
			Date date = new Date();
			sysDate = dateFormat.format(date);
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
		return sysDate;
	}

	/**
	 * Description : This function would wait for spinner that appears while loading the page
	 * @throws InterruptedException 
	 */
	public void waitForGenericSpinner() throws InterruptedException {
		try {
			Thread.sleep(4000);
		} catch (Exception e) {
			//
		}
	}

	/**
	 * Description : This function would swipe right to left on screen
	 */
	public void swipeRightToLeft() {
		try {
			androidDriver.swipe(787, 0, 50, 0, 3000);
		} catch (Exception e) {
			catchExceptions(androidDriver, e);
		}
	}
}