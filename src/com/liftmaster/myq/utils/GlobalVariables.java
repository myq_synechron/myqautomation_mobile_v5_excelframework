package com.liftmaster.myq.utils;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import com.liftmaster.myq.testngrunner.RunMethod; 

public class GlobalVariables {

	public static AndroidDriver<AndroidElement> androidDriver = null;
	public static IOSDriver<IOSElement> iOSDriver = null;
	public static String currentTestIOS = null;
	public static String currentTestAndroid = null;
	
	public static String actualEnvironment = RunMethod.getEnv() ;
	public static String envForURL = setEnv();
	
	/**
	 * Description : This method would set the environment to be used in the URL for webservice call as per passed from excel
	 * @return String
	 */
	public static String setEnv() {
		String temp = null;
		if(actualEnvironment.equalsIgnoreCase("Pre-Prod")) {
			temp = "pp";
		} else if(actualEnvironment.equalsIgnoreCase("Dev")) {
			temp = "dev";
		} 
		return temp;
	}
	

	/**
	 * All variables for service URL's
	 */
	public static String GetDeviceListWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/devices/");
	public static String CreateGatewayServiceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/gateways/");
	public static String SendGatewayOnlineWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/gateways/");
	public static String AddLightWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/lights/");
	public static String AddGDOWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/garageDoorOpeners/");
	public static String GDOWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/garageDoorOpeners/");
	public static String AddCDOWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/commercialDoorOpeners/");
	public static String CDOWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/commercialDoorOpeners/");
	public static String AddGateWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/gates/");
	public static String GateWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/gates/");
	public static String LightWebserviceURL = "http://myqsociety-".concat(envForURL).concat("-v2.centralus.cloudapp.azure.com:8155/api/lights/");
	
	/**
	 * All variables used for headers,
	 */
	public static String AuthorizationCode = "Bearer 70533feb-b029-4c84-a7ee-05ac2408e15c";
}
